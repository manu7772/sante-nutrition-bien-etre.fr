<?php

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

// ini_set('memory_limit','512M');
set_time_limit(180);
// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read https://symfony.com/doc/current/setup.html#checking-symfony-application-configuration-and-setup
// for more information
//umask(0000);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.
if (isset($_SERVER['HTTP_CLIENT_IP'])
	|| isset($_SERVER['HTTP_X_FORWARDED_FOR'])
	|| !(in_array(@$_SERVER['REMOTE_ADDR'], [
			'127.0.0.1',
			'::1',
			'77.144.28.171', // château Pont d'Ain
			'81.185.166.167', // Portable Manu
			'78.121.199.80', // Maison Cerdon - MEtronic
		], true)
		|| preg_match('/^192\\.168\\.1/', @$_SERVER['REMOTE_ADDR'])
		|| PHP_SAPI === 'cli-server'
	)
) {
	header('HTTP/1.0 403 Forbidden');
	exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information. If you are in DEV environment administrator, add your IP address on this site : '.@$_SERVER['REMOTE_ADDR'].'.');
}

require __DIR__.'/../vendor/autoload.php';
Debug::enable();

$kernel = new AppKernel('dev', true);
if (PHP_VERSION_ID < 70000) {
	$kernel->loadClassCache();
}
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
