<?php
namespace ModelApi\AnnotBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
// use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\HttpKernel\Event\GetResponseEvent;
// use Symfony\Component\HttpKernel\HttpKernel;
// use Symfony\Component\HttpKernel\HttpKernelInterface;
// use Symfony\Component\Translation\IdentityTranslator;
use Doctrine\Common\Inflector\Inflector;

use Doctrine\common\EventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
// use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
// use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
// use Doctrine\ORM\Event\OnClassMetadataNotFoundEventArgs;
// use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
// use Doctrine\ORM\Event\OnClearEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\Annotation;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
// use Doctrine\Common\Annotations\CachedReader;
// use Doctrine\Common\Cache\ApcCache;
// use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\SerializationContext;

// AnnotBundle
// use ModelApi\AnnotBundle\Annotation\AddModelTransformer;
// use ModelApi\AnnotBundle\Annotation\AttachFileformat;
use ModelApi\AnnotBundle\Annotation\Adminvalidated;
use ModelApi\AnnotBundle\Annotation\AttachUser;
use ModelApi\AnnotBundle\Annotation\AttachContexEnvironment;
use ModelApi\AnnotBundle\Annotation\AttachValidContentTypes;
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
use ModelApi\AnnotBundle\Annotation\Bddid;
use ModelApi\AnnotBundle\Annotation\ChooseInitPageweb;
use ModelApi\AnnotBundle\Annotation\HasTranslatable;
use ModelApi\AnnotBundle\Annotation\InitializeLogg;
use ModelApi\AnnotBundle\Annotation\injectBundles;
use ModelApi\AnnotBundle\Annotation\InjectDomainsChoices;
use ModelApi\AnnotBundle\Annotation\InjectEventClasses;
use ModelApi\AnnotBundle\Annotation\InjectItemtypesList;
use ModelApi\AnnotBundle\Annotation\InjectLocalesChoices;
use ModelApi\AnnotBundle\Annotation\InjectPagewebList;
use ModelApi\AnnotBundle\Annotation\injectRoles;
// use ModelApi\AnnotBundle\Annotation\InjectSessionLocale;
use ModelApi\AnnotBundle\Annotation\injectTemplatestructures;
// use ModelApi\AnnotBundle\Annotation\HasCache;
use ModelApi\AnnotBundle\Annotation\InsertContextdate;
use ModelApi\AnnotBundle\Annotation\InsertLoggUser;
use ModelApi\AnnotBundle\Annotation\JoinedOwnDirectory;
use ModelApi\AnnotBundle\Annotation\JoinSections;
use ModelApi\AnnotBundle\Annotation\PostCreate;
use ModelApi\AnnotBundle\Annotation\preferedPageweb;
use ModelApi\AnnotBundle\Annotation\Preferences;
// use ModelApi\AnnotBundle\Annotation\StoreDirContent;
use ModelApi\AnnotBundle\Annotation\Timezone;
use ModelApi\AnnotBundle\Annotation\Tomcat;
use ModelApi\AnnotBundle\Annotation\Typitem;
// use ModelApi\AnnotBundle\Annotation\Translatable;
use ModelApi\AnnotBundle\Annotation\UniqueField;
use ModelApi\AnnotBundle\Annotation\UpdateDate;
use ModelApi\AnnotBundle\Annotation\AnnotAnnotationInterface;
use ModelApi\AnnotBundle\Annotation\EventAnnotationInterface;
use ModelApi\AnnotBundle\Annotation\EntityAnnotationInterface;
// BaseBundle
use ModelApi\BaseBundle\Entity\Logg;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceCacheInterface;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceLogg;
use ModelApi\BaseBundle\Service\servicePreference;
use ModelApi\BaseBundle\Service\serviceTypitem;
use ModelApi\BaseBundle\Component\Preference as PreferenceComponent;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Fileformat;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\NestedInterface;
// use ModelApi\FileBundle\Component\Linkdata;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceImage;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// MarketBundle
use ModelApi\MarketBundle\Entity\Baseprod;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
use ModelApi\InternationalBundle\Service\serviceTranslation;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
use ModelApi\InternationalBundle\Service\serviceTimezone;
use ModelApi\InternationalBundle\Entity\Language;
use ModelApi\InternationalBundle\Entity\TwgTranslation;
use ModelApi\InternationalBundle\Entity\managedEntity;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\OwnerTierInterface;
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Service\serviceRoles;
use ModelApi\UserBundle\Service\serviceGrants;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
use ModelApi\PagewebBundle\Service\servicePageweb;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Service\serviceCruds;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotationInterface;

// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \ReflectionClass;
use \ReflectionProperty;
use \ReflectionMethod;
use \RuntimeException;
use \Exception;
use \DateTime;

class serviceAnnotation implements EventSubscriber {

	use \ModelApi\BaseBundle\Service\baseService;

	// https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
	// https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html

	const MAX_REFLUSH = 20;
	// const RESET_CACHE_TRANSLATIONS = true;

	protected $container;
	protected $reader;
	protected $infiniteLoopPreventer;
	protected $postFlushActions;
	protected $modeContext;
	// protected $deepControl;
	// protected $publicBundle;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		// ************************************************************************
		// --> DO NOT DECLARE OTHER SERVICES HERE, BECAUSE OF CIRCULAR REFERENCE!!!
		// ************************************************************************
		$this->infiniteLoopPreventer = 0;
		$this->postFlushActions = new ArrayCollection();
		$this->reader = new AnnotationReader();
		$this->modeContext = $this->container->get(serviceKernel::class)->isFixturesContext() ? 'fixtures' : 'default';
		// $this->publicBundle = $this->container->get(serviceKernel::class)->isPublicBundle();
		// $this->deepControl = !$this->container->get(serviceKernel::class)->isPublicBundle() && $this->container->get(serviceKernel::class)->isDev();
		// $this->deepControl = false;
		return $this;
	}

	/**
	 * Master running Events
	 */
	public function getSubscribedEvents() {
		return array(
			BaseAnnotation::postNew,
			BaseAnnotation::checkItem,
			BaseAnnotation::preCreateForm,
			BaseAnnotation::preUpdateForm,
			Events::postLoad,
			Events::prePersist,
			Events::postPersist,
			Events::preUpdate,
			Events::postUpdate,
			Events::preRemove,
			Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			Events::onFlush,
			Events::postFlush,
			// Events::onClear,
		);
	}

	public function postNew(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf Item) $entity->initItem($this->container->get(serviceBasedirectory::class));
		$entity->setServiceKernel($this->container->get(serviceKernel::class));
		$entity->setModeContext($this->modeContext);
		if($entity instanceOf CrudInterface) {
			$entity->setServiceCruds($this->container->get(serviceCruds::class));
			$this->convertAnnotations($args, BaseAnnotation::postNew);
		}
		$entity->setPostNewEventsLoaded(true);
	}

	public function checkItem(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf CrudInterface) {
			$this->convertAnnotations($args, BaseAnnotation::checkItem);
		}
	}

	public function preCreateForm(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		// $entity->setServiceKernel($this->container->get(serviceKernel::class));
		$entity->setModeContext($this->modeContext);
		$entity->_for_form = isset($entity->_for_form) ? array_unique(array_merge($entity->_for_form, ['preCreateForm'])) : ['preCreateForm'];
		if($entity instanceOf CrudInterface) {
			$this->convertAnnotations($args, BaseAnnotation::preCreateForm);
		}
	}

	public function preUpdateForm(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		// $entity->setServiceKernel($this->container->get(serviceKernel::class));
		$entity->setModeContext($this->modeContext);
		$entity->_for_form = isset($entity->_for_form) ? array_unique(array_merge($entity->_for_form, ['preUpdateForm'])) : ['preUpdateForm'];
		if($entity instanceOf CrudInterface) {
			$this->convertAnnotations($args, BaseAnnotation::preUpdateForm);
			// if(!$this->isXmlHttpRequest()) YamlLog::directYamlLogfile(['preUpdateForm' => $entity->getShortname().' > '.$entity]);
		}
	}

	public function postLoad(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf Item) $entity->initItem($this->container->get(serviceBasedirectory::class));
		$entity->setServiceKernel($this->container->get(serviceKernel::class));
		$entity->setModeContext($this->modeContext);
		if($entity instanceOf CrudInterface) {
			$entity->setServiceCruds($this->container->get(serviceCruds::class));
			$this->convertAnnotations($args, Events::postLoad);
			// if(!$this->isXmlHttpRequest()) YamlLog::directYamlLogfile(['postLoad' => $entity->getShortname().' > '.$entity]);
		}
		$entity->setPostLoadEventsLoaded(true);
	}

	public function prePersist(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf CrudInterface) {
			// $entity->setServiceCruds($this->container->get(serviceCruds::class));
			$this->convertAnnotations($args, Events::prePersist);
		}
	}

	public function postPersist(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf CrudInterface) {
			$this->convertAnnotations($args, Events::postPersist);
		}
	}

	public function preUpdate(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf CrudInterface) {
			// $entity->setServiceCruds($this->container->get(serviceCruds::class));
			$this->convertAnnotations($args, Events::preUpdate);
		}
	}

	public function postUpdate(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf CrudInterface) {
			$this->convertAnnotations($args, Events::postUpdate);
		}
	}

	public function preRemove(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf CrudInterface) {
			$this->convertAnnotations($args, Events::preRemove);
		}
	}

	public function postRemove(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf CrudInterface) {
			$this->convertAnnotations($args, Events::postRemove);
		}
	}

	// public function loadClassMetadata(LoadClassMetadataEventArgs $args) {
	// }

	// public function onClassMetadataNotFound(LoadClassMetadataEventArgs $args) {
	// }

	// public function preFlush(PreFlushEventArgs $args) {
	// }

	public function onFlush(OnFlushEventArgs $args) {
		// if($this->container->get(serviceKernel::class)->isProfiler()) return;
		$em = $args->getEntityManager();
		$uow = $em->getUnitOfWork();
		foreach (array_merge($uow->getScheduledEntityInsertions(), $uow->getScheduledEntityUpdates(), $uow->getScheduledEntityDeletions()) as $entity) {
			if($entity instanceOf CrudInterface) {
				$this->convertAnnotations($args, Events::onFlush, $entity);
			}
		}
		// $uow->computeChangeSets();
	}

	public function postFlush(PostFlushEventArgs $args) {
		// if($this->container->get(serviceKernel::class)->isProfiler()) return;
		if($this->infiniteLoopPreventer++ >= static::MAX_REFLUSH && $this->isDefaultContext()) throw new Exception("Error on PostFlush infinite loop! Stopped now after ".$this->infiniteLoopPreventer." loops!", 1);
		$em = $args->getEntityManager();
		$serviceTranslation = $this->container->get(serviceTranslation::class);
		$this->postFlushActions->add($serviceTranslation->postFlushCompileAllTranslations($args));
		// $reflush = $serviceTranslation->postFlushCompileAllTranslations($args);
		$reflush = false;
		foreach ($this->postFlushActions as $action) {
			if(is_callable($action)) $reflush = $reflush || $action($args);
				else if(is_bool($action)) $reflush = $reflush || $action;
				else throw new Exception("Items puted in postFlushActions must be callable (returning boolean) or boolean. Got ".gettype($action)."!", 1);
			$this->postFlushActions->removeElement($action);
		}
		if($reflush) $em->flush(); else $this->infiniteLoopPreventer = 0;
	}


	/***********************************/
	/*** ANNOTATIONS
	/***********************************/

	public function getAnnotationsList() {
		// if($this->container->get(serviceKernel::class)->isProfiler()) return [];
		return array(
			Adminvalidated::class,
			AttachContexEnvironment::class,
			AttachUser::class,
			AttachValidContentTypes::class,
			Bddid::class,
			ChooseInitPageweb::class,
			// HasCache::class,
			HasTranslatable::class,
			InitializeLogg::class,
			injectBundles::class,
			InjectDomainsChoices::class,
			InjectEventClasses::class,
			InjectItemtypesList::class,
			InjectLocalesChoices::class,
			InjectPagewebList::class,
			injectRoles::class,
			injectTemplatestructures::class,
			InsertContextdate::class,
			InsertLoggUser::class,
			JoinedOwnDirectory::class,
			JoinSections::class,
			preferedPageweb::class,
			Preferences::class,
			// StoreDirContent::class,
			Timezone::class,
			Tomcat::class,
			Typitem::class,
			UniqueField::class,
			UpdateDate::class,
			PostCreate::class,
		);
	}




	/***********************************/
	/*** APPLY ANNOTATIONS
	/***********************************/

	/**
	 * Launch annotations operations in Event context
	 * @param *EventArgs $args
	 */
	protected function convertAnnotations($args, $eventType, $entity = null) {
		if($this->isMasterRequest()) {
			$entity ??= $args->getEntity();
			if(!is_object($entity)) throw new Exception("Error ".__METHOD__."(): no entity found!", 1);
			$annotationsList = $this->getAnnotationsList();
			$entityAnnotations = $this->container->get(serviceCruds::class)->getFilteredAnnotations(
				$entity,
				$annotationsList,
				[],
				function($annotation) use ($eventType) {
					return $annotation instanceOf EventAnnotationInterface && $annotation->hasTypeEvent($eventType);
				},
			);
			foreach ($annotationsList as $annotation) {
				foreach ($entityAnnotations['properties'] ??= [] as $property => $propertyAnnotations) {
					foreach ($propertyAnnotations as $propertyAnnotation) if($propertyAnnotation instanceOf $annotation && $propertyAnnotation->isEnvContexts($this->getModeContext())) {
						if($propertyAnnotation instanceOf EntityAnnotationInterface) $propertyAnnotation->setEntity($entity, ['property' => $property]);
						$annot_call = $propertyAnnotation->__toString();
						$this->$annot_call($propertyAnnotation, $args, $eventType);
					}
				}
				foreach ($entityAnnotations['methods'] ??= [] as $method => $methodAnnotations) {
					foreach ($methodAnnotations as $methodAnnotation) if($methodAnnotation instanceOf $annotation && $methodAnnotation->isEnvContexts($this->getModeContext())) {
						if($methodAnnotation instanceOf EntityAnnotationInterface) $methodAnnotation->setEntity($entity, ['method' => $method]);
						$annot_call = $methodAnnotation->__toString();
						$this->$annot_call($methodAnnotation, $args, $eventType);
					}
				}
				foreach ($entityAnnotations['class'] ??= [] as $classAnnotation) if($classAnnotation instanceOf $annotation && $classAnnotation->isEnvContexts($this->getModeContext())) {
					if($classAnnotation instanceOf EntityAnnotationInterface) $classAnnotation->setEntity($entity, []);
					$annot_call = $classAnnotation->__toString();
					$this->$annot_call($classAnnotation, $args, $eventType);
				}
			}
		}
	}


	/***********************************/
	/*** REQUEST CONTEXT
	/***********************************/

	public function isMasterRequest() {
		return $this->container->get(serviceKernel::class)->isMasterRequest();
	}

	public function isXmlHttpRequest() {
		return $this->container->get(serviceKernel::class)->isXmlHttpRequest();
	}

	public static function getValidContexts() {
		return [
			'default',
			'check',
			'init',
			'fixtures',
		];
	}

	public function getModeContext() {
		return $this->modeContext;
	}

	public function setModeContext($mode) {
		$this->modeContext = $mode;
		// if(!$this->isDefaultContext()) $this->deepControl = true;
	}

	public function isModeContext($mode) {
		return $this->modeContext === $mode;
	}

	public function isDefaultContext() {
		return $this->isModeContext('default');
	}

	public function isCheckContext() {
		return $this->isModeContext('check');
	}

	public function isInitContext() {
		return $this->isModeContext('init');
	}

	public function isFixturesContext() {
		return $this->isModeContext('fixtures');
	}

	public function isPublicFiltered() {
		return $this->container->get(serviceKernel::class)->isPublicBundle();
	}

	public function isDeepControl() {
		return !$this->isPublicFiltered() && $this->container->get(serviceKernel::class)->isDev();
	}

	/***********************************/
	/*** GETTERS / SETTERS
	/***********************************/

	/**
	 * Get Getter for property of $entity
	 * @param Object $property
	 * @param Object $entity
	 * @param BaseAnnotation $propertyAnnotation = null
	 * @return string
	 */
	public static function getGetter($property, $entity, $propertyAnnotation = null, $priority = 'singularized') {
		// if(empty($property) || !is_string($property)) throw new Exception("Error ".__METHOD__."(): property is ".(is_object($property) ? get_class($property) : json_encode($property))." for ".get_class($entity)."!", 1);
		return isset($propertyAnnotation->getter) && is_string($propertyAnnotation->getter) && strlen($propertyAnnotation->getter) > 3 ? $propertyAnnotation->getter : Inflector::camelize('get_'.$property);
		// 
		// $search = $propertyAnnotation instanceOf BaseAnnotation && property_exists($propertyAnnotation, 'getter') && is_string($propertyAnnotation->getter) ? $propertyAnnotation->getter : 'get';
		// if(is_string($propertyAnnotation) && (method_exists($entity, $propertyAnnotation) || $propertyAnnotation === 'get')) $search = $propertyAnnotation;
		// $getter = serviceClasses::getPropertyMethod($property, $entity, $search, $priority);
		// // echo('<pre>');var_dump(['Property' => $property, 'Annotation' => $propertyAnnotation, 'Getter' => $getter]);echo('</pre>');
		// if(!is_string($getter)) throw new Exception("Error ".__METHOD__."(): getter could not be find for ".json_encode(get_class($entity))." property ".json_encode($property)."!", 1);
		// return $getter;
	}

	/**
	 * Get Getter for property of $entity
	 * @param Object $property
	 * @param Object $entity
	 * @param BaseAnnotation $propertyAnnotation = null
	 * @return string
	 */
	public static function getSetter($property, $entity, $propertyAnnotation = null, $priority = 'singularized') {
		// if(empty($property) || !is_string($property)) throw new Exception("Error ".__METHOD__."(): property is ".(is_object($property) ? get_class($property) : json_encode($property))." for ".get_class($entity)."!", 1);
		return isset($propertyAnnotation->setter) && is_string($propertyAnnotation->setter) && strlen($propertyAnnotation->setter) > 3 ? $propertyAnnotation->setter : Inflector::camelize('set_'.$property);
		// 
		// $search = $propertyAnnotation instanceOf BaseAnnotation && property_exists($propertyAnnotation, 'setter') && is_string($propertyAnnotation->setter) ? $propertyAnnotation->setter : 'set';
		// if(is_string($propertyAnnotation) && (method_exists($entity, $propertyAnnotation) || $propertyAnnotation === 'set')) $search = $propertyAnnotation;
		// $setter = serviceClasses::getPropertyMethod($property, $entity, $search, $priority);
		// // echo('<pre>');var_dump(['Property' => $property, 'Annotation' => $propertyAnnotation, 'Setter' => $setter]);echo('</pre>');
		// if(!is_string($setter)) throw new Exception("Error ".__METHOD__."(): setter could not be find for ".json_encode(get_class($entity))." property ".json_encode($property)."!", 1);
		// return $setter;
	}

	/**
	 * Set current User
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function AttachUser(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		// $entityManager = $args->getObjectManager();
		// No relation
		// $relation = $this->container->get(serviceEntities::class)->getRelationType($property, $entity);
		// if(null !== $relation) throw new RuntimeException("Relation has no association: no Many/OneToMany/One attribute found.");
		if($entity instanceOf User) return $this;
		// $entity->defineOwner();
		$getter = $this->getGetter($property, $entity, $propertyAnnotation);
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$item = $entity->$getter();
		if(!($item instanceOf Item) || $propertyAnnotation->replace) {
			if(!$this->isDefaultContext()) {
				$user = $this->container->get(serviceUser::class)->getMainUserAdmin();
				if(empty($user)) $user = $this->container->get(serviceUser::class)->getDefaultEntrepriseOrCurrentUserOrRoot();
				// $user = $this->container->get(serviceUser::class)->getCurrentUserOrRoot();
				// die('<h3>TEST: is not default context!</h3>');
			} else {
				switch (strtolower($propertyAnnotation->context)) {
					case 'environment':
						$user = $this->container->get(serviceContext::class)->getEnvironment();
						break;
					case 'entreprise':
						$user = $this->container->get(serviceContext::class)->getEntreprise();
						break;
					case 'user':
						$user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
						break;
					default: // default
						$user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
						break;
				}
			}
			if($user instanceOf Item && $user !== $item) {
				// echo('<div class="text-white">'.__METHOD__.'(): Old owner > '.(null === $item ? 'NULL' : $item->getShortname().' '.json_encode($item->getName())).' for '.$entity->getShortname().' '.json_encode($entity->getNameOrTempname()).'</div>');
				// echo('<div class="text-white">'.__METHOD__.'(): > '.$setter.'('.$user->getShortname().' '.json_encode($user->getName()).') for '.$entity->getShortname().' '.json_encode($entity->getNameOrTempname()).'</div>');
				$entity->$setter($user);
				// echo('<div class="text-white">'.__METHOD__.'(): <u>Final owner</u> > '.(empty($entity->$getter()) ? 'NULL' : $entity->$getter()->getShortname().' '.json_encode($entity->$getter()->getName())).' for '.$entity->getShortname().' '.json_encode($entity->getNameOrTempname()).'</div>');
			} else {
				// echo('<div>'.__METHOD__.'(): Old owner > '.(null === $item ? 'NULL' : $item->getShortname().' '.json_encode($item->getName())).'</div>');
				// echo('<div>'.__METHOD__.'(): ??? owner > '.(null === $user ? 'NULL' : $user->getShortname().' '.json_encode($user->getName())).'</div>');
			}
			// else throw new Exception("Error: only connected user can manage this entity ".json_encode($entity->getShortname())."!", 1);
		}
		return $this;
	}

	/**
	 * Entity is/not admin validated
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function Adminvalidated(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		if(!$this->isDefaultContext()) return $this;
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		// if($this->container->isGranted('ROLE_ADMIN')) $entity->$setter(true);
		$user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
		if(!empty($user) && $this->container->get(serviceGrants::class)->isGranted('ROLE_ADMIN', $user)) {
			$entity->$setter(true);
		} else {
			$getter = $this->getGetter($property, $entity, $propertyAnnotation);
			$isAdminvalidated = $entity->$getter();
			$parameterBag = $this->container->getParameterBag();
			if(!$parameterBag->has('entities_parameters')) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): parameter ".json_encode('entities_parameters')." is missing in config.yml!", 1);
			$entities_parameters = $parameterBag->get('entities_parameters');
			if(isset($entities_parameters[$entity->getShortname()]) && isset($entities_parameters[$entity->getShortname()]['adminvalidation']) && $entities_parameters[$entity->getShortname()]['adminvalidation']) {
				$entity->$setter(false);
				// echo('<div>Entity '.$entity->getShortname().' is <span style="color: red;">NOT</span> admin validated.</div>');
			} else {
				$entity->$setter(true);
				// echo('<div>Entity '.$entity->getShortname().' is admin validated.</div>');
			}
		}
		return $this;
	}

	/**
	 * Set current User
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function AttachContexEnvironment(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		if($entity instanceOf User) return $this;
		$property = $propertyAnnotation->property;
		$getter = $this->getGetter($property, $entity, $propertyAnnotation);
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$environment = $this->container->get(serviceContext::class)->getEnvironment();
		if(empty($entity->$getter()) || $propertyAnnotation->replace) {
			if(!empty($environment)) $entity->$setter($environment);
		}
		return $this;
	}

	/**
	 * Set valid content-types
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function AttachValidContentTypes(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		// $getter = $this->getGetter($property, $entity, $propertyAnnotation);
		if(method_exists($entity, 'getAuthorizedTypes')) {
			$setter = $this->getSetter($property, $entity, $propertyAnnotation);
			$authTypes = $entity->getAuthorizedTypes();
			$contentTypes = $this->container->get(FileManager::class)->getContentTypesByMediaTypes($authTypes, true);
			$entity->$setter(implode(', ', $contentTypes));
			// echo('<pre>');var_dump($entity->$getter());echo('</pre>');
		}
		return $this;
	}

	/**
	 * Add Logg service in Logg and initialize
	 * @param BaseAnnotation $methodAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function InitializeLogg(BaseAnnotation $methodAnnotation, EventArgs $args, $eventType) {
		$logg = $methodAnnotation->entity;
		$method = $methodAnnotation->method;
		if(!($logg instanceOf Logg)) throw new Exception("Error ".__METHOD__."(): entity is not instance of Logg!", 1);
		$logg->$method($this->container->get(serviceLogg::class));
		return $this;
	}

	/**
	 * Add Bundles
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function injectBundles(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$bundles = $this->container->get(serviceKernel::class)->getTemlatableBundles();
		$entity->$setter($bundles);
		return $this;
	}

	/**
	 * Add Event classes
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function InjectEventClasses(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		// switch ($eventType) {
		// 	case BaseAnnotation::postNew:
		// 	case BaseAnnotation::preUpdateForm:
				$classes = $this->container->get(serviceEntities::class)->getEntitiesNames(true, Event::class);
				$eventClasses = [];
				foreach ($classes as $classname => $shortname) $eventClasses[$shortname] = $classname;
				$entity->$setter($eventClasses);
		// 		break;
		// 	case Events::prePersist:
		// 	case Events::preUpdate:
		// 		// 
		// 		break;
		// }
		return $this;
	}

	/**
	 * Inject Entities classnames list for Pagewebs
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function InjectItemtypesList(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		// $itemtypes = $this->container->get(serviceEntities::class)->getEntitiesNames(false, Item::class);
		$classes = [];
		foreach ($this->container->get(serviceEntities::class)->getEntitiesNames(false, Item::class) as $classname => $shortname) {
			$classes[serviceEntities::singlar($shortname)] = serviceEntities::singlar($classname);
			$classes[serviceEntities::plural($shortname)] = serviceEntities::plural($classname);
		}
		// echo('<pre><h3>Inject list of entites classnames</h3>');var_dump($classes);echo('</pre>');
		$entity->$setter($classes);
		return $this;
	}

	/**
	 * Add locale choices
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function InjectLocalesChoices(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$translatableLocalechoices = $this->container->get(serviceLanguage::class)->getLocaleschoices(false);
		$entity->$setter($translatableLocalechoices);
		return $this;
	}

	/**
	 * Inject choice of Pageweb for Item
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function InjectPagewebList(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$name = $entity->getClassname();
		$pagewebs = array_filter(
			$this->container->get(servicePageweb::class)->getRepository()->findAll(),
			function ($pageweb) use ($name) { return in_array($name, $pageweb->getItemtypes()); }
		);
		// echo('<pre><h3>Inject pageweb choices ('.$setter.')</h3>');var_dump(implode(' / ', $pagewebs));echo('</pre>');
		$entity->$setter($pagewebs);
		return $this;
	}

	/**
	 * Add domains choices
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function InjectDomainsChoices(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$domainchoices = $this->container->get(serviceTwgTranslation::class)->getDomainesChoices();
		$entity->$setter($domainchoices);
		return $this;
	}

	/**
	 * Add Roles
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function injectRoles(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$roles = $this->container->get(serviceRoles::class)->getRoles(true);
		// var_dump($roles);
		$entity->$setter($roles);
		return $this;
	}

	/**
	 * Add locale by request (in header, POST or GET)
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function injectTemplatestructures(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$templates = $this->container->get(servicePageweb::class)->getTemplateStructures($this->container->get(serviceUser::class)->getCurrentUserOrNull());
		$entity->$setter($templates);
		return $this;
	}

	/**
	 * Insert context date (default is now)
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function InsertContextdate(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$instances = $entity->getInstances();
		$valids = array_intersect($instances, ['Event','Baseprod','Partenaire','File','Basedirectory','Binary','Eventdate','Blog']);
		if(!count($valids)) {
			$date = new DateTime(serviceContext::DEFAULT_DATE);
			$entity->$setter($date);
		} else {
			// $asObjectOrFormat = $propertyAnnotation->type === 'object';
			// $entity->$setter($this->container->get(serviceContext::class)->getDate($asObjectOrFormat));
			// 
			// // $serviceContext = $this->container->get(serviceContext::class);
			// $asObjectOrFormat = $propertyAnnotation->type === 'object';
			// // $date = $serviceContext->isInitialized() ? $serviceContext->getDate($asObjectOrFormat) : new DateTime(serviceContext::DEFAULT_DATE);
			// $this->container->get(serviceContext::class)->whenInitialized(
			// 	$entity,
			// 	function(serviceContext $serviceContext) use ($entity, $setter, $asObjectOrFormat) {
			// 		$entity->$setter($serviceContext->getDate($asObjectOrFormat));
			// 	}
			// );
			// $entity->$setter($date);
		}
		return $this;
	}

	/**
	 * Insert Logg User
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function InsertLoggUser(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$logg = $propertyAnnotation->entity;
		if(!($logg instanceOf Logg)) throw new Exception("Error ".__METHOD__."(): entity is not instance of Logg!", 1);
		$userid = $logg->getUserid();
		if(!empty($userid)) {
			$user = $this->container->get(serviceUser::class)->getRepository()->find($userid);
			if(!empty($user)) $logg->setUser($user);
		}
		return $this;
	}

	/**
	 * Joined Directorys
	 * @param JoinedOwnDirectory $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function JoinedOwnDirectory(JoinedOwnDirectory $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		// $entity->initItem($this->container->get(serviceBasedirectory::class));
		$property = $propertyAnnotation->property;
		$getter = $this->getGetter($property, $entity, $propertyAnnotation);
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		// $origin_directory = $entity->$getter();
		switch ($eventType) {
			case BaseAnnotation::postNew:
				$this->container->get(serviceBasedirectory::class)->attachJoinedOwnDirectoryByAnnotation($propertyAnnotation, $getter, $setter, false, 'replace');
				break;
			case BaseAnnotation::checkItem:
				$this->container->get(serviceBasedirectory::class)->attachJoinedOwnDirectoryByAnnotation($propertyAnnotation, $getter, $setter, true, 'mix');
				break;
		}
		return $this;
	}

	/**
	 * Execute method after creating entity
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function JoinSections(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$getter = $this->getGetter($property, $entity, $propertyAnnotation);
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$tier = $this->container->get(serviceContext::class)->getEnvironment();
		$elements = $entity->$getter();
		if($elements->isEmpty()) {
			// $sections = $this->container->get(serviceTwig::class)->getRepository()->findDefaultjoinsByTier($tier);
			$sections = $this->container->get(serviceTwig::class)->getRepository()->findDefaultjoins();
			// echo('<pre>');
			// echo('<p>Environment: '.$tier.' / '.$setter.'(Sections) > found '.count($sections).' :</p>');
			// echo('<ul><li>'.implode('</li><li>', $sections).'</li></ul>');
			// echo('</pre>');
			if(preg_match('#^add#i', $setter)) {
				foreach ($sections as $section) $entity->$setter($section);
			} else {
				$entity->$setter(new ArrayCollection($sections));
			}
		} else {
			// echo('<pre>');
			// echo('<p>Environment: '.$tier.' / '.$setter.'(Sections) > found '.count($elements).' :</p>');
			// echo('<ul><li>'.implode('</li><li>', $elements->toArray()).'</li></ul>');
			// echo('</pre>');
		}
		return $this;
	}

	/**
	 * Execute method after creating entity
	 * @param BaseAnnotation $methodAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function PostCreate(BaseAnnotation $methodAnnotation, EventArgs $args, $eventType) {
		$entity = $methodAnnotation->entity;
		$method = $methodAnnotation->method;
		$result = $entity->$method($args);
		return $this;
	}

	/**
	 * insert prefered Pageweb
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function preferedPageweb(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		// $getter = $this->getGetter($property, $entity, $propertyAnnotation);
		$pageweb = $this->container->get(servicePageweb::class)->getPreferedPageweb();
		if(!empty($pageweb)) $entity->$setter($pageweb);
		return $this;
	}


	/**
	 * Import array of data (by class) from config.yml's parameters (data = "preferences") as preferences of entity
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function Preferences(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		if($this->isDefaultContext()) $entity->eventAction($eventType, $args, $this->container->get(servicePreference::class));
		return $this;
	}


	/**
	 * Transform BDDID to Entity / Entity to BDDID
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function Bddid(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$getter = $this->getGetter($property, $entity, $propertyAnnotation);
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$serviceEntities = $this->container->get(serviceEntities::class);
		switch ($eventType) {
			// case BaseAnnotation::postNew:
			// 	break;
			case BaseAnnotation::preCreateForm:
				// $serviceTranslation->translateEntity($entity);
				$item = $entity->$getter();
				if(is_object($item)) $entity->$setter($item->getBddid());
				break;
			case BaseAnnotation::preUpdateForm:
				// $serviceTranslation->translateEntity($entity);
				$item = $entity->$getter();
				if(is_object($item)) $entity->$setter($item->getBddid());
				break;
			// case Events::postLoad:
			// 	break;
			case Events::prePersist:
				if($serviceEntities->isValidBddid($item)) {
					$target = $serviceEntities->findByBddid($item);
					if(empty($target)) $entity->$setter($target);
						else throw new Exception("Error ".__METHOD__."(): could not find Entity with BDDID ".json_encode($item)."!", 1);
				}
				break;
			case Events::preUpdate:
				if($serviceEntities->isValidBddid($item)) {
					$target = $serviceEntities->findByBddid($item);
					if(empty($target)) $entity->$setter($target);
						else throw new Exception("Error ".__METHOD__."(): could not find Entity with BDDID ".json_encode($item)."!", 1);
				}
				break;
			// case Events::postPersist:
			// 	break;
			// case Events::postUpdate:
			// 	break;
		}
		return $this;
	}

	/**
	 * Captcha (Tom the Captcha) for forms
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function Tomcat(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		if($entity instanceOf User && empty($entity->getId())) {
			$setter = $this->getSetter($propertyAnnotation->property, $entity, $propertyAnnotation);
			$entity->$setter(true);
		}
		// $getter = $this->getGetter($propertyAnnotation->property, $entity, $propertyAnnotation);
		// echo('<h3>Setted TOMCAT to '.json_encode($entity->$getter()).'</h3>');
		return $this;
	}

	/**
	 * Choose default Pageweb for entity
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function ChooseInitPageweb(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$getter = $this->getGetter($property, $entity, $propertyAnnotation);
		if(!($entity->$getter() instanceOf Pageweb)) {
			$pageweb = $this->container->get(servicePageweb::class)->getRepository()->findOnePageForItem($entity);
			if($pageweb instanceOf Pageweb) {
				// die('Found Pageweb: '.$pageweb->getName());
				$setter = $this->getSetter($property, $entity, $propertyAnnotation);
				$entity->$setter($pageweb);
			}
		}
		return $this;
	}

	/**
	 * Define Timezone
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function Timezone(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$getter = $this->getGetter($property, $entity, $propertyAnnotation);
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$serviceTimezone = $this->container->get(serviceTimezone::class);
		$timezone = $entity->$getter();
		if(!$serviceTimezone->isValidTimezone($timezone)) {
			$timezone = $propertyAnnotation->default;
			if(empty($timezone)) $timezone = $serviceTimezone->getDefaultTimezone();
			if(!$serviceTimezone->isValidTimezone($timezone)) throw new Exception("Error ".__METHOD__."(): timezone ".json_encode($timezone)." is not valid!", 1);
			$entity->$setter($timezone);
		}
		return $this;
	}

	/**
	 * Manage Entities with Typitem(s)
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function Typitem(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		// $getter = $this->getGetter($property, $entity, $propertyAnnotation);
		$data = [];
		foreach ($this->container->get(serviceTypitem::class)->getRepository()->findByGroupitem($propertyAnnotation->groups) as $typitem) $data[$typitem->getName()] = $typitem->getId();
		$setter = $this->getSetter($property.'Choices', $entity, $propertyAnnotation);
		$entity->$setter($data);
		return $this;
	}

	/**
	 * manage translation fields of entity
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function HasTranslatable(BaseAnnotation $classAnnotation, EventArgs $args, $eventType) {
		$entity = $classAnnotation->entity;
		// $property = $classAnnotation->property;
		$serviceTranslation = $this->container->get(serviceTranslation::class);
		if(!$serviceTranslation->isTranslatator()) return $this;
		switch ($eventType) {
			case BaseAnnotation::postNew:
				$serviceTranslation->translateEntity($entity);
				// if($serviceTranslation->translateEntity($entity)) echo('<p>translationManager methods added: '.$entity->translationManager->getCurrentLocale().'</p>');
				// if($entity instanceOf Entreprise) DevTerminal::Info("     *** Event: ".$eventType);
				break;
			case BaseAnnotation::preCreateForm:
				// $serviceTranslation->translateEntity($entity);
				if(isset($entity->_tm)) $entity->_tm->preCreateForm($args);
				break;
			case BaseAnnotation::preUpdateForm:
				// $serviceTranslation->translateEntity($entity);
				if(isset($entity->_tm)) $entity->_tm->preUpdateForm($args);
				break;
			case Events::postLoad:
				$serviceTranslation->translateEntity($entity);
				// if($serviceTranslation->translateEntity($entity)) echo('<p>translationManager methods added: '.$entity->translationManager->getCurrentLocale().'</p>');
				// if($entity instanceOf Entreprise) DevTerminal::Info("     *** Event: ".$eventType);
				break;
			// case Events::prePersist:
			// 	if(isset($entity->_tm)) $entity->_tm->prePersistActions($args);
			// 	break;
			// case Events::preUpdate:
			// 	if(isset($entity->_tm)) $entity->_tm->preUpdateActions($args);
			// 	break;
			// case Events::postPersist:
			// 	if(isset($entity->_tm)) $entity->_tm->postPersistActions($args);
			// 	break;
			// case Events::postUpdate:
			// 	if(isset($entity->_tm)) $entity->_tm->postUpdateActions($args);
			// 	break;
		}
		return $this;
	}

	/**
	 * Verify if is unique value regarding to other fields (associations or columns)
	 * @param BaseAnnotation $propertyAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function UniqueField(BaseAnnotation $propertyAnnotation, EventArgs $args, $eventType) {
		$entity = $propertyAnnotation->entity;
		$property = $propertyAnnotation->property;
		$getter = $this->getGetter($property, $entity, $propertyAnnotation);
		$setter = $this->getSetter($property, $entity, $propertyAnnotation);
		$entityManager = $args->getEntityManager();
		// $entityManager = $this->container->get(serviceEntities::class)->getEntityManager();
		$reflProp = new ReflectionProperty($entity, $property);
		$annot_column = $this->reader->getPropertyAnnotation($reflProp, serviceEntities::DOCTRINE_MAPPING.serviceEntities::DOCTRINE_COLUMN);
		if(null != $annot_column) {
			// ORM\Column
			// $serviceEntity = $this->container->get(serviceBasentity::class)->getEntityService($entity);
			// $repo = $serviceEntity->getRepository(get_class($entity));
			// echo('      - REPO: '.get_class($repo).' \n');
			$value = $propertyAnnotation->value;
			switch (gettype($value)) {
				case 'string':
					$value = "'".$value."'";
					break;
				case 'boolean':
					$value = $value ? 1 : 0;
					break;
			}
			$groupedFields = array();
			if(count($propertyAnnotation->groupedByFields) > 0) {
				// Grouped by fields
				foreach ($propertyAnnotation->groupedByFields as $field) {
					$subGetter = serviceClasses::getPropertyMethod($field, $entity, 'get', 'singularized');
					// $subGetter = $this->container->get(serviceEntities::class)->getGetterMethodName($field, $entity);
					$subvalue = $entity->$subGetter();
					if(gettype($subvalue) == 'string') $subvalue = "'".$subvalue."'";
					if(gettype($subvalue) == 'boolean') $subvalue = $subvalue ? 1 : 0;
					$groupedFields[] = " AND element.".$field." = ".$subvalue;
				}
			}
			$ID = $entity->getId() !== null ? "element.id != ".$entity->getId()." AND " : "";
			$countQuery = "SELECT COUNT(element.id) FROM ".get_class($entity)." element WHERE ".$ID."element.".$property." = ".$value.implode(" ", $groupedFields);
			$searchQuery = "SELECT element FROM ".get_class($entity)." element WHERE ".$ID."element.".$property." = ".$value.implode(" ", $groupedFields);
			$count = $entityManager->createQuery($countQuery)->getSingleScalarResult();
			// if(!$this->isDefaultContext()) DevTerminal::Warning("      --> Query unique field count (".$count."): ".$countQuery);
			if($count >= $propertyAnnotation->max && $propertyAnnotation->value === $entity->$getter()) {
				$user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
				$user = $user instanceOf User ? [$user->getUsername(), $user->getId(), $user->getRoles()] : null;
				// YamlLog::registerYamlLogfile(__METHOD__, false, ['Entity_bddid' => $entity->getBddid(), 'Entity_value' => $entity->$getter(), 'Entity_property' => $property, 'Methods' => [$getter, $setter], 'Reason' => 'Max value reached!', 'Annotation_values' => json_encode($propertyAnnotation), 'Count' => $count, 'User' => $user]);
				switch (strtolower($propertyAnnotation->onError)) {
					case 'exception':
						if($args instanceOf PreUpdateEventArgs) {
							// on update, retrieve original value
							$origin = $args->getEntityChangeSet();
							if(isset($origin[$property][0]) && $origin[$property][0] !== $propertyAnnotation->value) $entity->$setter($origin[$property][0]);
								else $entity->$setter($propertyAnnotation->altValue);
						} else {
							$entity->$setter($propertyAnnotation->altValue);
							$entityManager->flush();
						}
						$others = $entityManager->createQuery($searchQuery)->getResult();
						throw new Exception("Max entities (".$propertyAnnotation->max.") ".get_class($entity)."::".$property." with value ".json_encode($propertyAnnotation->value)." has been reached: total should be ".(count($others)+1)." now!", 1);
						break;
					case 'cancel':
						if($args instanceOf PreUpdateEventArgs) {
							// on update, retrieve original value
							$origin = $args->getEntityChangeSet();
							if(isset($origin[$property][0]) && $origin[$property][0] !== $propertyAnnotation->value) $entity->$setter($origin[$property][0]);
								else $entity->$setter($propertyAnnotation->altValue);
						} else {
							$entity->$setter($propertyAnnotation->altValue);
							$entityManager->flush();
						}
						break;
					default: // force
						$others = $entityManager->createQuery($searchQuery)->getResult();
						foreach ($others as $other) $other->$setter($propertyAnnotation->altValue);
						if(count($others)) $entityManager->flush();
						break;
				}
			}
			if($count < $propertyAnnotation->min && $propertyAnnotation->value != $entity->$getter()) {
				$user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
				$user = $user instanceOf User ? [$user->getUsername(), $user->getId(), $user->getRoles()] : null;
				// YamlLog::registerYamlLogfile(__METHOD__, false, ['Entity_bddid' => $entity->getBddid(), 'Entity_value' => $entity->$getter(), 'Entity_property' => $property, 'Methods' => [$getter, $setter], 'Reason' => 'Min value reached!', 'Annotation_values' => json_encode($propertyAnnotation), 'Count' => $count, 'User' => $user]);
				switch (strtolower($propertyAnnotation->onError)) {
					case 'exception':
						if($args instanceOf PreUpdateEventArgs) {
							// on update, retrieve original value
							$origin = $args->getEntityChangeSet();
							if(isset($origin[$property][0]) && $origin[$property][0] !== $propertyAnnotation->value) $entity->$setter($origin[$property][0]);
								else $entity->$setter($propertyAnnotation->altValue);
						} else {
							$entity->$setter($propertyAnnotation->altValue);
							$entityManager->flush();
						}
						$others = $entityManager->createQuery($searchQuery)->getResult();
						throw new Exception("Min entities (".$propertyAnnotation->min.") ".get_class($entity)."::".$property." with value ".json_encode($propertyAnnotation->value)." has been reached: total should be ".count($others)." now!", 1);
						break;
					case 'cancel':
						if($args instanceOf PreUpdateEventArgs) {
							// on update, retrieve original value
							$origin = $args->getEntityChangeSet();
							if(isset($origin[$property][0]) && $origin[$property][0] !== $propertyAnnotation->value) $entity->$setter($origin[$property][0]);
								else $entity->$setter($propertyAnnotation->altValue);
						} else {
							$entity->$setter($propertyAnnotation->altValue);
							$entityManager->flush();
						}
						break;
					default: // force
						$others = $entityManager->createQuery($searchQuery)->getResult();
						foreach ($others as $other) $other->$setter($propertyAnnotation->altValue);
						if(count($others)) $entityManager->flush();
						break;
				}
			}
		} else {
			// ORM\Association
			// ...
			// No relation
			// $relation = $this->container->get(serviceEntities::class)->getRelationType($property, $entity);
			// if(null !== $relation) throw new RuntimeException("Relation has no association: no Many/OneToMany/One attribute found.");
		}
		return $this;
	}

	/**
	 * Update date (not in "check" context)
	 * @param BaseAnnotation $methodAnnotation
	 * @param EventArgs $args
	 * @param string $eventType
	 * @return serviceAnnotation
	 */
	public function UpdateDate(BaseAnnotation $methodAnnotation, EventArgs $args, $eventType) {
		$entity = $methodAnnotation->entity;
		$method = $methodAnnotation->method;
		if(!$this->isCheckContext()) $entity->$method();
		return $this;
	}




}