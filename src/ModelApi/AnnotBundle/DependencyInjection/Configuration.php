<?php

namespace ModelApi\AnnotBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
	const MAX_REFLUSH = 5;
	const MIN_MAX_REFLUSH = 1;
	const MAX_MAX_REFLUSH = 1000;

	/**
	 * {@inheritdoc}
	 */
	public function getConfigTreeBuilder()
	{
		$treeBuilder = new TreeBuilder();
		$rootNode = $treeBuilder->root('model_api_annot');

		// Here you should define the parameters that are allowed to
		// configure your bundle. See the documentation linked above for
		// more information on that topic.

		$rootNode
			->append($this->addOptionsNode())
			->append($this->addPostflushNode())
		;

		return $treeBuilder;
	}


	public function addOptionsNode() {
		$treeBuilder = new TreeBuilder();
		$node = $treeBuilder->root('activity');
		return $node
			->children()
				->booleanNode('active')
				->defaultTrue()
				->info('Enable/Disable annotations provider.')
				->end()
			->end()
			->children()
				->booleanNode('master_request_only')
				->defaultTrue()
				->info('If true, works only on master requests.')
				->end()
			->end()
		;
	}

	public function addPostflushNode() {
		$treeBuilder = new TreeBuilder();
		$node = $treeBuilder->root('postflush');
		return $node
			->children()
				->integerNode('max_flushes')
				->min(static::MIN_MAX_REFLUSH)
				->max(static::MAX_MAX_REFLUSH)
				->defaultValue(static::MAX_REFLUSH)
				->info('Set then max. number of reFlush (default to '.static::MAX_REFLUSH.') to prevent any infinite flushes.')
				->end()
			->end()
			->children()
				->booleanNode('max_raised_exception')
				->defaultTrue()
				->info('If true, got an Exception while raising max. number of flushes (max. default set to '.static::MAX_REFLUSH.').')
				->end()
			->end()
		;
	}

}
