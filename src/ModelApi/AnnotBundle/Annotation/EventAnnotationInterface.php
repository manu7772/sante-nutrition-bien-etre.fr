<?php
namespace ModelApi\AnnotBundle\Annotation;

use ModelApi\AnnotBundle\Annotation\AnnotAnnotationInterface;

interface EventAnnotationInterface extends AnnotAnnotationInterface {


	/**
	 * Root Class running Events
	 */
	public static function getSubscribedEvents();

	public static function hasTypeEvent($typeEvent);


}
