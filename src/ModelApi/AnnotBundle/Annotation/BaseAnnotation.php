<?php
namespace ModelApi\AnnotBundle\Annotation;

// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;
use ModelApi\AnnotBundle\Annotation\EventAnnotationInterface;
use ModelApi\AnnotBundle\Annotation\EntityAnnotationInterface;

use Doctrine\ORM\Events;
use \ReflectionClass;
use \BadMethodCallException;
// use \InvalidArgumentException;
// use \RuntimeException;

abstract class BaseAnnotation implements EventAnnotationInterface, EntityAnnotationInterface {

	const postNew = 'postNew';
	const checkItem = 'checkItem';
	const preCreateForm = 'preCreateForm';
	const preUpdateForm = 'preUpdateForm';

	public $entity = null;
	public $property = null;
	public $method = null;

	/**
	 * Constructor.
	 *
	 * @param array $options Key-value for properties to be defined in this class.
	 */
	public function __construct(array $options = []) {
		foreach ($options as $key => $value) $this->$key = $value;
	}

	/**
	 * Error handler for unknown property accessor in Annotation class.
	 *
	 * @param string $name Unknown property name.
	 *
	 * @throws \BadMethodCallException
	 */
	public function __get($name) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, get_class($this)));
	}

	/**
	 * Error handler for unknown property mutator in Annotation class.
	 *
	 * @param string $name  Unknown property name.
	 * @param mixed  $value Property value.
	 *
	 * @throws \BadMethodCallException
	 */
	public function __set($name, $value) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, get_class($this)));
	}

	/**
	 * Get shortname
	 * @return string
	 */
	public function __toString() {
		$class = new ReflectionClass(static::class);
		return $class->getShortName();
	}

	/**
	 * Root Class running Events
	 */
	public static function getSubscribedEvents() {
		return array(
			// static::postNew,
			// static::checkItem,
			// static::preCreateForm,
			// static::preUpdateForm,
			// Events::postLoad,
			// Events::prePersist,
			// Events::postPersist,
			// Events::preUpdate,
			// Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			// Events::onFlush,
			// Events::postFlush,
			// Events::onClear,
		);
	}

	public function getEnvContexts() {
		return serviceAnnotation::getValidContexts();
	}

	public function isEnvContexts($contexts = []) {
		if(empty($contexts)) return true;
		if(is_string($contexts)) $contexts = [$contexts];
		$envContexts = $this->getEnvContexts();
		foreach ($contexts as $context) {
			if(in_array($context, $envContexts)) return true;
		}
		return false;
	}

	public function toArray() {
		$data = [];
		$RC = new ReflectionClass($this);
		foreach ($RC->getProperties(ReflectionProperty::IS_PUBLIC) as $prop) {
			$name = $prop->getName();
			$data[$name] = $this->$name;
		}
		return $data;
	}

	public static function hasTypeEvent($typeEvent) {
		$typeEvents = static::getSubscribedEvents();
		return in_array($typeEvent, $typeEvents);
	}

	public function getAddedAttributes() {
		return [
			// 'entity',
			'property',
			'method',
		];
	}

	public function setEntity(CrudInterface $entity, $attributes = []) {
		$this->entity = $entity;
		$addattr = $this->getAddedAttributes();
		foreach ($attributes as $name => $value) if(in_array($name, $addattr)) {
			$this->$name = $value;
		}
		return $this;
	}

}
