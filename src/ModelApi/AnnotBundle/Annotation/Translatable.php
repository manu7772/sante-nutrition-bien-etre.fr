<?php
namespace ModelApi\AnnotBundle\Annotation;

use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
use Doctrine\ORM\Events;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
// use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// serviceTools
use ModelApi\BaseBundle\Service\serviceTools;

use ModelApi\InternationalBundle\Entity\TwgTranslation;

/**
 * @Annotation
 * @Target("PROPERTY")
 * 
 * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class Translatable extends BaseAnnotation {

	/**
	 * Constructor.
	 *
	 * @param array $options Key-value for properties to be defined in this class.
	 */
	// public function __construct(array $options = []) {
	// 	// if(!array_key_exists('striptags', $options)) $options['striptags'] = 'auto';
	// 	if(array_key_exists('striptags', $options) && !is_bool($options['striptags'])) {
	// 		switch ($options['type']) {
	// 			case 'html': $options['striptags'] = false; break;
	// 			default: $options['striptags'] = true; break;
	// 		}
	// 	}
	// 	parent::__construct($options);
	// }

	/**
	 * @var string
	 */
	public $getter = null;

	/**
	 * @var string
	 */
	public $setter = null;

	/**
	 * @var string
	 */
	public $domain = 'auto';

	/**
	 * @var string
	 * @Enum({"auto", TwgTranslation::TYPE_TEXT, TwgTranslation::TYPE_TEXTAREA, TwgTranslation::TYPE_HTML, TwgTranslation::TYPE_NUMERIC, TwgTranslation::TYPE_DATETIME} )
	 */
	public $type = 'auto';

	/**
	 * @var mixed
	 */
	public $striptags = 'auto';

	/**
	 * Class running Events
	 */
	public static function getSubscribedEvents() {
		return array(
			static::postNew,
			// static::checkItem,
			static::preCreateForm,
			static::preUpdateForm,
			Events::postLoad,
			Events::prePersist,
			Events::postPersist,
			Events::preUpdate,
			Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			// Events::onFlush,
			// Events::postFlush,
			// Events::onClear,
		);
	}

	public function getEnvContexts() {
		return ['default'];
	}

	public function addModelTransformer($propertyName, FormBuilderInterface $formBuilder) {
		$striptags = in_array(strtolower($this->type), [TwgTranslation::TYPE_TEXT, TwgTranslation::TYPE_TEXTAREA, TwgTranslation::TYPE_NUMERIC, TwgTranslation::TYPE_DATETIME]);
		if(strtolower($this->striptags) !== 'auto') {
			if(!is_bool($this->striptags)) throw new Exception("Error ".__METHOD__."(): striptags parameter must be boolean or ".json_encode("auto").". Got ".json_encode($this->striptags)."!", 1);
			$striptags = $this->striptags;
		}
		if($striptags) {
			$formBuilder->get($propertyName)->addModelTransformer(
				new CallbackTransformer(
					function ($data) {
						return is_string($data) ? serviceTools::striptags($data) : $data;
					},
					function ($data) {
						return is_string($data) ? serviceTools::striptags($data) : $data;
					}
				)
			);
		} else {
			$formBuilder->get($propertyName)->addModelTransformer(
				new CallbackTransformer(
					function ($data) {
						return is_string($data) ? preg_replace('/^(<p>)(.*)(<br>)*(<\\/p>)$/i', '$2', $data) : $data;
					},
					function ($data) {
						return is_string($data) ? preg_replace('/^(<p>)(.*)(<br>)*(<\\/p>)$/i', '$2', $data) : $data;
					}
				)
			);			
		}
		return $this;
	}


}
