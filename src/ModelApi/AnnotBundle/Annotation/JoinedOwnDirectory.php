<?php
namespace ModelApi\AnnotBundle\Annotation;

use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
use Doctrine\ORM\Events;

use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\FileBundle\Service\serviceBasedirectory;

/**
 * @Annotation
 * @Target("PROPERTY")
 * 
 * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class JoinedOwnDirectory extends BaseAnnotation {

	/**
	 * @var string
	 */
	public $getter = null;

	/**
	 * @var string
	 */
	public $setter = null;

	/**
	 * @var string
	 */
	public $altgetter = null;

	/**
	 * @var string
	 */
	public $altsetter = null;

	/**
	 * @var string
	 */
	public $path = null;


	// public $entity = null;
	// public $property = null;
	// public $method = null;
	public $altproperty = null;
	public $ownPath = null;

	/**
	 * Constructor.
	 *
	 * @param array $options Key-value for properties to be defined in this class.
	 */
	public function __construct(array $options = []) {
		parent::__construct($options);
		$this->altproperty = lcfirst(preg_replace('/^get/i', '', $this->altgetter));
	}

	/**
	 * Class running Events
	 */
	public static function getSubscribedEvents() {
		return array(
			static::postNew,
			static::checkItem,
			// static::preCreateForm,
			// static::preUpdateForm,
			// Events::postLoad,
			// Events::prePersist,
			// Events::postPersist,
			// Events::preUpdate,
			// Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			// Events::onFlush,
			// Events::postFlush,
			// Events::onClear,
		);
	}

	public function setEntity(CrudInterface $entity, $attributes = []) {
		parent::setEntity($entity, $attributes);
		// set own path
		$name = serviceBasedirectory::ROOT_NAME_WITH_OWNERNAME ? '_'.$entity->getName() : '';
		$this->ownPath = preg_replace('/^@?(drive|system)(.*)$/', '@$1'.$name.'$2', $this->path);
		return $this;
	}

	public function getHierarchizePath() {
		$directorys = array_reverse(preg_split('/\\'.DIRECTORY_SEPARATOR.'+/', $this->path));
		// $path = ['_values' => serviceBasedirectory::DEFAULT_TYPEDIR];
		$path = ['_values' => null];
		foreach ($directorys as $name) {
			$old_path = $path;
			$path = [];
			$path[$name] = $old_path;
		}
		return $path;
	}

}
