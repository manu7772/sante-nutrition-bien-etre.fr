<?php
namespace ModelApi\AnnotBundle\Annotation;

use ModelApi\CrudsBundle\Entity\CrudInterface;

interface EntityAnnotationInterface {

	public function getAddedAttributes();

	public function getEnvContexts();

	public function isEnvContexts($contexts = []);

	public function setEntity(CrudInterface $entity, $attributes = []);

}
