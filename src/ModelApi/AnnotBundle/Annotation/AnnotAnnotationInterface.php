<?php
namespace ModelApi\AnnotBundle\Annotation;


interface AnnotAnnotationInterface {


	public function toArray();

	public function __toString();


}
