<?php
namespace ModelApi\AnnotBundle\Annotation;

// use Symfony\Component\DependencyInjection\ContainerInterface;
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
use Doctrine\ORM\Events;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\PersistentCollection;
use Doctrine\Common\Collections\ArrayCollection;
// use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// serviceTools
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceEntities;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use \Exception;

/**
 * @Annotation
 * @Target("PROPERTY")
 * 
 * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class AddModelTransformer extends BaseAnnotation {

	// /**
	//  * Constructor.
	//  *
	//  * @param array $options Key-value for properties to be defined in this class.
	//  */
	// public function __construct(array $options = []) {
	// 	// $options['method'] ??= 
	// 	parent::__construct($options);
	// }

	/**
	 * @var string
	 */
	public $method = null;


	public $serviceEntities = null;
	public $entity = null;

	/**
	 * Class running Events
	 */
	public static function getSubscribedEvents() {
		return array(
			// static::postNew,
			// static::checkItem,
			// static::preCreateForm,
			// static::preUpdateForm,
			// Events::postLoad,
			// Events::prePersist,
			// Events::postPersist,
			// Events::preUpdate,
			// Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			// Events::onFlush,
			// Events::postFlush,
			// Events::onClear,
		);
	}

	public function getSelfModelTransformer($name) {
		$found = false;
		if(preg_match('/^annot::/i', $name)) {
			$method = '_self_'.preg_replace('/^annot::/i', '', $name);
			$found = method_exists($this, $method) ? $method : false;
		}
		return $found;
	}

	public function addModelTransformer($propertyName, FormBuilderInterface $formBuilder, serviceEntities $serviceEntities, $entity = null) {
		$this->entity = $serviceEntities->entityExists($entity) ? $entity : $formBuilder->getData();
		if($serviceEntities->entityExists($this->entity)) {
			if(!$serviceEntities->entityExists($this->entity)) throw new Exception("Error ".__METHOD__."(): could not find entity in FormBuilder!", 1);
			$this->serviceEntities = $serviceEntities;
			$this->method ??= $propertyName.'AddModelTransformer';
			if($method = $this->getSelfModelTransformer($this->method)) {
				// USE SELF METHOD
				$this->$method($propertyName, $formBuilder);
			} else if(method_exists($this->entity, $this->method)) {
				// METHOD IN ENTITY
				$method = $this->method;
				$this->entity->$method($propertyName, $formBuilder);
			} else {
				throw new Exception("Error ".__METHOD__."(): method ".json_encode($this->method)." for addModelTransformer does not exist!", 1);
			}
		} else {
			throw new Exception("Error ".__METHOD__."(): not entity found for addModelTransformer!", 1);
		}
		return $this;
	}

	// Symfony\Component\Form\FormBuilder
	// Symfony\Component\Form\Form

	// SELF TRANSFORMERS

	/**
	 * method = transformEntitiesAsChoices
	 */
	public function _self_transformEntitiesAsChoices($propertyName, FormBuilderInterface $formBuilder) {
		$formBuilder->get($propertyName)->addModelTransformer(
			new CallbackTransformer(
				function ($data) {
					if(empty($data)) return $data;
					// if($data instanceOf PersistentCollection || $data instanceOf ArrayCollection) $data = $data->toArray();
					if(is_object($data) && is_iterable($data)) $data = $data->toArray();
					return $data = is_array($data) ?
						array_map(function($value) { return is_object($value) ? $value->getId() : $value; }, $data) :
						$data->getId();
				},
				function ($data) {
					if(empty($data)) return $data;
					// if($data instanceOf PersistentCollection || $data instanceOf ArrayCollection) $data = $data->toArray();
					if(is_object($data) && is_iterable($data)) $data = $data->toArray();
					$values = $this->serviceEntities->getRepository('Typitem')->findById($data);
					return is_array($data) ? $values : reset($values);
				}
			)
		);
	}


}
