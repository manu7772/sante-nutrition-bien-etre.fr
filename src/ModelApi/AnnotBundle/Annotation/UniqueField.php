<?php
namespace ModelApi\AnnotBundle\Annotation;

use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
use Doctrine\ORM\Events;

/**
 * @Annotation
 * @Target("PROPERTY")
 * 
 * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class UniqueField extends BaseAnnotation {

	// Champ unique obligatoire
	// fields = Array <string>
	// 		- column: name
	// 		- association: name[.field]

	// /**
	//  * @var Array
	//  */
	// public $fields = array();

	/**
	 * @var mixed
	 */
	public $value;

	/**
	 * @var mixed
	 */
	public $altValue;

	/**
	 * @var Array
	 */
	public $groupedByFields = array();

	/**
	 * @var string
	 * values = 'force','cancel','exception'
	 */
	public $onError = 'cancel';

	/**
	 * @var integer
	 * values >= 0
	 */
	public $max = 1;

	/**
	 * @var integer
	 * values >= 0
	 */
	public $min = 1;


	/**
	 * Class running Events
	 */
	public static function getSubscribedEvents() {
		return array(
			// static::postNew,
			// static::checkItem,
			// static::preCreateForm,
			// static::preUpdateForm,
			// Events::postLoad,
			// Events::prePersist,
			Events::postPersist,
			// Events::preUpdate,
			Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			// Events::onFlush,
			// Events::postFlush,
			// Events::onClear,
		);
	}


}
