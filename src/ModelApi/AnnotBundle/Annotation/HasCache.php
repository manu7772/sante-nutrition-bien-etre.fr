<?php
// namespace ModelApi\AnnotBundle\Annotation;

// use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
// use Doctrine\ORM\Events;
// use JMS\Serializer\Serializer;
// use JMS\Serializer\SerializationContext;

// use ModelApi\BaseBundle\Service\serviceCache;
// // use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// use ModelApi\AnnotBundle\Annotation\CacheData;
// // use ModelApi\BaseBundle\Service\serviceTools;
// use ModelApi\BaseBundle\Service\serviceClasses;

// use \Exception;

// /**
//  * @Annotation
//  * @Target("CLASS")
//  * 
//  * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
//  * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
//  * @see https://www.doctrine-project.org/api/annotations/1.6/Doctrine/Common/Annotations/AnnotationReader.html
//  */
// class HasCache extends BaseAnnotation {

// 	/**
// 	 * @var string
// 	 * @Enum({"systemFile","attribute"})
// 	 */
// 	public $default_method = 'systemFile';

// 	/**
// 	 * @var integer
// 	 */
// 	public $levels = 3;

// 	/**
// 	 * @var integer
// 	 */
// 	public $lifetime = serviceCache::CACHE_LIFETIME;

// 	/**
// 	 * @var mixed
// 	 */
// 	public $apply_condition = null;

// 	/**
// 	 * @var array
// 	 */
// 	public $serialization_groups = ['cached_item'];


// 	protected $entity = null;
// 	protected $doApply = false;
// 	protected $serviceCache = null;
// 	// protected $jms_serializer = null;
// 	protected $CacheDataAnnotations = [];


// 	/**
// 	 * Constructor
// 	 * @param array $options Key-value for properties to be defined in this class.
// 	 */
// 	public final function __construct(array $options = []) {
// 		foreach ($options as $key => $value) {
// 			switch ($key) {
// 				case 'apply_condition':
// 					if(null === $value) $value = true;
// 					$this->$key = is_string($value) ? preg_replace('/[\\(\\)]/', '', $value) : (boolean)$value;
// 					break;
// 				default: $this->$key = $value; break;
// 			}
// 		}
// 		$this->computeProperties();
// 	}

// 	/**
// 	 * Class running Events
// 	 */
// 	public static function getSubscribedEvents() {
// 		return array(
// 			static::postNew,
// 			// static::checkItem,
// 			// static::preCreateForm,
// 			// static::preUpdateForm,
// 			Events::postLoad,
// 			Events::prePersist,
// 			Events::postPersist,
// 			Events::preUpdate,
// 			Events::postUpdate,
// 			Events::preRemove,
// 			Events::postRemove,
// 			// Events::loadClassMetadata,
// 			// Events::onClassMetadataNotFound,
// 			// Events::preFlush,
// 			// Events::onFlush,
// 			// Events::postFlush,
// 			// Events::onClear,
// 		);
// 	}

// 	protected function computeProperties() {
// 		if(null === $this->apply_condition) $this->apply_condition = true;
// 		$this->apply_condition = is_string($this->apply_condition) ? preg_replace('/[\\(\\)]/', '', $this->apply_condition) : (boolean)$this->apply_condition;
// 		return $this;
// 	}

// 	public function attachEntity($entity, serviceCache $serviceCache) {
// 		if($this->isLoaded(false)) throw new Exception("Error ".__METHOD__."(): this annotation is allready loaded!", 1);
// 		if(!is_object($entity)) throw new Exception("Error ".__METHOD__."(): entity must be object!", 1);
// 		$this->entity = $entity;
// 		$this->serviceCache = $serviceCache;
// 		$this->entity->_fsCache = $this;
// 		// $this->jms_serializer = $jms_serializer;
// 		return $this->isApply(true);
// 	}

// 	public function isLoaded($exception = false) {
// 		$loaded = is_object($this->entity) && $this->serviceCache instanceOf serviceCache;
// 		if(!$loaded && $exception) throw new Exception("Error ".__METHOD__."(): no sufficient data loaded!", 1);
// 		return $loaded;
// 	}

// 	public function getEntity() {
// 		return $this->entity;
// 	}

// 	public function getServiceCache() {
// 		return $this->serviceCache;
// 	}

// 	// public function getSerializer() {
// 	// 	return $this->jms_serializer;
// 	// }

// 	protected function computePropertiesAnnotations() {
// 		$applyProp = false;
// 		if($this->isApply(false)) {
// 			$this->CacheDataAnnotations = serviceClasses::getPropertysAnnotation(get_class($this->entity), CacheData::class, true);
// 			foreach ($this->CacheDataAnnotations as $name => $CDannot) {
// 				if($CDannot['Annotation']->attachEntity($this, $CDannot['ReflectionProperty'])) $applyProp = true;
// 			}
// 		}
// 		return $applyProp;
// 	}

// 	public function isApply($testProperties = true) {
// 		$this->isLoaded(true);
// 		$apply = false;
// 		if(is_string($this->apply_condition)) {
// 			$method = $this->apply_condition;
// 			$apply = (boolean)$this->entity->$method();
// 		} else {
// 			$apply = (boolean)$this->apply_condition;
// 		}
// 		if($apply && $testProperties) {
// 			if(false === $this->doApply) $apply = $this->computePropertiesAnnotations();
// 			// if none CacheData annotation is apply, so consider do not apply globally
// 			$CDapplied = array_filter($this->CacheDataAnnotations, function ($CDannot) {
// 				return $CDannot['Annotation']->isApply();
// 			});
// 			if(!count($CDapplied)) $apply = false;
// 		}
// 		$this->doApply = $apply;
// 		return $this->doApply;
// 	}

// 	public function getCacheds($reset = false) {
// 		foreach ($this->CacheDataAnnotations as $CDannot) $CDannot['Annotation']->getCached($reset);
// 		return $this;
// 	}


// }
