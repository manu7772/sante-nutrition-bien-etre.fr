<?php
namespace ModelApi\AnnotBundle\Annotation;

use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
use Doctrine\ORM\Events;

// use ModelApi\InternationalBundle\Service\serviceTwgTranslation;

/**
 * @Annotation
 * @Target("CLASS")
 * 
 * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 * @see https://www.doctrine-project.org/api/annotations/1.6/Doctrine/Common/Annotations/AnnotationReader.html
 */
class HasTranslatable extends BaseAnnotation {

	/**
	 * @var string
	 */
	public $domain = 'auto';

	/**
	 * Class running Events
	 */
	public static function getSubscribedEvents() {
		return array(
			static::postNew,
			// static::checkItem,
			static::preCreateForm,
			static::preUpdateForm,
			Events::postLoad,
			// Events::prePersist,
			// Events::postPersist,
			// Events::preUpdate,
			// Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			// Events::onFlush,
			// Events::postFlush,
			// Events::onClear,
		);
	}

	public function getEnvContexts() {
		return ['default'];
	}


}
