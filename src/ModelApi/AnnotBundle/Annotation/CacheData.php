<?php
// namespace ModelApi\AnnotBundle\Annotation;

// use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
// use Doctrine\ORM\Events;
// use JMS\Serializer\Serializer;
// use JMS\Serializer\SerializationContext;
// use JMS\Serializer\DeserializationContext;
// use Symfony\Component\Cache\Adapter\FilesystemAdapter;

// use ModelApi\BaseBundle\Service\serviceCache;
// use ModelApi\FileBundle\Service\serviceMenu;
// use ModelApi\AnnotBundle\Annotation\HasCache;
// use ModelApi\AnnotBundle\Service\serviceAnnotation;

// use \Exception;
// use \ReflectionClass;
// use \ReflectionProperty;

// /**
//  * @Annotation
//  * @Target("PROPERTY")
//  * 
//  * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
//  * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
//  */
// class CacheData extends BaseAnnotation {

// 	/**
// 	 * @var string
// 	 */
// 	public $getter = 'get';

// 	/**
// 	 * @var string
// 	 */
// 	public $setter = 'set';

// 	/**
// 	 * @var string
// 	 */
// 	public $cached_property_name = '_auto';

// 	/**
// 	 * @var string
// 	 */
// 	public $cached_property_getter = 'get';

// 	/**
// 	 * @var string
// 	 */
// 	public $cached_property_setter = 'set';

// 	/**
// 	 * @var string
// 	 * @Enum({"systemFile","attribute"})
// 	 */
// 	public $default_method = 'systemFile';

// 	/**
// 	 * @var mixed
// 	 */
// 	public $levels = 3;

// 	/**
// 	 * @var integer
// 	 */
// 	public $lifetime = null;

// 	/**
// 	 * @var integer
// 	 */
// 	public $folder = null;

// 	/**
// 	 * @var integer
// 	 */
// 	public $context = serviceCache::CONTEXT_LANG;

// 	/**
// 	 * @var integer
// 	 */
// 	public $env = null;

// 	/**
// 	 * @var mixed
// 	 */
// 	public $apply_condition = null;

// 	/**
// 	 * @var mixed
// 	 */
// 	public $serialization_groups = ['cached_item'];


// 	protected $entity = null;
// 	protected $property_name = null;
// 	protected $reflProperty = null;
// 	// protected $reflClass = null;
// 	protected $HasCache = null;
// 	protected $serviceCache = null;
// 	// protected $jms_serializer = null;
// 	// protected $FilesystemAdapter = null;
// 	protected $self_name = null;
// 	protected $data = null;

// 	/**
// 	 * Constructor
// 	 * @param array $options Key-value for properties to be defined in this class.
// 	 */
// 	public final function __construct(array $options = []) {
// 		foreach ($options as $key => $value) {
// 			switch ($key) {
// 				case 'apply_condition':
// 					if(null === $value) $value = true;
// 					$this->$key = is_string($value) ? preg_replace('/[\\(\\)]/', '', $value) : (boolean)$value;
// 					break;
// 				default: $this->$key = $value; break;
// 			}
// 		}
// 		$this->computeProperties();
// 	}

// 	/**
// 	 * Class running Events
// 	 */
// 	public static function getSubscribedEvents() {
// 		return array(
// 			// static::postNew,
// 			// static::checkItem,
// 			// static::preCreateForm,
// 			// static::preUpdateForm,
// 			// Events::postLoad,
// 			// Events::prePersist,
// 			// Events::postPersist,
// 			// Events::preUpdate,
// 			// Events::postUpdate,
// 			// Events::preRemove,
// 			// Events::postRemove,
// 			// Events::loadClassMetadata,
// 			// Events::onClassMetadataNotFound,
// 			// Events::preFlush,
// 			// Events::onFlush,
// 			// Events::postFlush,
// 			// Events::onClear,
// 		);
// 	}


// 	protected function computeProperties() {
// 		// apply_condition
// 		if(null === $this->apply_condition) $this->apply_condition = true;
// 		$this->apply_condition = is_string($this->apply_condition) ? preg_replace('/[\\(\\)]/', '', $this->apply_condition) : (boolean)$this->apply_condition;
// 		// serialization_groups
// 		if(!is_array($this->serialization_groups) || empty($this->serialization_groups) || (is_string($this->serialization_groups) && strtolower($this->serialization_groups) !== '_parent')) $this->serialization_groups = '_parent';
// 		// lifetime
// 		if(empty($this->lifetime)) $this->lifetime = serviceCache::CACHE_LIFETIME;
// 		return $this;
// 	}

// 	protected function computeByParent() {
// 		if($this->HasCache instanceOf HasCache) {
// 			foreach (['default_method','levels','apply_condition','serialization_groups','lifetime'] as $property) {
// 				if(strtolower($this->$property) === '_parent') $this->$property = $this->HasCache->$property;
// 			}
// 			// property names
// 			$this->property_name = $this->reflProperty->name;
// 			if(empty($this->cached_property_name) || $this->cached_property_name === '_auto') {
// 				$this->cached_property_name = preg_replace('/^(cached)/', '', $this->property_name);
// 			}
// 			$this->self_name = '_fsCache_'.$this->property_name;
// 			$this->entity->{$this->self_name} = $this;
// 			if(!property_exists($this->entity, $this->cached_property_name)) throw new Exception("Error ".__METHOD__."(): property ".json_encode($this->cached_property_name)." does not exist in entity ".$this->entity->getShortname()."!", 1);
// 			// property getter
// 			$this->getter = serviceAnnotation::getGetter($this->property_name, $this->entity, $this->getter);
// 			if(!method_exists($this->entity, $this->getter)) throw new Exception("Error ".__METHOD__."(): getter method ".json_encode($this->getter)." does not exist in entity ".$this->entity->getShortname()."!", 1);
// 			// property setter
// 			$this->setter = serviceAnnotation::getSetter($this->property_name, $this->entity, $this->setter);
// 			if(!method_exists($this->entity, $this->setter)) throw new Exception("Error ".__METHOD__."(): setter method ".json_encode($this->setter)." does not exist in entity ".$this->entity->getShortname()."!", 1);
// 			// cached property getter
// 			$this->cached_property_getter = serviceAnnotation::getGetter($this->cached_property_name, $this->entity, $this->cached_property_getter);
// 			if(!method_exists($this->entity, $this->cached_property_getter)) throw new Exception("Error ".__METHOD__."(): getter method ".json_encode($this->cached_property_getter)." does not exist in entity ".$this->entity->getShortname()."!", 1);
// 			// cached property setter
// 			$this->cached_property_setter = serviceAnnotation::getSetter($this->cached_property_name, $this->entity, $this->cached_property_setter);
// 			if(!method_exists($this->entity, $this->cached_property_setter)) throw new Exception("Error ".__METHOD__."(): setter method ".json_encode($this->cached_property_setter)." does not exist in entity ".$this->entity->getShortname()."!", 1);
// 		} else {
// 			throw new Exception("Error ".__METHOD__."(): HasCache parent does not exist!", 1);
// 		}
// 		$apply = $this->isApply();
// 		// FilesystemAdapter
// 		// if($apply) $this->FilesystemAdapter = new FilesystemAdapter(serviceCache::GLOBAL_CHILDS_CACHE_NAME, $this->lifetime, serviceCache::getCachePath($this->entity));
// 		return $apply;
// 	}

// 	public function attachEntity(HasCache $HasCache, ReflectionProperty $reflProperty) {
// 		if($this->isLoaded(false)) throw new Exception("Error ".__METHOD__."(): this annotation is allready loaded!", 1);
// 		if(!preg_match('/^(cached)/', $reflProperty->name)) throw new Exception("Error ".__METHOD__."(): this annotation's property name must start with \"cached\"!", 1);
// 		$this->HasCache = $HasCache;
// 		$this->reflProperty = $reflProperty;
// 		// $this->reflClass = $reflClass;
// 		$this->entity = $this->HasCache->getEntity();
// 		$this->serviceCache = $this->HasCache->getServiceCache();
// 		// $this->jms_serializer = $this->HasCache->getSerializer();
// 		return $this->computeByParent();
// 	}

// 	public function getServiceCache() {
// 		return $this->serviceCache;
// 	}

// 	public function isLoaded($exception = false) {
// 		$loaded = is_object($this->entity)
// 			&& $this->serviceCache instanceOf serviceCache
// 			&& $this->HasCache instanceOf HasCache
// 			&& $this->reflProperty instanceOf ReflectionProperty
// 			// && $this->reflClass instanceOf ReflectionClass
// 			;
// 		if(!$loaded && $exception) throw new Exception("Error ".__METHOD__."(): no sufficient data loaded!", 1);
// 		return $loaded;
// 	}

// 	public function getEntity() {
// 		return $this->entity;
// 	}

// 	// public function getSerializer() {
// 	// 	return $this->jms_serializer;
// 	// }

// 	public function isApply() {
// 		$this->isLoaded(true);
// 		$apply = false;
// 		if(is_string($this->apply_condition)) {
// 			$method = $this->apply_condition;
// 			$apply = (boolean)$this->entity->$method();
// 		} else {
// 			$apply = (boolean)$this->apply_condition;
// 		}
// 		return $apply;
// 	}

// 	public function isSystemFile() {
// 		return $this->default_method === 'systemFile';
// 	}

// 	public function isAttribute() {
// 		return $this->default_method === 'attribute';
// 	}

// 	public function getCached($refresh = false) {
// 		if($this->isApply()) {
// 			if($this->isSystemFile() && !empty($this->entity->getId())) {
// 				$entity = $this->entity;
// 				$getter = $this->cached_property_getter;
// 				// return $this->serviceCache->refreshEntityDataToCache($this->entity, $this->entity->$getter(), $this->serialization_groups, 'json', $this->lifetime, $refresh);
// 				return $this->serviceCache->getCacheSerialized(
// 					$entity,
// 					function() use ($entity, $getter) { return $entity->$getter(); },
// 					['groups' => $this->serialization_groups, 'format' => 'json'],
// 					$this->folder,
// 					$this->context,
// 					$this->env,
// 					$this->lifetime,
// 					$refresh
// 				);
// 			} else {
// 				throw new Exception("Error ".__METHOD__."(): cache method ".json_encode($this->default_method)." does not exist or not supported!", 1);
// 			}
// 		}
// 		// not applyed, so return normal property
// 		$getter = $this->cached_property_getter;
// 		return $this->entity->$getter();
// 	}


// }
