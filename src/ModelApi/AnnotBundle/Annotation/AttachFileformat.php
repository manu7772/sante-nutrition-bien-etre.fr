<?php
namespace ModelApi\AnnotBundle\Annotation;

use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
use Doctrine\ORM\Events;

/**
 * @Annotation
 * @Target("PROPERTY")
 * 
 * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class AttachFileformat extends BaseAnnotation {

	/**
	 * @var string
	 */
	public $attribute = 'stream_type_while_upload';

	/**
	 * @var string
	 */
	public $setter = 'setFileformat';

	/**
	 * Class running Eventspublic static
	 */
	public static function getSubscribedEvents() {
		return array(
			// static::postNew,
			// static::checkItem,
			// static::preCreateForm,
			// static::preUpdateForm,
			// Events::postLoad,
			// Events::prePersist,
			// Events::postPersist,
			// Events::preUpdate,
			// Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			// Events::onFlush,
			// Events::postFlush,
			// Events::onClear,
		);
	}


}
