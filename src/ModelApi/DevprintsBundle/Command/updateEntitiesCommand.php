<?php

namespace ModelApi\DevprintsBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Classes\DirectoryCollection;
use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceEntities;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceGroup;
use ModelApi\UserBundle\Service\serviceEntreprise;
// FileBundle
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// BinaryBundle
use ModelApi\BinaryBundle\Service\serviceBinary;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;

use \Exception;
use \DateTime;

class updateEntitiesCommand extends ContainerAwareCommand {

	// const DATE_FORMAT = 'Y-m-d H:i:s';
	const DATE_FORMAT = DATE_ATOM;

	protected $container;
	protected $cron_tasks_methods;
	protected $cron_tasks;
	protected $serviceEntities;
	protected $entityManager;

	protected function configure () {

		// On set le nom de la commande
		// --> $ php bin/console app:check <option>
		$this->setName('entities:update');

		// On set la description
		$this->setDescription("Mises à jour de toutes les entités");

		// On set l'aide
		$this->cron_tasks_methods = array_filter(get_class_methods($this), function($name) { return preg_match('/^cron_/', $name); });
		$this->cron_tasks = [];
		foreach ($this->cron_tasks_methods as $key => $cron_task) {
			$this->cron_tasks[$key] = strtolower(preg_replace('/^cron_/i', '', $cron_task));
		}
		$this->setHelp("Lancez cette commande pour mettre à jour toutes les entités.");

		// On prépare les arguments
		// $this
			// ->addArgument('name', InputArgument::REQUIRED, "Quel est ton prenom ?")
			// ->addArgument('action', InputArgument::OPTIONAL, "actions: all,clear")
			// ;
	}

	public function execute (InputInterface $input, OutputInterface $output) {
		$this->container = $this->getContainer();
		$this->container->get(serviceAnnotation::class)->setModeContext('check'); // Disable any unecessary functionnalities

		$operations = $this->getOperationsList();
		DevTerminal::Info('     Liste des opérations :');
		foreach ($operations as $method => $description) {
			DevTerminal::Info('     - '.$method.' > '.$description);
		}
		// $excludes = [];
		$excludes = ['Logg'];

		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->entityManager = $this->serviceEntities->getEntityManager();
		$entity_classnames = $this->serviceEntities->getEntitiesNames(true, null, true);
		foreach ($entity_classnames as $classname => $shortname) if (!in_array($shortname, $excludes)) {
			$treatedEntities = new ArrayCollection();
			DevTerminal::EOL();
			DevTerminal::ProgressText('Chargement des données : '.$shortname, DevTerminal::SUCCESS_STYLE, 200);
			$entities = $this->serviceEntities->getRepository($classname)->findAll();
			DevTerminal::ProgressText('Données '.$shortname.' chargées : '.count($entities), DevTerminal::SUCCESS_STYLE, 200);
			if(count($entities) > 0) {
				$nb = 0;
				DevTerminal::EOL();
				DevTerminal::progressBar($nb, count($entities), '---> Traitement opérations...', 50, true, null, 42);
				foreach ($entities as $entity) {
					if(!$treatedEntities->contains($entity)) {
						$this->doOperations($entity);
						$treatedEntities->add($entity);
					}
					DevTerminal::progressBar($nb++, count($entities), '---> Mises à jour...', 50, true, null, 42);
				}
				DevTerminal::progressBar($nb, count($entities), '---> Enregistrement', 50, true, null, 42);
				
				try {
					$this->entityManager->flush();
				} catch (Exception $e) {
					DevTerminal::Error(PHP_EOL.'!!!!!Errors while flush : '.$e->getMessage());
					die();
				}
				DevTerminal::progressBar($nb, count($entities), '---> '.DevTerminal::LIGHT_CYAN.$shortname.DevTerminal::PROGRESS_BAR_COLOR.' terminé!', 50, true, null, 42);
				$entities = null;
				$this->entityManager->clear();
			} else {
				// DevTerminal::EOL();
				DevTerminal::Info(' > Aucune donnée pour '.DevTerminal::LIGHT_CYAN.$shortname.DevTerminal::NO_STYLE.' : aucun traitement nécessaire.');
			}
		}
		DevTerminal::EOL();
		DevTerminal::Success('Tous les traitements ont été effectués : opérations terminées.', DevTerminal::SUCCESS_STYLE, 500);
		DevTerminal::EOL(2);
	}

	protected function getOperationsList() {
		return [
			'updateShortname' => 'Met à jour le nouveau champ "shortname".',
			'checkDirectorys' => 'Check le système de dossiers de l\'entité ainsi que les annotations "JoinedOwnDirectory".',
			'removeUsersParent' => 'Retire les parents (hardlink) aux utilisateurs en gardant le parent en tant que Dirparent (symlink).',
		];
	}

	protected function doOperations($entity) {
		$operations = $this->getOperationsList();
		// $manager = $this->container->get(serviceEntities::class)->getEntityManager();
		foreach ($operations as $method => $description) {
			if(method_exists($this, $method)) $this->$method($entity);
				else DevTerminal::Error(PHP_EOL.'Cette operation '.json_encode($method).' n\'existe pas!');
		}
	}

	// OPERATION #1
	protected function updateShortname($entity) {
		if(method_exists($entity, 'updateShortname') && empty($entity->getShortname())) {
			$entity->updateShortname();
		}
	}

	// OPERATION #2
	protected function checkDirectorys($entity) {
		if($entity instanceOf NestedInterface) {
			$entity->recomputeAllNesteds('owner');
			$this->container->get(serviceBasedirectory::class)->checkAttachedJoinedOwnDirectorys($entity);
			$this->container->get(serviceBasedirectory::class)->checkDirectorys($entity);
		}
		if($entity instanceOf Basedirectory) {
			if($entity->isCompleteOrIncompleteRoot()) {
				$entity->removeParent(false);
				$entity->removeDirparents();
			}
		}
	}

	// OPERATION #3
	protected function removeUsersParent($entity) {
		if($entity instanceOf User) $entity->removeParent(true);
	}


}
