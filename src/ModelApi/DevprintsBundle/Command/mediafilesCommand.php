<?php

namespace ModelApi\DevprintsBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Classes\DirectoryCollection;
use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceEntities;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceGroup;
use ModelApi\UserBundle\Service\serviceEntreprise;
// FileBundle
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Service\FileManager;
// BinaryBundle
use ModelApi\BinaryBundle\Service\serviceBinary;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;

use \Exception;
use \DateTime;

class mediafilesCommand extends ContainerAwareCommand {

	// const DATE_FORMAT = 'Y-m-d H:i:s';
	const DATE_FORMAT = DATE_ATOM;

	protected $container;
	protected $cron_tasks_methods;
	protected $cron_tasks;

	protected function configure () {

		// On set le nom de la commande
		// --> $ php bin/console app:check <option>
		$this->setName('mediafiles:generate');

		// On set la description
		$this->setDescription("Regénération du dossier /files : originaux et déclinaisons de binaryfiles (images, pdf, etc.).");

		// On set l'aide
		$this->cron_tasks_methods = array_filter(get_class_methods($this), function($name) { return preg_match('/^cron_/', $name); });
		$this->cron_tasks = [];
		foreach ($this->cron_tasks_methods as $key => $cron_task) {
			$this->cron_tasks[$key] = strtolower(preg_replace('/^cron_/i', '', $cron_task));
		}
		$this->setHelp("Lancez cette commande pour regénérer les originaux et déclinaisons des binaryfiles.".PHP_EOL."ATTENTION: le dossier web/files est totalement effacé pour la regénération.".PHP_EOL.PHP_EOL);

		// On prépare les arguments
		// $this
			// ->addArgument('name', InputArgument::REQUIRED, "Quel est ton prenom ?")
			// ->addArgument('action', InputArgument::OPTIONAL, "actions: all,clear")
			// ;
	}

	public function execute (InputInterface $input, OutputInterface $output) {
		$this->container = $this->getContainer();
		$this->container->get(serviceAnnotation::class)->setModeContext('check');
		DevTerminal::Info('Suppression du dossier web/files...');
		$fileSystem = new Filesystem();
		$path = $this->container->get(serviceKernel::class)->getBaseDir('web/files');
		// if(!$fileSystem->exists($path)) throw new Exception("Error ".__METHOD__."(): ".(is_dir($path) ? "directory" : "file")." ".json_encode($path)." could not be found!", 1);
		$fileSystem->remove($path);
		if($fileSystem->exists($path)) throw new Exception("Error ".__METHOD__."(): ".(is_dir($path) ? "directory" : "file")." ".json_encode($path)." could not be deleted!", 1);
		// serviceTools::removeFileEverywhere('web/files', '*');
		// $output->writeln('Début initialisation Entreprise : ');
		DevTerminal::Info('Commencement de la génération...');
		// $done_operations = false;

		//***************************************************************************************************************
		// Update all Items
		//***************************************************************************************************************
		// Just load them... will be udpated during flush operation...
		$items = $this->container->get(serviceBinary::class)->getRepository()->findAll();
		// $items = [];
		$dataCount = count($items);
		if($dataCount > 0) {
			DevTerminal::progressBar(0, 10, '---> loading Items...', 50, true);
			foreach ($items as $key => $item) {
				$this->container->get(serviceEntities::class)->applyPreFormEvents($item);
				DevTerminal::progressBar($key, $dataCount, '---> Updating...', 50, true);
				$item->checkFileUrls(true);
			}

			//***************************************************************************************************************
			// FLUSH ALL
			//***************************************************************************************************************
			try {
				$this->container->get(serviceEntities::class)->getEntityManager()->flush();
				$done_operations = true;
				DevTerminal::progressBar($dataCount, $dataCount, '---> All updated!', 50, true);
				DevTerminal::Info('Génération terminée!');
			} catch (Exception $e) {
				DevTerminal::Error(PHP_EOL.'Error while flush : '.$e->getMessage());
			}
			DevTerminal::Info('Génération terminée!');
		} else {
			DevTerminal::Warning('Aucun fichier binary file trouvé : aucune génération n\'a été effectuée!');
		}
		return $this;
	}







}
