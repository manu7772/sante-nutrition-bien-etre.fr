<?php

namespace ModelApi\DevprintsBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Classes\DirectoryCollection;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceBlog;
use ModelApi\BaseBundle\Service\serviceEntities;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceGroup;
use ModelApi\UserBundle\Service\serviceEntreprise;
// FileBundle
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Service\FileManager;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;

use \DateTime;

class checkallCommand extends ContainerAwareCommand {

	// const DATE_FORMAT = 'Y-m-d H:i:s';
	const DATE_FORMAT = DATE_ATOM;

	protected $container;
	protected $cron_tasks_methods;
	protected $cron_tasks;

	protected function configure () {

		// On set le nom de la commande
		// --> $ php bin/console app:check <option>
		$this->setName('app:check');

		// On set la description
		$this->setDescription("Commande de check d'entités.");

		// On set l'aide
		$this->cron_tasks_methods = array_filter(get_class_methods($this), function($name) { return preg_match('/^cron_/', $name); });
		$this->cron_tasks = [];
		foreach ($this->cron_tasks_methods as $key => $cron_task) {
			$this->cron_tasks[$key] = strtolower(preg_replace('/^cron_/i', '', $cron_task));
		}
		$this->setHelp("Lancez cette commande pour checker entitiés du site.".PHP_EOL.PHP_EOL);

		// On prépare les arguments
		// $this
			// ->addArgument('name', InputArgument::REQUIRED, "Quel est ton prenom ?")
			// ->addArgument('action', InputArgument::OPTIONAL, "actions: all,clear")
			// ;
	}

	public function execute (InputInterface $input, OutputInterface $output) {
		// $output->writeln('Début initialisation Entreprise : ');
		DevTerminal::Info('Début initialisation Entreprise : ');
		$this->container = $this->getContainer();
		$this->container->get(serviceAnnotation::class)->setModeContext('check');
		$done_operations = false;


		// //***************************************************************************************************************
		// // Update all Items
		// //***************************************************************************************************************
		// // Just load them... will be udpated during flush operation...
		// DevTerminal::progressBar(0, 10, '  ---> loading Items...', 50, true);
		// $items = $this->container->get(serviceItem::class)->getRepository()->findAll();
		// $dataCount = count($items);
		// foreach ($items as $key => $item) {
		// 	DevTerminal::progressBar($key, $dataCount, '  ---> updating Items/Basedirectorys...', 50, true);
		// 	$dd = $item->getDriveDir();
		// 	if($dd instanceOf Directory && $dd->getOwnerdrivedir() !== $item) $dd->setOwnerdrivedir($item, false);
		// 	$sd = $item->getSystemDir();
		// 	if($sd instanceOf Directory && $sd->getOwnersystemdir() !== $item) $sd->setOwnersystemdir($item, false);
		// }
		// DevTerminal::progressBar($dataCount, $dataCount, '  ---> Updated. Flushing...', 50, true);

		//***************************************************************************************************************
		// Update all Items
		//***************************************************************************************************************
		// Just load them... will be udpated during flush operation...
		DevTerminal::progressBar(0, 10, '  ---> loading Items...', 50, true);
		$items = $this->container->get(serviceBlog::class)->getRepository()->findAll();
		$dataCount = count($items);
		foreach ($items as $key => $item) {
			DevTerminal::progressBar($key, $dataCount, '  ---> updating publication date...', 50, true);
			$item->setPublication(new DateTime());
		}
		DevTerminal::progressBar($dataCount, $dataCount, '  ---> Updated. Flushing...', 50, true);




		//***************************************************************************************************************
		// FLUSH ALL
		//***************************************************************************************************************
		try {
			$this->container->get(serviceEntities::class)->getEntityManager()->flush();
			$done_operations = true;
			DevTerminal::progressBar($dataCount, $dataCount, '  ---> FLUSHED!', 50, true);
		} catch (Exception $e) {
			DevTerminal::Error(PHP_EOL.PHP_EOL.'Error while flush : ');
		}

		if($done_operations) {
			$date = new DateTime();
			$date = $date->format(static::DATE_FORMAT);
			DevTerminal::Warning(PHP_EOL.PHP_EOL.'Entités checkées : '.$date.PHP_EOL);
		} else {
			DevTerminal::Info(PHP_EOL.PHP_EOL.'Aucune opération réalisée.'.PHP_EOL);
		}

	}







}
