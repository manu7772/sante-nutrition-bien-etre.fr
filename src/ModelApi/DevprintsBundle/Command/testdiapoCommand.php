<?php

namespace ModelApi\DevprintsBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Classes\DirectoryCollection;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceEntities;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceGroup;
use ModelApi\UserBundle\Service\serviceEntreprise;
// FileBundle
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Service\FileManager;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;

use \DateTime;

class testdiapoCommand extends ContainerAwareCommand {

	const DATE_FORMAT = 'Y-m-d H:i:s';

	protected $container;
	protected $cron_tasks_methods;
	protected $cron_tasks;

	protected function configure () {

		// On set le nom de la commande
		// --> $ php bin/console app:init <option>
		$this->setName('app:diapotest');

		// On set la description
		$this->setDescription("Commande de test d'intégration d'images dans un diaporama.");

		// On set l'aide
		$this->cron_tasks_methods = array_filter(get_class_methods($this), function($name) { return preg_match('/^cron_/', $name); });
		$this->cron_tasks = [];
		foreach ($this->cron_tasks_methods as $key => $cron_task) {
			$this->cron_tasks[$key] = strtolower(preg_replace('/^cron_/i', '', $cron_task));
		}
		$this->setHelp("Lancez cette commande pour tester l'intégration d'images dans un dossier (system) Diaporamas.".PHP_EOL.PHP_EOL);

		// On prépare les arguments
		// $this
			// ->addArgument('name', InputArgument::REQUIRED, "Quel est ton prenom ?")
			// ->addArgument('action', InputArgument::OPTIONAL, "actions: all,clear")
			// ;
	}

	public function execute (InputInterface $input, OutputInterface $output) {
		// $output->writeln('Début initialisation Entreprise : ');
		DevTerminal::Info('Début du test :');
		$this->container = $this->getContainer();
		$em = $this->container->get(serviceEntities::class)->getEntityManager();

		$user = $em->getRepository(User::class)->findOneByUsername('sadmin');
		if(empty($user)) { DevTerminal::Error('  --> User Sadmin non trouvé !'.PHP_EOL);die(); }
		DevTerminal::Info('--- User : '.$user->getUsername());

		$images = [
			__DIR__.'/../../../FixturesBundle/DataFixtures/media/images/img_avatars/Portrait600_5.png',
			__DIR__.'/../../../FixturesBundle/DataFixtures/media/images/img_avatars/centos-logo.png',
			__DIR__.'/../../../FixturesBundle/DataFixtures/media/images/img_avatars/Portrait600_3.png',
		];
		foreach ($images as $file) {
			if(!file_exists($file)) { DevTerminal::Error('  --> Fichier '.json_encode($file).' non trouvé !'.PHP_EOL);die(); }
		}

		$user->setUrlAsDiapos($images);
		// foreach ($user->getDiapos() as $diapo) {
		// 	$em->persist($diapo);
		// }
		$em->flush();

		foreach ($user->findInSystemDirByName('Diaporamas')->getChilds() as $key => $diapo) {
			DevTerminal::Info('--- Diapo '.$key.' : '.$diapo->getSlug().' ('.get_class($diapo).')');
		}

		DevTerminal::Warning('Opération effectuée. Veuillez vérifer que le User Sadmin a bien récupéré les fichiers dans son dossier système > "Diaporamas".'.PHP_EOL);

	}







}
