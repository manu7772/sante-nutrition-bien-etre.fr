<?php

namespace ModelApi\DevprintsBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Classes\DirectoryCollection;
use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceUser;
// use ModelApi\UserBundle\Service\serviceGroup;
use ModelApi\UserBundle\Service\serviceEntreprise;
// BinaryBundle
use ModelApi\BinaryBundle\Service\serviceBinary;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;

use \Exception;
use \DateTime;

class restoreOwnersCommand extends ContainerAwareCommand {

	// const DATE_FORMAT = 'Y-m-d H:i:s';
	const DATE_FORMAT = DATE_ATOM;
	const TEST = false;

	protected $container;
	protected $cron_tasks_methods;
	protected $cron_tasks;

	protected function configure () {

		// On set le nom de la commande
		// --> $ php bin/console app:check <option>
		$this->setName('owners:restore');

		// On set la description
		$this->setDescription("Restore les owners par défaut");

		// On set l'aide
		$this->cron_tasks_methods = array_filter(get_class_methods($this), function($name) { return preg_match('/^cron_/', $name); });
		$this->cron_tasks = [];
		foreach ($this->cron_tasks_methods as $key => $cron_task) {
			$this->cron_tasks[$key] = strtolower(preg_replace('/^cron_/i', '', $cron_task));
		}
		$this->setHelp("Lancez cette commande pour rendre les entités à leurs owners par défaut.".PHP_EOL.PHP_EOL);

		// On prépare les arguments
		// $this
			// ->addArgument('name', InputArgument::REQUIRED, "Quel est ton prenom ?")
			// ->addArgument('action', InputArgument::OPTIONAL, "actions: all,clear")
			// ;
	}

	public function execute (InputInterface $input, OutputInterface $output) {
		ini_set('memory_limit','1024M');
		// ini_set('memory_limit','4096M');
		$this->container = $this->getContainer();
		$this->container->get(serviceAnnotation::class)->setModeContext('check');
		$serviceEntities = $this->container->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		$serviceItem = $this->container->get(serviceItem::class);
		$serviceTwig = $this->container->get(serviceTwig::class);

		DevTerminal::EOL(1);
		$mainUserAdmin = $this->container->get(serviceUser::class)->getMainUserAdmin();
		DevTerminal::Info('<T5>Start restore and check owners!');
		DevTerminal::Info('<T5>Info: main User admin is '.DevTerminal::CLI_LIGHT_CYAN.($mainUserAdmin instanceOf User ? $mainUserAdmin->getUsername() : 'NOT FOUND!!!').DevTerminal::INFO_STYLE.'.');
		if(!($mainUserAdmin instanceOf User)) {
			DevTerminal::Error('<T5>Could not find main admin User!');
			DevTerminal::Error('<T5>---> Process <<<STOPPED>>>! <---', 2);
			die();
		}

		DevTerminal::Success('<T3>* Restore fichiers Twig: <<<...>>>');
		$twigs = $serviceTwig->getRepository()->findBy(['shortname' => 'Twig']);
		foreach ($twigs as $key => $twig) {
			DevTerminal::progressBar($key, count($twigs), '---> Restore Twigs', 50, true, null, 42);
			$twig->checkAndRepairValidity();
			$twig->setOwner($mainUserAdmin, true);
			if($twig->getOwner() !== $mainUserAdmin) {
				DevTerminal::Error('<T5>Error line '.__LINE__.' '.__METHOD__.'(): for '.$twig->getShortname().' '.json_encode($twig->getName()).' ==> owner '.$twig->getOwner()->getShortname().' '.json_encode($twig->getOwner()->getName()).' is wrong!');
				DevTerminal::Error('<T5>---> Process <<<STOPPED>>>! <---', 2);
				die();
			}
			if($twig->getParent()->getOwner() !== $mainUserAdmin) {
				DevTerminal::Error('<T5>Error line '.__LINE__.' '.__METHOD__.'(): for '.$twig->getShortname().' '.json_encode($twig->getName()).' ==> parent\'s owner is wrong : '.$twig->getParent()->getShortname().' '.json_encode($twig->getParent()->getName()).' (Owner is '.$twig->getParent()->getOwner()->getShortname().' '.json_encode($twig->getParent()->getOwner()->getName()).') could not be removed!');
				DevTerminal::Error('<T5>---> Process <<<STOPPED>>>! <---', 2);
				die();
			}
			foreach ($twig->getDirparents() as $dirparent) {
				$dirparent->checkAndRepairValidity();
				if($dirparent->getOwnerdir() instanceOf Pageweb) {
					$dirparent->addChild($twig);
				} else if($dirparent->getOwner() !== $mainUserAdmin) {
					if(!$twig->removeDirparent($dirparent)) {
						DevTerminal::Error('<T5>Error line '.__LINE__.' '.__METHOD__.'(): for '.$twig->getShortname().' '.json_encode($twig->getName()).' ==> '.$dirparent->getShortname().' '.json_encode($dirparent->getName()).' (Owner is '.$dirparent->getOwner()->getShortname().' '.json_encode($dirparent->getOwner()->getName()).') could not be removed!');
						DevTerminal::Error('<T5>---> Process <<<STOPPED>>>! <---', 2);
						die();
					}
				}
			}
		}
		DevTerminal::progressBar(100, 100, '---> Restore Twigs', 50, true, null, 42);
		DevTerminal::EOL(1);
		DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Twigs<<<...>>>', DevTerminal::WARN_STYLE, 500);
		try {
			if(!static::TEST) $em->flush();
			DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Twigs : terminé !', DevTerminal::SUCCESS_STYLE, 500);
		} catch (Exception $e) {
			DevTerminal::launchException('<E<!!!!!>>> Errors while flush Twigs : '.$e->getMessage(), __LINE__, __METHOD__);
		}
		// $em->clear();
		DevTerminal::EOL(1);

		DevTerminal::Success('<T3>* Restore fichiers Pageweb: <<<...>>>');
		$pagewebs = $serviceTwig->getRepository()->findBy(['shortname' => 'Pageweb']);
		foreach ($pagewebs as $key => $pageweb) {
			DevTerminal::progressBar($key, count($pagewebs), '---> Restore Pagewebs', 50, true, null, 42);
			$pageweb->checkAndRepairValidity();
			$pageweb->setOwner($mainUserAdmin, true);
			foreach ($pageweb->getDirparents() as $dirparent) {
				if($dirparent->getOwner() !== $mainUserAdmin && !($dirparent instanceOf Menu)) $pageweb->removeDirparent($dirparent);
			}
		}
		DevTerminal::progressBar(100, 100, '---> Restore Pagewebs', 50, true, null, 42);
		DevTerminal::EOL(1);
		DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Pagewebs<<<...>>>', DevTerminal::WARN_STYLE, 500);
		try {
			if(!static::TEST) $em->flush();
			DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Pagewebs : terminé !', DevTerminal::SUCCESS_STYLE, 500);
		} catch (Exception $e) {
			DevTerminal::launchException('<E<!!!!!>>> Errors while flush Pagewebs : '.$e->getMessage(), __LINE__, __METHOD__);
		}
		$em->clear();
		DevTerminal::EOL(1);

		DevTerminal::Success('<T3>* Restore fichiers Entreprises: <<<...>>>');
		$entreprises = $this->container->get(serviceEntreprise::class)->getRepository()->findAll();
		foreach ($entreprises as $key => $entreprise) {
			DevTerminal::progressBar($key, count($entreprises), '---> Restore Entreprises', 50, true, null, 42);
			$entreprise->checkAndRepairValidity();
			if($entreprise->getOwner()->getName() === 'root') {
				$entreprise->setOwner($entreprise->getOwnerdir(), true);
			}
			foreach ($entreprise->getDirparents() as $dirparent) {
				if($dirparent->getOwner() !== $mainUserAdmin && !($dirparent instanceOf Entreprise)) $entreprise->removeDirparent($dirparent);
			}
		}
		DevTerminal::progressBar(100, 100, '---> Restore Entreprises', 50, true, null, 42);
		DevTerminal::EOL(1);
		DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Entreprises<<<...>>>', DevTerminal::WARN_STYLE, 500);
		try {
			if(!static::TEST) $em->flush();
			DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Entreprises : terminé !', DevTerminal::SUCCESS_STYLE, 500);
		} catch (Exception $e) {
			DevTerminal::launchException('<E<!!!!!>>> Errors while flush Entreprises : '.$e->getMessage(), __LINE__, __METHOD__);
		}
		$em->clear();
		DevTerminal::EOL(1);

		// DevTerminal::Success('<T3>* Restore fichiers Menus: <<<...>>>');
		// $menus = $this->container->get(serviceMenu::class)->getRepository()->findBy(['rootmenu' => true]);
		// foreach ($menus as $key => $menu) {
		// 	DevTerminal::progressBar($key, count($menus), '---> Restore Menus', 50, true, null, 42);
		// 	$menu->checkAndRepairValidity();
		// 	$menu->setOwner($mainUserAdmin, true);
		// 	foreach ($menu->getDirparents() as $dirparent) {
		// 		if($dirparent->getOwner() !== $mainUserAdmin && !($dirparent instanceOf Menu)) $menu->removeDirparent($dirparent);
		// 	}
		// }
		// DevTerminal::progressBar(100, 100, '---> Restore Menus', 50, true, null, 42);
		// DevTerminal::EOL(1);
		// DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Menus<<<...>>>', DevTerminal::WARN_STYLE, 500);
		// try {
		// 	if(!static::TEST) $em->flush();
		// 	DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Menus : terminé !', DevTerminal::SUCCESS_STYLE, 500);
		// } catch (Exception $e) {
		// 	DevTerminal::launchException('<E<!!!!!>>> Errors while flush Menus : '.$e->getMessage(), __LINE__, __METHOD__);
		// }
		// $em->clear();
		// DevTerminal::EOL(1);

		DevTerminal::Success('<T3>* Check all Items owners: <<<...>>>');
		$items = $serviceItem->getRepository()->findAll();
		foreach ($items as $key => $item) {
			DevTerminal::progressBar($key, count($items), '---> Restore Items', 50, true, null, 42);
			$item->checkAndRepairValidity();
			if($item->getOwner()->getName() === 'root') {
				$item->setOwner($item->getOwnerdir(), true);
			}
		}
		DevTerminal::progressBar(100, 100, '---> Restore Items', 50, true, null, 42);
		DevTerminal::EOL(1);
		DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Items<<<...>>>', DevTerminal::WARN_STYLE, 500);
		try {
			if(!static::TEST) $em->flush();
			DevTerminal::ProgressText('Enregistrement'.(static::TEST ? DevTerminal::CLI_LIGHT_GREEN.' TEST'.DevTerminal::SUCCESS_STYLE : '').' Items : terminé !', DevTerminal::SUCCESS_STYLE, 500);
		} catch (Exception $e) {
			DevTerminal::launchException('<E<!!!!!>>> Errors while flush Items : '.$e->getMessage(), __LINE__, __METHOD__);
		}
		$em->clear();
		DevTerminal::EOL(1);




		// DevTerminal::EOL(1);
		// if($check_transfer) {
		// 	DevTerminal::ProgressText('Vérification finale<<<...>>>', null, 1000);
		// 	$directorys = $serviceBasedirectory->findAll();
		// 	$directorys = array_filter($directorys, function($directory) { return !$directory->isTransited(); });
		// 	if(count($directorys)) {
		// 		DevTerminal::ProgressText('Nombre de transferts encore non traités: '.count($directorys).' sur '.$total.'! Veuillez relancer le processus, svp.', DevTerminal::ERROR_STYLE, 500);
		// 		DevTerminal::EOL(2);
		// 	} else {
		// 		DevTerminal::ProgressText('Vérification correcte. Tous les transferts sont traités. Merci!', DevTerminal::SUCCESS_STYLE, 500);
		// 		DevTerminal::EOL(2);
		// 	}
		// } else {
			DevTerminal::EOL(1);
			DevTerminal::ProgressText('Tous les éléments sont restorés. Merci!', DevTerminal::SUCCESS_STYLE, 500);
			DevTerminal::EOL(2);
		// }
		return $this;
	}






}
