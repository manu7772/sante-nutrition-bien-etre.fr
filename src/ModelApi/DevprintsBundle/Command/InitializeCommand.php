<?php

namespace ModelApi\DevprintsBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Classes\DirectoryCollection;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceEntities;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceGroup;
use ModelApi\UserBundle\Service\serviceEntreprise;
// FileBundle
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Service\FileManager;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;

use \DateTime;

class InitializeCommand extends ContainerAwareCommand {

	const DATE_FORMAT = 'Y-m-d H:i:s';

	protected $container;
	protected $cron_tasks_methods;
	protected $cron_tasks;

	protected function configure () {

		// On set le nom de la commande
		// --> $ php bin/console app:init <option>
		$this->setName('app:init');

		// On set la description
		$this->setDescription("Commande d'initialisation du site web.");

		// On set l'aide
		$this->cron_tasks_methods = array_filter(get_class_methods($this), function($name) { return preg_match('/^cron_/', $name); });
		$this->cron_tasks = [];
		foreach ($this->cron_tasks_methods as $key => $cron_task) {
			$this->cron_tasks[$key] = strtolower(preg_replace('/^cron_/i', '', $cron_task));
		}
		$this->setHelp("Lancez cette commande juste après la génération des fixtures, afin de terminer les liaisons entre entitiés. Le site sera alors totalement opérationel.".PHP_EOL.PHP_EOL);

		// On prépare les arguments
		// $this
			// ->addArgument('name', InputArgument::REQUIRED, "Quel est ton prenom ?")
			// ->addArgument('action', InputArgument::OPTIONAL, "actions: all,clear")
			// ;
	}

	public function execute (InputInterface $input, OutputInterface $output) {
		// $output->writeln('Début initialisation Entreprise : ');
		DevTerminal::Info('Début initialisation Entreprise : ');
		$this->container = $this->getContainer();
		$this->container->get(serviceAnnotation::class)->setModeContext('init');
		$done_operations = false;

		$entreprise = $this->container->get(serviceEntreprise::class)->getDefaultEntreprise();
		if(empty($entreprise)) { DevTerminal::Error('  --> Aucune entreprise trouvée !'.PHP_EOL);die(); }
		DevTerminal::Info('--- Entreprise : '.$entreprise->getName());

		// $classnames = [
		// 	Text::class,
		// 	Html::class,
		// 	Twig::class,
		// 	Pageweb::class,
		// 	Image::class,
		// 	Pdf::class,
		// 	Video::class,
		// ];
		foreach ($this->container->get(serviceEntities::getEntitiesOfInstance(Item::class, true, true)) as $classname => $shortname) {
			$directorys[$classname] = $this->container->get(serviceItem::class)->getDefaultOwnerDirectory($classname, $entreprise, true);
			// $pathname = $classname::DEFAULT_OWNER_DIRECTORY;
			// $directorys[$classname] = $this->container->get(serviceEntreprise::class)->getTierDirectory($pathname, $entreprise);
			if(!($directorys[$classname] instanceOf Directory)) { DevTerminal::Error('  --> Aucun dossier '.json_encode($pathname).' trouvé !'.PHP_EOL);die(); }
		}

		//***************************************************************************************************************
		// Get orphan Files
		//***************************************************************************************************************
		$files = $this->container->get(FileManager::class)->findOrphanFiles($classnames);
		if(count($files)) {
			foreach ($files as $file) $directorys[$file->getClassname()]->addChild($file, false);
			$this->container->get(serviceEntities::class)->getEntityManager()->flush();
		}
		DevTerminal::Info('  - Orphan files : '.count($files).' -> OK.');

		//***************************************************************************************************************
		// Attribute Groups
		//***************************************************************************************************************
		$groupE = $entreprise->getMaingroup();
		if(!($groupE instanceOf Group)) { DevTerminal::Error('  --> Aucun groupe trouvé pour Entreprise !'.PHP_EOL);die(); }
		$group = $this->container->get(serviceGroup::class)->getRepository()->findOneByName('DIRECTION');
		if(!($group instanceOf Group)) { DevTerminal::Error('  --> Groupe DIRECTION introuvable !'.PHP_EOL);die(); }
		$groupE->addChild($group);
		DevTerminal::Info('  - Groups : '.$group.' -> set in '.$groupE.' -> OK.');

		//***************************************************************************************************************
		// Menu principal --> headers
		//***************************************************************************************************************
		$menu_name = 'Principal_1';
		$menu = $this->container->get(serviceMenu::class)->getRepository()->findOneByName($menu_name);
		if(!($menu instanceOf Menu)) { DevTerminal::Error('  --> Menu principal '.json_encode($menu_name).' introuvable !'.PHP_EOL);die(); }
		$headers = $this->container->get(serviceTwig::class)->getRepository()->findBy(['typepage' => 'header']);
		if(!count($headers)) { DevTerminal::Error('  --> Aucun header trouvé !'.PHP_EOL);die(); }
		foreach ($headers as $key => $header) {
			// $header_directory = $this->container->get(FileManager::class)->getAttributeDirectory($header, 'dirmenus', true);
			// if(!($header_directory instanceOf Basedirectory)) { DevTerminal::Error('  --> No directory found for '.json_encode($header).' attribute -> "dirmenus"!'.PHP_EOL);die(); }
			$header->setDirmenus(new ArrayCollection([$menu]));
			foreach ($header->getDirmenus() as $retrieved_menu) {
				DevTerminal::Info('  - Header '.$key.' : '.$header.' -> setted menu '.json_encode($retrieved_menu->getSlug()).' -> OK.');
			}
		}

		//***************************************************************************************************************
		// Menu side --> sidemenus
		//***************************************************************************************************************
		$menu_name = 'Side_1';
		$menu = $this->container->get(serviceMenu::class)->getRepository()->findOneByName($menu_name);
		if(!($menu instanceOf Menu)) { DevTerminal::Error('  --> Side menu '.json_encode($menu_name).' introuvable !'.PHP_EOL);die(); }
		$sides = $this->container->get(serviceTwig::class)->getRepository()->findBy(['typepage' => 'sidemenu']);
		// if(!count($sides)) { DevTerminal::Error('  --> Aucun side trouvé !'.PHP_EOL);die(); }
		if(!count($sides)) {
			DevTerminal::Info('  - side  : no side menu found -> OK.');
		} else {
			foreach ($sides as $key => $side) {
				// $side_directory = $this->container->get(FileManager::class)->getAttributeDirectory($side, 'dirmenus', true);
				// if(!($side_directory instanceOf Basedirectory)) { DevTerminal::Error('  --> No directory found for '.json_encode($side).' attribute -> "dirmenus"!'.PHP_EOL);die(); }
				$side->setDirmenus(new ArrayCollection([$menu]));
				foreach ($side->getDirmenus() as $retrieved_menu) {
					DevTerminal::Info('  - side '.$key.' : '.$side.' -> setted menu '.json_encode($retrieved_menu->getSlug()).' -> OK.');
				}
			}
		}

		//***************************************************************************************************************
		// Menu footer --> footermenus
		//***************************************************************************************************************
		$menu_name = 'Footer_1';
		$menu = $this->container->get(serviceMenu::class)->getRepository()->findOneByName($menu_name);
		if(!($menu instanceOf Menu)) { DevTerminal::Error('  --> Footer menu '.json_encode($menu_name).' introuvable !'.PHP_EOL);die(); }
		$footers = $this->container->get(serviceTwig::class)->getRepository()->findBy(['typepage' => 'footer']);
		// if(!count($footers)) { DevTerminal::Error('  --> Aucun footer trouvé !'.PHP_EOL);die(); }
		if(!count($footers)) {
			DevTerminal::Info('  - footer  : no footer menu found -> OK.');
		} else {
			foreach ($footers as $key => $footer) {
				// $footer_directory = $this->container->get(FileManager::class)->getAttributeDirectory($footer, 'dirmenus', true);
				// if(!($footer_directory instanceOf Basedirectory)) { DevTerminal::Error('  --> No directory found for '.json_encode($footer).' attribute -> "dirmenus"!'.PHP_EOL);die(); }
				$footer->setDirmenus(new ArrayCollection([$menu]));
				foreach ($footer->getDirmenus() as $retrieved_menu) {
					DevTerminal::Info('  - footer '.$key.' : '.$footer.' -> setted menu '.json_encode($retrieved_menu->getSlug()).' -> OK.');
				}
			}
		}

		//***************************************************************************************************************
		// Update all StoreDirContents
		//***************************************************************************************************************
		// Just load them... will be udpated during flush operation...
		DevTerminal::progressBar(0, 10, '  ---> loading Items...', 50, true);
		$items = $this->container->get(serviceItem::class)->getRepository()->findAll();
		$dataCount = count($items);
		foreach ($items as $key => $item) {
			DevTerminal::progressBar($key, $dataCount, '  ---> updating Items...', 50, true);
			$item->updateDate();
		}
		DevTerminal::progressBar($dataCount, $dataCount, '  ---> Updated. Flushing...', 50, true);




		//***************************************************************************************************************
		// FLUSH ALL
		//***************************************************************************************************************
		$this->container->get(serviceEntities::class)->getEntityManager()->flush();
		$done_operations = true;
		DevTerminal::progressBar($dataCount, $dataCount, '  ---> FLUSHED!', 50, true);

		if($done_operations) {
			$date = new DateTime();
			$date = $date->format(static::DATE_FORMAT);
			DevTerminal::Warning(PHP_EOL.PHP_EOL.'Site initialisé et opérationel : '.$date.PHP_EOL);
		} else {
			DevTerminal::Info(PHP_EOL.PHP_EOL.'Aucune opération réalisée. Le site est certainement déjà initialisé.'.PHP_EOL);
		}

	}







}
