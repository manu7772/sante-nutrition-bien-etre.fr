<?php

namespace ModelApi\DevprintsBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Classes\DirectoryCollection;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Traits\BaseEntity;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceGroup;
use ModelApi\UserBundle\Service\serviceEntreprise;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Service\FileManager;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;

use \DateTime;

class checkAdminvalidationsCommand extends ContainerAwareCommand {

	const DATE_FORMAT = 'Y-m-d H:i:s';

	protected $container;
	protected $cron_tasks_methods;
	protected $cron_tasks;

	protected function configure () {

		// On set le nom de la commande
		// --> $ php bin/console app:init <option>
		$this->setName('app:adminvalidations');

		// On set la description
		$this->setDescription("Check admin validations.");

		// On set l'aide
		$this->cron_tasks_methods = array_filter(get_class_methods($this), function($name) { return preg_match('/^cron_/', $name); });
		$this->cron_tasks = [];
		foreach ($this->cron_tasks_methods as $key => $cron_task) {
			$this->cron_tasks[$key] = strtolower(preg_replace('/^cron_/i', '', $cron_task));
		}
		$this->setHelp("Lancez cette commande pour vérifier les validations admins sur les entités existantes.".PHP_EOL.PHP_EOL);

		// On prépare les arguments
		// $this
			// ->addArgument('name', InputArgument::REQUIRED, "Quel est ton prenom ?")
			// ->addArgument('action', InputArgument::OPTIONAL, "actions: all,clear")
			// ;
	}

	public function execute (InputInterface $input, OutputInterface $output) {
		// $output->writeln('Début initialisation check : ');
		DevTerminal::ProgressText('Start check: <<<...>>>', null, 500);
		$this->container = $this->getContainer();
		$this->container->get(serviceAnnotation::class)->setModeContext('check');

		$serviceEntities = $this->container->get(serviceEntities::class);
		$classes = $serviceEntities->getEntitiesOfTrait(BaseEntity::class, true, true);
		DevTerminal::ProgressText('Start check:', null, 500);
		DevTerminal::EOL();
		foreach ($classes as $classname => $shortname) {
			DevTerminal::progressBar(0, 10, '---> load '.$shortname.'...', 50, true);
			$entities = $serviceEntities->getRepository($classname)->findAll();
			$dataCount = count($entities);
			if($dataCount > 0) {
				foreach ($entities as $key => $entity) {
					DevTerminal::progressBar($key, $dataCount, '---> check '.$shortname, 50, true);
					if($entity instanceOf Item) $entity->getOwnerdir(true);
					$entity->checkAdminvalidation();
				}
				DevTerminal::progressBar($dataCount, $dataCount, '---> '.$shortname.' Flushing...', 50, true);
				$serviceEntities->getEntityManager()->flush();
				DevTerminal::progressBar($dataCount, $dataCount, '---> '.$shortname.' Checked', 50, true);
				DevTerminal::EOL();
			} else {
				DevTerminal::ProgressText('<T5>---> No '.$shortname.' found. Continue...');
				DevTerminal::EOL();
			}
			$serviceEntities->getEntityManager()->clear();
		}

		DevTerminal::SuccessPlus(PHP_EOL.PHP_EOL.'Opération terminée !');

	}







}
