<?php

namespace ModelApi\DevprintsBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Yaml\Yaml;
// use Symfony\Component\Yaml\Parser;
// use Symfony\Component\Yaml\Dumper;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceContext;
// EventBundle
use ModelApi\EventBundle\Service\serviceEvent;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;
// UserBundle
use ModelApi\UserBundle\Entity\User;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
// FileBundle
use ModelApi\FileBundle\Entity\Image;

use \DateTime;

class SfcronCommand extends ContainerAwareCommand {

	const REPORT_PATH = '/var/cron/';
	const REPORT_FILE = 'cron.yml';

	protected $container;
	protected $entityManager;
	protected $yaml_report;
	protected $cron_tasks_methods;
	protected $cron_tasks;
	protected $hrStart;
	protected $hrMark;

	protected function configure () {
		// On set le nom de la commande
		// --> $ bin/console app:sfcron <option>
		$this->setName('app:sfcron');

		// On set la description
		$this->setDescription("Actions CRON pour Symfony / Toutes les minutes");

		// On set l'aide
		$this->cron_tasks_methods = array_filter(get_class_methods($this), function($name) { return preg_match('/^cron_/', $name); });
		$this->cron_tasks = [];
		foreach ($this->cron_tasks_methods as $key => $cron_task) {
			$this->cron_tasks[$key] = strtolower(preg_replace('/^cron_/i', '', $cron_task));
		}
		$this->setHelp("Vous pouvez lancer cette action manuellement, sans paramètre (ou 'all'), pour lancer toutes les tâches Cron.".PHP_EOL."L'option 'clear' permettra de vider le fichier de rapport".PHP_EOL."--> ".static::REPORT_PATH.static::REPORT_FILE."".PHP_EOL."Vous pouvez lancer uniquement certaines tâches (séparées par des slashes \"/\") :".PHP_EOL."- ".implode(PHP_EOL.'- ', $this->cron_tasks).PHP_EOL.PHP_EOL);

		// On prépare les arguments
		$this
			// ->addArgument('name', InputArgument::REQUIRED, "Quel est ton prenom ?")
			->addArgument('action', InputArgument::OPTIONAL, "actions: all,clear")
			;
	}

	public function execute (InputInterface $input, OutputInterface $output) {
		$this->initHr();
		$this->container = $this->getContainer();

		// optional actions
		$action = strtolower($input->getArgument('action'));
		if(!strlen($action)) $action = 'all';
		$actions = explode(DIRECTORY_SEPARATOR, $action);
		$serviceLanguage = $this->container->get(serviceLanguage::class);
		// $locales = $serviceLanguage->getLanguagesNames();
		$locales = [$serviceLanguage->getDefaultLocale()];
		$serviceEvent = $this->container->get(serviceEvent::class);
		// $serviceContext = new serviceContext($this->container);
		// $this->container->get(serviceContext::class)->__construct($this->container);
		// Refresh cache every 10 minutes
		// start
		$start = new DateTime();

		if(array_intersect($actions, ['server','server-manual'])) {
			// server

			$commands = ['START' => '---> Initialization done for '.$this->getEndHr(true)];
			$services = $this->container->getServiceIds();
			foreach ($services as $serviceName) {
				if(class_exists($serviceName) && method_exists($serviceName, 'sfcron_task')) {
					$service = $this->container->get($serviceName);
					foreach ($service->sfcron_task($start, in_array('server-manual', $actions)) as $name => $result) {
						// $commands[$name] ??= [];
						if(null === $result) $commands[$name][$serviceName] = false;
							else $commands[$name][$serviceName] = $result !== false ? 'Executed for '.$this->getEndHr(true) : '--- ('.$this->getEndHr(true).')';
					}
				}
			}
			$commands['END'] = '---> Total done for '.$this->getEndHr();

			YamlLog::registerYamlLogfile('Server Cron Task > begined at '.$start->format('Y-m-d H:i:s'), true, $commands, static::REPORT_PATH, static::REPORT_FILE);

		} else {
			// Terminal command

			// action "clear" ---> clear cron.yml file
			// Done by server everyday at 00:00
			if(array_intersect($actions, ['clear'])) {
				YamlLog::clearYamlLogfile(static::REPORT_PATH, static::REPORT_FILE);
				if(!@file_exists(static::REPORT_PATH.static::REPORT_FILE)) $output->writeln('Report file has been cleared!');
					else $output->writeln(DevTerminal::ERROR_STYLE.'ERROR: Report file could not be cleared!'.DevTerminal::NO_STYLE);
			}
			// action "calendar" ---> clear and rebuild cache for calendar
			// Done anyway every 10 minutes
			if(array_intersect($actions, ['calendar','cache'])) {
				$serviceEvent->recreateMainsCalendarCaches();
				// $serviceEvent->recreateMainsCalendarCaches();
				YamlLog::registerYamlLogfile('Refresh calendars cache for these languages: '.json_encode($locales), true, 'OK', static::REPORT_PATH, static::REPORT_FILE);
			}
			YamlLog::registerYamlLogfile('Command > begined at '.$start->format('Y-m-d H:i:s'), true, $this->getEndHr(), static::REPORT_PATH, static::REPORT_FILE);
			$output->writeln('Sf cron done at: '.$start->format(YamlLog::DATE_FORMAT).' for these languages: '.json_encode($locales).' and took '.$this->getEndHr());
		}


		// Refresh main menu every minute
		// $this->get(serviceCache::class)->clearEntityCache(Menu::class);

		return $this;
	}

	protected function initHr() {
		$this->hrStart = $this->hrMark = hrtime(true);
	}

	protected function getEndHr($mark = false, $print = 'ms') {
		$end = hrtime(true);
		if(!is_integer($this->hrStart)) throw new Exception("Error ".__METHOD__."(): hrtime start is not initialized!", 1);
		if($mark) {
			$result = $print === 'ms' ?
				number_format(($end/1e+6 - $this->hrMark/1e+6), 0, ',', '').' ms.':
				number_format(($end/1e+6 - $this->hrMark/1e+6) / 1000, 2, ',', '').' sec.';
			$this->hrMark = hrtime(true);
		} else {
			$result = $print === 'ms' ?
				number_format(($end/1e+6 - $this->hrStart/1e+6), 0, ',', '').' ms.':
				number_format(($end/1e+6 - $this->hrStart/1e+6) / 1000, 2, ',', '').' sec.';
		}
		return $result;
	}




}
