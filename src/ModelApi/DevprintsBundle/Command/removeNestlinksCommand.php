<?php

namespace ModelApi\DevprintsBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Classes\DirectoryCollection;
use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceUser;
// use ModelApi\UserBundle\Service\serviceGroup;
// use ModelApi\UserBundle\Service\serviceEntreprise;
// BinaryBundle
use ModelApi\BinaryBundle\Service\serviceBinary;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;

use \Exception;
use \DateTime;

class removeNestlinksCommand extends ContainerAwareCommand {

	// const DATE_FORMAT = 'Y-m-d H:i:s';
	const DATE_FORMAT = DATE_ATOM;

	protected $container;
	protected $cron_tasks_methods;
	protected $cron_tasks;

	protected function configure () {

		// On set le nom de la commande
		// --> $ php bin/console app:check <option>
		$this->setName('directorys:update');

		// On set la description
		$this->setDescription("Transfert des anciens Nestlinks vers nouveau système de dossiers");

		// On set l'aide
		$this->cron_tasks_methods = array_filter(get_class_methods($this), function($name) { return preg_match('/^cron_/', $name); });
		$this->cron_tasks = [];
		foreach ($this->cron_tasks_methods as $key => $cron_task) {
			$this->cron_tasks[$key] = strtolower(preg_replace('/^cron_/i', '', $cron_task));
		}
		$this->setHelp("Lancez cette commande pour transférer les anciens liens Nestlinks dans les dossiers.".PHP_EOL."Après cette opération, il convient de supprimer la table nestlink de la base de données.".PHP_EOL.PHP_EOL);

		// On prépare les arguments
		// $this
			// ->addArgument('name', InputArgument::REQUIRED, "Quel est ton prenom ?")
			// ->addArgument('action', InputArgument::OPTIONAL, "actions: all,clear")
			// ;
	}

	public function execute (InputInterface $input, OutputInterface $output) {
		ini_set('memory_limit','1024M');
		// ini_set('memory_limit','4096M');
		$this->container = $this->getContainer();
		$this->container->get(serviceAnnotation::class)->setModeContext('check');
		$serviceEntities = $this->container->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		$serviceBasedirectory = $this->container->get(serviceBasedirectory::class);
		$serviceItem = $this->container->get(serviceItem::class);

		DevTerminal::EOL(1);
		$mainUserAdmin = $this->container->get(serviceUser::class)->getMainUserAdmin();
		DevTerminal::Info('<T5>Info: main User admin is '.DevTerminal::CLI_LIGHT_CYAN.($mainUserAdmin instanceOf User ? $mainUserAdmin->getUsername() : 'NOT FOUND!!!').DevTerminal::INFO_STYLE.'.');

		$check_transfer = true;
		if($serviceEntities->entityExists('Nestlink')) {
			DevTerminal::Info('<T5>Transfert anciens Nestlink');
			$directorys = $serviceBasedirectory->findAll();
			$total = count($directorys);
			$directorys = array_filter($directorys, function($directory) { return !$directory->isTransited(); });
			DevTerminal::Info('<T5>Nombre de dossiers à traiter: '.count($directorys).' sur '.$total);

			//***************************************************************************************************************
			// Update all Items
			//***************************************************************************************************************

			if(count($directorys) > 0) {
				$nb = 0;
				DevTerminal::progressBar($nb, count($directorys), '---> Chargement des dossiers...', 50, true);
				foreach ($directorys as $directory) {
					// $directory->setDeepControl(true);
					DevTerminal::progressBar($nb++, count($directorys), '---> Mises à jour...', 50, true);
					$directory->init_old_nestlink(false);
				}
				DevTerminal::progressBar($nb, count($directorys), '---> Terminé!', 50, true);
				//***************************************************************************************************************
				// FLUSH ALL
				//***************************************************************************************************************
				DevTerminal::EOL(1);
				DevTerminal::ProgressText('Enregistrement<<<...>>>', null, 500);
				try {
					$em->flush();
					DevTerminal::ProgressText('Enregistrement des transferts terminé !', DevTerminal::SUCCESS_STYLE, 500);
				} catch (Exception $e) {
					DevTerminal::Error(PHP_EOL.'<<<!!!!!>>> Errors while flush : '.$e->getMessage());
				}
			} else {
				$check_transfer = false;
				DevTerminal::Success('<T5>Aucun dossier à mettre à jour : aucun transfert n\'a été nécessaire!');
			}
			$em->clear();
		} else {
			$check_transfer = false;
			DevTerminal::Success('<T5>Le transfert a déjà été effectué et la base de données est à jour. Aucun transfert n\'est nécessaire.');
		}
		DevTerminal::EOL(1);

		// Vérification ROOT Directorys
		DevTerminal::ProgressText('Chargement Directorys<<<...>>>', null, 500);
		$rootDirectorys = $serviceBasedirectory->findRootDirectorys();
		// $rootDirectorys = array_filter($directorys, function($directory) { return $directory->isCompleteOrIncompleteRoot(); });
		$total = count($rootDirectorys);
		DevTerminal::ProgressText('Vérification '.$total.' ROOT Directorys : root and childs...', null, 200);
		DevTerminal::EOL(1);
		foreach (array_values($rootDirectorys) as $key => $rootDirectory) {
			// $rootDirectory->refreshName(false);
			DevTerminal::ProgressText(DevTerminal::LIGHT_CYAN.($key + 1).DevTerminal::CYAN.'/'.$total.'. CHECK '.($rootDirectory->isCompleteOrIncompleteRoot() ? 'ROOT' : DevTerminal::LIGHT_CYAN.'NOT ROOT'.DevTerminal::CYAN).' '.$rootDirectory->getShortname().' '.json_encode($rootDirectory->getName()).' : nested elements... / '.DevTerminal::LOUD_STYLE.DevTerminal::getMemoryUsage(), null, 500);
			DevTerminal::EOL(1);
			// $rootDirectory->checkDirparents();
			// $rootDirectory->checkDirchilds();
			$serviceItem->recomputeItem($rootDirectory);
			// $em->flush();
			// if(!$rootDirectory->checkDirparents()) DevTerminal::launchException('<<<!!!!!>>> Errors: found duplicates dirparents for '.$rootDirectory->getShortname().' '.json_encode($rootDirectory->getName()).'!', __LINE__, __METHOD__);
			// if(!$rootDirectory->checkDirchilds()) DevTerminal::launchException('<<<!!!!!>>> Errors: found duplicates dirchilds for '.$rootDirectory->getShortname().' '.json_encode($rootDirectory->getName()).'!', __LINE__, __METHOD__);
			// DevTerminal::EOL(1);
		}
		DevTerminal::EOL(1);
		DevTerminal::ProgressText('Vérification Dossiers liens uniques: <<<...>>>', DevTerminal::WARN_STYLE, 500);
		$serviceItem->searchSameOwnerdirs($rootDirectorys);
		DevTerminal::ProgressText('Vérification Dossiers liens uniques: CHECKED!', DevTerminal::WARN_STYLE, 500);
		DevTerminal::EOL(1);
		DevTerminal::ProgressText('Enregistrement Dossiers<<<...>>>', DevTerminal::WARN_STYLE, 500);
		try {
			$em->flush();
			DevTerminal::ProgressText('Enregistrement Dossiers : terminé !', DevTerminal::SUCCESS_STYLE, 500);
			// DevTerminal::Warning(' > Recommencez l\'opération svp.');
		} catch (Exception $e) {
			DevTerminal::launchException('<<<!!!!!>>> Errors while flush Directorys : '.$e->getMessage(), __LINE__, __METHOD__);
		}
		$em->clear();
		DevTerminal::EOL(1);

		// Vérification OTHER Directorys
		$otherDirectorys = $serviceBasedirectory->findNotRootDirectorys();
		// $otherDirectorys = array_filter($directorys, function($directory) { return !$directory->isCompleteOrIncompleteRoot(); });
		$total = count($otherDirectorys);
		DevTerminal::ProgressText('Vérification '.$total.' OTHER Directorys : root and childs...', null, 200);
		DevTerminal::EOL(1);
		foreach (array_values($otherDirectorys) as $key => $directory) {
			DevTerminal::ProgressText(DevTerminal::LIGHT_CYAN.($key + 1).DevTerminal::CYAN.'/'.$total.'. CHECK '.($directory->isCompleteOrIncompleteRoot() ? DevTerminal::LIGHT_CYAN.'ROOT'.DevTerminal::CYAN : 'NOT ROOT').' '.$directory->getShortname().' '.json_encode($directory->getName()).' : nested elements... / '.DevTerminal::LOUD_STYLE.DevTerminal::getMemoryUsage(), null, 500);
			DevTerminal::EOL(1);
			// $directory->checkDirparents();
			// $directory->checkDirchilds();
			$serviceItem->recomputeItem($directory);
			// $em->flush();
			// if(!$directory->checkDirparents()) DevTerminal::launchException('<<<!!!!!>>> Errors: found duplicates dirparents for '.$directory->getShortname().' '.json_encode($directory->getName()).'!', __LINE__, __METHOD__);
			// if(!$directory->checkDirchilds()) DevTerminal::launchException('<<<!!!!!>>> Errors: found duplicates dirchilds for '.$directory->getShortname().' '.json_encode($directory->getName()).'!', __LINE__, __METHOD__);
			// DevTerminal::EOL(1);
		}
		// DevTerminal::ProgressText('Vérification Dossiers liens uniques: <<<...>>>', DevTerminal::WARN_STYLE, 500);
		// $serviceItem->searchSameOwnerdirs($otherDirectorys);
		// DevTerminal::ProgressText('Vérification Dossiers liens uniques: CHECKED!', DevTerminal::WARN_STYLE, 500);
		DevTerminal::EOL(1);
		DevTerminal::ProgressText('Enregistrement Dossiers<<<...>>>', DevTerminal::WARN_STYLE, 500);
		try {
			$em->flush();
			DevTerminal::ProgressText('Enregistrement Dossiers : terminé !', DevTerminal::SUCCESS_STYLE, 500);
			// DevTerminal::Warning(' > Recommencez l\'opération svp.');
		} catch (Exception $e) {
			DevTerminal::launchException('<<<!!!!!>>> Errors while flush Directorys : '.$e->getMessage(), __LINE__, __METHOD__);
		}
		$em->clear();
		DevTerminal::EOL(1);

		// Vérification Users
		DevTerminal::ProgressText('Chargement Users<<<...>>>', null, 500);
		$users = $serviceItem->getRepository()->findBy(['shortname' => 'User']);
		$total = count($users);
		DevTerminal::ProgressText('Vérification '.$total.' Users : ...', null, 200);
		DevTerminal::EOL(1);
		foreach (array_values($users) as $key => $user) {
			DevTerminal::ProgressText(DevTerminal::LIGHT_CYAN.($key + 1).DevTerminal::CYAN.'/'.$total.'. CHECK USER '.$user->getShortname().' '.json_encode($user->getName()).' : ... / '.DevTerminal::LOUD_STYLE.DevTerminal::getMemoryUsage(), null, 500);
			DevTerminal::EOL(1);
			$serviceItem->recomputeItem($user);
			// $em->flush();
			// DevTerminal::EOL(1);
		}
		DevTerminal::EOL(1);
		DevTerminal::ProgressText('Enregistrement Users<<<...>>>', DevTerminal::WARN_STYLE, 500);
		try {
			$em->flush();
			DevTerminal::ProgressText('Enregistrement Users : terminé !', DevTerminal::SUCCESS_STYLE, 500);
			// DevTerminal::Warning(' > Recommencez l\'opération svp.');
		} catch (Exception $e) {
			DevTerminal::launchException('<<<!!!!!>>> Errors while flush Users : '.$e->getMessage(), __LINE__, __METHOD__);
		}
		$em->clear();
		DevTerminal::EOL(1);

		// Vérification Items
		DevTerminal::ProgressText('Chargement Items<<<...>>>', null, 500);
		$items = $serviceItem->findAll();
		$items = array_filter($items, function($item) { return !($item instanceOf Basedirectory) && !($item instanceOf User); });
		$total = count($items);
		DevTerminal::ProgressText('Vérification '.$total.' Items : parent <-> child...', null, 200);
		DevTerminal::EOL(1);
		// $checkeditems = [];
		foreach (array_values($items) as $key => $item) {
			DevTerminal::ProgressText(DevTerminal::LIGHT_CYAN.($key + 1).DevTerminal::CYAN.'/'.$total.'. CHECK '.$item->getShortname().' '.json_encode($item->getName()).' : nested elements... / '.DevTerminal::LOUD_STYLE.DevTerminal::getMemoryUsage(), null, 500);
			DevTerminal::EOL(1);
			// $item->checkDirparents();
			// $serviceBasedirectory->checkDirectorys($item);
			$serviceItem->recomputeItem($item);
			// $em->flush();
			// if(!$item->checkDirparents()) DevTerminal::launchException('<<<!!!!!>>> Errors: found duplicates dirparents for '.$item->getShortname().' '.json_encode($item->getName()).'!', __LINE__, __METHOD__);
			// DevTerminal::EOL(1);
		}
		DevTerminal::EOL(1);
		DevTerminal::Info('Enregistrement Items<<<...>>>', DevTerminal::WARN_STYLE, 500);
		try {
			$em->flush();
			DevTerminal::ProgressText('Enregistrement Items : terminé !', DevTerminal::SUCCESS_STYLE, 500);
			// DevTerminal::Warning(' > Recommencez l\'opération svp.');
		} catch (Exception $e) {
			DevTerminal::launchException('<<<!!!!!>>> Errors while flush : '.$e->getMessage(), __LINE__, __METHOD__);
		}
		$em->clear();
		DevTerminal::EOL(1);

		// // Vérification Items schema directorys
		// DevTerminal::ProgressText('Chargement Items<<<...>>>', null, 500);
		// $items = $serviceItem->findAll();
		// $items = array_values(array_filter($items, function($item) { return !($item instanceOf Basedirectory); }));
		// $total = count($items);
		// DevTerminal::ProgressText('Vérification '.$total.' Items : check schema directorys...', null, 200);
		// DevTerminal::EOL(1);
		// // $checkeditems = [];
		// foreach ($items as $key => $item) {
		// 	DevTerminal::ProgressText(DevTerminal::LIGHT_CYAN.($key + 1).DevTerminal::CYAN.'. CHECK '.$item->getShortname().' '.json_encode($item->getName()).' : schemas...', null, 500);
		// 	DevTerminal::EOL(1);
		// 	$serviceBasedirectory->checkDirectorys($item);
		// 	// $serviceItem->recomputeItem($item);
		// 	DevTerminal::EOL(1);
		// }
		// DevTerminal::EOL(1);
		// DevTerminal::Info('Enregistrement Items<<<...>>>', DevTerminal::WARN_STYLE, 500);
		// try {
		// 	$em->flush();
		// 	DevTerminal::ProgressText('Enregistrement Items : terminé !', DevTerminal::SUCCESS_STYLE, 500);
		// 	// DevTerminal::Warning(' > Recommencez l\'opération svp.');
		// } catch (Exception $e) {
		// 	DevTerminal::launchException('<<<!!!!!>>> Errors while flush : '.$e->getMessage(), __LINE__, __METHOD__);
		// }
		// $em->clear();

		DevTerminal::EOL(1);
		if($check_transfer) {
			DevTerminal::ProgressText('Vérification finale<<<...>>>', null, 1000);
			$directorys = $serviceBasedirectory->findAll();
			$directorys = array_filter($directorys, function($directory) { return !$directory->isTransited(); });
			if(count($directorys)) {
				DevTerminal::ProgressText('Nombre de transferts encore non traités: '.count($directorys).' sur '.$total.'! Veuillez relancer le processus, svp.', DevTerminal::ERROR_STYLE, 500);
				DevTerminal::EOL(2);
			} else {
				DevTerminal::ProgressText('Vérification correcte. Tous les transferts sont traités. Merci!', DevTerminal::SUCCESS_STYLE, 500);
				DevTerminal::EOL(2);
			}
		} else {
			DevTerminal::ProgressText('Vérification correcte. Tous les éléments sont traités. Merci!', DevTerminal::SUCCESS_STYLE, 500);
			DevTerminal::EOL(2);
		}
		return $this;
	}






}
