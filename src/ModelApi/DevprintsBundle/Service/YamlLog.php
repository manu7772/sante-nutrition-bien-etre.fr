<?php
namespace ModelApi\DevprintsBundle\Service;

use Symfony\Component\Yaml\Yaml;
// use Symfony\Component\Yaml\Parser;
// use Symfony\Component\Yaml\Dumper;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\servicesBaseInterface;

use ModelApi\DevprintsBundle\Command\SfcronCommand;

use \DateTime;

class YamlLog implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const BASE_PROJECT = __DIR__.'/../../../..';
	const REPORT_PATH = '/var/logs/';
	const REPORT_FILE = 'aew_logs.yml';
	const DATE_FORMAT = 'Y-m-d H:i:s';

	/*************************************************************************************************/
	/*** SFCRON TASK
	/*************************************************************************************************/

	public static function sfcron_task(DateTime $start, $force = false) {
		// if($force || preg_match('/0000$/', $start->format('Hi'))) {
		if(preg_match('/0000$/', $start->format('Hi'))) {
			static::clearYamlLogfile(SfcronCommand::REPORT_PATH, SfcronCommand::REPORT_FILE);
			return ['Clear sfcron yml file reports' => true];
		}
		return ['Clear sfcron yml file reports' => false];
	}




	public static function getLogfilePath(&$reportPath = null) {
		if(!is_string($reportPath)) {
			$reportPath = static::BASE_PROJECT.static::REPORT_PATH;
		} else if(preg_match('#^\\/#', $reportPath)) {
			$reportPath = static::BASE_PROJECT.$reportPath;
		}
		if(!preg_match('#\\/$#', $reportPath)) $reportPath .= DIRECTORY_SEPARATOR;
		return $reportPath;
	}

	public static function clearYamlLogfile($reportPath = null, $reportFile = null) {
		// if(!is_string($reportFile)) $reportFile = static::REPORT_FILE;
		static::getLogfilePath($reportPath);
		serviceTools::removeFile($reportPath, $reportFile);
		serviceTools::appendToFile($reportPath, $reportFile, '');
	}

	// public static function checkYamlLogFile($reportPath, $reportFile) {
	// 	// if(!is_string($reportFile)) $reportFile = static::REPORT_FILE;
	// 	// Create YML file
	// 	if(!file_exists(static::REPORT_PATH.$reportFile)) serviceTools::appendToFile(static::REPORT_PATH, $reportFile, '');
	// }

	protected static function createYamlLine($operation, $result, $data) {
		$date = new DateTime();
		$date = $date->format(static::DATE_FORMAT);
		$data_for_yaml = [
			'date' => $date,
			'operation' => $operation,
			'result' => $result,
			'data' => $data,
		];
		return $data_for_yaml;
	}

	public static function registerYamlLogfile($operation, $result, $data, $reportPath = null, $reportFile = null) {
		if(!is_string($reportFile)) $reportFile = static::REPORT_FILE;
		static::getLogfilePath($reportPath);
		// static::checkYamlLogFile($reportPath, $reportFile);
		$data_for_yaml = static::createYamlLine($operation, $result, $data);
		$date = new DateTime();
		$yaml = Yaml::dump([$date->format(static::DATE_FORMAT).'@'.uniqid('aew_logs-', true) => $data_for_yaml], 4, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
		serviceTools::appendToFile($reportPath, $reportFile, $yaml);
	}

	public static function directYamlLogfile($data, $reportPath = null, $reportFile = null) {
		if(!is_string($reportFile)) $reportFile = static::REPORT_FILE;
		static::getLogfilePath($reportPath);
		$date = new DateTime();
		$yaml = Yaml::dump([$date->getTimestamp() => $data], 4, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
		serviceTools::appendToFile($reportPath, $reportFile, $yaml);
	}


}