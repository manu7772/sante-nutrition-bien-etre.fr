<?php
namespace ModelApi\DevprintsBundle\Service;

use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolation;

// BaseBundle
use ModelApi\BaseBundle\Entity\Reseausocial;
use ModelApi\BaseBundle\Entity\Telephon;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\OwnerTierInterface;

use \Exception;

class DevTerminal implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const DELAY_RATIO = 0.00;

	// TABS
	const PROG_TAB_3 = "\033[4G\033[2K";
	const PROG_TAB_5 = "\033[6G\033[2K";
	const PROG_TAB_7 = "\033[8G\033[2K";
	const PROG_TAB_8 = "\033[9G\033[2K";
	// NAMED COLORS
	const NO_STYLE = "\033[0;3m";
	const ERROR_STYLE = "\033[0;31m";
	const WARN_STYLE = "\033[0;33m";
	const SUCCESS_STYLE = "\033[0;32m";
	const INFO_STYLE = "\033[0;36m";
	const LOUD_STYLE = "\033[1;30m";

	// FG COLORS
	const BLACK = "\033[0;30m";
	const DARK_GRAY = "\033[1;30m";
	const BLUE = "\033[0;34m";
	const LIGHT_BLUE = "\033[1;34m";
	const GREEN = "\033[0;32m";
	const LIGHT_GREEN = "\033[1;32m";
	const CYAN = "\033[0;36m";
	const LIGHT_CYAN = "\033[1;36m";
	const RED = "\033[0;31m";
	const LIGHT_RED = "\033[1;31m";
	const PURPLE = "\033[0;35m";
	const LIGHT_PURPLE = "\033[1;35m";
	const BROWN = "\033[0;33m";
	const YELLOW = "\033[1;33m";
	const LIGHT_GRAY = "\033[0;3m";
	const WHITE = "\033[1;3m";
	// FG COLORS CLIGNOTANT
	const CLI_BLACK = "\033[0;30;5m";
	const CLI_DARK_GRAY = "\033[1;30;5m";
	const CLI_BLUE = "\033[0;34;5m";
	const CLI_LIGHT_BLUE = "\033[1;34;5m";
	const CLI_GREEN = "\033[0;32;5m";
	const CLI_LIGHT_GREEN = "\033[1;32;5m";
	const CLI_CYAN = "\033[0;36;5m";
	const CLI_LIGHT_CYAN = "\033[1;36;5m";
	const CLI_RED = "\033[0;31;5m";
	const CLI_LIGHT_RED = "\033[1;31;5m";
	const CLI_PURPLE = "\033[0;35;5m";
	const CLI_LIGHT_PURPLE = "\033[1;35;5m";
	const CLI_BROWN = "\033[0;33;5m";
	const CLI_YELLOW = "\033[1;33;5m";
	const CLI_LIGHT_GRAY = "\033[0;37;5m";
	const CLI_WHITE = "\033[1;3;5m";
	//  Progress Bar Color
	const PROGRESS_BAR_COLOR = "\033[1;32m";
	// BG COLORS
	const BG_BLACK = "\033[40m";
	const BG_RED = "\033[41m";
	const BG_GREEN = "\033[42m";
	const BG_YELLOW = "\033[43m";
	const BG_BLUE = "\033[44m";
	const BG_MAGENTA = "\033[45m";
	const BG_CYAN = "\033[46m";
	const BG_LIGHT_GRAY = "\033[4m";
	// BG COLORS CLIGNOTANT
	const CLI_BG_BLACK = "\033[40;5m";
	const CLI_BG_RED = "\033[41;5m";
	const CLI_BG_GREEN = "\033[42;5m";
	const CLI_BG_YELLOW = "\033[43;5m";
	const CLI_BG_BLUE = "\033[44;5m";
	const CLI_BG_MAGENTA = "\033[45;5m";
	const CLI_BG_CYAN = "\033[46;5m";
	const CLI_BG_LIGHT_GRAY = "\033[4;5m";

	const REPLACEMENTS = [
		'/<<</', // cli same color
		'/<S</', // static::LIGHT_GREEN
		'/<E</', // static::LIGHT_RED
		'/<@</', // static::CLI_LIGHT_GREEN
		'/<#</', // static::CLI_LIGHT_RED
		'/>>>/', // end cli > returns to normal style
		'/<T8>/i',
		'/<T7>/i',
		'/<T5>/i',
		'/<T3>/i',
	];

	public static function getMemoryUsage($prefix = 'PHP memory') {
		$m_usage = floor(memory_get_usage() / 1024 / 1024);
		$m_limit = intval(ini_get('memory_limit'));
		$m_ratio = $m_limit > 0 ? floor(($m_usage / $m_limit) * 100)."%% : " : "";
		$m_limit = $m_limit < 0 ? floor(memory_get_peak_usage(true) / 1024 / 1024).'Mo/unlimited' : floor($m_limit).'Mo';
		return $prefix." ".$m_ratio.$m_usage."Mo (max.".$m_limit.")";
	}

	public static function progressBar($done, $total, $title, $lenght = 50, $printMemory = false, $style = null, $spacer = 32) {
		$style ??= static::PROGRESS_BAR_COLOR;
		// https://stackoverflow.com/questions/17769041/notice-use-of-undefined-constant-stdout-assumed-stdout
		$perc = floor(($done / $total) * 100);
		$ratio = floor(($done / $total) * $lenght);
		$left = $lenght - $ratio;
		$printMemory = $printMemory ? ' / '.static::getMemoryUsage() : '';
		$write = sprintf(static::PROG_TAB_5."$style%-{$spacer}s[%'={$ratio}s>%-{$left}s] $perc%%".static::DARK_GRAY." $done/$total$printMemory".static::NO_STYLE, $title, "", "");
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, $write);
	}

	public static function ms_sleep($milliseconds = 0) {
		if($milliseconds > 0) {
			$test = $milliseconds / 1000;
			$seconds = floor($test);
			$micro = round(($test - $seconds) * 1000000);
			if($seconds > 0) sleep($seconds);
			if($micro > 0) usleep($micro);
		}
	}

	public static function transformCliText(&$text, $color) {
		$cli_color = preg_match('/;5m$/', $color) ? $color : preg_replace('/(m)$/', ';5m', $color);
		if($cli_color === $color) $cli_color = $color = '';
		$text = preg_replace(static::REPLACEMENTS, [$cli_color, static::LIGHT_GREEN, static::LIGHT_RED, static::CLI_LIGHT_GREEN, static::CLI_LIGHT_RED, $color, static::PROG_TAB_8, static::PROG_TAB_7, static::PROG_TAB_5, static::PROG_TAB_3], $text); // preg_quote() ???
		return $text;
	}

	public static function ProgressText($text, $color = null, $timing_millisecondes = 0) {
		$color ??= static::NO_STYLE;
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, $color.static::PROG_TAB_5.static::transformCliText($text, $color).static::NO_STYLE);
		static::ms_sleep(round($timing_millisecondes * static::DELAY_RATIO));
	}

	public static function ProgressSuccess($text, $color = null, $timing_millisecondes = 0) {
		$color ??= static::SUCCESS_STYLE;
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, $color.static::PROG_TAB_5.static::transformCliText($text, $color).static::NO_STYLE);
		static::ms_sleep(round($timing_millisecondes * static::DELAY_RATIO));
	}

	public static function ProgressWarning($text, $color = null, $timing_millisecondes = 0) {
		$color ??= static::WARN_STYLE;
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, $color.static::PROG_TAB_5.static::transformCliText($text, $color).static::NO_STYLE);
		static::ms_sleep(round($timing_millisecondes * static::DELAY_RATIO));
	}

	public static function ProgressError($text, $color = null, $timing_millisecondes = 0) {
		$color ??= static::ERROR_STYLE;
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, $color.static::PROG_TAB_5.static::transformCliText($text, $color).static::NO_STYLE);
		static::ms_sleep(round($timing_millisecondes * static::DELAY_RATIO));
	}

	public static function Errors($text, $errors) {
		if(!defined('STDERR')) define('STDERR', fopen('php://stderr', 'wb'));
		fwrite(STDERR, static::ERROR_STYLE.static::PROG_TAB_3.static::transformCliText($text, static::ERROR_STYLE).PHP_EOL);
		foreach ($errors as $error) print(static::PROG_TAB_3."- ".$error.PHP_EOL);
		fwrite(STDERR, static::NO_STYLE.PHP_EOL);
		// static::EOL();
	}

	public static function Error($text, $EOL = true) {
		if(!defined('STDERR')) define('STDERR', fopen('php://stderr', 'wb'));
		fwrite(STDERR, static::ERROR_STYLE.static::transformCliText($text, static::ERROR_STYLE).static::NO_STYLE.static::getEOL($EOL));
	}

	public static function ErrorPlus($text, $EOL = true) {
		if(!defined('STDERR')) define('STDERR', fopen('php://stderr', 'wb'));
		fwrite(STDERR, static::LIGHT_RED.static::transformCliText($text, static::LIGHT_RED).static::NO_STYLE.static::getEOL($EOL));
		// static::ms_sleep(1000);
	}

	public static function ErrorPlusPlus($text, $EOL = 2) {
		if(!defined('STDERR')) define('STDERR', fopen('php://stderr', 'wb'));
		fwrite(STDERR, static::BG_RED.PHP_EOL.PHP_EOL.static::transformCliText($text, static::BG_RED).PHP_EOL.static::NO_STYLE.static::getEOL($EOL));
		// static::ms_sleep(1000);
	}

	public static function Warning($text, $EOL = true) {
		if(!defined('STDERR')) define('STDERR', fopen('php://stderr', 'wb'));
		fwrite(STDERR, static::WARN_STYLE.static::transformCliText($text, static::WARN_STYLE).static::NO_STYLE.static::getEOL($EOL));
	}

	public static function Loud($text, $EOL = true) {
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, static::LOUD_STYLE.static::transformCliText($text, static::LOUD_STYLE).static::NO_STYLE.static::getEOL($EOL));
	}

	public static function Info($text, $EOL = true) {
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, static::INFO_STYLE.static::transformCliText($text, static::INFO_STYLE).static::NO_STYLE.static::getEOL($EOL));
	}

	public static function Success($text, $EOL = true) {
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, static::SUCCESS_STYLE.static::transformCliText($text, static::SUCCESS_STYLE).static::NO_STYLE.static::getEOL($EOL));
	}

	public static function SuccessPlus($text, $EOL = true) {
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, static::LIGHT_GREEN.static::transformCliText($text, static::LIGHT_GREEN).static::NO_STYLE.static::getEOL($EOL));
		// static::ms_sleep(100);
	}

	public static function Debug($text, $EOL = true) {
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, static::SUCCESS_STYLE.static::transformCliText($text, static::SUCCESS_STYLE).static::NO_STYLE.static::getEOL($EOL));
	}

	public static function FixturesText($text, $EOL = true) {
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, static::PROG_TAB_5.static::SUCCESS_STYLE.'- '.static::transformCliText($text, static::SUCCESS_STYLE).static::NO_STYLE.static::getEOL($EOL));
	}

	public static function FixturesWarning($text, $EOL = true) {
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, static::PROG_TAB_5.static::SUCCESS_STYLE.'- '.static::WARN_STYLE.static::transformCliText($text, static::SUCCESS_STYLE).static::NO_STYLE.static::getEOL($EOL));
	}

	public static function FixturesError($text, $EOL = true, $die = false) {
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, static::PROG_TAB_5.static::SUCCESS_STYLE.'- '.static::ERROR_STYLE.static::transformCliText($text, static::ERROR_STYLE).static::NO_STYLE.static::getEOL($EOL));
		if($die) die();
	}

	public static function FixturesMain($text, $EOL = true) {
		if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
		fwrite(STDOUT, static::PROG_TAB_3.static::WARN_STYLE.'> '.static::SUCCESS_STYLE.static::transformCliText($text, static::SUCCESS_STYLE).static::NO_STYLE.static::getEOL($EOL));
	}

	public static function getEOL(&$EOL) {
		if(is_bool($EOL)) return $EOL = $EOL ? PHP_EOL : '';
		if(is_integer($EOL) && $EOL >= 0) return $EOL = implode('', array_fill(0, $EOL, PHP_EOL));
		return $EOL = '';
	}

	public static function EOL($number = 1) {
		print(static::getEOL($number));
	}

	/********************************************/
	/*** CONSOLE
	/********************************************/

	public static function console($title, $data = null, $type = 'debug') {
		if(null === $data) {
			echo('<script type="text/javascript">console.'.$type.'("'.$title.'")</script>'.PHP_EOL);
		} else {
			echo('<script type="text/javascript">var data = '.json_encode($data).'; console.'.$type.'("'.$title.'", data)</script>'.PHP_EOL);
		}
	}



	/********************************************/
	/*** COLORS
	/*** https://www.if-not-true-then-false.com/2010/php-class-for-coloring-php-command-line-cli-scripts-output-php-output-colorizing-using-bash-shell-colors/
	/********************************************/

	public static function getColors() {
		return [
			'foreground'  => [
				'black' => '0;30',
				'dark_gray' => '1;30',
				'blue' => '0;34',
				'light_blue' => '1;34',
				'green' => '0;32',
				'light_green' => '1;32',
				'cyan' => '0;36',
				'light_cyan' => '1;36',
				'red' => '0;31',
				'light_red' => '1;31',
				'purple' => '0;35',
				'light_purple' => '1;35',
				'brown' => '0;33',
				'yellow' => '1;33',
				'light_gray' => '0;37',
				'white' => '1;37'
			],
			'background' => [
				'black' => '40',
				'red' => '41',
				'green' => '42',
				'yellow' => '43',
				'blue' => '44',
				'magenta' => '45',
				'cyan' => '46',
				'light_gray' => '47'
			]
		];
	}

	// Returns all foreground color names
	public static function getForegroundColors() {
		$colors = static::getColors();
		return array_keys($colors['foreground']);
	}

	// Returns all background color names
	public static function getBackgroundColors() {
		$colors = static::getColors();
		return array_keys($colors['background']);
	}

	// Returns colored string
	public static function getColoredString($string, $foreground_color = null, $background_color = null, $EOL = false) {
		$colored_string = "";
		$colors = static::getColors();
		if (isset($colors['foreground'][$foreground_color])) $colored_string .= "\033[".$colors['foreground'][$foreground_color]."m";
		if (isset($colors['background'][$background_color])) $colored_string .= "\033[".$colors['background'][$background_color]."m";
		return $colored_string.$string.static::NO_STYLE.static::getEOL($EOL);
	}

	public static function launchException($message = null, $line = null, $method = null, $addTerminalText = true, $backtrace = 20, $exception = true) {
		if($backtrace !== false) {
			static::EOL(2);
			$trace = array_reverse(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, is_integer($backtrace) ? $backtrace : 0));
			$cpt = count($trace) - 1;
			foreach ($trace as $num => $trace) {
				static::Info('<T3>'.$cpt--.'. Trace '.$num.' / line '.($trace['line'] ?? '?').' => '.($trace['class'] ?? '??class?? ').($trace['type'] ?? '??type?? ').($trace['function'] ?? '??function?? '));
			}
		}
		$line ??= __LINE__;
		$method ??= __METHOD__;
		$message ??= 'Sorry man, you will have to program something to resolve this big problem...';
		if($addTerminalText) static::Error(PHP_EOL.static::LIGHT_RED.'<T3>STOP line '.$line.' in '.$method.' :'.PHP_EOL.'<T3>'.$message.'>>>'.PHP_EOL, null, 3000);
		$message = preg_replace(static::REPLACEMENTS, array_fill(0, count(static::REPLACEMENTS), ''), $message);
		if($exception) throw new Exception('EXCEPTION line '.$line.' in '.$method.' :'.PHP_EOL.$message, 1);
	}

	// public static function launchValidateException($message, ConstraintViolationList $errors, $line = null, $method = null, $addTerminalText = true, $backtrace = 20) {
	// 	$line ??= __LINE__;
	// 	$method ??= __METHOD__;
	// 	static::launchException($message, $line, $method, $addTerminalText, $backtrace, false);
	// 	static::Errors($message, $errors);
	// 	throw new Exception('EXCEPTION line '.$line.' in '.$method.' :'.PHP_EOL.$message.' ===> '.$errors, 1);
	// }

	public static function launchEntityValidateException(Item $item, $message, ConstraintViolationList $errors = null, $line = null, $method = null, $addTerminalText = true, $backtrace = 20) {
		$line ??= __LINE__;
		$method ??= __METHOD__;
		static::launchException($message, $line, $method, $addTerminalText, $backtrace, false);
		if(!empty($errors)) static::Errors($message, $errors);
		// static::validityEntityReport($item);
		$item->printItem();
		throw new Exception('EXCEPTION line '.$line.' in '.$method.' :'.PHP_EOL.$message.(!empty($errors) ? ' ===> '.$errors : ''), 1);
	}

	public static function contextException(Item $item, $result, $messages, ConstraintViolationList $errors = null, $line = null, $method = null, $backtrace = 50) {
		$line ??= __LINE__;
		$method ??= __METHOD__;
		if(is_string($messages)) $messages = [($result ? 'success' : 'error') => $messages];
		$item_name = method_exists($item, 'getNameOrTempname') ? $item->getNameOrTempname() : $item->getName();
		if(!$item->isDefaultModeContext()) {
			if($result) {
				static::Info('<T5>- On '.$item->getShortname().' '.json_encode($item_name).' Id#'.json_encode($item->getId()).': '.$messages['success'].PHP_EOL.static::LOUD_STYLE.'<T7>line '.$line.' '.$method.'()');
			} else {
				static::launchEntityValidateException($item, '- On '.$item->getShortname().' '.json_encode($item_name).': '.$messages['error'], $errors, $line, $method, true, $backtrace);
			}
		} else {
			$errorstr = [];
			foreach ($errors ?? [] as $error) $errorstr[] = $error.'';
			throw new Exception('Error line '.$line.' '.$method.'(): on '.$item->getShortname().' '.json_encode($item_name).' Id#'.json_encode($item->getId()).' : '.$messages['error'].PHP_EOL.implode(PHP_EOL, $errorstr), 1);
		}
	}

	public static function validationException(Item $item, $message = null, $line = null, $method = null, $lauchException = true) {
		$line ??= __LINE__;
		$method ??= __METHOD__;
		$constraintViolationList = $item->getConstraintViolationList();
		if($constraintViolationList->count() > 0 && $lauchException) {
			$message ??= 'Errors list ('.$constraintViolationList->count().'):';
			static::ErrorPlusPlus("<T3>On entity ".$item->getShortname()." ".json_encode($item->getName())." ".(empty($item->getId()) ? '(created)' : '(updated)')."".PHP_EOL.'<T3>'.$message);
			$key = 1;
			foreach ($constraintViolationList->checklist as $property_name => $data) {
				$type = 'Info';
				if($data['constraintViolation'] instanceOf ConstraintViolation) {
					$type = 'ErrorPlus';
					$new_line = '<T5>'.$key++.'. '.$data['label'].' => message : '.$data['constraintViolation']->getMessage().' ('.json_encode($data['constraintViolation']->getPropertyPath()).')';
				} else {
					$new_line = '<T5>'.$key++.'. '.$data['label'].' => '.static::SUCCESS_STYLE.'VALID'.static::INFO_STYLE;
				}
				if(is_object($data['value'])) {
					if(is_iterable($data['value'])) {
						$value = '['.get_class($data['value']).'] (items: '.count($data['value']).')';
					} else {
						$value = '['.get_class($data['value']).']'.(method_exists($data['value'], 'getName') ? ' '.json_encode($data['value']->getName()) : '');
					}
				} else {
					if(is_iterable($data['value'])) {
						$value = '['.gettype($data['value']).'] (items: '.count($data['value']).')';
					} else {
						$value = '['.gettype($data['value']).'] '.json_encode($data['value']);
					}					
				}
				static::$type($new_line.' > '.json_encode($property_name).' value ('.(true === $data['getter'] ? 'P' : 'G').') = '.$value.'.');
			}
			if($lauchException) throw new Exception("Error line ".$line." on ".$method."(): ".PHP_EOL."On entity ".$item->getShortname()." ".json_encode($item->getName())." ".(empty($item->getId()) ? '(created)' : '(updated)')."".PHP_EOL.$message, 1);
		}
	}


	public static function validityEntityReport(Item $item) {
		$dc = $item->isDeepControl();
		$item->setDeepControl(false);
		$item_ownerdir = $item->getOwnerdir();
		$item_ownerdirs = $item->getOwnerdirs();
		$item_parent_ownerdir = empty($item->getParent()) ? null : $item->getParent()->getOwnerdir();
		static::Info('<T5>Validity of '.($item->isCompleteOrIncompleteRoot() ? 'ROOT ': '').$item->getShortname().' '.json_encode($item->getName()).':');
		static::Info('<T5>---------------------------------------');
		if($item instanceOf Basedirectory) {
			if($item->isCompleteOrIncompleteRoot()) {
				static::Info('<T5>- Id is '.json_encode($item->getId()));
				$call = $item->isValidName() ? 'Info' : 'Error';
				static::$call('<T5>- Name is '.json_encode($item->getName()));
				$call = !empty($item->getInputname()) ? 'Info' : 'Error';
				static::$call('<T5>- Inputname is '.json_encode($item->getInputname()));
				$call = $item->isValidRoot() ? 'Info' : 'Error';
				static::$call('<T5>- Is root '.json_encode($item->getRoot()));
				$call = $item->getOrphan() ? 'Info' : 'Error';
				static::$call('<T5>- Is orphan '.json_encode($item->getOrphan()));
				$call = $item->getLvl() <= 0 ? 'Info' : 'Error';
				static::$call('<T5>- Level is '.json_encode($item->getLvl()));
				$call = $item->isValidOwnerAndParent() ? 'Info' : 'Error';
				static::$call('<T5>- Owner is '.(!empty($item->getOwner()) ? $item->getOwner()->getShortname().' '.json_encode($item->getOwner()->getName()) : json_encode(null)));
				$call = $item_ownerdir instanceOf Item ? 'Info' : 'Error';
				static::$call('<T5>- Ownerdir is '.(!empty($item_ownerdir) ? $item_ownerdir->getShortname().' '.json_encode($item_ownerdir->getName()) : json_encode(null)));
				$call = $item->isValidOwnerAndParent() ? 'Info' : 'Error';
				$parentOwner = '';
				if(!empty($item->getParent())) {
					$parentOwner = !empty($item_parent_ownerdir) ? '(Ownerdir of parent is '.$item_parent_ownerdir->getShortname().' '.json_encode($item_parent_ownerdir->getName()).'' : '(Ownerdir of parent is NULL';
					$parentOwner .= !empty($item->getParent()->getOwner()) ? ' / Owner of parent is '.$item->getParent()->getOwner()->getShortname().' '.json_encode($item->getParent()->getOwner()->getName()).')' : ' / Owner of parent is NULL)';
				}
				static::$call('<T5>- Parent is '.(!empty($item->getParent()) ? $item->getParent()->getShortname().' '.json_encode($item->getParent()->getName()).$parentOwner : json_encode(null)));
				$call = $item->getDirparents()->isEmpty() ? 'Info' : 'Error';
				static::$call('<T5>- Dirparents are '.$item->getDirparents()->count());
				$call = $item->getRootDirs()->isEmpty() ? 'Info' : 'Error';
				static::$call('<T5>- System directorys is '.($item->getRootDirs()->isEmpty() ? 'empty' : 'not empty'));
				$call = $item_ownerdirs->count() === 1 ? 'Info' : 'Error';
				static::Info('<T5>----- Root owner dirs for '.$item->getShortname());
				static::$call('<T5>- Owner System dir is '.(!empty($item->getOwnersystemdir()) ? $item->getOwnersystemdir()->getShortname().' '.json_encode($item->getOwnersystemdir()->getName()) : 'empty'));
				static::$call('<T5>- Owner Drive dir is '.(!empty($item->getOwnerdrivedir()) ? $item->getOwnerdrivedir()->getShortname().' '.json_encode($item->getOwnerdrivedir()->getName()) : 'empty'));
			} else {
				static::Info('<T5>- Id is '.json_encode($item->getId()));
				$call = $item->isValidName() ? 'Info' : 'Error';
				static::$call('<T5>- Name is '.json_encode($item->getName()));
				$call = !empty($item->getInputname()) ? 'Info' : 'Error';
				static::$call('<T5>- Inputname is '.json_encode($item->getInputname()));
				$call = $item->isValidRoot() ? 'Info' : 'Error';
				static::$call('<T5>- Is root '.json_encode($item->getRoot()));
				$call = !$item->getOrphan() ? 'Info' : 'Error';
				static::$call('<T5>- Is orphan '.json_encode($item->getOrphan()));
				$call = $item->getLvl() > 0 ? 'Info' : 'Error';
				static::$call('<T5>- Level is '.json_encode($item->getLvl()));
				$call = $item->isValidOwnerAndParent() ? 'Info' : 'Error';
				static::$call('<T5>- Owner is '.(!empty($item->getOwner()) ? $item->getOwner()->getShortname().' '.json_encode($item->getOwner()->getName()) : json_encode(null)));
				$call = $item_ownerdir instanceOf Item ? 'Info' : 'Error';
				static::$call('<T5>- Ownerdir is '.(!empty($item_ownerdir) ? $item_ownerdir->getShortname().' '.json_encode($item_ownerdir->getName()) : json_encode(null)));
				$call = $item->isValidOwnerAndParent() ? 'Info' : 'Error';
				$parentOwner = '';
				if(!empty($item->getParent())) {
					$parentOwner = !empty($item_parent_ownerdir) ? '(Ownerdir of parent is '.$item_parent_ownerdir->getShortname().' '.json_encode($item_parent_ownerdir->getName()).'' : '(Ownerdir of parent is NULL';
					$parentOwner .= !empty($item->getParent()->getOwner()) ? ' / Owner of parent is '.$item->getParent()->getOwner()->getShortname().' '.json_encode($item->getParent()->getOwner()->getName()).')' : ' / Owner of parent is NULL)';
				}
				static::$call('<T5>- Parent is '.(!empty($item->getParent()) ? $item->getParent()->getShortname().' '.json_encode($item->getParent()->getName()).$parentOwner : json_encode(null)).(!$item->canHaveParent() ? ' ('.$item->getShortname().' with NO parent)' : ''));
				$call ='Info';
				static::$call('<T5>- Dirparents are '.$item->getDirparents()->count());
				$call = $item->getRootDirs()->isEmpty() ? 'Info' : 'Error';
				static::$call('<T5>- System directorys is '.($item->getRootDirs()->isEmpty() ? 'empty' : 'not empty'));
				static::Info('<T5>----- Root owner dirs for '.$item->getShortname());
				$call = $item_ownerdirs->count() === 0 ? 'Info' : 'Error';
				static::$call('<T5>- Owner System dir is '.(!empty($item->getOwnersystemdir()) ? $item->getOwnersystemdir()->getShortname().' '.json_encode($item->getOwnersystemdir()->getName()) : 'empty'));
				static::$call('<T5>- Owner Drive dir is '.(!empty($item->getOwnerdrivedir()) ? $item->getOwnerdrivedir()->getShortname().' '.json_encode($item->getOwnerdrivedir()->getName()) : 'empty'));
			}
		} else if($item instanceOf User) {
			static::Info('<T5>- Id is '.json_encode($item->getId()));
			$call = $item->isValidName() ? 'Info' : 'Error';
			static::$call('<T5>- Name is '.json_encode($item->getName()));
			$call = !empty($item->getInputname()) ? 'Info' : 'Error';
			static::$call('<T5>- Inputname is '.json_encode($item->getInputname()));
			$call = $item->isValidRoot() ? 'Info' : 'Error';
			static::$call('<T5>- Is root '.json_encode($item->getRoot()));
			$call = 'Info';
			static::$call('<T5>- Is orphan '.json_encode($item->getOrphan()));
			$call = $item->getLvl() === 0 ? 'Info' : 'Error';
			static::$call('<T5>- Level is '.json_encode($item->getLvl()));
			$call = $item->isValidOwnerAndParent() ? 'Info' : 'Error';
			static::$call('<T5>- Owner is '.(!empty($item->getOwner()) ? $item->getOwner()->getShortname().' '.json_encode($item->getOwner()->getName()) : json_encode(null)));
			$call = empty($item_ownerdir) ? 'Info' : 'Error';
			static::$call('<T5>- Ownerdir is '.(!empty($item_ownerdir) ? $item_ownerdir->getShortname().' '.json_encode($item_ownerdir->getName()) : json_encode(null)));
			$call = $item->isValidOwnerAndParent() ? 'Info' : 'Error';
			$parentOwner = '';
			if(!empty($item->getParent())) {
				$parentOwner = !empty($item_parent_ownerdir) ? '(Ownerdir of parent is '.$item_parent_ownerdir->getShortname().' '.json_encode($item_parent_ownerdir->getName()).'' : '(Ownerdir of parent is NULL';
				$parentOwner .= !empty($item->getParent()->getOwner()) ? ' / Owner of parent is '.$item->getParent()->getOwner()->getShortname().' '.json_encode($item->getParent()->getOwner()->getName()).')' : ' / Owner of parent is NULL)';
			}
			static::$call('<T5>- Parent is '.(!empty($item->getParent()) ? $item->getParent()->getShortname().' '.json_encode($item->getParent()->getName()).$parentOwner : json_encode(null)).(!$item->canHaveParent() ? ' ('.$item->getShortname().' with NO parent)' : ''));
			$call ='Info';
			static::$call('<T5>- Dirparents are '.$item->getDirparents()->count());
		} else {
			static::Info('<T5>- Id is '.json_encode($item->getId()));
			$call = $item->isValidName() ? 'Info' : 'Error';
			static::$call('<T5>- Name is '.json_encode($item->getName()));
			$call = !empty($item->getInputname()) ? 'Info' : 'Error';
			static::$call('<T5>- Inputname is '.json_encode($item->getInputname()));
			$call = $item->isValidRoot() ? 'Info' : 'Error';
			static::$call('<T5>- Is root '.json_encode($item->getRoot()));
			$call = !$item->getOrphan() ? 'Info' : 'Error';
			static::$call('<T5>- Is orphan '.json_encode($item->getOrphan()));
			$call = $item->getLvl() > 0 ? 'Info' : 'Error';
			static::$call('<T5>- Level is '.json_encode($item->getLvl()));
			$call = $item->isValidOwnerAndParent() || $item instanceOf OwnerTierInterface ? 'Info' : 'Error';
			static::$call('<T5>- Owner is '.(!empty($item->getOwner()) ? $item->getOwner()->getShortname().' '.json_encode($item->getOwner()->getName()) : json_encode(null)));
			$call = $item_ownerdir instanceOf Item || $item instanceOf OwnerTierInterface ? 'Info' : 'Error';
			static::$call('<T5>- Ownerdir is '.(!empty($item_ownerdir) ? $item_ownerdir->getShortname().' '.json_encode($item_ownerdir->getName()) : json_encode(null)));
			$call = $item->isValidOwnerAndParent() ? 'Info' : 'Error';
			$parentOwner = '';
			if(!empty($item->getParent())) {
				$parentOwner = !empty($item_parent_ownerdir) ? '(Ownerdir of parent is '.$item_parent_ownerdir->getShortname().' '.json_encode($item_parent_ownerdir->getName()).'' : '(Ownerdir of parent is NULL';
				$parentOwner .= !empty($item->getParent()->getOwner()) ? ' / Owner of parent is '.$item->getParent()->getOwner()->getShortname().' '.json_encode($item->getParent()->getOwner()->getName()).')' : ' / Owner of parent is NULL)';
			}
			static::$call('<T5>- Parent is '.(!empty($item->getParent()) ? $item->getParent()->getShortname().' '.json_encode($item->getParent()->getName()).$parentOwner : json_encode(null)).(!$item->canHaveParent() ? ' ('.$item->getShortname().' with NO parent)' : ''));
			$call ='Info';
			static::$call('<T5>- Dirparents are '.$item->getDirparents()->count());
		}
		// Added 
		if($item instanceOf Reseausocial) {
			static::Info('<T5>----- adds for '.$item->getShortname());
			$call = !empty($item->getUrl()) ? 'Info' : 'Error';
			static::$call('<T5>- Url is '.json_encode($item->getUrl()));
			$call = !empty($item->getTypeReseau()) ? 'Info' : 'Error';
			static::$call('<T5>- Type reseau is '.json_encode($item->getTypeReseau()));
			$call = !empty($item->getIcon()) ? 'Info' : 'Error';
			static::$call('<T5>- Icon is '.json_encode($item->getIcon()));
			$call = !empty($item->getColor()) ? 'Info' : 'Error';
			static::$call('<T5>- color is '.json_encode($item->getColor()));
			$call = !empty($item->getTitle()) ? 'Info' : 'Error';
			static::$call('<T5>- Title is '.json_encode($item->getTitle()));
		}
		if($item instanceOf Telephon) {
			static::Info('<T5>----- adds for '.$item->getShortname());
			$call = $item->isValidPhonenumber() ? 'Info' : 'Error';
			static::$call('<T5>- Phonenumber is '.json_encode($item->getPhonenumber()));
		}
		$item->setDeepControl($dc);
	}


}