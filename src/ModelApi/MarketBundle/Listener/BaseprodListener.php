<?php
namespace ModelApi\MarketBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

use ModelApi\MarketBundle\Entity\Baseprod;
use ModelApi\MarketBundle\Service\serviceBaseprod;

// use ModelApi\DevprintsBundle\Service\DevTerminal;

use \Exception;

class BaseprodListener {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	protected $serviceBaseprod;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceBaseprod = $this->container->get(serviceBaseprod::class);
		return $this;
	}

	// /**
	//  * @ORM\PostLoad()
	//  */
	// public function postLoad(Baseprod $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	/**
	 * @ORM\PrePersist()
	 */
	public function prePersist(Baseprod $entity, LifecycleEventArgs $args) {
		$this->serviceBaseprod->checkAddedAutoEvents($entity, $args);
		// $entity->prePersist();
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdate(Baseprod $entity, PreUpdateEventArgs $args) {
		$this->serviceBaseprod->checkAddedAutoEvents($entity, $args);
		// $entity->preUpdate();
		return $this;
	}

	// /**
	//  * @ORM\PostPersist()
	//  */
	// public function postPersist(Baseprod $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	// /**
	//  * @ORM\PostUpdate()
	//  */
	// public function postUpdate(Baseprod $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	/**
	 * @ORM\PreRemove()
	 */
	public function preRemove(Baseprod $entity, LifecycleEventArgs $args) {
		$entity->setEvents(new ArrayCollection());
		$entity->setAddedAutoEvents(new ArrayCollection());
		return $this;
	}

	// /**
	//  * @ORM\PostRemove()
	//  */
	// public function postRemove(Baseprod $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }






}