<?php
namespace ModelApi\MarketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// Gedmo
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
// use ModelApi\BaseBundle\Traits\BaseItem;
// MarketBundle
use ModelApi\MarketBundle\Service\serviceBaseprod;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Service\FileManager;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Photo;
use ModelApi\BinaryBundle\Entity\Thumbnail;
// EventBundle
use ModelApi\EventBundle\Entity\Event;

use \ReflectionClass;
use \DateTime;

/**
 * Baseprod
 * 
 * @ORM\Entity(repositoryClass="ModelApi\MarketBundle\Repository\BaseprodRepository")
 * @ORM\EntityListeners({"ModelApi\MarketBundle\Listener\BaseprodListener"})
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(
 *      name="`baseprod`",
 *		options={"comment":"Base products"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_baseprod",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @Annot\HasTranslatable()
 */
abstract class Baseprod extends Item {

	const DEFAULT_ICON = 'fa-ticket';
	// const DEFAULT_ICON = 'fa-shopping-cart';
	const DEFAULT_OWNER_DIRECTORY = '@@@default@@@';
	const ENTITY_SERVICE = serviceBaseprod::class;

	// const CLASSEMENT_NAMES = ['visites','animations','evenements','enfant','adulte','undated'];
	const CLASSEMENT_NAMES = [
		'visites' => true,
		'animations' => true,
		'evenements' => false,
		'enfant' => false,
		'adulte' => false,
		'pass' => true,
		'undated' => true,
	];

	use \ModelApi\BaseBundle\Traits\BaseItem {
		computeBaseTexts as BaseItem_computeBaseTexts;
	}


	/**
	 * @var string
	 * @ORM\Column(name="longname", type="string", nullable=true, unique=false)
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=2,
	 * 		options={"required"=true},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=2,
	 * 		options={"required"=true},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=2)
	 */
	protected $longname;

	/**
	 * @var string
	 * @ORM\Column(name="categorie", type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $categorie;

	/**
	 * @var array
	 * @ORM\Column(name="classements", type="json_array", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "choices"="auto"},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "choices"="auto"},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $classements;

	/**
	 * Is visible even if is not active
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=100)
	 */
	protected $visibleEvenIfInactive;

	/**
	 * @var string
	 * @ORM\Column(name="opcode", type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $opcode;

	/**
	 * @var float
	 * @ORM\Column(type="float", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="MoneyType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		type="MoneyType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $prixttc;

	/**
	 * @var float
	 * @ORM\Column(type="float", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="MoneyType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		type="MoneyType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $prixaccueilttc;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type="CodemirrorType",
	 * 		order=101,
	 * 		options={"by_reference"=false, "required"=true},
	 * 		contexts={"all"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type="CodemirrorType",
	 * 		order=101,
	 * 		options={"by_reference"=false, "required"=true},
	 * 		contexts={"all"}
	 * )
	 * @CRUDS\Show(role=true, order=101)
	 */
	protected $pricecompute;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\EventBundle\Entity\Event", inversedBy="products")
	 * @ORM\JoinColumn(nullable=true, unique=false, name="products_events")
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "multiple"=true, "expanded"=false, "group_by"="shortname", "by_reference"=false, "query_builder"="auto"},
	 * 		contexts={"simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "multiple"=true, "expanded"=false, "group_by"="shortname", "by_reference"=false, "query_builder"="auto"},
	 * 		contexts={"simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=600)
	 */
	protected $events;

	/**
	 * @var array
	 * @ORM\Column(name="autoevents", type="json_array", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "multiple"=true, "expanded"=false, "by_reference"=false, "choices"="auto"},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "multiple"=true, "expanded"=false, "by_reference"=false, "choices"="auto"},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=600)
	 */
	protected $autoevents;
	/**
	 * @ORM\ManyToMany(targetEntity="ModelApi\EventBundle\Entity\Event", inversedBy="addedAutoProducts")
	 * @ORM\JoinTable(name="autoproducts_autoevents")
	 */
	protected $addedAutoEvents;
	/**
	 * @Annot\InjectEventClasses(setter="setAutoeventschoices")
	 */
	protected $autoeventschoices;

	/**
	 * @var integer
	 * @ORM\Column(type="integer", nullable=false, unique=false)
	 * @Gedmo\SortablePosition
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=701,
	 * 		options={"required"=true},
	 * 		contexts={"simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=701,
	 * 		options={"required"=true},
	 * 		contexts={"simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=701)
	 */
	protected $sortprod;

	/**
	 * @var array
	 * @ORM\ManyToOne(targetEntity="ModelApi\MarketBundle\Entity\Baseprod", inversedBy="childs")
	 * @Gedmo\SortableGroup
	 */
	protected $parentprod;

	/**
	 * @var array
	 * @ORM\OneToMany(targetEntity="ModelApi\MarketBundle\Entity\Baseprod", mappedBy="parentprod", orphanRemoval=true)
	 * @ORM\JoinTable(name="baseprod_childs",
	 *     joinColumns={@ORM\JoinColumn(name="childs_id", referencedColumnName="id")},
	 *     inverseJoinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")}
	 * )
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=595,
	 * 		options={"required"=false, "multiple"=true, "expanded"=false, "by_reference"=false, "query_builder"="auto"},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=595,
	 * 		options={"required"=false, "multiple"=true, "expanded"=false, "by_reference"=false, "query_builder"="auto"},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=595)
	 * @ORM\OrderBy({"sortprod" = "ASC"})
	 */
	protected $childs;

	/**
	 * @var array
	 * @ORM\ManyToMany(targetEntity="ModelApi\MarketBundle\Entity\Baseprod")
	 * @ORM\JoinTable(name="baseprod_products")
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "multiple"=true, "expanded"=false, "by_reference"=false, "query_builder"="auto"},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "multiple"=true, "expanded"=false, "by_reference"=false, "query_builder"="auto"},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=600)
	 */
	protected $products;

	/**
	 * @var boolean
	 * @ORM\Column(name="season", type="boolean", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $season;

	/**
	 * @var boolean
	 * @ORM\Column(name="onlyweb", type="boolean", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $onlyweb;

	/**
	 * @var boolean
	 * @ORM\Column(name="accesshandicap", type="boolean", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $accesshandicap;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Thumbnail", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=109,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsThumbnail"},
	 * 		contexts={"simple", "medium", "expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=109,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsThumbnail"},
	 * 		contexts={"simple", "medium", "expert"}
	 * 	)
	 * @CRUDS\Show(role=true, order=109)
	 */
	protected $thumbnail;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $photo;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=111,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsEntete"},
	 * 		contexts={"simple", "medium", "expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=111,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsEntete"},
	 * 		contexts={"simple", "medium", "expert"}
	 * 	)
	 * @CRUDS\Show(role=true, order=111)
	 */
	protected $entete;


	public function __construct() {
		parent::__construct();
		$this->longname = null;
		$this->categorie = null;
		$this->opcode = null;
		$this->accroche = null;
		$this->prixttc = 0.0;
		$this->prixaccueilttc = 0.0;
		$this->pricecompute = '';
		$this->classements = [];
		$this->events = new ArrayCollection();
		$this->autoevents = [];
		$this->addedAutoEvents = new ArrayCollection();
		$this->autoeventschoices = [];
		$this->sortprod = 0;
		$this->parentprod = null;
		$this->childs = new ArrayCollection();
		$this->products = new ArrayCollection();
		$this->season = false;
		$this->onlyweb = false;
		$this->accesshandicap = true;
		$this->thumbnail = null;
		$this->photo = null;
		$this->entete = null;
		return $this;
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getSlug();
	}

	/**
	 * Get enabled
	 * @return boolean 
	 */
	public function getEnabled() {
		if($this->getParentprod() instanceOf Baseprod && $this->getParentprod()->isDisabled()) return false;
		return $this->enabled;
	}

	/**
	 * Get title
	 * @param string $title = null
	 * @return self
	 */
	public function setTitle($title = null) {
		$this->title = serviceTools::getTextOrNull($title);
		return $this;
	}


	/**
	 * Get menutitle
	 * @param string $menutitle = null
	 * @return self
	 */
	public function setMenutitle($menutitle = null) {
		$this->menutitle = serviceTools::getTextOrNull($menutitle);
		return $this;
	}


	/**
	 * Set description
	 * @param string $description = null
	 * @return self
	 */
	public function setDescription($description = null) {
		$this->description = serviceTools::getTextOrNull($description);
		return $this;
	}

	/**
	 * Get longname
	 * @return string 
	 */
	public function getLongname() {
		return $this->longname;
	}

	/**
	 * Set longname
	 * @param string $longname = null
	 * @return Baseprod
	 */
	public function setLongname($longname = null) {
		$this->longname = serviceTools::getTextOrNull($longname);
		return $this;
	}

	/**
	 * @return Baseprod
	 */
	public function defineLongname() {
		if(serviceTools::getTextOrNull($this->longname)) $this->setLongname($this->getName());
		// if(strlen((string)$this->longname) <= 0) $this->longname = $this->name;
		return $this;
	}

	/**
	 * Get categorie
	 * @return string 
	 */
	public function getCategorie() {
		return $this->categorie;
	}

	/**
	 * Set categorie
	 * @param string $categorie
	 * @return Baseprod
	 */
	public function setCategorie($categorie) {
		$this->categorie = serviceTools::getTextOrNull($categorie);
		return $this;
	}

	/**
	 * Get opcode
	 * @return string 
	 */
	public function getOpcode() {
		if(!is_string($this->opcode) && $this->getParentprod() instanceOf Baseprod) return $this->getParentprod()->getOpcode();
		return $this->opcode;
	}

	/**
	 * Set opcode
	 * @param string $opcode
	 * @return Baseprod
	 */
	public function setOpcode($opcode = null) {
		$this->opcode = serviceTools::getTextOrNull($opcode);
		if(is_string($this->opcode) && !preg_match('#^CA-#', $this->opcode)) $this->opcode = 'CA-'.$this->opcode;
		return $this;
	}


	/**
	 * Set menutitle if not defined
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return self
	 */
	public function computeBaseTexts() {
		if(strlen((string)$this->menutitle) <= 0) $this->menutitle = $this->getCategorie() !== null ? implode(' - ', [$this->getName(), $this->getCategorie()]) : $this->getName();
		$this->defineLongname();
		if(null === $this->getTitle()) $this->setTitle($this->getLongname());
		if(null === $this->getMenutitle()) $this->setMenutitle($this->getLongname());
		$this->defineParentdata();
		$this->BaseItem_computeBaseTexts();
		return $this;		
	}
	// public function BaseItem_computeBaseTexts() {
	// 	return $this->computeBaseTexts();
	// }

	/**
	 * Get accroche
	 * @return string
	 */
	public function getAccroche() {
		return $this->accroche;
	}

	/**
	 * Set accroche
	 * @param string $accroche = null
	 * @return Baseprod
	 */
	public function setAccroche($accroche = null) {
		$this->accroche = serviceTools::getTextOrNull($accroche);
		return $this;
	}

	/**
	 * Get sortprod
	 * @return integer
	 */
	public function getSortprod() {
		return $this->sortprod;
	}

	/**
	 * Set sortprod
	 * @param integer $sortprod
	 * @return Baseprod
	 */
	public function setSortprod($sortprod) {
		$this->sortprod = $sortprod;
		return $this;
	}

	/**
	 * Get min prixttc
	 * @return float 
	 */
	public function getMinprixttc($onlyThis = false) {
		$minprixttc = min([$this->prixttc, $this->prixaccueilttc]);
		if(!$onlyThis) {
			foreach ($this->getChilds() as $child) {
				$chidlminprixttc = $child->getMinprixttc();
				if($minprixttc > $chidlminprixttc && $chidlminprixttc > 0) $minprixttc = $chidlminprixttc;
			}
		}
		return $minprixttc;
	}

	/**
	 * Get max prixttc
	 * @return float 
	 */
	public function getMaxprixttc($onlyThis = false) {
		$maxprixttc = max([$this->prixttc, $this->prixaccueilttc]);
		if(!$onlyThis) {
			foreach ($this->getChilds() as $child) {
				$chidlmaxprixttc = $child->getMaxprixttc();
				if($maxprixttc < $chidlmaxprixttc) $maxprixttc = $chidlmaxprixttc;
			}
		}
		return $maxprixttc;
	}

	/**
	 * Get prixttc
	 * @return float 
	 */
	public function getPrixttc() {
		return $this->prixttc;
	}

	/**
	 * Get prixttc
	 * @return float 
	 */
	public function getComputedPrixttc($nbpersons = 1, DateTime $date = null) {
		if(empty($this->getPricecompute())) return $this->prixttc;
		return eval($this->getPricecompute());
	}

	/**
	 * Set prixttc
	 * @param float $prixttc
	 * @return Baseprod
	 */
	public function setPrixttc($prixttc) {
		$this->prixttc = floatval($prixttc);
		return $this;
	}

	/**
	 * Get prixaccueilttc
	 * @return float 
	 */
	public function getPrixaccueilttc() {
		return $this->prixaccueilttc;
	}

	/**
	 * Set prixaccueilttc
	 * @param float $prixaccueilttc
	 * @return Baseprod
	 */
	public function setPrixaccueilttc($prixaccueilttc) {
		$this->prixaccueilttc = floatval($prixaccueilttc);
		return $this;
	}

	public function setPricecompute($pricecompute) {
		$this->pricecompute = (string)$pricecompute;
		return $this;
	}

	public function getPricecompute() {
		return $this->pricecompute;
	}


	public function getDefaultClassements() {
		return static::CLASSEMENT_NAMES;
	}

	public function getDefaultValidClassements() {
		return array_filter(static::CLASSEMENT_NAMES, function ($valid) { return $valid; });
	}



	/**
	 * Set classements
	 * @param array $classements
	 * @return Baseprod
	 */
	public function setClassements($classements) {
		// keep unvalid classements
		$dcls = $this->getDefaultClassements();
		$this->classements = array_filter($this->classements, function($name) use ($dcls) { return array_key_exists($name, $dcls) && !$dcls[$name]; });
		// ...and add/replace/remove with list of valid classements
		$defaultClassements = static::CLASSEMENT_NAMES;
		$classements = array_filter($classements, function ($valid, $name) use ($defaultClassements) {
			return in_array($name, $defaultClassements);
		}, ARRAY_FILTER_USE_BOTH);
		$this->classements = array_unique(array_merge($this->classements, $classements));
		return $this;
	}

	/**
	 * Get classements
	 * @return array 
	 */
	public function getClassements($normalized = false, $forTranslation = false) {
		if(!$normalized && !$forTranslation) return $this->classements;
		$classements = [];
		foreach ($this->classements as $classement) {
			$classements[] = $normalized ? FileManager::urlize($classement,'-') : 'classement.'.$classement;;
		}
		return array_unique($classements);
	}

	public function getValidClassements($normalized = false, $forTranslation = false) {
		$dcls = $this->getDefaultClassements();
		$validClassements = array_filter($this->classements, function($name) use ($dcls) { return array_key_exists($name, $dcls) && $dcls[$name]; });
		if(!$normalized && !$forTranslation) return $validClassements;
		foreach ($validClassements as $key => $classement) {
			$validClassements[$key] = $normalized ? FileManager::urlize($classement,'-') : 'classement.'.$classement;
		}
		return array_unique($validClassements);
	}

	public function getUnvalidClassements($normalized = false, $forTranslation = false) {
		$dcls = $this->getDefaultClassements();
		$unvalidClassements = array_filter($this->classements, function($name) use ($dcls) { return array_key_exists($name, $dcls) && !$dcls[$name]; });
		if(!$normalized && !$forTranslation) return $unvalidClassements;
		foreach ($unvalidClassements as $key => $classement) {
			$unvalidClassements[$key] = $normalized ? FileManager::urlize($classement,'-') : 'classement.'.$classement;
		}
		return array_unique($unvalidClassements);
	}

	/**
	 * Get all classements (with childs classements)
	 * @return array 
	 */
	public function getAllClassements($normalized = false, $forTranslation = false) {
		$allClassements = $this->classements;
		foreach ($this->getChilds() as $child) {
			$allClassements = array_unique(array_merge($allClassements, $child->getAllClassements()));
		}
		if(!$normalized && !$forTranslation) return $allClassements;
		$classements = [];
		foreach ($allClassements as $classement) {
			$classements[] = $normalized ? FileManager::urlize($classement,'-') : 'classement.'.$classement;;
		}
		return array_unique($classements);
	}

	public function getAllValidClassements($normalized = false, $forTranslation = false) {
		$classements = $this->getAllClassements(false);
		$dcls = $this->getDefaultClassements();
		$validClassements = array_filter($classements, function($name) use ($dcls) { return array_key_exists($name, $dcls) && $dcls[$name]; });
		if(!$normalized && !$forTranslation) return $validClassements;
		$classements = [];
		foreach ($validClassements as $classement) {
			$classements[] = $normalized ? FileManager::urlize($classement,'-') : 'classement.'.$classement;;
		}
		return array_unique($classements);
	}

	/**
	 * Get choices for classements
	 * @return array
	 */
	public function getClassementsChoices() {
		$classements = [];
		foreach ($this->getDefaultClassements() as $name => $valid) {
			$classements['classement.'.$name] = $name;
			// $classements[$name] = ['name' => $name, 'choice_attr' => static::CLASSEMENT_NAMES[$name] ? [] : ['disabled' => 'disabled'];
		}
		return $classements;
	}





	/**
	 * Set autoevents
	 * @param array $autoevents
	 * @return Baseprod
	 */
	public function setAutoevents($autoevents) {
		// $autoevents = array_unique($autoevents);
		$autoeventschoices = $this->getAutoeventsChoices();
		$this->autoevents = [];
		foreach ($autoevents as $autoevent) {
			if(array_key_exists($autoevent, $autoeventschoices)) {
				$this->autoevents[$autoevent] = $autoeventschoices[$autoevent];
			} else if(in_array($autoevent, $autoeventschoices)) {
				$keys = array_keys($autoeventschoices, $autoevent);
				$this->autoevents[reset($keys)] = $autoevent;
			}
		}
		return $this;
	}

	/**
	 * Get autoevents
	 * @return array 
	 */
	public function getAutoevents($shortnames = false) {
		// if($shortnames) {
		// 	$autoevents = [];
		// 	foreach ($this->autoevents as $classname) {
		// 		$RC = new ReflectionClass($classname);
		// 		$autoevents[] = $RC->getShortName();
		// 	}
		// 	return $autoevents;
		// }
		// return $this->autoevents;
		return $shortnames ? array_keys($this->autoevents) : $this->autoevents;
	}

	/**
	 * Get choices for autoevents
	 * @return array
	 */
	public function getAutoeventsChoices() {
		if(!isset($this->autoeventschoices) || !is_array($this->autoeventschoices)) $this->autoeventschoices = [];
		return $this->autoeventschoices;
	}

	public function setAutoeventschoices($autoeventschoices) {
		$this->autoeventschoices = $autoeventschoices;
		return $this;
	}





	/**
	 * Get AddedAuto Events
	 * @return ArrayCollection
	 */
	public function getAddedAutoEvents() {
		return $this->addedAutoEvents;
	}

	/**
	 * Add AddedAuto Event
	 * @param Event $event
	 * @return Baseprod
	 */
	public function addAddedAutoEvent(Event $event, $inverse = true) {
		if($inverse) $event->addAddedAutoProduct($this, false);
		if(!$this->addedAutoEvents->contains($event)) $this->addedAutoEvents->add($event);
		return $this;
	}

	/**
	 * Remove AddedAuto Event
	 * @param Event $event
	 * @return Baseprod
	 */
	public function removeAddedAutoEvent(Event $event, $inverse = true) {
		if($inverse) $event->removeAddedAutoProduct($this, false);
		return $this->addedAutoEvents->removeElement($event);
	}

	/**
	 * Set AddedAuto Events
	 * @param Collection <Event> $addedAutoEvents
	 * @return Baseprod
	 */
	public function setAddedAutoEvents(Collection $addedAutoEvents) {
		foreach ($this->addedAutoEvents as $event) {
			if(!$addedAutoEvents->contains($event)) $this->removeAddedAutoEvent($event);
		}
		foreach ($addedAutoEvents as $event) {
			$this->addAddedAutoEvent($event);
		}
		return $this;
	}





	/**
	 * Get Events + AddedAuto Events
	 * @return ArrayCollection <Event>
	 */
	public function getAllEvents($sortedByClass = false, $classes = [], $removeObsolete = false, $date = null) {
		$events = clone $this->getEvents();
		foreach ($this->getAddedAutoEvents() as $addedAutoEvent) {
			if(!$events->contains($addedAutoEvent)) $events->add($addedAutoEvent);
		}
		return serviceBaseprod::sortEvents($events, $sortedByClass, $classes, $removeObsolete, $date);
	}

	/**
	 * Get Events + AddedAuto Events sorteds
	 * @return ArrayCollection <Event>
	 */
	public function getAllEventsSorted($classes = [], $removeObsolete = true, $date = null) {
		// return [];
		if(null === $date) $date = $this->getContextdate();
		return $this->getAllEvents(true, $classes, $removeObsolete, $date);
	}




	/**
	 * Get Events
	 * @return ArrayCollection
	 */
	public function getEventsSorted($sortedByClass = false, $classes = [], $removeObsolete = false, $date = null) {
		$events = clone $this->events;
		if(null === $date) $date = $this->getContextdate();
		return serviceBaseprod::sortEvents($events, $sortedByClass, $classes, $removeObsolete, $date);
	}

	/**
	 * Get Events
	 * @return ArrayCollection
	 */
	public function getEvents() {
		return $this->events;
	}

	/**
	 * Add Event
	 * @param Event $event
	 * @return Baseprod
	 */
	public function addEvent(Event $event, $inverse = true) {
		if($inverse) $event->addProduct($this, false);
		if(!$this->events->contains($event)) $this->events->add($event);
		return $this;
	}

	/**
	 * Remove Event
	 * @param Event $event
	 * @return Baseprod
	 */
	public function removeEvent(Event $event, $inverse = true) {
		if($inverse) $event->removeProduct($this, false);
		return $this->events->removeElement($event);
	}

	/**
	 * Set Events
	 * @param Collection <Event> $events
	 * @return Baseprod
	 */
	public function setEvents(Collection $events) {
		foreach ($this->events as $event) {
			if(!$events->contains($event)) $this->removeEvent($event);
		}
		foreach ($events as $event) {
			$this->addEvent($event);
		}
		return $this;
	}





	/**
	 * Define data by parent
	 * @return Baseprod
	 */
	public function defineParentdata() {
		if($this->getParentprod() instanceOf Baseprod) {
			$this->setName($this->getParentprod()->getName());
			if($this->getPrixttc() == null) $this->setPrixttc($this->getParentprod()->getPrixttc());
			if($this->getPrixaccueilttc() == null) $this->setPrixaccueilttc($this->getParentprod()->getPrixaccueilttc());
			if($this->getDescription() == null) $this->setDescription($this->getParentprod()->getDescription());
			if($this->getAccroche() == null) $this->setAccroche($this->getParentprod()->getAccroche());
			// if($this->getLanguage() == null) $this->setLanguage($this->getParentprod()->getLanguage());
			if($this->getOpcode() == null) $this->setOpcode($this->getParentprod()->getOpcode());
			// if($this->getImageurls() == null) $this->setImageurls($this->getParentprod()->getImageurls());
			// Get parent's Products
			if($this->getProducts()->isEmpty()) $this->setProducts($this->getParentprod()->getProducts());
			$this->setSeason($this->getParentprod()->getSeason());
			$this->setOnlyweb($this->getParentprod()->getOnlyweb());
			// $this->setClassements(array_merge($this->classements, $this->getParentprod()->getClassements()));
			if($this->getEvents()->isEmpty()) $this->setEvents($this->getParentprod()->getEvents());
			if($this->getAddedAutoEvents()->isEmpty()) $this->setAddedAutoEvents($this->getParentprod()->getAddedAutoEvents());
		}
	}


	/**
	 * Get parentprod Baseprod
	 * @return Baseprod | null
	 */
	public function getParentprod() {
		return $this->parentprod;
	}

	/**
	 * Has parentprod Baseprod
	 * @return boolean
	 */
	public function hasParentprod() {
		return $this->parentprod instanceOf Baseprod;
	}

	/**
	 * Is root Baseprod
	 * @return boolean
	 */
	public function isRootProd() {
		return !($this->parentprod instanceOf Baseprod);
	}

	/**
	 * Set parentprod Baseprod
	 * @param Baseprod $parentprod = null
	 * @return Baseprod
	 */
	public function setParentprod(Baseprod $parentprod = null, $inverse = true) {
		if($inverse && $this->parentprod instanceOf Baseprod) {
			$this->parentprod->removeChild($this, false);
		}
		$this->parentprod = $parentprod;
		if($this->parentprod instanceOf Baseprod) {
			if($inverse) $this->parentprod->addChild($this, false);
			$this->defineParentdata();
		}
		return $this;
	}

	/**
	 * Add Child Baseprod
	 * @param Baseprod $child
	 * @return Baseprod
	 */
	public function addChild(Baseprod $child, $inverse = true) {
		if(!$this->childs->contains($child)) $this->childs->add($child);
		if($inverse) $child->setParentprod($this, false);
		return $this;
	}

	/**
	 * Set Childs Baseprod
	 * @param ArrayCollection <Baseprod> $childs
	 * @return Baseprod
	 */
	public function setChilds(ArrayCollection $childs) {
		foreach ($this->childs as $child) {
			if(!$childs->contains($child)) $this->removeChild($child);
		}
		foreach ($childs as $child) {
			$this->addChild($child);
		}
		return $this;
	}

	/**
	 * Get Childs Baseprod
	 * @return ArrayCollection
	 */
	public function getChilds() {
		return $this->childs;
	}

	/**
	 * Remove Child Baseprod
	 * @param Baseprod $child
	 * @return Baseprod
	 */
	public function removeChild(Baseprod $child, $inverse = true) {
		if($inverse) $child->setParentprod(null, false);
		return $this->childs->removeElement($child);
	}


	/**
	 * Add Product Baseprod
	 * @param Baseprod $product
	 * @return Baseprod
	 */
	public function addProduct(Baseprod $product) {
		if(!$this->products->contains($product)) $this->products->add($product);
		return $this;
	}

	/**
	 * Set Products Baseprod
	 * @param ArrayCollection <Baseprod> $products
	 * @return Baseprod
	 */
	public function setProducts(ArrayCollection $products) {
		foreach ($this->products as $product) {
			if(!$products->contains($product)) $this->removeProduct($product);
		}
		foreach ($products as $product) {
			$this->addProduct($product);
		}
		return $this;
	}

	/**
	 * Get Products Baseprod
	 * @return ArrayCollection
	 */
	public function getProducts() {
		// return $this->products;
		$parents = $this->getParentprod() instanceOf Baseprod ? $this->getParentprod()->getProducts() : new ArrayCollection();
		return new ArrayCollection(array_unique(array_merge($this->products->toArray(), $parents->toArray())));
	}

	/**
	 * Remove Product Baseprod
	 * @param Baseprod $product
	 * @return Baseprod
	 */
	public function removeProduct(Baseprod $product) {
		return $this->products->removeElement($product);
	}

	/**
	 * Get season
	 * @return boolean 
	 */
	public function getSeason() {
		return $this->season;
	}

	/**
	 * Set season
	 * @param boolean $season
	 * @return Baseprod
	 */
	public function setSeason($season) {
		$this->season = $season;
		return $this;
	}

	/**
	 * Get onlyweb
	 * @return boolean 
	 */
	public function getOnlyweb() {
		return $this->onlyweb;
	}

	/**
	 * Set onlyweb
	 * @param boolean $onlyweb
	 * @return Baseprod
	 */
	public function setOnlyweb($onlyweb) {
		$this->onlyweb = $onlyweb;
		return $this;
	}

	/**
	 * Get accesshandicap
	 * @return boolean 
	 */
	public function getAccesshandicap() {
		return $this->accesshandicap;
	}

	/**
	 * Set accesshandicap
	 * @param boolean $accesshandicap
	 * @return Baseprod
	 */
	public function setAccesshandicap($accesshandicap) {
		$this->accesshandicap = $accesshandicap;
		return $this;
	}





	/*************************************************************************************/
	/*** THUMBNAIL
	/*************************************************************************************/

	/**
	 * Set URL as thumbnail
	 * @param string $url
	 * @return Language
	 */
	public function setUrlAsThumbnail($url) {
		if(!$this->hasThumbnail()) { $this->setThumbnail(new Thumbnail()); }
		$this->getThumbnail()->setUploadFile($url);
	}

	/**
	 * Set resource as thumbnail
	 * @param string $resource
	 * @return Baseprod
	 */
	public function setResourceAsThumbnail(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$thumbnail = new Thumbnail();
			$thumbnail->setUploadFile($resource);
			$this->setThumbnail($thumbnail);
		}
		return $this;
	}

	public function getResourceAsThumbnail() {
		return null;
	}

	/**
	 * Set thumbnail
	 * @param Thumbnail $thumbnail = null
	 * @return Language
	 */
	public function setThumbnail(Thumbnail $thumbnail = null) {
		$this->thumbnail = $thumbnail;
		return $this;
	}

	/**
	 * Get thumbnail
	 * @return Thumbnail | null
	 */
	public function getThumbnail() {
		if($this->thumbnail instanceOf Thumbnail) return $this->thumbnail;
		$parentprod = $this->getParentprod();
		if($parentprod instanceOf Baseprod) return $parentprod->getThumbnail();
		return null;
	}

	/**
	 * Has thumbnail
	 * @return boolean
	 */
	public function hasThumbnail() {
		return $this->getThumbnail() instanceOf Thumbnail;
	}



	/*************************************************************************************/
	/*** PHOTO
	/*************************************************************************************/

	/**
	 * Set URL as photo
	 * @param string $url
	 * @return Baseprod
	 */
	public function setUrlAsPhoto($url) {
		$photo = new Photo();
		$photo->setUploadFile($url);
		$this->setPhoto($photo);
	}

	/**
	 * Set resource as photo
	 * @param string $resource
	 * @return Baseprod
	 */
	public function setResourceAsPhoto(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$photo = new Photo();
			$photo->setUploadFile($resource);
			$this->setPhoto($photo);
		}
		return $this;
	}

	public function getResourceAsPhoto() {
		return null;
	}

	/**
	 * Set photo
	 * @param Photo $photo = null
	 * @return Baseprod
	 */
	public function setPhoto(Photo $photo = null) {
		$this->photo = $photo;
		return $this;
	}

	/**
	 * Get photo
	 * @return Photo | null
	 */
	public function getPhoto() {
		return $this->photo;
	}


	/*************************************************************************************/
	/*** ENTETE
	/*************************************************************************************/

	/**
	 * Set URL as entete
	 * @param string $url
	 * @return Baseprod
	 */
	public function setUrlAsEntete($url) {
		$entete = new Photo();
		$entete->setUploadFile($url);
		$this->setEntete($entete);
	}

	/**
	 * Set resource as entete
	 * @param string $resource
	 * @return Baseprod
	 */
	public function setResourceAsEntete(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$entete = new Photo();
			$entete->setUploadFile($resource);
			$this->setEntete($entete);
		}
		return $this;
	}

	public function getResourceAsEntete() {
		return null;
	}

	/**
	 * Set entete
	 * @param Entete $entete = null
	 * @return Baseprod
	 */
	public function setEntete(Photo $entete = null) {
		$this->entete = $entete;
		return $this;
	}

	/**
	 * Get entete
	 * @return Entete | null
	 */
	public function getEntete() {
		return $this->entete;
	}

	/**
	 * Has entete
	 * @return boolean
	 */
	public function hasEntete() {
		return $this->getEntete() instanceOf Photo;
	}


}