<?php
namespace ModelApi\MarketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// MarketBundle
use ModelApi\MarketBundle\Entity\Baseprod;
use ModelApi\MarketBundle\Service\serviceProduct;

use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ModelApi\MarketBundle\Repository\ProductRepository")
 * @ORM\Table(
 *      name="product",
 *		options={"comment":"Products"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_product",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @Annot\HasTranslatable()
 */
class Product extends Baseprod {

	const ENTITY_SERVICE = serviceProduct::class;


	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR")
	 * @CRUDS\Update(show="ROLE_EDITOR", update="ROLE_EDITOR")
	 * @CRUDS\Show(role=false)
	 */
	protected $pricecompute;

	/**
	 * @var string
	 * @ORM\Column(name="typep", type="string", length=32, nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $typep;

	/**
	 * Begin date
	 * @var DateTime
	 * @ORM\Column(name="start", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $start;

	/**
	 * End date
	 * @var DateTime
	 * @ORM\Column(name="end", type="datetime", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $end;

	public function __construct() {
		parent::__construct();
		$this->typep = 'USUAL';
		// $tz = '+02:00';
		$year = new DateTime();
		$this->setStart(new DateTime($year->format("Y").'-01-01'));
		// $this->setEnd(new DateTime($year.'-12-31T23:59:59'.$tz));
		$this->setEnd(null);
	}


	/**
	 * Define data by parent
	 * @return Baseprod
	 */
	public function defineParentdata() {
		if($this->getParentprod() instanceOf Baseprod) {
			parent::defineParentdata();
			if(null == $this->getTypep()) $this->setTypep($this->getParentprod()->getTypep());
			if(null == $this->getStart()) $this->setStart($this->getParentprod()->getStart());
			if(null == $this->getEnd()) $this->setEnd($this->getParentprod()->getEnd());
		}
		return $this;
	}


	/**
	 * Is visible, even if inactive
	 * @return boolean 
	 */
	public function isVisible($contextdate = null) {
		$active = $this->isInStartEnd($contextdate) && $this->isActive() && !$this->isSoftdeleted($contextdate);
		return $this->getVisibleEvenIfInactive() || $active;
	}

	/**
	 * Is visible, even if inactive
	 * @return boolean 
	 */
	public function isInactiveButVisible($contextdate = null) {
		$inactive = !$this->isInStartEnd($contextdate) || $this->isDisabled() || $this->isSoftdeleted($contextdate);
		return $this->getVisibleEvenIfInactive() && $inactive;
	}


	/**
	 * Set product type
	 * @param string $typep
	 * @return Product
	 */
	public function setTypep($typep) {
		$this->typep = $typep;
		return $this;
	}

	/**
	 * Get product type
	 * @return string
	 */
	public function getTypep() {
		return $this->typep;
	}

	public function isInStartEnd($contextdate = null) {
		$contextdate = $this->getContextdate($contextdate);
		$isBeforeEnd = $this->getEnd() instanceOf DateTime ? $contextdate <= $this->getEnd() : true;
		return $contextdate >= $this->getStart() && $isBeforeEnd;
	}

	/**
	 * Set start
	 * @param DateTime $start
	 * @return Product
	 */
	public function setStart(DateTime $start) {
		$this->start = $start;
		return $this;
	}

	/**
	 * Get start
	 * @return DateTime
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * Set end
	 * @param DateTime $end
	 * @return Product
	 */
	public function setEnd(DateTime $end = null) {
		if(!empty($end)) {
			$oldate = new DateTime();
			$oldate = intval($oldate->modify('-2 YEARS')->format('Y'));
			if(intval($end->format('Y')) < $oldate) $end = null;
		}
		$this->end = $end;
		return $this;
	}

	/**
	 * Get end
	 * @return DateTime
	 */
	public function getEnd() {
		$this->setEnd($this->end);
		return $this->end;
	}


}