<?php
namespace ModelApi\MarketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// MarketBundle
use ModelApi\MarketBundle\Entity\Baseprod;
use ModelApi\MarketBundle\Service\serviceActivity;

use \DateTime;
use \Exception;

/**
 * @ORM\Entity(repositoryClass="ModelApi\MarketBundle\Repository\ActivityRepository")
 * @ORM\Table(
 *      name="activity",
 *		options={"comment":"Activitys"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_activity",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @Annot\HasTranslatable()
 */
class Activity extends Baseprod {

	const DEFAULT_ICON = 'fa-gamepad';
	const ENTITY_SERVICE = serviceActivity::class;
	const MAX_DIFFICULTY = 5;

	/**
	 * @var string
	 * @ORM\Column(name="activitygroups", type="json_array")
	 * @CRUDS\Create(
	 * 		type="DualListboxType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=true, "multiple"=true, "choices"="auto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="DualListboxType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=true, "multiple"=true, "choices"="auto"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=600)
	 */
	protected $activitygroups;

	/**
	 * @var string
	 * @ORM\Column(name="minactors", type="integer")
	 * @CRUDS\Create(
	 * 		type="IntegerType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=601,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="IntegerType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=601,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=601)
	 */
	protected $minactors;

	/**
	 * @var string
	 * @ORM\Column(name="maxactors", type="integer")
	 * @CRUDS\Create(
	 * 		type="IntegerType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=602,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="IntegerType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=602,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=602)
	 */
	protected $maxactors;

	/**
	 * @CRUDS\Create(
	 * 		type="RangesliderType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=602,
	 * 		options={"required"=true, "attr"={"class"="rangeslider"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="RangesliderType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=602,
	 * 		options={"required"=true, "attr"={"class"="rangeslider"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=602)
	 */
	protected $rangeactors;

	/**
	 * @var string
	 * @ORM\Column(name="timebefore", type="integer")
	 * @CRUDS\Create(
	 * 		type="KnobType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=605,
	 * 		options={"required"=true, "attr"={"class"="dial", "data-width"=100, "data-height"=100, "data-min"=0, "data-max"=60, "data-step"=5, "data-angleArc"=180, "data-angleOffset"=-90, "data-bgColor"="#ebebff", "data-fgColor"="#337ab7"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="KnobType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=605,
	 * 		options={"required"=true, "attr"={"class"="dial", "data-width"=100, "data-height"=100, "data-min"=0, "data-max"=60, "data-step"=5, "data-angleArc"=180, "data-angleOffset"=-90, "data-bgColor"="#ebebff", "data-fgColor"="#337ab7"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=605)
	 */
	protected $timebefore;

	/**
	 * @var string
	 * @ORM\Column(name="timeduring", type="integer")
	 * @CRUDS\Create(
	 * 		type="KnobType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=605,
	 * 		options={"required"=true, "attr"={"class"="dial", "data-width"=100, "data-height"=100, "data-min"=15, "data-max"=240, "data-step"=5, "data-angleArc"=180, "data-angleOffset"=-90, "data-bgColor"="#ebebff", "data-fgColor"="#337ab7"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="KnobType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=605,
	 * 		options={"required"=true, "attr"={"class"="dial", "data-width"=100, "data-height"=100, "data-min"=15, "data-max"=240, "data-step"=5, "data-angleArc"=180, "data-angleOffset"=-90, "data-bgColor"="#ebebff", "data-fgColor"="#337ab7"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=605)
	 */
	protected $timeduring;

	/**
	 * @var string
	 * @ORM\Column(name="timeafter", type="integer")
	 * @CRUDS\Create(
	 * 		type="KnobType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=605,
	 * 		options={"required"=true, "attr"={"class"="dial", "data-width"=100, "data-height"=100, "data-min"=0, "data-max"=60, "data-step"=5, "data-angleArc"=180, "data-angleOffset"=-90, "data-bgColor"="#ebebff", "data-fgColor"="#337ab7"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="KnobType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=605,
	 * 		options={"required"=true, "attr"={"class"="dial", "data-width"=100, "data-height"=100, "data-min"=0, "data-max"=60, "data-step"=5, "data-angleArc"=180, "data-angleOffset"=-90, "data-bgColor"="#ebebff", "data-fgColor"="#337ab7"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=605)
	 */
	protected $timeafter;

	/**
	 * @var string
	 * @ORM\Column(name="difficulty", type="integer")
	 * @CRUDS\Create(
	 * 		type="KnobType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=610,
	 * 		options={"required"=true, "attr"={"class"="dial", "data-width"=100, "data-height"=100, "data-min"=1, "data-max"=Activity::MAX_DIFFICULTY, "data-step"=1, "data-angleArc"=180, "data-angleOffset"=-90, "data-bgColor"="#ebebff", "data-fgColor"="#337ab7"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="KnobType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=610,
	 * 		options={"required"=true, "attr"={"class"="dial", "data-width"=100, "data-height"=100, "data-min"=1, "data-max"=Activity::MAX_DIFFICULTY, "data-step"=1, "data-angleArc"=180, "data-angleOffset"=-90, "data-bgColor"="#ebebff", "data-fgColor"="#337ab7"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=610)
	 */
	protected $difficulty;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $longname;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $categorie;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $classements;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $title;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $subtitle;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $menutitle;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $description;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $opcode;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $prixaccueilttc;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $events;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $picone;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $entete;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $tags;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $pageweb;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $groups;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $onlyweb;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $color;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $visibleEvenIfInactive;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $sortprod;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $autoevents;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $childs;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $products;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $season;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false,)
	 * @CRUDS\Show(role=true)
	 */
	protected $urls;


	public function __construct() {
		parent::__construct();
		$this->activitygroups = [0];
		$this->minactors = 16;
		$this->maxactors = 24;
		$this->rangeactors = [$this->minactors, $this->maxactors];
		$this->timebefore = 15;
		$this->timeduring = 60;
		$this->timeafter = 10;
		$this->difficulty = floor((static::MAX_DIFFICULTY + 1) / 2);
		return $this;
	}


	// /**
	//  * Define data by parent
	//  * @return Baseprod
	//  */
	// public function defineParentdata() {

	// 	return $this;
	// }



	/**
	 * Set activity type
	 * @param array $activitygroups
	 * @return Activity
	 */
	public function setActivitygroups($activitygroups) {
		$this->activitygroups = [];
		foreach ((array)$activitygroups as $activitygroup) {
			$this->addActivitygroup($activitygroup);
		}
		// return $this->checkActivitygroups();
		return $this;
	}

	public function addActivitygroup($activitygroup) {
		$activitygroupsValues = $this->getActivitygroupsAvailableNames(true);
		$activitygroupsNames = $this->getActivitygroupsAvailableNames();
		if(in_array($activitygroup, $activitygroupsValues) && !in_array($activitygroup, $this->activitygroups)) {
			$this->activitygroups[] = $activitygroup;
		}
		if(in_array($activitygroup, $activitygroupsNames) && !in_array($activitygroupsValues[$activitygroup], $this->activitygroups)) {
			$this->activitygroups[] = $activitygroupsValues[$activitygroup];
		}
		// return $this->checkActivitygroups();
		return $this;
	}

	/**
	 * Check activity groups: remove duplicates and wrong values
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return Activity
	 */
	public function checkActivitygroups() {
		$activitygroupsNames = $this->getActivitygroupsAvailableNames();
		$this->activitygroups = array_filter($this->activitygroups, function($value) use ($activitygroupsNames) {
			return array_key_exists($value, $activitygroupsNames);
		});
		$this->activitygroups = array_unique($this->activitygroups);
		return $this;
	}

	/**
	 * Get activity type
	 * @return array
	 */
	public function getActivitygroups($asNames = false) {
		return (!$asNames) ? $this->activitygroups : $this->activitygroupsNames();
	}

	public function activitygroupsNames() {
		$activitygroups = [];
		$activitygroupsNames = $this->getActivitygroupsAvailableNames();
		foreach ($this->activitygroups as $activitygroup) {
			$activitygroups[$activitygroup] = $activitygroupsNames[$activitygroup];
		}
		return $activitygroups;
	}

	/**
	 * Get available activity types list
	 * @return array
	 */
	public static function getActivitygroupsAvailableNames($reverse = false) {
		$activitygroupsNames = array(
			// 0 => 'Tous groupes',
			1 => 'Adultes & familles',
			2 => 'Groupes scolaires',
			3 => 'Centres de loisirs',
			4 => 'Entreprises',
			5 => 'Anniversaires',
			6 => 'Autres',
		);
		if($reverse) {
			$activitygroupsValues = [];
			foreach ($activitygroupsNames as $key => $name) $activitygroupsValues[$name] = $key;
			return $activitygroupsValues;
		}
		return $activitygroupsNames;
	}

	/**
	 * Get choices for activitys
	 * @return array
	 */
	public function getActivitygroupsChoices() {
		return $this->getActivitygroupsAvailableNames(true);
	}


	public function getMinactors() {
		return $this->minactors;
	}

	public function setMinactors($minactors) {
		$this->minactors = $minactors;
		if($this->minactors > $this->maxactors) $this->minactors = $this->maxactors;
		// $this->rangeactors = [$this->minactors, $this->maxactors];
		return $this;
	}

	public function getMaxactors() {
		return $this->maxactors;
	}

	public function setMaxactors($maxactors) {
		$this->maxactors = $maxactors;
		if($this->maxactors < $this->minactors) $this->maxactors = $this->minactors;
		// $this->rangeactors = [$this->minactors, $this->maxactors];
		return $this;
	}

	public function setRangeactors($rangeactors) {
		if(!is_array($rangeactors)) $rangeactors = json_decode($rangeactors);
		if(!is_array($rangeactors) || count($rangeactors) < 2) throw new Exception("Error ".__METHOD__."(): range actors data must be array with 2 values! Got ".(is_object($rangeactors) ? get_class($rangeactors) : json_encode($rangeactors))."!", 1);
		$max = max($rangeactors);
		$min = min($rangeactors);
		$this->setMaxactors($max);
		$this->setMinactors($min);
		return $this;
	}

	public function getRangeactors($asArray = false) {
		$this->rangeactors = [$this->minactors, $this->maxactors];
		return $asArray ? $this->rangeactors : json_encode($this->rangeactors);
	}

	public function getRangeactorsText($suffix = 'pers.') {
		$this->rangeactors = [$this->minactors, $this->maxactors];
		$suffix = !empty(trim($suffix)) ? ' '.trim($suffix) : '';
		if($this->minactors === $this->maxactors) return $this->maxactors.$suffix;
		return implode(' à ', $this->rangeactors).$suffix;
	}

	public function getTimebefore() {
		return $this->timebefore;
	}

	public function setTimebefore($timebefore) {
		$this->timebefore = $timebefore;
		return $this;
	}

	public function getTimeduring() {
		return $this->timeduring;
	}

	public function setTimeduring($timeduring) {
		$this->timeduring = $timeduring;
		return $this;
	}

	public function getTimeafter() {
		return $this->timeafter;
	}

	public function setTimeafter($timeafter) {
		$this->timeafter = $timeafter;
		return $this;
	}

	public function getDifficulty() {
		return $this->difficulty;
	}

	public function setDifficulty($difficulty) {
		$this->difficulty = (integer)$difficulty;
		if($this->difficulty > static::MAX_DIFFICULTY) $this->difficulty = static::MAX_DIFFICULTY;
		if($this->difficulty < 1) $this->difficulty = 1;
		return $this;
	}


}