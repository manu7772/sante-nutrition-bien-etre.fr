<?php
namespace ModelApi\MarketBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// MarketBundle
use ModelApi\MarketBundle\Entity\Product;

// use \ReflectionClass;

class ProductAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Product::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}