<?php
namespace ModelApi\MarketBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// MarketBundle
use ModelApi\MarketBundle\Entity\Activity;

// use \ReflectionClass;

class ActivityAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Activity::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}