<?php
namespace ModelApi\MarketBundle\Service;

// use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\Common\Collections\ArrayCollection;

// FileBundle
use ModelApi\FileBundle\Service\serviceItem;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// MarketBundle
use ModelApi\MarketBundle\Entity\Product;
use ModelApi\MarketBundle\Entity\Baseprod;
// EventBundle
use ModelApi\EventBundle\Entity\Event;

// use \DateTime;
use \ReflectionClass;

class serviceBaseprod extends serviceItem {

	const ENTITY_CLASS = Baseprod::class;
	const METHOD_CHECK_AUTOEVENTS = 2;


	public function checkAllAddedAutoEvents(LifecycleEventArgs $args = null) {
		$baseprods = $this->getRepository(Baseprod::class)->findAll();
		foreach ($baseprods as $baseprod) {
			$this->checkAddedAutoEvents($baseprod, $args);
		}
		return $this;
	}

	public function checkAddedAutoEvents(Baseprod $baseprod, LifecycleEventArgs $args = null) {
		$changes = false;
		switch (static::METHOD_CHECK_AUTOEVENTS) {
			case 1:
				// Keep new Events
				$changes = true; // can not test one by one... sorry!
				$events = $baseprod->getAddedAutoEvents()->filter(function($event) {
					return null == $event->getId();
				});
				foreach ($baseprod->getAutoevents(false) as $classname) {
					$addevents = $this->serviceEntities->getRepository($classname)->findAll();
					foreach ($addevents as $addevent) if(!$events->contains($addevent)) {
						$events->add($addevent);
					}
				}
				$baseprod->setAddedAutoEvents($events);
				break;
			default:
				$events = $baseprod->getAddedAutoEvents();
				foreach ($baseprod->getAutoevents(false) as $classname) {
					$addevents = $this->serviceEntities->getRepository($classname)->findAll();
					$testEvents = $events->filter(function($event) use ($classname) {
						return !empty($event->getId()) && $event->getClassname() === $classname;
					});
					foreach ($events as $event) {
						if(!$testEvents->contains($event)) {
							$events->removeElement($event);
							$changes = true;
						}
					}
					foreach ($addevents as $addevent) {
						if(!$testEvents->contains($addevent)) {
							$events->add($addevent);
							$changes = true;
						}
					}
				}
				break;
		}

		if($changes) {
			$em = $args->getEntityManager();
			$uow = $em->getUnitOfWork();
			if(empty($baseprod->getId())) $em->persist($baseprod); // ????????
			$uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($baseprod)), $baseprod);
		}
		return $this;
	}


	public static function sortEvents($events, $sortedByClass = false, $classes = [], $removeObsolete = false, $date = null) {
		if($events instanceOf Event) $events = new ArrayCollection([$events]);
		if(is_array($events)) $events = new ArrayCollection($events);
		if(!($events instanceOf ArrayCollection)) return new ArrayCollection();
		if($removeObsolete) {
			$events = $events->filter(function($event) use ($date) {
				return !$event->isObsolete($date) && $event->isActive();
			});
		}
		if($sortedByClass) {
			$sorteds = [];
			foreach ($events as $event) {
				if(!isset($sorteds[$event->getShortname()])) $sorteds[$event->getShortname()] = [];
				$sorteds[$event->getShortname()][] = $event;
			}
			if(!empty($classes)) {
				$sort2 = [];
				foreach ($classes as $class) {
					$class = ucfirst($class);
					if(isset($sorteds[$class])) $sort2[$class] = $sorteds[$class];
				}
				return $sort2;
			}
			return $sorteds;
		}
		if(!empty($classes)) {
			$sorteds = new ArrayCollection();
			foreach ($classes as $class) {
				foreach ($events as $event) if($event->getShortname() === ucfirst($class)) $sorteds->add($event);
			}
			return $sorteds;
		}
		return $events;
	}


}