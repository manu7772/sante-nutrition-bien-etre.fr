<?php

namespace ModelApi\InternationalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;

use ModelApi\InternationalBundle\Entity\Language;


class LanguageController extends Controller {

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="International",
	 *    description="Get Languages",
	 *    output = { "class" = Language::class, "collection" = true, "groups" = {"International"} },
	 *    views = { "default", "international" }
	 * )
	 * @Rest\View(serializerGroups={"base","Language","International","Directory","jstree"})
	 * @Rest\Get("/", name="modelapiinternational-getlanguages")
	 */
	public function getLanguagesAction(Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$languages = $em->getRepository(Language::class)->findAll();
		return $languages;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="International",
	 *    description="Get Languages",
	 *    output = { "class" = Language::class, "collection" = false, "groups" = {"International"} },
	 *    views = { "default", "international" }
	 * )
	 * @Rest\View(serializerGroups={"base","Language","International","Directory"})
	 * @Rest\Get("/language/{id}", name="modelapiinternational-getlanguage")
	 */
	public function getLanguageAction($id, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$language = $em->getRepository(Language::class)->find($id);
		return $language;
	}

	/**
	 * @Route("/create/international/{name}", name="modelapiinternational-create-international", methods={"GET"})
	 */
	public function createInternationalAction($name, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();

		$international = new International();
		$international->setName($name);

		$em->persist($international);
		$em->flush();

		return $this->redirectToRoute('modelapiinternational-getlanguages');
	}

	/**
	 * @Route("/remove/international/{slug}", name="modelapiinternational-remove-international", methods={"GET"})
	 */
	public function removeInternationalAction($slug, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$international = $em->getRepository('ModelApiInternationalBundle:International')->findOneBySlug($slug);

		if(!empty($international)) {
			$em->remove($international);
			$em->flush();
		}

		return $this->redirectToRoute('modelapiinternational-getlanguages');
	}

	/**
	 * @Route("/create/directory/{name}", name="modelapiinternational-create-directory", methods={"GET"})
	 */
	public function createDirectoryAction($name, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();

		$directory = new Directory();
		$directory->setName($name);

		$em->persist($directory);
		$em->flush();

		return $this->redirectToRoute('modelapiinternational-getlanguages');
	}

	/**
	 * @Route("/remove/directory/{slug}", name="modelapiinternational-remove-directory", methods={"GET"})
	 */
	public function removeDirectoryAction($slug, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$directory = $em->getRepository('ModelApiInternationalBundle:Directory')->findOneBySlug($slug);

		if(!empty($directory)) {
			$em->remove($directory);
			$em->flush();
		}

		return $this->redirectToRoute('modelapiinternational-getlanguages');
	}

	/**
	 * @Route("/directory-add-international/{slug}", name="modelapiinternational-directory-add-international", methods={"POST"})
	 */
	public function directoryAddInternationalAction($slug, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$language = $em->getRepository(Language::class)->findOneBySlug($slug);
		$directory = $em->getRepository('ModelApiInternationalBundle:Directory')->find($request->request->get('directory'));
		$directory->addChild($language);
		$em->flush();

		return $this->redirectToRoute('modelapiinternational-getlanguages');
	}

	/**
	 * @Route("/directory-remove-international/{id}/{slug}", name="modelapiinternational-directory-remove-international", methods={"GET"})
	 */
	public function directoryRemoveInternationalAction($id, $slug, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$directory = $em->getRepository('ModelApiInternationalBundle:Directory')->find($id);
		$language = $em->getRepository(Language::class)->findOneBySlug($slug);
		$directory->removeChild($language);
		$em->flush();

		return $this->redirectToRoute('modelapiinternational-getlanguages');
	}



}
