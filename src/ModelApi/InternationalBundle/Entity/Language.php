<?php
namespace ModelApi\InternationalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use ModelApi\AnnotBundle\Annotation as Annot;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\Basentity;
use ModelApi\BaseBundle\Service\serviceTools;
// UserBundle
use ModelApi\UserBundle\Entity\UserInterface;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Thumbnail;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

use \DateTimeZone;
use \Exception;

/**
 * Language
 * 
 * @ORM\Entity(repositoryClass="ModelApi\InternationalBundle\Repository\LanguageRepository")
 * @ORM\EntityListeners({"ModelApi\InternationalBundle\Listener\LanguageListener"})
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(
 *      name="`Language`",
 *		options={"comment":"Languages"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_Language",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR", label="Item")
 */
class Language implements CrudInterface {

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	const COUNTRY_SEPARATOR = '-';
	const DEFAULT_ICON = 'fa-flag';
	const ENTITY_SERVICE = serviceLanguage::class;
	const SHORTCUT_CONTROLS = true;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(name="country", type="string", length=8, nullable=false, unique=true)
	 * @CRUDS\Create(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=200,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=200,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role=true, order=200)
	 */
	protected $country;

	/**
	 * @ORM\Column(name="prefered", type="boolean", nullable=false, unique=false)
	 * @Annot\UniqueField(value=true, onError="force", min=1, max=1, altValue=false)
	 */
	protected $prefered;

	/**
	 * @var string
	 * @ORM\Column(name="countryName", type="string", length=128, nullable=false, unique=true)
	 * @CRUDS\Create(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=300,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=300,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role=true, order=300)
	 */
	protected $countryName;

	/**
	 * @ORM\Column(type="text", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=400,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=400,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role=true, order=400)
	 */
	protected $description;

	/**
	 * @ORM\Column(type="string", length=128, nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=350,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=350,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role=true, order=350)
	 */
	protected $timezone;

	/**
	 * color
	 * @var string
	 * @ORM\Column(name="color", type="string", length=32, nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		type="ColorpickerType",
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=350,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="ColorpickerType",
	 * 		contexts={"permanent"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=350,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role=true, order=350)
	 */
	protected $color;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Thumbnail", cascade={"persist"}, orphanRemoval=true)
	 */
	protected $thumbnail;

	/**
	 * Position
	 * @var integer
	 * @ORM\Column(name="position", type="integer")
	 * @Gedmo\SortablePosition
	 */
	protected $position;


	public function __construct() {
		$this->name = null;
		$this->prefered = false;
		$this->country = null;
		$this->countryName = null;
		$this->description = null;
		$this->timezone = null;
		$this->color = '#CCCCCC';
		$this->thumbnail = null;
		$this->setPosition(-1);
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return $this->getFullname();
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function getFullname() {
		return $this->getName().static::COUNTRY_SEPARATOR.$this->getCountry();
	}

	/**
	 * Get name
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set name
	 * @param string $name
	 * @return Language
	 */
	public function setName($name) {
		$this->name = strtolower(trim(serviceTools::removeSeparator($name)));
		return $this;
	}

	/**
	 * Get prefered
	 * @return boolean
	 */
	public function getPrefered() {
		return $this->prefered;
	}

	/**
	 * Is prefered
	 * @return boolean
	 */
	public function isPrefered() {
		return $this->getPrefered();
	}

	/**
	 * Set prefered
	 * @param boolean $prefered = true
	 * @return Language
	 */
	public function setPrefered($prefered = true) {
		$this->prefered = $prefered;
		return $this;
	}

	/**
	 * Get country
	 * @return string
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * Set country
	 * @param string $country
	 * @return Language
	 */
	public function setCountry($country) {
		$this->country = strtoupper(trim($country));
		// $timezones = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $this->country);
		// print($this->country." *************************************************************************************");
		// var_dump(implode(' / ', $timezones));
		// print("***************************************************************************************");
		// $this->setTimezone(reset($timezones));
		return $this;
	}

	/**
	 * Get country name
	 * @return string
	 */
	public function getCountryName() {
		return $this->countryName;
	}

	/**
	 * Set country name
	 * @param string $countryName
	 * @return Language
	 */
	public function setCountryName($countryName) {
		$this->countryName = trim($countryName);
		return $this;
	}

	/**
	 * Get description
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set description
	 * @param string $description = null
	 * @return Language
	 */
	public function setDescription($description = null) {
		$this->description = trim($description);
		return $this;
	}

	/**
	 * Set timezone
	 * @param string $timezone = null
	 * @return Language
	 */
	public function setTimezone($timezone = null) {
		// if(null === $timezone) {
			$this->timezone = $timezone;
			return $this;
		// }
		// $timezones = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $this->country);
		// if(in_array($timezone, $timezones)) $this->timezone = $timezone;
		// 	else throw new Exception("Error ".__METHOD__."() Timezone ".json_encode($timezone)." does not exist with this locale: ".json_encode($this->locale).".", 1);
		// return $this;
	}

	/**
	 * Get timezone
	 * @return string | null
	 */
	public function getTimezone() {
		return $this->timezone;
	}

	/**
	 * Get color
	 * @return string 
	 */
	public function getColor() {
		return $this->color;
	}

	/**
	 * Set color
	 * @param string $color
	 * @return Language
	 */
	public function setColor($color) {
		$this->color = $color;
		return $this;
	}


	/*************************************************************************************/
	/*** THUMBNAIL
	/*************************************************************************************/

	/**
	 * Set URL as thumbnail
	 * @param string $url
	 * @return Language
	 */
	public function setUrlAsThumbnail($url) {
		if(!$this->hasThumbnail()) { $this->setThumbnail(new Thumbnail()); }
		$this->getThumbnail()->setUploadFile($url);
	}

	// /**
	//  * Set resource as thumbnail
	//  * @param string $resource
	//  * @return Language
	//  */
	// public function setResourceAsThumbnail($resource) {
	// 	$this->resourceAsThumbnail = $resource;
	// 	if(!$this->hasThumbnail()) { $this->setThumbnail(new Thumbnail()); }
	// 	$this->getThumbnail()->setResource($this->resourceAsThumbnail);
	// 	return $this;
	// }

	// public function getResourceAsThumbnail() {
	// 	$this->resourceAsThumbnail = $this->getThumbnail();
	// 	if(!($this->resourceAsThumbnail instanceOf Thumbnail)) {
	// 		$this->resourceAsThumbnail = new Thumbnail();
	// 	}
	// 	return $this->resourceAsThumbnail;
	// }

	/**
	 * Set thumbnail
	 * @param Thumbnail $thumbnail = null
	 * @return Language
	 */
	public function setThumbnail(Thumbnail $thumbnail = null) {
		$this->thumbnail = $thumbnail;
		return $this;
	}

	/**
	 * Get thumbnail
	 * @return Thumbnail | null
	 */
	public function getThumbnail() {
		return $this->thumbnail;
	}

	/**
	 * Has thumbnail
	 * @return boolean
	 */
	public function hasThumbnail() {
		return $this->getThumbnail() instanceOf Thumbnail;
	}

	/**
	 * Get position
	 * @param integer $position
	 * @return Language
	 */
	public function setPosition($position) {
		$this->position = $position;
		return $this;
	}

	/**
	 * Get position
	 * @return integer
	 */
	public function getPosition() {
		return $this->position;
	}



}