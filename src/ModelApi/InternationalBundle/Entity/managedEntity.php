<?php
namespace ModelApi\InternationalBundle\Entity;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\DataCollectorTranslator; // for DEV
use Symfony\Bundle\FrameworkBundle\Translation\Translator; // for PROD
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceKernel;
// AnnotBundle
use ModelApi\AnnotBundle\Annotation\Translatable;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\TwgTranslation;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
use ModelApi\InternationalBundle\Service\serviceTranslation;
use ModelApi\InternationalBundle\Service\serviceLanguage;
// DevprintBundle
// use ModelApi\DevprintsBundle\Service\DevTerminal;
// use ModelApi\DevprintsBundle\Service\YamlLog;

use \Exception;

class managedEntity {

	// const NAME_TRANSLATED = 'translated';
	// const NAME_CODED = 'coded';
	const REMOVE_TAGS_IFNULLWITHOUT = true;
	const SHORTCUT_CONTROL = true;
	const DEEP_CONTROL = false;
	const INITIAL_USE_BDD = false;
	// const FILL_OTHER_LOCALES_ON_INITIALIZATION = false;
	const TEST = true;
	const UNDEFINED_VALUE = "#@@@#UNDEFINED_VALUE#@@@#";

	const STATUS_NOTRANSLATED = 0;
	const STATUS_TRANSLATED = 1;
	const STATUS_CODED = 2;

	const TRY_TO_RESOLVE = true;


	protected $entity;
	protected $locales;
	protected $serviceTranslation;
	protected $serviceKernel;
	protected $translator;
	protected $entityManager;
	protected $properties;
	protected $currentLocale;
	protected $twgTrequests;
	// protected $status;
	protected $cloned;
	protected $to_clone_entity;
	protected $initialized;
	protected $inicompleting;
	protected $inicomplete;

	public function __construct($entity, serviceTranslation $serviceTranslation) {
		$this->cloned = false;
		$this->to_clone_entity = null;
		$this->initialized = false;
		$this->inicompleting = false;
		$this->inicomplete = false;
		$this->properties = [];
		// if(!($translator instanceOf Translator) && !($translator instanceOf DataCollectorTranslator)) throw new Exception("Error ".__METHOD__."(): Translator argument must be instance of Translator (PROD) or DataCollectorTranslator (DEV)!", 1);
		if(!is_object($entity)) throw new Exception("Error ".__METHOD__."(): constructor argument must be object. ".gettype($entity)."given!", 1);
		if($entity instanceOf TwgTranslation) throw new Exception("Error ".__METHOD__."(): entity can not be instance of TwgTranslation!", 1);
		$this->entity = $entity;
		$this->serviceTranslation = $serviceTranslation;
		$this->translator = $this->serviceTranslation->getTranslator();
		$this->serviceKernel = $this->serviceTranslation->getServiceKernel();
		$this->setLocales($this->serviceTranslation->getLocales());
		$this->currentLocale = $this->serviceKernel->getLocale();
		$this->addLocale($this->currentLocale);
		$this->addLocale($this->serviceKernel->getDefaultLocale());
		// $this->resetCurrentLocale();
		if(!count($this->getLocales())) throw new Exception("Error ".__METHOD__."(): locales list is empty! Got ".json_encode($locale).".", 1);
		// $this->entityManager = $this->serviceTranslation->getEntityManager();
		// Initialize...
		$this->initialize();
		// if($this->hasErrors()) throw new Exception("Error on ".json_encode($this->entity->getShortname())." entity initialization: ".json_encode($this->getErrors()), 1);
		// add in entity
		$this->entity->translationManager = $this;
		$this->entity->_tm = $this;
		// $this->addUtils();
		return $this;
	}



	/*************************************************************************************/
	/*** CLONE
	/*************************************************************************************/
	/** @see https://github.com/myclabs/DeepCopy */


	public function __clone() {
		if(!is_object($this->to_clone_entity)) throw new Exception("Error ".__METHOD__."(): a new entity must be declared before cloning operation! Please use preClone() method before cloning!", 1);
		// Replace entity
		$this->entity = $this->getTo_clone_entity();
		$this->removeTo_clone_entity();
		// Add entity to serviceTranslation to me managed
		$this->startCloning();
		foreach ($this->properties as $property => $data) {
			$this->properties[$property]['keytext'] = serviceTranslation::getNewUniqueId($this->entity);
		}
		// Ending clone operation
		$this->stopCloning();
		if(!$this->serviceTranslation->addManagedEntity($this->entity)) throw new Exception("Error ".__METHOD__."(): ERROR while trying to manage new entity!", 1);
	}

	protected function startCloning() {
		$this->cloned = true;
		// $this->initialized = false;
		// $this->inicompleting = false;
		// $this->inicomplete = false;
		$this->twgTrequests = [];
		return $this;
	}

	protected function stopCloning() {
		$this->cloned = false;
		return $this;
	}

	protected function isCloning() {
		return $this->cloned;
	}

	public function preClone($to_clone_entity) {
		$this->setTo_clone_entity($to_clone_entity);
		return $this;
	}


	public function getTo_clone_entity() {
		return $this->to_clone_entity;
	}

	protected function setTo_clone_entity($to_clone_entity) {
		if(!is_object($to_clone_entity)) throw new Exception("Error ".__METHOD__."(): new entity is not valid!", 1);
		if($to_clone_entity === $this->entity) throw new Exception("Error ".__METHOD__."(): new entity can not be same instance as old entity before TM has been cloned!", 1);
		if(get_class($to_clone_entity) !== get_class($this->entity)) throw new Exception("Error ".__METHOD__."(): new entity class must be the same as current entity! Got ".get_class($to_clone_entity)." but ".get_class($this->entity)." requested!", 1);
		$this->to_clone_entity = $to_clone_entity;
		return $this;
	}

	protected function removeTo_clone_entity() {
		$this->to_clone_entity = null;
	}

	public function hasTo_clone_entity() {
		return !empty($this->to_clone_entity);
	}


	/*************************************************************************************/
	/*** ERRORS
	/*************************************************************************************/

	public function getErrors($globalOnly = false) {
		$errors = [];
		if(count($this->properties) <= 0) $errors[] = 'Aucune propriété trouvée!';
		if(count($this->getLocales()) <= 0) $errors[] = 'Aucune locale trouvée!';
		if(!$globalOnly) {
			foreach ($this->getPropertiesNames() as $property) {
				$propertyErrors = $this->getPropertyErrors($property);
				$errors = array_merge($errors, $propertyErrors);
				// if(!array_key_exists('keytext', $this->properties[$property]) || !$this->isValidKeytext($this->properties[$property]['keytext'])) $errors[] = 'Keytext de '.json_encode($property).' invalide';
				// if(!array_key_exists('value', $this->properties[$property]) || !(is_array($this->properties[$property]['value']) && count($this->properties[$property]['value']))) $errors[] = 'Données de texte de '.$property.' Invalide. Trouvé '.json_encode($this->properties[$property]['value']).'.';
			}
		}
		return $errors;
	}

	public function getPropertyErrors($property) {
		$errors = [];
		if(!array_key_exists('keytext', $this->properties[$property]) || !$this->isValidKeytext($this->properties[$property]['keytext'])) $errors[] = 'Keytext de '.json_encode($property).' invalide';
		if(!array_key_exists('value', $this->properties[$property]) || !(is_array($this->properties[$property]['value']) && count($this->properties[$property]['value']))) $errors[] = 'Données de texte de '.$property.' Invalide. Trouvé '.json_encode($this->properties[$property]['value']).'.';
		return $errors;		
	}

	public function hasErrors() {
		return count($this->getErrors()) > 0;
	}

	public function isComplete() {
		return count($this->getErrors()) <= 0;
	}



	/*************************************************************************************/
	/*** ENTITY
	/*************************************************************************************/

	public function getEntity() {
		return $this->entity;
	}

	protected function getEntityValue($property) {
		if(!$this->isInitializing()) $this->hasProperty($property, true);
		$getter = $this->properties[$property]['getter'];
		return $this->entity->$getter();
	}
	public function getValue($property) {
		return $this->getEntityValue($property);
	}

	protected function setEntityValue($property, $value, $excludeKeytext = false) {
		if(!$this->isInitializing()) $this->hasProperty($property, true);
		$setter = $this->properties[$property]['setter'];
		// $value = static::UNDEFINED_VALUE === $value ? null : $value;
		// $this->entity->$setter($value);
		$this->entity->$setter($this->getTextOrNull($value, $excludeKeytext));
		return $this;
	}

	protected function isValidKeytext($keytext, $makeException = false) {
		$valid = is_string($keytext) ? serviceTranslation::isUniqueId($keytext, $this->entity) : false;
		if(true === $makeException && !is_string($makeException)) $makeException = __METHOD__;
		// if(!$valid && $makeException === '#getError#') return "keytext ".json_encode($keytext)." is not a valid keytext!", 1);
		if(!$valid && $makeException !== false) {
			if(!static::TRY_TO_RESOLVE) throw new Exception("Error ".$makeException."(): keytext ".json_encode($keytext)." is not a valid keytext!", 1);
			return "keytext ".json_encode($keytext)." is not a valid keytext!";
		}
		return $valid;
	}

	public function getTextOrNull($value, $excludeKeytext = true) {
		if($excludeKeytext && $this->isValidKeytext($value, false)) return null;
		if($value === static::UNDEFINED_VALUE) return null;
		return serviceTools::getTextOrNull($value, static::REMOVE_TAGS_IFNULLWITHOUT);
	}

	public function getTextOrUndefined($value, $excludeKeytext = true) {
		if($excludeKeytext && $this->isValidKeytext($value, false)) return static::UNDEFINED_VALUE;
		return serviceTools::getTextOrDefault($value, static::UNDEFINED_VALUE, static::REMOVE_TAGS_IFNULLWITHOUT);
	}



	/*************************************************************************************/
	/*** UTILS
	/*************************************************************************************/

	public function getEntityManager() {
		return $this->entityManager ??= $this->serviceTranslation->getEntityManager();
	}


	/*************************************************************************************/
	/*** INITIALIZED --> INICOMPLETING --> INICOMPLETED
	/*************************************************************************************/

	public function isInitializing() {
		return !$this->isInitialized() && !$this->isInicompleting() && !$this->isInicomplete();
	}

	public function isInitialized() {
		return $this->initialized;
	}

	public function isOnlyInitialized() {
		return $this->isInitialized() && !$this->isInicompleting() && !$this->isInicomplete();
	}

	public function setInitialized() {
		if($this->isInicompleting()) throw new Exception("Error ".$method."(): can not set initialized as managedEntity is init completing!", 1);
		if($this->isInicomplete()) throw new Exception("Error ".$method."(): can not set initialized as managedEntity is init completed!", 1);
		$this->initialized = true;
		return $this;
	}



	public function isInicompleting() {
		return $this->inicompleting;
	}

	public function setInicompleting() {
		$this->ExceptionIfNotInitialized(__METHOD__);
		if($this->isInicomplete()) throw new Exception("Error ".$method."(): can not set inicompleting as managedEntity is init completed!", 1);
		$this->inicompleting = true;
		return $this;
	}



	public function isInicomplete() {
		return $this->inicomplete;
	}

	public function isInicompletingOrInicomplete() {
		return $this->isInicompleting() || $this->isInicomplete();
	}

	public function setInicomplete() {
		$this->ExceptionIfNotInitialized(__METHOD__);
		if(!$this->isInicompleting()) throw new Exception("Error ".$method."(): can set init completed ONLY AFTER init completing!", 1);
		$this->inicompleting = false;
		$this->inicomplete = true;
		return $this;
	}




	protected function ExceptionIfNotInitializing($method = null) {
		if(!is_string($method)) $method = __METHOD__;
		if(($this->entity->translationManager ??= null) instanceOf managedEntity || ($this->entity->_tm ??= null) instanceOf managedEntity) throw new Exception("Error ".$method."(): this entity has already been initialized!", 1);
		if(!$this->isInitializing()) throw new Exception("Error ".$method."(): managedEntity is still initialized or more!", 1);
	}

	protected function ExceptionIfNotInitialized($method = null) {
		if(!is_string($method)) $method = __METHOD__;
		if(!$this->isInitialized()) throw new Exception("Error ".$method."(): managedEntity is not initialized!", 1);
	}

	protected function ExceptionIfNotInicompleting($method = null) {
		if(!is_string($method)) $method = __METHOD__;
		if(!$this->isInicompleting()) throw new Exception("Error ".$method."(): managedEntity is not init completing!", 1);
		$this->ExceptionIfNotInitialized($method);
	}

	protected function ExceptionIfNotInicomplete($method = null) {
		if(!is_string($method)) $method = __METHOD__;
		if(!$this->isInicompleting()) throw new Exception("Error ".$method."(): managedEntity is still init completing!", 1);
		if(!$this->isInicomplete()) throw new Exception("Error ".$method."(): managedEntity is not init completed!", 1);
		$this->ExceptionIfNotInitialized($method);
	}

	protected function ExceptionIfNotInicompletingOrComplete($method = null) {
		if(!is_string($method)) $method = __METHOD__;
		if(!$this->isInicompleting() && !$this->isInicomplete()) throw new Exception("Error ".$method."(): managedEntity is not init completing or completed!", 1);
	}


	/*************************************************************************************/
	/*** STATUS
	/*************************************************************************************/

	// public function getStatus() {
	// 	return $this->status;
	// }

	// protected function setStatus($status) {
	// 	$this->ExceptionIfNotInitialized(__METHOD__);
	// 	if(!in_array($status, [static::STATUS_NOTRANSLATED, static::STATUS_TRANSLATED, static::STATUS_CODED])) throw new Exception("Error ".__METHOD__."(): status ".json_encode($status)." is not valid!", 1);
	// 	$this->status = $status;
	// 	return $this;
	// }

	// protected function computeStatus() {
	// 	$status = static::STATUS_NOTRANSLATED;

	// 	$this->setStatus($status);
	// 	return $this;
	// }


	/*************************************************************************************/
	/*** LOCALES
	/*************************************************************************************/

	public function getCurrentLocale() {
		return $this->currentLocale;
	}

	public function setCurrentLocale($locale = null) {
		if($this->hasLocale($locale, true) && $locale !== $this->currentLocale) {
			$this->doInicomplete();
			$this->currentLocale = $locale;
			foreach ($this->getPropertiesNames() as $property) {
				if(!$this->isValidKeytext($this->getEntityValue($property))) {
					$this->setEntityValue($property, $this->properties[$property]['value'][$locale]['value']);
				}
			}
			// if($this->isTranslated()) $this->translate();
		}
		return $this;
	}

	public function resetCurrentLocale() {
		$this->setCurrentLocale($this->serviceKernel->getLocale());
		return $this;
	}

	public function getDefaultLocale() {
		return $this->serviceKernel->getDefaultLocale();
	}



	public function setLocales($locales) {
		$this->locales = [];
		foreach ($locales as $locale) $this->addLocale($locale);
		return $this;
	}

	public function addLocale($locale) {
		if(!serviceLanguage::isValidLocale($locale)) throw new Exception("Error ".__METHOD__."(): locale ".json_encode($locale)." is not valid!", 1);
		if(!$this->hasLocale($locale)) {
			$this->locales[] = $locale;
			// update properties
			if($this->isInitialized()) {
				foreach ($this->getPropertiesNames() as $property => $data) {
					$twgTranslation = $this->getTwgTranslationByParams($data['keytext'], $data['domain'], $locale, true);
					$this->properties[$property]['value'][$locale]['twg'] = $twgTranslation;
					$this->properties[$property]['value'][$locale]['value'] = $this->getTextOrUndefined($twgTranslation->getContent());
					// $this->properties[$property]['value'][$locale]['value'] = $twgTranslation->getContent();
				}
				// should not be neccessary... but... anyway
				if($this->isTranslated() && $this->getCurrentLocale() === $locale) $this->translate();
			}
		}
		return $this;
	}

	public function removeLocale($locale) {
		$this->locales = array_filter($this->locales, function($loc) use ($locale) { return $loc !== $locale; });
		return $this;
	}

	public function getLocales() {
		return $this->locales;
	}

	public function hasLocale($locale = null, $makeException = false) {
		if(empty($locale)) return false;
		if($makeException && !in_array($locale, $this->locales)) throw new Exception("Error ".__METHOD__."(): locale ".json_encode($locale)." does not exist!", 1);
		return in_array($locale, $this->locales);
	}

	protected function filterLocales(&$locales, $allIfEmpty = true) {
		if(is_string($locales)) $locales = [$locales];
		if($allIfEmpty && empty($locales)) {
			$locales = $this->getLocales();
		} else {
			$allLocales = $this->getLocales();
			$locales = array_filter($locales, function($locale) use ($allLocales) { return in_array($locale, $allLocales); });
		}
		return $locales;
	}

	// protected function isFillOtherLocales() {
	// 	if(!$this->isInitializing()) return false;
	// 	return static::FILL_OTHER_LOCALES_ON_INITIALIZATION;
	// }


	/*************************************************************************************/
	/*** PERIST, UPDATE AND FLUSH ACTIONS
	/*************************************************************************************/

	public function preCreateForm(LifecycleEventArgs $args) {
		$this->doInicomplete();
		$this->translate();
		return $this;
	}

	public function preUpdateForm(LifecycleEventArgs $args) {
		$this->doInicomplete();
		$this->translate();
		return $this;
	}

	public function onFlushActions(OnFlushEventArgs $args) {
		$em = $args->getEntityManager();
		$uow = $em->getUnitOfWork();
		$this->encode();
		// Combine TwgTranslations
		$modifieds = $this->combineTwgs();
		foreach ($modifieds as $twgTranslation) if($twgTranslation instanceOf TwgTranslation) {
			// $emmsg = $this->getEntityManager() === $em ? '-- same EntityManager --' : '-- different EntityManager --';
			// if(!$this->is_entityManaged($twgTranslation)) {
			// 	throw new Exception("Error ".__METHOD__."(): (ext. EM ".$emmsg.") ".json_encode($twgTranslation->getKeytext())." (belongs to ".$this->entity->getShortname()." ".$this->entity->getName().") is not managed by EntityManager!", 1);
			// }
			// if(!$em->contains($twgTranslation)) {
			// 	throw new Exception("Error ".__METHOD__."(): (param. EM ".$emmsg.") ".json_encode($twgTranslation->getKeytext())." (belongs to ".$this->entity->getShortname()." ".$this->entity->getName().") is not managed by EntityManager!", 1);
			// }
			if($this->is_entityManaged($twgTranslation) && !$twgTranslation->_isDeleted) {
				$uow->computeChangeSet($em->getClassMetadata(get_class($twgTranslation)), $twgTranslation);
				// $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($twgTranslation)), $twgTranslation);
			}
		}
		$uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($this->entity)), $this->entity);
		// $uow->computeChangeSet($em->getClassMetadata(get_class($this->entity)), $this->entity);
		return $this;
	}

// twg_isNew
// twg_isFromDb
// _isPersisted
// _isDeleted

	public function postFlushActions(PostFlushEventArgs $args) {
		$this->translate();
		return false;
		// // if entity does not come from preCreateForm or preUpdateForm event:
		// if(!isset($this->entity->_for_form)) return false;
		// $modifieds = $this->combineTwgs()->count() > 0;

		// return $modifieds;
	}


	/*************************************************************************************/
	/*** TRANSLATIONS
	/*************************************************************************************/

	protected function getKeytextLocaleTranslation($keytext, $domain, $locale = null, $forceRequest = false) {
		$this->isValidKeytext($keytext, __METHOD__);
		// if(!$this->hasLocale($locale)) $locale = $this->getCurrentLocale();
		if(!$this->hasLocale($locale)) throw new Exception("Error ".__METHOD__."(): locale ".json_encode($locale)." does not exist!", 1);
		$translations = $this->getKeytextTranslations($keytext, $domain, [$locale], $forceRequest);
		return $translations[$locale];
	}

	protected function getKeytextTranslations($keytext, $domain, $locales = null, $forceRequest = false) {
		$this->isValidKeytext($keytext, __METHOD__);
		$this->filterLocales($locales, true);
		$translations = [];
		foreach ($locales as $locale) $translations[$locale] = null;
		if(!is_string($this->getTextOrNull($keytext, false))) return $translations;
		if($this->isInicompletingOrInicomplete()) {
			// By BDD
			$twgTranslations = $this->getTwgTranslationsByParams($keytext, $domain, null, $forceRequest);
			foreach ($locales as $locale) {
				foreach ($twgTranslations as $twgTranslation) {
					if(!$twgTranslation->isValidContent()) throw new Exception("Error ".__METHOD__."(): translation for ".$locale." / ".$keytext." / ".$domain." is invalid!", 1);
					if($locale === $twgTranslation->getTranslatableLocale()) $translations[$locale] = $twgTranslation->getContent();
				}
				if(!array_key_exists($locale, $translations)) $translations[$locale] = null;
			}
			// Set default locale text anyway
			if(in_array($this->getDefaultLocale(), $locales)) {
				if(null === $translations[$this->getDefaultLocale()]) $translations[$this->getDefaultLocale()] = $translations[$this->getCurrentLocale()];
				if(null === $translations[$this->getDefaultLocale()]) {
					foreach ($translations as $locale => $value) {
						if(null !== $value) {
							$translations[$this->getDefaultLocale()] = $value;
							break;
						}
					}
				}
			}
		} else {
			// By TRANSLATOR
			foreach ($locales as $locale) {
				$value = $this->translator->trans($keytext, [], $domain, $locale);
				$translations[$locale] = $this->isValidKeytext($value) ? null : $value;
			}
		}
		return $translations;
	}

	protected function getTwgTranslationsByParams($keytext, $domain = null, $locale = null, $forceRequest = false) {
		$this->isValidKeytext($keytext, __METHOD__);
		$this->ExceptionIfNotInicompletingOrComplete();
		$params = ['keytext' => $keytext];
		// if(is_string($domain)) $params['domain'] = $domain;
		// if(is_string($locale)) $params['translatableLocale'] = $locale;
		// ksort($params);
		$memRequName = implode('', $params);
		if(!array_key_exists($memRequName, $this->twgTrequests) || true === $forceRequest) {
			// echo('<div>TranslateManager REQUEST!!!</div>');
			$this->twgTrequests[$memRequName] = $this->getEntityManager()->getRepository(TwgTranslation::class)->findBy($params);
			foreach ($this->twgTrequests[$memRequName] as $twgTranslation) $this->twg_setFromDb($twgTranslation);
		}
		if(static::DEEP_CONTROL) {
			foreach ($this->twgTrequests[$memRequName] as $twgTranslation) {
				if($twgTranslation->getKeytext() !== $keytext) throw new Exception("Error ".__METHOD__."(): TwgTranslations from request do not have the good keytext ".json_encode($keytext)."!", 1);
			}
		}
		return array_filter($this->twgTrequests[$memRequName], function($twgTranslation) use ($domain, $locale) {
			return (empty($domain) || $twgTranslation->getDomain() === $domain) && (empty($locale) || $twgTranslation->getTranslatableLocale() === $locale);
		});
	}

	public function getTwgTranslationByParams($keytext, $domain, $locale, $createNewIfNotFound = true, $forceRequest = false) {
		$this->isValidKeytext($keytext, __METHOD__);
		// $this->ExceptionIfNotInicompletingOrComplete();
		// $twgTranslations = $this->getTwgTranslationsByParams($keytext, $domain, $locale, $forceRequest);
		// if(empty($twgTranslations)) $twgTranslations = $this->getTwgTranslationsByParams($keytext, $domain, null, $forceRequest);
		// if(empty($twgTranslations)) $twgTranslations = $this->getTwgTranslationsByParams($keytext, null, null, $forceRequest);
		// if($this->isValidKeytext($keytext)) {
			$twgTranslations = $this->getTwgTranslationsByParams($keytext, null, null, $forceRequest);
			// if(empty($twgTranslations)) return null;
			// if(count($twgTranslation) > 1) throw new Exception("Error ".__METHOD__."(): found more than just one unique TwgTranslation with params ".json_encode([$keytext, $domain, $locale])."!", 1);
			$uniqkeychain = TwgTranslation::getUniqkeychainWithData($keytext, $domain, $locale);
			$twgTranslation = null;
			foreach ($twgTranslations as $twg) {
				if($twg->getUniqkeychain() === $uniqkeychain) {
					if($twgTranslation instanceOf TwgTranslation) throw new Exception("Error ".__METHOD__."(): translation ".$uniqkeychain." is duplicate!", 1);
					$twgTranslation = $twg;
					// return $twgTranslation;
				}
			}
		// }
		if($createNewIfNotFound && empty($twgTranslation)) {
			$twgTranslation = $this->twg_getNew($keytext, $domain, $locale);
		}
		// return null;
		return $twgTranslation;
	}


	// protected function getTranslatedByParams($keytext, $domain, $locale) {
	// 	$twgTranslation = $this->getTwgTranslationByParams($keytext, $domain, $locale, false);
	// 	return $twgTranslation instanceOf TwgTranslation ? $twgTranslation->getContent() : null;
	// }

	// protected function cleanTwgTranslations($properties = []) {
	// 	$properties = $this->getPropertiesNames($properties);
	// }

	public function getAllTwgTranslations() {
		$twgs = new ArrayCollection();
		foreach ($this->getProperties() as $property => $data) {
			foreach ($data['value'] as $locale => $dat2) {
				if($data['value'][$locale]['twg'] instanceOf TwgTranslation && !$twgs->contains($data['value'][$locale]['twg'])) $twgs->add($data['value'][$locale]['twg']);
			}
		}
		return $twgs;
	}

	// public function getNewTwgTranslations() {
	// 	return $this->getAllTwgTranslations()->filter(function($twgTranslation) { return $twgTranslation->_isNew; });
	// }

	// public function getUnpersistedTwgTranslations() {
	// 	return $this->getAllTwgTranslations()->filter(function($twgTranslation) { return !$twgTranslation->_isPersisted; });
	// }

	// public function getDbTwgTranslations() {
	// 	return $this->getAllTwgTranslations()->filter(function($twgTranslation) { return $twgTranslation->_fromDb; });
	// }

	// public function getTodeleteTwgTranslations() {
	// 	return $this->getAllTwgTranslations()->filter(function($twgTranslation) { return $twgTranslation->_isTodelete; });
	// }

	// public function getDeletedTwgTranslations() {
	// 	return $this->getAllTwgTranslations()->filter(function($twgTranslation) { return $twgTranslation->_isDeleted; });
	// }



	protected function twg_getNew($keytext, $domain, $locale) {
		$this->isValidKeytext($keytext, __METHOD__);
		$twgTranslation = new TwgTranslation();
		$twgTranslation->init_ClassDescriptor();
		$twgTranslation->setKeytext($keytext);
		$twgTranslation->setDomain($domain);
		$twgTranslation->setTranslatableLocale($locale);
		$twgTranslation->_fromDb = false;
		$twgTranslation->_isNew = true;
		$twgTranslation->_isPersisted = false;
		$twgTranslation->_isDeleted = false;
		return $twgTranslation;
	}
	protected function twg_isNew(TwgTranslation $twgTranslation) { return isset($twgTranslation->_isNew) ? $twgTranslation->_isNew : false; }

	protected function twg_setFromDb(TwgTranslation $twgTranslation) {
		if(!$this->twg_isNew($twgTranslation)) {
			$twgTranslation->_fromDb = true;
			$twgTranslation->_isNew = false;
			$twgTranslation->_isPersisted = true;
			$twgTranslation->_isDeleted = false;
		} else {
			throw new Exception("Error ".__METHOD__."(): TwgTranslation is new, so can not be from DB", 1);
		}
	}
	protected function twg_isFromDb(TwgTranslation $twgTranslation) { return isset($twgTranslation->_fromDb) ? $twgTranslation->_fromDb : false; }



	protected function twg_setPersisted(TwgTranslation $twgTranslation) {
		$this->getEntityManager()->persist($twgTranslation);
		$twgTranslation->_isPersisted = true;
		$twgTranslation->_isDeleted = false;
	}
	protected function twg_isPersisted(TwgTranslation $twgTranslation) { return $twgTranslation->_isPersisted; }

	protected function twg_setDeleted(TwgTranslation $twgTranslation) {
		if($this->twg_isNew($twgTranslation)) {
			if($this->twg_isPersisted($twgTranslation)) $this->getEntityManager()->detach($twgTranslation);
		} else {
			$this->getEntityManager()->remove($twgTranslation);
		}
		$twgTranslation->_isPersisted = false;
		$twgTranslation->_isDeleted = true;
	}
	protected function twg_setUndeleted(TwgTranslation $twgTranslation) {
		$twgTranslation->_isDeleted = false;
		$this->twg_setPersisted($twgTranslation);
	}
	protected function twg_isDeleted(TwgTranslation $twgTranslation) { return $twgTranslation->_isDeleted; }

	protected function is_entityManaged(TwgTranslation $twgTranslation) { return $this->getEntityManager()->contains($twgTranslation); }



	/*************************************************************************************/
	/*** PROPERTIES
	/*************************************************************************************/

	public function getTranslatableProperties() {
		return $this->serviceTranslation->getTranslatableProperties($this->entity);
	}

	public function getProperties() {
		$this->ExceptionIfNotInitialized(__METHOD__);
		return $this->properties;
	}

	public function getPropertiesNames($properties = []) {
		$this->ExceptionIfNotInitialized(__METHOD__);
		if(is_string($properties)) $properties = [$properties];
		if(!is_array($properties)) throw new Exception("Error ".__METHOD__."(): property must be string or array of string. ".gettype($properties)." of value ".json_encode($properties)." given!", 1);
		if(!count($properties)) $properties = array_keys($this->properties);
		foreach ($properties as $property) $this->hasProperty($property, true);
		return $properties;
	}

	public function getPropertyData($property) {
		$this->hasProperty($property, true);
		return $this->properties[$property];
	}

	public function hasProperty($property, $makeException = false) {
		$this->ExceptionIfNotInitialized(__METHOD__);
		$has = array_key_exists($property, $this->properties);
		if(!$has && $makeException) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property)." does not exist!", 1);
		return $has;
	}


	/*************************************************************************************/
	/*** TEXT
	/*************************************************************************************/

	public function isTranslated($properties = []) {
		foreach ($this->getPropertiesNames($properties) as $property) {
			if($this->isValidKeytext($this->getEntityValue($property))) return false;
		}
		return true;
	}

	public function translate($properties = []) {
		$this->compileAllValues();
		foreach ($this->getPropertiesNames($properties) as $property) {
			$value = $this->getEntityValue($property);
			if($this->isValidKeytext($value)) {
				$this->setEntityValue($property, $this->properties[$property]['value'][$this->getCurrentLocale()]['value']);
			}
		}
		if(!$this->isTranslated()) throw new Exception("Error ".__METHOD__."(): translation of entity ".$this->entity." is not complete!", 1);
		$this->compileAllValues();
		// $this->controlEntity(__METHOD__);
		return $this;
	}


	/*************************************************************************************/
	/*** CODE
	/*************************************************************************************/

	public function getCode($property) {
		$this->hasProperty($property, true);
		return $this->properties[$property]['keytext'];
	}

	public function isEncoded($properties = []) {
		foreach ($this->getPropertiesNames($properties) as $property) {
			if(!$this->isValidKeytext($this->getEntityValue($property))) return false;
		}
		return true;
	}

	public function encode($properties = []) {
		$this->compileAllValues();
		foreach ($this->getPropertiesNames($properties) as $property) {
			$value = $this->getEntityValue($property);
			if(!$this->isValidKeytext($value)) {
				$this->isValidKeytext($this->properties[$property]['keytext'], __METHOD__);
				$this->setEntityValue($property, $this->properties[$property]['keytext']);
				$this->properties[$property]['value'][$this->getCurrentLocale()]['value'] = $this->getTextOrUndefined($value);
			}
		}
		if(!$this->isEncoded()) throw new Exception("Error ".__METHOD__."(): encoding of entity ".$this->entity." is not complete!", 1);
		$this->compileAllValues();
		// $this->controlEntity(__METHOD__);
		return $this;
	}


	/*************************************************************************************/
	/*** INITIALIZATION
	/*************************************************************************************/

	protected function controlEntity($methodIfException = null) {
		if(static::SHORTCUT_CONTROL) return $this;
		if(!is_string($methodIfException)) $methodIfException = __METHOD__;
		$this->ExceptionIfNotInitialized($methodIfException);
		if($this->isInicomplete()) {
			// IS INIT COMPLETE
			foreach ($this->getProperties() as $property => $data) {
				// CONTROL GETTER & SETTER
				if(!is_string($data['getter'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": GETTER is not valid! Got ".gettype($data['getter'])." of value ".json_encode($data['getter'])."!", 1);
				if(static::DEEP_CONTROL && !method_exists($this->entity, $data['getter'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": GETTER ".json_encode($data['getter'])." does not exist!", 1);
				if(!is_string($data['setter'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": SETTER is not valid! Got ".gettype($data['setter'])." of value ".json_encode($data['setter'])."!", 1);
				if(static::DEEP_CONTROL && !method_exists($this->entity, $data['setter'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": SETTER ".json_encode($data['setter'])." does not exist!", 1);
				// DOMAIN
				if(!is_string($data['domain'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": DOMAIN is not valid! Got ".gettype($data['domain'])." of value ".json_encode($data['domain'])."!", 1);
				// CONTROL KEYTEXT
				if(!$this->isValidKeytext($data['keytext'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": KEYTEXT DATA is not valid! Got ".json_encode($data['keytext'])."!", 1);
				// CONTROL VALUES
				if(!is_array($data['value'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": VALUE DATA is not array! Got ".gettype($data['value'])."!", 1);
				if(static::DEEP_CONTROL) {
					foreach ($this->getLocales() as $locale) {
						if(!array_key_exists($locale, $data['value'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": VALUE DATA of locale ".json_encode($locale)." is missing!", 1);
						if(!array_key_exists('value', $data['value'][$locale])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value of locale ".json_encode($locale)." does not exist in VALUE DATA!", 1);
						if($this->isValidKeytext($data['value'][$locale]['value'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value of locale ".json_encode($locale)." can not be a keytext!", 1);
						if(empty($data['value'][$locale]['value'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value of locale ".json_encode($locale)." can not be a null! Needs to be string or UNDEFINED_VALUE!", 1);
						if(empty($data['value'][$locale]['twg'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": TwgTranslation DATA of locale ".json_encode($locale)." must instance of TwgTranslation! Got ".json_encode($data['value'][$locale]['twg']).".", 1);
						if($data['value'][$locale]['twg'] instanceOf TwgTranslation && $data['keytext'] !== $data['value'][$locale]['twg']->getKeytext()) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property)." of locale ".json_encode($locale)." DATA keytext is not equal to TwgTranslation!", 1);
					}
				}
				// TWGTRANSLATIONS
				if(static::DEEP_CONTROL) {
					$controls = [];
					foreach ($this->getAllTwgTranslations() as $twgTranslation) {
						if($this->isValidKeytext($twgTranslation->getContent())) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": content of translation can not be a code!", 1);
						if(!array_key_exists($twgTranslation->getIdAnyway(), $controls) && in_array($twgTranslation->getUniqkeychain(), $controls)) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": duplicate translation in this entity!", 1);
						$controls[$twgTranslation->getIdAnyway()] = $twgTranslation->getUniqkeychain();
					}
				}
			}
		} else if($this->isOnlyInitialized()) {
			// IS JUST INITIALIZED
			foreach ($this->getProperties() as $property => $data) {
				// CONTROL GETTER & SETTER
				if(!is_string($data['getter'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": GETTER is not valid! Got ".gettype($data['getter'])." of value ".json_encode($data['getter'])."!", 1);
				if(static::DEEP_CONTROL && !method_exists($this->entity, $data['getter'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": GETTER ".json_encode($data['getter'])." does not exist!", 1);
				if(!is_string($data['setter'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": SETTER is not valid! Got ".gettype($data['setter'])." of value ".json_encode($data['setter'])."!", 1);
				if(static::DEEP_CONTROL && !method_exists($this->entity, $data['setter'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": SETTER ".json_encode($data['setter'])." does not exist!", 1);
				// DOMAIN
				if(!is_string($data['domain'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": DOMAIN is not valid! Got ".gettype($data['domain'])." of value ".json_encode($data['domain'])."!", 1);
				// CONTROL KEYTEXT
				if(!$this->isValidKeytext($data['keytext'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": KEYTEXT DATA is not valid! Got ".json_encode($data['keytext'])."!", 1);
				// CONTROL VALUES
				if(!array_key_exists('value', $data)) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": VALUE DATA does not exist!", 1);
				if(!is_array($data['value'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": VALUE DATA is not array! Got ".gettype($data['value'])."!", 1);
				if(!array_key_exists('value', $data['value'][$this->getCurrentLocale()])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value of CURRENT locale ".json_encode($this->getCurrentLocale())." does not exist in VALUE DATA!", 1);
				if(static::DEEP_CONTROL) {
					foreach ($this->getLocales() as $locale) {
						if(!array_key_exists($locale, $data['value'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": VALUE DATA of locale ".json_encode($locale)." is missing!", 1);
						if(!array_key_exists('value', $data['value'][$locale])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value of locale ".json_encode($locale)." does not exist in VALUE DATA!", 1);
						if($this->isValidKeytext($data['value'][$locale]['value'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value of locale ".json_encode($locale)." can not be a keytext!", 1);
						if(empty($data['value'][$locale]['value'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value of locale ".json_encode($locale)." can not be a null! Needs to be string or UNDEFINED_VALUE!", 1);
						if($data['value'][$locale]['twg'] instanceOf TwgTranslation) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property)." of locale ".json_encode($locale)." can not be TwgTranslation, must be null!", 1);
					}
				}
				// TWGTRANSLATIONS
				if(!$this->getAllTwgTranslations()->isEmpty()) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": TwgTranslations loaded must be empty!", 1);
			}
		}
		return $this;
	}

	protected function initialize() {
		$this->twgTrequests = [];
		$this->ExceptionIfNotInitializing(__METHOD__);
		foreach ($this->getTranslatableProperties() as $property => $annotation) {
			$this->properties[$property] = [];
			// Getter
			$this->properties[$property]['getter'] = $getter = is_string($annotation->getter) && strlen($annotation->getter) > 3 ? $annotation->getter : Inflector::camelize('get_'.$property);
			// if(null === $annotation->getter) $annotation->getter = Inflector::camelize('get_'.$property);
			// $getter = serviceClasses::getPropertyMethod($property, $this->entity, $annotation->getter);
			// if(!is_string($getter)) throw new Exception("Error ".__METHOD__."(): getter method could not be found for entity ".$this->entity->getShortname()."!", 1);
			// $this->properties[$property]['getter'] = $getter;
			// Setter
			$this->properties[$property]['setter'] = $setter = is_string($annotation->setter) && strlen($annotation->setter) > 3 ? $annotation->setter : Inflector::camelize('set_'.$property);
			// if(null === $annotation->setter) $annotation->setter = Inflector::camelize('set_'.$property);
			// $setter = serviceClasses::getPropertyMethod($property, $this->entity, $annotation->setter);
			// if(!is_string($setter)) throw new Exception("Error ".__METHOD__."(): setter method could not be found for entity ".$this->entity->getShortname()."!", 1);
			// $this->properties[$property]['setter'] = $setter;
			// Domain
			$this->properties[$property]['domain'] = $annotation->domain;
			if($this->properties[$property]['domain'] === 'auto') $this->properties[$property]['domain'] = $this->entity->getShortname();
			if($this->properties[$property]['domain'] === 'default') $this->properties[$property]['domain'] = serviceTwgTranslation::DEFAULT_DOMAIN;
			// Keytext
			$this->properties[$property]['keytext'] = null;
			// Locale
			// $this->properties[$property]['original_locale'] = $this->getCurrentLocale();
			// Value
			// $this->properties[$property]['original_value'] = $this->getEntityValue($property);
			$this->properties[$property]['value'] = [$this->getCurrentLocale() => ['twg' => null, 'value' => static::UNDEFINED_VALUE]];
			// $this->addValuesToProperty($property, $this->properties[$property]['original_value'], $this->getCurrentLocale());
			$this->addValuesToProperty($property, $this->getEntityValue($property), $this->getCurrentLocale());
			$this->entity->$setter($this->properties[$property]['keytext']);
			// Control
			$errorMsg = $this->isValidKeytext($this->getEntityValue($property), __METHOD__);
			if($errorMsg !== true) {
				$this->serviceTranslation->addFlashToastr('error', $errorMsg);
				// $this->entity->$setter(null);
			}
		}
		$this->setInitialized();
		$this->compileAllValues();
		$this->controlEntity(__METHOD__);
		$this->translate();
		return $this;
	}

	protected function addValuesToProperty($property, $values, $locale = null) {
		if(empty($locale)) $locale = $this->getCurrentLocale();
		$this->hasLocale($locale, true);
		if(is_array($values)) {
			// array of [locale => value]
			foreach ($values as $loc => $value) {
				if($this->hasLocale($loc)) {
					if(!is_string($value) && !empty($value)) throw new Exception("Error ".__METHOD__."(): item for property ".json_encode($property)." on ".json_encode(get_class($this->entity))." locale ".json_encode($loc)." in array of locales must be string or null! Got ".json_encode($value)."!", 1);
					// if($this->isValidKeytext($value)) $value = $this->getKeytextLocaleTranslation($value, $this->properties[$property]['domain'], $locale);
					$this->addValuesToProperty($property, $value, $loc);
				}
			}
		} else {
			if(is_object($values)) {
				if($values instanceOf TwgTranslation) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property)." on ".json_encode(get_class($this->entity)).": value passed as argument can not be a TwgTranslation object!", 1);
				throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value passed as argument can not be an object ".json_encode(get_class($values))."!", 1);
			} else if($this->isValidKeytext($values)) {
				// keytext
				if($this->isValidKeytext($this->properties[$property]['keytext']) && $values !== $this->properties[$property]['keytext']) throw new Exception("Error ".__METHOD__."(): this property ".json_encode($property)." on ".json_encode(get_class($this->entity))." has still a keytext!", 1);
				$this->properties[$property]['keytext'] = $values;
				$this->properties[$property]['value'][$locale]['twg'] = null;
				$this->properties[$property]['value'][$locale]['value'] = $this->getTextOrUndefined($this->getKeytextLocaleTranslation($values, $this->properties[$property]['domain'], $locale));
				if($locale !== $this->getDefaultLocale() && !array_key_exists($this->getDefaultLocale(), $this->properties[$property]['value'])) {
					$this->properties[$property]['value'][$this->getDefaultLocale()]['twg'] = null;
					$this->properties[$property]['value'][$this->getDefaultLocale()]['value'] = $this->getTextOrUndefined($this->getKeytextLocaleTranslation($values, $this->properties[$property]['domain'], $this->getDefaultLocale()));
				}
			} else {
				if(!$this->isValidKeytext($this->properties[$property]['keytext'])) $this->properties[$property]['keytext'] = serviceTranslation::getNewUniqueId($this->entity);
				// single value or null (but keytext or array)
				$this->properties[$property]['value'][$locale]['twg'] = null;
				$this->properties[$property]['value'][$locale]['value'] = $this->getTextOrUndefined($values);
				if($locale !== $this->getDefaultLocale() && !array_key_exists($this->getDefaultLocale(), $this->properties[$property]['value'])) {
					$this->properties[$property]['value'][$this->getDefaultLocale()]['twg'] = null;
					$this->properties[$property]['value'][$this->getDefaultLocale()]['value'] = $this->getTextOrUndefined($values);
				}
			}
			// if(!array_key_exists($this->getDefaultLocale(), $this->properties[$property]['value'])) throw new Exception("Error ".__METHOD__."(): default locale data can not be empty!", 1);
			// UNDEFINED_VALUE
			// foreach ($this->getLocales() as $locale) {
			// 	if(!array_key_exists($locale, $this->properties[$property]['value'])) {
			// 		$this->properties[$property]['value'][$locale]['twg'] = null;
			// 		$this->properties[$property]['value'][$locale]['value'] = static::UNDEFINED_VALUE;
			// 	}
			// }
		}
		return $this;
	}

	protected function compileValues($property, $forceRequest = false) {
		if($this->isInitializing()) return $this;
		$value = $this->getEntityValue($property);
		$banned_values = [$this->getCurrentLocale()];
		if(is_object($value)) {
			// is valued : as object
			if($value instanceOf TwgTranslation) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value passed as argument can not be a TwgTranslation object!", 1);
			throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value passed as argument can not be an object ".json_encode(get_class($values))."!", 1);
		} else if($this->isValidKeytext($value)) {
			// is encoded
			$keytext = $value;
			if($keytext !== $this->properties[$property]['keytext']) throw new Exception("Error ".__METHOD__."(): on property ".json_encode($property)." value of entity and data keytext are different!", 1);
			$value = $this->properties[$property]['value'][$this->getCurrentLocale()]['value'];
			$isEncoded = true;
		} else if(is_array($value)) {
			// is valued : as array of [locale => value]
			$keytext = $this->properties[$property]['keytext'];
			foreach ($value as $locale => $val) {
				if($this->hasLocale($locale)) {
					$banned_values[] = $locale;
					$this->properties[$property]['value'][$locale]['value'] = $this->getTextOrUndefined($val);
					if($locale === $this->getCurrentLocale()) $this->setEntityValue($property, $this->properties[$property]['value'][$locale]['value']);
				}
			}
			$value = $this->properties[$property]['value'][$this->getCurrentLocale()]['value'];
			$isEncoded = false;
		} else {
			// is valued : as single value or null
			$keytext = $this->properties[$property]['keytext'];
			// $value = $this->getTextOrNull($value);
			$this->properties[$property]['value'][$this->getCurrentLocale()]['value'] = $this->getTextOrUndefined($value);
			$isEncoded = false;
		}

		if($this->isInicompleting()) {
			// $default_value = array_key_exists($this->getDefaultLocale(), $this->properties[$property]['value']) ? $this->properties[$property]['value'][$this->getDefaultLocale()]['value'] : $this->getTextOrUndefined($value);
			foreach ($this->getLocales() as $locale) {
				// if(!array_key_exists($locale, $this->properties[$property]['value'])) $this->properties[$property]['value'][$locale] = ['twg' => null, 'value' => $default_value];
				if(!array_key_exists($locale, $this->properties[$property]['value'])) $this->properties[$property]['value'][$locale] = ['twg' => null, 'value' => static::UNDEFINED_VALUE];
				if(!($this->properties[$property]['value'][$locale]['twg'] instanceOf TwgTranslation)) {
					$this->properties[$property]['value'][$locale]['twg'] = $this->getTwgTranslationByParams($keytext, $this->properties[$property]['domain'], $locale, true);
				}
				if(!in_array($locale, $banned_values) && !$this->twg_isNew($this->properties[$property]['value'][$locale]['twg'])) {
				// if(!in_array($locale, $banned_values) && null !== $this->properties[$property]['value'][$locale]['twg']->getContent()) {
					$this->properties[$property]['value'][$locale]['value'] = $this->getTextOrUndefined($this->properties[$property]['value'][$locale]['twg']->getContent());
				}
				if($this->getCurrentLocale() === $locale && !$isEncoded) {
					$this->setEntityValue($property, $this->properties[$property]['value'][$locale]['value']);
				}
			}
		}
		return $this;
	}

	protected function compileAllValues() {
		foreach ($this->getPropertiesNames() as $property) $this->compileValues($property);
		$this->controlEntity(__METHOD__);
		return $this;
	}

	protected function doInicomplete() {
		if($this->isInicomplete()) {
			$this->compileAllValues();
		} else {
			$this->setInicompleting();
			$this->compileAllValues();
			$this->setInicomplete();
		}
		return $this;
	}

	/**
	 * Prepare all TwgTranslation before flush
	 * @return boolean
	 */
	protected function combineTwgs() {
		$modifieds = new ArrayCollection();
		if(!$this->isInicomplete()) $this->doInicomplete();
		foreach ($this->getProperties() as $property => $data) {
			// $default_value = $this->properties[$property]['value'][$this->getDefaultLocale()]['value'];
			// Define default locale value
			if(static::DEEP_CONTROL && !static::SHORTCUT_CONTROL) {
				foreach ($this->getLocales() as $locale) {
					$twgTranslation = $this->properties[$property]['value'][$locale]['twg'];
					// controls
					if(!($twgTranslation instanceOf TwgTranslation)) throw new Exception("Error ".__METHOD__."(): TwgTranslation is missing for property ".json_encode($property)." of locale ".json_encode($locale)."!", 1);
					if($this->isValidKeytext($this->properties[$property]['value'][$locale]['value'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": value of locale ".json_encode($locale)." can not be a keytext!", 1);
					if(null === $this->properties[$property]['value'][$locale]['value']) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property)." value can not be null. Only text or UNDEFINED_VALUE is available!", 1);
					if(!$this->isValidKeytext($this->properties[$property]['keytext'])) throw new Exception("Error ".__METHOD__."(): property ".json_encode($property).": keytext ".json_encode($this->properties[$property]['keytext'])." is invalid!", 1);
				}
			}
			// find best value if default is null
			if(static::UNDEFINED_VALUE === $this->properties[$property]['value'][$this->getDefaultLocale()]['value']) {
				if(static::UNDEFINED_VALUE !== $this->properties[$property]['value'][$this->getCurrentLocale()]['value']) {
					$this->properties[$property]['value'][$this->getDefaultLocale()]['value'] = $this->properties[$property]['value'][$this->getCurrentLocale()]['value'];
				} else {
					foreach ($this->getLocales() as $locale) {
						if(static::UNDEFINED_VALUE !== $this->properties[$property]['value'][$locale]['value']) $this->properties[$property]['value'][$this->getDefaultLocale()]['value'] = $this->properties[$property]['value'][$locale]['value'];
					}
				}
			}
			// Remove (set null) all locales if equal to default locale
			foreach ($this->getLocales() as $locale) {
				if($locale !== $this->getDefaultLocale() && $this->properties[$property]['value'][$this->getDefaultLocale()]['value'] === $this->properties[$property]['value'][$locale]['value']) $this->properties[$property]['value'][$locale]['value'] = static::UNDEFINED_VALUE;
			}
			// Control
			if(static::DEEP_CONTROL) $this->controlEntity(__METHOD__);
			// Persist or/and remove
			foreach ($this->getLocales() as $locale) {
				$twgTranslation = $this->properties[$property]['value'][$locale]['twg'];
				if($this->properties[$property]['value'][$locale]['value'] === static::UNDEFINED_VALUE && !$this->twg_isDeleted($twgTranslation)) {
					if($this->twg_isPersisted($twgTranslation)) {
						$this->entity->setUpdated();
						// $modifieds->set(spl_object_hash($this->entity), $this->entity);
					}
					$this->twg_setDeleted($twgTranslation);
					$modifieds->set(spl_object_hash($twgTranslation), $twgTranslation);
					// $modifieds = true;
				} else {
					$value = $this->getTextOrNull($this->properties[$property]['value'][$locale]['value']);
					if(!empty($value) && $twgTranslation->isDifferentThanContent($value)) {
						$twgTranslation->setContent($value);
						$this->properties[$property]['value'][$locale]['value'] = $twgTranslation->getContent();
						$this->twg_setPersisted($twgTranslation);
						$this->entity->setUpdated();
						// $modifieds->set(spl_object_hash($this->entity), $this->entity);
						$modifieds->set(spl_object_hash($twgTranslation), $twgTranslation);
						// $modifieds = true;
					}
				}
			}
		}
		return $modifieds;
	}




}