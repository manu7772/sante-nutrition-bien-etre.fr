<?php
namespace ModelApi\InternationalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use ModelApi\BaseBundle\Annotation\ModelForCreate;
// use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

use ModelApi\AnnotBundle\Annotation\Translatable;
use Symfony\Component\Form\FormBuilderInterface;

// Gedmo
// use Gedmo\Mapping\Annotation as Gedmo;
// use Gedmo\Translatable\Translatable;
// FileBundle
use ModelApi\FileBundle\Entity\Html;
// InternationalBundle
// use ModelApi\InternationalBundle\Entity\Utranslation;
use ModelApi\InternationalBundle\Service\serviceTranslation;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \Exception;
use \DateTime;

/**
 * twg_translation
 * 
 * @ORM\Entity(repositoryClass="ModelApi\InternationalBundle\Repository\TwgTranslationRepository")
 * @ORM\Table(
 *      name="`twg_translation`",
 *		options={"comment":"Twig translations"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_twg_translation",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", delete="ROLE_SUPER_ADMIN")
 */
class TwgTranslation implements CrudInterface {

	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	const DEFAULT_ICON = 'fa-flag-checkered';
	const ENTITY_SERVICE = serviceTwgTranslation::class;
	const CONTROL_CHOICES = ["controle.no" => 0 , "controle.probably" => 1 , "control.needed" => 2];
	const SOURCE_FILE = 'file';
	const SOURCE_TEXT = 'text';

	const TYPE_TEXT = 'text';
	const TYPE_TEXTAREA = 'textarea';
	const TYPE_HTML = 'html';
	const TYPE_NUMERIC = 'numeric';
	const TYPE_DATETIME = 'datetime';
	// const TYPE_FLOAT = 'float';
	// const TYPE_INT = 'integer';
	const SHORTCUT_CONTROLS = true;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="locale", type="string", length=8, nullable=false, unique=false)
	 * @CRUDS\Annotations({
	 * 		@CRUDS\Create(contexts={"saisie_km"}, show=true, update=false)
	 * 		},{
	 * 		@CRUDS\Create(
	 * 			contexts={"simple"},
	 * 			type="ChoiceType",
	 * 			show="ROLE_TRANSLATOR",
	 * 			update="ROLE_TRANSLATOR",
	 * 			order=100,
	 * 			options={"required"=true, "by_reference"=false, "multiple"=false, "expanded"=false, "choices"="auto"},
	 * 			)
	 * 		},{
	 * 		@CRUDS\Update(contexts={"saisie_km"}, show=true, update=false)
	 * 		},{
	 * 		@CRUDS\Update(
	 * 			contexts={"simple"},
	 * 			type="ChoiceType",
	 * 			show="ROLE_TRANSLATOR",
	 * 			update="ROLE_TRANSLATOR",
	 * 			order=100,
	 * 			options={"required"=true, "by_reference"=false, "multiple"=false, "expanded"=false, "disabled"=true, "choices"="auto"},
	 * 			)
	 * 		},{
	 * 		@CRUDS\Show(
	 * 			role=true,
	 * 			order=100
	 * 			)
	 * 		}
	 * 	)
	 *
	 */
	protected $translatableLocale;
	/**
	 * @Annot\InjectLocalesChoices(setter="setTranslatableLocalechoices")
	 */
	protected $translatableLocalechoices;

	/**
	 * @var string
	 * @ORM\Column(name="domain", type="string", length=32, nullable=false, unique=false)
	 * @CRUDS\Annotations({
	 * 		@CRUDS\Create(contexts={"saisie_km"}, show=true, update=false)
	 * 		},{
	 * 		@CRUDS\Create(
	 * 			type="ChoiceType",
	 * 			show=true,
	 * 			update="ROLE_TRANSLATOR",
	 * 			order=200,
	 * 			options={"required"=true, "by_reference"=false, "multiple"=false, "expanded"=false, "choices"="auto"}
	 * 			)
	 * 		},{
	 * 		@CRUDS\Update(contexts={"saisie_km"}, show=true, update=false)
	 * 		},{
	 * 		@CRUDS\Update(
	 * 			type="ChoiceType",
	 * 			show=true,
	 * 			update="ROLE_TRANSLATOR",
	 * 			order=200,
	 * 			options={"required"=true, "by_reference"=false, "multiple"=false, "expanded"=false, "disabled"=true, "choices"="auto"}
	 * 			)
	 * 		},{
	 * 		@CRUDS\Show(
	 * 			role="ROLE_TRANSLATOR",
	 * 			order=200
	 * 			)
	 * 		}
	 * 	)
	 * @ModelForCreate()
	 */
	protected $domain;
	/**
	 * @Annot\InjectDomainsChoices(setter="setDomainchoices")
	 */
	protected $domainchoices;

	/**
	 * @var string
	 * @ORM\Column(name="keytext", type="text", nullable=false, unique=false)
	 * @CRUDS\Annotations({
	 * 		@CRUDS\Create(
	 * 			type="TextType",
	 * 			contexts={"saisie_km"},
	 * 			show="ROLE_TRANSLATOR",
	 * 			update=false,
	 * 			order=300,
	 * 			options={"required"=true, "by_reference"=false}
	 * 			)
	 * 		},{
	 * 		@CRUDS\Create(
	 * 			type="TextType",
	 * 			show="ROLE_TRANSLATOR",
	 * 			update="ROLE_TRANSLATOR",
	 * 			order=300,
	 * 			options={"required"=true, "by_reference"=false}
	 * 			)
	 * 		},{
	 * 		@CRUDS\Update(
	 * 			type="TextType",
	 * 			show="ROLE_TRANSLATOR",
	 * 			update=false,
	 * 			order=300,
	 * 			options={"required"=true, "by_reference"=false}
	 * 			)
	 * 		},{
	 * 		@CRUDS\Show(
	 * 			role="ROLE_TRANSLATOR",
	 * 			order=300
	 * 			)
	 * 		}
	 * 	)
	 */
	protected $keytext;

	/**
	 * @var string
	 * @ORM\Column(name="content", type="text", nullable=true)
	 * @CRUDS\Annotations({
	 * 		@CRUDS\Create(
	 * 			type="auto",
	 * 			show="ROLE_TRANSLATOR",
	 * 			update="ROLE_TRANSLATOR",
	 * 			order=350,
	 * 			options={"required"=false, "by_reference"=false}
	 * 			)
	 * 		},{
	 * 		@CRUDS\Update(
	 * 			type="auto",
	 * 			show="ROLE_TRANSLATOR",
	 * 			update="ROLE_TRANSLATOR",
	 * 			order=350,
	 * 			options={"required"=false, "by_reference"=false}
	 * 			)
	 * 		},{
	 * 		@CRUDS\Show(
	 * 			role="ROLE_TRANSLATOR",
	 * 			order=350
	 * 			)
	 * 		}
	 * 	)
	 * @Annot\AddModelTransformer(method="AddModelTransformerContent")
	 */
	protected $content;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Html")
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"multiple"=false, "required"=false, "by_reference"=false, "class"="ModelApi\FileBundle\Entity\Html", "query_builder"="auto"}
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"multiple"=false, "required"=false, "by_reference"=false, "class"="ModelApi\FileBundle\Entity\Html", "query_builder"="auto"}
	 * )
	 * @CRUDS\Show(role=true, order=400)
	 */
	protected $htmlfile;

	/**
	 * @var boolean
	 * @ORM\Column(name="keeplink", type="boolean")
	 * @CRUDS\Create(
	 * 		type="switcheryType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=401,
	 * 		options={"required"=false, "by_reference"=false}
	 * )
	 * @CRUDS\Update(
	 * 		type="switcheryType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=401,
	 * 		options={"required"=false, "by_reference"=false}
	 * )
	 */
	protected $keeplink;

	/**
	 * @var integer
	 * @ORM\Column(name="needcontrol", type="integer")
	 * @CRUDS\Create(
	 * 		type="choiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=398,
	 * 		options={"multiple"=false, "expanded"=true, "choices"="auto", "required"=true, "by_reference"=false}
	 * )
	 * @CRUDS\Update(
	 * 		type="choiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=398,
	 * 		options={"multiple"=false, "expanded"=true, "choices"="auto", "required"=true, "by_reference"=false}
	 * )
	 */
	protected $needcontrol;

	/**
	 * @var string
	 * @ORM\Column(name="typecontent", type="string", length=8, nullable=true, unique=false)
	 * @CRUDS\Annotations({
	 * 		@CRUDS\Create(
	 * 			type="ChoiceType",
	 * 			show=true,
	 * 			update=true,
	 * 			order=1000,
	 * 			options={"required"=true, "by_reference"=false, "multiple"=false, "expanded"=false, "choices"="auto"},
	 * 			)
	 * 		},{
	 * 		@CRUDS\Update(
	 * 			type="ChoiceType",
	 * 			show=true,
	 * 			update=true,
	 * 			order=1000,
	 * 			options={"required"=true, "by_reference"=false, "multiple"=false, "expanded"=false, "choices"="auto"},
	 * 			)
	 * 		},{
	 * 		@CRUDS\Show(
	 * 			role=true,
	 * 			order=1000
	 * 			)
	 * 		}
	 * 	)
	 * @ModelForCreate()
	 */
	protected $typecontent;

	/**
	 * @var string
	 * @ORM\Column(name="source", type="string", length=16, nullable=false, unique=false)
	 */
	protected $source;

	/**
	 * @var string
	 * @ORM\Column(name="fa_icon", type="string", length=64, nullable=false, unique=false)
	 */
	protected $icon;


	public function __construct() {
		$this->id = null;
		$this->translatableLocale = null;
		$this->translatableLocalechoices = [];
		$this->domain = serviceTwgTranslation::DEFAULT_DOMAIN;
		$this->domainchoices = [];
		$this->keytext = null;
		$this->content = null;
		$this->htmlfile = null;
		$this->keeplink = false;
		$this->needcontrol = 0;
		$this->typecontent = null;
		$this->source = null;
		$this->setIcon(static::DEFAULT_ICON);
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		// return $this->getContent();
		return $this->domain.'@'.$this->id;
	}

	public function getName() {
		return $this->__toString();
	}

	/**
	 * Is valid
	 * @return boolean
	 */
	public function isValid() {
		return
			$this->isValidContent()
			&& is_string($this->translatableLocale)
			&& is_string($this->domain)
			&& is_string($this->keytext)
			// && (is_string($this->content) || $this->isSourceFile())
			;
	}

	/**
	 * getSerialized
	 */
	public function serialize($asArray = false) {
		$data = array(
			'id' => $this->id,
			'translatableLocale' => $this->translatableLocale,
			'domain' => $this->domain,
			'keytext' => $this->keytext,
			'content' => $this->content,
			'typecontent' => $this->typecontent,
		);
		return $asArray ? $data : serialize($data);
	}

	public static function getUniqkeychainWithData($keytext = null, $domain = null, $translatableLocale = null) {
		return implode(' / ', [$keytext, $domain, $translatableLocale]);
	}

	public function getUniqkeychain() {
		return static::getUniqkeychainWithData($this->keytext, $this->domain, $this->translatableLocale);
	}

	/**
	 * @ORM\PostUpdate()
	 * @ORM\PostPersist()
	 */
	public function PostPersistPostUpdate_TwgTranslation(LifecycleEventArgs $args) {
		$this->_origin_data = $this->serialize();
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function PreUpdate_TwgTranslation(PreUpdateEventArgs $args) {
		$this->PrePersist_TwgTranslation();
		// $origin = $args->getEntityChangeSet();
		// YamlLog::registerYamlLogfile(__METHOD__, true, ['entity' => $this->getShortname(), 'id' => $this->getId(), "changes" => $origin]);
		// Si needcontrol n'a pas été modifié volontairement...
		// if(isset($origin['needcontrol'])) {
		// 	switch (implode('|', $origin['needcontrol']) {
		// 		case '2|1':
		// 			if(isset($origin['content'])) $this->setNeedcontrol(0);
		// 			break;
				
		// 		default:
		// 			# code...
		// 			break;
		// 	}
		// }
		// if($this->getNeedcontrol() > 1) $this->setNeedcontrol(1);
		// $this->setNeedcontrol(2);
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 */
	public function PrePersist_TwgTranslation() {
		if(!$this->keeplink) {
			$this->content = $this->getContent();
			$this->htmlfile = null;
		}
		$this->defineSource();
		$this->getTypeContent();
		$this->defaultIconIfNone();
		return $this;
	}

	/**
	 * @ORM\PostLoad()
	 */
	public function PostLoad_TwgTranslation() {
		$this->_origin_data = $this->serialize();
		// $this->getHtmlfile();
		// if($this->getHtmlfile() instanceOf Html) $this->htmlfile->getCurrentVersion()->getBinaryFile();
		// if($this->getNeedcontrol() > 1) $this->setNeedcontrol(1);
		// $this->setNeedcontrol(2);
		return $this;
	}

	public function hasChanged() {
		return isset($this->_origin_data) ? $this->_origin_data !== $this->serialize() : true;
	}

	/**
	 * Get id
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	// public function fillWithUtranslation(Utranslation $Utranslation) {
	// 	if($Utranslation->isValid()) {
	// 		foreach ($Utranslation->getTranslatedTexts() as $locale => $content) {
	// 			$Utranslation->getTranslationRepository()->translate($this, 'content', $locale, $content);
	// 		}
	// 	} else {
	// 		throw new Exception("Error ".__METHOD__."(): Utranslation is not valid: ".$errors->__toString(), 1);
	// 	}
	// 	return $this;
	// }

	/**
	 * Get translatable locale of entity
	 * @return string | null
	 */
	public function getTranslatableLocale() {
		return $this->translatableLocale;
	}

	/**
	 * Set translatable locale of entity
	 * @param string $translatablelocale
	 * @return TwgTranslation
	 */
	public function setTranslatableLocale($translatableLocale) {
		$this->translatableLocale = $translatableLocale;
		return $this;
	}

	/**
	 * Set translatable locale choices
	 * @param array $translatableLocalechoices
	 * @return TwgTranslation
	 */
	public function setTranslatableLocalechoices($translatableLocalechoices) {
		$this->translatableLocalechoices = $translatableLocalechoices;
		return $this;
	}

	/**
	 * Get translatable locale choices
	 * @return array
	 */
	public function getTranslatableLocalechoices() {
		if(!isset($this->translatableLocalechoices) || !is_array($this->translatableLocalechoices)) $this->translatableLocalechoices = [];
		return $this->translatableLocalechoices;
	}

	/**
	 * Get domain
	 * @return string 
	 */
	public function getDomain() {
		return $this->domain;
	}

	/**
	 * Set domain
	 * @param string $domain
	 * @return TwgTranslation
	 */
	public function setDomain($domain) {
		serviceTools::computeTextOrNull($domain);
		$this->domain = $domain;
		return $this;
	}

	/**
	 * Set domain choices
	 * @param array $domainchoices
	 * @return TwgTranslation
	 */
	public function setDomainchoices($domainchoices) {
		$this->domainchoices = $domainchoices;
		// transform
		// $this->domainchoices = [];
		// foreach ($domainchoices as $key => $value) {
		// 	$this->domainchoices[$value.' / '.$key] = $value;
		// }
		return $this;
	}

	/**
	 * Get domain choices
	 * @return array
	 */
	public function getDomainchoices() {
		if(!isset($this->domainchoices) || !is_array($this->domainchoices)) $this->domainchoices = [];
		return $this->domainchoices;
	}

	/**
	 * Get keytext
	 * @return string 
	 */
	public function getKeytext() {
		return $this->keytext;
	}

	/**
	 * Set keytext
	 * @param string $keytext
	 * @return TwgTranslation
	 */
	public function setKeytext($keytext) {
		serviceTools::computeTextOrNull($keytext, false);
		$this->keytext = $keytext;
		return $this;
	}


	public function defineSource() {
		$this->source = $this->getHtmlfile() instanceOf Html ? static::SOURCE_FILE : static::SOURCE_TEXT;
		return $this;
	}

	public function getSource() {
		$this->defineSource();
		return $this->source;
	}

	public function isSourceFile() {
		return $this->source === static::SOURCE_FILE;
	}

	public function isSourceText() {
		return $this->source === static::SOURCE_TEXT;
	}

	// /**
	//  * Set editcontent
	//  * @param string $editcontent = null
	//  * @return TwgTranslation
	//  */
	// public function setEditcontent($editcontent = null) {
	// 	return $this->setContent($editcontent);
	// }

	// /**
	//  * Get editcontent
	//  * @return string 
	//  */
	// public function getEditcontent() {
	// 	// if($this->getHtmlfile() instanceOf Html) return null;
	// 	return $this->getContent();
	// }

	public static function treatContent($content = null) {
		serviceTools::computeTextOrNull($content, false);
		return $content;
	}

	/**
	 * Set content
	 * @param string $content = null
	 * @return TwgTranslation
	 */
	public function setContent($content = null) {
		$this->content = static::treatContent($content);
		$this->defineSource();
		return $this;
	}

	/**
	 * Get content
	 * @return string 
	 */
	public function getContent() {
		if($this->getHtmlfile() instanceOf Html && $this->getKeeplink()) {
			return $this->getHtmlfile()->getCurrentVersion()->getBinaryFile();
		}
		return $this->content;
	}

	public function AddModelTransformerContent($propertyName, FormBuilderInterface $formBuilder) {
		$translatable = new Translatable();
		$translatable->getter = 'getContent';
		$translatable->setter = 'setContent';
		$translatable->domain = $this->getDomain();
		$translatable->type = $this->getTypeContent();
		return $translatable->addModelTransformer($propertyName, $formBuilder);
	}

	public function setTypecontent($typecontent) {
		$this->typecontent = $typecontent;
		$typecontents = $this->getTypeContents();
		if(!in_array($this->typecontent, $typecontents)) $this->typecontent = $this->getDefaultTypecontent();
		return $this;
	}

	public function getDefaultTypecontent() {
		return static::TYPE_TEXTAREA;
	}

	public function getTypeContents() {
		return [static::TYPE_TEXT, static::TYPE_TEXTAREA, static::TYPE_HTML, static::TYPE_NUMERIC, static::TYPE_DATETIME];
	}

	public function getTypeContentChoices() {
		return array(
			'Texte simple' => static::TYPE_TEXT,
			'Zone de texte' => static::TYPE_TEXTAREA,
			'Texte enrichi (html)' => static::TYPE_HTML,
			// 'Nombre' => static::TYPE_NUMERIC,
			// 'Date/heure' => static::TYPE_DATETIME,
		);
	}

	public function getTypeContent() {
		if(!empty($this->typecontent)) return $this->typecontent;
		$content = $this->getContent();
		switch (gettype($content)) {
			case 'string':
				$this->typecontent = $content !== strip_tags($content) ? static::TYPE_HTML : static::TYPE_TEXT;
				break;
			case 'integer':
			case 'double':
				$this->typecontent = static::TYPE_NUMERIC;
				break;
			case 'object':
				if($content instanceOf DateTime) $this->typecontent = static::TYPE_DATETIME;
				break;
			default:
				$this->typecontent = $this->getDefaultTypecontent();
				break;
		}
		return $this->typecontent;
	}

	/**
	 * Get TYPE for update (used by Cruds while parameter is "auto")
	 * @return string
	 */
	public function getUpdateCrudType() {
		switch ($this->getTypeContent()) {
			case static::TYPE_TEXT: return 'TextType'; break;
			case static::TYPE_TEXTAREA: return 'TextareaType'; break;
			case static::TYPE_HTML: return 'SummernoteType'; break;
			case static::TYPE_NUMERIC: return 'NumberType'; break;
			case static::TYPE_DATETIME: return 'DateTimeType'; break;
			default: return 'TextType'; break; // TYPE NULL
		}
	}

	public function isTextType() {
		return $this->getTypeContent() === static::TYPE_TEXT;
	}

	public function isTextareaType() {
		return $this->getTypeContent() === static::TYPE_TEXTAREA;
	}

	public function isHtmlType() {
		return $this->getTypeContent() === static::TYPE_HTML;
	}

	public function isNumericType() {
		return $this->getTypeContent() === static::TYPE_NUMERIC;
	}

	public function isDatetimeType() {
		return $this->getTypeContent() === static::TYPE_DATETIME;
	}



	public function isValidContent($validIfNull = false) {
		return $this->hasContent() ? serviceTranslation::isUniqueId($this->getContent(), $this) : $validIfNull;
	}

	public function isEqualThanContent($content = null) {
		return static::treatContent($content) === $this->getContent();
	}

	public function isDifferentThanContent($content = null) {
		return static::treatContent($content) != $this->getContent();
	}

	public function hasContent() {
		return !empty($this->getContent());
	}

	/**
	 * Set htmlfile
	 * @param Html $htmlfile = null
	 * @return TwgTranslation
	 */
	public function setHtmlfile(Html $htmlfile = null) {
		$this->htmlfile = $htmlfile;
		$this->defineSource();
		return $this;
	}

	/**
	 * Get htmlfile
	 * @return Html | null 
	 */
	public function getHtmlfile() {
		return $this->htmlfile;
	}

	public function setKeeplink($keeplink) {
		$this->keeplink = $keeplink;
		return $this;
	}

	public function getKeeplink() {
		return $this->keeplink;
	}

	/**
	 * Set default icon if not defined
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return self
	 */
	public function defaultIconIfNone() {
		if(!is_string($this->getIcon())) $this->setIcon(static::DEFAULT_ICON);
		return $this;
	}

	/**
	 * Set icon
	 * @param string $icon = null
	 * @return self
	 */
	public function setIcon($icon = null) {
		$this->icon = is_string($icon) ? $icon : static::DEFAULT_ICON;
		return $this;
	}

	/**
	 * Get icon
	 * @return string
	 */
	public function getIcon() {
		return $this->icon;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDEFAULT_ICON() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDefaultIcon() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public function getIconClass() {
		return 'fa '.$this->icon;
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public static function getDefaultIconClass() {
		return 'fa '.static::DEFAULT_ICON;
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public function getIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.$this->getIconClass().$classes.'"></i>';
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public static function getDefaultIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.static::getDefaultIconClass().$classes.'"></i>';
	}

	public function setNeedcontrol($needcontrol) {
		$this->needcontrol = is_string($needcontrol) ? static::CONTROL_CHOICES[$needcontrol] : $needcontrol;
		return $this;
	}

	public function getNeedcontrol() {
		return $this->needcontrol;
	}

	public function getNeedcontrolcolor() {
		switch ($this->needcontrol) {
			case 1: return 'text-warning'; break;
			case 2: return 'text-danger'; break;
		}
		return 'text-info';
	}

	public function getNeedcontrolChoices() {
		return static::CONTROL_CHOICES;
	}

	public function isNeedcontrol() {
		return $this->needcontrol > 0;
	}



}