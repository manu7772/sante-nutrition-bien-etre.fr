<?php
namespace ModelApi\InternationalBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
// use Symfony\Component\HttpKernel\Event\GetResponseEvent;
// use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
// use Symfony\Component\HttpFoundation\Request;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
use ModelApi\InternationalBundle\Service\serviceTranslation;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
use ModelApi\InternationalBundle\Entity\Language;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

// https://symfony.com/doc/3.4/components/filesystem.html
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class LanguageListener {

	const TRANSLATIONS_DIR = 'translations/';

	protected $container;
	protected $Filesystem;
	protected $reloaded;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->Filesystem = new Filesystem();
		$this->reloaded = false;
		return $this;
	}

	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function PostPersistAndUpdate(Language $language, LifecycleEventArgs $event) {
		$filename = serviceTwgTranslation::DEFAULT_DOMAIN.'.'.$language.'.'.serviceTranslation::BDD_FILE_EXT;
		$file = $this->container->get(serviceLanguage::class)->getAppTranslationsDir().$filename;
		if(!$this->Filesystem->exists($file)) {
			$this->Filesystem->dumpFile($file, '');
			$this->Filesystem->chmod($file, 0755);
		}
		if(!$this->reloaded) {
			$this->container->get(serviceLanguage::class)->getAllLanguages(true);
			$this->reloaded = true;
		}
		return $this;
	}

	/**
	 * @ORM\PostRemove()
	 */
	public function PostRemove(Language $language, LifecycleEventArgs $event) {
		$filename = serviceTwgTranslation::DEFAULT_DOMAIN.'.'.$language.'.'.serviceTranslation::BDD_FILE_EXT;
		$this->Filesystem = new Filesystem();
		$file = $this->container->get(serviceLanguage::class)->getAppTranslationsDir().$filename;
		if($this->Filesystem->exists($file)) $this->Filesystem->remove($file);
		if(!$this->reloaded) {
			$this->container->get(serviceLanguage::class)->getAllLanguages(true);
			$this->reloaded = true;
		}
		return $this;
	}





}