<?php
namespace ModelApi\InternationalBundle\Listener;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
// use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
// use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
// use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
// use Symfony\Component\Security\Core\Event\AuthenticationEvent;
// use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
// use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
// use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\AcceptHeader;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceKernel;
// use ModelApi\BaseBundle\Service\serviceRoute;
// use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceContext;
// use ModelApi\BaseBundle\Service\serviceFlashbag;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
// use ModelApi\InternationalBundle\Entity\Language;
// use ModelApi\InternationalBundle\Service\serviceTranslation;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// UserBundle
// use ModelApi\UserBundle\Entity\Entreprise;
// use ModelApi\UserBundle\Service\serviceEntreprise;
// use ModelApi\UserBundle\Service\serviceTier;
// EventBundle
// use ModelApi\EventBundle\Entity\Event;
// DevprintBundle
// use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \Exception;
use \ReflectionClass;

class localeListener {

	use \ModelApi\BaseBundle\Service\baseService;

	private $container;
	private $serviceLanguage;
	private $serviceKernel;
	private $serviceContext;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceLanguage = $this->container->get(serviceLanguage::class);
		$this->serviceKernel = $this->container->get(serviceKernel::class);
		$this->serviceContext = $this->container->get(serviceContext::class);
		return $this;
	}

	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString() {
		return serviceClasses::getShortname(static::class);
	}

	public function loadLanguagesBeforeAll(GetResponseEvent $event) {
		// Request
		$request = $event->getRequest();
		if($request instanceOf Request) {
			if(HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
				if(!$request->isXmlHttpRequest()) {
					// REQUEST create CONTEXT
					$current_context = $this->serviceContext->init_by_request($request);

					// save in serviceKernel
					$this->serviceKernel->setLocale($current_context->getLocale());
					$this->serviceKernel->setDefaultLocale($current_context->getDefaultLocale());
					// PHP system locale
					setlocale(LC_TIME, $this->serviceKernel->getLocale(true));
					// Translator
					$this->container->get('translator')->setLocale($current_context->getLocale());

					$session = $request->getSession();
					if($session instanceOf Session) {
						// TWIG service
						$twig = $this->container->get('twig');
						$twig->addGlobal('Languages', $current_context->getLanguages());
						$twig->addGlobal('currentLanguage', $current_context->getCurrentLanguage());
						$twig->addGlobal('defaultLanguage', $current_context->getDefaultLanguage());
					}
					// if($current_context->version_lt("2.0.0")) {
					// 	// REQUEST post operations in versions < 2.0.0
					// 	if($session instanceOf Session) $twig->addGlobal('Languages_actifs', $current_context->getLanguages());
					// 	// set in request
					// 	$request->setLocale($current_context->getLocale());
					// 	$request->setDefaultLocale($current_context->getDefaultLocale());
					// }
				}
			}
		}
	}



}