<?php
namespace ModelApi\InternationalBundle\Listener;

// use Doctrine\ORM\Mapping as ORM;
// use Symfony\Component\HttpKernel\Event\GetResponseEvent;
// use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
// use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Events;
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
use ModelApi\InternationalBundle\Service\serviceTranslation;
use ModelApi\InternationalBundle\Entity\TwgTranslation;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

// https://symfony.com/doc/3.4/components/filesystem.html
// use Symfony\Component\Filesystem\Filesystem;
// use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class TwgTranslationSubscriber implements EventSubscriber {

	use \ModelApi\BaseBundle\Service\baseService;

	const RESET_CACHE_TRANSLATIONS = true;

	protected $container;
	protected $twg_domains;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->twg_domains = [];
		return $this;
	}

	/**
	 * Master running Events
	 */
	public function getSubscribedEvents() {
		return array(
			// BaseAnnotation::postNew,
			// BaseAnnotation::preCreateForm,
			// BaseAnnotation::preUpdateForm,
			// Events::postLoad,
			// Events::prePersist,
			// Events::postPersist,
			// Events::preUpdate,
			// Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			Events::onFlush,
			Events::postFlush,
			// Events::onClear,
		);
	}

	public function onFlush(OnFlushEventArgs $args) {
		$uow = $args->getEntityManager()->getUnitOfWork();
		if(static::RESET_CACHE_TRANSLATIONS) {
			foreach ($uow->getScheduledEntityInsertions() as $entity) {
				if($entity instanceOf TwgTranslation) $this->twg_domains[$entity->getDomain()] = $entity->getDomain();
			}
			foreach ($uow->getScheduledEntityUpdates() as $entity) {
				if($entity instanceOf TwgTranslation) $this->twg_domains[$entity->getDomain()] = $entity->getDomain();
			}
			foreach ($uow->getScheduledEntityDeletions() as $entity) {
				if($entity instanceOf TwgTranslation) $this->twg_domains[$entity->getDomain()] = $entity->getDomain();
			}
		}
	}

	public function postFlush(PostFlushEventArgs $args) {
		if(static::RESET_CACHE_TRANSLATIONS && count($this->twg_domains)) $this->container->get(serviceLanguage::class)->resetCacheLanguages($this->twg_domains);
		$this->twg_domains = [];
	}




}