<?php
namespace ModelApi\InternationalBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use \Twig_Extension;
use \Twig_SimpleFilter;
use \Twig_SimpleFunction;
use JMS\Serializer\SerializationContext;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\TwgTranslation;
use ModelApi\InternationalBundle\Service\serviceTranslation;

// use \DateTime;
use \Twig_Environment;
use \ReflectionClass;
use \ReflectionMethod;
use \Exception;

class TwigTranslation extends Twig_Extension implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	private $container;
	private $translator;
	private $serviceTranslation;
	// private $twig;
	// private $filters;
	// private $raw;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->translator = $this->container->get('translator');
		// $this->twig = $this->container->get('twig');
		$this->serviceTranslation = $this->container->get(serviceTranslation::class);
		return $this;
	}


	public function getFunctions() {
		return array(
			// functions
			new Twig_SimpleFunction('isTranslatator', array($this, 'isTranslatator')),
			new Twig_SimpleFunction('translation_mode', array($this, 'getTranslationMode')),
			// new Twig_SimpleFunction('currentBundle', array($this, 'getCurrentBundleName')),
			// new Twig_SimpleFunction('getCurrentBundle', array($this, 'getCurrentBundle')),
			new Twig_SimpleFunction('getTranslationDomain', array($this, 'getTranslationDomain')),
		);
	}


	public function getFilters() {
		$filters = array(
			// filters
			new Twig_SimpleFilter('transno', array($this, 'translate_no')),
			new Twig_SimpleFilter('translatorManaged', array($this, 'getManagedEntities')),
		);
		if($this->serviceTranslation->isTranslatorMode()) {
			// https://twig.symfony.com/doc/1.x/advanced.html#environment-aware-filters
			// https://twig.symfony.com/doc/1.x/advanced.html#automatic-escaping
			$filters[] = new Twig_SimpleFilter('trans', array($this, 'translate'), ['is_safe' => ['html']]);
		} else {
			$filters[] = new Twig_SimpleFilter('trans', array($this, 'translateNoTranslator'), ['is_safe' => ['html']]);
		}
		return $filters;
	}



	////////////////////////////// FUNCTIONS /////////////////

	public function isTranslatator() {
		return $this->serviceTranslation->isTranslatator();
	}

	public function getTranslationMode($asHtml = false) {
		$mode = $this->serviceTranslation->isTranslatorMode();
		$string = $mode ? '<i class="fa fa-check fa-fw text-info"></i>' : '<i class="fa fa-times fa-fw text-danger"></i>';
		return $asHtml ? $string : $mode;
	}

	public function getTranslationDomain() {
		return $this->serviceTranslation->getTranslationDomain();
	}


	////////////////////////////// FILTERS /////////////////

	/**
	 * Translate and add icon link if Live Translator mode is true
	 * @param string $string
	 * @param string $values = []
	 * @param string $domain = null
	 * @param string $locale = null
	 * @return string
	 */
	public function translate($string, $values = [], $domain = null, $locale = null) {
		if(null === $domain) $domain = $this->serviceTranslation->getTranslationDomain();
		$string = preg_replace('/^field\\./i', '', $string);
		if($this->serviceTranslation->isTranslatorMode()) {
			$router = $this->container->get('router');
			$url = $router->generate('wsa_twgtranslation_update_bykeys', ['keytext' => urlencode($string), 'domain' => $domain]);
			$translated = '<i class="fa '.TwgTranslation::DEFAULT_ICON.' fa-1x text-warning" data-live-translator=\''.json_encode(['url' => $url]).'\' title="Live translator" style="cursor: pointer;"></i>'.$this->translator->trans($string, $values, $domain);
		} else {
			$translated = $this->translator->trans($string, $values, $domain, $locale);
		}
		return $translated;
	}

	/**
	 * Translate in no translator mode
	 * @param string $string
	 * @param string $values = []
	 * @param string $domain = null
	 * @param string $locale = null
	 * @return string
	 */
	public function translateNoTranslator($string, $values = [], $domain = null, $locale = null) {
		if(null === $domain) $domain = $this->serviceTranslation->getTranslationDomain();
		// $string = preg_replace('/^field\\./i', '', $string);
		return $this->translator->trans($string, $values, $domain, $locale);
	}

	/**
	 * Translate
	 * @param string $string
	 * @param string $values = []
	 * @param string $domain = null
	 * @param string $locale = null
	 * @return string
	 */
	public function translate_no($string, $values = [], $domain = null, $locale = null) {
		if(null === $domain) $domain = $this->serviceTranslation->getTranslationDomain();
		return $this->translator->trans($string, $values, $domain, $locale);
	}

	/**
	 * Is managed by 
	 * @param object $entity = null
	 * @return mixed
	 */
	public function getManagedEntities($entity = null) {
		$manageds = $this->serviceTranslation->getManagedEntities(true);
		if(empty($entity)) return $manageds;
		foreach ($manageds as $managed) {
			if($managed === $entity) return $managed;
		}
		return null;
	}






}