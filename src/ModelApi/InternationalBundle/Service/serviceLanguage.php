<?php
namespace ModelApi\InternationalBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\Language;
use ModelApi\InternationalBundle\Entity\TwgTranslation;
use ModelApi\InternationalBundle\Service\serviceTranslation;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;

// https://symfony.com/doc/3.4/components/filesystem.html
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

// use \DateTime;
use \ReflectionClass;

class serviceLanguage implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Language::class;
	const LANGUAGE_TOKEN_NAME = 'X-Language-Locale';
	const TRANSLATIONS_DIR = 'translations/';
	// const DEFAULT_CACHE_MODE = 'cache'; // ['cache','preload','none']
	const CACHE_CACHE = 'cache';
	const CACHE_PRELOAD = 'preload';
	const CACHE_NONE = 'none';
	// const CACHE_MODES = ['cache','preload','none'];

	protected $container;
	protected $parameterBag;
	// protected $serviceKernel;
	protected $preload_languages;
	protected $cache_mode;


	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->parameterBag = $this->container->getParameterBag();
		// $this->serviceKernel = $this->container->get(serviceKernel::class);
		$this->preload_languages = null;
		$this->setCacheMode('auto');
		$this->initialize(false);
		return $this;
	}



	protected function initialize($reload = false) {
		switch ($this->getCacheMode()) {
			case static::CACHE_CACHE:
				$this->preload_languages = $this->container->get(serviceCache::class)->getCacheSerialized(
					'languages',
					function() {
						return $this->getRepository()->findAll();
					},
					['groups' => ['cached_language'], 'format' => 'json'], // serialization
					'cache_languages', // folder name
					serviceCache::NO_CONTEXT, // context language
					null, // no env.
					serviceCache::CACHE_LONGLIFE, // for long time
					$reload // refresh
				);
				break;
			case static::CACHE_PRELOAD:
				if(empty($this->preload_languages) || $reload) $this->preload_languages = $this->getRepository()->findAll();
				break;
			default:
				// none
				if($reload) $this->preload_languages = $this->getRepository()->findAll();
				break;
		}
		return $this;
	}

	public function getCacheModes() {
		return [static::CACHE_CACHE, static::CACHE_PRELOAD, static::CACHE_NONE];
	}

	public function getDefaultCacheMode() {
		return static::CACHE_CACHE;
	}

	public function getCacheMode() {
		return $this->cache_mode;
	}

	public function isCacheMode() {
		return $this->getCacheMode() === static::CACHE_CACHE;
	}

	public function isPreloadMode() {
		return $this->getCacheMode() === static::CACHE_PRELOAD;
	}

	public function isNoneMode() {
		return $this->getCacheMode() === static::CACHE_NONE;
	}

	public function clearLanguagesCache() {
		if($this->isCacheMode()) {
			$this->container->get(serviceCache::class)->clearCacheData(
				'languages',
				'cache_languages',
				serviceCache::NO_CONTEXT,
				null
			);
		}
		return $this;
	}

	public function setCacheMode($cache_mode = null) {
		if($cache_mode === 'auto') {
			$this->cache_mode = $this->getServiceParameter('cache_mode', $this->getDefaultCacheMode());
		}
		$cache_modes = $this->getCacheModes();
		if(!in_array($this->cache_mode, $cache_modes)) $this->cache_mode = $this->getDefaultCacheMode();
		return $this;
	}


	/**
	 * Get all Languages
	 * @return Array <Language>
	 */
	public function getAllLanguages($reload = false) {
		if(empty($this->preload_languages) || $reload) $this->initialize(true);
		return $this->preload_languages;
	}

	/**
	 * Get default locale
	 * @param boolean $reload = false
	 * @return string
	 */
	public function getDefaultLocale($reload = false) {
		$locale = $this->getPreferedLanguage($reload);
		return $locale instanceOf Language ? (string)$locale : $this->container->getParameter('locale');
	}

	public static function isValidLocale($locale) {
		return preg_match('/^[a-zA-Z]{2}'.Language::COUNTRY_SEPARATOR.'[a-zA-Z]{2,5}$/', $locale);
	}

	/**
	 * Get all Languages
	 * @param boolean $onlyActive = true
	 * @param boolean $reload = false
	 * @return Array <Language>
	 */
	public function getLanguages($onlyActive = true, $reload = false) {
		$languages = $this->getAllLanguages($reload);
		if(!$onlyActive) return $languages;
		$parent = $this;
		return array_filter($languages, function($language) use ($parent) {
			return $parent->language_isActive($language);
		});
	}

	/**
	 * Get all Languages names
	 * @param boolean $onlyActive = true
	 * @param boolean $reload = false
	 * @return Array <string>
	 */
	public function getLanguagesNames($onlyActive = true, $reload = false) {
		$locales = [];
		foreach ($this->getLanguages($onlyActive, $reload) as $language) {
			$locale = $this->language_toString($language);
			if($this->isValidLocale($locale)) $locales[$locale] = $locale;
		}
		return $locales;
	}

	/**
	 * Get all locales
	 * @param boolean $onlyActive = true
	 * @param boolean $reload = false
	 * @return Array <string>
	 */
	public function getLocales($onlyActive = true, $reload = false) {
		$locales = [];
		foreach ($this->getLanguages($onlyActive, $reload) as $language) {
			$locale = $this->language_fullname($language);
			if($this->isValidLocale($locale)) $locales[$locale] = $locale;
		}
		return $locales;
	}

	/**
	 * Get locales for form choiceType
	 * @param boolean $onlyActive = true
	 * @param boolean $reload = false
	 * @return Array <string>
	 */
	public function getLocaleschoices($onlyActive = true, $reload = false) {
		$languages = $this->getLocales($onlyActive, $reload);
		$locales = [];
		foreach ($languages as $locale) $locales[$locale] = $locale;
		return $locales;
	}

	/**
	 * Has language (by name)
	 * @param string $name
	 * @param boolean $onlyActive = true
	 * @param boolean $reload = false
	 * @return boolean
	 */
	public function hasLanguage($name, $onlyActive = true, $reload = false) {
		$names = $this->getLanguagesNames($onlyActive, $reload);
		return in_array($name, $names);
	}

	/**
	 * Get prefered Language
	 * @param boolean $reload = false
	 * @return Language | null
	 */
	public function getPreferedLanguage($reload = false) {
		foreach ($this->getAllLanguages($reload) as $language) {
			if($this->language_isActiveAndPrefered($language)) return $language;
		}
		return null;
	}

	/**
	 * Get prefered Locale
	 * @param boolean $reload = false
	 * @return string | null
	 */
	public function getPreferedLocale($reload = false) {
		return $this->language_fullname($this->getPreferedLanguage($reload));
	}

	/**
	 * Get Language by locale-country
	 * @param string $localeAndCountry = null
	 * @param boolean $onlyActive = true
	 * @param boolean $reload = false
	 * @return Language | null
	 */
	public function getLanguageByLocale($localeAndCountry = null, $onlyActive = true, $reload = false) {
		$language = null;
		if(null != $localeAndCountry) {
			// Récupération de la locale dans le header
			$localeAndCountry = preg_replace("#_#", Language::COUNTRY_SEPARATOR, $localeAndCountry);
			$exp = explode(Language::COUNTRY_SEPARATOR, $localeAndCountry);
			$searchParams = array('name' => strtolower($exp[0]));
			if(true === $onlyActive) $searchParams['enabled'] = 1;
			$searchParamsADD['country'] = count($exp) > 1 ? strtoupper($exp[1]) : strtoupper($exp[0]);
			foreach ($this->getAllLanguages($reload) as $lang) {
				if(false === $onlyActive || $this->language_isActive($lang)) {
					if($this->language_getName($lang) === $searchParams['name'] && $this->language_getCountry($lang) === $searchParamsADD) return $lang;
					if($this->language_getName($lang) === $searchParams['name']) $language = $lang;
				}
			}
		}
		return $this->isLanguage($language) ? $language : $this->getPreferedLanguage();
	}

	public function getAppTranslationsDir() {
		$Filesystem = new Filesystem();
		$resources = __DIR__."/../../../../app/Resources/";
		if(!$Filesystem->exists($resources.static::TRANSLATIONS_DIR)) {
			$Filesystem->mkdir($resources.static::TRANSLATIONS_DIR, 0755);
		}
		unset($Filesystem);
		return $resources.static::TRANSLATIONS_DIR;
	}

	public function resetCacheLanguages($domains = []) {
		$Filesystem = new Filesystem();
		if(empty($domains)) $domains = $this->container->get(serviceTwgTranslation::class)->getDomaineNames();
		foreach ($this->getLanguages(false) as $language) {
			foreach ($domains as $domain) {
				$file = $this->getAppTranslationsDir().$domain.'.'.$this->language_toString($language).'.'.serviceTranslation::BDD_FILE_EXT;
				if(!$Filesystem->exists($file)) {
					$Filesystem->dumpFile($file, '');
					$Filesystem->chmod($file, 0755);
				}
				if(!$Filesystem->exists($file)) throw new Exception("Error writing cache languages: could not create file ".json_encode($file)."!", 1);
			}
		}
		$this->container->get(serviceTranslation::class)->clearLanguageCache(null);
		return $this;
	}



	/*************************************************************************************/
	/*** TESTS ON OBJECT OR ARRAY ANYWAY
	/*************************************************************************************/

	public static function isLanguage($language) {
		return is_array($language) || $language instanceOf Language;
	}

	public static function language_isActive($language) {
		if($language instanceOf Language && $language->isActive()) return true;
		if(is_array($language) && $language['active']) return true;
		return false;
	}

	public static function language_isPrefered($language) {
		if($language instanceOf Language && $language->isPrefered()) return true;
		if(is_array($language) && $language['prefered']) return true;
		return false;
	}

	public static function language_isActiveAndPrefered($language) {
		if($language instanceOf Language && $language->isPrefered() && $language->isActive()) return true;
		if(is_array($language) && $language['prefered'] && $language['active']) return true;
		return false;
	}

	public static function language_getName($language) {
		return is_array($language) ? $language['name'] : $language->getName();
	}

	public static function language_getCountry($language) {
		return is_array($language) ? $language['country'] : $language->getCountry();
	}

	public static function language_toString($language) {
		if(is_string($language) || empty($language)) return $language;
		return is_array($language) ? $language['fullname'] : $language->__toString();
	}

	public static function language_fullname($language) {
		if(is_string($language) || empty($language)) return $language;
		return is_array($language) ? $language['fullname'] : $language->getFullname();
	}


}