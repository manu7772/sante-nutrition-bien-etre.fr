<?php
namespace ModelApi\InternationalBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceModules;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\TwgTranslation;
use ModelApi\InternationalBundle\Entity\managedEntity;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// UserBundle
use ModelApi\UserBundle\Entity\Entreprise;
// AnnotBundle
use ModelApi\AnnotBundle\Annotation\Translatable;
use ModelApi\AnnotBundle\Annotation\HasTranslatable;

use Symfony\Component\Finder\Finder;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

// use \DateTime;
use \ReflectionClass;
use \Exception;

class serviceTranslation implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	// const ENTITY_CLASS = TwgTranslation::class;
	// TRANSLATOR
	const TRANSLATOR_MODE_NAME = 'translator_mode';
	const TRANSLATOR_MODES = [false, true];
	const BDD_FILE_EXT = 'bdd';
	const CODE_PREFIX = '@trans_';
	const CACHE_NAME_TRANSLATABLE = 'trans_annots_translatable';
	const CACHE_NAME_HASTRANSLATABLE = 'trans_annots_hastranslatable';

	protected $container;
	protected $enabled;
	protected $serviceKernel;
	protected $translator;
	protected $translatator;
	protected $managedEntities;


	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceKernel = $this->container->get(serviceKernel::class);
		$this->translator = $this->container->get('translator');
		$this->translatator = true;
		$this->managedEntities = new ArrayCollection();
		if($this->container->get(serviceModules::class)->isModuleEnabled('translations')) $this->enableTranslatator(); else $this->disableTranslatator();
		$this->controleTranslatationsIntegrity();
		return $this;
	}


	public function getTranslationDomain($refresh = false) {
		return $this->serviceKernel->getTranslationDomainByBundle(null, $refresh, true);
	}

	/*************************************************************************************/
	/*** TRANSLATOR MODE (on screen edit translations)
	/*************************************************************************************/

	public function isTranslatorMode() {
		if(!$this->isTranslatator()) return false;
		$mode = $this->serviceKernel->getSessionValue(static::TRANSLATOR_MODE_NAME, $this->getDefaultTranslatorMode());
		// confirm mode if does not exists
		$this->setTranslatorMode($mode);
		return $mode;
	}

	public function getDefaultTranslatorMode() {
		return static::TRANSLATOR_MODES[0];
	}

	public function setTranslatorMode($mode) {
		if(in_array($mode, static::TRANSLATOR_MODES)) $this->serviceKernel->setSessionValue(static::TRANSLATOR_MODE_NAME, $mode);
		return $this;
	}

	public function switchTranslatorMode() {
		$mode = $this->serviceKernel->getSessionValue(static::TRANSLATOR_MODE_NAME, $this->getDefaultTranslatorMode());
		$this->setTranslatorMode(!$mode);
		return $this->isTranslatorMode();
	}


	/*************************************************************************************/
	/*** TRANSLATATOR
	/*************************************************************************************/

	public static function getNewUniqueId($shortname) {
		if(is_object($shortname)) $shortname = $shortname->getShortname(true);
		return serviceTools::geUniquid(static::CODE_PREFIX.$shortname.'_');
	}

	public static function isUniqueId($value, $entity) {
		if(!is_string($value)) return false;
		$shortname = is_string($entity) ? $entity : $entity->getShortname(true);
		return preg_match('/^'.static::CODE_PREFIX.$shortname.'_/', $value) > 0;
	}

	/**
	 * Get entity's properties names with Translatable annotation
	 * @return array [property_name => Annotation]
	 */
	public function getTranslatableProperties($entity, $refresh = false) {
		$cacheId = is_object($entity) ? get_class($entity) : (class_exists((string)$entity) ? $entity : null);
		if(class_exists($cacheId)) {
			return $this->container->get(serviceCache::class)->getCacheData(
				$cacheId,
				function() use ($entity) {
					return serviceClasses::getPropertysAnnotation($entity, Translatable::class);
				},
				static::CACHE_NAME_TRANSLATABLE,
				serviceCache::NO_CONTEXT,
				null,
				serviceCache::CACHE_HIGH_LIFE,
				$refresh
			);
		} else {
			throw new Exception("Error ".__METHOD__."(): first parameter is not a class!", 1);
		}
	}

	public static function getManagedAttributes() {
		return ['getter','setter','domain','keytext','value'];
	}

	public function controleTranslatationsIntegrity() {
		if($this->isTranslatator() && $this->container->get(serviceKernel::class)->isDev()) {
			$entities = $this->container->get(serviceEntities::class)->getEntitiesNames();
			foreach ($entities as $classname => $shortname) {
				$attributes = $this->getTranslatableProperties($classname);
				// Entity has translatable fields BUT HasTranslatable class annotation
				if(count($attributes) && !$this->isTranslatable($classname)) throw new Exception("Warning Translatable: entity ".json_encode($shortname)." has Translatable fields. You must add \"HasTranslatable\" annotation for this class, please!", 1);
				if(!count($attributes) && $this->isTranslatable($classname)) throw new Exception("Warning Translatable: entity ".json_encode($shortname)." has no Translatable fields but has \"HasTranslatable\" annotation!", 1);
				if($classname === TwgTranslation::class) {
					if(count($attributes) || $this->isTranslatable($classname)) throw new Exception("Warning Translatable: entity ".json_encode($shortname)." can not be translatable!");
				}
			}
		}
		return $this;
	}

	public function isTranslatator() {
		return $this->translatator;
	}

	public function enableTranslatator() {
		$this->translatator = true;
		return $this;
	}

	public function disableTranslatator() {
		$this->translatator = false;
		return $this;
	}

	public function translateEntity($entity) {
		if(!isset($entity->_tm)) return $this->addManagedEntity($entity);
		$entity->_tm->translate();
		return true;
	}

	public function translateAllEntities() {
		foreach ($this->getManagedEntities() as $entity) $entity->translate();
		return $this;
	}

	public function encodeEntity($entity) {
		if(!isset($entity->_tm)) $this->addManagedEntity($entity);
		$entity->_tm->encode();
		return true;
	}

	public function encodeAllEntities() {
		foreach ($this->getManagedEntities() as $entity) $entity->encode();
		return $this;
	}

	public function isValidEntityForTranlatation($entity, $exceptionIfNotValid = false) {
		if(!is_object($entity)) {
			if($exceptionIfNotValid) throw new Exception("Error ".__METHOD__."(): parameter 1 must be object entity. Got ".gettype($entity)."!", 1);
			return false;
		}
		if(!$this->isTranslatable($entity)) {
			if($exceptionIfNotValid) throw new Exception("Error ".__METHOD__."(): entity ".$entity->getShortname()." can not be managed for translations.", 1);
			return false;
		}
		return $this->isTranslatator();
	}

	/**
	 * Has HasTranslatable annotation
	 * @param mixed $entity
	 * @return boolean
	 */
	public function isTranslatable($entity, $refresh = false) {
		// if(!$this->isTranslatator()) return false;
		// return serviceClasses::hasClassAnnotation($entity, HasTranslatable::class) && !($entity instanceOf TwgTranslation);
		// $RC = new ReflectionClass($entity);
		// return serviceClasses::hasClassAnnotation($entity, new HasTranslatable()) && !$RC->isSubclassOf(TwgTranslation::class);

		if($this->isTranslatator() && !($entity instanceOf TwgTranslation)) {
			$cacheId = is_object($entity) ? get_class($entity) : (class_exists((string)$entity) ? $entity : null);
			if(class_exists($cacheId)) {
				$hasTranslatable = $this->container->get(serviceCache::class)->getCacheData(
					$cacheId,
					function() use ($entity) {
						return serviceClasses::getClassAnnotation($entity, HasTranslatable::class);
					},
					static::CACHE_NAME_HASTRANSLATABLE,
					serviceCache::NO_CONTEXT,
					null,
					serviceCache::CACHE_HIGH_LIFE,
					$refresh
				);
				return !empty($hasTranslatable);
			}
		}
		return false;
	}

	public function getManagedEntities($asEntites = false) {
		if(!$this->isTranslatator()) return new ArrayCollection();
		if(!$asEntites) return $this->managedEntities;
		$entities = new ArrayCollection();
		foreach ($this->managedEntities as $managedEntity) $entities->add($managedEntity->getEntity());
		return $entities;
	}

	public function isManaged($entity) {
		if(!$this->isValidEntityForTranlatation($entity, false)) return false;
		return null !== $this->getManagedEntityByEntity($entity);
	}

	public function getManagedEntityByEntity($entity) {
		if(!$this->isValidEntityForTranlatation($entity, false)) return null;
		// if($entity instanceOf managedEntity) return $entity;
		foreach ($this->managedEntities as $managedEntity) {
			if($managedEntity->getEntity() === $entity) {
				if(!isset($entity->_tm)) throw new Exception("Error ".__METHOD__."(): entity ".$entity->__toString()." is managed, but has no managedEntity!", 1);
				return $managedEntity;
			}
		}
		return null;
	}

	public function getServiceKernel() {
		return $this->container->get(serviceKernel::class);
	}

	public function getTranslator() {
		return $this->translator;
	}

	public function getEntityManager() {
		return $this->container->get('doctrine')->getEntityManager();
	}

	public function getLocales() {
		return $this->container->get(serviceLanguage::class)->getLanguagesNames(false, false);
	}

	public function addManagedEntity($entity) {
		if($this->isValidEntityForTranlatation($entity, false)) {
			$managedEntity = $this->getManagedEntityByEntity($entity);
			if(null === $managedEntity) {
				if(isset($entity->_tm)) {
					if(!($entity->_tm instanceOf managedEntity)) throw new Exception("Error ".__METHOD__."(): entity ".$entity->__toString()." has _tm attribute, but is not a managedEntity instance!", 1);
					$managedEntity = $entity->_tm;
				} else {
					$managedEntity = new managedEntity($entity, $this);
				}
				if($managedEntity->isComplete()) $this->managedEntities->add($managedEntity);
					else if(!$managedEntity::TRY_TO_RESOLVE) throw new Exception("Error ".__METHOD__."(): entity ".$entity->__toString()." with translations made incomplete managedEntity!", 1);
					// else return false;
			}
			return $managedEntity instanceOf managedEntity && $managedEntity->isComplete();
		}
		return false;
	}

	// protected function removeManagedEntity($entity) {
	// 	$this->managedEntities = $this->managedEntities->filter(function($managedEntity) use ($entity) {
	// 		return $managedEntity->getEntity() !== $entity;
	// 	});
	// 	return $this;
	// }

	// protected function removeAllManagedEntity() {
	// 	$this->managedEntities = new ArrayCollection();
	// 	return $this;
	// }



	public function postFlushCompileAllTranslations(PostFlushEventArgs $args) {
		if(!$this->isTranslatator()) return false;
		$reflush = 0;
		foreach ($this->getManagedEntities() as $managedEntity) {
			if($managedEntity->postFlushActions($args)) $reflush++;
		}
		// YamlLog::directYamlLogfile(['Reflush entities' => ['Enabled' => $reflush > 0, 'Number' => $reflush]]);
		// $this->removeAllManagedEntity();
		return $reflush > 0;
	}


	// /**
	//  * Create TwgTranslations of each locale, by array of [locale => text]
	//  * @param array $data
	//  * @param string $keytext
	//  * @param string $domain = null
	//  * @param boolean $persist = true
	//  * @param boolean $removeEmptyItems = true
	//  * @param boolean $removeUnknownLocales = true
	//  * @return array [locale => TwgTranslation | false]
	//  */
	// public function createTranslationsByArray($data, $keytext, $domain = null, EntityManager $em = null) {
	// 	if(!$this->isTranslatator()) return [];
	// 	if(!is_string($domain)) $domain = serviceTwgTranslation::DEFAULT_DOMAIN;
	// 	if(!($em instanceOf EntityManager)) $em = $this->container->get('doctrine')->getManager();
	// 	$validator = $this->container->get('validator');
	// 	$founds = $em->getRepository(TwgTranslation::class)->findBy(['keytext' => $keytext, 'domain' => $domain]);
	// 	foreach ($data as $locale => $text) {
	// 		$data[$locale] = false;
	// 		foreach ($founds as $twgTranslation) {
	// 			if($twgTranslation->getTranslatableLocale() === $locale) $data[$locale] = $twgTranslation;
	// 		}
	// 		if(is_string(serviceTools::getTextOrNull($text))) {
	// 			if(!($data[$locale] instanceOf TwgTranslation)) {
	// 				$data[$locale] = $this->container->get(serviceTwgTranslation::class)->createNew();
	// 				$data[$locale]->setTranslatableLocale($locale);
	// 				$data[$locale]->setDomain($domain);
	// 				$data[$locale]->setKeytext($keytext);
	// 			}
	// 			$data[$locale]->setContent($text);
	// 			$data[$locale]->_to_remove = false;
	// 		} else if($data[$locale] instanceOf TwgTranslation) {
	// 			$data[$locale]->_to_remove = true;
	// 		}
	// 	}
	// 	$data = array_filter($data, function ($twgTranslation) use ($validator) { return $twgTranslation instanceOf TwgTranslation && $validator->validate($twgTranslation); });
	// 	return $data;
	// }

	public function addFlashToastr($type, $message, $role = null) {
		$this->container->get(serviceFlashbag::class)->addFlashToastr($type, $message, $role);
		return $this;
	}

	public function addFlashSweet($type, $message, $role = null) {
		$this->container->get(serviceFlashbag::class)->addFlashToastr($type, $message, $role);
		return $this;
	}




	public function clearLanguageCache($environments = 'default') {
		if(is_string($environments)) {
			if(strtolower($environments) === 'current') $environments = [$this->serviceKernel->getEnv()];
			if(strtolower($environments) === 'default') $environments = ['dev','prod','test'];
		}
		if(!is_array($environments)) $environments = ['dev','prod','test'];
		// $cacheDir = __DIR__."/../../../../var/cache/";
		$cacheDir = $this->serviceKernel->getBaseDir('var/cache/');
		$finder = new Finder();
		$dirs = array();
		foreach ($environments as $env) {
			$dir = $cacheDir.strtolower($env)."/translations";
			if(file_exists($dir) && is_dir($dir)) $dirs[] = $dir;
		}
		//TODO quick hack...
		if(count($dirs) > 0) { 
			$finder->in($dirs)->files();
			foreach($finder as $file) unlink($file->getRealpath());
			// if($this->serviceKernel->isDev()) 
				$this->container->get(serviceFlashbag::class)->addFlashToastr('warning', 'Cleared language cache.', 'ROLE_SUPER_ADMIN');
		}
		return $this;
	}


}