<?php
namespace ModelApi\InternationalBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\servicesBaseInterface;

// use \DateTime;
use \DateTimeZone;

class serviceTimezone implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	protected $defaultTimezone;


	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->defaultTimezone = $this->container->getParameterBag()->get('timeZone');
		return $this;
	}

	/*************************************************************************************/
	/*** DEFAULTS
	/*************************************************************************************/

	/**
	 * Get default timezone
	 * @return string
	 */
	public function getDefaultTimezone() {
		return $this->defaultTimezone;
	}

	/**
	 * Get timezones
	 * @return array <string>
	 */
	public static function getTimezones() {
		return DateTimeZone::listIdentifiers();
	}

	/**
	 * Get timezones choices for form
	 * @param boolean $grouped = true
	 * @return array <string>
	 */
	public static function getTimezoneChoices($grouped = true) {
		$list = [];
		foreach (static::getTimezones() as $timezone) {
			if($grouped) {
				$test = explode(DIRECTORY_SEPARATOR, $timezone);
				if(count($test) > 1) {
					$list[$test[0]] ??= [];
					$list[$test[0]][$test[1]] = $timezone;
				} else {
					$list[$timezone] = $timezone;
				}
			} else {
				$list[$timezone] = $timezone;
			}
		}
		return $list;
	}

	public static function isValidTimezone($timezone) {
		$timezones = static::getTimezones();
		return in_array($timezone, $timezones);
	}



}