<?php
namespace ModelApi\InternationalBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Loader\LoaderInterface;
use Symfony\Component\Translation\MessageCatalogue;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

// use \DateTime;
use \ReflectionClass;

class TranslationsLoader implements LoaderInterface, servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;
	// https://web.archive.org/web/20160619063201/http://blog.elendev.com:80/development/php/symfony/use-a-database-as-translation-provider-in-symfony-2/

	private $container;
	private $serviceLanguage;
	private $serviceTwgTranslation;
	private $messageCatalogues;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		// $this->serviceLanguage = $this->container->get(serviceLanguage::class);
		$this->serviceTwgTranslation = $this->container->get(serviceTwgTranslation::class);
		$this->messageCatalogues = array();
	}


	/**
	 * Loads a locale.
	 * @param mixed  $resource A resource
	 * @param string $locale   A locale
	 * @param string $domain   The domain
	 * @return MessageCatalogue A MessageCatalogue instance
	 * @throws NotFoundResourceException when the resource cannot be found
	 * @throws InvalidResourceException  when the resource cannot be loaded
	 */
	public function load($resource, $locale, $domain = null) {
		if(null == $domain) $domain = serviceTwgTranslation::DEFAULT_DOMAIN;
		if(!isset($this->messageCatalogues[$locale]) || !isset($this->messageCatalogues[$locale][$domain])) {
			$TwgTranslations = $this->serviceTwgTranslation->getTwgTranslations($domain, $locale);
			// echo('</pre>');var_dump($TwgTranslations);die('</pre>');
			$this->messageCatalogues[$locale][$domain] = new MessageCatalogue($locale);
			foreach ($TwgTranslations as $TwgTranslation) {
				// $TwgTranslation->setTranslatableLocale($locale);
				// $this->container->get(serviceEntities::class)->getEntityManager()->refresh($TwgTranslation);
				$this->messageCatalogues[$locale][$domain]->set($TwgTranslation['keytext'], $TwgTranslation['content'], $domain);
				// $this->messageCatalogues[$locale][$domain]->set($TwgTranslation->getKeytext(), $TwgTranslation->getContent(), $domain);
			}
		}
		return $this->messageCatalogues[$locale][$domain];
	}



}