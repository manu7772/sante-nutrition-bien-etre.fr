<?php
namespace ModelApi\InternationalBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Doctrine\ORM\EntityManager;
// use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// InternationalBundle
// use ModelApi\InternationalBundle\Service\serviceUtranslation;
// use ModelApi\InternationalBundle\Entity\Language;
use ModelApi\InternationalBundle\Entity\TwgTranslation;
use ModelApi\InternationalBundle\Repository\TwgTranslationRepository;
// use ModelApi\InternationalBundle\Entity\Utranslation;
// AnnotBundle
// use ModelApi\AnnotBundle\Annotation\Translatable;
// use ModelApi\AnnotBundle\Annotation\HasTranslatable;

use \Exception;
// use \DateTime;
use \ReflectionClass;

class serviceTwgTranslation implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = TwgTranslation::class;
	const DEFAULT_DOMAIN = 'messages';
	const ADMIN_DOMAIN = 'admin';

	const TRANSLATIONS_DIRECTORY = '../Resources/files/';

	protected $container;
	protected $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}




	/*************************************************************************************/
	/*** QUERYS
	/*************************************************************************************/

	public function findAllSortedByDomain($domain, $nb = 0, $asArray = false) {
		return $this->getRepository()->findAllSortedByDomain($domain, $nb, $asArray);
	}

	public function findByKeytext($keytext, $mode = null, $nb = 0, $domain = null) {
		return $this->getRepository()->findByKeytext($keytext, $mode, $nb, $domain);
	}

	public function getDomaineNames($addEntitiesEmptyDomains = true) {
		$domains = $this->getRepository()->getDomaineNames();
		if($addEntitiesEmptyDomains) {
			$shortnames = $this->serviceEntities->getEntitiesNames(true);
			foreach ($shortnames as $classname => $shortname) {
				if(!in_array($shortname, $domains)) $domains[] = $shortname;
			}
		}
		return $domains;
	}

	public function getDefaultDomaineName() {
		$names = $this->getRepository()->getDomaineNames();
		if(in_array(static::DEFAULT_DOMAIN, $names)) return static::DEFAULT_DOMAIN;
		return reset($names);
	}

	public function getDomainesChoices() {
		$domains = $this->getDomaineNames();
		$domainchoices = [];
		foreach ($domains as $domain) $domainchoices[$domain] = $domain;
		return $domainchoices;
	}

	public static function getRegexModes() {
		return TwgTranslationRepository::REGEX_MODES;
	}





	// public function findTwgTranslationAnywhere($keytext, $domain = null, $locales = [], $typeSchedules = ['database','updates','deletes']) {
	// 	if(!is_string($domain)) $domain = static::DEFAULT_DOMAIN;
	// 	$serviceKernel = $this->container->get(serviceKernel::class);
	// 	$locales = (array)$locales;
	// 	if(count($locales) <= 0) $locales = [$serviceKernel->getLocale()];
	// 	$results = [];
	// 	$scheduleds = $this->getScheduledEntities();
	// 	foreach ($typeSchedules as $loc) if(isset($scheduleds[$loc])) {
	// 		foreach ($scheduleds[$loc] as $collec) {
	// 			foreach ($collec as $twg) {
	// 				if(
	// 					$twg->getKeytext() === $keytext
	// 					&& $twg->getDomain() === $domain
	// 					&& in_array($twg->getTranslatableLocale(), $locales)
	// 				) $results[] = ['status' => $loc, 'id' => $twg->getId(), 'entity' => $twg];
	// 			}
	// 		}
	// 	}
	// 	if(in_array('database', $typeSchedules)) {
	// 		foreach ($locales as $locale) {
	// 			$bdd = $this->getRepository()->findBy(['keytext' => $keytext, 'domain' => $domain, 'translatableLocale' => $locale]);
	// 			foreach ($bdd as $twg) {
	// 				$results[] = ['status' => 'database', 'id' => $twg->getId(), 'entity' => $twg];
	// 			}
	// 		}
	// 	}
	// 	return $results;
	// }

	// protected function getScheduledEntities() {
	// 	$uow = $this->container->get('doctrine')->getManager()->getUnitOfWork();
	// 	$results['updates'] = array_filter($uow->getScheduledCollectionUpdates(), function ($entity) { return $entity instanceOf TwgTranslation; });
	// 	$results['deletes'] = array_filter($uow->getScheduledCollectionDeletions(), function ($entity) { return $entity instanceOf TwgTranslation; });
	// 	return $results;
	// }

	public function trans($keytext, $data = [], $domain = null, $locale = null) {
		if(!is_string($domain)) $domain = static::DEFAULT_DOMAIN;
		$serviceKernel = $this->container->get(serviceKernel::class);
		if(!is_string($locale)) $locale = $serviceKernel->getLocale();
		$defaultLocale = $serviceKernel->getDefaultLocale();
		$uow = $this->container->get('doctrine')->getManager()->getUnitOfWork();
		// 1 - find in updated entities
		// foreach ($uow->getScheduledEntityUpdates() as $entity) {
			# code...
		// }
		// 2 - find in insert entities
		// foreach ($uow->getScheduledEntityInsertions() as $entity) {
			# code...
		// }
		// 3 - find in BDD
		$entity = $this->getRepository()->findOneBy(['keytext' => $keytext, 'domain' => $domain, 'translatableLocale' => $locale]);
		if($entity instanceOf TwgTranslation) return $entity->getContent();
		if($locale !== $defaultLocale) {
			$entity = $this->getRepository()->findOneBy(['keytext' => $keytext, 'domain' => $domain, 'translatableLocale' => $defaultLocale]);
			if($entity instanceOf TwgTranslation) return $entity->getContent();
		}
		return $keytext;
	}

	/*************************************************************************************/
	/*** GEDMO TRANSLATIONS
	/*************************************************************************************/



	/*************************************************************************************/
	/*** TWIG TRANSLATIONS
	/*************************************************************************************/


	public function getTwgTranslations($domain, $locale) {
		// $domain = null == $domain ? static::DEFAULT_DOMAIN : $domain;
		return $this->getRepository()->findForLoader($domain, $locale);
		// return $this->getRepository()->findByDomain($domain);
	}



	/*************************************************************************************/
	/*** TWIG TRANSLATIONS FILES
	/*************************************************************************************/

	// public function getFilesFolder() {
	// 	return static::TRANSLATIONS_DIRECTORY;
	// }

	// public function getFiles() {
	// 	$folder = $this->getFilesFolder();
	// 	$files = [];

	// 	return $files;
	// }

	// public function getFileForm() {

	// }

	// public function 



}