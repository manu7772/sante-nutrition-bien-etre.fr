<?php
namespace ModelApi\InternationalBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;

// use ModelApi\BaseBundle\Repository\BasentityRepository;
use ModelApi\InternationalBundle\Entity\TwgTranslation;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;

// use Symfony\Component\HttpFoundation\Request;
// use FOS\RestBundle\Request\ParamFetcherInterface;

// use \DateTime;

/**
 * TwgTranslationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TwgTranslationRepository extends EntityRepository {

	const ELEMENT = 'twgtranslation'; // entity
	const REGEX_MODES = ['begins','ends','exact'];

	public function findAllSortedByDomain($domain, $nb = 0, $asArray = false) {
		if(!is_string($domain)) $domain = serviceTwgTranslation::DEFAULT_DOMAIN;
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$qb->where(static::ELEMENT.'.domain = :domain')
			->setParameter('domain', $domain)
			;
		$qb->orderBy(static::ELEMENT.'.created', 'DESC'); // ordre par created
		$qb->addOrderBy(static::ELEMENT.'.updated', 'DESC'); // ordre par updated
		if($nb > 0) $qb->setMaxResults($nb);
		return $asArray ? $qb->getQuery()->getArrayResult() : $qb->getQuery()->getResult();
	}

	/** 
	 * Find all TwgTranslations in other domains with same keytext or same content
	 * @param TwgTranslation $twgtranslation
	 * @return array
	 */
	public function findSimilars(TwgTranslation $twgtranslation) {
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$qb->where(static::ELEMENT.'.keytext = :keytext')
			->setParameter('keytext', $twgtranslation->getKeytext())
			->andWhere(static::ELEMENT.'.domain != :domain')
			->setParameter('domain', $twgtranslation->getDomain())
			->orWhere(static::ELEMENT.'.content = :content')
			->setParameter('content', $twgtranslation->getContent())
			;
		$results = $qb->getQuery()->getResult();
		return array_values(array_filter($results, function($item) use ($twgtranslation) { return $item !== $twgtranslation; }));
	}

	public function findByKeytext($keytext, $mode = null, $nb = 0, $domain = null) {
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$whr = 'where';
		if(is_string($keytext) && !empty($keytext)) {
			switch ($mode) {
				case 'begins': $search = '^'.$keytext; break;
				case 'ends': $search = $keytext.'$'; break;
				case 'exact': $search = '^'.$keytext.'$'; break;
				default: $search = $keytext; break;
			}
			$search = preg_replace('/\\./', '\\\\.', $search);
			$qb->where('REGEXP('.static::ELEMENT.'.keytext, :search) = true')
				->setParameter('search', $search)
			;
			$whr = 'andWhere';
		}
		if(is_string($domain)) {
			$qb->$whr(static::ELEMENT.'.domain = :domain')
				->setParameter('domain', $domain)
				;
		}
		if($nb > 0) $qb->setMaxResults($nb);
		return $qb->getQuery()->getResult();
	}

	public function findForLoader($domain, $locale) {
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$qb
			->where(static::ELEMENT.'.domain = :domain')
			->setParameter('domain', $domain)
			->andWhere(static::ELEMENT.'.translatableLocale = :locale')
			->setParameter('locale', $locale)
			->select(static::ELEMENT.'.content')
			->addSelect(static::ELEMENT.'.keytext')
			;
		return $qb->getQuery()->getScalarResult();
	}

	public function getDomaineNames() {
		$qb = $this->createQueryBuilder(static::ELEMENT)
			->select(static::ELEMENT.'.domain')->distinct()
			;
		return array_map(function($result) { return $result['domain']; }, $qb->getQuery()->getScalarResult());
	}


	/*************************************************************************/
	/** GROUPED BY KEYTEXT
	/** @see https://stackoverflow.com/questions/17642665/sql-query-with-count-and-group-by-in-symfony2-querybuilder/21832112
	/*************************************************************************/

	public function countTwgTranslationsByKDL($keytexts = null, $domains = null, $locales = null) {
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$this->selectByKDL($qb, $keytexts, $domains, $locales);
		// $qb->select('COUNT(DISTINCT '.static::ELEMENT.'.keytext)');
		// $result = $qb->getQuery()->getSingleScalarResult();
		$qb->select(static::ELEMENT.'.id, '.static::ELEMENT.'.keytext, '.static::ELEMENT.'.domain');
		$qb->groupBy(static::ELEMENT.'.domain');
		$qb->addGroupBy(static::ELEMENT.'.keytext');
		$qb->orderBy(static::ELEMENT.'.id', 'ASC');
		$result = $qb->getQuery()->getScalarResult();
		return count($result);
	}

	public function getScalarGrouppedKeytextTwgTranslationByKDL($keytexts = null, $domains = null, $locales = null) {
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$this->selectByKDL($qb, $keytexts, $domains, $locales);
		$qb->select(static::ELEMENT.'.id, '.static::ELEMENT.'.keytext, '.static::ELEMENT.'.domain, '.static::ELEMENT.'.content, '.static::ELEMENT.'.translatableLocale AS locale');
		// $qb->select(static::ELEMENT.'.keytext')->distinct();
		$qb->groupBy(static::ELEMENT.'.domain');
		$qb->addGroupBy(static::ELEMENT.'.keytext');
		$qb->orderBy(static::ELEMENT.'.id', 'ASC');
		// $qb->setFirstResult($index);
		// $qb->setMaxResults(1);
		// return $qb->getQuery()->getScalarResult();
		return $qb->getQuery()->getScalarResult();
	}

	public function getGrouppedKeytextTwgTranslationByKDL($index, $keytexts = null, $domains = null, $locales = null) {
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$this->selectByKDL($qb, $keytexts, $domains, $locales);
		$qb->select(static::ELEMENT.'.id, '.static::ELEMENT.'.keytext, '.static::ELEMENT.'.domain');
		// $qb->select(static::ELEMENT.'.keytext')->distinct();
		$qb->groupBy(static::ELEMENT.'.domain');
		$qb->addGroupBy(static::ELEMENT.'.keytext');
		$qb->orderBy(static::ELEMENT.'.id', 'ASC');
		$qb->setFirstResult($index);
		$qb->setMaxResults(1);
		// return $qb->getQuery()->getScalarResult();
		$result = $qb->getQuery()->getScalarResult();
		if(empty($result)) return [];
		// Now, find TwgTranslations
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$this->selectByKDL($qb, reset($result)['keytext'], reset($result)['domain']);
		return $qb->getQuery()->getResult();
	}

	public function selectByKDL(QueryBuilder $qb = null, $keytexts = null, $domains = null, $locales = null) {
		$qb ??= $this->createQueryBuilder(static::ELEMENT);
		$whr = 'where';
		if(!empty($keytexts)) {
			$qb->$whr($qb->expr()->in(static::ELEMENT.'.keytext', (array)$keytexts));
			$whr = 'andWhere';
		}
		if(!empty($domains)) {
			$qb->$whr($qb->expr()->in(static::ELEMENT.'.domain', (array)$domains));
			$whr = 'andWhere';
		}
		if(!empty($locales)) {
			$qb->$whr($qb->expr()->in(static::ELEMENT.'.translatableLocale', (array)$locales));
			// $whr = 'andWhere';
		}
		return $qb;
	}

}