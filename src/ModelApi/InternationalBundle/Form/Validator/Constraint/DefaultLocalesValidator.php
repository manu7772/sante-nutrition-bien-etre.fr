<?php
namespace ModelApi\InternationalBundle\Form\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ModelApi\InternationalBundle\Service\serviceLanguage;

class DefaultLocalesValidator extends ConstraintValidator {

	protected $container;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	public function validate($locale, Constraint $constraint) {
		// echo('Locale: "'.$locale.'" / ');
		// var_dump($this->container->get(serviceLanguage::class)->getLanguagesNames(false));
		if(!$this->container->get(serviceLanguage::class)->hasLanguage($locale, false)) {
			$this->context->buildViolation($constraint->message)->addViolation();
		}
		return;
	}

}
