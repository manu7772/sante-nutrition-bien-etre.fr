<?php
namespace ModelApi\InternationalBundle\Form\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DefaultLocales extends Constraint {
    public $message = 'locale.invalid';
}
