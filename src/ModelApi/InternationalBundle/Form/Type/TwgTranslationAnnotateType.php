<?php
namespace ModelApi\InternationalBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\TwgTranslation;

// use \ReflectionClass;

class TwgTranslationAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = TwgTranslation::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}