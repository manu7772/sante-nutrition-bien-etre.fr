<?php
namespace ModelApi\InternationalBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\Language;

// use \ReflectionClass;

class LanguageAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Language::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}