<?php
namespace ModelApi\InternationalBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
// Gedmo
use Gedmo\Mapping\Annotation as Gedmo;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\Language as IntLanguage;

use \ReflectionClass;
use \Exception;

trait Language {

	// /**
	//  * @var integer
	//  * @ORM\ManyToOne(targetEntity="ModelApi\InternationalBundle\Entity\Language", fetch="EAGER")
	//  * @ORM\JoinColumn(nullable=false, unique=false)
	//  * @Annot\HydrateLanguage()
	//  */
	// protected $language;

	// /**
	//  * Locale of entity / Read only
	//  * @var string
	//  */
	// protected $locale;




	// /**
	//  * Get locale of entity
	//  * @return string | null
	//  */
	// public function getLocale() {
	// 	$this->locale = (string)$this->getLanguage();
	// 	return $this->locale;
	// }

	// /**
	//  * Get language
	//  * @return IntLanguage 
	//  */
	// public function getLanguage() {
	// 	return $this->language;
	// }

	// /**
	//  * Set language
	//  * @param IntLanguage $language
	//  * @return Entity
	//  */
	// public function setLanguage(IntLanguage $language) {
	// 	$this->language = $language;
	// 	if(method_exists($this, 'setTimezone') && method_exists($this, 'getTimezone')) {
	// 		if(null === $this->getTimezone()) $this->setTimezone($this->language->getTimezone());
	// 	}
	// 	return $this;
	// }




}