<?php
namespace ModelApi\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// Gedmo
// use Gedmo\Mapping\Annotation as Gedmo;
// use Gedmo\Translatable\Translatable;
// BaseBundle
use ModelApi\BaseBundle\Entity\Adresse;
use ModelApi\BaseBundle\Entity\Telephon;
use ModelApi\BaseBundle\Entity\Reseausocial;
use ModelApi\BaseBundle\Entity\Message;
use ModelApi\BaseBundle\Entity\Annonce;
use ModelApi\BaseBundle\Entity\Blog;
use ModelApi\BaseBundle\Entity\Bloger;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use ModelApi\BaseBundle\Form\Type\ReseausocialAnnotateType;
use ModelApi\BaseBundle\Form\Type\AdresseAnnotateType;
use ModelApi\BaseBundle\Form\Type\TelephonAnnotateType;
// UserBundle
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\Partenaire;
use ModelApi\UserBundle\Entity\Certificat;
use ModelApi\UserBundle\Service\serviceTier;
use ModelApi\UserBundle\Repository\TierRepository;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Avatar;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceTimezone;

use \DateTimeZone;
use \Exception;

/**
 * Tier
 * 
 * @ORM\Entity(repositoryClass=TierRepository::class)
 * @ORM\EntityListeners({"ModelApi\UserBundle\Listener\TierListener"})
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(
 *      name="`tier`",
 *		options={"comment":"Tiers"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_tier",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_COLLAB", update="ROLE_USER", delete="ROLE_USER")
 */
abstract class Tier extends Item {

	const DEFAULT_ICON = 'fa-user-circle';
	const ENTITY_SERVICE = serviceTier::class;

	const DEFAULT_OWNER_DIRECTORY = 'system/Associates';

	use \ModelApi\BaseBundle\Traits\Preferences;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $title;

	/**
	 * Main Group
	 * @var integer
	 * @ORM\OneToOne(targetEntity="ModelApi\UserBundle\Entity\Group", inversedBy="groupowner", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true)
	 */
	protected $maingroup;

	/**
	 * Main Group
	 * @var string
	 */
	protected $maingroupName;

	/**
	 * @var string
	 * @Annot\Translatable
	 * @ORM\Column(name="functionality", type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=false}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=false}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=100)
	 */
	protected $functionality;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 * @ORM\JoinColumn(name="`owneds_directory`", nullable=true, unique=false)
	 * @Annot\JoinedOwnDirectory(getter="getOwneditem", setter="setOwneditem", altgetter="getOwneditems", altsetter="setOwneditems", path=serviceItem::OWNEDS_DIRECTORY_NAME)
	 */
	protected $owneditem;
	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1000,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Propriétaire de", "class"="ModelApi\FileBundle\Entity\Item"},
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1000,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Propriétaire de", "class"="ModelApi\FileBundle\Entity\Item"},
	 * )
	 * @CRUDS\Show(role=true, order=1000)
	 */
	protected $owneditems;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 * @ORM\JoinColumn(name="`manageds_directory`", nullable=true, unique=false)
	 * @Annot\JoinedOwnDirectory(getter="getItemformanager", setter="setItemformanager", altgetter="getItemformanagers", altsetter="setItemformanagers", path=serviceItem::MANAGERS_DIRECTORY_NAME)
	 */
	protected $itemformanager;
	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1000,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Manager de", "class"="ModelApi\FileBundle\Entity\Item"},
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1000,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Manager de", "class"="ModelApi\FileBundle\Entity\Item"},
	 * )
	 * @CRUDS\Show(role=true, order=1000)
	 */
	protected $itemformanagers;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 * @ORM\JoinColumn(name="`telephon_directory`", nullable=true, unique=false)
	 * @Annot\JoinedOwnDirectory(getter="getTelephon", setter="setTelephon", altgetter="getTelephons", altsetter="setTelephons", path="system/Telephons")
	 */
	protected $telephon;
	/**
	 * @CRUDS\Create(
	 * 		type="CollectionType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=701,
	 * 		options={
	 * 			"required"=false,
	 * 			"by_reference"=false,
	 * 			"allow_add"=true,
	 * 			"allow_delete"=true,
	 * 			"label"="Téléphones",
	 * 			"entry_type"=TelephonAnnotateType::class,
	 * 			"entry_options"={"label": true, "cruds_action"="create"},
	 * 			"attr"={"data-collection-type"=true},
	 * 		},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Update(
	 * 		type="CollectionType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=701,
	 * 		options={
	 * 			"required"=false,
	 * 			"by_reference"=false,
	 * 			"allow_add"=true,
	 * 			"allow_delete"=true,
	 * 			"label"="Téléphones",
	 * 			"entry_type"=TelephonAnnotateType::class,
	 * 			"entry_options"={"label": true, "cruds_action"="create"},
	 * 			"attr"={"data-collection-type"=true},
	 * 		},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Show(role=true, order=701)
	 */
	protected $telephons;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 * @ORM\JoinColumn(name="`adresse_directory`", nullable=true, unique=false)
	 * @Annot\JoinedOwnDirectory(getter="getAdresse", setter="setAdresse", altgetter="getAdresses", altsetter="setAdresses", path="system/Adresses")
	 */
	protected $adresse;
	/**
	 * @CRUDS\Create(
	 * 		type="CollectionType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=700,
	 * 		options={
	 * 			"required"=false,
	 * 			"by_reference"=false,
	 * 			"allow_add"=true,
	 * 			"allow_delete"=true,
	 * 			"label"="Adresses",
	 * 			"entry_type"=AdresseAnnotateType::class,
	 * 			"entry_options"={"label": true, "cruds_action"="create"},
	 * 			"attr"={"data-collection-type"=true},
	 * 		},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Update(
	 * 		type="CollectionType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=700,
	 * 		options={
	 * 			"required"=false,
	 * 			"by_reference"=false,
	 * 			"allow_add"=true,
	 * 			"allow_delete"=true,
	 * 			"label"="Adresses",
	 * 			"entry_type"=AdresseAnnotateType::class,
	 * 			"entry_options"={"label": true, "cruds_action"="create"},
	 * 			"attr"={"data-collection-type"=true},
	 * 		},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Show(role=true, order=700)
	 */
	protected $adresses;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 * @ORM\JoinColumn(name="`reseausocial_directory`", nullable=true, unique=false)
	 * @Annot\JoinedOwnDirectory(getter="getReseausocial", setter="setReseausocial", altgetter="getReseausocials", altsetter="setReseausocials", path="system/Socials")
	 */
	protected $reseausocial;
	/**
	 * @CRUDS\Create(
	 * 		type="CollectionType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=702,
	 * 		options={
	 * 			"required"=false,
	 * 			"by_reference"=false,
	 * 			"allow_add"=true,
	 * 			"allow_delete"=true,
	 * 			"label"="Réseaux sociaux",
	 * 			"entry_type"=ReseausocialAnnotateType::class,
	 * 			"entry_options"={"label": true, "cruds_action"="create"},
	 * 			"attr"={"data-collection-type"=true},
	 * 		},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Update(
	 * 		type="CollectionType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=702,
	 * 		options={
	 * 			"required"=false,
	 * 			"by_reference"=false,
	 * 			"allow_add"=true,
	 * 			"allow_delete"=true,
	 * 			"label"="Réseaux sociaux",
	 * 			"entry_type"=ReseausocialAnnotateType::class,
	 * 			"entry_options"={"label": true, "cruds_action"="create"},
	 * 			"attr"={"data-collection-type"=true},
	 * 		},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Show(role=true, order=702)
	 */
	protected $reseausocials;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 * @ORM\JoinColumn(name="`associate_directory`", nullable=true, unique=false)
	 * @Annot\JoinedOwnDirectory(getter="getAssociate", setter="setAssociate", altgetter="getAssociates", altsetter="setAssociates", path="system/Associates")
	 */
	protected $associate;
	protected $associates;
	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=705,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Associés User", "class"="ModelApi\UserBundle\Entity\User"},
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=705,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Associés User", "class"="ModelApi\UserBundle\Entity\User"},
	 * )
	 * @CRUDS\Show(role=true, order=705)
	 */
	protected $associateUsers;
	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=706,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Associés Entreprises", "class"="ModelApi\UserBundle\Entity\Entreprise"},
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=706,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Associés Entreprises", "class"="ModelApi\UserBundle\Entity\Entreprise"},
	 * )
	 * @CRUDS\Show(role=true, order=706)
	 */
	protected $associateEntreprises;
	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=707,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Associés Partenaires", "class"="ModelApi\UserBundle\Entity\Partenaire"},
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=707,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Associés Partenaires", "class"="ModelApi\UserBundle\Entity\Partenaire"},
	 * )
	 * @CRUDS\Show(role=true, order=707)
	 */
	protected $associatePartenaires;
	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=708,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Certificats", "class"="ModelApi\UserBundle\Entity\Certificat"},
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=708,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Certificats", "class"="ModelApi\UserBundle\Entity\Certificat"},
	 * )
	 * @CRUDS\Show(role=true, order=708)
	 */
	protected $associateCertificats;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Avatar", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsAvatar"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsAvatar"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $avatar;

	/**
	 * Email
	 * @var string
	 * @ORM\Column(name="email", type="string", length=180)
	 * @CRUDS\Create(
	 * 		type="emailType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="emailType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=100)
	 */
	protected $email;

	/**
	 * Time zone
	 * @var string
	 * @ORM\Column(name="timezone", type="string", nullable=true)
	 * @Annot\Timezone()
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=600,
	 * 		options={"multiple"=false, "required"=true, "by_reference"=false, "choices"="auto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=600,
	 * 		options={"multiple"=false, "required"=true, "by_reference"=false, "choices"="auto"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_COLLABORATOR", order=600)
	 */
	protected $timezone;

	/**
	 * @var array
	 * @ORM\ManyToMany(targetEntity="ModelApi\BaseBundle\Entity\Bloger", mappedBy="recipients")
	 * @ORM\JoinTable(
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 */
	protected $blogers;

	/**
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 	)
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $visibleEvenIfInactive;

	protected $cookies;
	protected $newsletter;
	protected $forms_contexts;

	// protected $membres;

	public function __construct() {
		parent::__construct();
		$this->email = null;
		$this->maingroup = null;
		$this->maingroupName = null;
		$this->functionality = null;
		$this->adresse = null;
		$this->adresses = new ArrayCollection();
		$this->owneditem = null;
		$this->owneditems = new ArrayCollection();
		$this->itemformanager = null;
		$this->itemformanagers = new ArrayCollection();
		$this->telephon = null;
		$this->telephons = new ArrayCollection();
		$this->reseausocial = null;
		$this->reseausocials = new ArrayCollection();
		$this->associate = null;
		$this->associates = new ArrayCollection();
		$this->associateUsers = new ArrayCollection();
		$this->associateEntreprises = new ArrayCollection();
		$this->associatePartenaires = new ArrayCollection();
		$this->associateCertificats = new ArrayCollection();
		// $this->membres = new ArrayCollection();
		$this->avatar = null;
		$this->timezone = null; // DateTimeZone::UTC; // -->https://www.php.net/manual/fr/class.datetimezone.php
		$this->mainblogers = new ArrayCollection();
		$this->blogers = new ArrayCollection();
		// preferences
		$this->setNewsletter(false);
		$this->setFormsContexts([BaseAnnotateType::getDefaultUserContext()]);
		$this->initTier();
		$this->setNormalizeNameMethod(); // No normalization
		return $this;
	}

	/**
	 * Set timezone
	 * @param string $timezone = null
	 * @return User
	 */
	public function setTimezone($timezone) {
		$this->timezone = is_array($timezone) ? reset($timezone) : $timezone;
		return $this;
	}

	/**
	 * Get timezone
	 * @return string | null
	 */
	public function getTimezone($asObject = false) {
		if(!serviceTimezone::isValidTimezone($this->timezone)) return null;
		return $asObject ? new DateTimeZone($this->timezone) : $this->timezone;
	}

	/**
	 * Get Country code
	 * @return string | null
	 */
	public function getCountryCode() {
		$timezone = $this->getTimezone(true);
		if(!empty($timezone)) {
			$location = $timezone->getLocation();
			return $location['country_code'];
		}
		return null;
	}

	/**
	 * Get timezones
	 * @param boolean $grouped = true
	 * @return array
	 */
	public function getTimezoneChoices($grouped = true) {
		return serviceTimezone::getTimezoneChoices();
	}


	/**
	 * @ORM\PostLoad()
	 * @return Tier
	 */
	public function initTier() {
		// $this->membres = new ArrayCollection();
		$this->getNewsletter();
		$this->getCookies();
		$this->getFormsContexts();
		return $this;
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getSlug();
	}

	// public function getMembres() {
	// 	$this->membres = new ArrayCollection();
	// 	foreach ($this->getGroups() as $group) {
	// 		foreach ($group->getMembres() as $membre) {
	// 			if(!$this->membres->contains($membre)) $this->membres->add($membre);
	// 		}
	// 	}
	// 	return $this->membres;
	// }


	/*************************************************************************************/
	/*** OWNER
	/*************************************************************************************/

	// /**
	//  * Set owner
	//  * @param Item $item = null
	//  * @return Item
	//  */
	// public function setOwner(Item $item = null, $refresh = false) {
	// 	parent::setOwner($tier, $refresh);
	// 	if(!($this->owner instanceOf User)) $this->owner->addAssociate($this);
	// 	return $this;
	// }

	/**
	 * {@inheritdoc}
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Get name for choice list
	 */
	public function getListingname() {
		return $this->getName().' > '.$this->email;
		// return $this->getIconHtml().' '.$this->email.' > '.$this->getName();
	}

	/**
	 * {@inheritdoc}
	 */
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	/**
	 * Set name
	 * @param string $name
	 * @param boolean $updateNested = true
	 * @return self
	 */
	public function setName($name, $updateNested = true) {
		parent::setName($name, $updateNested);
		if(empty(serviceTools::computeTextOrNull($this->maingroupName))) $this->setMaingroupName($this->getName());
		return $this;
	}

	public function setMaingroupName($maingroupName = null) {
		if(serviceTools::computeTextOrNull($maingroupName)) $this->maingroupName = $maingroupName;
		if(!empty($this->maingroupName) && $this->maingroup instanceOf Group) $this->maingroup->setName($this->getMaingroupName());
		return $this;
	}

	public function getMaingroupName() {
		return strtoupper(null !== $this->maingroupName ? $this->maingroupName : $this->getName());
	}



	/*************************************************************************************/
	/*** MAIN GROUP
	/*************************************************************************************/

	/**
	 * Get main Group
	 * @return Group | null
	 */
	public function getMaingroup() {
		return $this->maingroup;
	}

	/**
	 * Set main Group
	 * --> BEWARE!!! Tier entity can not have no main Group!!!
	 * @param Group $maingroup
	 * @param boolean $inverse = true
	 * @return Tier
	 */
	public function setMaingroup(Group $maingroup = null, $inverse = true) {
		if($maingroup === $this->maingroup) return $this;
		if($this->maingroup instanceOf Group) $this->maingroup->setGroupowner(null, false);
		if($maingroup instanceOf Group && $inverse) $maingroup->setGroupowner($this, false);
		$this->maingroup = $maingroup;
		return $this;
	}


	/*************************************************************************************/
	/*** GROUPS
	/*************************************************************************************/

	/**
	 * Gets the groups granted to the Tier --> add main group
	 * @return \Traversable
	 */
	public function getGroups() {
		$groups = parent::getGroups();
		if($this->maingroup instanceOf Group && !$groups->contains($this->maingroup)) $groups->add($this->maingroup);
		return $groups;
	}

	/**
	 * Remove a group from the user groups.
	 * @param Group $group
	 * @return self
	 */
	public function removeGroup(Group $group) {
		if($this->maingroup instanceOf Group && $this->maingroup === $group) return $this;
		$this->getGroups()->removeElement($group);
		// if($inverse) $group->removeItem($this, false);
		return $this;
	}

	public function setGroups($groups) {
		foreach ($this->groups as $group) {
			if(!$groups->contains($group) && !($this->maingroup instanceOf Group && $this->maingroup === $group)) $this->removeGroup($group);
		}
		foreach ($groups as $group) $this->addGroup($group);
		return $this;
	}



	/*************************************************************************************/
	/*** ADRESSES
	/*************************************************************************************/

	/**
	 * Get Basedirectory of Adresses
	 * @return Basedirectory
	 */
	public function getAdresse() {
		if(empty($this->adresse)) $this->adresse = $this->getOwnerDirectory('system/Adresses');
		return $this->adresse;
	}

	public function setAdresse() {
		$this->adresse = null;
		$this->getAdresse();
		return $this;
	}

	public function getAdresses() {
		if($this->getAdresse() instanceOf Basedirectory) {
			$this->adresses = $this->adresse->getChilds()->filter(function($item) { return $item instanceOf Adresse; });
			return $this->adresses;
		}
		if(!($this->adresses instanceOf ArrayCollection)) $this->adresses = new ArrayCollection();
		return $this->adresses;
	}

	public function setAdresses($adresses) {
		foreach ($this->getAdresses() as $adresse) {
			if(!$adresses->contains($adresse)) $this->removeAdresse($adresse);
		}
		foreach ($adresses as $adresse) $this->addAdresse($adresse);
		return $this;
	}

	public function addAdresse(Adresse $adresse) {
		if($this->getAdresse() instanceOf Basedirectory) {
			$this->adresse->addChild($adresse);
			return $this->getAdresses()->contains($adresse);
		}
		if(!$this->getAdresses()->contains($adresse)) $this->adresses->add($adresse);
		return true;
	}

	public function removeAdresse(Adresse $adresse) {
		if($this->getAdresse() instanceOf Basedirectory) {
			$this->adresse->removeChild($adresse);
			return !$this->getAdresses()->contains($adresse);
		}
		$this->getAdresses()->removeElement($adresse);
		return true;
	}

	public function hasAdresse(Adresse $adresse) {
		return $this->getAdresses()->contains($adresse);
	}

	public function getMainAdresse() {
		$mainAdresse = $this->getAdresses()->first();
		return $mainAdresse instanceOf Adresse ? $mainAdresse : null;
	}


	/*************************************************************************************/
	/*** OWNEDITEMS
	/*************************************************************************************/

	/**
	 * Get Basedirectory of Owneditems
	 * @return Basedirectory
	 */
	public function getOwneditem() {
		if(empty($this->owneditem)) $this->owneditem = $this->getOwnerDirectory(serviceItem::OWNEDS_DIRECTORY_NAME);
		return $this->owneditem;
	}

	public function setOwneditem() {
		$this->owneditem = null;
		$this->getOwneditem();
		return $this;
	}


	public function getOwneditems() {
		if($this->getOwneditem() instanceOf Basedirectory) {
			$this->owneditems = $this->owneditem->getChilds();
			return $this->owneditems;
		}
		if(!($this->owneditems instanceOf ArrayCollection)) $this->owneditems = new ArrayCollection();
		return $this->owneditems;
	}

	public function setOwneditems($owneditems) {
		foreach ($this->getOwneditems() as $owneditem) {
			if(!$owneditems->contains($owneditem)) $this->removeOwneditem($owneditem);
		}
		foreach ($owneditems as $owneditem) $this->addOwneditem($owneditem);
		return $this;
	}

	public function addOwneditem(Item $owneditem) {
		if(!($owneditem instanceOf Basedirectory)) {
			if($this->getOwneditem() instanceOf Basedirectory) {
				// $this->owneditem->addChild($owneditem);
				$owneditem->setParent($this->owneditem); // MUST BE HARDLINK PARENT!!!
				if($owneditem->getParent() !== $this->owneditem) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." could not set as parent!", 1);
				return $this->getOwneditems()->contains($owneditem);
			}
			if(!$this->getOwneditems()->contains($owneditem)) $this->owneditems->add($owneditem);
			return true;
		}
		return false;
	}

	public function removeOwneditem(Item $owneditem) {
		if($this->getOwneditem() instanceOf Basedirectory) {
			if($owneditem->getDirparents()->count() > 1) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." can not delete parent until still has dirparents!", 1);
			$this->owneditem->removeChild($owneditem);
			return !$this->getOwneditems()->contains($owneditem);
		}
		$this->getOwneditems()->removeElement($owneditem);
		return true;
	}

	public function hasOwneditem(Item $owneditem) {
		return $this->getOwneditems()->contains($owneditem);
	}

	/*************************************************************************************/
	/*** ITEMFORMANAGERS
	/*************************************************************************************/

	/**
	 * Get Basedirectory of Itemformanagers
	 * @return Basedirectory
	 */
	public function getItemformanager() {
		if(empty($this->itemformanager)) $this->itemformanager = $this->getOwnerDirectory(serviceItem::MANAGERS_DIRECTORY_NAME);
		return $this->itemformanager;
	}

	public function setItemformanager() {
		$this->itemformanager = null;
		$this->getItemformanager();
		return $this;
	}


	public function getItemformanagers() {
		if($this->getItemformanager() instanceOf Basedirectory) {
			$this->itemformanagers = $this->itemformanager->getChilds();
			return $this->itemformanagers;
		}
		if(!($this->itemformanagers instanceOf ArrayCollection)) $this->itemformanagers = new ArrayCollection();
		return $this->itemformanagers;
	}

	public function setItemformanagers($itemformanagers) {
		foreach ($this->getItemformanagers() as $itemformanager) {
			if(!$itemformanagers->contains($itemformanager)) $this->removeItemformanager($itemformanager);
		}
		foreach ($itemformanagers as $itemformanager) $this->addItemformanager($itemformanager);
		return $this;
	}

	public function addItemformanager(Item $itemformanager) {
		if(!($itemformanager instanceOf Basedirectory)) {
			if($this->getItemformanager() instanceOf Basedirectory) {
				if(!$itemformanager->hasParent() && $this->isDeepControl()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." has no parent, please set parent before adding in managed items!", 1);
				$this->itemformanager->addChild($itemformanager);
				return $this->getItemformanagers()->contains($itemformanager);
			}
			if(!$this->getItemformanagers()->contains($itemformanager)) $this->itemformanagers->add($itemformanager);
			return true;
		}
		return false;
	}

	public function removeItemformanager(Item $itemformanager) {
		if($this->getItemformanager() instanceOf Basedirectory) {
			$this->itemformanager->removeChild($itemformanager);
			return !$this->getItemformanagers()->contains($itemformanager);
		}
		$this->getItemformanagers()->removeElement($itemformanager);
		return true;
	}

	public function hasItemformanager(Item $itemformanager) {
		return $this->getItemformanagers()->contains($itemformanager);
	}

	/*************************************************************************************/
	/*** TELEPHONS
	/*************************************************************************************/

	/**
	 * Get Basedirectory of Telephons
	 * @return Basedirectory
	 */
	public function getTelephon() {
		if(empty($this->telephon)) $this->telephon = $this->getOwnerDirectory('system/Telephons');
		return $this->telephon;
	}

	public function setTelephon() {
		$this->telephon = null;
		$this->getTelephon();
		return $this;
	}

	public function getTelephons() {
		if($this->getTelephon() instanceOf Basedirectory) {
			$this->telephons = $this->telephon->getChilds()->filter(function($item) { return $item instanceOf Telephon; });
			return $this->telephons;
		}
		if(!($this->telephons instanceOf ArrayCollection)) $this->telephons = new ArrayCollection();
		return $this->telephons;
	}

	public function setTelephons($telephons) {
		foreach ($this->getTelephons() as $telephon) {
			if(!$telephons->contains($telephon)) $this->removeTelephon($telephon);
		}
		foreach ($telephons as $telephon) $this->addTelephon($telephon);
		return $this;
	}

	public function addTelephon(Telephon $telephon) {
		if($this->getTelephon() instanceOf Basedirectory) {
			$this->telephon->addChild($telephon);
			return $this->getTelephons()->contains($telephon);
		}
		if(!$this->getTelephons()->contains($telephon)) $this->telephons->add($telephon);
		return true;
	}

	public function removeTelephon(Telephon $telephon) {
		if($this->getTelephon() instanceOf Basedirectory) {
			$this->telephon->removeChild($telephon);
			return !$this->getTelephons()->contains($telephon);
		}
		$this->getTelephons()->removeElement($telephon);
		return true;
	}

	public function hasTelephon(Telephon $telephon) {
		return $this->getTelephons()->contains($telephon);
	}

	public function getMainTelephon() {
		$mainTelephon = $this->getTelephons()->first();
		return $mainTelephon instanceOf Telephon ? $mainTelephon : null;
	}


	/*************************************************************************************/
	/*** RESEAUSOCIALS
	/*************************************************************************************/

	/**
	 * Get Basedirectory of Reseausocials
	 * @return Basedirectory
	 */
	public function getReseausocial() {
		if(empty($this->reseausocial)) $this->reseausocial = $this->getOwnerDirectory('system/Socials');
		return $this->reseausocial;
	}

	public function setReseausocial() {
		$this->reseausocial = null;
		$this->getReseausocial();
		return $this;
	}

	public function getReseausocials() {
		if($this->getReseausocial() instanceOf Basedirectory) {
			$this->reseausocials = $this->reseausocial->getChilds()->filter(function($item) { return $item instanceOf Reseausocial; });
			return $this->reseausocials;
		}
		if(!($this->reseausocials instanceOf ArrayCollection)) $this->reseausocials = new ArrayCollection();
		return $this->reseausocials;
	}

	public function setReseausocials($reseausocials) {
		foreach ($this->getReseausocials() as $reseausocial) {
			if(!$reseausocials->contains($reseausocial)) $this->removeReseausocial($reseausocial);
		}
		foreach ($reseausocials as $reseausocial) $this->addReseausocial($reseausocial);
		return $this;
	}

	public function addReseausocial(Reseausocial $reseausocial) {
		if($this->getReseausocial() instanceOf Basedirectory) {
			$this->reseausocial->addChild($reseausocial);
			return $this->getReseausocials()->contains($reseausocial);
		}
		if(!$this->getReseausocials()->contains($reseausocial)) $this->reseausocials->add($reseausocial);
		return true;
	}

	public function removeReseausocial(Reseausocial $reseausocial) {
		if($this->getReseausocial() instanceOf Basedirectory) {
			$this->reseausocial->removeChild($reseausocial);
			return !$this->getReseausocials()->contains($reseausocial);
		}
		$this->getReseausocials()->removeElement($reseausocial);
		return true;
	}

	public function hasReseausocial(Reseausocial $reseausocial) {
		return $this->getReseausocials()->contains($reseausocial);
	}


	/*************************************************************************************/
	/*** ASSOCIATES
	/*************************************************************************************/

	/**
	 * Get Basedirectory of Associates
	 * @return Basedirectory
	 */
	public function getAssociate() {
		if(empty($this->associate)) $this->associate = $this->getOwnerDirectory('system/Associates');
		return $this->associate;
	}

	public function setAssociate() {
		$this->associate = null;
		$this->getAssociate();
		return $this;
	}



	public function getAssociates() {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associates = $this->associate->getChilds()->filter(function($item) { return $item instanceOf Tier; });
			return $this->associates;
		}
		if(!($this->associates instanceOf ArrayCollection)) $this->associates = new ArrayCollection();
		return $this->associates;
	}

	public function setAssociates($associates) {
		foreach ($this->getAssociates() as $associate) {
			if(!$associates->contains($associate)) $this->removeAssociate($associate);
		}
		foreach ($associates as $associate) $this->addAssociate($associate);
		return $this;
	}

	public function addAssociate(Tier $associate) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->addChild($associate);
			return $this->getAssociates()->contains($associate);
		}
		if(!$this->getAssociates()->contains($associate)) $this->associates->add($associate);
		return true;
	}

	public function removeAssociate(Tier $associate) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->removeChild($associate);
			return !$this->getAssociates()->contains($associate);
		}
		$this->getAssociates()->removeElement($associate);
		return true;
	}

	public function hasAssociate(Tier $associate) {
		return $this->getAssociates()->contains($associate);
	}


	/*** ASSOCIATES USER */

	public function getAssociateUsers() {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associateUsers = $this->associate->getChilds()->filter(function($item) { return $item instanceOf User; });
			return $this->associateUsers;
		}
		if(!($this->associateUsers instanceOf ArrayCollection)) $this->associateUsers = new ArrayCollection();
		return $this->associateUsers;
	}
	public function getManagers() {
		return $this->getAssociateUsers();
	}

	public function setAssociateUsers($associateUsers) {
		foreach ($this->getAssociateUsers() as $associateUser) {
			if(!$associateUsers->contains($associateUser)) $this->removeAssociateUser($associateUser);
		}
		foreach ($associateUsers as $associateUser) $this->addAssociateUser($associateUser);
		return $this;
	}

	public function addAssociateUser(User $associateUser) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->addChild($associateUser);
			if(!$associateUser->hasAssociateEntreprise($this)) $associateUser->addAssociateEntreprise($this);
			return $this->getAssociateUsers()->contains($associateUser);
		}
		if(!$this->getAssociateUsers()->contains($associateUser)) $this->associateUsers->add($associateUser);
		return true;
	}

	public function removeAssociateUser(User $associateUser) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->removeChild($associateUser);
			if($associateUser->hasAssociateEntreprise($this)) $associateUser->removeAssociateEntreprise($this);
			return !$this->getAssociateUsers()->contains($associateUser);
		}
		$this->getAssociateUsers()->removeElement($associateUser);
		return true;
	}

	public function hasAssociateUser(User $associateUser) {
		return $this->getAssociateUsers()->contains($associateUser);
	}

	/*** ASSOCIATES ENTREPRISE */

	public function getAssociateEntreprises() {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associateEntreprises = $this->associate->getChilds()->filter(function($item) { return $item instanceOf Entreprise; });
			return $this->associateEntreprises;
		}
		if(!($this->associateEntreprises instanceOf ArrayCollection)) $this->associateEntreprises = new ArrayCollection();
		return $this->associateEntreprises;
	}

	public function setAssociateEntreprises($associateEntreprises) {
		foreach ($this->getAssociateEntreprises() as $associateEntreprise) {
			if(!$associateEntreprises->contains($associateEntreprise)) $this->removeAssociateEntreprise($associateEntreprise);
		}
		foreach ($associateEntreprises as $associateEntreprise) $this->addAssociateEntreprise($associateEntreprise);
		return $this;
	}

	public function addAssociateEntreprise(Entreprise $associateEntreprise) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->addChild($associateEntreprise);
			if(!$associateEntreprise->hasAssociateUser($this)) $associateEntreprise->addAssociateUser($this);
			return $this->getAssociateEntreprises()->contains($associateEntreprise);
		}
		if(!$this->getAssociateEntreprises()->contains($associateEntreprise)) $this->associateEntreprises->add($associateEntreprise);
		return true;
	}

	public function removeAssociateEntreprise(Entreprise $associateEntreprise) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->removeChild($associateEntreprise);
			if($associateEntreprise->hasAssociateUser($this)) $associateEntreprise->removeAssociateUser($this);
			return !$this->getAssociateEntreprises()->contains($associateEntreprise);
		}
		$this->getAssociateEntreprises()->removeElement($associateEntreprise);
		return true;
	}

	public function hasAssociateEntreprise(Entreprise $associateEntreprise) {
		return $this->getAssociateEntreprises()->contains($associateEntreprise);
	}

	/*** ASSOCIATES PARTENAIRE */

	public function getAssociatePartenaires() {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associatePartenaires = $this->associate->getChilds()->filter(function($item) { return $item instanceOf Partenaire; });
			return $this->associatePartenaires;
		}
		if(!($this->associatePartenaires instanceOf ArrayCollection)) $this->associatePartenaires = new ArrayCollection();
		return $this->associatePartenaires;
	}

	public function setAssociatePartenaires($associatePartenaires) {
		foreach ($this->getAssociatePartenaires() as $associatePartenaire) {
			if(!$associatePartenaires->contains($associatePartenaire)) $this->removeAssociatePartenaire($associatePartenaire);
		}
		foreach ($associatePartenaires as $associatePartenaire) $this->addAssociatePartenaire($associatePartenaire);
		return $this;
	}

	public function addAssociatePartenaire(Partenaire $associatePartenaire) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->addChild($associatePartenaire);
			return $this->getAssociatePartenaires()->contains($associatePartenaire);
		}
		if(!$this->getAssociatePartenaires()->contains($associatePartenaire)) $this->associatePartenaires->add($associatePartenaire);
		return true;
	}

	public function removeAssociatePartenaire(Partenaire $associatePartenaire) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->removeChild($associatePartenaire);
			return !$this->getAssociatePartenaires()->contains($associatePartenaire);
		}
		$this->getAssociatePartenaires()->removeElement($associatePartenaire);
		return true;
	}

	public function hasAssociatePartenaire(Partenaire $associatePartenaire) {
		return $this->getAssociatePartenaires()->contains($associatePartenaire);
	}

	/*** ASSOCIATES CERTIFICAT */

	public function getAssociateCertificats() {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associateCertificats = $this->associate->getChilds()->filter(function($item) { return $item instanceOf Certificat; });
			return $this->associateCertificats;
		}
		if(!($this->associateCertificats instanceOf ArrayCollection)) $this->associateCertificats = new ArrayCollection();
		return $this->associateCertificats;
	}

	public function setAssociateCertificats($associateCertificats) {
		foreach ($this->getAssociateCertificats() as $associateCertificat) {
			if(!$associateCertificats->contains($associateCertificat)) $this->removeAssociateCertificat($associateCertificat);
		}
		foreach ($associateCertificats as $associateCertificat) $this->addAssociateCertificat($associateCertificat);
		return $this;
	}

	public function addAssociateCertificat(Certificat $associateCertificat) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->addChild($associateCertificat);
			return $this->getAssociateCertificats()->contains($associateCertificat);
		}
		if(!$this->getAssociateCertificats()->contains($associateCertificat)) $this->associateCertificats->add($associateCertificat);
		return true;
	}

	public function removeAssociateCertificat(Certificat $associateCertificat) {
		if($this->getAssociate() instanceOf Basedirectory) {
			$this->associate->removeChild($associateCertificat);
			return !$this->getAssociateCertificats()->contains($associateCertificat);
		}
		$this->getAssociateCertificats()->removeElement($associateCertificat);
		return true;
	}

	public function hasAssociateCertificat(Certificat $associateCertificat) {
		return $this->getAssociateCertificats()->contains($associateCertificat);
	}



	/*************************************************************************************/
	/*** FUNCTIONALITY
	/*************************************************************************************/

	/**
	 * Get functionality
	 * @return string
	 */
	public function getFunctionality() {
		return $this->functionality;
	}

	/**
	 * Set functionality
	 * @param string $functionality
	 * @return Tier
	 */
	public function setFunctionality($functionality) {
		$this->functionality = $functionality;
		return $this;
	}


	/*************************************************************************************/
	/*** AVATAR
	/*************************************************************************************/

	/**
	 * Set URL as avatar
	 * @param string $url
	 * @return Tier
	 */
	public function setUrlAsAvatar($url) {
		$avatar = new Avatar();
		$avatar->setUploadFile($url);
		$this->setAvatar($avatar);
	}

	/**
	 * Set resource as avatar
	 * @param string $resource
	 * @return Tier
	 */
	public function setResourceAsAvatar(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$avatar = new Avatar();
			$avatar->setUploadFile($resource);
			$this->setAvatar($avatar);
		}
		return $this;
	}

	public function getResourceAsAvatar() {
		return null;
	}

	/**
	 * Set avatar
	 * @param Avatar $avatar = null
	 * @return Tier
	 */
	public function setAvatar(Avatar $avatar = null) {
		$this->avatar = $avatar;
		return $this;
	}

	/**
	 * Get avatar
	 * @return Avatar | null
	 */
	public function getAvatar() {
		return $this->avatar;
	}







	/*************************************************************************************/
	/*** COOKIES
	/*************************************************************************************/

	public function isCookies() {
		return $this->getCookies();
	}

	public function getCookies() {
		$this->cookies = $this->getPreference('cookies', false, false, true);
		return $this->cookies;
	}

	public function setCookies($cookies) {
		$this->cookies = (boolean)$cookies;
		$this->setPreference('cookies', $this->cookies);
		return $this;
	}

	/*************************************************************************************/
	/*** NEWSLETTER
	/*************************************************************************************/

	public function isNewsletter() {
		return $this->getNewsletter() && is_string($this->getEmail()) && $this->isActive();
	}

	public function getNewsletter() {
		$this->newsletter = $this->getPreference('newsletter', false, false, true);
		return $this->newsletter;
	}

	public function setNewsletter($newsletter) {
		$this->newsletter = (boolean)$newsletter;
		$this->setPreference('newsletter', $this->newsletter);
		return $this;
	}

	/*************************************************************************************/
	/*** FORM_CONTEXT
	/*************************************************************************************/

	public function getFormsContextsChoices() {
		$contexts = BaseAnnotateType::getChoicesUserContexts();
		$list = [];
		foreach ($contexts as $context) {
			$list[$context] = $context;
		}
		// echo('</pre>');var_dump($list);die('</pre>');
		return $list;
	}

	public function getFormsContexts() {
		$this->forms_contexts = $this->getPreference('forms/contexts', 'simple', false, true);
		if(!is_array($this->forms_contexts) || !count($this->forms_contexts)) $this->forms_contexts = ['simple'];
		return $this->forms_contexts;
	}

	public function setFormsContexts($forms_contexts = ['simple']) {
		$this->setPreference('forms/contexts', (array)$forms_contexts);
		$this->getFormsContexts();
		return $this;
	}







	/**
	 * Set enabled
	 * @param boolean $enabled = true
	 * @return self
	 */
	public function setEnabled($enabled = true) {
		$this->enabled = $enabled;
		if($this->isDisabled()) $this->setNewsletter(false);
		return $this;
	}

	/**
	 * Set disabled
	 * @return self
	 */
	public function setDisabled() {
		$this->enabled = false;
		$this->setNewsletter(false);
		return $this;
	}



	/*************************************************************************************/
	/*** MAIN BLOGERS
	/*************************************************************************************/

	public function getMainblogers() {
		return $this->blogers->filter(function($bloger) {
			return $bloger->getMainrecipient() === $this;
		});
	}

	public function getMainmessages() {
		return $this->getMainblogers()->filter(function($bloger) { return $bloger instanceOf Message; });
	}
	public function getMessages() { return $this->getMainmessages(); }

	public function getMainannonces() {
		return $this->getMainblogers()->filter(function($bloger) { return $bloger instanceOf Annonce; });
	}
	public function getAnnonces() { return $this->getMainannonces(); }

	public function getMainblogs() {
		return $this->getMainblogers()->filter(function($bloger) { return $bloger instanceOf Blog; });
	}
	public function getBlogs() { return $this->getMainblogs(); }

	public function isMainbloger(Bloger $bloger) {
		return $bloger->getMainrecipient() === $this;
	}


	/*************************************************************************************/
	/*** BLOGERS
	/*************************************************************************************/

	public function addBloger(Bloger $bloger, $inverse = true) {
		if(!$this->blogers->contains($bloger)) $this->blogers->add($bloger);
		if($inverse) $bloger->addRecipient($this, false);
		return $this;
	}

	public function removeBloger(Bloger $bloger, $inverse = true) {
		$this->blogers->removeElement($bloger);
		if($inverse) $bloger->removeRecepient($this, false);
		return $this;
	}

	public function getBlogers() {
		return $this->blogers;
	}

	public function getAllmessages() {
		return $this->blogers->filter(function($bloger) { return $bloger instanceOf Message; });
	}
	// public function getMessages() { return $this->getAllmessages(); }

	public function getAllannonces() {
		return $this->blogers->filter(function($bloger) { return $bloger instanceOf Annonce; });
	}
	// public function getAnnonces() { return $this->getAllannonces(); }

	public function getAllblogs() {
		return $this->blogers->filter(function($bloger) { return $bloger instanceOf Blog; });
	}
	// public function getBlogs() { return $this->getAllblogs(); }

}