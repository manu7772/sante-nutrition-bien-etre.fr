<?php
namespace ModelApi\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// Gedmo
use Gedmo\Mapping\Annotation as Gedmo;
// use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceTools;
// TierBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Service\serviceCertificat;

// use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ModelApi\UserBundle\Repository\CertificatRepository")
 * @ORM\Table(
 *      name="`certificat`",
 *		options={"comment":"Co-business"}
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "wsa_certificat_show",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_EDITOR", label="Certificat")
 */
class Certificat extends Tier {

	use \ModelApi\BaseBundle\Traits\BaseItem;

	const DEFAULT_ICON = 'fa-certificate';
	const ENTITY_SERVICE = serviceCertificat::class;
	const TYPES_CERTIFICATS = ['gouvernemental','industriel'];

	const DEFAULT_OWNER_DIRECTORY = 'system/Associates';

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\BaseBundle\Entity\Typitem")
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "label"="Types", "description"="Types de certificat", "choices"="auto"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "label"="Types", "description"="Types de certificat", "choices"="auto"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 * @Annot\Typitem(groups={"certificat_type"}, description="Types d'affichage des certificats")
	 * @Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 */
	protected $typecertificats;
	protected $typecertificatsChoices;

	/**
	 * @var array
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=101,
	 * 		options={"required"=true, "multiple"=true, "label"="Catégories", "description"="Catégories de certificat", "choices"="auto"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=101,
	 * 		options={"required"=true, "multiple"=true, "label"="Catégories", "description"="Catégories de certificat", "choices"="auto"}
	 * )
	 * @CRUDS\Show(role=true, order=101)
	 * @Annot\Typitem(groups={"certificat_cat"}, description="Catégories des certificats")
	 * @Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 */
	protected $categorys;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $title;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $menutitle;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $subtitle;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $description;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $accroche;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $resume;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $fulltext;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $functionality;


	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=700)
	 */
	protected $associateUsers;
	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=700)
	 */
	protected $associateEntreprises;
	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=700)
	 */
	protected $associatePartenaires;
	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=700)
	 */
	protected $associateCertificats;


	public function __construct() {
		parent::__construct();
		$this->typecertificats = new ArrayCollection();
		$this->typecertificatsChoices = new ArrayCollection();
		return $this;
	}


	/*************************************************************************************/
	/*** TYPECERTIFICATS
	/*************************************************************************************/

	/**
	 * Get typecertificats
	 * @return ArrayCollection <Typitem>
	 */
	public function getTypecertificats() {
		return $this->typecertificats;
	}

	/**
	 * Set typecertificats
	 * @param ArrayCollection <Typitem> $typecertificats
	 * @return Item
	 */
	public function setTypecertificats($typecertificats) {
		foreach ($this->typecertificats as $typecertificat) {
			if(!$typecertificats->contains($typecertificat)) $this->removeTypecertificat($typecertificat);
		}
		foreach ($typecertificats as $typecertificat) $this->addTypecertificat($typecertificat);
		return $this;
	}

	/**
	 * Add typecertificat
	 * @param Typitem $typecertificat
	 * @return Item
	 */
	public function addTypecertificat(Typitem $typecertificat, $inverse = true) {
		if(!$this->typecertificats->contains($typecertificat)) $this->typecertificats->add($typecertificat);
		if($inverse) $typecertificat->addItem($this, false);
		return $this;
	}

	/**
	 * Remove typecertificat
	 * @param Typitem $typecertificat
	 * @return Item
	 */
	public function removeTypecertificat(Typitem $typecertificat, $inverse = true) {
		$this->typecertificats->removeElement($typecertificat);
		if($inverse) $typecertificat->removeItem($this, false);
		return $this;
	}

	/**
	 * Get typecertificats Choices
	 * @return array
	 */
	public function getTypecertificatsChoices() {
		return $this->typecertificatsChoices;
	}

	/**
	 * Set typecertificats Choices
	 * @param array
	 * @return Item
	 */
	public function setTypecertificatsChoices($typecertificatsChoices) {
		$this->typecertificatsChoices = $typecertificatsChoices;
		return $this;
	}





	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getSlug();
	}



}




