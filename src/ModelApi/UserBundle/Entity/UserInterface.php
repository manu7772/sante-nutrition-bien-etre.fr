<?php
namespace ModelApi\UserBundle\Entity;

use FOS\UserBundle\Model\UserInterface as ModelUserInterface;

/**
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
interface UserInterface extends ModelUserInterface {

    // const ROLE_DEFAULT = 'ROLE_USER';
    const ROLE_ALLOWED_TO_SWITCH = 'ROLE_ALLOWED_TO_SWITCH';
    const IS_AUTHENTICATED_ANONYMOUSLY = 'IS_AUTHENTICATED_ANONYMOUSLY';
    const IS_AUTHENTICATED_REMEMBERED = 'IS_AUTHENTICATED_REMEMBERED';
    const ROLE_USER = 'ROLE_USER';
    const ROLE_TESTER = 'ROLE_TESTER';
    const ROLE_TRANSLATOR = 'ROLE_TRANSLATOR';
    const ROLE_EDITOR = 'ROLE_EDITOR';
    const ROLE_COLLAB = 'ROLE_COLLAB';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    // const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
	const MASTER_USER_NAME = "sadmin";
	const SERVER_USER_NAME = "root";

}
