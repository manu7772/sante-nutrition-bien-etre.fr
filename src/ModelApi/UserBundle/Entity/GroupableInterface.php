<?php
namespace ModelApi\UserBundle\Entity;

use ModelApi\UserBundle\Entity\Group;

/**
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
interface GroupableInterface {

	/**
	 * Gets the groups granted to the user.
	 *
	 * @return \Traversable
	 */
	public function getGroups();

	/**
	 * Gets the name of the groups which includes the user.
	 *
	 * @return array
	 */
	public function getGroupNames();

	/**
	 * Indicates whether the user belongs to the specified group or not.
	 *
	 * @param string $name Name of the group
	 *
	 * @return bool
	 */
	public function hasGroup(Group $group = null);

	/**
	 * Add a group to the user groups.
	 *
	 * @param Group $group
	 *
	 * @return self
	 */
	public function addGroup(Group $group);

	/**
	 * Remove a group from the user groups.
	 *
	 * @param Group $group
	 *
	 * @return self
	 */
	public function removeGroup(Group $group);


}
