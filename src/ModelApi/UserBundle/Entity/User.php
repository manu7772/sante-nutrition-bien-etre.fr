<?php
namespace ModelApi\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Util\TokenGenerator;
// Gedmo
use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Entity\Userrequest;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// UserBundle
use ModelApi\UserBundle\Entity\UserInterface;
use ModelApi\UserBundle\Entity\ModelGroup;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\OwnerTierInterface;
// use ModelApi\UserBundle\Entity\ModelPersonnemorale;
use ModelApi\UserBundle\Service\serviceUser;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// EventBundle
use API\EventBundle\Entity\Eventdate;
// 
use ModelApi\DevprintsBundle\Service\DevTerminal;

use \DateTime;

/**
 * User
 * 
 * @ORM\Entity(repositoryClass="ModelApi\UserBundle\Repository\UserRepository")
 * @ORM\EntityListeners({"ModelApi\UserBundle\Listener\TierListener","ModelApi\UserBundle\Listener\UserListener"})
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(
 *      name="`user`",
 *		options={"comment":"Users"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_user",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks()
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER", label="Utilisateur")
 * @Annot\HasTranslatable()
 */
class User extends Tier implements UserInterface, OwnerTierInterface {

	const DEFAULT_ICON = 'fa-user';
	const ENTITY_SERVICE = serviceUser::class;

	const DEFAULT_OWNER_DIRECTORY = false;

	/**
	 * DISABLE OWNER FOR USER!!!
	 * -------------------------
	 * @CRUDS\Create(show=false, update=false, options={"required"=false})
	 * @CRUDS\Update(show=false, update=false, options={"required"=false})
	 * @CRUDS\Show(role=false, order=210)
	 * !!!@Annot\AttachUser()
	 */
	protected $owner;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=1,
	 * 		options={"required"=true, "attr"={"autocomplete"="off"}},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=1,
	 * 		options={"required"=true, "attr"={"autocomplete"="off"}},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=1)
	 */
	protected $inputname;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=2,
	 * 		options={"required"=true},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=2,
	 * 		options={"required"=true},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=2)
	 */
	protected $email;

	/**
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role=false, order=100)
	 */
	protected $name;
	/**
	 * User username (login)
	 * @var string
	 * @ORM\Column(name="username", type="string", length=180)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=3,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=3,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role=false, order=3)
	 */
	protected $username;

	/**
	 * @var string
	 * @ORM\Column(name="usernameCanonical", type="string", length=180, unique=true)
	 */
	protected $usernameCanonical;

	/**
	 * First name
	 * @var string
	 * @ORM\Column(type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=4,
	 * 		options={"required"=false},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=4,
	 * 		options={"required"=false},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=4)
	 */
	protected $firstname;

	/**
	 * Second name
	 * @var string
	 * @ORM\Column(type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=5,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=5,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Show(role=false, order=5)
	 */
	protected $secondname;

	/**
	 * Last name
	 * @var string
	 * @ORM\Column(type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=4,
	 * 		options={"required"=false},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=4,
	 * 		options={"required"=false},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=4)
	 */
	protected $lastname;

	/**
	 * Last name
	 * @var string
	 * @ORM\Column(type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show=" entreprise.getPreferences()->getValue('UserBundle|ordernumber', false)",
	 * 		update=true,
	 * 		order=5,
	 * 		options={"required"=true, "label"="Numéro d'ordre", "description"="Pour vous inscrire, veuillez renseigner ce numéro d'ordre : 5 chiffres. Votre demande d'adhésion sera traitée plus rapidement."},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show=" entreprise.getPreferences()->getValue('UserBundle|ordernumber', false)",
	 * 		update=true,
	 * 		order=5,
	 * 		options={"required"=true, "label"="Numéro d'ordre", "description"="Pour vous inscrire, veuillez renseigner ce numéro d'ordre : 5 chiffres. Votre demande d'adhésion sera traitée plus rapidement."},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Show(role=false, order=5)
	 */
	protected $ordernumber;

	/**
	 * @var string
	 * @ORM\Column(name="emailCanonical", type="string", length=180, unique=true)
	 */
	protected $emailCanonical;

	/**
	 * The salt to use for hashing
	 * @var string
	 * @ORM\Column(name="salt", type="string", nullable=true)
	 */
	protected $salt;

	/**
	 * Encrypted password. Must be persisted.
	 * @var string
	 * @ORM\Column(name="password", type="string")
	 * !!!@CRUDS\Create(
	 * 		type="passwordType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=4,
	 * 		options={"required"=false},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * !!!@CRUDS\Update(
	 * 		type="passwordType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=4,
	 * 		options={"required"=false},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=4)
	 */
	protected $password;

	/**
	 * Temporary password
	 * @var string
	 * @ORM\Column(name="temppassword", type="string", nullable=true, unique=false)
	 */
	protected $temppassword;

	/**
	 * Plain password. Used for model validation. Must not be persisted.
	 * @var string
	 * @CRUDS\Create(
	 * 		type="RepeatedType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=99,
	 * 		options={"type"="Symfony\Component\Form\Extension\Core\Type\PasswordType", "required"=true, "invalid_message"="fos_user.password.mismatch", "first_options"={"label"="field.plainPassword", "attr"={"autocomplete"="off"}}, "second_options"={"label"="field.plainPassword_confirmation", "attr"={"autocomplete"="off"}}},
	 * 		contexts={"register", BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 	)
	 * @CRUDS\Show(role=false)
	 */
	protected $plainPassword;

	// /**
	//  * Plain password. Used for model validation. Must not be persisted.
	//  * @var string
	//  * @CRUDS\Create(
	//  * 		type="PasswordType",
	//  * 		show="ROLE_USER",
	//  * 		update="ROLE_USER",
	//  * 		order=99,
	//  * 		options={"required"=true}
	//  * 	)
	//  * @CRUDS\Update(
	//  * 		show=false,
	//  * 		update=false,
	//  * 	)
	//  * @CRUDS\Show(role=false)
	 // */
	// protected $confirmPlainPassword;

	/**
	 * Last login time
	 * @var DateTime
	 * @ORM\Column(name="lastLogin", type="datetime", nullable=true)
	 */
	protected $lastLogin;

	/**
	 * Random string sent to the user email address in order to verify it.
	 * @var string
	 * @ORM\Column(name="confirmationToken", type="string", length=180, unique=true, nullable=true)
	 */
	protected $confirmationToken;

	/**
	 * @var DateTime
	 * @ORM\Column(name="passwordRequestedAt", type="datetime", nullable=true)
	 */
	protected $passwordRequestedAt;

	/**
	 * Roles
	 * @var array
	 * @ORM\Column(name="roles", type="array")
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "expanded"=false, "by_reference"=false, "choices"="auto"},
	 * 		contexts={"medium"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "expanded"=false, "by_reference"=false, "choices"="auto"},
	 * 		contexts={"medium"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=100)
	 */
	protected $roles;

	protected $superadmin;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Directory")
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=200,
	 * 		options={"multiple"=false, "required"=true, "group_by"="rootparent.name", "class"="ModelApi\FileBundle\Entity\Directory", "query_builder"="qb_findForParentType"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=200,
	 * 		options={"multiple"=false, "required"=false, "group_by"="rootparent.name", "class"="ModelApi\FileBundle\Entity\Directory", "query_builder"="qb_findForParentType"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=200)
	 */
	protected $parent;

	/**
	 * @ORM\OneToMany(targetEntity="ModelApi\BaseBundle\Entity\Userrequest", mappedBy="requser", cascade={"persist"}, orphanRemoval=true)
	 */
	protected $userrequests;
	protected $onOrderRequestCollab;

	/**
	 * Newsletter
	 * @var boolean
	 * @CRUDS\Create(
	 * 		type="switcheryType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=false}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="switcheryType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=false}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=100)
	 */
	protected $newsletter;

	/**
	 * Cookies
	 * @var boolean
	 * @CRUDS\Create(
	 * 		type="switcheryType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="switcheryType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=100)
	 */
	protected $cookies;

	/**
	 * Forms contexts
	 * @var boolean
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "expanded"=false, "by_reference"=false, "choices"="auto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "expanded"=false, "by_reference"=false, "choices"="auto"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=100)
	 */
	protected $forms_contexts;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $tags;

	/**
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPicone", "label"="Image lien", "description"="Image servant à illustrer un lien externe vers le profil."},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPicone", "label"="Image lien", "description"="Image servant à illustrer un lien externe vers le profil."},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $picone;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $subtitle;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $menutitle;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $accroche;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=600,
	 * 		options={"required"=false, "description"="Description de l'utilisateur pour l'admin. Non utilisé en public."},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=600,
	 * 		options={"required"=false, "description"="Description de l'utilisateur pour l'admin. Non utilisé en public."},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=600)
	 */
	protected $description;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=601,
	 * 		options={"required"=false, "description"="Résumé de présentation de l'utilisateur."},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=601,
	 * 		options={"required"=false, "description"="Résumé de présentation de l'utilisateur."},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=601)
	 */
	protected $resume;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=602,
	 * 		options={"required"=false, "description"="Présentation de l'utilisateur."},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=602,
	 * 		options={"required"=false, "description"="Présentation de l'utilisateur."},
	 * 		contexts={"expert"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=602)
	 */
	protected $fulltext;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=706,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Associés Entreprises", "class"="ModelApi\UserBundle\Entity\Entreprise"},
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=706,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Associés Entreprises", "class"="ModelApi\UserBundle\Entity\Entreprise"},
	 * )
	 * @CRUDS\Show(role=true, order=706)
	 */
	protected $associateEntreprises;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $pageweb;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $urls;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1000,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Propriétaire de", "class"="ModelApi\FileBundle\Entity\Item"},
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1000,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Propriétaire de", "class"="ModelApi\FileBundle\Entity\Item"},
	 * )
	 * @CRUDS\Show(role=true, order=1000)
	 */
	protected $owneditems;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1000,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Manager de", "class"="ModelApi\FileBundle\Entity\Item"},
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1000,
	 * 		options={"required"=false, "by_reference"=false, "multiple"=true, "label"="Manager de", "class"="ModelApi\FileBundle\Entity\Item"},
	 * )
	 * @CRUDS\Show(role=true, order=1000)
	 */
	protected $itemformanagers;


	/**
	 * User constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->firstname = null;
		$this->secondname = null;
		$this->lastname = null;
		$this->ordernumber = null;
		$this->functionality = null;
		$this->enabled = true;
		$this->color = '#066379';
		$this->owner = null;
		$this->userrequests = new ArrayCollection();
		$this->onOrderRequestCollab = false;
		$this->temppassword = null;
		$this->password = TokenGenerator::generateToken();
		$this->setRoles(array(static::ROLE_DEFAULT));
		$this->setNormalizeNameMethod(); // No normalization
		return $this;
	}

	/**
	 * Get userrequests
	 * @return ArrayCollection <Userrequest>
	 */
	public function getUserrequests() {
		return $this->userrequests;
	}

	public function hasUserrequest(Userrequest $userrequest = null) {
		if($userrequest instanceOf Userrequest) return $this->userrequests->contains($userrequest);
		return !$this->userrequests->isEmpty();
	}

	/**
	 * Add Userrequest
	 * @param Userrequest $userrequest
	 * @return User
	 */
	public function addUserrequests(Userrequest $userrequest, $inverse = true) {
		if(!$this->userrequests->contains($userrequest)) $this->userrequests->add($userrequest);
		if($inverse) $userrequest->setRequser($this, false);
		return $this;
	}

	public function removeUserrequest(Userrequest $userrequest) {
		$this->userrequests->removeElement($userrequest);
		return $this;
	}

	public function getOnOrderRequestCollab() {
		return $this->onOrderRequestCollab;
	}
	public function isOnOrderRequestCollab() {
		return $this->getOnOrderRequestCollab();
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getSlug();
	}

	/**
	 * Set menutitle if not defined
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return self
	 */
	public function computeBaseTexts() {
		if(!serviceTools::isStringOverTags($this->getTitle())) {
			$title = trim(preg_replace('#\\s+#', ' ', $this->firstname.' '.$this->secondname.' '.$this->lastname));
			$this->setTitle($title);
		}
		parent::computeBaseTexts();
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function serialize() {
		return serialize(array(
			$this->password,
			$this->salt,
			$this->usernameCanonical,
			$this->username,
			$this->timezone,
			$this->enabled,
			$this->id,
			$this->email,
			$this->emailCanonical,
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function unserialize($serialized) {
		$data = unserialize($serialized);

		if (13 === count($data)) {
			// Unserializing a User object from 1.3.x
			unset($data[4], $data[5], $data[6], $data[9], $data[10]);
			$data = array_values($data);
		} elseif (11 === count($data)) {
			// Unserializing a User from a dev version somewhere between 2.0-alpha3 and 2.0-beta1
			unset($data[4], $data[7], $data[8]);
			$data = array_values($data);
		}

		list(
			$this->password,
			$this->salt,
			$this->usernameCanonical,
			$this->username,
			$this->timezone,
			$this->enabled,
			$this->id,
			$this->email,
			$this->emailCanonical
		) = $data;
	}


	/**
	 * {@inheritdoc}
	 */
	public function eraseCredentials() {
		$this->plainPassword = null;
		// $this->confirmPlainPassword = null;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getId() {
		return parent::getId();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getUsernameCanonical() {
		return $this->usernameCanonical;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getSalt() {
		return $this->salt;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getEmailCanonical() {
		return $this->emailCanonical;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getPlainPassword() {
		return $this->plainPassword;
	}

	// /**
	//  */
	// public function getConfirmPlainPassword() {
	// 	return $this->confirmPlainPassword;
	// }

	/**
	 * Gets the last login time.
	 * @return DateTime
	 */
	public function getLastLogin() {
		return $this->lastLogin;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getConfirmationToken() {
		return $this->confirmationToken;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isAccountNonExpired() {
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isAccountNonLocked() {
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isCredentialsNonExpired() {
		return true;
	}

	/**
	 * Get User full names or what is possible
	 */
	public function getFullnameOrUsername($size = 'medium') {
		$fullname = '';
		if($size == 'short' && serviceTools::isStringOverTags($this->getFirstname())) {
			$fullname = $this->getFirstname();
		} else if(serviceTools::isStringOverTags($this->getLastname())) {
			$fullname .= $this->getLastname();
			if(serviceTools::isStringOverTags($this->getFirstname())) $fullname = $this->getFirstname().' '.$fullname;
			if($size == 'long') $fullname .= ' <small><i>('.$this->getUsername().')</i></small>';
		} else {
			$fullname = $this->getUsername();
		}
		return $fullname;
	}

	public function getNameOrMailButUsername() {
		$name = '';
		if(serviceTools::isStringOverTags($this->getLastname())) {
			$name = serviceTools::isStringOverTags($this->getFirstname()) ? $this->getFirstname().' '.$this->getLastname() : $this->getLastname();
		} else {
			$name = $this->getEmail();
		}
		return $name;
	}

	/**
	 * Get User username (login)
	 * {@inheritdoc}
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setUsername($username) {
		// $this->username = $username;
		$this->setName($username);
		// if(null == $this->name) $this->name = $username;
		return $this;
	}

	/**
	 * Set name
	 * @param string $name
	 * @param boolean $updateNested = true
	 * @return self
	 */
	public function setName($name, $updateNested = true) {
		$this->username = $name;
		parent::setName($name, $updateNested);
		// DevTerminal::Info("     --> set Username (".$name."): ".$this->getName()." > ".$this->getUsername());
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setUsernameCanonical($usernameCanonical) {
		$this->usernameCanonical = $usernameCanonical;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setSalt($salt) {
		$this->salt = $salt;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setEmailCanonical($emailCanonical) {
		$this->emailCanonical = $emailCanonical;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setPassword($password) {
		$this->password = $password;
		$this->setPlainPassword($password);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setPlainPassword($password) {
		// $this->plainPassword = empty($this->getId()) ? $password : null;
		$this->plainPassword = $password;
		return $this;
	}

	/**
	 */
	// public function setConfirmPlainPassword($confirmPlainPassword) {
	// 	// $this->confirmPlainPassword = empty($this->getId()) ? $confirmPlainPassword : null;
	// 	$this->confirmPlainPassword = $confirmPlainPassword;
	// 	return $this;
	// }

	// public function isPlainPasswordValid() {
	// 	// if(!empty($this->getId())) {
	// 	// 	$this->eraseCredentials();
	// 	// 	return true;
	// 	// }
	// 	return $this->plainPassword === $this->confirmPlainPassword;
	// }

	public function isPasswordLenghtValid() {
		return (empty($this->plainPassword) && !empty($this->getId())) || strlen($this->plainPassword) > 5;
	}

	/**
	 * Set temporary password
	 */
	public function setTemppassword($password = null) {
		$this->temppassword = $password;
		if(!empty($this->temppassword)) {
			$this->setPassword($this->temppassword);
			$this->setPlainPassword($this->temppassword);
		}
		return $this;
	}

	/**
	 * Get temporary password
	 */
	public function getTemppassword() {
		return $this->temppassword;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setLastLogin(DateTime $time = null) {
		if($time instanceOf DateTime) $this->setTemppassword(null);
		$this->lastLogin = $time;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setConfirmationToken($confirmationToken) {
		$this->confirmationToken = $confirmationToken;
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setPasswordRequestedAt(DateTime $date = null) {
		$this->passwordRequestedAt = $date;
		return $this;
	}

	/**
	 * Gets the timestamp that the user requested a password reset.
	 * @return null|DateTime
	 */
	public function getPasswordRequestedAt() {
		return $this->passwordRequestedAt;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isPasswordRequestNonExpired($ttl) {
		return $this->getPasswordRequestedAt() instanceOf DateTime &&
			   $this->getPasswordRequestedAt()->getTimestamp() + $ttl > time();
	}


	public function getFirstname() {
		return $this->firstname;
	}

	public function setFirstname($firstname) {
		$this->firstname = trim($firstname);
		return $this;
	}

	public function getSecondname() {
		return $this->secondname;
	}

	public function setSecondname($secondname) {
		$this->secondname = trim($secondname);
		return $this;
	}

	public function getLastname() {
		return $this->lastname;
	}

	public function setLastname($lastname) {
		$this->lastname = trim($lastname);
		return $this;
	}

	public function getOrdernumber() {
		return $this->ordernumber;
	}

	public function setOrdernumber($ordernumber = null) {
		$old_order = $this->ordernumber;
		$this->ordernumber = trim($ordernumber);
		if($old_order !== $this->ordernumber) {
			if(!empty($this->ordernumber) && $this->isValidOrdernumber()) {
				if(!$this->hasRole(static::ROLE_COLLAB)) $this->onOrderRequestCollab = true;
				// $this->addRole(static::ROLE_COLLAB);
			}
			// if(empty($this->ordernumber)) $this->removeRole(static::ROLE_COLLAB);
		}
		return $this;
	}

	public function isValidOrdernumber() {
		return empty($this->ordernumber) || preg_match('/\\d{5,5}$/', $this->ordernumber);
	}

	/**
	 * Get functionality
	 * @return string
	 */
	public function getFunctionality() {
		return $this->functionality;
	}

	/**
	 * Set functionality
	 * @param string $functionality
	 * @return User
	 */
	public function setFunctionality($functionality) {
		$this->functionality = $functionality;
		return $this;
	}

	/**
	 * @param bool $boolean
	 * @return User
	 */
	public function setEnabled($boolean = true) {
		$this->enabled = $boolean;
		return $this;
	}

	/**
	 * Checks whether the user is enabled.
	 *
	 * Internally, if this method returns false, the authentication system
	 * will throw a DisabledException and prevent login.
	 *
	 * @return bool true if the user is enabled, false otherwise
	 *
	 * @see DisabledException
	 */
	public function isEnabled() {
		return $this->enabled;
	}



	/*************************************************************************************/
	/*** ROLES
	/*************************************************************************************/

	// /**
	//  * {@inheritdoc}
	//  * @param string $role
	//  * @return User
	//  */
	// public function addRole($role) {
	// 	$role = strtoupper($role);
	// 	if ($role === static::ROLE_DEFAULT) {
	// 		return $this;
	// 	}
	// 	$this->roles = array_unique(array_merge($this->roles, array($role)));
	// 	return $this;
	// }

	// /**
	//  * Set roles
	//  * @param array <string> $roles
	//  * @return User
	//  */
	// public function setRoles(array $roles) {
	// 	$this->roles = array();
	// 	foreach ($roles as $role) $this->addRole($role);
	// 	return $this;
	// }

	// /**
	//  * {@inheritdoc}
	//  * Get roles
	//  * @param array <string>
	//  */
	// public function getRoles() {
	// 	// we need to make sure to have at least one role : static::ROLE_DEFAULT
	// 	return array_unique(array_merge($this->roles, $this->getAllGroupsRoles(), array(static::ROLE_DEFAULT)));
	// }

	// /**
	//  * {@inheritdoc}
	//  */
	// public function removeRole($role) {
	// 	if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
	// 		unset($this->roles[$key]);
	// 		$this->roles = array_values($this->roles);
	// 	}
	// 	return $this;
	// }

/*	public function getRolesChoices($all = false) {
		$serviceGrants = $this->serviceCruds->getServiceGrants();
		if(!$all && !$serviceGrants->isGranted(static::ROLE_ADMIN)) return [];
		//if(!$serviceGrants->isGranted($this->getHigherRole(false))) return [];
		$list = [
			'Utilisateur' => static::ROLE_USER,
			'Testeur' => static::ROLE_TESTER,
			'Traducteur' => static::ROLE_TRANSLATOR,
			'Editeur' => static::ROLE_EDITOR,
			'Collaborateur' => static::ROLE_COLLAB,
			'Administrateur' => static::ROLE_ADMIN,
		];
		if($all || $serviceGrants->isGranted(static::ROLE_SUPER_ADMIN)) $list['Super administrateur'] = static::ROLE_SUPER_ADMIN;
		return $list;
	}
*/
	/*public function isUpdateRoles() {
		return $this->serviceCruds->getServiceGrants()->isGranted(static::ROLE_ADMIN);
	}*/


	/*************************************************************************************/
	/*** RIGHTS & GRANTS
	/*************************************************************************************/

	/**
	 * {@inheritdoc}
	 */
	public function setSuperAdmin($boolean = true) {
		if (true === $boolean) {
			$this->addRole(static::ROLE_SUPER_ADMIN);
		} else {
			$this->removeRole(static::ROLE_SUPER_ADMIN);
			foreach ($this->groups as $group) {
				if($group->hasRole(static::ROLE_SUPER_ADMIN)) $this->removeGroup($group);
			}
		}
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isSuperAdmin() {
		return $this->hasRole(static::ROLE_SUPER_ADMIN);
	}
	// public function isSuperadmin() {
	// 	return $this->isSuperAdmin();
	// }



}