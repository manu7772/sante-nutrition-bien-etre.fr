<?php
namespace ModelApi\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// Gedmo
use Gedmo\Mapping\Annotation as Gedmo;
// use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceTools;
// TierBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\OwnerTierInterface;
use ModelApi\UserBundle\Service\servicePartenaire;

// use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ModelApi\UserBundle\Repository\PartenaireRepository")
 * @ORM\Table(
 *      name="`partenaire`",
 *		options={"comment":"Co-business"}
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "wsa_partenaire_show",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_EDITOR", label="Partenaire")
 */
class Partenaire extends Tier implements OwnerTierInterface {

	use \ModelApi\BaseBundle\Traits\BaseItem;

	const DEFAULT_ICON = 'fa-building';
	const ENTITY_SERVICE = servicePartenaire::class;
	const TYPES_PARTENAIRES = ['institution','touristique'];

	const DEFAULT_OWNER_DIRECTORY = 'system/Associates';

	/**
	 * Newsletter
	 * @var boolean
	 * @CRUDS\Create(
	 * 		type="switcheryType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=false}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="switcheryType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=false}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=100)
	 */
	protected $newsletter;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\BaseBundle\Entity\Typitem")
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "label"="Types", "description"="Types de partenaire", "choices"="auto"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true, "label"="Types", "description"="Types de partenaire", "choices"="auto"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 * @Annot\Typitem(groups={"partenaire_type"}, description="Types de partenaire")
	 * @Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 */
	protected $typepartenaires;
	protected $typepartenairesChoices;

	/**
	 * @var array
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=101,
	 * 		options={"required"=true, "multiple"=true, "label"="Catégories", "description"="Catégories de partenaire", "choices"="auto"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=101,
	 * 		options={"required"=true, "multiple"=true, "label"="Catégories", "description"="Catégories de partenaire", "choices"="auto"}
	 * )
	 * @CRUDS\Show(role=true, order=101)
	 * @Annot\Typitem(groups={"partenaire_cat"}, description="Catégories de partenaire")
	 * @Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 */
	protected $categorys;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $title;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $menutitle;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $subtitle;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $description;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $accroche;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $resume;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $fulltext;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Update(show=false, update=false, contexts={"translation", "simple", "medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $functionality;


	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=700)
	 */
	protected $associateUsers;
	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=700)
	 */
	protected $associateEntreprises;
	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=700)
	 */
	protected $associatePartenaires;
	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=700)
	 */
	protected $associateCertificats;


	public function __construct() {
		parent::__construct();
		$this->typepartenaires = new ArrayCollection();
		$this->typepartenairesChoices = new ArrayCollection();
		return $this;
	}


	/*************************************************************************************/
	/*** TYPEPARTENAIRES
	/*************************************************************************************/

	/**
	 * Get typepartenaires
	 * @return ArrayCollection <Typitem>
	 */
	public function getTypepartenaires() {
		return $this->typepartenaires;
	}

	/**
	 * Set typepartenaires
	 * @param ArrayCollection <Typitem> $typepartenaires
	 * @return Item
	 */
	public function setTypepartenaires($typepartenaires) {
		foreach ($this->typepartenaires as $typepartenaire) {
			if(!$typepartenaires->contains($typepartenaire)) $this->removeTypepartenaire($typepartenaire);
		}
		foreach ($typepartenaires as $typepartenaire) $this->addTypepartenaire($typepartenaire);
		return $this;
	}

	/**
	 * Add typepartenaire
	 * @param Typitem $typepartenaire
	 * @return Item
	 */
	public function addTypepartenaire(Typitem $typepartenaire, $inverse = true) {
		if(!$this->typepartenaires->contains($typepartenaire)) $this->typepartenaires->add($typepartenaire);
		if($inverse) $typepartenaire->addItem($this, false);
		return $this;
	}

	/**
	 * Remove typepartenaire
	 * @param Typitem $typepartenaire
	 * @return Item
	 */
	public function removeTypepartenaire(Typitem $typepartenaire, $inverse = true) {
		$this->typepartenaires->removeElement($typepartenaire);
		if($inverse) $typepartenaire->removeItem($this, false);
		return $this;
	}

	/**
	 * Get typepartenaires Choices
	 * @return array
	 */
	public function getTypepartenairesChoices() {
		return $this->typepartenairesChoices;
	}

	/**
	 * Set typepartenaires Choices
	 * @param array
	 * @return Item
	 */
	public function setTypepartenairesChoices($typepartenairesChoices) {
		$this->typepartenairesChoices = $typepartenairesChoices;
		return $this;
	}






	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getSlug();
	}




}




