<?php
namespace ModelApi\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
// use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Entity\DataFillableInterface;
use ModelApi\BaseBundle\Service\serviceTools;
// use ModelApi\BaseBundle\Service\serviceEntities;
// UserBundle
use ModelApi\UserBundle\Entity\BaseUser;
use ModelApi\UserBundle\Entity\ModelUser;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\GroupInterface;
use ModelApi\UserBundle\Service\serviceGroup;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Service\FileManager;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

use \ReflectionClass;
use \DateTime;

/**
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * Group
 * 
 * @ORM\Entity(repositoryClass="ModelApi\UserBundle\Repository\GroupRepository")
 * @ORM\EntityListeners({"ModelApi\UserBundle\Listener\GroupListener"})
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(
 *      name="`group`",
 *		options={"comment":"Groups"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_group",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks()
 * @CRUDS\Actions(show="ROLE_TRANSLATOR", create="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", delete="ROLE_TRANSLATOR", label="Groupe")
 */
class Group implements GroupInterface, CrudInterface, DataFillableInterface {

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\BaseBundle\Traits\DataFiller;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	const DEFAULT_ICON = 'fa-users';
	const ENTITY_SERVICE = serviceGroup::class;
	const SHORTCUT_CONTROLS = true;

	// const DELETABLE = 'ROLE_SUPER_ADMIN';

	/**
	 * Group Id
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", nullable=false, unique=true)
	 * @CRUDS\Create(
	 * 		contexts={"all"},
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		contexts={"all","change_name"},
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=true}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=100)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(name="`longname`", type="string", nullable=false, unique=true)
	 * @CRUDS\Show(role="ROLE_USER", order=101)
	 */
	protected $longname;

	/**
	 * @var integer
	 * Parent
	 * @ORM\ManyToOne(targetEntity="ModelApi\UserBundle\Entity\Group", inversedBy="childs", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(name="group_parentgroups")
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		contexts={"all"},
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=102,
	 * 		options={"multiple"=false, "required"=false, "group_by"="groupedName", "class"="ModelApi\UserBundle\Entity\Group", "query_builder"="qb_findTierAllGroups"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		contexts={"all"},
	 * 		show="ROLE_USER",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=102,
	 * 		options={"disabled"="object.isRoot()", "multiple"=false, "required"=false, "group_by"="groupedName", "class"="ModelApi\UserBundle\Entity\Group", "query_builder"="qb_findTierAllGroups"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=102)
	 */
	protected $parent;

	/**
	 * Root Group
	 * @var integer
	 * @ORM\ManyToOne(targetEntity="ModelApi\UserBundle\Entity\Group", fetch="EAGER")
	 * @ORM\JoinColumn(name="group_rootparent")
	 */
	protected $rootParent;

	/**
	 * @var integer
	 * Level
	 * @ORM\Column(name="lvl", type="integer")
	 * @CRUDS\Show(role="ROLE_USER", order=103)
	 */
	protected $lvl;

	/**
	 * @var array
	 * Direct childs
	 * @ORM\OneToMany(targetEntity="ModelApi\UserBundle\Entity\Group", mappedBy="parent")
	 * @ORM\JoinColumn(name="group_childgroups")
	 * @ORM\OrderBy({"name" = "ASC"})
	 */
	protected $childs;

	/**
	 * @var array
	 * All childs
	 */
	protected $allChilds;

	/**
	 * @var array
	 * All parents
	 */
	protected $allParents;

	/**
	 * @var array
	 * All parents
	 * @ORM\Column(name="groupschema", type="json_array", nullable=false, unique=false)
	 */
	protected $groupschema;

	/**
	 * Roles
	 * @var array
	 * @ORM\Column(name="roles", type="array")
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=200,
	 * 		options={"required"=false, "choices"="auto", "multiple"=true}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=200,
	 * 		options={"required"=false, "choices"="auto", "multiple"=true}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=200)
	 */
	protected $roles;

	/**
	 * Public
	 * @var string
	 * @ORM\Column(name="role", type="string", nullable=true, unique=false)
	 */
	protected $role;

	/**
	 * @var boolean
	 * Is public
	 * @CRUDS\Show(role="ROLE_USER", order=200, label="Public")
	 */
	protected $public;

	// /**
	//  * @ORM\ManyToMany(targetEntity="ModelApi\FileBundle\Entity\Item", inversedBy="compgroups", cascade={"persist"})
	//  * @ORM\JoinColumn(onDelete="SET NULL")
	//  */
	// protected $items;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\UserBundle\Entity\Tier", mappedBy="maingroup", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		contexts={"all"},
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=300,
	 * 		options={"required"="object.isRoot()", "label"="Propriétaire", "multiple"=false, "class"="ModelApi\UserBundle\Entity\Tier"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		contexts={"all"},
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=300,
	 * 		options={"required"="object.isRoot()", "label"="Propriétaire", "multiple"=false, "class"="ModelApi\UserBundle\Entity\Tier"}
	 * 	)
	 * @CRUDS\Show(label="Propriétaire", role="ROLE_USER", order=300)
	 */
	protected $groupowner;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		contexts={"all"},
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=300,
	 * 		options={"required"=false, "multiple"=true, "group_by"="shortname", "class"="ModelApi\UserBundle\Entity\Tier"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		contexts={"all"},
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=300,
	 * 		options={"required"=false, "multiple"=true, "group_by"="shortname", "class"="ModelApi\UserBundle\Entity\Tier"}
	 * 	)
	 * @CRUDS\Show(label="Propriétaire", role="ROLE_USER", order=300)
	 */
	protected $membres;

	protected $rootSchemaChecked;


	/**
	 * Group constructor.
	 */
	public function __construct() {
		$this->name = null;
		$this->longname = null;
		$this->parent = null;
		$this->rootParent = null;
		$this->lvl = 0;
		$this->childs = new ArrayCollection();
		$this->allChilds = new ArrayCollection();
		$this->allParents = new ArrayCollection();
		$this->groupschema = [];
		$this->roles = array();
		$this->role = null;
		// $this->items = new ArrayCollection();
		$this->public = true;
		$this->groupowner = null;
		$this->membres = new ArrayCollection();
		$this->rootSchemaChecked = false;
	}

	public function getMembres() {
		if(!($this->membres instanceOf ArrayCollection)) $this->membres = new ArrayCollection();
		return $this->membres;
	}

	public function setMembres($membres) {
		if(!($this->membres instanceOf ArrayCollection)) $this->membres = new ArrayCollection();
		foreach ($membres as $membre) {
			$membre->addGroup($this);
		}
		return $this;
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getName();
	}

	/**
	 * Is Group valid?
	 * @return boolean
	 */
	
	public function isValid() {
		$this->isGroupownerValid() && $this->isRecursivityValid();
	}

	public function isGroupownerValid() {
		// $root = $this->getRootParent(false);
		// if($root instanceOf Group) {
		// 	if($root->getGroupowner() instanceOf Tier && !($this->getGroupowner() instanceOf Tier)) return false;
		// }
		return true;
	}

	public function isRecursivityValid() {
		if($this->hasParent($this, true) || $this->hasChild($this, true)) return false;
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getId() {
		return $this->id;
	}


	public function getJstreetype() {
		return $this->hasParent(null) ? $this->getShortname() : 'Root';
	}

	public function getJstreeData() {
		$parent = $this->getParent();
		return array(
			'valid_children' => [$this->getShortname()],
			'can_have_children' => true,
			'name' => $this->getName(),
			'shortname' => $this->getShortname(),
			'parent_id' => $parent instanceOf Group ? $parent->getId() : null,
			'instances' => $this->getInstances(),
		);
	}

	public function getJstreeState() {
		return [
			'opened' => $this->getLvl() < 4,
			// 'opened' => !$this->hasParent(null),
			'disabled' => false,
			'selected' => false,
			'deletable' => !$this->hasChild(),
		];
	}

	public function getLiattr() {
		return [
			'title' => $this->getLongname(),
		];
	}

	public function getAattr() {
		return [];
	}


	/**
	 * Compute long name
	 * @return Group
	 */
	public function computeLongname() {
		if($this->getGroupowner() instanceOf Tier) {
			$groupownername = $this->getGroupowner()->getName();
			$this->longname = FileManager::urluppercase($groupownername).serviceGroup::NAME_JOINER.$this->getName();
		} else {
			$this->longname = $this->getName();
		}
		return $this;
	}

	/**
	 * Get long name
	 * @return string
	 */
	public function getLongname() {
		return $this->longname;
	}

	/**
	 * {@inheritdoc}
	 * Get name for Jstree
	 * @return string
	 */
	public function getText() {
		return $this->name;
	}

	/**
	 * {@inheritdoc}
	 * Get name
	 * @return string
	 */
	public function getGroupedName() {
		$rootParent = $this->getRootParent(false);
		$groupedName = $rootParent instanceOf Group ? $rootParent->getName() : $this->getName();
		return $groupedName;
	}

	/**
	 * {@inheritdoc}
	 * Get name
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * {@inheritdoc}
	 * Set name
	 * @param string $name
	 * @return Group
	 */
	public function setName($name) {
		serviceTools::computeTextOrNull($name);
		serviceTools::removeSeparator($name);
		$this->name = FileManager::urluppercase($name);
		$this->computeLongname();
		return $this;
	}


	/*************************************************************************************/
	/*** ICON
	/*************************************************************************************/

	public function getIcon() {
		return 'fa '.static::DEFAULT_ICON;
	}




	/*************************************************************************************/
	/*** ITEMS
	/*************************************************************************************/


	// /**
	//  * Get Items
	//  * @return ArrayCollection <Item>
	//  */
	// public function getItems() {
	// 	return $this->items;
	// }

	// /**
	//  * Add Item
	//  * @param Item $item
	//  * @return Group
	//  */
	// public function addItem(Item $item, $inverse = true) {
	// 	if(!$this->items->contains($item)) $this->items->add($item);
	// 	if($inverse) $item->addGroup($this, false);
	// 	return $this;
	// }

	// /**
	//  * Remove Item
	//  * @param Item $item
	//  * @return Group
	//  */
	// public function removeItem(Item $item, $inverse = true) {
	// 	$this->items->removeElement($item);
	// 	if($inverse) $item->removeGroup($this, false);
	// 	return $this;
	// }

	// /**
	//  * @ORM\PreRemove()
	//  * Remove all Items
	//  * @return Group
	//  */
	// public function preRemove_Group() {
	// 	foreach ($this->items as $item) $this->removeItem($item);
	// 	return $this;
	// }




	/*************************************************************************************/
	/*** GROUPOWNER
	/*************************************************************************************/

	/**
	 * Get groupowner
	 * @return Tier | null
	 */
	public function getGroupowner() {
		// if($this->hasParent(null)) $this->groupowner = $this->getRootParent(false)->getGroupowner();
		return $this->groupowner;
	}

	/**
	 * Set groupowner
	 * @param Tier $groupowner
	 * @param boolean $inverse = true
	 * @return Group
	 */
	public function setGroupowner(Tier $groupowner, $inverse = true) {
		// if($this->hasParent(null)) return $this;
		// if($this->hasParent(null)) throw new Exception("Error ".__METHOD__."(): this Group has parent(s), so can not have an Groupowner! Please define groupowner in root group if necessary!", 1);
		// if($this->hasParent()) {
		// 	$rootParent = $this->getRootParent();
		// 	$rootParent->setGroupowner($groupowner, true);
		// } else
		if($this->isRoot()) {
			if($groupowner === $this->groupowner) return $this;
			if($this->groupowner instanceOf Tier) $this->groupowner->setMaingroup(null, false);
			$this->groupowner = $groupowner;
			$this->setName($this->groupowner->getMaingroupName());
			if($inverse) $this->groupowner->setMaingroup($this, false);
			foreach ($this->getAllChilds() as $child) $child->computeLongname();
		} else {
			throw new Exception("Error ".__METHOD__."(): this Group has parent(s), so can not have an Groupowner! Please define groupowner in root group if necessary!", 1);
		}
		return $this;
	}

	/*************************************************************************************/
	/*** ROLES
	/*************************************************************************************/


	/**
	 * {@inheritdoc}
	 */
	public function addRole($role) {
		if (!$this->hasRole($role)) $this->roles[] = strtoupper($role);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function hasRole($role) {
		return in_array(strtoupper($role), $this->roles, true);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRoles() {
		return $this->roles;
	}

	/**
	 * Get Roles choices
	 * @return array <string>
	 */
	public function getRolesChoices() {
		return array(
			User::ROLE_USER => User::ROLE_USER,
			User::ROLE_TESTER => User::ROLE_TESTER,
			User::ROLE_TRANSLATOR => User::ROLE_TRANSLATOR,
			User::ROLE_EDITOR => User::ROLE_EDITOR,
			User::ROLE_COLLAB => User::ROLE_COLLAB,
			User::ROLE_ADMIN => User::ROLE_ADMIN,
			User::ROLE_SUPER_ADMIN => User::ROLE_SUPER_ADMIN,
		);
	}

	/**
	 * {@inheritdoc}
	 * Remove Role $role
	 * @param string $role
	 * @return Group
	 */
	public function removeRole($role) {
		// if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
		// 	unset($this->roles[$key]);
		// 	$this->roles = array_values($this->roles);
		// }
		$role = strtoupper($role);
		$this->roles = array_filter($this->roles, function($oneRole) use ($role) {
			return $oneRole !== $role;
		});
		return $this;
	}

	/**
	 * Remove all Roles
	 * @return Group
	 */
	public function removeRoles() {
		$this->roles = array();
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setRoles(array $roles) {
		$this->removeRoles();
		foreach($roles as $role) $this->addRole($role);
		return $this;
	}



	/**
	 * Access role (lower role in roles)
	 */
	public function setRole($role = null) {
		$this->role = $role;
		return $this;
	}

	public function getRole() {
		return $this->role;
	}



	public function isPublic() {
		$this->public = count($this->roles) < 1;
		return $this->public;
	}

	public function getPublic() {
		return $this->isPublic();
	}

	public function setPublic($public = true) {
		if($public) {
			$this->removeRoles();
		} else if($this->isPublic()) {
			$this->addRole(User::ROLE_USER);
		}
		return $this;
	}


	/*************************************************************************************/
	/*** PARENT GROUPS
	/*************************************************************************************/


	/**
	 * Is root Group
	 * @return boolean
	 */
	public function isRoot() {
		return !$this->hasParent(null);
	}

	/**
	 * Has parent Group
	 * @param Group $group = null
	 * @param boolean $recursive = false
	 * @return boolean
	 */
	public function hasParent(Group $group = null) {
		if($group instanceOf Group) return $group === $this->getParent();
		return $this->parent instanceOf Group;
	}

	/**
	 * Set parent Group
	 * @param Group $group = null
	 * @param boolean $reverse = true
	 * @return Group
	 */
	public function setParent(Group $group = null, $reverse = true) {
		if($group === $this->parent) return $this;
		if($this->parent instanceOf Group) $this->parent->removeChild($this, false);
		if($group instanceOf Group) {
			$this->parent = $group;
			if($reverse) $this->parent->addChild($this, false);
		}
		$this->parent = $group;
		$this->computeAllParents();
		return $this;
	}

	/**
	 * Get parent Group
	 * @return Group
	 */
	public function getParent() {
		return $this->parent;
	}

	/**
	 * Get groupschema
	 * @return array
	 */
	public function getGroupschema($refresh = false) {
		if($refresh) $this->computeSchema();
		return $this->groupschema;
	}

	public function getSchemaIndex() {
		return $this->getMicrotimeid();
		// return $this->getLongname();
	}

	public function constructSchema($path = []) {
		$this->groupschema['roles'] = $this->getRoles();
		$this->groupschema['path'] = $path;
		$this->groupschema['path'][] = $this->getSchemaIndex();
		$this->groupschema['lvl'] = $this->lvl = count($this->groupschema['path']) - 1;
		$this->groupschema['schema'] = [];
		foreach ($this->childs as $child) {
			$child->computeLongname();
			$this->groupschema['schema'][$child->getSchemaIndex()] = $child->constructSchema($this->groupschema['path']);
		}
		// $this->allChilds = new ArrayCollection(array_unique(array_merge($this->allChilds->toArray(), $child->getAllChilds(true)->toArray())));
		return $this->groupschema;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * Get root parent
	 * @param boolean $himselfIfRoot = false
	 * @return Group | null
	 */
	public function computeSchema() {
		$root = $this->getRootParent(true, true);
		if(!$root->isRootSchemaChecked()) {
			$root->constructSchema();
			$root->setRootSchemaChecked();
		}
		// if($this->isRoot()) {
		// 	if(!$this->rootSchemaChecked) {
		// 		$this->constructSchema();
		// 		$this->rootSchemaChecked = true;
		// 	}
		// } else {
		// 	$this->getRootParent(false, true)->computeSchema();
		// }
		return $this;
	}

	/**
	 * @ORM\PostLoad()
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 * @return Group | null
	 */
	public function resetRootSchemaChecked() {
		$this->rootSchemaChecked = false;
		$this->computeSchema();
		// $this->rootSchemaChecked = false;
		return $this;
	}

	public function isRootSchemaChecked() {
		return $this->rootSchemaChecked;
	}

	public function setRootSchemaChecked() {
		$this->rootSchemaChecked = true;
		return $this;
	}

	/**
	 * Compute root parent
	 * @return Group | null
	 */
	public function computeRootParent() {
		$root = $this->getAllParents(false, true);
		return $this->rootParent = $root->isEmpty() ? null : $root->last();
	}

	/**
	 * Get root parent
	 * @param boolean $himselfIfRoot = false
	 * @return Group | null
	 */
	public function getRootParent($himselfIfRoot = false, $recompute = false) {
		if($recompute || !($this->allParents instanceOf ArrayCollection)) $this->computeRootParent();
		return $himselfIfRoot && empty($this->rootParent) ? $this : $this->rootParent;
	}

	/**
	 * Compute all parent Groups
	 * @return Group
	 */
	public function computeAllParents() {
		$this->allParents = new ArrayCollection();
		$parent = $this;
		while (null !== $parent = $parent->getParent()) {
			if($this->allParents->contains($parent)) throw new Exception("Error ".__METHOD__."(): this Group contains parent ".json_encode($parent->getName())." twice!", 1);
			if($this->allParents->contains($this)) throw new Exception("Error ".__METHOD__."(): this Group contains himself!", 1);
			if(!$this->allParents->contains($parent)) $this->allParents->add($parent);
		}
		$this->computeLongname();
		// $this->lvl = $this->allParents->count();
		// $this->allParents = new ArrayCollection(array_unique($this->allParents->toArray()));
		// $ids = $this->allParents->map(function($group) { return $group->getId(); })->toArray();
		// var_dump($this->getName().' => '.implode(' / ', $ids));
		return $this;
	}

	/**
	 * Get all parent Groups
	 * @param boolean $inverse = false
	 * @param boolean $recompute = false
	 * @return Group
	 */
	public function getAllParents($inverse = false, $recompute = false) {
		if($recompute || !($this->allParents instanceOf ArrayCollection)) $this->computeAllParents();
		if($inverse) return new ArrayCollection(array_reverse($this->allParents->toArray()));
		return clone $this->allParents;
	}

	/**
	 * Get all parent Groups (alias)
	 * @param boolean $inverse = false
	 * @param boolean $recompute = false
	 * @return ArrayCollection <Group> 
	 */
	public function getParents($inverse = false, $recompute = false) {
		return clone $this->getAllParents($inverse, $recompute || !($this->allParents instanceOf ArrayCollection));
	}

	public function getLvl() {
		return $this->lvl;
	}


	/*************************************************************************************/
	/*** CHILD GROUPS
	/*************************************************************************************/

	/**
	 * Set Childs
	 * @param ArrayCollection $childs
	 * @return Group
	 */
	public function setChilds($childs) {
		foreach ($this->childs as $child) {
			if(!$childs->contains($child)) $this->removeChild($child);
		}
		foreach ($childs as $child) $this->addChild($child);
		return $this;
	}

	/**
	 * Add child Group
	 * @param Group $child
	 * @param boolean $reverse = true
	 * @return Group
	 */
	public function addChild(Group $child, $reverse = true) {
		if(!$this->hasParent($child, true)) {
			if(!$this->hasChild($child, false)) $this->childs->add($child);
			if(!$this->hasChild($child, true)) $this->allChilds->add($child);
			if($reverse) {
				// if($child->getParent() !== $this && $child->getParent() instanceOf Group) $child->getParent()->removeChild($child, false);
				$child->setParent($this, false);
			}
		}
		return $this;
	}

	/**
	 * Remove child Group
	 * @param Group $group
	 * @return Group
	 */
	public function removeChild(Group $group, $reverse = true) {
		$this->childs->removeElement($group);
		$this->allChilds->removeElement($group);
		if($reverse) $group->setParent(null);
		return $this;
	}

	/**
	 * Get child Groups
	 * @return ArrayCollection <Group>
	 */
	public function getChilds() {
		return $this->childs;
	}

	/**
	 * Get number of child Groups
	 * @return integer
	 */
	public function getCountChilds() {
		return $this->childs->count();
	}

	/**
	 * Compute all childs
	 * @return Group
	 */
	public function computeAllChilds() {
		$this->allChilds = clone $this->childs;
		foreach($this->childs as $child){
			$this->allChilds = new ArrayCollection(array_unique(array_merge($this->allChilds->toArray(), $child->getAllChilds(true)->toArray())));
		}
		return $this;
	}

	/**
	 * Get ALL child Groups
	 * @return ArrayCollection <Group>
	 */
	public function getAllChilds($recompute = true) {
		if($recompute || !($this->allChilds instanceOf ArrayCollection)) $this->computeAllChilds();
		return $this->allChilds;
	}

	/**
	 * Has child group?
	 * @param Group $group
	 * @param boolean $recursive = false
	 * @return boolean
	 */
	public function hasChild(Group $group = null, $recursive = false) {
		if(!($group instanceOf Group)) return !$this->childs->isEmpty();
		return $recursive ? $this->getAllChilds()->contains($group) : $this->childs->contains($group);
	}


}
