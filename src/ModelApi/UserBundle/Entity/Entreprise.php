<?php
namespace ModelApi\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// Gedmo
// use Gedmo\Mapping\Annotation as Gedmo;
// use Gedmo\Translatable\Translatable;
// BaseBundle
use ModelApi\BaseBundle\Entity\Reseausocial;
use ModelApi\BaseBundle\Service\serviceTools;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\Partenaire;
use ModelApi\UserBundle\Entity\Certificat;
// use ModelApi\UserBundle\Entity\OwnerTierInterface;
use ModelApi\UserBundle\Service\serviceEntreprise;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Favicon;
use ModelApi\BinaryBundle\Entity\Photo;
// FileBundle
use ModelApi\FileBundle\Entity\Image;

// use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ModelApi\UserBundle\Repository\EntrepriseRepository")
 * @ORM\Table(
 *      name="`entreprise`",
 *		options={"comment":"Website Factories"}
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_entreprise",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_ADMIN", update="ROLE_ADMIN", delete="ROLE_ADMIN")
 */
class Entreprise extends Tier {

	use \ModelApi\BaseBundle\Traits\BaseItem;

	const DEFAULT_ICON = 'fa-building-o';
	const ENTITY_SERVICE = serviceEntreprise::class;

	const DEFAULT_OWNER_DIRECTORY = 'system/Associates';

	/**
	 * @ORM\Column(name="prefered", type="boolean", nullable=false, unique=false)
	 * @Annot\UniqueField(value=true, onError="force", min=1, max=1, altValue=false)
	 */
	protected $prefered;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $subtitle;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show=true, update=true, order=4, options={"by_reference"=false, "required"=false}, contexts={"simple"})
	 * @CRUDS\Update(show=true, update=true, order=4, options={"by_reference"=false, "required"=false}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $resume;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show=true, update=true, order=4, options={"by_reference"=false, "required"=false}, contexts={"simple"})
	 * @CRUDS\Update(show=true, update=true, order=4, options={"by_reference"=false, "required"=false}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $fulltext;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show=false, update=false, contexts={"simple"})
	 * @CRUDS\Update(show=false, update=false, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $functionality;

	/**
	 * @var string
	 * @Annot\Translatable
	 */
	protected $menutitle;

	/**
	 * @var string
	 * @ORM\Column(name="ceocodes", type="json_array", nullable=true, unique=false)
	 */
	protected $ceocodes;
	protected $ceocodesnames;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Favicon", cascade={"persist","remove"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsFavicon", "description"="Favicon : icône apparaissant dans l'onglet du navigateur."},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsFavicon", "description"="Favicon : icône apparaissant dans l'onglet du navigateur."},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $favicon;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		label="QR-Code",
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPlanparc", "description"="QR-Code"},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		label="QR-Code",
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPlanparc", "description"="QR-Code"},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $planparc;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		label="Photo secondaire",
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPlangrotte", "description"="Photo secondaire"},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		label="Photo secondaire",
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPlangrotte", "description"="Photo secondaire"},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $plangrotte;

	/**
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 	)
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $picone;

	/**
	 * @var string
	 * @ORM\Column(name="title", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		type="TextType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=30,
	 * 		options={"by_reference"=false, "required"=true, "description"="Nom commercial, dénomination juridique : <span class='text-danger'>IMPORTANT</span>"},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="TextType",
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=30,
	 * 		options={"by_reference"=false, "required"=true, "description"="Nom commercial, dénomination juridique : <span class='text-danger'>IMPORTANT</span>"},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $title;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @ORM\Column(name="accroche", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		type="TextType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=30,
	 * 		options={"by_reference"=false, "required"=false},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="TextType",
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=30,
	 * 		options={"by_reference"=false, "required"=false},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $accroche;

	/**
	 * @var string
	 * @ORM\Column(name="description", type="text", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		type="SummernoteType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=30,
	 * 		options={"by_reference"=false, "required"=false},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="SummernoteType",
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=30,
	 * 		options={"by_reference"=false, "required"=false},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $description;

	public function __construct() {
		parent::__construct();
		$this->prefered = false;
		$this->subtitle = null;
		$this->accroche = null;
		$this->initCeocodes();
		$this->favicon = null;
		$this->planparc = null;
		$this->plangrotte = null;
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getSlug();
	}


	/**
	 * Get title
	 * @param string $title = null
	 * @return self
	 */
	public function setTitle($title = null) {
		$this->title = $title;
		return $this;
	}


	/**
	 * Get menutitle
	 * @param string $menutitle = null
	 * @return self
	 */
	public function setMenutitle($menutitle = null) {
		$this->menutitle = $menutitle;
		return $this;
	}


	/**
	 * Set description
	 * @param string $description = null
	 * @return self
	 */
	public function setDescription($description = null) {
		$this->description = $description;
		return $this;
	}

	/**
	 * @ORM\PostLoad()
	 */
	public function getCeocodesnames() {
		$this->ceocodesnames = array(
			'googlecode',
			'fbcode',
		);
		return $this->ceocodesnames;
	}

	public function ceocodesnameExists($ceocodename) {
		return array_key_exists($ceocodename, $this->ceocodes);
		// return in_array($ceocodename, $this->ceocodesnames);
	}

	public function initCeocodes() {
		$this->ceocodes = [];
		foreach ($this->getCeocodesnames() as $codename) {
			$this->ceocodes[$codename] = null;
		}
		return $this;
	}

	public function setCeocodes($codes) {
		foreach ($codes as $codename => $code) $this->addCeocode($codename, $code);
		return $this;
	}

	public function getCeocodes() {
		return $this->ceocodes;
	}

	public function getCeocode($media) {
		return isset($this->ceocodes[$media]) ? $this->ceocodes[$media] : null;
	}

	public function addCeocode($codename, $code) {
		if($this->ceocodesnameExists($codename)) $this->ceocodes[$codename] = trim($code);
		return $this;
	}



	/**
	 * Get prefered
	 * @return boolean
	 */
	public function getPrefered() {
		return $this->prefered;
	}

	/**
	 * Is prefered
	 * @return boolean
	 */
	public function isPrefered() {
		return $this->getPrefered();
	}

	/**
	 * Set prefered
	 * @param boolean $prefered = true
	 * @return Entreprise
	 */
	public function setPrefered($prefered = true) {
		$this->prefered = $prefered;
		return $this;
	}

	/**
	 * Get subtitle
	 * @return string|null
	 */
	public function getSubtitle() {
		return $this->subtitle;
	}

	/**
	 * Set subtitle
	 * @param string $subtitle = null
	 * @return Entreprise
	 */
	public function setSubtitle($subtitle = null) {
		$this->subtitle = $subtitle;
		return $this;
	}

	/**
	 * Get accroche
	 * @return string
	 */
	public function getAccroche() {
		return $this->accroche;
	}

	/**
	 * Set accroche
	 * @param string $accroche = null
	 * @return Entreprise
	 */
	public function setAccroche($accroche = null) {
		$this->accroche = $accroche;
		return $this;
	}



	/*************************************************************************************/
	/*** FAVICON
	/*************************************************************************************/

	/**
	 * Set URL as favicon
	 * @param string $url
	 * @return Entreprise
	 */
	public function setUrlAsFavicon($url) {
		$favicon = new Favicon();
		$favicon->setUploadFile($url);
		$this->setFavicon($favicon);
		return $this;
	}

	/**
	 * Set resource as favicon
	 * @param string $resource
	 * @return Tier
	 */
	public function setResourceAsFavicon(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$favicon = new Favicon();
			$favicon->setUploadFile($resource);
			$this->setFavicon($favicon);
		}
		return $this;
	}

	public function getResourceAsFavicon() {
		return null;
	}

	/**
	 * Set favicon
	 * @param Favicon $favicon = null
	 * @return Entreprise
	 */
	public function setFavicon(Favicon $favicon = null) {
		$this->favicon = $favicon;
		return $this;
	}

	/**
	 * Get favicon
	 * @return Favicon | null
	 */
	public function getFavicon() {
		return $this->favicon;
	}



	/*************************************************************************************/
	/*** PLANPARC
	/*************************************************************************************/

	/**
	 * Set URL as planparc
	 * @param string $url
	 * @return Entreprise
	 */
	public function setUrlAsPlanparc($url) {
		$planparc = new Photo();
		$planparc->setUploadFile($url);
		$this->setPlanparc($planparc);
		return $this;
	}

	/**
	 * Set resource as planparc
	 * @param string $resource
	 * @return Tier
	 */
	public function setResourceAsPlanparc(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$planparc = new Photo();
			$planparc->setUploadFile($resource);
			$this->setPlanparc($planparc);
		}
		return $this;
	}

	public function getResourceAsPlanparc() {
		return null;
	}

	/**
	 * Set planparc
	 * @param Photo $planparc = null
	 * @return Entreprise
	 */
	public function setPlanparc(Photo $planparc = null) {
		$this->planparc = $planparc;
		return $this;
	}

	/**
	 * Get planparc
	 * @return Photo | null
	 */
	public function getPlanparc() {
		return $this->planparc;
	}



	/*************************************************************************************/
	/*** PLANGROTTE
	/*************************************************************************************/

	/**
	 * Set URL as plangrotte
	 * @param string $url
	 * @return Entreprise
	 */
	public function setUrlAsPlangrotte($url) {
		$plangrotte = new Photo();
		$plangrotte->setUploadFile($url);
		$this->setPlangrotte($plangrotte);
		return $this;
	}

	/**
	 * Set resource as plangrotte
	 * @param string $resource
	 * @return Tier
	 */
	public function setResourceAsPlangrotte(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$plangrotte = new Photo();
			$plangrotte->setUploadFile($resource);
			$this->setPlangrotte($plangrotte);
		}
		return $this;
	}

	public function getResourceAsPlangrotte() {
		return null;
	}

	/**
	 * Set plangrotte
	 * @param Photo $plangrotte = null
	 * @return Entreprise
	 */
	public function setPlangrotte(Photo $plangrotte = null) {
		$this->plangrotte = $plangrotte;
		return $this;
	}

	/**
	 * Get plangrotte
	 * @return Photo | null
	 */
	public function getPlangrotte() {
		return $this->plangrotte;
	}





	// /**
	//  * Set planparc
	//  * @param Image $planparc = null
	//  * @return Entreprise
	//  */
	// public function setPlanparc(Image $planparc = null) {
	// 	$this->planparc = $planparc;
	// 	return $this;
	// }

	// /**
	//  * Get planparc
	//  * @return Image | null
	//  */
	// public function getPlanparc() {
	// 	return $this->planparc;
	// }

	// /**
	//  * Set plangrotte
	//  * @param Image $plangrotte = null
	//  * @return Entreprise
	//  */
	// public function setPlangrotte(Image $plangrotte = null) {
	// 	$this->plangrotte = $plangrotte;
	// 	return $this;
	// }

	// /**
	//  * Get plangrotte
	//  * @return Image | null
	//  */
	// public function getPlangrotte() {
	// 	return $this->plangrotte;
	// }


}




