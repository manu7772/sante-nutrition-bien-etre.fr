<?php
namespace ModelApi\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class BlackString extends Constraint {

	public $message = 'Cette valeur {{ value }} pour {{ field }} est invalide. Vous ne pouvez renseigner que des lettres, chiffres et espaces!';
	public $fields = array();

	public function getRequiredOptions() {
		return array('fields');
	}

	public function validatedBy() {
		return static::class.'Validator';
	}

	public function getDefaultOption() {
		return 'fields';
	}

	public function getTargets() {
		return self::CLASS_CONSTRAINT;
	}

}