<?php
namespace ModelApi\UserBundle\Validator\Constraints;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Doctrine\Common\Inflector\Inflector;
// UserBundle
use ModelApi\UserBundle\Validator\Constraints\BlackString;

class BlackStringValidator extends ConstraintValidator {

	const STRING_TYPES = [
		0 => 'standard',
		1 => 'username',
		2 => 'name',
	];

	protected $container;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		return $this;
	}

	public function validate($entity, Constraint $constraint) {

		if (!($constraint instanceof BlackString)) {
			throw new UnexpectedTypeException($constraint, BlackString::class);
		}

		$fields = (array) $constraint->fields;

		if (0 === \count($fields)) {
			throw new ConstraintDefinitionException('At least one field has to be specified.');
		}

		if (null === $entity) return;

		foreach ($fields as $field => $type) {
			$message = $constraint->message;
			if(is_integer($field)) {
				$field = $type;
				$type = static::STRING_TYPES[0];
			}
			$getter = Inflector::camelize('get_'.$field);
			if(method_exists($entity, $getter)) {
				$errors = 0;
				$value = $entity->$getter();
				if(!empty($value)) {
					// Tests
					switch ($type) {
						case static::STRING_TYPES[1]:
							# username
							if(!preg_match('/^[\\w\\d\\s-]+$/', $value)) $errors++;
							$message = 'Cette valeur {{ value }} pour {{ field }} est invalide. Vous ne pouvez renseigner que des lettres sans accent, chiffres et espaces!';
							break;
						case static::STRING_TYPES[2]:
							# name
							if(!preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,\\.-]+$/u', $value)) $errors++;
							$message = 'Cette valeur {{ value }} pour {{ field }} est invalide. Vous ne pouvez renseigner que des lettres!';
							break;
						default:
							# standard
							if(!preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,\\.-]+$/u', $value)) $errors++;
							$message = 'Cette valeur {{ value }} pour {{ field }} est invalide. Vous ne pouvez renseigner que des lettres!';
							break;
					}
					// Tests for all
					if(preg_match('/(https?)/', $value)) {
						$errors++;
						$message .= ' En outre, les URL sont interdits!';
					}

					if($errors > 0) {
						$this->context->buildViolation($message)
							// ->atPath($errorPath)
							->setParameter('{{ field }}', $field)
							->setParameter('{{ value }}', $value)
							// ->setInvalidValue($invalidValue)
							// ->setCode(UniqueEntity::NOT_UNIQUE_ERROR)
							// ->setCause($result)
							->addViolation();
					}
				}
			}
		}

		// custom constraints should ignore null and empty values to allow
		// other constraints (NotBlank, NotNull, etc.) take care of that
		// if (null === $value || '' === $value) {
			// return;
		// }

		// if (!is_string($value)) {
		// 	throw new UnexpectedTypeException($value, 'string');
		// }

		# if (!preg_match('/^[a-zA-Z0-9]+$/', $value, $matches)) {
			// $this->context->buildViolation($constraint->message)
			// 	->setParameter('{{ string }}', $value)
			// 	->addViolation();
		// }
	}


}
