<?php
namespace ModelApi\UserBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
// use Symfony\Component\HttpKernel\Event\GetResponseEvent;
// use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
// use Symfony\Component\HttpFoundation\Request;
// AnnotBundle
// use ModelApi\AnnotBundle\Annotation as Annot;
// FileBundle
// use ModelApi\FileBundle\Entity\Item;
// use ModelApi\FileBundle\Entity\File;
// use ModelApi\FileBundle\Entity\Directory;
// use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\FileManager;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// use ModelApi\UserBundle\Service\serviceTier;
// UserBundle
use ModelApi\UserBundle\Entity\Group;
// use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Service\serviceUser;

// https://symfony.com/doc/3.4/components/filesystem.html
// use Symfony\Component\Filesystem\Filesystem;
// use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use \Exception;

class GroupListener {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	protected $serviceGroup;
	// protected $serviceTier;
	// protected $FileManager;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceGroup = $this->container->get(Group::ENTITY_SERVICE);
		// $this->serviceTier = $this->container->get(Tier::ENTITY_SERVICE);
		// $this->FileManager = $this->container->get(FileManager::class);
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 */
	public function PrePersist(Group $group, LifecycleEventArgs $event) {
		// *** Creator must be in this new Group
		$user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
		if($user instanceOf User) $user->addGroup($group);
		$this->serviceGroup->defineRole($group);
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function PreUpdate(Group $group, LifecycleEventArgs $event) {
		$this->serviceGroup->defineRole($group);
		return $this;
	}






}