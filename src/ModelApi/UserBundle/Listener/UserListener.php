<?php
namespace ModelApi\UserBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
// use Symfony\Component\Security\Core\Event\AuthenticationEvent;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceUserrequest;
// UserBundle
// use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Service\serviceGrants;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\YamlLog;

use \Exception;

class UserListener {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	protected $serviceUser;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceUser = $this->container->get(User::ENTITY_SERVICE);
		return $this;
	}

	// /**
	//  * @param AuthenticationEvent $event
	//  */
	// public function onLoginSuccessSessionUserCookies(AuthenticationEvent $event) {
	// 	$session = $event->getRequest()->getSession();
	// 	// var_dump($event->getAuthenticationToken());die();
	// 	YamlLog::directYamlLogfile(['User' => 'logged!']);
	// 	return $this;
	// }

	/**
	 * @ORM\PrePersist()
	 */
	public function PrePersist(User $user, LifecycleEventArgs $event) {
		$this->serviceUser->setTemppassword($user, null);
		$serviceUserrequest = $this->container->get(serviceUserrequest::class);
		if($user->isOnOrderRequestCollab() && $serviceUserrequest->isPossibleRequestCreate(serviceUserrequest::ROLECOLLAB_NAME, $user)) {
			$userrequest = $serviceUserrequest->namedRequest_create(serviceUserrequest::ROLECOLLAB_NAME, $user, $this->container->get(serviceUser::class)->getCurrentUserOrNull());
		}
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function PreUpdate(User $user, LifecycleEventArgs $event) {
		// $this->serviceUser->setTemppassword($user, null);
		$this->serviceUser->setTemppassword($user, null);
		// if($tier->getId() === 405) die(__METHOD__.' > '.$tier->getMaingroup()->getName().' / '.$tier->getMaingroup()->getLongname());
		$serviceUserrequest = $this->container->get(serviceUserrequest::class);
		if($user->isOnOrderRequestCollab() && $serviceUserrequest->isPossibleRequestCreate(serviceUserrequest::ROLECOLLAB_NAME, $user)) {
			$userrequest = $serviceUserrequest->namedRequest_create(serviceUserrequest::ROLECOLLAB_NAME, $user, $this->container->get(serviceUser::class)->getCurrentUserOrNull());
		}
		return $this;
	}


}