<?php
namespace ModelApi\UserBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
// use Symfony\Component\HttpKernel\Event\GetResponseEvent;
// use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
// use Symfony\Component\HttpFoundation\Request;
// AnnotBundle
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
// use ModelApi\FileBundle\Entity\Item;
// use ModelApi\FileBundle\Entity\File;
// use ModelApi\FileBundle\Entity\Directory;
// use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\FileManager;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// use ModelApi\UserBundle\Service\serviceTier;
// UserBundle
use ModelApi\UserBundle\Entity\Group;
// use ModelApi\UserBundle\Entity\User;
// use ModelApi\UserBundle\Service\serviceUser;

// https://symfony.com/doc/3.4/components/filesystem.html
// use Symfony\Component\Filesystem\Filesystem;
// use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use ModelApi\DevprintsBundle\Service\DevTerminal;

use \Exception;

class TierListener {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	// protected $serviceGroup;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		// $this->serviceGroup = $this->container->get(Group::ENTITY_SERVICE);
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 */
	public function PrePersist(Tier $tier, LifecycleEventArgs $event) {
		$this->container->get(Group::ENTITY_SERVICE)->checkMaingroup($tier);
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function PreUpdate(Tier $tier, LifecycleEventArgs $event) {
		$em = $event->getEntityManager();
		$uow = $em->getUnitOfWork();
		$changeset = $uow->getEntityChangeSet($tier);
		if(isset($changeset['name']) || isset($changeset['username'])) {
			$this->container->get(Group::ENTITY_SERVICE)->checkMaingroup($tier);
		}
		return $this;
	}





}