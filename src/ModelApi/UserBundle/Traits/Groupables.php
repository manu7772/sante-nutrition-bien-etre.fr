<?php
namespace ModelApi\UserBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;

Trait Groupables {


	/**
	 * All Groups list
	 * @ORM\ManyToMany(targetEntity="ModelApi\UserBundle\Entity\Group")
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		options={"multiple"=true, "required"=false, "group_by"="groupedName", "class"="ModelApi\UserBundle\Entity\Group", "query_builder"="qb_findTierAllGroups"},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		options={"multiple"=true, "required"=false, "group_by"="groupedName", "class"="ModelApi\UserBundle\Entity\Group", "query_builder"="qb_findTierAllGroups"},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=1000)
	 */
	protected $groups;


	/**
	 * @Annot\PostCreate()
	 */
	public function init_Groupables() {
		$this->groups = new ArrayCollection();
		return $this;
	}


	/*************************************************************************************/
	/*** ROLES
	/*************************************************************************************/

	/**
	 * {@inheritdoc}
	 * @param string $role
	 * @return User
	 */
	public function addRole($role) {
		$role = strtoupper($role);
		if ($role === User::ROLE_DEFAULT) return $this;
		$this->roles = array_unique(array_merge($this->roles, array($role)));
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRoles() {
		$roles = $this->roles ?? [];
		// we need to make sure to have at least one role
		$roles[] = User::ROLE_DEFAULT;
		foreach ($this->getAllGroups() as $group) {
			$roles = array_merge($roles, $group->getRoles());
		}
		return array_unique($roles);
	}

	public function getHumanizedRoles() {
		$roles = array_flip($this->getRoles());
		$roles_choices = array_flip($this->getRolesdefaultList(true));
		foreach ($roles as $role => $key) {
			$roles[$role] = $roles_choices[$role];
		}
		return $roles;
	}

	public function getHigherRole($humanized = false) {
		$roles_choices = array_flip($this->getRolesdefaultList(true));
		$roles_values = array_keys($roles_choices);
		$roles = array_flip($roles_values);
		$high = 0;
		// echo('<pre>'); var_dump($this->getRoles()); die('</pre>');
		foreach ($this->getRoles() as $role) {
			if($roles[$role] >= $high) $high = $roles[$role];
		}
		$role = $roles_values[$high];
		return $humanized ? $roles_choices[$role] : $role;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setRoles(array $roles) {
		$this->roles = array();
		foreach ($roles as $role) $this->addRole($role);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function removeRole($role) {
		$role = strtoupper($role);
		$this->roles = array_filter($this->getRoles(), function($oneRole) use ($role) {
			return $oneRole !== $role;
		});
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function hasRole($role) {
		return in_array(strtoupper($role), $this->getRoles(), true);
	}

	/**
	 * Get roles
	 * @return array <string>
	 */
	public function getGroupsRoles() {
		$groupsRoles = array();
		foreach ($this->getGroups() as $group) {
			$groupsRoles = array_merge($groupsRoles, $group->getRoles());
		}
		return array_unique($groupsRoles);
	}

	/**
	 * Get roles
	 * @return array <string>
	 */
	public function getAllGroupsRoles() {
		$groupsRoles = array();
		foreach ($this->getAllGroups() as $group) {
			$groupsRoles = array_merge($groupsRoles, $group->getRoles());
		}
		return array_unique($groupsRoles);
	}

	public function getRolesChoices($all = false) {
		$serviceGrants = $this->serviceCruds->getServiceGrants();
		if(!$all && !$serviceGrants->isGranted(static::ROLE_ADMIN)) return [];
		//if(!$serviceGrants->isGranted($this->getHigherRole(false))) return [];
		return $this->getRolesdefaultList();
		/*$list = [
			'Utilisateur' => static::ROLE_USER,
			'Testeur' => static::ROLE_TESTER,
			'Traducteur' => static::ROLE_TRANSLATOR,
			'Editeur' => static::ROLE_EDITOR,
			'Collaborateur' => static::ROLE_COLLAB,
			'Administrateur' => static::ROLE_ADMIN,
		];
		if($all || $serviceGrants->isGranted(static::ROLE_SUPER_ADMIN)) $list['Super administrateur'] = static::ROLE_SUPER_ADMIN;
		return $list;*/
	}

	public function getRolesdefaultList($all = false) {
		$list = [
			'Utilisateur' => static::ROLE_USER,
			'Testeur' => static::ROLE_TESTER,
			'Traducteur' => static::ROLE_TRANSLATOR,
			'Editeur' => static::ROLE_EDITOR,
			'Collaborateur' => static::ROLE_COLLAB,
			'Administrateur' => static::ROLE_ADMIN,
		];
		if($all || $this->serviceCruds->getServiceGrants()->isGranted(static::ROLE_SUPER_ADMIN)) $list['Super administrateur'] = static::ROLE_SUPER_ADMIN;
		return $list;
	}


	/*************************************************************************************/
	/*** GROUPS
	/*************************************************************************************/

	/**
	 * Gets the groups granted to the Item
	 * @return \Traversable
	 */
	public function getGroups() {
		return $this->groups;
	}

	/**
	 * Gets the name of the groups which includes the Item
	 * @return array
	 */
	public function getGroupNames($longname = false) {
		$names = array();
		foreach ($this->getGroups() as $group) $names[] = $longname ? $group->getLongname() : $group->getName();
		return $names;
	}

	/**
	 * Gets the groups granted to the item.
	 * @return ArrayCollection <Group>
	 */
	public function getAllGroups() {
		$allGroups = clone $this->getGroups();
		foreach ($this->getGroups() as $group) {
			foreach ($group->getAllChilds() as $childgroup) {
				if(!$allGroups->contains($childgroup)) $allGroups->add($childgroup);
			}
		}
		return $allGroups;
	}

	/**
	 * Gets the name of the groups which includes the Item
	 * @return array
	 */
	public function getAllGroupNames($longname = false) {
		$names = array();
		foreach ($this->getAllGroups() as $group) $names[] = $longname ? $group->getLongname() : $group->getName();
		return $names;
	}

	/**
	 * Gets the groups granted to the item.
	 * @return ArrayCollection <Group>
	 */
	public function getAllParentGroups() {
		$allGroups = clone $this->getGroups();
		foreach ($this->getGroups() as $group) {
			foreach ($group->getAllParents() as $childgroup) {
				if(!$allGroups->contains($childgroup)) $allGroups->add($childgroup);
			}
		}
		return $allGroups;
	}

	/**
	 * Gets the name of the groups which includes the Item
	 * @return array
	 */
	public function getAllParentGroupNames($longname = false) {
		$names = array();
		foreach ($this->getAllParentGroups() as $group) $names[] = $longname ? $group->getLongname() : $group->getName();
		return $names;
	}


	public function setGroups($groups) {
		foreach ($this->groups as $group) {
			if(!$groups->contains($group)) $this->removeGroup($group);
		}
		foreach ($groups as $group) $this->addGroup($group);
		return $this;
	}

	/**
	 * Indicates whether the user belongs to the specified group or not.
	 * @param Group $group = null
	 * @param boolean $recursive = true
	 * @return bool
	 */
	public function hasGroup(Group $group = null, $recursive = true) {
		if($this->getGroups()->isEmpty()) return false;
		// return in_array($name, $this->getGroupNames($recursive));
		$groups = $recursive ? $this->getAllGroups() : $this->getGroups();
		return $group instanceOf Group ? $groups->contains($group) : !$groups->isEmpty();
	}

	/**
	 * Add a group to the user groups.
	 * @param Group $group
	 * @return self
	 */
	public function addGroup(Group $group) {
		if (!$this->getGroups()->contains($group)) $this->groups->add($group);
		// if($inverse) $group->addItem($this, false);
		return $this;
	}

	/**
	 * Remove a group from the user groups.
	 * @param Group $group
	 * @return self
	 */
	public function removeGroup(Group $group) {
		$this->groups->removeElement($group);
		// if($inverse) $group->removeItem($this, false);
		return $this;
	}


	/*************************************************************************************/
	/*** CHOSENGROUPS
	/*************************************************************************************/

	// /**
	//  * @ORM\PostLoad()
	//  * Memorize list of chosengroups, as json string
	//  * @return self
	//  */
	// protected function memoriseChosengroups() {
	// 	if($this->isSerializedChosengroups()) {
	// 		ksort($this->chosengroups);
	// 		$this->memoChosengroups = json_encode($this->chosengroups);
	// 	}
	// 	return $this;
	// }

	// protected function hasChangedChosengroups() {
	// 	$memoChosengroups = $this->memoChosengroups;
	// 	$this->memoriseChosengroups();
	// 	return $memoChosengroups != $this->memoChosengroups;
	// }

	// /**
	//  * @ORM\PrePersist()
	//  * @ORM\PreUpdate()
	//  * serliaze before persist/update
	//  * @return self
	//  */
	// public function preSerializeChosengroups() {
	// 	$this->memserialized = $this->isSerializedChosengroups();
	// 	if(!$this->memserialized) $this->serializeChosengroups();
	// 	if($this->hasChangedChosengroups()) $this->computeGroups();
	// 	return $this;
	// }

	// /**
	//  * @ORM\PostPersist()
	//  * @ORM\PostUpdate()
	//  * unserliaze after persist/update --> only if was unserialized before persist
	//  * @return self
	//  */
	// public function postSerializeChosengroups() {
	// 	// $this->memoriseChosengroups();
	// 	if(!$this->memserialized) $this->unserializeChosengroups();
	// }


	// public function isSerializedChosengroups() {
	// 	return !($this->chosengroups instanceOf Collection);
	// }

	// public function unserializeChosengroups() {
	// 	$chosengroups = new ArrayCollection();
	// 	if($this->isSerializedChosengroups()) {
	// 		foreach ($this->groups as $group) {
	// 			if(array_key_exists($group->getId(), $this->chosengroups) && !$chosengroups->contains($group)) $chosengroups->add($group);
	// 		}
	// 	}
	// 	if($this->isDeepControl()) {
	// 		if(count($this->chosengroups) !== $chosengroups->count()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): while checking before and after unserializing!", 1);			
	// 	}
	// 	$this->chosengroups = $chosengroups;
	// 	return $this;
	// }

	// public function serializeChosengroups() {
	// 	if(!$this->isSerializedChosengroups()) {
	// 		$chosengroups = [];
	// 		foreach ($this->chosengroups as $chosengroup) $chosengroups[$chosengroup->getId()] = $chosengroup->getName();
	// 		unset($this->chosengroups);
	// 		$this->chosengroups = $chosengroups;
	// 	}
	// 	return $this;
	// }

	// /**
	//  * Gets the computed groups granted to the Item
	//  * @return \Traversable
	//  */
	// public function getChosengroups($unserliazed = false) {
	// 	if($unserliazed) $this->unserializeChosengroups();
	// 		else $this->serializeChosengroups();
	// 	return $this->chosengroups;
	// }


	// public function setChosengroups($chosengroups) {
	// 	// foreach ($this->chosengroups as $chosengroup) {
	// 	// 	if(!$chosengroups->contains($chosengroup)) $this->removeChosengroup($chosengroup);
	// 	// }
	// 	// foreach ($chosengroups as $chosengroup) $this->addChosengroup($chosengroup);
	// 	unset($this->chosengroups);
	// 	if($this->isSerializedChosengroups()) {
	// 		$this->chosengroups = [];
	// 		foreach ($chosengroups as $chosengroup) $this->chosengroups[$chosengroup->getId()] = $chosengroup->getName();
	// 	} else {
	// 		$this->chosengroups = $chosengroups instanceOf ArrayCollection ? $chosengroups : new ArrayCollection($chosengroups);
	// 	}
	// 	return $this;
	// }

	// // public function addChosengroup(Group $chosengroup) {
	// // 	if(!$this->chosengroups->contains($chosengroup)) $this->chosengroups->add($chosengroup);
	// // 	return $this;
	// // }

	// // public function removeChosengroup(Group $chosengroup) {
	// // 	$this->chosengroups->removeElement($chosengroup);
	// // 	return $this;
	// // }


	// public function computeGroups() {
	// 	$new_groups = $this->getAllGroups();
	// 	// $new_groups = new ArrayCollection();
	// 	// foreach ($this->getGroups() as $group) {
	// 	// 	if(!$new_groups->contains($group)) $new_groups->add($group);
	// 	// }
	// 	foreach ($this->getParents(true) as $parent) {
	// 		foreach ($parent->getAllGroups() as $parent_group) {
	// 			if(!$new_groups->contains($parent_group)) $new_groups->add($parent_group);
	// 		}
	// 	}
	// 	return $this->setGroups($new_groups);
	// }

	// public function cleanupChosengroups() {

	// }






}