<?php
namespace ModelApi\UserBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceItem;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;

use ModelApi\DevprintsBundle\Service\DevTerminal;

// use \DateTime;
use \ReflectionClass;

class serviceTier extends serviceItem {

	const ENTITY_CLASS = Tier::class;

	protected $container;
	protected $serviceEntities;

	// public function __construct(ContainerInterface $container) {
	// 	parent::__construct($container);
	// 	return $this;
	// }


	/**
	 * Get Tier's Directorys by pathname
	 * @param string $pathname
	 * @param Tier $tier = null
	 * @param boolean $addChilds = false
	 * @param boolean $isRegexp = false
	 * @return Array <Directory>
	 */
	public function getTierDirectorys($pathname, Tier $tier, $addChilds = false, $isRegexp = false) {
		return $this->container->get(FileManager::class)->getEntityDirectorys($pathname, $tier, $addChilds, $isRegexp);
	}

	/**
	 * Get Tier's Directory by pathname
	 * @param string $pathname
	 * @param Tier $tier = null
	 * @param boolean $addChilds = false
	 * @param boolean $isRegexp = false
	 * @return Directory | null
	 */
	public function getTierDirectory($pathname, Tier $tier, $addChilds = false, $isRegexp = false) {
		$directorys = $this->getTierDirectorys($pathname, $tier, $addChilds, $isRegexp);
		return count($directorys) ? reset($directorys) : null;
	}




}