<?php
namespace ModelApi\UserBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\Role\Role;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// UserBundle
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Service\serviceGroup;
use ModelApi\UserBundle\Service\serviceRoles;
use ModelApi\UserBundle\Entity\UserInterface;
use ModelApi\UserBundle\Entity\GroupableInterface;

// use \DateTime;
use \ReflectionClass;

/**
 * @see https://symfony.com/doc/3.4/components/security.html
 * @see https://symfony.com/doc/3.4/components/security/authorization.html
 * @see https://github.com/symfony/symfony/blob/3.4/src/Symfony/Component/Security/Core/Role/RoleHierarchy.php
 */
class serviceGrants implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;


	protected $container;
	protected $roleHierarchy;
	protected $AuthorizationChecker;
	protected $user;
	// protected $serviceUser;
	// protected $serviceGroup;
	// protected $serviceRoles;
	protected $states_roles;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		// Symfony\Component\Security\Core\Authorization\AuthorizationChecker
		// https://github.com/symfony/symfony/blob/3.4/src/Symfony/Component/Security/Core/Authorization/AuthorizationChecker.php
		$this->AuthorizationChecker = $this->container->get('security.authorization_checker');
		// Symfony\Component\Security\Core\Role\RoleHierarchy
		// https://github.com/symfony/symfony/blob/3.4/src/Symfony/Component/Security/Core/Role/RoleHierarchy.php
		$this->roleHierarchy = $this->container->get('security.role_hierarchy');

		// $this->rolesParams = $this->container->getParameter('security.role_hierarchy.roles');
		// foreach ($this->rolesParams as $role => $desc) {
		// 	$reachableRoles = $this->getReachableRoles(new Role($role));
		// 	echo('<pre><h3>ROLE '.strtoupper($role).'</h3>'); var_dump(array_values($reachableRoles)); echo('</pre>');
		// }
		// die();

		$this->user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
		// $this->serviceUser = $this->container->get(serviceUser::class);
		// $this->serviceGroup = $this->container->get(serviceGroup::class);
		// $this->serviceRoles = $this->container->get(serviceRoles::class);
		$this->states_roles = $this->container->getParameter('roles');
		return $this;
	}

	/**
	 * Get all valid Roles by roles
	 * @param array|string $roles
	 * @return array
	 */
	public function getReachableRoles($roles) {
		// foreach ($roles as $key => $role) if(is_string($role)) $roles[$key] = new Role($role);
		$roles = array_map(function($value) {
			if(is_string($value)) $value = new Role($value);
			if(!($value instanceOf Role)) throw new Exception("Error ".__METHOD__."(): roles can only be strings or Role objects!", 1);
			return $value;
		}, (array)$roles);
		$reachableRoles = $this->roleHierarchy->getReachableRoles($roles);
		return array_map(function($role) { return $role->getRole(); }, $reachableRoles);
	}

	/**
	 * Is granted User for roles
	 * @param mixed $roles
	 * @param User $user = null
	 * @return bool
	 */
	public function isGranted($roles, User $user = null) {
		if(empty($roles)) return false;
		foreach ((array)$roles as $role) {
			if(is_bool($role)) return $role;
		}
		if(!($user instanceOf User)) $user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
		if($user instanceOf User && $user->isSuperAdmin()) {
			return true;
		}
		$userRoles = $user instanceOf User ? $user->getRoles() : [UserInterface::IS_AUTHENTICATED_ANONYMOUSLY];
		$userRoles = $this->getReachableRoles((array)$userRoles);
		$result = count(array_intersect($userRoles, (array)$roles)) > 0;
		// echo('<div>'.__METHOD__.'(): Result for User '.(empty($user) ? 'NULL' : $user->getUsername()).' with roles '.json_encode($userRoles).'<br>--> and <span class="text-white">'.json_encode((array)$roles).'</span> > '.json_encode($result).'</div><br>');
		return $result;
	}

	/**
	 * isGranted Object
	 * @param Object $entity
	 * @param User $user = null
	 * @return bool
	 */
	public function isGrantedEntity($entity, User $user = null) {
		// if(!is_object($entity)) return true;
		if(!($user instanceOf User)) $user = $this->user;
		if($user instanceOf User && $user->isSuperAdmin()) return true;
		// system dirinterfaces
		if($entity instanceOf Directory && $entity->isRootSystem()) {
			if(!($user instanceOf GroupableInterface)) return false;
			if(!$this->isGranted($this->states_roles['system_directory'], $user)) return false;
		}
		// softdeleted
		if(method_exists($entity, 'isSoftdeleted') && $entity->isSoftdeleted()) {
			return $this->isGranted($this->states_roles['state_softdeleted'], $user);
		}
		// if User is owner
		// echo('<div>Test granted: '.$entity.'…</div>');
		if($user instanceOf User && method_exists($entity, 'getOwner') && $entity->getOwner() === $user) {
			return true;
		}
		if($user instanceOf User && $user->getAssociates()->contains($entity)) {
			return true;
		}
		// groups
		if($user instanceOf User && $entity instanceOf GroupableInterface) {
			$roles = $entity instanceOf User ? $entity->getHigherRole() : $entity->getRoles();
			return $this->isGranted($roles, $user);
		}
		// disabled
		if(method_exists($entity, 'isDisabled') && $entity->isDisabled()) {
			if(!$this->isGranted($this->states_roles['state_enabled'], $user)) return false;
		}
		if(method_exists($entity, 'getRoles') && $user instanceOf User) {
			return $this->isGranted($entity->getRoles(), $user);
		}
		return true;
	}

	/**
	 * Filter an array of entities regarding to $user, and returning new array of permitted entities for this $user
	 * @param array|Collection|object $entities
	 * @param User $user = null
	 * @param integer $max_depth = 1
	 * @return bool
	 */
	public function filterEntitiesForUser($entities, User $user = null, $max_depth = 1) {
		$entType = 'Array';
		if(!($user instanceOf User)) $user = $this->user;
		if($user instanceOf User && $user->isSuperAdmin()) return $entities;
		if($entities instanceOf Collection) {
			$entities = $entities->toArray();
			$entType = 'ArrayCollection';
		}
		if(!is_array($entities)) {
			$entities = [$entities];
			$entType = 'Single';
		}
		$entities = array_filter($entities, function($entity) use ($user) {
			return $this->isGrantedEntity($entity, $user);
		});
		// if($max_depth > 0) {
		// 	foreach ($entities as $entity) {
		// 		// Get associations and filter related entities
		// 		$associations = $this->em->getClassMetadata(get_class($entity))->getAssociationMappings();
		// 		foreach ($associations as $association) {
		// 			# code...
		// 			$targetEntity = $association['targetEntity'];
		// 			// enabled / softdeleted
		// 			if(is_subclass_of($association['targetEntity'], BaseMain::class)) {

		// 			}

		// 			echo("<pre>");
		// 			echo("<h3>Entity #".$entity->getId()." ".$entity.": </h3>");
		// 			var_dump($association);
		// 			echo("</pre>");
		// 			// $childs = $this->filterEntitiesForUser($entity->$getter(), $user, $max_depth - 1);
		// 		}
		// 	}
		// }
		switch ($entType) {
			case 'ArrayCollection': return new ArrayCollection($entities); break;
			case 'Single': return empty($entities) ? null : reset($entities); break;
		}
		return array_values($entities);
	}






// <?php
// namespace ModelApi\UserBundle\Service;

// use Symfony\Component\DependencyInjection\ContainerInterface;
// // use Symfony\Component\HttpFoundation\RequestStack;
// use Doctrine\Common\Collections\ArrayCollection;
// use Doctrine\Common\Collections\Collection;

// use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
// use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
// use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
// use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
// use Symfony\Component\Security\Core\Role\RoleHierarchy;
// use Symfony\Component\Security\Core\Role\Role;

// // BaseBundle
// use ModelApi\BaseBundle\Service\serviceEntities;
// use ModelApi\BaseBundle\Service\servicesBaseInterface;
// use ModelApi\BaseBundle\Service\serviceKernel;
// use ModelApi\BaseBundle\Service\serviceModules;
// // UserBundle
// use ModelApi\UserBundle\Entity\Group;
// use ModelApi\UserBundle\Entity\User;
// use ModelApi\UserBundle\Service\serviceUser;
// use ModelApi\UserBundle\Service\serviceGroup;
// use ModelApi\UserBundle\Service\serviceRoles;
// use ModelApi\UserBundle\Entity\UserInterface;
// use ModelApi\UserBundle\Entity\GroupableInterface;

// use \Exception;
// // use \DateTime;
// // use \ReflectionClass;

// /**
//  * @see https://symfony.com/doc/3.4/components/security.html
//  * @see https://symfony.com/doc/3.4/components/security/authorization.html
//  * @see https://github.com/symfony/symfony/blob/3.4/src/Symfony/Component/Security/Core/Role/RoleHierarchy.php
//  * 
//  * @see https://numa-bord.com/miniblog/symfony-5-utiliser-la-fonction-isgranted-sur-nimporte-quel-objet-utilisateur/
//  */
// class serviceGrants implements servicesBaseInterface {

// 	use \ModelApi\BaseBundle\Service\baseService;


// 	protected $container;
// 	protected $serviceEntities;
// 	protected $roleHierarchy;
// 	protected $AuthorizationChecker;
// 	protected $accessDecisionManager;
// 	protected $serviceModules;
// 	protected $states_roles;
// 	protected $user;

// 	/** 
// 	 * Constructor
// 	 * @see https://github.com/symfony/symfony/blob/3.4/src/Symfony/Component/Security/Core/Role/RoleHierarchy.php
// 	 * @see https://github.com/symfony/symfony/blob/3.4/src/Symfony/Component/Security/Core/Authorization/AuthorizationChecker.php
// 	 */
// 	public function __construct(ContainerInterface $container, AccessDecisionManagerInterface $accessDecisionManager) {
// 		$this->container = $container;
// 		$this->accessDecisionManager = $accessDecisionManager;
// 		$this->serviceEntities = $this->container->get(serviceEntities::class);
// 		$this->AuthorizationChecker = $this->container->get('security.authorization_checker');
// 		$this->roleHierarchy = $this->container->get('security.role_hierarchy');
// 		// State roles
// 		$this->states_roles = $this->container->getParameter('roles');
// 		if(!isset($this->states_roles['state_enabled'])) $this->states_roles['state_enabled'] = UserInterface::ROLE_TRANSLATOR;
// 		if(!isset($this->states_roles['state_softdeleted'])) $this->states_roles['state_softdeleted'] = UserInterface::ROLE_SUPER_ADMIN;
// 		// Modules
// 		$this->serviceModules = $this->container->get(serviceModules::class);
// 		// User
// 		$this->getUser();
// 		return $this;
// 	}

// 	public function getUser() {
// 		return $this->user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
// 	}


// 	/***************************************************************************/
// 	/*** BUNDLE
// 	/***************************************************************************/

// 	// public function getCurrentBundle() {
// 	// 	return $this->container->get(serviceKernel::class)->getCurrentBundle();
// 	// }

// 	public function getBundleByName($bundleName = null) {
// 		return empty($bundleName) ?
// 			$this->container->get(serviceKernel::class)->getCurrentBundle():
// 			$this->container->get(serviceKernel::class)->getBundleByName($bundleName);
// 	}

// 	// public function isDistinctBundle() {
// 	// 	return $this->getServiceParameter('distincts.bundle', true, false, true);
// 	// }

// 	public function isDistinctBundle($bundleName = null) {
// 		if(!$this->getServiceParameter('distincts.bundle', true, false, true)) return false;
// 		$bundle = $this->getBundleByName($bundleName);
// 		if(empty($bundle)) return true;
// 		return method_exists($bundle, 'isPublicBundle') ? $bundle->isPublicBundle() : false;
// 	}

// 	/***************************************************************************/
// 	** BUNDLE
// 	*************************************************************************





// 	/**
// 	 * Get all valid Roles by roles
// 	 * @param array|string $roles
// 	 * @return array
	 
// 	public function getReachableRoles($roles) {
// 		// foreach ($roles as $key => $role) if(is_string($role)) $roles[$key] = new Role($role);
// 		$roles = array_map(function($value) {
// 			if(is_string($value)) $value = new Role($value);
// 			if(!($value instanceOf Role)) throw new Exception("Error ".__METHOD__."(): roles can only be strings or Role objects!", 1);
// 			return $value;
// 		}, (array)$roles);
// 		$reachableRoles = $this->roleHierarchy->getReachableRoles($roles);
// 		return array_map(function($role) { return $role->getRole(); }, $reachableRoles);
// 	}

// 	/**
// 	 * Is granted User for roles
// 	 * @param mixed $roles
// 	 * @param User $user = null
// 	 * @param Object $object = null
// 	 * @return bool
// 	 */
// 	public function isGranted($roles, User $user = null, $object = null) {
// 		if(empty($roles)) $roles = [];
// 		if(is_string($roles)) $roles = [$roles];
// 		foreach ($roles as $role) {
// 			if(is_bool($role)) return $role;
// 		}
//  		if(!($user instanceOf User)) $user = $this->user;
//  		if(!($user instanceOf User)) return false;
//  		$token = new UsernamePasswordToken($user, 'none', 'none', $user->getRoles());
// 		return $this->accessDecisionManager->decide($token, $roles, $object);
// 	}

// 	/**
// 	 * isGranted Object
// 	 * @param Object $entity
// 	 * @param User $user = null
// 	 * @param string $view_context = 'list'
// 	 * @param string $bundle_context = 'auto' (if "auto", then use current bundle)
// 	 * @return bool
// 	 */
// 	public function isGrantedEntity($entity, User $user = null, $view_context = 'list', $bundle_context = 'auto') {
// 		// if(!is_object($entity)) return true;
// 		if(!($user instanceOf User)) $user = $this->user;
// 		if($user instanceOf User) if($user->isSuperAdmin()) return true;
// 		// system dirinterfaces
// 		if($entity instanceOf Directory && $entity->isRootSystem()) {
// 			if(!($user instanceOf GroupableInterface)) return false;
// 			if(!$this->isGranted($this->states_roles['system_directory'], $user)) return false;
// 		}
// 		// softdeleted
// 		if(method_exists($entity, 'isSoftdeleted') && $entity->isSoftdeleted()) {
// 			if(!$this->isGranted($this->states_roles['state_softdeleted'], $user)) return false;
// 		}
// 		// if User is owner
// 		// echo('<div>Test granted: '.$entity.'…</div>');
// 		if($user instanceOf User && method_exists($entity, 'getOwner') && $entity->getOwner() === $user) {
// 			return true;
// 		}
// 		// groups
// 		if($user instanceOf GroupableInterface && $entity instanceOf GroupableInterface) {
// 			$E_roles = $entity->getGroupsRoles();
// 			$U_roles = $user->getGroupsRoles();
// 			$counts = count(array_intersect($E_roles, $U_roles));
// 			if($counts > 0) return true;
// 			// if($counts < 1) return false;
// 			// return $counts > 0;
// 		}
// 		// disabled
// 		if(method_exists($entity, 'isDisabled') && $entity->isDisabled()) {
// 			if(!$this->isGranted($this->states_roles['state_enabled'], $user)) return false;
// 		}
// 		if(method_exists($entity, 'getRoles')) {
// 			if(!$this->isGranted($entity->getRoles(), $user)) return false;
// 		}
// 		return true;
// 	}

// 	/**
// 	 * Filter an array of entities regarding to $user, and returning new array of permitted entities for this $user
// 	 * @param array|Collection|entity $entities
// 	 * @param User $user = null
// 	 * @param string $view_context = 'list'
// 	 * @param string $bundle_context = 'auto' (if "auto", then use current bundle)
// 	 * @return mixed
// 	 */
// 	public function filterEntitiesForUser($entities, User $user = null, $view_context = 'list', $bundle_context = 'auto') {
// 		if(!($user instanceOf User)) $user = $this->user;
// 		if($user instanceOf User && $user->isSuperAdmin()) return $entities;
// 		// Single entity
// 		if(is_object($entities) && $this->serviceEntities->entityExists($entities)) {
// 			return $this->isGrantedEntity($entity, $user, $view_context, $bundle_context);
// 		}
// 		// Collection of entities
// 		if($entities instanceOf Collection) {
// 			return $entities->filter(function($item) use ($user) {
// 				return $this->isGrantedEntity($entity, $user, $view_context, $bundle_context);
// 			});
// 		}
// 		// Array of entities
// 		if(is_array($entities)) {
// 			return array_filter($entities, function($entity) use ($user) {
// 				return $this->isGrantedEntity($entity, $user, $view_context, $bundle_context);
// 			});
// 		}
// 		throw new Exception("Error ".__METHOD__."(): first parameter entities is invalid!", 1);
// 	}

// }
}