<?php
namespace ModelApi\UserBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
// FileBundle
use ModelApi\FileBundle\Entity\Directory;
// UserBundle
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Service\serviceTier;
use ModelApi\UserBundle\Service\serviceUser;

// use \DateTime;
use \ReflectionClass;

class serviceEntreprise extends serviceTier {

	const ENTITY_CLASS = Entreprise::class;

	public function getPreferedEntreprise() {
		return $this->getRepository()->findOneByPrefered(1);
	}

	public function getDefaultEntreprise() {
		return $this->getRepository()->findForSession();
	}

	public function isOwnerEntreprises(User $user = null) {
		// if(!($user instanceOf User)) $user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
		// if(!($user instanceOf User)) return false;
		$roles = $this->getServiceParameter('owner_creators/roles', false, false);
		// if(!$role || empty($role)) return false;
		return $this->container->get(serviceGrants::class)->isGranted($roles, $user);
	}



}