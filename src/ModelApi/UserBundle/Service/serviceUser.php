<?php
namespace ModelApi\UserBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
// use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
// use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceForms;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceMailer;
// FileBundle
use ModelApi\FileBundle\Entity\Directory;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Service\serviceTier;
use ModelApi\UserBundle\Service\serviceEntreprise;

use FOS\UserBundle\Util\TokenGenerator;
// use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

// use \DateTime;
// use \OAuthProvider; // --> https://www.php.net/manual/fr/oauthprovider.generatetoken.php
use \Swift_Message;
// use \Swift_Mailer;
use \ReflectionClass;

class serviceUser extends serviceTier {

	const ENTITY_CLASS = User::class;

	// protected $user;
	protected $security;

	public function __construct(ContainerInterface $container, Security $security) {
		parent::__construct($container);
		$this->security = $security;
		// $this->user = $this->security->getUser();
		return $this;
	}


	/**
	 * Find User by User, username, id or email
	 * @param mixed $user
	 * @return User | null
	 */
	public function findUserByUniqueField($user) {
		$id = intval($user);
		if(!empty($id) && $id == $user) $user = $this->getRepository()->find($id);
		if(is_string($user)) $user = $this->getRepository()->findOneByUsername($user);
		if(!($user instanceOf User)) $user = $this->getRepository()->findOneByEmail($user);
		return $user;
	}

	public function controlUserBeforePersist(User $user) {
		return !$this->getRepository()->userExists($user);
	}

	public function getSecurity() {
		return $this->security;
	}

	/***********************************/
	/*** LOGIN DATA & FORM
	/***********************************/

	public function getLoginDataForForm($getView = true, $options = []) {
		$session = $this->container->get(serviceKernel::class)->getSession();
		// $authErrorKey = Security::AUTHENTICATION_ERROR;
		// $lastUsernameKey = Security::LAST_USERNAME;
		$lastUsername = (null === $session) ? '' : $session->get(Security::LAST_USERNAME);
		$error = null;
		$csrfToken = null;
		return array(
			'last_username' => $lastUsername,
			'error' => $error,
			'csrf_token' => $csrfToken,
		);
	}

	/***********************************/
	/*** REGISTRATION DATA & FORM
	/***********************************/

	public function getRegistrationDataForForm($getView = true, $options = [], $contexts = ['register'], $asModel = true) {
		$default_options = [
			'SUBMITS' => false,
			// 'ACTION_ROUTE' => $this->container->get('router')->generate('user_register_post', []),
			'ACTION_ROUTE' => ['route' => 'user_register_post', 'params' => null],
			'LARGE_FOOTER' => false,
			'METHOD' => 'POST',
		];
		$options = array_merge($default_options, $options);
		$form = $this->container->get(serviceForms::class)->getCreateForm($asModel ? $this->getModel(static::ENTITY_CLASS, true) : $this->createNew(), null, $contexts, $options);
		return $getView ? $form->createView() : $form;
	}


	/***********************************/
	/*** USER
	/***********************************/

	/**
	 * Get logged (current) user
	 * @see https://symfony.com/doc/3.4/security.html#always-check-if-the-user-is-logged-in
	 * @return User | string "anon."
	 */
	public function getCurrentUser() {
		// if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
		//     throw $this->container->createAccessDeniedException();
		// }
		// $token = $this->container->get('security.token_storage')->getToken();
		// $user = empty($token) ? null : $token->getUser();
		// $user = empty($this->token) ? null : $this->token->getUser();
		return $this->security->getUser();
	}

	/**
	 * Get logged (current) user or null
	 * @return User | null
	 */
	public function getCurrentUserOrNull() {
		$user = $this->getCurrentUser();
		return $user instanceOf User ? $user : null;
	}

	/**
	 * Get main User admin
	 * @return User | null
	 */
	public function getMainUserAdmin() {
		$main_admin = $this->getServiceParameter('main_admin', 'sadmin');
		// if(empty($main_admin)) return null;
		$user = is_string($main_admin) ? $this->getRepository()->findOneByUsername($main_admin) : $this->getRepository()->find($main_admin);
		return $user instanceOf User ? $user : null;
	}

	/**
	 * Get logged (current) user or root
	 * @return User | null
	 */
	public function getCurrentUserOrRoot() {
		$user = $this->getCurrentUser();
		return $user instanceOf User ? $user : $this->getRepository()->findOneByUsername(User::SERVER_USER_NAME);
	}

	/**
	 * Get default Entreprise or (current) uer or root
	 * @return Tier | null
	 */
	public function getDefaultEntrepriseOrCurrentUserOrRoot() {
		$user = $this->container->get(serviceEntities::class)->getRepository(Entreprise::class)->findByPrefered(1);
		return $user instanceOf Entreprise ? $user : $this->getCurrentUserOrRoot();
	}

	// /**
	//  * Get default Entreprise or root
	//  * @return Tier | null
	//  */
	// public function getDefaultEnvironmentOrCurrentUserOrRoot() {
	// 	$tier = $this->container->get(serviceContext::class)->getContextEnvironment();
	// 	return $tier instanceOf Tier ? $tier : $this->getCurrentUserOrRoot();
	// }

	/**
	 * Get root user
	 * @return User | null
	 */
	public function getRootUser() {
		return $this->getRepository()->findOneByUsername(User::SERVER_USER_NAME);
	}


	public function getEmailSender($asArray = false) {
		$entreprise = $this->container->get(serviceContext::class)->getContextEntreprise();
		return $asArray ? [$entreprise->getEmail() => $entreprise->getName()] : $entreprise->getEmail();
	}

	/**
	 * Environment Entreprise authorizes to register?
	 * @return boolean
	 */
	public function isAuthorizedToRegister() {
		return $this->container->get(serviceContext::class)->getEntreprise()->getPreferences()->getValue('UserBundle|self_register', false);
	}

	/**
	 * Get User's Directorys by pathname
	 * @param string $pathname
	 * @param User $user = null
	 * @param boolean $addChilds = false
	 * @param boolean $isRegexp = false
	 * @return Array <Directory>
	 */
	public function getUserDirectorys($pathname, User $user = null, $addChilds = false, $isRegexp = false) {
		if(!($user instanceOf User)) $user = $this->getCurrentUserOrNull();
		if(!($user instanceOf User)) return [];
		return $this->getTierDirectorys($pathname, $user, $addChilds, $isRegexp);
	}

	public function setTemppassword(User $user, $tmppwd = null) {
		if(!is_string($user->getPlainPassword()) && !is_string($user->getPassword())) {
			$tmppwd ??= TokenGenerator::generateToken();
			$user->setPlainPassword($tmppwd);
			$user->setTemppassword($tmppwd); // Is removed after first login
		}
		return $this;
	}

	public function createTempUser($data) {
		$user = $this->createNew();
		if(empty($user)) return null;
		foreach ($data as $attribute => $value) {
			if(preg_match('/^password$/i', $attribute)) $attribute = 'plainPassword';
			$setter = 'set'.ucfirst($attribute);
			if(method_exists($user, $setter)) $user->$setter($value);
		}
		if(!is_string($user->getUsername())) {
			$exp = explode('@', $data['email']);
			$username = $exp[0];
			$user->setUsername($username);
		}
		$this->setTemppassword($user, null);
		if($this->controlUserBeforePersist($user)) {
			$this->getEntityManager()->persist($user);
			$this->getEntityManager()->flush();
		} else {
			return null;
		}
		return $user;
	}

	public function sendUserInformations($user) {
		$memo = $user;
		$user = $this->findUserByUniqueField($user);
		if($user instanceOf User) {
			// Send email
			// $templating = $this->container->get('templating');
			// $message = (new Swift_Message())
			// 	->setSubject('Vos informations utilisateur '.$user->getUsername())
			// 	->setFrom($this->getEmailSender(true))
			// 	->setTo([$user->getEmail() => $user->getUsername()])
			// 	->setBody($templating->render('WebsiteAdminBundle:Emails:user_informations.html.twig', ['user' => $user]), 'text/html')
			// 	->addPart($templating->render('WebsiteAdminBundle:Emails:user_informations.text.twig', ['user' => $user]), 'text/plain')
			// 	;
			// $mailer = $this->container->get('mailer');
			// $result = $mailer->send($message);
			// YamlLog::directYamlLogfile([
			// 	__METHOD__ => json_encode($result)
			// ]);
			$this->container->get(serviceMailer::class)->sendMessage(
				$user,
				[
					'subject' => 'Vos informations utilisateur '.$user->getUsername(),
					'title' => 'Vos informations utilisateur '.$user->getFullnameOrUsername('long'),
					'text' => 'Vous trouverez ci-dessous les informations demandées, relatives à votre compte sur notre site.',
					'entreprise' => $this->container->get(serviceEntreprise::class)->getPreferedEntreprise(),
					'user' => $user,
				],
				null,
				'text/html',
				'WebsiteSiteBundle:Email:user_informations.html.twig'
			);

			$this->container->get(serviceFlashbag::class)->addFlashSweet('info', 'Les informations de l\'utilisateur '.json_encode($memo).' ont été envoyées par mail.');
		} else {
			$this->container->get(serviceFlashbag::class)->addFlashSweet('error', 'Echec à l\'envoi des informations de l\'utilisateur '.json_encode($memo).'.');
		}
		return $this;
	}



}