<?php
namespace ModelApi\UserBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceRoles;

// use \DateTime;
use \ReflectionClass;

class serviceGroup implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Group::class;
	const NAME_JOINER = '___';

	protected $container;
	protected $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}


	public function checkMaingroup(Tier $tier) {
		$maingroup = $tier->getMaingroup();
		if(!($maingroup instanceOf Group)) {
			$maingroup = $this->createNew([], function($maingroup) use ($tier) {
				$tier->setMaingroup($maingroup);
				$maingroup->addRole('ROLE_USER');
			});
		} else {
			$maingroup->setName($tier->getMaingroupName());
		}
		return $this;
	}

	public function findByTier(Tier $tier = null, $asArray = false) {
		return $this->getRepository()->findByTier($tier, $asArray);
	}

	public function defineRole(Group $group) {
		// Set to lower role or null
		$role = $this->container->get(serviceRoles::class)->getLowerRole($group->getRoles());
		$group->setRole($role);
		return $this;
	}

}