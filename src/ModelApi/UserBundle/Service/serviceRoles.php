<?php
namespace ModelApi\UserBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Role\Role;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Entity\UserInterface;
use ModelApi\UserBundle\Entity\GroupableInterface;
use ModelApi\UserBundle\Service\serviceUser;
// NestedBundle
use ModelApi\NestedBundle\Entity\Directory;

use \ReflectionClass;

class serviceRoles implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	protected $roleHierarchy;

	protected $em;
	// protected $user;
	protected $rolesCompleteHierarchy;
	protected $rolesParams;
	protected $states_roles;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->roleHierarchy = $this->container->get('security.role_hierarchy');
		$this->rolesParams = $this->container->getParameter('security.role_hierarchy.roles');
		$this->states_roles = $this->container->getParameter('roles');
		// defaults states roles
		if(!isset($this->states_roles['state_enabled'])) $this->states_roles['state_enabled'] = UserInterface::ROLE_TRANSLATOR;
		if(!isset($this->states_roles['state_softdeleted'])) $this->states_roles['state_softdeleted'] = UserInterface::ROLE_SUPER_ADMIN;
		// var_dump($this->states_roles);
		// entity manager
		$this->em = $this->container->get(serviceEntities::class)->getEntityManager();
		// User
		// $this->defineCurrentUser();
		// if($this->getUser() instanceOf User) {
		// 	var_dump($this->getUser()->getRoles());
		// } else {
		// 	Echo('<p>NO USER</p>');
		// }
		// Roles Hierarchy
		$this->rolesCompleteHierarchy = array();
		foreach ($this->getRoles() as $rolename) {
			$this->rolesCompleteHierarchy[$rolename] = $this->getAllRoles($rolename);
		}
		return $this;
	}



	// public function defineCurrentUser() {
	// 	$this->user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
	// 	return $this;
	// }

	public function getUser() {
		return $this->container->get(serviceUser::class)->getCurrentUserOrNull();
	}

	public function removeLowRoles($roles) {
		// $roles = $this->removeNotExistentRoles($roles);
		return array_filter($roles, function($role) {
			return !in_array($role, [UserInterface::ROLE_ALLOWED_TO_SWITCH, UserInterface::IS_AUTHENTICATED_ANONYMOUSLY, UserInterface::IS_AUTHENTICATED_REMEMBERED]);
		});
	}

	public function sortRoles($roles, $inverse = false) {
		$rolesCompleteHierarchy = $this->getRolesCompleteHierarchy();
		// echo('<pre>');var_dump($rolesCompleteHierarchy);die('</pre>');
		// $roles = $this->removeNotExistentRoles($roles);
		usort($roles, function($roleA, $roleB) use ($rolesCompleteHierarchy) {
			return in_array($roleB, $rolesCompleteHierarchy[$roleA]) ? 1 : -1;
		});
		return $inverse ? array_reverse($roles) : $roles;
	}

	public function removeNotExistentRoles($roles) {
		$roles = array_unique($roles);
		$existentRoles = $this->getRoles();
		return array_filter($roles, function($role) use ($existentRoles) {
			return in_array($role, $existentRoles);
		});
	}

	/**
	 * Sort roles and remove duplicates
	 * @param array <string> $roles
	 * @param boolean $inverse = false
	 * @return array <string>
	 */
	public function sortAndRemoveRoles($roles, $inverse = false) {
		$roles = $this->removeLowRoles($roles);
		$roles = $this->removeNotExistentRoles($roles);
		$roles = $this->sortRoles($roles, $inverse);
		return $roles;
	}

	public function hasCommonRoles($elementRoles, $roles) {
		foreach($elementRoles as $key => $elementRole) $elementRoles[$key] = new Role($elementRole);
		foreach ($roles as $role) {
			$role = new Role($role);
			if(in_array($role, $this->roleHierarchy->getReachableRoles($elementRoles))) return true;
			// foreach($elementRoles as $elementRole) {
			// 	if(in_array($role, $this->roleHierarchy->getReachableRoles(array(new Role($elementRole))))) return true;
			// }
		}
		return false;
	}


	/**
	 * Return all roles of User
	 * @param User $user = null
	 * @return array <string> | string
	 */
	public function getUserAllRoles(User $user = null, $inverse = false) {
		if(!$user instanceOf User) $user = $this->getUser();
		if(!$user instanceOf User) return [];
		$rolesHierarchy = $this->getRolesCompleteHierarchy();
		$roles = $user->getRoles();
		foreach ($roles as $role) {
			if(isset($rolesHierarchy[$role])) $roles = array_merge($roles, $rolesHierarchy[$role]);
		}
		return $this->sortAndRemoveRoles($roles, $inverse);
	}

	public function getUserHigherRole(User $user = null) {
		if(!$user instanceOf User) $user = $this->getUser();
		return $user instanceOf User ? $this->getHigherRole($user->getRoles()) : null;
	}

	public function getUserLowerRole(User $user = null) {
		if(!$user instanceOf User) $user = $this->getUser();
		return $user instanceOf User ? $this->getLowerRole($user->getRoles()) : null;
	}

	public function getHigherRole($roles) {
		if(count($roles) <= 0) return null;
		$higher = reset($roles);
		$rolesHierarchy = $this->getRolesCompleteHierarchy();
		foreach ($roles as $role) if($this->roleExists($role)) {
			if(in_array($higher, $rolesHierarchy[$role])) $higher = $role;
		}
		return $higher;
	}

	public function getLowerRole($roles) {
		if(count($roles) <= 0) return null;
		$lower = reset($roles);
		$rolesHierarchy = $this->getRolesCompleteHierarchy();
		foreach ($roles as $role) if($this->roleExists($role)) {
			if(!in_array($lower, $rolesHierarchy[$role])) $lower = $role;
		}
		return $lower;
	}



	/***********************************/
	/*** ROLES HIERARCHY
	/***********************************/

	public function getAllRoles($role) {
		$roles = array();
		if($this->roleExists($role)) {
			$attr_roles = $this->getRoleHierarchy()[$role];
			// echo('- '.implode(', ', $attr_roles).'<br>');
			foreach ($attr_roles as $attr_role) if($attr_role !== $role) {
				$roles[] = $attr_role;
				$roles = array_merge($roles, $this->getAllRoles($attr_role));
			}
		}
		return $this->removeLowRoles(array_unique($roles));
	}

	/**
	 * Role exists
	 * @param string $role
	 * @return boolean
	 */
	public function roleExists($role) {
		$exists = in_array($role, $this->getRoles());
		return $exists;
	}

	public function getRoleHierarchy() {
		return $this->rolesParams;
	}

	public function getRolesCompleteHierarchy() {
		return $this->rolesCompleteHierarchy;
	}

	public function getRoles($forChoices = false) {
		$roles = array_keys($this->getRoleHierarchy());
		if(!$forChoices) return $roles;
		$rolesForChoices = [];
		foreach ($roles as $role) {
			$rolesForChoices[$role] = $role;
		}
		return $rolesForChoices;
	}

	/**
	 * Get roles of $role
	 * @param string $role
	 * @return array <string>
	 */
	public function getRights($role) {
		return $this->roleExists($role) ? $this->getRolesCompleteHierarchy()[$role] : array();
	}

	// /**
	//  * is granted for view (use current User if $user is null)
	//  * @param object $entity
	//  * @param User $user = null
	//  * @return boolean
	//  */
	// public function isGrantedForView($entity, User $user = null) {
	// 	if(!is_object($entity)) throw new Exception("serviceRoles::isGrantedForView() Error : first parameter must be of type object.", 1);
	// 	// if object has no roles attribute : it's public
	// 	if(!method_exists($entity, 'getRoles')) return true;
		
	// 	if(null === $user) $user = $this->getUser();
	// 	if($user instanceOf User) {
	// 		// User is defined and logged
	// 		// ………………………… ????
	// 		return true;
	// 	} else {
	// 		// no User logged (public context)
	// 		return count($entity->getRoles() <= 0);
	// 	}
	// }

	// /**
	//  * is granted for view (use current User if $user is null)
	//  * @param object $entity
	//  * @param User $user = null
	//  * @return boolean
	//  */
	// public function isGrantedForManage($entity, User $user = null) {
	// 	if(!is_object($entity)) throw new Exception("serviceRoles::isGrantedForManage() Error : first parameter must be of type object.", 1);
	// 	// if object has no roles attribute : it's public
	// 	if(!method_exists($entity, 'getRoles')) return true;

	// 	if(null === $user) $user = $this->getUser();
	// 	if($user instanceOf User) {
	// 		// User is defined and logged
	// 		// ………………………… ????
	// 		return true;
	// 	} else {
	// 		// no User logged (public context)
	// 		return count($entity->getRoles() <= 0);
	// 	}
	// }


}