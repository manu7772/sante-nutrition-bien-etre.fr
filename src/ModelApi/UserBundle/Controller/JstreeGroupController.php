<?php

namespace ModelApi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Response;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
// UserBundle
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceGroup;

use \Exception;

class JstreeGroupController extends Controller {

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Group",
	 *    description="Get Groups for Jstree",
	 *    output = { "class" = Group::class, "collection" = true, "groups" = {"Group"} },
	 *    views = { "default", "group" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/group", name="apigroup-jstree-getgroups")
	 */
	public function getGroupsAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$groups = $serviceEntities->getRepository(Group::class)->findByParent(null);
		return $groups;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Group",
	 *    description="Get one Group",
	 *    output = { "class" = Group::class, "collection" = false, "groups" = {"Group"} },
	 *    views = { "default", "group" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/group/{id}", name="apigroup-jstree-getgroup")
	 */
	public function getGroupAction($id, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$group = $serviceEntities->getRepository(Group::class)->find($id);
		return $group;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Group",
	 *    description="Get Group's childs for Jstree",
	 *    output = { "class" = Group::class, "collection" = true, "groups" = {"Group"} },
	 *    views = { "default", "group" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/group-childs/{id}", name="apigroup-jstree-getgroupchilds")
	 */
	public function getGroupChildsAction($id, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$group = $serviceEntities->getRepository(Group::class)->find($id);
		return $group->getChilds();
	}




	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Group",
	 *    description="Remove Group from Group",
	 * )
	 * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
	 * @Rest\Delete("/jstree/group/{shortname}/{id}", name="apigroup-jstree-group-delete")
	 */
	public function groupDeleteAction($shortname, $id, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		// $shortname = $request->request->get('shortname');
		// $id = $request->request->get('id');
		$classname = $serviceEntities->getClassnameByShortname($shortname);
		// $new_group->setFilledError('TEST');
		switch ($shortname) {
			case 'Nestlink':
				$nestlink = $serviceEntities->getRepository($classname)->find($id);
				if(!empty($nestlink)) {
					if($nestlink->isSymbolic()) {
						// remove symbolic link only
						$em->remove($nestlink);
						$em->flush();
					} else if($nestlink->isSolid()) {
						// remove all links and softdelete Group
						$group = $nestlink->getChild();
						$group->removeParents();
						$group->setSoftdeleted();
						$em->flush();
					}
				} else {
					// throw new Exception("Group not found", 1);
				}
				break;
			default: // groups
				throw new Exception("Error Processing Request: ".json_encode($shortname), 1);
				break;
		}
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Group",
	 *    description="Create Group",
	 *    output = { "class" = Group::class, "collection" = false, "groups" = {"Group"} },
	 *    views = { "default", "group" }
	 * )
	 * @Rest\View(serializerGroups={"jstree","jstree_updated"})
	 * @Rest\Post("/jstree/group/create", name="apigroup-jstree-group-create")
	 */
	public function groupCreateAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		$group = $request->request->all();
		if(!isset($group['classname'])) $group['classname'] = $serviceEntities->getClassnameByShortname($group['shortname']);
		$new_group = $serviceEntities->getEntityService($group['shortname'])->createNew();
		// $new_group->setFilledError('TEST');
		$parent = $serviceEntities->findByBddid($group['parent']);
		if(!empty($parent)) {
			$serviceEntities->compileSerializedEntity($group);
			$new_group->fillWithData($group);
			$parent->addChild($new_group);
			if($this->get('validator')->validate($group)) {
				$em->persist($new_group);
				$em->flush();
			} else {
				$new_group->setFilledError($group['shortname'].' is invalid!');
			}
		} else {
			$new_group->setFilledError('Ce '.$group['shortname'].' doit être placé dans un dossier. Aucun dossier spécifié !');
		}
		return $new_group;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Group",
	 *    description="Create Group",
	 *    output = { "class" = Group::class, "collection" = false, "groups" = {"Group"} },
	 *    views = { "default", "group" }
	 * )
	 * @Rest\View(serializerGroups={"jstree","jstree_updated"})
	 * @Rest\Post("/jstree/group/move", name="apigroup-jstree-group-move")
	 */
	public function groupMoveAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		$data = $request->request->all();
		// if(!isset($data['node']['original']['classname'])) $data['node']['original']['classname'] = $serviceEntities->getClassnameByShortname($data['node']['original']['shortname']);
		$nestlink = $serviceEntities->getRepository(Nestlink::class)->find($data['node']['id']);
		$group = $serviceEntities->getRepository($data['node']['original']['classname'])->find($data['node']['original']['regular_id']);
		if(!empty($group) && !empty($nestlink)) {
			if($data['parent'] === $data['old_parent']) {
				// just change position in same Group
				if($data['position'] !== $data['old_position']) $group->setPosition($data['position'], $nestlink);
			} else {
				$new_parent = $serviceEntities->getRepository(Group::class)->getChildByNestlink($data['parent']);
				if(!empty($new_parent)) {
					$new_parent->addChild($group);
					if($new_parent->isLastResultSuccess()) {
						$group->setPosition($data['position']);
					} else {
						throw new Exception('Le nouveau parent a rejeté ce '.$data['node']['original']['shortname'].' !', 1);
					}
				} else {
					throw new Exception('Le nouveau parent de ce '.$data['node']['original']['shortname'].' n\'a pu être trouvé !', 1);
				}
			}
			if($this->get('validator')->validate($group)) {
				$em->flush();
			} else {
				$group->setFilledError($data['node']['original']['shortname'].' is invalid!');
			}
		} else {
			throw new Exception('Ce '.$data['node']['original']['shortname'].' n\'a pu être trouvé !', 1);
		}
		return $group;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Group",
	 *    description="Update and create Groups",
	 *    output = { "class" = Group::class, "groups" = {"Group"} },
	 *    views = { "default", "group" }
	 * )
	 * @Rest\View(serializerGroups={"jstree","jstree_updated"})
	 * @Rest\Post("/jstree/post/groups", name="apigroup-jstree-post-groups")
	 */
	public function postGroupsAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		$groups = $request->request->all();
		$oneGroup = false;
		// if not array of groups but just one group...
		if(isset($groups['classname'])) {
			$groups = [$groups];
			$oneGroup = true;
		}
		foreach ($groups as $key => $group) {
			$id = null;
			if(isset($group['regular_id'])) {
				$id = $group['regular_id'];
			} else if(isset($group['id'])) {
				if(preg_match('/\\d+/', $group['id'])) $id = $group['id'];
				if(preg_match('/^\\d+_\\w+_\\d+$/', $group['id'])) {
					$expl = preg_split('/_/', $group['id']);
					$id = end($expl);
				}
				$id = intval($id);
				if($id <= 0) $id = null;
			}
			if(null === $id) {
				// new Group
				if(!isset($group['classname'])) $group['classname'] = $serviceEntities->getClassnameByShortname($group['shortname']);
				$new_group = $serviceEntities->getEntityService($group['shortname'])->createNew();
				$serviceEntities->compileSerializedEntity($group);
				$new_group->fillWithData($group);
				if($this->get('validator')->validate($group)) {
					$em->persist($new_group);
					$em->flush();
				} else {
					$new_group->setFilledError($group['shortname'].' is invalid!');
				}
				$groups[$key] = $new_group;
			} else {
				// update Group
				$update_group = $serviceEntities->getRepository(Group::class)->find($id);
				// echo('<pre><h3>n°'.$key.' = '.$update_group->getShortname().' #'.$update_group->getId().' '.(empty($update_group) ? 'NOT FOUND' : 'FOUND').'</h3>');var_dump($group);die('</pre>');
				$serviceEntities->compileSerializedEntity($group);
				$update_group->fillWithData($group);
				if($this->get('validator')->validate($group)) {
					$em->flush();
				} else {
					$update_group->setFilledError($group['shortname'].' is invalid!');
				}
				$groups[$key] = $update_group;
			}
		}
		return $oneGroup ? reset($groups) : $groups;
	}




}
