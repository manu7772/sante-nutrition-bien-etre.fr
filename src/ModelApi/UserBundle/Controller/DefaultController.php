<?php
namespace ModelApi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Response;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Service\serviceUser;

class DefaultController extends Controller {

	/**
	 * @Route("/")
	 */
	public function indexAction() {
		return new Response('This web page is the homepage of UserBundle. Never use it please.');
	}


	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="user",
	 *    description="User accept cookies",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"File"} },
	 *    views = { "default", "User" }
	 * )
	 * @Rest\View(serializerGroups={"base","Item","User"})
	 * @Rest\Post("/user-accept-cookies/{id}", name="user_accept_cookies")
	 */
	public function useracceptcookiesAction($id = 0, Request $request) {
		$serviceUser = $this->get(serviceUser::class);
		if($id < 1) $user = $this->getUser();
			else $user = $serviceUser->getRepository()->find($id);
		if(!empty($user)) {
			$user->setPreference('cookies', true);
			$this->getDoctrine()->getEntityManager()->flush();
		}
		if(($this->getUser() instanceOf User && $this->getUser() === $user) || empty($user)) {
			// Keep in session
			$session = $request->getSession();
			$session->set('accepted_cookies', true);
		}
		return $user;
	}


}
