<?php
namespace ModelApi\UserBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// UserBundle
use ModelApi\UserBundle\Entity\Certificat;

// use \ReflectionClass;

class CertificatAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Certificat::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}