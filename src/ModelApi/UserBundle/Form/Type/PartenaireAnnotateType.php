<?php
namespace ModelApi\UserBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// UserBundle
use ModelApi\UserBundle\Entity\Partenaire;

// use \ReflectionClass;

class PartenaireAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Partenaire::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}