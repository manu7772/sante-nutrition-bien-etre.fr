<?php
namespace ModelApi\UserBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;

// use \ReflectionClass;

class UserAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = User::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}

	// /**
	//  * Get form contexts
	//  * Returns all contexts separated by |
	//  * @return array
	//  */
	// public function getFormAllContexts(Tier $user = null, $formContexts = null) {
	// 	// Form for register
	// 	if(empty($this->user) && $this->entity instanceOf User && empty($this->entity->getId())) {
	// 		$this->user = $this->entity;
	// 		return [static::USER_DEFAULT_CONTEXT, 'simple'];
	// 	}

	// 	return parent::getFormAllContexts($user, $formContexts);
	// }


}