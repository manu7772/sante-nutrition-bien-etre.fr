<?php

namespace ModelApi\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ModelApiUserBundle extends Bundle
{

	public function getParent() {
		return 'FOSUserBundle';
	}

}
