<?php
namespace ModelApi\BinaryBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Service\serviceEntities;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Tempbinary;
// FileBundle
// use ModelApi\FileBundle\Entity\Item;
// use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Service\FileManager;

// use \DateTime;
use \ReflectionClass;

class serviceTempbinary implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Tempbinary::class;

	private $container;
	private $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
	}



}