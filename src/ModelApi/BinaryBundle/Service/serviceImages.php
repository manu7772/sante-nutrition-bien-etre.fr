<?php
namespace ModelApi\BinaryBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Imagine\Imagick\Imagine as Imagine_Imagick;
use Imagine\Gd\Imagine as Imagine_Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Filter\Transformation;
// use Imagine\Filter\Basic\Crop;
use Imagine\Image\ImageInterface;
use Imagine\Image\Palette\RGB;

// BaseBundle
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\UploadInterface;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

use \imagick;
use \Exception;

/**
 * Uses imagine/imagine 1.2.2
 * @see https://packagist.org/packages/imagine/imagine
 * @see https://www.slideshare.net/avalanche123/introduction-toimagine
 * @see https://imagine.readthedocs.io/en/1.2.2/
 * @see https://imagine.readthedocs.io/en/1.2.2/_static/API/
 */
class serviceImages implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const IMAGE_MANQUANTE_TEXT = 'image'.PHP_EOL.'manquante';
	const IMAGICK_NAME = 'Imagick';
	const GD_NAME = 'Gd';

	protected $container;
	protected $mediaEnvironment;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->setMediaEnvironment($this->container->getParameter('UploadBinary')['config']['processor']);
		return $this;
	}

	public function setMediaEnvironment($mediaEnvironment) {
		if(!in_array($mediaEnvironment, [static::IMAGICK_NAME,static::GD_NAME])) $mediaEnvironment = static::IMAGICK_NAME;
		if(!class_exists(static::IMAGICK_NAME)) $mediaEnvironment = static::GD_NAME;
		$this->mediaEnvironment = $mediaEnvironment;
		return $this;
	}

	public function getMediaEnvironment() {
		return $this->mediaEnvironment;
	}

	public function isImagick() {
		return $this->mediaEnvironment === static::IMAGICK_NAME;
	}

	public function isGd() {
		return $this->mediaEnvironment === static::GD_NAME;
	}

	public function getNewImagineInstance() {
		if($this->isImagick()) return new Imagine_Imagick();
		if($this->isGd()) return new Imagine_Gd();
		return null;
	}



	/**************************************************************/
	/*** STATIC METHODS
	/**************************************************************/

	public static function isImagickContext() {
		return class_exists(static::IMAGICK_NAME);
	}

	public static function isImageInterface($object) {
		return $object instanceOf ImageInterface;
	}

	public static function isTransformable($object) {
		return static::isImageInterface($object) || is_resource($object) || is_string($object);
	}

	public static function getStaticNewImagineInstance() {
		if(!static::isImagickContext()) return new Imagine_Gd();
		return new Imagine_Imagick();
	}

	public static function makeImagineObject($stream, $page = null, $missingIfError = true) {
		if(null !== $page) {
			$page = intval($page);
			if(!is_integer($page) || $page < 0) $page = 0;
		}
		if(static::isImageInterface($stream)) {
			if(is_integer($page)) {
				if($page > $stream->layers()->count()) $page = $stream->layers()->count() - 1;
				return $stream->layers()->get($page);
			}
			return $stream;
		}
		if(static::isTransformable($stream)) {
			if(is_resource($stream)) {
				$imagine = static::getStaticNewImagineInstance();
				return is_integer($page) ? $imagine->read($stream.'['.$page.']') : $imagine->read($stream);
			} else if(is_string($stream)) {
				$imagine = static::getStaticNewImagineInstance();
				return is_integer($page) ? $imagine->load($stream.'['.$page.']') : $imagine->load($stream);
			} else if(is_integer($page)) {
				if($page > $stream->layers()->count()) $page = $stream->layers()->count() - 1;
				return $stream->layers()->get($page);
			}
		}
		return $missingIfError ? static::getMissingImage(400, 400) : null;
	}

	public static function getImagineFromPdfFile($path, $page = 0, $resolution = 72) {
		$image = new imagick();
		$image->setResolution($resolution, $resolution);
		$image->readImage(realpath($path).'['.$page.']');
		$image->setImageBackgroundColor('white');
		$image->setImageAlphaChannel(imagick::ALPHACHANNEL_REMOVE);
		$image->setImageFormat('png');
		$image->flattenImages();
		return static::makeImagineObject($image->getImageBlob(), 0, true);
	}

	public static function getImagineFromPdf($stream, $page = 0, $resolution = 72) {
		if(!($stream instanceOf imagick)) {
			$image = new imagick();
			$image->setResolution($resolution, $resolution);
			$image->readImageBlob($stream);
		}
		// $image->readImage(realpath($stream).'['.$page.']');
		$cpt = 0;
		foreach ($image as $frame) {
			if($cpt === $page) {
				$image2 = $frame;
				break;
			}
			$cpt++;
		}
		unset($image);
		$image2->setImageBackgroundColor('white');
		$image2->setImageAlphaChannel(imagick::ALPHACHANNEL_REMOVE);
		$image2->setImageFormat('png');
		$image2->flattenImages();
		return static::makeImagineObject($image2->getImageBlob(), 0, true);
	}

	public static function getResizeSizes(BoxInterface $size, $Xsize = null, $Ysize = null) {
		$Xinit = $size->getWidth();
		$Yinit = $size->getHeight();
		$ratio = $Xinit / $Yinit;
		// size
		if($Xsize == null && $Ysize == null) {
			$Xsize = $Xinit;
			$Ysize = $Yinit;
		}
		if($Xsize == null) $Xsize = floor($Ysize * $ratio);
		if($Ysize == null) $Ysize = floor($Xsize / $ratio);
		// upsize
		$up_ratio = max(floor($Xsize / $Xinit), floor($Ysize / $Yinit));
		$Xupsize = $up_ratio > 1 ? $Xinit * $up_ratio : $Xinit;
		$Yupsize = $up_ratio > 1 ? $Yinit * $up_ratio : $Yinit;
		return ['original' => ['x' => $Xinit, 'y' => $Yinit], 'resized' => ['x' => $Xsize, 'y' => $Ysize], 'upsize' => ['x' => $Xupsize, 'y' => $Yupsize]];
	}

	public static function computeSizeAgree(BoxInterface $size, $Xsize = null, $Ysize = null, $onlyReduce = true) {
		if(!$onlyReduce || ($Xsize === null && $Ysize === null)) return true;
		$sizes = static::getResizeSizes($size, $Xsize, $Ysize);
		return $sizes['resized']['x'] <= $sizes['original']['x'] || $sizes['resized']['y'] <= $sizes['original']['y'];
		// return $sizes['resized']['x'] <= $sizes['original']['x'] && $sizes['resized']['y'] <= $sizes['original']['y'];
	}

	public static function ok_for_generation(ImageInterface $stream, $Xsize = null, $Ysize = null, $onlyReduce = true, $page = 0) {
		$stream = static::makeImagineObject($stream, $page, false);
		if(null === $stream) return false;
		return static::computeSizeAgree($stream->getSize(), $Xsize, $Ysize, $onlyReduce);
	}

	/**
	* thumb_image
	* Crée et enregistre un tumbnail de l'image
	* @param image $stream
	* @param integer $Xsize = null
	* @param integer $Ysize = null
	* @param string $mode = "no"
	* @param string $format = null
	* @param boolean $onlyReduce = true
	* @return image
	*/
	public static function thumb_image($stream, $Xsize = null, $Ysize = null, $mode = null, $onlyReduce = true, $flatten = false, $page = 0) {
		// $mode =
		// cut       : remplit la box avec l'image et la coupe si besoin
		// in        : inclut l'image pour qu'elle soit entièrerement visible
		// deform    : déforme l'image pour qu'elle soit exactement à la taille
		// no | null : ne modifie pas la taille de l'image
		// calcul…
		set_time_limit(600);
		// ini_set('memory_limit', '2048M');

		$stream = static::makeImagineObject($stream, $page, false);
		if(null === $stream) return null;
		// resize control
		$size = $stream->getSize();
		if(!static::computeSizeAgree($size, $Xsize, $Ysize, $onlyReduce)) return null;
		$size = static::getResizeSizes($size, $Xsize, $Ysize);
		// transformation
		$newBox = new Box($size['resized']['x'], $size['resized']['y']);
		// $newBox->usePalette(new RGB);
		// $newBox->strip();
		$transformation = new Transformation();
		// $transformation->WebOptimization($newBox);
		if($mode !== null) {
			// Got thumbnail
			$thum_method = ImageInterface::THUMBNAIL_FLAG_UPSCALE;
			if($mode === 'cut') $thum_method = ImageInterface::THUMBNAIL_OUTBOUND;
			if($mode === 'in') $thum_method = ImageInterface::THUMBNAIL_INSET;
			if($mode === 'formated') $thum_method = ImageInterface::THUMBNAIL_INSET;
			$transformation->resize(new Box($size['upsize']['x'], $size['upsize']['y']));
			$transformation->thumbnail($newBox, $thum_method);
		} else {
			$transformation->resize($newBox);
			// $dolayers = true;
		}
		// Apply
		if($stream->layers()->count() > 1 && !$flatten) {
			// is animated
			$stream->layers()->coalesce();
			foreach ($stream->layers() as $layer) $transformation->apply($layer);
		} else {
			// not animated, get first image
			$stream = $transformation->apply($stream->layers()->get(0));
		}
		// DevTerminal::Warning('     --> created image: '.get_class($stream));
		// $test_size = $stream->getSize();
		// if($test_size->getWidth() !== $size['resized']['x'] || $test_size->getHeight() !== $size['resized']['y']) {
		// 	$message = "Error image thum is not at the good size: ".$test_size->getWidth()."x".$test_size->getHeight()."px instead of ".$size['resized']['x']."x".$size['resized']['y']."px (used ".json_encode($mode)." mode).";
		// 	DevTerminal::Warning('     --> '.$message);
		// }
		return $stream;
	}


	public static function setAsString(&$resource, $format = null) {
		if(!is_resource($resource)) throw new Exception("Error ".__METHOD___."() line ".__LINE__." : first parameter is not a resource (".gettype($resource)." given).", 1);
		ob_start();
		switch(strtolower($format)) {
			case 'jpeg':
			case 'jpg': imagejpeg($resource); break;
			case 'gif': imagegif($resource); break;
			case 'png': imagepng($resource); break;
			default: imagepng($resource); break;
		}
		$resource = ob_get_contents();
		ob_end_clean();
	}

	public static function getCropped($image, $w, $h, $x, $y, $width, $height, $rotate = 0) {
		$message = '';
		if(is_string($image)) {
			try {
				$image = @imagecreatefromstring($image);
			} catch (Exception $e) {
				$message = $e->getMessage();
			} finally {
				if(!is_resource($image)) {
					$image = array('image' => static::getMissingImage($width, $height), 'width' => $width, 'height' => $height);
					// return $reponse->initAeReponse(true, $image, 'Génération de l\'image impossible. Une image de remplacement a été générée. '.$message);
					return $image;
				}
			}
		}
		if($rotate != 0) imagerotate($image, $rotate, 0, 0);
		$message = '';
		try {
			$stream = @imagecreatetruecolor($w, $h);
		} catch (Exception $e) {
			$message = $e->getMessage();
		} finally {
			if(!is_resource($stream)) {
				$stream = array('image' => static::getMissingImage($width, $height), 'width' => $width, 'height' => $height);
				// return $reponse->initAeReponse(true, $stream, 'Dimensionnement de l\'image impossible. Une image de remplacement a été générée. '.$message);
				return $stream;
			}
		}
		imagealphablending($stream, false);
		imagesavealpha($stream, true);
		// $reponse->setResult(imagecopyresampled($stream, $image, 0, 0, $x, $y, $w, $h, $width, $height));
		if(true === imagecopyresampled($stream, $image, 0, 0, $x, $y, $w, $h, $width, $height)) {
			// OK
			// $message = 'Génération de l\'image réussie. ';
			// $gravity = ' légèrement';
			// if($width < ($x / 2) || $height < ($y / 2)) $gravity = ' beaucoup';
			// if($width < $x || $height < $y) $message .= 'Attention, l\'image a été'.$gravity.' agrandie. Sa résolution ne sera pas suffisante pour une qualité d\'affichage optimale.';
			// $reponse->setMessage($message);
			// $reponse->setData(array('image' => $stream, 'width' => $width, 'height' => $height));
		} else {
			// ERROR
			// $reponse->setMessage('Une erreur s\'est produite pendant la génération. Veuillez recommencer l\'opération.');
			throw new Exception("Error ".__METHOD___."() line ".__LINE__." while generating resampled image", 1);
		}
		// return $reponse;
		return $stream;
	}

	public static function computeXandY($originX, $originY, &$x = null, &$y = null) {
		if((integer)$x < 1 && (integer)$y < 1) {
			$x = $originX;
			$y = $originY;
		} else if((integer)$y < 1) {
			$y = $originY / $originX * $x;
		} else if((integer)$x < 1) {
			$x = $originX / $originY * $y;
		}
	}

	public static function getMissingImage($x, $y, $message = null) {
		if($message === null) $message = self::IMAGE_MANQUANTE_TEXT;
		if((integer)$x == 0 || (integer)$y == 0) $x = $y = 80;
		$image = @imagecreatetruecolor($x, $y);
		$col_back = imagecolorallocate($image, 200, 200, 200);
		imagefill($image, 0, 0, $col_back);
		$col_text = imagecolorallocate($image, 100, 100, 100);
		$messages = explode(PHP_EOL, (string)$message);
		$decal = 14;
		$cptDecal = -floor((count($messages) * $decal / 2) - ($decal / 2));
		foreach ($messages as $text) {
			static::imageCenteredString($image, $text, 2, 0, $cptDecal, $col_text);
			$cptDecal += $decal;
		}
		return $image;
	}

	/**
	 * Add text in image resource, with optional x and y shift
	 * @param resource &$image
	 * @param integer $font
	 * @param integer $x
	 * @param integer $y
	 * @param string $str
	 * @param resource $col_text = null
	 */
	public function imageCenteredString(&$image, $str, $font = 2, $x = 0, $y = 0, $col_text = null) {
		if($col_text == null) $col_text = imagecolorallocate($image, 100, 100, 100);
		$xLoc = floor((imagesx($image) - (imagefontwidth($font) * strlen($str))) / 2 + $x);
		$yLoc = floor((imagesy($image) - imagefontheight($font)) / 2 + $y);
		imagestring($image, $font, $xLoc, $yLoc, $str, $col_text);
	}

}









