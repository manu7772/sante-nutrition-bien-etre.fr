<?php
namespace ModelApi\BinaryBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\UploadInterface;
use ModelApi\BinaryBundle\Service\serviceImages;
// FileBundle
use ModelApi\FileBundle\Entity\File as EntityFile;
use ModelApi\FileBundle\Entity\Fileformat;
// use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\serviceFileformat;

use ModelApi\DevprintsBundle\Service\DevTerminal;

use \Exception;

/**
 * Uses imagine/imagine 1.2.2
 * @see https://packagist.org/packages/imagine/imagine
 * @see https://www.slideshare.net/avalanche123/introduction-toimagine
 * @see https://imagine.readthedocs.io/en/1.2.2/
 * @see https://imagine.readthedocs.io/en/1.2.2/_static/API/
 */
class serviceBinaryFile implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const DEFAULT_DECLINATION_NAME = 'default';
	const WEB_PATH = [__DIR__, '..', '..', '..', '..', 'web'];
	// const PDF_FLATTEN_RESOLUTION = 150;
	// const PDF_FLATTEN_FORMAT = 'png';

	protected $container;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		return $this;
	}

	// public function getConfig($names = null) {
	// 	if(null === $names) return $this->config;
	// 	$names = explode('.', $names);
	// 	reset($names);
	// 	$config = $this->config;
	// 	foreach ($names as $current) {
	// 		if(!isset($config[$current])) throw new Exception("Error on getting UploadBinary config: ".json_encode($names)." > ".json_encode($current)." does not exist!", 1);
	// 		$config = $config[$current];
	// 	}
	// 	return $config;
	// }

	// /**
	//  * Get declinations for UploadInterface
	//  * @param UploadInterface $entity
	//  * @return array
	//  */
	// public function getMediaDeclinations(UploadInterface $entity) {
	// 	$decl = $this->getConfig('declinations');
	// 	$media = $entity->getMediaType();
	// 	$media_declinations = isset($decl[$media]) ? $decl[$media] : $decl[static::DEFAULT_DECLINATION_NAME];
	// 	$contentType = $entity->getContentType();
	// 	// echo('<p>Media type: '.$contentType.' > '.$media.'</p>');
	// 	return isset($media_declinations[$contentType]) ? $media_declinations[$contentType] : $media_declinations[static::DEFAULT_DECLINATION_NAME];
	// }

	/**
	 * Set declinations for UploadInterface if not exists (or force it!)
	 * Called in PrePersist & PreUpdate
	 * @param UploadInterface $entity
	 * @param boolean $force = false
	 * @return serviceBinaryFile
	 */
	public function setBaseDeclinations(UploadInterface $entity, $force = false) {
		if(!$entity->has_declinations() || $force) {
			$entity->defineBaseDeclinations($this->getServiceParameter('declinations'));
		}
		return $this;
	}

	// /**
	//  * Set declinations for UploadInterface
	//  * @param UploadInterface $entity
	//  * @return serviceBinaryFile
	//  */
	// public function defineBaseDeclinations(UploadInterface $entity) {
	// 	$base_declinations = $this->getMediaDeclinations($entity);
	// 	if(!is_array($base_declinations)) throw new Exception("Error: ".json_encode($entity->__toString())." has no media declinations!", 1);
	// 	$entity->setBase_declinations($base_declinations);
	// 	return $this;
	// }

	public static function transformRequestBagsUploadedFiles(Request $request) {
		if(!$request->request->count()) return;
		// echo('<pre><h3>transformRequest(): Request > '.get_class($request->request).'</h3>'); var_dump($request->request->all()); echo('</pre><hr>');
		// echo('<pre><h3>transformRequest(): Files > '.get_class($request->files).'</h3>'); var_dump($request->files->all()); echo('</pre><hr>');
		$data = $request->request->all();
		serviceTools::fromJson($data);
		$new_data = static::replaceUploadedFileData($data, $request->files->all());
		$request->request->replace($new_data['request']);
		foreach ($new_data['files'] as $type_name => $file) {
			if(!empty($file)) $request->files->set($type_name, $file);
		}
		// echo('<pre><h3>transformRequest(): Request > '.get_class($request->request).'</h3>'); var_dump($request->request->all()); echo('</pre><hr>');
		// echo('<pre><h3>transformRequest(): Files > '.get_class($request->files).'</h3>'); var_dump($request->files->all()); echo('</pre><hr>');
		// $uploadedFile = $request->files->get('image_annotate')['uploadFile'];
		// echo('<h3>image_annotate > uploadFile real path: '.$uploadedFile->getRealPath().'</h3>');
		// die();
		return;
	}

	public static function replaceUploadedFileData($request_data, $files_data) {
		foreach ($request_data as $type_name => $data) if(is_array($data)) {
			if(array_key_exists('type', $data)) {
				switch (strtolower($data['type'] ?? null)) {
					case 'uploadedfile':
						// $data['file'] = __DIR__.'/../../../../web/'.$data['file'];
						if(!@file_exists($data['file'])) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): file ".$data['file']." not found on server!", 1);
						unset($request_data[$type_name]);
						// $file = serviceTools::getUploadFileInstance($data['file'], $data['filename']);
						// $test = new File($data['file']);
						// echo('<div>Generated File: '.$test->getMimeType().'</div><pre>'); var_dump($test); echo('</pre><hr>');
						$file = new UploadedFile($data['file'], $data['filename'], null, null, null, true);

						// $fileSystem = new Filesystem();
						// $tmpfile = '/tmp/'.$data['filename'];
						// $fileSystem->copy($data['file'], $tmpfile, true);
						// $fileSystem->chmod($tmpfile, 0777);
						// // $fileSystem->chown($tmpfile, 'root');
						// // $fileSystem->chgrp($tmpfile, 'root');
						// $file = new UploadedFile($tmpfile, $data['filename']);
						// unset($fileSystem);

						if($file instanceOf UploadedFile) {
							if(!$file->isValid()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): generated UploadedFile with file ".$data['file']." has error(s) > ".json_encode(static::getErrorMessage($file->getError()))."!", 1);
							if(!@file_exists($file->getRealPath())) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): file ".$file->getRealPath()." not found!", 1);
							$files_data[$type_name] = $file;
							// echo('<div>Generated UploadedFile: '.$file->getRealPath().'</div><pre>'); var_dump($file); echo('</pre><hr>');
							// $content = file_get_contents($file->getRealPath());
							// echo('<div style="height: 200px; overflow: auto;">'); var_dump($content); echo('</div><hr>');
						} else {
							throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not generate UploadedFile with file ".$data['file']."!", 1);
						}
						break;
					default:
						# code...
						break;
				}
			} else {
				$new_data = static::replaceUploadedFileData($request_data[$type_name], $files_data[$type_name] ?? []);
				if(!empty($new_data['files'])) {
					$request_data[$type_name] = $new_data['request'];
					$files_data[$type_name] = $new_data['files'];
				}
			}
		}
		return ['request' => $request_data, 'files' => $files_data];
	}

	public static function getErrorMessage($error) {
		$phpFileUploadErrors = array(
		    0 => 'There is no error, the file uploaded with success',
		    1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
		    2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
		    3 => 'The uploaded file was only partially uploaded',
		    4 => 'No file was uploaded',
		    6 => 'Missing a temporary folder',
		    7 => 'Failed to write file to disk.',
		    8 => 'A PHP extension stopped the file upload.',
		);
		return $phpFileUploadErrors[$error];
	}


	/**
	 * Verify if is unique value regarding to other fields (associations or columns)
	 * @param UploadInterface $entity
	 * @return serviceBinaryFile
	 */
	public function AttachFileformat(UploadInterface $entity, $exceptionIfNotFound = true) {
		// $fileformat = null;
		// $find = null;
		// $streamType = null;
		if($entity->hasStreamTypeWhileUpload()) {
			// find Fileformat
			$streamType = $entity->getStreamTypeWhileUpload(true);
			$fileFormat = $this->getFileformatByUploadedFile($streamType);
				// $streamType = preg_replace('/\\//', '_', $entity->getStreamTypeWhileUpload(true));
				// // $fileFormat = $this->container->get(serviceFileformat::class)->getRepository()->findOneBy(array('name' => $entity->getStreamTypeWhileUpload(true), 'enabled' => 1));
				// $find = $this->container->get(serviceFileformat::class)->getRepository()->findOneBy(array('name' => $streamType));
				// if(empty($find)) $find = $this->container->get(serviceFileformat::class)->getRepository()->findOneBy(array('name' => $entity->getStreamTypeWhileUpload(true)));
				// $fileFormat = $find instanceOf Fileformat && $find->isActive() ? $find : null;
				// // echo('<div>Attached file format: '.json_encode($entity->getStreamTypeWhileUpload()).' -> '.$fileFormat.'</div>');
			if($exceptionIfNotFound && empty($fileFormat)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this file format ".json_encode($streamType)." is not supported!", 1);
			$entity->setFileformat($fileFormat);
		}
		if(empty($entity->getFileformat())) {
			if($exceptionIfNotFound) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this file has no fileformat!", 1);
		}
		// $entity->EntityReportLog();
		// DevTerminal::Info('     --> Attached file format: '.json_encode($entity->getStreamTypeWhileUpload()));
		return $this;
	}

	public function getFileformatByUploadedFile($uploadedFile) {
		if($uploadedFile instanceOf UploadedFile) {
			$mimeType = $uploadedFile->getMimeType();
			$fileExtension = $uploadedFile->getClientOriginalExtension();
			if(!is_string($fileExtension)) $fileExtension = $uploadedFile->getExtension();
			if(!is_string($mimeType)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not find file MIMETYPE by ".(is_object($uploadedFile) ? get_class($uploadedFile) : json_encode($uploadedFile))."!", 1);
			if(!is_string($fileExtension)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not find file EXTENSION by ".(is_object($uploadedFile) ? get_class($uploadedFile) : json_encode($uploadedFile))."!", 1);
			$streamType = $mimeType.'@'.$fileExtension;
		} else {
			$streamType = is_string($uploadedFile) ? $uploadedFile : null;
		}
		if(!is_string($streamType)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not find fileFormat by ".(is_object($uploadedFile) ? get_class($uploadedFile) : json_encode($uploadedFile))."!", 1);
		$fileformat = $this->container->get(serviceFileformat::class)->getRepository()->findOneBy(array('name' => preg_replace('/\\//', '_', $streamType)));
		return $fileformat instanceOf Fileformat && $fileformat->isActive() ? $fileformat : null;
	}

	public function getFileClassesByUploadedFile(UploadedFile $uploadedFile) {
		$fileFormat = $this->getFileformatByUploadedFile($uploadedFile);
		if(empty($fileFormat)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not find any valid FileFormat whith this UploadedFile!", 1);
		$classnames = [];
		$fileClasses = $this->container->get(serviceEntities::class)->getSubentities(EntityFile::class);
		// echo('<pre><h3>Search by FileFormat</h3>'); var_dump($fileFormat->getMediaType()); echo('</pre>');
		// echo('<pre><h3>In sub File classes</h3>'); var_dump($fileClasses); echo('</pre>');
		foreach ($fileClasses as $class) {
			if(in_array($fileFormat->getMediaType(), $class::getAuthorizedTypes())) $classnames[$class] = $class;
		}
		// echo('<pre><h3>Found valid classes</h3>'); var_dump($classnames); echo('</pre>'); die('OK');
		return $classnames;
	}


}









