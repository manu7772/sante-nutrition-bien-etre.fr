<?php
namespace ModelApi\BinaryBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Service\serviceEntities;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;
// FileBundle
// use ModelApi\FileBundle\Entity\Item;
// use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Service\FileManager;

// use \DateTime;
use \ReflectionClass;

class serviceBinary implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Binary::class;

	private $container;
	private $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
	}

	// /**
	//  * Get shortname of service
	//  * @return string
	//  */
	// public function __toString() {
	// 	$class = new ReflectionClass(static::class);
	// 	return $class->getShortName();
	// }

	// public function getEntityManager() {
	// 	return $this->serviceEntities->getEntityManager();
	// }

	// public function getRepository() {
	// 	return $this->serviceEntities->getRepository(static::ENTITY_CLASS);
	// }


	// public function getModel($classname = null) {
	// 	if(null === $classname) $classname = static::ENTITY_CLASS;
	// 	return $this->container->get(serviceEntities::class)->getModel($classname);
	// }

	// public function createNew($options = [], $closure = null) {
	// 	$RC = new ReflectionClass(static::ENTITY_CLASS);
	// 	if($RC->isInstantiable()) return $this->serviceEntities->createNew(static::ENTITY_CLASS, $options, $closure);
	// 	throw new Exception("Error ".__METHOD__."(): Entity ".Item::class." is not instantiable!", 1);
	// }

	// public function delete($entity, $flush = true, $forceDelete = false) {
	// 	$entityManager = $this->serviceEntities->getEntityManager();
	// 	if($entity->isInstance(['BaseEntity']) && !$forceDelete) $entity->setSoftdeleted();
	// 		else $entityManager->remove($entity);
	// 	if($flush) $entityManager->flush();
	// 	return $this;
	// }


}