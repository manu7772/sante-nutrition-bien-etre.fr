<?php
namespace ModelApi\BinaryBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use ModelApi\AnnotBundle\Annotation as Annot;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
// use Imagine\Imagick\Imagine as Imagine_Imagick;
// use Imagine\Gd\Imagine as Imagine_Gd;
use Imagine\Imagick\Image as Image_Imagick;
use Imagine\Gd\Image as Image_Gd;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Fileformat;
// BinaryBundle
use ModelApi\BinaryBundle\Service\serviceImages;
use ModelApi\BinaryBundle\Service\serviceBinaryFile;
use ModelApi\BinaryBundle\Entity\UploadInterface;
// use ModelApi\BinaryBundle\Entity\Thumbnail;

// https://symfony.com/doc/3.4/components/filesystem.html
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use ModelApi\DevprintsBundle\Service\DevTerminal;

// use \SplFileInfo;
use \pathinfo;
use \Exception;

/**
 * Note that all classes using this trait must implements UploadInterface!!!
 */
trait BinaryFile {

	/**
	 * @var string
	 * @ORM\Column(name="binaryFile", type="blob", nullable=false)
	 */
	protected $binaryFile;

	/**
	 * @var string
	 * @ORM\Column(name="meta_thumbnail", type="blob", nullable=true)
	 */
	protected $meta_thumbnail;

	/**
	 * @var array
	 * @ORM\Column(name="meta_data", type="json_array", nullable=true)
	 */
	protected $meta_data;

	/**
	 * @var string
	 * @ORM\Column(name="fileName", type="string", nullable=true)
	 * @CRUDS\Show(role="ROLE_USER", contexts={"all"}, order=20)
	 * @CRUDS\Show(role="ROLE_USER", contexts={"list"}, order=20)
	 */
	protected $fileName;

	/**
	 * @var string
	 * @ORM\Column(name="originalFileName", type="string", nullable=true)
	 */
	protected $originalFileName;

	/**
	 * @var string
	 * @ORM\Column(name="fileExtension", type="string", length=4, nullable=true)
	 */
	protected $fileExtension;

	/**
	 * @var int
	 * @ORM\Column(name="fileSize", type="integer", length=10, nullable=true)
	 * @CRUDS\Show(role="ROLE_USER", contexts={"list"}, order=20, type="filesize")
	 */
	protected $fileSize;

	/**
	 * @var array
	 * @ORM\Column(name="fileResolutions", type="json_array", nullable=true)
	 */
	protected $fileResolutions;

	/**
	 * @var string
	 * @ORM\Column(name="fileUrl", type="text")
	 */
	protected $fileUrl;

	/**
	 * @var array
	 * @ORM\Column(name="fileUrls", type="json_array")
	 */
	protected $fileUrls;

	/**
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=150,
	 * 		options={"required"=true, "attr"={"accept" = "auto"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=150,
	 * 		options={"required"=false, "attr"={"accept" = "auto"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=200)
	 */
	protected $uploadFile;
	protected $remove_file = false;
	// public $stream_type_while_upload = null;
	public $flattenForCompute = null;
	protected $streamTypeWhileUpload = null;

	// public $fixturesContext = false;

	/**
	 * @var int
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Fileformat")
	 * @ORM\JoinColumn(nullable=false)
	 * !!!@Annot\AttachFileformat(attribute="stream_type_while_upload", setter="setFileformat")
	 */
	protected $fileformat;

	/**
	 * @var array
	 * @ORM\Column(name="typeparameters", type="json_array", nullable=false)
	 */
	protected $typeparameters;

	/**
	 * Valid content-types
	 * !!!@CRUDS\Create(
	 * 		type="HiddenType",
	 * 		show=true,
	 * 		update=false,
	 * 		order=0,
	 * 		options={"required"=true, "disabled"=true}
	 * 	)
	 * !!!@CRUDS\Update(
	 * 		type="HiddenType",
	 * 		show=true,
	 * 		update=false,
	 * 		order=0,
	 * 		options={"required"=true, "disabled"=true}
	 * 	)
	 * @CRUDS\Show(role=false, order=0)
	 * @Annot\AttachValidContentTypes
	 */
	protected $validContentTypes;

	protected $checkFilesReport;
	// protected $tempfileUrl;


	/**
	 * @Annot\PostCreate()
	 */
	public function init_BinaryFile() {
		// $this->controlImplementsUploadInterface();
		$this->uploadFile ??= null;
		$this->binaryFile ??= null;
		$this->meta_thumbnail ??= null;
		$this->meta_data ??= [];
		$this->fileName ??= null;
		$this->originalFileName ??= null;
		$this->fileExtension ??= null;
		$this->fileSize ??= null;
		$this->fileResolutions ??= null;
		$this->fileUrls ??= [];
		$this->fileUrl ??= null;
		$this->fileformat ??= null;
		$this->flattenForCompute ??= null;
		$this->checkFilesReport ??= null;
		// $this->tempfileUrl ??= null;
		$this->validContentTypes ??= null;
		$this->streamTypeWhileUpload ??= null;
		// declinations (for image type)
		$this->initTypeparameters();
	}



	public function getValidContentTypes($asArray = false) {
		return $asArray ? preg_split("/[\s,]+/", $this->validContentTypes) : $this->validContentTypes;
	}

	public function setValidContentTypes($validContentTypes) {
		$this->validContentTypes = $validContentTypes;
		return $this;
	}

	/*************************************************************************************************/
	/*** CROP PARAMETERS
	/*************************************************************************************************/

	public function setCroppable($croppable = true) {
		$this->typeparameters['cropp_info']['croppable'] = $croppable;
		return $this;
	}

	public function isCroppable() {
		return $this->typeparameters['cropp_info']['croppable'];
	}

	public function getCroppable() {
		return $this->typeparameters['cropp_info']['croppable'];
	}

	public function setCroppdata($croppdata) {
		$this->typeparameters['cropp_info']['croppdata'] = $croppdata;
		return $this;
	}

	public function addCroppdata($name, $croppdata) {
		$this->typeparameters['cropp_info']['croppdata'][$name] = $croppdata;
		return $this;
	}

	public function removeCroppdata($name) {
		// if(!isset($this->typeparameters['cropp_info']['croppdata'][$name])) return $this;
		$this->typeparameters['cropp_info']['croppdata'] = array_filter($this->typeparameters['cropp_info']['croppdata'], function ($key) use ($name) {
			return $name !== $key;
		}, ARRAY_FILTER_USE_KEY);
		return $this;
	}

	public function getCroppdata($name = null) {
		if(null === $name) return $this->typeparameters['cropp_info']['croppdata'];
		if(!isset($this->typeparameters['cropp_info']['croppdata'][$name])) throw new Exception("Error Croppdata has no property ".json_encode($name)."!", 1);
		return $this->typeparameters['cropp_info']['croppdata'][$name];
		
	}




	/*************************************************************************************************/
	/*** TYPEPARAMETERS
	/*************************************************************************************************/

	public function getTypeparameters() {
		return $this->typeparameters;
	}

	protected function setTypeparameters($typeparameters) {
		$this->typeparameters = $typeparameters;
		return $this;
	}

	public function initTypeparameters() {
		if(!is_array($this->typeparameters)) $this->typeparameters = [];
		// common typeparameters
		$this->typeparameters['declinations'] = [];
		if(!isset($this->typeparameters['base_declinations']) || !is_array($this->typeparameters['base_declinations'])) $this->typeparameters['base_declinations'] = [];
		$this->typeparameters['cropp_info'] = ['croppable' => false, 'croppdata' => []];
		// $this->setDefaultDeclinations(false);
		// // by type
		// switch ($this->getMediaType()) {
		// 	case 'image':
		// 		$this->typeparameters['cropp_info'] = ['croppable' => true, 'croppdata' => []];
		// 		break;
		// 	default:
		// 		// 
		// 		break;
		// }
		return $this;
	}

	/*************************************************************************************************/
	/*** BASE DECLINATIONS
	/*************************************************************************************************/

	/**
	 * Define base declinations
	 * @param array $declinations
	 * @return BinaryFile
	 */
	public function defineBaseDeclinations($declinations) {
		$mediaType = $this->getMediaType();
		$subType = $this->getSubtype();
		$contentType = $this->getContentType();
		$media_declinations = isset($declinations[$mediaType]) ? $declinations[$mediaType] : $declinations[serviceBinaryFile::DEFAULT_DECLINATION_NAME];
		$base_declinations = isset($media_declinations[$contentType]) ? $media_declinations[$contentType] : $media_declinations[serviceBinaryFile::DEFAULT_DECLINATION_NAME];
		if(!is_array($base_declinations)) throw new Exception("Error: ".json_encode($this->__toString())." has no media declinations!", 1);
		$this->typeparameters['base_declinations'] = $base_declinations;
		// echo('<pre>');
		// 	echo('<h2>Media: '.$mediaType.' / ContentType: '.$contentType.' / SubType: '.$subType.'</h2>');
		// 	echo('<h5><u>StreamType:</u></h5>');
		// 	var_dump($this->getStreamTypeWhileUpload(true));
		// 	echo('<h5><u>Has uploadFile:</u></h5>');
		// 	echo('<p>'.($this->getUploadFile() instanceOf UploadedFile ? '<span style="color:green;">YES -> '.$this->getFileSize().'bytes</span>' : '<span style="color:red;">NO</span>').'</p>');
		// 	echo('<h5><u>Base declinations:</u></h5>');
		// 	var_dump($base_declinations);
		// die('</pre>');
		return $this;
	}

	// public function setBase_declinations($declinations) {
	// 	$this->typeparameters['base_declinations'] = $declinations;
	// 	// DevTerminal::Info('      --> Set base declinations: '.json_encode($this->getBase_declinations_names()));
	// 	return $this;
	// }

	/**
	 * Get base declinations
	 * @return array
	 */
	public function getBase_declinations() {
		return isset($this->typeparameters['base_declinations']) ? $this->typeparameters['base_declinations'] : [];
	}

	// /**
	//  * Get base declinations names
	//  * @return array
	//  */
	// public function getBase_declinations_names() {
	// 	$base_declinations = $this->getBase_declinations();
	// 	return array_keys($base_declinations);
	// }

	/**
	 * Has declinations
	 * @return boolean
	 */
	public function has_declinations() {
		return count($this->getBase_declinations()) > 0;
	}

	/**
	 * Has named declination
	 * @param string $name
	 * @return boolean
	 */
	public function has_declination($name) {
		$base_declinations = $this->getBase_declinations();
		return array_key_exists($name, $base_declinations);
	}

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		$base_declinations = $this->getBase_declinations();
		$base_declinations = array_filter($base_declinations, function($values) { return $values['default']; });
		return array_keys($base_declinations);
	}

	public function getRequiredDeclinationsNames() {
		$base_declinations = $this->getBase_declinations();
		$base_declinations = array_filter($base_declinations, function($values) { return $values['required']; });
		return array_keys($base_declinations);
	}

	/*************************************************************************************************/
	/*** ENTITY DECLINATIONS
	/*************************************************************************************************/

	/**
	 * Declination exists
	 * @param string $name
	 * @return boolean
	 */
	public function declination_exists($name) {
		return array_key_exists($name, $this->typeparameters['declinations']);
	}

	// /**
	//  * Set declinations
	//  * @param array $declinations
	//  * @return Entity
	//  */
	// public function setDeclinations($declinations) {
	// 	$this->typeparameters['declinations'] = [];
	// 	$this->addDeclinations($declinations);
	// 	$this->keepRequiredDeclinations();
	// 	return $this;
	// }

	// /**
	//  * Set declinations by baseDeclinations names
	//  * @param array $base_declinations_names
	//  * @return Entity
	//  */
	// public function setDeclinationsByBaseDeclinations_names($base_declinations_names) {
	// 	$this->typeparameters['declinations'] = [];
	// 	$this->addDeclinationsByBaseDeclinations_names($base_declinations_names);
	// 	$this->keepRequiredDeclinations();
	// 	return $this;
	// }

	// /**
	//  * Add declinations by baseDeclinations names
	//  * @param array $base_declinations_names
	//  * @return Entity
	//  */
	// public function addDeclinationsByBaseDeclinations_names($base_declinations_names) {
	// 	$base_declinations = $this->getBase_declinations();
	// 	$declinations = array_filter($base_declinations, function($key_name) use ($base_declinations_names) {
	// 		return in_array($key_name, $base_declinations_names);
	// 	}, ARRAY_FILTER_USE_KEY);
	// 	$this->addDeclinations($declinations);
	// 	$this->keepRequiredDeclinations();
	// 	return $this;
	// }

	protected function keepRequiredDeclinations() {
		$requireds = $this->getRequiredDeclinationsNames();
		$base_declinations = $this->getBase_declinations();
		// Original is always required
		// $this->typeparameters['declinations']['original'] = $base_declinations['original'];
		// Others...
		foreach ($requireds as $required) {
			if(!array_key_exists($required, $this->typeparameters['declinations'])) $this->typeparameters['declinations'][$required] = $base_declinations[$required];
		}
		// Add realsize if media not unique image (GIF, PDF, video types, etc.)
		if($this->needRealsize() && isset($base_declinations['realsize'])) {
			$this->typeparameters['declinations']['realsize'] = $base_declinations['realsize'];
		}
		return $this;
	}

	public function needRealsize() {
		if($this->getMediaType() === 'video') return true;
		if($this->getFileformat() instanceOf Fileformat) {
			switch ($this->getFileformat()->getContentType()) {
				case 'image/gif':
					// GIF : verify if multi image gif
					// https://stackoverflow.com/questions/12551646/how-to-extract-frames-of-an-animated-gif-with-php
					// https://en.wikipedia.org/wiki/GIF#File_format
					return true;
					// $stream = $this->getBinaryFile();
					// if(null === $stream) return false;
					// $images = explode("\x00\x21\xF9\x04", $stream);
					// return count($images) > 1;
					break;
				case 'application/pdf':
					return true;
					break;
			}
		}
		return false;
	}

	/**
	 * Get declinations
	 * @return array
	 */
	public function getDeclinations() {
		$this->keepRequiredDeclinations();
		return $this->typeparameters['declinations'];
	}

	/**
	 * Set default declinations
	 * @param boolean $onlyIfEmpty = true
	 * @param array $declinations_names = []
	 * @return Entity
	 */
	public function setDefaultDeclinations($onlyIfEmpty = true, $declinations_names = []) {
		$base_declinations = $this->getBase_declinations();
		$decl = $this->getDeclinations();
		if(null === $decl || count($decl) <= 0 || false === $onlyIfEmpty) {
			if(count($declinations_names) <= 0) $declinations_names = $this->getDefaultDeclinationsNames();
			// DevTerminal::Info('     --> Set default declinations: '.json_encode($declinations_names));
			$declinations = array_filter($base_declinations, function($key_name) use ($declinations_names) {
				return in_array($key_name, $declinations_names);
			}, ARRAY_FILTER_USE_KEY);
			$this->addDeclinations($declinations);
		}
		$this->keepRequiredDeclinations();
		// $this->checkFileUrls(false);
		// $names = array_keys($this->typeparameters['declinations']);
		// DevTerminal::Info('     --> Setted default declinations: '.implode(' / ', $names));
		return $this;
	}

	// /**
	//  * Get declinations names
	//  * @return array
	//  */
	// public function getDeclinations_names() {
	// 	$this->keepRequiredDeclinations();
	// 	return array_keys($this->typeparameters['declinations']);
	// }

	/**
	 * Add declination
	 * @param array $declinations
	 * @return Entity
	 */
	public function addDeclinations($declinations) {
		foreach ($declinations as $name => $declination) {
			if(!is_string($name) || (!isset($declination['x']) && !empty($declination['x'])) || (!isset($declination['y']) && !empty($declination['y']))) throw new Exception("BynaryFile declination add error: invalid parameters: ".$name." => ".json_encode($declination)."! You have to define an array of [name => ['x' => integer >= 8 or null, 'y' => intege >= 8 or null, 'mode'(optional) => string('in'(as default), 'cut' or 'deform'), 'folder'(optional) => string(name as default)]].", 1);
			$declination['x'] = intval($declination['x']);
			if($declination['x'] <= 0) $declination['x'] = null;
				else if($declination['x'] < 8) $declination['x'] = 8;
			$declination['y'] = intval($declination['y']);
			if($declination['y'] <= 0) $declination['y'] = null;
				else if($declination['y'] < 8) $declination['y'] = 8;
			if(!isset($declination['folder']) || !is_string($declination['folder'])) $declination['folder'] = $name;
			if(!isset($declination['mode']) || !is_string($declination['mode'])) $declination['mode'] = 'in';
			// if(!in_array($declination['mode'], ['in','cut','deform','formated'])) $declination['mode'] = 'in';
			$this->typeparameters['declinations'][$name] = $declination;
		}
		$this->keepRequiredDeclinations();
		return $this;
	}

	// /**
	//  * Remove declination
	//  * @param string $name
	//  * @return Entity
	//  */
	// public function removeDeclination($name) {
	// 	$requireds = $this->getRequiredDeclinationsNames();
	// 	if($this->declination_exists($name)) {
	// 		$this->typeparameters['declinations'] = array_filter($this->typeparameters['declinations'], function($key_name) use ($name, $requireds) {
	// 			return $key_name !== $name && !in_array($name, $requireds);
	// 		}, ARRAY_FILTER_USE_KEY);
	// 	} else {
	// 		// throw new Exception("Can not remove declination ".json_encode($name)." because it does not exists in ".json_encode($this->getDeclinations_names())."!", 1);
	// 	}
	// 	$this->keepRequiredDeclinations();
	// 	return $this;
	// }



	/*************************************************************************************************/
	/*** TYPES & AUTHORIZATIONS
	/*************************************************************************************************/

	/**
	 * Get authorized types of media (image, pdf, video…)
	 * @return array
	 */
	public function getAuthorizedTypes() {
		return Fileformat::getTypes(); // ---> For all types authorized
	}

	/**
	 * Get authorized types of media (image, pdf, video…)
	 * @return array
	 */
	public function getAccept() {
		return $this->getAuthorizedTypes();
	}

	/**
	 * is authorized types of media (image, pdf, video…)
	 * @return boolean
	 */
	public function isAuthorizedType() {
		$authorizedTypes = $this->getAuthorizedTypes();
		return in_array($this->getMediaType(), $authorizedTypes) || in_array('*', $authorizedTypes) || null === $this->getUploadFile();
	}

	/**
	 * Get type of media (image, pdf, video…)
	 * @return string | null
	 */
	public function getMediaType() {
		if($this->getFileformat() instanceOf Fileformat) return $this->getFileformat()->getMediaType();
		$mediaType = is_string($this->getStreamTypeWhileUpload(true)) ? $this->getStreamTypeWhileUpload()['MimeType'] : null;
		return Fileformat::getMediaTypeByContentType($mediaType);
	}

	/**
	 * Get type of media (image, pdf, video…)
	 * @return string | null
	 */
	public function getSubtype() {
		if($this->getFileformat() instanceOf Fileformat) return $this->getFileformat()->getSubtype();
		$subType = is_string($this->getStreamTypeWhileUpload(true)) ? $this->getStreamTypeWhileUpload()['MimeType'] : null;
		return Fileformat::getSubTypeByContentType($subType);
	}

	/**
	 * Is image type
	 * @return boolean
	 */
	public function isImage() {
		return $this->getMediaType() === 'image';
	}

	/**
	 * Is pdf type
	 * @return boolean
	 */
	public function isPdf() {
		return $this->getMediaType() === 'pdf';
	}

	/**
	 * Is video type
	 * @return boolean
	 */
	public function isVideo() {
		return $this->getMediaType() === 'video';
	}

	/**
	 * Is audio type
	 * @return boolean
	 */
	public function isAudio() {
		return $this->getMediaType() === 'audio';
	}

	public function isScreenableAsImage() {
		return in_array($this->getMediaType(), ["image","pdf"]);
	}


	/**
	 * Set binaryFile
	 * @param string $binaryFile
	 * @return Entity
	 */
	public function setBinaryFile($binaryFile) {
		$this->binaryFile = $binaryFile;
		return $this;
	}

	/**
	 * Get binaryFile
	 * @return string | null
	 */
	public function getBinaryFile() {
		if(is_resource($this->binaryFile)) {
			rewind($this->binaryFile);
			// if($this->getSubtype() === 'text') {
			// 	$text = stream_get_contents($this->binaryFile);
			// 	return htmlspecialchars($text);
			// }
			return stream_get_contents($this->binaryFile);
		}
		if(is_string($this->binaryFile)) {
			return $this->binaryFile;
		}
		return null;
	}

	/**
	 * Get required files url (ragarding to declinations)
	 * @return array <string>
	 */
	public function getRequiredFileUrls() {
		$binaryFile = $this->getBinaryFile();
		if(empty($binaryFile)) return [];
		$fileUrls = array();
		$fileSystem = new Filesystem();
		$declinations = $this->getDeclinations();
		$stream = $this->getFlattenForCompute($binaryFile);
		// echo('<pre>');var_dump($declinations);die('</pre>');
		if(null === $stream) {
			// Files without declinations : only original
			$fileUrls['original'] = static::FILES_FOLDER.DIRECTORY_SEPARATOR.$this->getMediaType().DIRECTORY_SEPARATOR.$declinations['original']['folder'].DIRECTORY_SEPARATOR.$this->getExtensionFileName();
		} else {
			$size = $stream->getSize();
			foreach ($declinations as $name => $values) {
				$sizes = serviceImages::getResizeSizes($size, $values['x'], $values['y']);
				$extension = isset($values['format']) && is_string($values['format']) ? $values['format'] : $this->getFileExtension();
				if(serviceImages::ok_for_generation($stream, $values['x'], $values['y'], $values['discard_smaller'])) {
					$fileUrls[$name] = static::FILES_FOLDER.DIRECTORY_SEPARATOR.$this->getMediaType().DIRECTORY_SEPARATOR.$values['folder'].DIRECTORY_SEPARATOR.$this->getFileName().'.'.$extension;
				}
			}
		}
		unset($fileSystem);
		return $fileUrls;
	}

	public function getCheckFilesReport() {
		$this->checkFileUrls(false);
		return $this->checkFilesReport;
	}

	/**
	 * Check files url
	 * @param boolean $reset = false
	 * @return boolean (true if has changed)
	 */
	public function checkFileUrls($reset = false) {
		$this->checkFilesReport = [];
		$memo = json_encode($this->fileUrls);
		$this->fileUrls = array();
		$fileSystem = new Filesystem();
		if(true === $reset) $this->writeAllFiles();
		// foreach ($this->getDeclinations() as $name => $values) {
		// 	$file = $this->getFilesPath($values['folder'], true, true).$this->getExtensionFileName();
		// 	if($fileSystem->exists($this->getWebPath().$file)) $this->fileUrls[$name] = $file;
		// }
		$pathWeb = $this->getWebPath();
		foreach ($this->getRequiredFileUrls() as $name => $url) {
			$this->checkFilesReport[$name] = false;
			if($fileSystem->exists($pathWeb.$url)) {
				$this->fileUrls[$name] = $url;
				$this->checkFilesReport[$name] = $url;
			}
		}
		unset($fileSystem);
		if(isset($this->fileUrls['original'])) $this->fileUrl = $this->fileUrls['original'];
		return json_encode($this->fileUrls) !== $memo;
	}

	/**
	 * Get file URL
	 * @param boolean $check = false
	 * @return string | null
	 */
	public function getFileUrl($check = false) {
		if($check) $this->checkFileUrls(false);
		return $this->fileUrl;
	}

	/**
	 * Get file URLs
	 * @param boolean $check = false
	 * @return array <string>
	 */
	public function getFileUrls($check = false) {
		if($check) $this->checkFileUrls(false);
		return $this->fileUrls;
	}

	// public function getTempfileUrl() {
		// return null;
	// }

	/**
	 * Retourne le code en Base64 du fichier / null si aucun
	 * @return string | null
	 */
	public function getB64File($includePrefix = false) {
		$includePrefix = true === $includePrefix ? 'data:'.$this->getFileformat()->getContentType().';base64, ' : '';
		// $includePrefix = true === $includePrefix ? 'data:image/'.strtolower($this->getFileExtension()).';base64, ' : '';
		// $includePrefix = true === $includePrefix ? 'data:image/png;base64, ' : '';
		$binaryFile = $this->getBinaryFile();
		return null !== $binaryFile ? $includePrefix.base64_encode($binaryFile) : null;
	}

	/**
	 * Set file name / set extension if exists
	 * @param string $fileName = null
	 * @return Entity
	 */
	public function setFileName($fileName = null) {
		$this->fileName = $fileName;
		return $this;
	}

	/**
	 * Get file name
	 * @return string | null
	 */
	public function getFileName() {
		return $this->fileName;
	}

	/**
	 * Get file name with path and extension
	 * @return string | null
	 */
	public function getExtensionFileName() {
		return $this->getFileName().'.'.$this->getFileExtension();
	}

	/**
	 * Set original file name
	 * @param string $originalFileName = null
	 * @return Entity
	 */
	public function setOriginalFileName($originalFileName = null) {
		$this->originalFileName = $originalFileName;
		return $this;
	}

	/**
	 * Get original file name
	 * @return string | null
	 */
	public function getOriginalFileName() {
		return $this->originalFileName;
	}

	/**
	 * Set file extension
	 * @param string $fileExtension = null
	 * @return Entity
	 */
	public function setFileExtension($fileExtension = null) {
		$this->fileExtension = $fileExtension;
		return $this;
	}

	/**
	 * Get file extension
	 * @return string | null
	 */
	public function getFileExtension() {
		return $this->fileExtension;
	}

	/**
	 * Set file size
	 * @param integer $fileSize = null
	 * @return Entity
	 */
	public function setFileSize($fileSize = null) {
		$this->fileSize = $fileSize;
		return $this;
	}

	/**
	 * Get file size in o
	 * @return integer | null
	 */
	public function getFileSize() {
		return $this->fileSize;
	}

	/**
	 * Get file size in Ko
	 * @return integer | null
	 */
	public function getFileSizeKo() {
		return $this->fileSize / 1024;
	}

	/**
	 * Get file size in Mo
	 * @return integer | null
	 */
	public function getFileSizeMo() {
		return $this->fileSize / (1024 * 1024);
	}





	/*************************************************************************************************/
	/*** METADATA
	/*************************************************************************************************/

	public function setMeta_data($meta_data = null) {
		$this->meta_data = serviceTools::utf8ize($meta_data);
		return $this;
	}

	public function getMeta_data() {
		return $this->meta_data;
	}

	public function setMeta_thumbnail($meta_thumbnail = null) {
		$this->meta_thumbnail = null !== $meta_thumbnail ? json_encode(serviceTools::utf8ize($meta_thumbnail)) : null;
		return $this;
	}

	public function getMeta_thumbnail() {
		if(is_resource($this->meta_thumbnail)) {
			rewind($this->meta_thumbnail);
			$decod = stream_get_contents($this->meta_thumbnail);
			return null !== $decod ? json_decode($decod) : $decod;
		}
		return null;
	}



	/*************************************************************************************************/
	/*** UPLOAD FILE
	/*************************************************************************************************/

	public function getStreamTypeWhileUpload($asString = false) {
		if(!$this->hasStreamTypeWhileUpload()) return null;
		return $asString ?
			$this->streamTypeWhileUpload['MimeType'].'@'.$this->streamTypeWhileUpload['Extension']:
			$this->streamTypeWhileUpload;
	}

	public function hasStreamTypeWhileUpload() {
		// if(empty($this->streamTypeWhileUpload)) return false;
		if(is_array($this->streamTypeWhileUpload) && array_key_exists('MimeType', $this->streamTypeWhileUpload) && array_key_exists('Extension', $this->streamTypeWhileUpload)) return true;
		return false;
	}

	/**
	 * @return Entity
	 * --> listener : postPersist / postUpdate
	 */
	public function PostUploadedFile() {
		if(true === $this->remove_file) {
			// remove files
			$this->removeAllFiles();
		} else if($this->uploadFile instanceOf UploadedFile) {
			$this->writeAllFiles();
		}
		return $this;
	}

	/**
	 * Set uploadFile
	 * @see https://symfony.com/doc/3.4/controller/upload_file.html
	 * if is 
	 * @param UploadedFile | string(url) $uploadFile = null
	 * @return UploadInterface
	 */
	public function setUploadFile($uploadFile = null) {
		// if(is_string($uploadFile)) echo('<div>Upload file: '.$uploadFile.' '.(@file_exists($uploadFile) ? 'EXISTS' : 'DOES NOT EXIST').'!</div>');
		// 	else if(empty($uploadFile)) echo('<div>Upload file: EMPTY!</div>');
		// die();
		$this->uploadFile = serviceTools::getUploadFileInstance($uploadFile);
		// $this->computeUploadFile();
		if($this->uploadFile instanceOf UploadedFile) {
			// DevTerminal::Warning('     -------------------- GOT UploadedFile '.$this->uploadFile->getClientOriginalName().' / '.$this->uploadFile->getClientSize().' ---------------------');
			if(null !== $this->getId()) $this->removeAllFiles();
			// FILE EXTENSION
			// $extension = $path_info['extension'];
			// $this->setFileExtension($this->uploadFile->guessExtension());
			$this->setFileExtension($this->uploadFile->getClientOriginalExtension());
			if(null == $this->getFileExtension()) $this->setFileExtension($this->uploadFile->getExtension());
			// FILE SIZE
			$this->setFileSize($this->uploadFile->getClientSize());
			if(null == $this->getFileSize()) $this->setFileSize($this->uploadFile->getSize());
			// FILE NAME (SAFE)
			$safeFilename = serviceTools::getSafeFilename(pathinfo($this->uploadFile->getClientOriginalName(), PATHINFO_FILENAME));
			$this->setFileName($safeFilename.'@'.$this->getMicrotimeid());
			// $this->setFileName($safeFilename.'@'.$this->geUniquid());
			$this->setOriginalFileName($safeFilename.'.'.$this->getFileExtension());
			// $this->streamTypeWhileUpload = $this->uploadFile->getMimeType().'@'.$this->fileExtension;
			$this->streamTypeWhileUpload = ['MimeType' => $this->uploadFile->getMimeType(), 'Extension' => $this->getFileExtension()];
			$file_path = $this->uploadFile->getRealPath();
			$binaryFile = file_get_contents($file_path);
			if(empty($binaryFile)) throw new Exception("Error getting content of file ".json_encode($file_path).": no data found!", 1);
			$this->setBinaryFile($binaryFile);
			// Meta data
			$this->setMeta_data([]);
			try {
				$meta_data = exif_read_data($file_path);
				if($meta_data) $this->setMeta_data($meta_data);
			} catch (Exception $e) { }
			// Meta thumbnail
			$this->setMeta_thumbnail(null);
			try {
				$meta_thumbnail = exif_thumbnail($file_path, $width, $height, $type);
				if($meta_thumbnail) $this->setMeta_thumbnail($meta_thumbnail);
			} catch (Exception $e) { }
			// Define default type parameters
			$this->initTypeparameters();
		}
		// $this->fileUrls = $this->getRequiredFileUrls();
		// if(isset($this->fileUrls['original'])) $this->fileUrl = $this->fileUrls['original'];
		return $this;
	}

	/**
	 * Get uploadFile
	 * @return UploadedFile | null
	 */
	public function getUploadFile() {
		return $this->uploadFile;
	}


	// public function setResource($resource = null) {
	// 	if(null !== $resource) {
	// 		$resource = base64_decode($resource);
	// 	}
	// 	return $this;
	// }

	/**
	 * Get web path
	 * @return string
	 */
	public function getWebPath($endSlash = true, $relativePath = false) {
		$path = static::WEB_PATH;
		if($relativePath) $path = array_filter($path, function($key) { return $key > 0; }, ARRAY_FILTER_USE_KEY);
		$path2 = serviceTools::concatWithSeparators($path);
		// if($relativePath) {
		// 	$fileSystem = new Filesystem();
		// 	$path2 = $fileSystem->makePathRelative(reset($path), $path2);
		// 	unset($fileSystem);
		// }
		// DevTerminal::Warning('     - test getWebPath: '.$path2.(false === $endSlash ? '' : DIRECTORY_SEPARATOR));
		// DevTerminal::Loud('     ----> test with: '.implode(DIRECTORY_SEPARATOR, $path));
		return $path2.(false === $endSlash ? '' : DIRECTORY_SEPARATOR);
	}

	/**
	 * Get files path
	 * @return string
	 */
	public function getFilesPath($subDirs = null, $endSlash = true, $relativePath = false) {
		$path = static::WEB_PATH;
		$path[] = static::FILES_FOLDER;
		$path[] = $this->getMediaType();
		if(is_string($subDirs)) $subDirs = serviceTools::splitPath($subDirs);
		if(is_array($subDirs) && count($subDirs) > 0) foreach ($subDirs as $subdir) {
			if(is_string($subdir) && strlen($subdir) > 0) $path[] = $subdir;
		}
		if($relativePath) $path = array_filter($path, function($key) { return $key > 0; }, ARRAY_FILTER_USE_KEY);
		$path2 = serviceTools::concatWithSeparators($path);
		// if($relativePath) {
		// 	$fileSystem = new Filesystem();
		// 	$path2 = $fileSystem->makePathRelative(reset($path), $path2);
		// 	unset($fileSystem);
		// }
		// DevTerminal::Warning('     - test getFilesPath: '.$path2.(false === $endSlash ? '' : DIRECTORY_SEPARATOR));
		// DevTerminal::Loud('     ----> test with: '.implode(DIRECTORY_SEPARATOR, $path));
		// DevTerminal::Loud('     ----> test with: '.json_encode($subDirs));
		return $path2.(false === $endSlash ? '' : DIRECTORY_SEPARATOR);
	}

	/**
	 * Remove all files on hard disk
	 * @return boolean (true if has deleted almost one)
	 */
	protected function removeAllFiles() {
		serviceTools::removeFileEverywhere($this->getFilesPath(null, false, false), $this->getExtensionFileName());
		return $this;
	}

	protected function writeFolderFile($dir, $filename, $stream){
		$fileSystem = new Filesystem();
		$dir = $this->getFilesPath($dir, true, false);
		// $dir = realpath($dir).DIRECTORY_SEPARATOR;
		if(strlen($dir) < 5) {
			DevTerminal::Error('     --> ERROR: tryed to create files/directories at the wrong place! --> '.json_encode($dir));
			die();
		}
		serviceTools::createPath($dir);
		// $old = umask(0);
		if($stream instanceOf Image_Imagick || $stream instanceOf Image_Gd) {
			// Image given
			$options = ['flatten' => true];
			if(preg_match('#^(gif|png)$#i', $this->getFileExtension())) {
				$options['flatten'] = false;
				if(count($stream->layers()) > 1) $options['animated'] = true;
			}
			if(preg_match('#^jpe?g$#i', $this->getFileExtension())) $options['jpeg_quality'] = 70;
			$stream->save($dir.$filename, $options);
			// echo('<p>-- OPTIONS : '.json_encode($options).'</p>');
		} else {
			// string given
			$fileSystem->dumpFile($dir.$filename, $stream);
		}
		// $fileSystem->chmod($dir.$filename, serviceTools::PUBLIC_RIGHTS['chmod']);
		// $fileSystem->chgrp($dir.$filename, serviceTools::PUBLIC_RIGHTS['group']);
		// umask($old);
		unset($fileSystem);
		// DevTerminal::Info('     --> saved file: '.$dir.$filename);
		return $this;
	}

	protected function getFlattenForCompute($stream, $page = 0, $refresh = false) {
		if(null !== $this->flattenForCompute && !$refresh) return $this->flattenForCompute;
		$this->flattenForCompute = null;
		switch ($this->getMediaType()) {
			case 'image':
				$this->flattenForCompute = serviceImages::thumb_image($stream, null, null, null, false, true, 0);
				break;
			case 'pdf':
				if($this->uploadFile instanceOf UploadedFile) $path = $this->uploadFile->getRealPath();
					else $path = $this->getFileUrl();
				if(is_string($path)) $this->flattenForCompute = serviceImages::getImagineFromPdfFile($path, $page, static::PDF_FLATTEN_RESOLUTION);
					else $this->flattenForCompute = serviceImages::getImagineFromPdf($stream, $page, static::PDF_FLATTEN_RESOLUTION);
				break;
			default:
				// $this->flattenForCompute = serviceImages::thumb_image($stream, null, null, null, false, true, 0);
				break;
		}
		return $this->flattenForCompute;
	}

	/**
	 * Write all files on hard disk
	 * @return Entity
	 */
	protected function writeAllFiles() {
		$this->removeAllFiles();
		$stream = $this->getBinaryFile();
		if(null !== $stream) {
			$fileSystem = new Filesystem();

			// $names = array_keys($this->getDeclinations());
			// DevTerminal::Info('     --> write files of '.$this->getExtensionFileName().': '.implode(' / ', $names));
			// echo('<h1>File '.$this->getFileName().'</h1>');

			foreach ($this->getDeclinations() as $name => $values) {
				if(!is_array($this->fileResolutions)) $this->fileResolutions = [];
				// Write file
				// DevTerminal::Info('     --> Write file in "'.$name.'" with '.json_encode($values['x']).'x'.json_encode($values['y']));
				$extension = !isset($values['format']) || !is_string($values['format']) ? $this->getFileExtension() : $values['format'];
				switch ($this->getMediaType()) {
					case 'image':
						if(null != $values['x'] || null != $values['y'] || ($this->needRealsize() && $name === 'realsize') || $extension !== $this->getFileExtension()) {
							// Resize image
							$objectImage = serviceImages::thumb_image($stream, $values['x'], $values['y'], $values['mode'], $values['discard_smaller'], $values['flatten']);
							if(serviceImages::isImageInterface($objectImage)) {
								$this->writeFolderFile($values['folder'], $this->getFileName().'.'.$extension, $objectImage);
								$size = $objectImage->getSize();
								$this->fileResolutions[$name] = ['X' => $size->getHeight(), 'Y' => $size->getWidth(), 'values' => $values];
							}
						} else {
							// not resized
							$this->writeFolderFile($values['folder'], $this->getExtensionFileName(), $stream);
							$imagine = serviceImages::getStaticNewImagineInstance();
							$img2 = is_resource($stream) ? $imagine->read($stream) : $imagine->load($stream);
							$size = $img2->getSize();
							$this->fileResolutions[$name] = ['X' => $size->getHeight(), 'Y' => $size->getWidth(), 'values' => $values];
						}
						break;
					case 'pdf':
						// create image of one page
						if(!isset($flatten_imagine)) {
							// $flatten_imagine = serviceImages::getImagineFromPdf($stream, 0, static::PDF_FLATTEN_RESOLUTION);
							$flatten_imagine = $this->getFlattenForCompute($stream);
							// echo('<p>Generated flatten image: '.(!is_object($flatten_imagine) ? gettype($flatten_imagine) : get_class($flatten_imagine)).'</p>');
							// echo('<img src="data:image/png;base64, '.base64_encode($flatten_imagine->__toString()).'" height="300">');
							$ss = $flatten_imagine->getSize();
							// echo('<p>Flatten PDF size: '.$ss->getWidth().'x'.$ss->getHeight().'px</p>');
						}
						if(!$values['flatten']) {
							// Original PDF file
							$this->writeFolderFile($values['folder'], $this->getExtensionFileName(), $stream);
							$size = $flatten_imagine->getSize();
							$this->fileResolutions[$name] = ['X' => $size->getHeight(), 'Y' => $size->getWidth(), 'values' => $values];
							// echo('<p>'.$extension.' ---> '.$values['folder'].DIRECTORY_SEPARATOR.$this->getExtensionFileName());
							// echo(' <strong>Original file</strong> '.$size->getHeight().'x'.$size->getWidth().'px');
						} else {
							// image representations
							// if(null != $values['x'] || null != $values['y'] || ($this->needRealsize() && $name === 'realsize')) {
								// Resize image
								$objectImage = serviceImages::thumb_image($flatten_imagine, $values['x'], $values['y'], $values['mode'], $values['discard_smaller'], $values['flatten'], 0);
								if(serviceImages::isImageInterface($objectImage)) {
									$this->writeFolderFile($values['folder'], $this->getFileName().'.'.static::PDF_FLATTEN_FORMAT, $objectImage);
									$size = $objectImage->getSize();
									$this->fileResolutions[$name] = ['X' => $size->getHeight(), 'Y' => $size->getWidth(), 'values' => $values];
									// echo(' <strong>Resized file</strong> '.$size->getHeight().'x'.$size->getWidth().'px');
								}
							// } else {
							// 	$objectImage = serviceImages::thumb_image($flatten_imagine, $values['x'], $values['y'], $values['mode'], $values['discard_smaller'], $values['flatten']);
							// 	if(serviceImages::isImageInterface($objectImage)) {
							// 		$this->writeFolderFile($values['folder'], $this->getFileName().'.'.$extension, $objectImage);
							// 		$size = $objectImage->getSize();
							// 		$this->fileResolutions[$name] = ['X' => $size->getHeight(), 'Y' => $size->getWidth(), 'values' => $values];
							// 	}
							// }
						}
						// echo('</p>');
						break;
					default:
						$this->writeFolderFile($values['folder'], $this->getExtensionFileName(), $stream);
						$this->fileResolutions[$name] = ['X' => null, 'Y' => null, 'values' => $values];
						break;
				}
			}
		}
		// // echo('<h2>--- Declinations</h2>');
		// // echo('<pre>');var_dump($this->getDeclinations());echo('</pre>');
		// echo('<h2>--- Before check</h2>');
		// // echo('<pre>');var_dump($this->getFileUrl());echo('</pre>');
		// echo('<pre>');var_dump($this->getFileUrls());echo('</pre>');
		// echo('<pre>');var_dump($this->fileResolutions);echo('</pre>');
		// $this->checkFileUrls(false);
		// echo('<h2>--- After check</h2>');
		// // echo('<pre>');var_dump($this->getFileUrl());echo('</pre>');
		// echo('<pre>');var_dump($this->getFileUrls());echo('</pre>');
		// echo('<pre>');var_dump($this->fileResolutions);echo('</pre>');
		// die('<h2>--- Media type: '.$this->getSubtype().' > '.$this->getMediaType().'</p>');
		return $this;
	}

	/**
	 * Remove file
	 * --> listener : postRemove
	 * @return Entity
	 */
	public function removeAll() {
		$this->removeAllFiles();
		// $this->init_BinaryFile();
		return $this;
	}

	/**
	 * Is authorized file format
	 * @return boolean (true if no BinaryFile)
	 */
	public function isAuthorizedFormat() {
		if(null === $this->getFileformat()) return true;
		return $this->getFileformat()->getEnabled();
	}

	/**
	 * Set file format
	 * @param Fileformat $fileformat = null
	 * @return Entity
	 */
	public function setFileformat(Fileformat $fileformat = null) {
		if($this->fileformat !== $fileformat) {
			$this->fileformat = $fileformat;
			if($this->fileformat instanceOf Fileformat) {
				$this->setFileExtension($this->fileformat->getExtension());
				$this->initTypeparameters();
				$this->setDefaultDeclinations(false);
				$this->fileUrls = $this->getRequiredFileUrls();
				if(isset($this->fileUrls['original'])) $this->fileUrl = $this->fileUrls['original'];
			} else {
				$this->setFileExtension(null);
				$this->removeAllFiles();
			}
		}
		return $this;
	}

	/**
	 * Get content-type
	 * @return string
	 */
	public function getContentType() {
		$default = is_string($this->getStreamTypeWhileUpload(true)) ? $this->getStreamTypeWhileUpload()['MimeType'] : null;
		return $this->getFileformat() instanceOf Fileformat ? $this->getFileformat()->getContentType() : $default;
	}

	/**
	 * Get file format
	 * @return Fileformat | null
	 */
	public function getFileformat() {
		return $this->fileformat;
	}




}