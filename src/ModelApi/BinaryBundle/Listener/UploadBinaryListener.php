<?php
namespace ModelApi\BinaryBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
// use Symfony\Component\HttpKernel\Event\GetResponseEvent;
// use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
// use Symfony\Component\HttpFoundation\Request;
// AnnotBundle
// use ModelApi\AnnotBundle\Annotation as Annot;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// use ModelApi\BaseBundle\Service\serviceKernel;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\UploadInterface;
use ModelApi\BinaryBundle\Service\serviceBinaryFile;
// use ModelApi\BinaryBundle\Service\serviceImages;

use ModelApi\DevprintsBundle\Service\DevTerminal;

// https://symfony.com/doc/3.4/components/filesystem.html
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use \Exception;

class UploadBinaryListener {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	// protected $serviceKernel;
	// protected $Filesystem;
	protected $serviceBinaryFile;
	protected $update_bynarycontent;
	// protected $serviceImages;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		// $this->serviceKernel = $this->container->get(serviceKernel::class);
		$this->serviceBinaryFile = $this->container->get(serviceBinaryFile::class);
		$this->update_bynarycontent = $this->serviceBinaryFile->getServiceParameter('update_bynarycontent');
		// $this->serviceImages = $this->container->get(serviceImages::class);
		// $this->Filesystem = new Filesystem();
		return $this;
	}

	/**
	 * @ORM\PostLoad()
	 */
	public function postLoad(UploadInterface $entity, LifecycleEventArgs $event) {
		// $this->serviceBinaryFile->setBaseDeclinations($entity, false);
		// $entity->checkFileUrls(false);
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 */
	public function prePersist(UploadInterface $entity, LifecycleEventArgs $event) {
		$this->serviceBinaryFile->setBaseDeclinations($entity, false);

		// if(!empty($entity->getUploadinfo())) {
		// 	$uploadinfo = $entity->getUploadinfo();
		// 	$serviceEntities = $this->container->get(serviceEntities::class);
		// 	if($serviceEntities->isValidBddid($uploadinfo, false)) {
		// 		$binary = $serviceEntities->findByBddid($uploadinfo);
		// 		if($binary instanceOf Binary) $entity->setUploadFile($binary->getFileUrl(false));
		// 	}
		// }

		$this->serviceBinaryFile->AttachFileformat($entity);
		if($entity instanceOf Fileversion) {
			$entity->getFile()->checkVersion();
		}
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdate(UploadInterface $entity, PreUpdateEventArgs $event) {
		// $this->serviceBinaryFile->setBaseDeclinations($entity, false);
		// $entity->checkFileUrls(false);
		// if can change binary content on update:
		if($this->update_bynarycontent) {

			// if(!empty($entity->getUploadinfo())) {
			// 	$uploadinfo = $entity->getUploadinfo();
			// 	$serviceEntities = $this->container->get(serviceEntities::class);
			// 	if($serviceEntities->isValidBddid($uploadinfo, false)) {
			// 		$binary = $serviceEntities->findByBddid($uploadinfo);
			// 		if($binary instanceOf Binary) $entity->setUploadFile($binary->getFileUrl(false));
			// 	}
			// }

			$this->serviceBinaryFile->AttachFileformat($entity);
			if($entity instanceOf Fileversion) {
				$entity->getFile()->checkVersion();
			}
		}
		return $this;
	}

	/**
	 * @ORM\PostPersist()
	 */
	public function postPersist(UploadInterface $entity, LifecycleEventArgs $event) {
		$entity->PostUploadedFile();
		return $this;
	}

	/**
	 * @ORM\PostUpdate()
	 */
	public function postUpdate(UploadInterface $entity, LifecycleEventArgs $event) {

		// if can change binary content on update:
		if($this->update_bynarycontent) {
			$entity->PostUploadedFile();
		}
		return $this;
	}

	/**
	 * @ORM\PostRemove()
	 */
	public function postRemove(UploadInterface $entity, LifecycleEventArgs $event) {
		$entity->removeAll();
		return $this;
	}

	// /**
	//  * @ORM\PostFlush()
	//  */
	// public function postFlush(UploadInterface $entity, PostFlushEventArgs $args) {
	// 	$entity->checkFileUrls(true);
	// 	return $this;
	// }





}