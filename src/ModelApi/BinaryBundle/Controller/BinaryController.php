<?php

namespace ModelApi\BinaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;

use ModelApi\BinaryBundle\Entity\Binary;

class BinaryController extends Controller {

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get Binarys",
	 *    output = { "class" = Binary::class, "collection" = true, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base"})
	 * @Rest\Get("/binary", name="modelapibinary-getbinarys")
	 */
	public function getBinarysAction(Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$binarys = $em->getRepository(Binary::class)->findAll();
		return $binarys;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get Binarys",
	 *    output = { "class" = Binary::class, "collection" = false, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","Binary"})
	 * @Rest\Get("/binary/{id}", name="modelapibinary-getbinary")
	 */
	public function getBinaryAction($id, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$binary = $em->getRepository(Binary::class)->find($id);
		return $binary;
	}





}
