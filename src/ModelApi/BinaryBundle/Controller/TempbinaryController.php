<?php

namespace ModelApi\BinaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceSysfiles;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Tempbinary;
use ModelApi\BinaryBundle\Service\serviceTempbinary;

use \Exception;

class TempbinaryController extends Controller {

	const TMP_PATH = [__DIR__, '..', '..', '..', '..', 'web', 'tmp'];

	// /**
	//  * @ApiDoc(
	//  *    resource=true,
	//  *    section="File",
	//  *    description="Get Tempbinarys",
	//  *    output = { "class" = Tempbinary::class, "collection" = true, "groups" = {"File"} },
	//  *    views = { "default", "file" }
	//  * )
	//  * @Rest\View(serializerGroups={"base"})
	//  * @Rest\Get("/tempbinary", name="modelapitempbinary-gettempbinarys")
	//  */
	// public function getTempbinarysAction(Request $request) {
	// 	$em = $this->getDoctrine()->getEntityManager();
	// 	$tempbinarys = $em->getRepository(Tempbinary::class)->findAll();
	// 	return $tempbinarys;
	// }

	// /**
	//  * @ApiDoc(
	//  *    resource=true,
	//  *    section="File",
	//  *    description="Get Tempbinarys",
	//  *    output = { "class" = Tempbinary::class, "collection" = false, "groups" = {"File"} },
	//  *    views = { "default", "file" }
	//  * )
	//  * @Rest\View(serializerGroups={"base","Tempbinary"})
	//  * @Rest\Get("/tempbinary/{id}", name="modelapitempbinary-gettempbinary")
	//  */
	// public function getTempbinaryAction($id, Request $request) {
	// 	$em = $this->getDoctrine()->getEntityManager();
	// 	$tempbinary = $em->getRepository(Tempbinary::class)->find($id);
	// 	return $tempbinary;
	// }

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Upload binary file",
	 *    output = { "class" = Tempbinary::class, "collection" = false, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base"})
	 * @Rest\Post("/upload/binary", name="upload_binary")
	 */
	public function uploadBinaryAction(Request $request) {
		$data = $request->request->all();
		// $data = $request->request->get('data');
		// $data = json_decode($data, true);
		// throw new Exception("Test data file: ".$data['file'].".", 1);
		$content = explode('base64', $data['file']);
		$content = base64_decode(end($content));

		$tempbinary = $this->get(serviceTempbinary::class)->createNew();
		if($tempbinary instanceOf Tempbinary) {
			$filepath = serviceSysfiles::createFile(serviceSysfiles::concatWithSeparators(static::TMP_PATH), $data['name'], $content, true);
			// $uploadFile = serviceSysfiles::getUploadFileInstance($data['file'], $data['name']);
			$file = new File($filepath);
			$uploadFile = new UploadedFile($filepath, $file->getFilename());
			// if($uploadFile instanceOf UploadedFile) {
				$tempbinary->setOriginalFileName($data['name']);
				// $tempbinary->setOriginalFileName(serviceTools::getSafeFilename($data['name']));
				$tempbinary->setUploadFile($uploadFile);
				$em = $this->getDoctrine()->getEntityManager();
				$em->persist($tempbinary);
				$em->flush();
			// } else {
				// throw new Exception("Could not generate binary file!", 1);
			// }
		} else {
			throw new Exception("Could not create binary file!", 1);
		}
		return $tempbinary;
	}





}
