<?php
namespace ModelApi\BinaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;

use \Exception;
use \DateTime;

/**
 * Photo
 *
 * @ORM\Entity(repositoryClass="ModelApi\BinaryBundle\Repository\PhotoRepository")
 * @ORM\Table(
 *      name="`photo`",
 *		options={"comment":"Photos"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_photo",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER", label="Binary")
 */
class Photo extends Binary {

	const DEFAULT_ICON = 'fa-picture-o';

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		$ddn = ['original','cropper','optimized_W128','optimized_W512','optimized_W800','optimized_W1024','panoramic_400x250','optimized_W1440','thumbnail_32x32','thumbnail_64x64','thumbnail_128x128','thumbnail_400x400','thumbnail_512x512','formated_32x32','formated_64x64','formated_128x128','formated_600x600','formated_800x600'];
		if(preg_match('#^gif$#i', $this->getFileExtension())) $ddn[] = 'realsize';
		return $ddn;
	}

	/**
	 * Get authorized types of media (image, pdf, video, audio…)
	 * @return array
	 */
	public function getAuthorizedTypes() {
		return ['image'];
	}



}