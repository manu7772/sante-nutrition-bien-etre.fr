<?php
namespace ModelApi\BinaryBundle\Entity;


interface UploadInterface {

	public function PostUploadedFile();

	public function getUploadFile();

	/**
	 * Set uploadFile
	 * @see https://symfony.com/doc/3.4/controller/upload_file.html
	 * @param string $uploadFile = null
	 * @return UploadedFile | null
	 */
	public function setUploadFile($uploadFile = null);


}

