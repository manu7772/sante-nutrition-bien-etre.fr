<?php
namespace ModelApi\BinaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\BinaryBundle\Entity\UploadInterface;
use ModelApi\BinaryBundle\Service\serviceBinary;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

use \Exception;
use \ReflectionClass;

/**
 * Item
 * 
 * @ORM\Entity(repositoryClass="ModelApi\BinaryBundle\Repository\BinaryRepository")
 * @ORM\EntityListeners({"ModelApi\BinaryBundle\Listener\UploadBinaryListener"})
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(name="`binary`", options={"comment":"Binary entities"})
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_binary",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER", label="Binary")
 */
abstract class Binary implements CrudInterface, UploadInterface {

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\BinaryBundle\Traits\BinaryFile;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	const DEFAULT_ICON = 'fa-file-o';
	// const ENTITY_SERVICE = serviceBinary::class;
	const WEB_PATH = [__DIR__, '..', '..', '..', '..', 'web'];
	const FILES_FOLDER = 'files';
	const PDF_FLATTEN_RESOLUTION = 150;
	const PDF_FLATTEN_FORMAT = 'png';

	const SHORTCUT_CONTROLS = true;

	/**
	 * Group Id
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", contexts={"all","list"}, order=10)
	 */
	protected $id;

	public function __construct() {
		$this->id = null;
		$this->init_BaseEntity();
		$this->initCreated();
		$this->init_ClassDescriptor();
		$this->init_BinaryFile();
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getId();
	}


	public function isValid() {
		return !empty($this->fileExtension)
			&& !empty($this->fileformat)
			&& !empty($this->fileName)
			;
	}

	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}



}