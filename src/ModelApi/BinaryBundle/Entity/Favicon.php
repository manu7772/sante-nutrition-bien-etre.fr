<?php
namespace ModelApi\BinaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;

use \Exception;
use \DateTime;

/**
 * Favicon
 *
 * @ORM\Entity(repositoryClass="ModelApi\BinaryBundle\Repository\FaviconRepository")
 * @ORM\Table(
 *      name="`favicon`",
 *		options={"comment":"Favicons"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_favicon",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER", label="Binary")
 */
class Favicon extends Binary {

	const DEFAULT_ICON = 'fa-picture-o';

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['favicons_16x16','favicons_57x57','favicons_64x64','favicons_72x72','favicons_114x114'];
	}

	/**
	 * Get authorized types of media (image, pdf, video, audio…)
	 * @return array
	 */
	public function getAuthorizedTypes() {
		return ['image'];
	}



}