<?php
namespace ModelApi\BinaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;

use \Exception;
use \DateTime;

/**
 * Thumbnail
 *
 * @ORM\Entity(repositoryClass="ModelApi\BinaryBundle\Repository\ThumbnailRepository")
 * @ORM\Table(
 *      name="`thumbnail`",
 *		options={"comment":"Thumbnails"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_thumbnail",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER", label="Binary")
 */
class Thumbnail extends Binary {

	const DEFAULT_ICON = 'fa-picture-o';

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['formated_32x32', 'formated_64x64','formated_128x128','panoramic_400x250','formated_512x512','thumbnail_32x32','thumbnail_64x64','thumbnail_128x128','formated_600x600'];
	}

	/**
	 * Get authorized types of media (image, pdf, video, audio…)
	 * @return array
	 */
	public function getAuthorizedTypes() {
		return ['image'];
	}



}