<?php
namespace ModelApi\BinaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;

use \Exception;
use \DateTime;

/**
 * Avatar
 *
 * @ORM\Entity(repositoryClass="ModelApi\BinaryBundle\Repository\AvatarRepository")
 * @ORM\Table(
 *      name="`avatar`",
 *		options={"comment":"Avatars"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_avatar",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER", label="Binary")
 */
class Avatar extends Binary {

	const DEFAULT_ICON = 'fa-picture-o';

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['optimized_W512','optimized_W128','formated_64x64','formated_128x128','panoramic_400x250','formated_600x600','thumbnail_33x33','thumbnail_128x128','thumbnail_512x512'];
	}

	/**
	 * Get authorized types of media (image, pdf, video, audio…)
	 * @return array
	 */
	public function getAuthorizedTypes() {
		return ['image'];
	}



}