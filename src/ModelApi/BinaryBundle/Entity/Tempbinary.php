<?php
namespace ModelApi\BinaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;
use ModelApi\BinaryBundle\Service\serviceTempbinary;

use Symfony\Component\Filesystem\Filesystem;

use \Exception;
use \DateTime;

/**
 * Tempbinary
 *
 * @ORM\Entity(repositoryClass="ModelApi\BinaryBundle\Repository\TempbinaryRepository")
 * @ORM\Table(
 *      name="`tempbinary`",
 *		options={"comment":"Tempbinarys"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_tempbinary",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER", label="Binary")
 */
class Tempbinary extends Binary {

	const DEFAULT_ICON = 'fa-file';
	const ENTITY_SERVICE = serviceTempbinary::class;
	const FILES_FOLDER = 'tmp';

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['thumbnail_128x128'];
	}

	// /**
	//  * Write all files on hard disk
	//  * @return Entity
	//  */
	// protected function writeAllFiles() {
	// 	$this->tempfileUrl = '/tmp/temp_'.$this->getExtensionFileName();
	// 	$fileSystem = new Filesystem();
	// 	$fileSystem->dumpFile($this->tempfileUrl, $this->getBinaryFile());
	// 	return parent::writeAllFiles();
	// }

	// public function getTempfileUrl() {
	// 	return $this->tempfileUrl;
	// }



}