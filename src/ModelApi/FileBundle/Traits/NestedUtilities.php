<?php
namespace ModelApi\FileBundle\Traits;

use ModelApi\CrudsBundle\Annotation as CRUDS;
use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\HttpFoundation\File\UploadedFile;

// BaseBundle
// use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Tempfile;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\OwnerTierInterface;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

use \Exception;

trait NestedUtilities {


	protected $owners = null;

	/**
	 * Tier environment of creation
	 * @Annot\AttachContexEnvironment(replace=false)
	 */
	protected $contextEnvironment = null;

	/**
	 * Is schema directorys checked
	 * @var boolean
	 */
	protected $directorysChecked = 0;

	protected $serviceBasedirectory = null;

	protected $changePlacementMode = false;


	/*************************************************************************************/
	/*** INITIALIZE ITEM
	/*************************************************************************************/

	public function initItem(serviceBasedirectory $serviceBasedirectory) {
		if(!$this->isValidInit()) {
			// $this->changePlacementMode = false;
			$this->serviceBasedirectory = $serviceBasedirectory;
			if($this instanceOf Basedirectory) {
				$this->initQuerys();
				$this->initSorts();
			}
			$this->testCount ??= [];
			$this->addTestCount(__METHOD__, 1);
		}
		return $this;
	}

	public function isValidInit() {
		return $this->serviceBasedirectory instanceOf serviceBasedirectory;
	}


	/*************************************************************************************/
	/*** CREATOR
	/*************************************************************************************/

	// public function setCreator(User $creator) {
	// 	$this->creator = $creator;
	// 	if(empty($this->owner)) $this->setOwner($this->creator);
	// 	return $this;
	// }

	// public function getCreator() {
	// 	return $this->creator;
	// }


	/*************************************************************************************/
	/*** DIRECTORYS CHECKED
	/*************************************************************************************/

	public function isDirectorysChecked() {
		return $this->directorysChecked > 0;
	}

	public function getDirectorysChecked() {
		return $this->directorysChecked;
	}

	public function setDirectorysChecked() {
		$this->directorysChecked++;
		return $this;
	}



	/*************************************************************************************/
	/*** DIAPOS
	/*************************************************************************************/

	// public function getResourceAsDiapo() {
	// 	return null;
	// }

	// public function setResourceAsDiapo(UploadedFile $resource) {
	// 	if(!is_array($this->diapos)) $this->diapos = [];
	// 	$this->diapos[] = $resource;
	// 	$this->setUpdated();
	// 	return $this;
	// }

	// public function addUrlAsDiapo($url) {
	// 	if(!is_array($this->diapos)) $this->diapos = [];
	// 	$this->diapos[] = $url;
	// 	$this->setUpdated();
	// 	return $this;
	// }

	// public function setUrlAsDiapos($urls) {
	// 	foreach ($urls as $url) $this->addUrlAsDiapo($url);
	// 	return $this;
	// }

	// public function getDiapos() {
	// 	if(!is_array($this->diapos)) $this->diapos = [];
	// 	return $this->diapos;
	// }

	// public function clearDiapos() {
	// 	$this->diapos = [];
	// 	return $this;
	// }


	/*************************************************************************************/
	/*** PDFS
	/*************************************************************************************/

	// public function addUrlAsPdf($url) {
	// 	if(!is_array($this->pdfs)) $this->pdfs = [];
	// 	$this->pdfs[] = $url;
	// 	$this->setUpdated();
	// 	return $this;
	// }

	// public function setUrlAsPdfs($urls) {
	// 	foreach ($urls as $url) $this->addUrlAsPdf($url);
	// 	return $this;
	// }

	// public function getPdfs() {
	// 	if(!is_array($this->pdfs)) $this->pdfs = [];
	// 	return $this->pdfs;
	// }

	// public function clearPdfs() {
	// 	$this->pdfs = [];
	// 	return $this;
	// }

	/**
	 * Get Owner Directory's childs by path
	 * @param string $pathname
	 * @param boolean $createIfNotFound = true
	 * @return ArrayCollection <Item>
	 */
	public function getOwnerDirectoryChilds($pathname, $shortnames = [], $createIfNotFound = true, $asFacticees = false) {
		$directory = $this->getOwnerDirectory($pathname, $createIfNotFound);
		if($asFacticees && $createIfNotFound) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): can not create while is arrayData mode!", 1);
		if(is_string($shortnames)) $shortnames = [$shortnames];
		if($directory instanceOf Basedirectory) {
			if($asFacticees) {
				$childs = $directory->getChildsAsFacticees();
				if(!empty($shortnames)) $childs = array_filter($childs, function($child) use ($shortnames) {
					// return count(array_intersect($child['instances'], $shortnames));
					return in_array($child['shortname'], $shortnames);
				});
				return $childs;
			}
			return !empty($shortnames) ? $directory->getChilds()->filter(function($item) use ($shortnames) { return $item->isInstanceOrInterface($shortnames); }) : $directory->getChilds();
			// return !empty($shortnames) ? $directory->getChilds()->filter(function($item) use ($shortnames) { return in_array($item->getShortname(), $shortnames); }) : $directory->getChilds();
		}
		return new ArrayCollection();
	}
	/**
	 * Get Owner Directory's childs by path
	 * @param string $pathname
	 * @param boolean $createIfNotFound = true
	 * @return ArrayCollection <Item>
	 */
	public function getContentDirectory($pathname, $shortnames = [], $createIfNotFound = true, $asFacticees = false) {
		return $this->getOwnerDirectoryChilds($pathname, $shortnames, $createIfNotFound, $asFacticees);
	}

	/**
	 * Get Owner's Directory by path
	 * @param string $pathname
	 * @param boolean $createIfNotFound = true
	 * @return Basedirectory | null
	 */
	public function getOwnerDirectory($pathname, $createIfNotFound = true) {
		if($this instanceOf Basedirectory) {
			// $item = $this->getOwnerdir();
			$path = $pathname;
			if(!preg_match('/^(@?system|@?drive)/', $pathname)) {
				$path = $this->getPathname(true).(empty($pathname) ? $pathname : DIRECTORY_SEPARATOR.$pathname);
			}
			return $this->serviceBasedirectory->getDirByPath($this->ownerdir, $path, $createIfNotFound);
		}
		return $this->serviceBasedirectory->getDirByPath($this, $pathname, $createIfNotFound);
		// return serviceBasedirectory::get_static_DirByPath($this, $pathname, $createIfNotFound, $this->serviceBasedirectory);
	}

	public function getDoublons($pathname) {
		return new ArrayCollection($this->serviceBasedirectory->getDoublons($this, $pathname));
	}

	/*************************************************************************************/
	/*** ORPHAN
	/*************************************************************************************/

	public function getOrphan() {
		return $this->orphan;
	}

	/**
	 * Is orphan: has no parent, nore even symbolic parents
	 * @return boolean
	 */
	public function isOrphan() {
		// return $this->orphan;
		return $this->orphan = empty($this->parent) && $this->dirparents->isEmpty();
	}

	/*************************************************************************************/
	/*** CONTEXT ENVIRONMENT
	/*************************************************************************************/

	public function setContextEnvironment(Tier $contextEnvironment) {
		$this->contextEnvironment = $contextEnvironment;
		return $this;
	}

	public function getContextEnvironment() {
		return $this->contextEnvironment;
	}

	/*************************************************************************************/
	/*** OWNER
	/*************************************************************************************/

	protected function findLogicOwner($changeRootToMainUserAdmin = false) {
		$this->addTestCount(__METHOD__, 10);

		if($this instanceOf User) return null;
		if(!($this->owner instanceOf Item)) {
			// Try ownerdir
			// $owner = $this->getOwnerdir();
			// Try by parent
			// if(empty($owner) && !empty($this->parent)) $owner = $this->parent->getOwnerdir();
			if(empty($owner) && !empty($this->parent)) $owner = $this->parent->getOwnerdir();
			// try by root parent
			// if(empty($owner) && !empty($this->rootparent)) $owner = $this->rootparent->getOwnerdir();
			if(empty($owner) && !empty($this->rootparent)) $owner = $this->rootparent->getOwnerdir();
			// Try Creator (only while create)
			// if(empty($owner)) $owner = $this->creator;
		} else {
			$owner = $this->owner;
			if(!empty($this->parent)) $owner = $this->parent->getOwnerdir();
		}
		// if(empty($this->getId()) && !empty($this->creator)) $owner = $this->creator;
		// Try main admin User
		if(empty($owner) || ($changeRootToMainUserAdmin && strtolower($owner->getUsername()) === 'root')) {
			$mainUserAdmin = $this->serviceBasedirectory->getMainUserAdmin();
			if($mainUserAdmin instanceOf User) $owner = $mainUserAdmin;
		}
		// echo('<div>'.__METHOD__.'(): Logic owner: '.(null === $owner ? 'NULL' : $owner->getShortname().' '.json_encode($owner->getName())).'</div>');
		return $owner;
	}

	/**
	 * Get owner
	 * @return Item | null
	 */
	public function getOwner() {
		return $this->owner;
	}

	/**
	 * Set owner
	 * @param Item $item = null
	 * @param boolean $refresh = false
	 * @return self
	 */
	public function setOwner(Item $item = null, $keepOldParentAsSymlink = false) {
		$this->addTestCount(__METHOD__, 10);
		
		if($this instanceOf Tempfile) {
			$this->owner = $item;
			// echo('<div>'.__METHOD__.'(): '.(null === $this->owner ? 'NULL' : $this->owner->getShortname().' '.json_encode($this->owner->getName())).' for '.$this->getShortname().' '.json_encode($this->getName()).'</div>');
			return $this;
		}
		// Forced owner
		if($item instanceOf Basedirectory) $item = $item->getOwnerdir();
		if($this->isCompleteOrIncompleteRoot()) {
			$item = $this->getOwnerdir(true);
			// $item = $this->getOwnerdir();
		}
		// if($this instanceOf Entreprise && $item instanceOf Entreprise) $item = $item->getOwner();
		if($this instanceOf User) $this->owner = $item = null;

		if(!empty($item)) {
			$item->isValidOwnerAndParent(true); // Owner must be valid!!!
			$this->changePlacement($item, null, $keepOldParentAsSymlink);
		} else {
			// compute parent
			if(!$this->isValidOwnerAndParent(false)) {
				if(!$this->tryResolveParent()) throw new Exception("Could not find logic parent with owner ".(empty($this->owner) ? 'NULL' : $this->owner->getShortname().' '.json_encode($this->owner->getNameOrTempname()))."!", 1);
				if($this->isCheckModeContext() && !$this->isValidOwnerAndParent()) {
					if(!empty($parent ?? null)) $parent->checkAndRepairValidity();
					$this->checkAndRepairValidity();
					// DevTerminal::contextException($this, false, "Owner (".(empty($this->owner) ? json_encode(null) : $this->owner->getShortname()." ".json_encode($this->owner->getNameOrTempname())).") and parent (".(empty($parent) ? json_encode(null) : $parent->getShortname()." ".json_encode($parent->getNameOrTempname())).") are not synchronized!", null, __LINE__, __METHOD__);
				}
			}
		}
		$propagOwner = $this instanceOf User ? $this : $this->owner;

		$this->isValidOwnerAndParent(true);
		// propagation to systemDirs
		foreach ($this->getRootDirs() as $rootdir) {
			if($rootdir->getOwner() !== $propagOwner) {
				$rootdir->setOwner($this, $keepOldParentAsSymlink);
			}
		}
		// propagation to childs
		if($this instanceOf Basedirectory) {
			foreach ($this->getSolidChilds() as $child) {
				$child->setOwner($propagOwner);
			}
		}
		// echo('<div>'.__METHOD__.'():<strong>END</strong> '.(null === $this->owner ? 'NULL' : $this->owner->getShortname().' '.json_encode($this->owner->getName())).' for '.$this->getShortname().' '.json_encode($this->getName()).'</div>');
		$this->UpdateHimselfToOwnersAndManagers();
		return $this;
	}

	protected function changeOwner(User $owner = null) {
		$this->owner = $owner;
		$this->checkOwners();
		return $this;
	}

	public function getAllOwnerdirs($all = false) {
		$ownerdirs = new ArrayCollection();
		foreach ($this->getDirparents() as $dirparent) {
			$ownerdir = $all ? $dirparent->getOwnerdirs() : [$dirparent->getOwnerdir()];
			foreach ($ownerdir as $od) {
				if(!$ownerdirs->contains($od)) $ownerdirs->add($od);
			}
		}
		return $ownerdirs;
	}

	/*************************************************************************************/
	/*** PARENT
	/*************************************************************************************/

	/**
	 * Get hardlink parent Directory
	 * @return Directory | null
	 */
	public function getParent() {
		return $this->parent;
	}

	/**
	 * Get all parent Directorys
	 * @param boolean $inverse = false
	 * @return ArrayCollection <Basedirectory>
	 */
	public function getAllParents($inverse = false) {
		$this->addTestCount(__METHOD__, 200);

		$parents = new ArrayCollection();
		$parent = $this;
		$count = 0;
		while (null !== $parent = $parent->getParent()) {
			if($count++ > 1000) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): on ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." infinite loop (>".$count.")!", 1);
			if($parents->contains($parent)) break;
			$parents->add($parent);
			if($parent->isCompleteOrIncompleteRoot()) {
				if(!empty($parent->getParent())) {
					// if($this->isCheckModeContext()) {
					// 	$parent->checkAndRepairValidity();
					// 	$this->checkAndRepairValidity();
					// 	return $this->getAllParents($inverse);
					// }
					DevTerminal::contextException($parent, false, "This root parent (of child ".$this->getShortname()." ".json_encode($this->getNameOrTempname()).") can not have parent!", null, __LINE__, __METHOD__);
				}
				break;
			}
		}
		return $inverse ? new ArrayCollection(array_reverse($parents->toArray())) : $parents;
	}


	/*************************************************************************************/
	/*** OWNERS / MANAGERS
	/*************************************************************************************/

	/**
	 * Get owners (allowed all CRUD)
	 * @param boolean $inverse = false
	 * @param boolean $recomputed = false
	 * @return ArrayCollection <Item>
	 */
	public function getOwners($inverse = false) {
		$this->checkOwners();
		return $inverse ? new ArrayCollection(array_reverse($this->owners->toArray())) : $this->owners;
	}

	public function checkOwners() {
		$this->owners = $this->getComputedOwners(false);
		return $this;
	}

	public function getComputedOwners($inverse = false) {
		$owners = new ArrayCollection();
		if($this instanceOf User) return $owners;
		$ownerdir = $this->getOwnerdir(true);
		while ($ownerdir) {
			if($this->_is_repair()) {
				if(!$ownerdir->canHaveParent() && $ownerdir->hasParent()) $ownerdir->tryResolveParent();
				if(empty($ownerdir->getRootparent(true))) {
					$ownerdir->checkAndRepairValidity(null, false);
					$ownerdir->updateNestedData(null, true);
				}
			}
			if(!$owners->contains($ownerdir) && $ownerdir !== $this) {
				$owners->add($ownerdir);
			}
			$rootparent = $ownerdir->getRootparent(true);
			if(!($rootparent instanceOf Basedirectory)) {
				$ownerdir = null;
			} else {
				$ownerdir = $rootparent->getOwnersystemdir() instanceOf Item ? $rootparent->getOwnersystemdir() : $rootparent->getOwnerdrivedir();
			}
		}
		// if(!$this->isValidOwners()) throw new Exception("Error Processing Request", 1);
		return $inverse ? new ArrayCollection(array_reverse($owners->toArray())) : $owners;
	}

	/**
	 * Get managers (allow CRU) --> NOT delete or move
	 * @param boolean $refresh = false
	 * @param boolean $fullcompute = false
	 * @return ArrayCollection <Tier>
	 */
	public function getItemmanagers($refresh = false, $fullcompute = false) {
		if($this instanceOf User) {
			$this->itemmanagers = new ArrayCollection();
		} else if($refresh) {
			$this->itemmanagers = new ArrayCollection();
			foreach ($this->dirparents as $dirparent) {
				foreach ($dirparent->getOwners() as $owner) {
					if($owner instanceOf Tier && !$this->itemmanagers->contains($owner)) $this->itemmanagers->add($owner);
				}
			}
		}
		return $this->itemmanagers;
	}

	public function canManage(Tier $tier) {
		return $this->itemmanagers->contains($tier);
	}

	public function UpdateHimselfToOwnersAndManagers() {
		if($this instanceOf User || $this->isCompleteOrIncompleteRoot()) return $this;
		if($this instanceOf Tier) {
			$this->removeDirparents(true);
			// Check data
			// if(empty($this->parent)) $this->tryResolveParent();
			// if(empty($this->parent)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." while checking owners and managers. Parent is empty!", 1);
			if(!($this->owner instanceOf User)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." while checking: owner is not OwnerTierInterface!", 1);
			$this->checkOwners();
			// Add to OWNER
			$this->owner->addOwneditem($this);
			// Remove OWNER
			foreach ($this->dirparents as $dirparent) {
				$owner = $dirparent->getOwner();
				if($owner instanceOf User && !$this->owners->contains($owner)) $owner->removeOwneditem($this);
			}
			// Add MANAGERS
			foreach ($this->owners as $owner) {
				if($owner instanceOf Tier) $owner->addItemformanager($this);
			}
			// Remove MANAGERS
			foreach ($this->dirparents as $dirparent) {
				$owner = $dirparent->getOwner();
				//if($owner instanceOf Tier && !$this->owners->contains($owner)) $owner->removeItemformanager($this);
				if($owner instanceOf Tier && !$this->itemmanagers->contains($owner)) $this->itemmanagers->add($owner);
			}
			// Add to ContextEnvironment MANAGER
			if($this->contextEnvironment instanceOf Tier) {
				$this->contextEnvironment->addItemformanager($this);
				if(!$this->itemmanagers->contains($this->contextEnvironment)) $this->itemmanagers->add($this->contextEnvironment);
			}
		}
		return $this;
	}


	// public function removeHimselfFromOwnersAndManagers() {
	// 	$result = $this->removeDirparents(true);
	// 	if(!$result && $this->isDeepControl()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." did not succeede to remove all dirparents! There are ".$this->dirparents->count()." parent(s) left!", 1);
	// 	return $this;
	// }

	public function getDiffuseurs($classes = null) {
		// return $this->serviceBasedirectory->getDiffuseurs($this, $classes);
		// if((is_array((array)$classes) || is_string($classes)) && !empty($classes)) {
			// $classes = array_map(function($class) {
				// return $this->container->get(serviceEntities::class)->getShortnameByAnything($class);
			// }, (array)$classes);
			// $classes = array_filter($classes, function($classname) { return !empty($classname); });
		// } else {
			// $classes = [];
		// }
		$diffuseurs = new ArrayCollection();
		foreach ($this->getDirparents() as $parent) {
			foreach($parent->getOwners() as $parent_ownerdir) {
				if(!$diffuseurs->contains($parent_ownerdir) && (empty($classes) || in_array($parent_ownerdir->getClassname(), (array)$classes)) || in_array($parent_ownerdir->getShortname(), (array)$classes)) $diffuseurs->add($parent_ownerdir);
			}
		}
		return $diffuseurs;
	}


}