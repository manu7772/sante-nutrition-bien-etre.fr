<?php
namespace ModelApi\FileBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueBymicrotime extends Constraint {

	public $message = 'This field value {{ value }} allready exists for this entity {{ microtime }}!';
    public $fields = array();

    public function getRequiredOptions() {
        return array('fields');
    }

	public function validatedBy() {
		return static::class.'Validator';
	}

    public function getDefaultOption() {
        return 'fields';
    }

}