<?php
namespace ModelApi\FileBundle\Validator\Constraints;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use ModelApi\FileBundle\Validator\Constraints\UniqueBymicrotime;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Doctrine\Common\Inflector\Inflector;

use ModelApi\BaseBundle\Service\serviceEntities;

class UniqueBymicrotimeValidator extends ConstraintValidator {

	protected $container;
	protected $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}

	public function validate($entity, Constraint $constraint) {

		if (!$constraint instanceof UniqueBymicrotime) {
			throw new UnexpectedTypeException($constraint, UniqueBymicrotime::class);
		}

		$fields = (array) $constraint->fields;

		if (0 === \count($fields)) {
			throw new ConstraintDefinitionException('At least one field has to be specified.');
		}

		if (null === $entity) return;

		$repo = $this->serviceEntities->getRepository($entity->getClassname());
		foreach ($fields as $field) {
			$getter = Inflector::camelize('get_'.$field);
			if(method_exists($entity, $getter)) {
				$pathnames = $repo->findBy([$field => $entity->$getter(), 'microtimeid' => $entity->getMicrotimeid()]);
				if(count($pathnames) > 1) {
					$this->context->buildViolation($constraint->message)
						// ->atPath($errorPath)
						->setParameter('{{ value }}', $entity->$getter())
						->setParameter('{{ microtime }}', $entity->getMicrotimeid())
						// ->setInvalidValue($invalidValue)
						// ->setCode(UniqueEntity::NOT_UNIQUE_ERROR)
						// ->setCause($result)
						->addViolation();
				}
			}
		}

		// custom constraints should ignore null and empty values to allow
		// other constraints (NotBlank, NotNull, etc.) take care of that
		// if (null === $value || '' === $value) {
			// return;
		// }

		// if (!is_string($value)) {
		// 	throw new UnexpectedTypeException($value, 'string');
		// }

		# if (!preg_match('/^[a-zA-Z0-9]+$/', $value, $matches)) {
			// $this->context->buildViolation($constraint->message)
			// 	->setParameter('{{ string }}', $value)
			// 	->addViolation();
		// }
	}


}
