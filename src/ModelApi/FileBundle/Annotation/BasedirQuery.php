<?php
namespace ModelApi\FileBundle\Annotation;

use ModelApi\AnnotBundle\Annotation\BaseAnnotation;

/**
 * @Annotation
 * @Target("METHOD")
 * 
 * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class BasedirQuery extends BaseAnnotation {

	/**
	 * @var string
	 */
	public $name = null;

	/**
	 * @var string
	 */
	public $description = null;

	/**
	 * @var string
	 */
	public $notice = null;


	public function __toString() { return 'BasedirQuery'; }


}
