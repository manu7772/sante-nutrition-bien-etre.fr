<?php
namespace ModelApi\FileBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use ModelApi\BaseBundle\Repository\BasentityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\DependencyInjection\ContainerInterface;

// use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcherInterface;
// use ModelApi\UserBundle\Entity\ModelUser;

use \LogicException;
use \DateTime;

/**
 * FileformatRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FileformatRepository extends BasentityRepository {

	const ELEMENT = 'fileformat';

}