<?php
namespace ModelApi\FileBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\DependencyInjection\ContainerInterface;

// use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcherInterface;

use ModelApi\BaseBundle\Representation\Pagination;
use ModelApi\BaseBundle\Service\serviceTools;

// UserBundle
// use ModelApi\UserBundle\Entity\Tier;
// use ModelApi\UserBundle\Entity\User;
// FileBundle
use ModelApi\FileBundle\Repository\BasedirectoryRepository;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Entity\Directory;

use \LogicException;
use \ReflectionClass;
use \DateTime;

/**
 * DirectoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DirectoryRepository extends BasedirectoryRepository {

	const ELEMENT = 'directory'; // entity



	/********************************************************************************************************************/
	/*** QUERY BUILDERS
	/********************************************************************************************************************/


}