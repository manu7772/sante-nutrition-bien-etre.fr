<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
// use Doctrine\Common\Collections\ArrayCollection;
use Hateoas\Configuration\Annotation as Hateoas;
// use Gedmo\Mapping\Annotation as Gedmo;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;

use Doctrine\ORM\Event\LifecycleEventArgs;

use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Item;
// use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\DirectoryInterface;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Repository\MenuRepository;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// UserBundle
// use ModelApi\UserBundle\Entity\User;
// use ModelApi\UserBundle\Entity\Tier;
// DevprintBundle
// use ModelApi\DevprintsBundle\Service\DevTerminal;

use \ReflectionClass;
use \Exception;

/**
 * Menu
 * 
 * @ORM\Entity(repositoryClass=MenuRepository::class)
 * @ORM\Table(name="`menu`", options={"comment":"Menus"})
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_menu",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER")
 * !!!@Annot\HasCache(serialization_groups={serviceMenu::FSCACHE_NAME})
 * @Annot\HasTranslatable()
 */
class Menu extends Basedirectory {

	const DEFAULT_ICON = 'fa-sitemap';
	const DEFAULT_OWNER_DIRECTORY = 'system/Menus';
	const ENTITY_SERVICE = serviceMenu::class;

	/**
	 * @var array
	 * @CRUDS\Updates({
	 * 		@CRUDS\Update(
	 * 			type="EntityType",
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=400,
	 * 			options={"required"=false, "by_reference"=false, "multiple"=true, "group_by"="shortname", "class"="ModelApi\PagewebBundle\Entity\Pageweb", "attr"={"class"="select-choice", "placeholder"="Pages web"}, "query_builder"="auto", "label"="Pages web"},
	 * 			contexts={"additem"},
	 * 		),
	 * })
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $childs_pagewebs;

	/**
	 * @var array
	 * @CRUDS\Updates({
	 * 		@CRUDS\Update(
	 * 			type="EntityType",
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=400,
	 * 			options={"required"=false, "by_reference"=false, "multiple"=true, "group_by"="shortname", "class"="ModelApi\FileBundle\Entity\Menu", "attr"={"class"="select-choice", "placeholder"="Menus"}, "query_builder"="auto", "label"="Menus"},
	 * 			contexts={"addbasedirectory"},
	 * 		),
	 * })
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $childs_directorys;

	/**
	 * Root (has no solid parent link)
	 * @var boolean
	 * @ORM\Column(name="rootmenu", type="boolean", nullable=false, unique=false)
	 */
	protected $rootmenu;

	// /**
	//  * Cached content of chidren (by level max)
	//  * @var array
	//  * @ORM\Column(name="cachedchilds", type="json_array", nullable=true, unique=false)
	//  * !!!@Annot\CacheData(apply_condition="isRootmenu", serialization_groups="_parent", lifetime=3600)
	//  */
	// protected $cachedchilds;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $subtitle;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 	)
	 */
	protected $menutitle;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 	)
	 */
	protected $accroche;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		options={"required"=false},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		options={"required"=false},
	 * 	)
	 */
	protected $resume;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		options={"required"=false},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		options={"required"=false},
	 * 	)
	 */
	protected $fulltext;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		options={"required"=false},
	 * 		contexts={"medium"},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		options={"required"=false},
	 * 		contexts={"medium"},
	 * 	)
	 */
	protected $description;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $color;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $urls;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $normalizeNameMethod;



	public function __construct() {
		parent::__construct();
		$this->typedir = serviceBasedirectory::TYPEDIR_MENU;
		$this->rootmenu = null;
		// $this->cachedchilds = null;
		return $this;
	}


	/**
	 * Set texts if not defined / Refresh Cachedchilds / Define root menu
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return Menu
	 */
	public function computeBaseTexts() {
		if(null === $this->getTitle()) $this->setTitle($this->getName());
		if(null === $this->getMenutitle()) $this->setMenutitle($this->getTitle());
		if(null === $this->getSubtitle()) $this->setSubtitle($this->getTitle());
		if(null === $this->getDescription()) $this->setDescription($this->getTitle());
		if(null === $this->getAccroche()) $this->setAccroche($this->getTitle());
		if(null === $this->getFulltext() && is_string($this->getResume())) $this->setFulltext($this->getResume());
		// Refresh Cachedchilds
		// $this->getCachedchilds(true);
		// Define root menu
		$this->defineRootmenu();
		return $this;
	}


	/**
	 * Define if is root menu
	 * @return Menu
	 */
	public function defineRootmenu() {
		$this->rootmenu = !($this->getParent() instanceOf Menu);
		return $this;
	}

	/**
	 * Get root menu
	 * @return boolean
	 */
	public function getRootmenu() {
		return $this->rootmenu;
	}

	/**
	 * Is root menu
	 * @return boolean
	 */
	public function isRootmenu() {
		return $this->rootmenu;
	}

	// /**
	//  * Get cached childs
	//  * @param boolean $refresh = false
	//  * @return mixed
	//  */
	// public function getCachedchilds($refresh = false, $findRootParent = false) {
	// 	if($findRootParent && !$this->isRootmenu()) {
	// 		$parentL1 = $this->getParent(); // Level -1
	// 		if($parentL1 instanceOf Menu && $parentL1->isRootmenu()) {
	// 			return $parentL1->getCachedchilds($refresh, false);
	// 		} else {
	// 			$parentL2 = $parentL1->getParent(); // Level -2
	// 			if($parentL2 instanceOf Menu && $parentL2->isRootmenu()) {
	// 				return $parentL2->getCachedchilds($refresh, false);
	// 			} else {
	// 				$parentL3 = $parentL2->getParent(); // Level -3
	// 				if($parentL3 instanceOf Menu && $parentL3->isRootmenu()) {
	// 					return $parentL3->getCachedchilds($refresh, false);
	// 				}
	// 			}
	// 		}
	// 	}
	// 	if(!isset($this->_fsCache_cachedchilds)) throw new Exception("Error ".__METHOD__."(): CacheData annotation is not loaded!", 1);
	// 	return $this->_fsCache_cachedchilds->isSystemFile() ? $this->_fsCache_cachedchilds->getCached($refresh) : $this->cachedchilds;
	// }

	// /**
	//  * Set cached childs
	//  * @param mixed $cachedchilds
	//  * @return Menu
	//  */
	// public function setCachedchilds($cachedchilds) {
	// 	if(!isset($this->_fsCache_cachedchilds)) throw new Exception("Error ".__METHOD__."(): CacheData annotation is not loaded!", 1);
	// 	$this->cachedchilds = $this->_fsCache_cachedchilds->isSystemFile() ? null : $cachedchilds;
	// 	return $this;
	// }

	/*************************/
	/*** MENU              ***/
	/*************************/

	/**
	 * Is menuable
	 * @return boolean
	 */
	public function isMenuable() {
		return true;
	}


}




