<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Doctrine\Common\Collections\ArrayCollection;
use Hateoas\Configuration\Annotation as Hateoas;
// use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Symfony\Component\HttpFoundation\File\UploadedFile;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Video;
// use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Entity\Fileformat;
use ModelApi\FileBundle\Service\FileManager;

// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \Exception;
use \ReflectionClass;

/**
 * File
 * 
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\FileRepository")
 * @ORM\Table(name="`file`", options={"comment":"Files"})
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_file",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER")
 * @Annot\HasTranslatable()
 */
abstract class File extends Item {

	const DEFAULT_ICON = 'fa-file';
	// SUBTYPES
	const SUBTYPE_UNKNOWN = "unknown";
	const SUBTYPE_MEDIA = "media";
	const SUBTYPE_TEXT = "text";
	const ENTITY_SERVICE = FileManager::class;
	const DEFAULT_ASSTRING_EXTENSION = null;

	/**
	 * @ORM\ManyToMany(targetEntity="ModelApi\UserBundle\Entity\Group")
	 * @ORM\JoinTable(name="file_group_downloadable",
	 *     joinColumns={@ORM\JoinColumn(name="groups_id", referencedColumnName="id", nullable=false)},
	 * )
	 */
	protected $downloadableGroups;

	/**
	 * @ORM\ManyToMany(targetEntity="ModelApi\UserBundle\Entity\Group")
	 * @ORM\JoinTable(name="file_group_sendable",
	 *     joinColumns={@ORM\JoinColumn(name="groups_id", referencedColumnName="id", nullable=false)},
	 * )
	 */
	protected $sendableGroups;

	/**
	 * @ORM\OneToMany(targetEntity="ModelApi\FileBundle\Entity\Fileversion", cascade={"persist","remove"}, mappedBy="file", orphanRemoval=true)
	 * @ORM\JoinTable(
	 * 		name="`file_version`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false, onDelete="CASCADE") }
	 * )
	 * @ORM\OrderBy({"created" = "desc"})
	 */
	protected $versions;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\FileBundle\Entity\Fileversion", cascade={"persist"})
	 * @ORM\JoinColumn(name="file_currentversion", onDelete="CASCADE")
	 */
	protected $currentVersion;
	protected $uploadFile;
	protected $stringAsVersion;
	protected $changedCurrentVersion;

	/**
	 * @var integer
	 * @ORM\Column(name="nbversions", type="integer", nullable=false, unique=false)
	 */
	protected $nbversions;

	/**
	 * @var integer
	 * @ORM\Column(name="fileSize", type="integer", nullable=false, unique=false)
	 */
	protected $fileSize;

	/**
	 * @var string
	 * @ORM\Column(name="fileExtension", type="string", nullable=true, unique=false)
	 */
	protected $fileExtension;

	/**
	 * @var string
	 * @ORM\Column(name="mediatype", type="string", length=32, nullable=true, unique=false)
	 */
	protected $mediatype;

	/**
	 * @var string
	 * @ORM\Column(name="subtype", type="string", length=32, nullable=true, unique=false)
	 */
	protected $subtype;

	/**
	 * @var string
	 * @ORM\Column(name="fileUrl", type="text")
	 */
	protected $fileUrl;

	/**
	 * @var array
	 * @ORM\Column(name="fileUrls", type="json_array")
	 */
	protected $fileUrls;

	/**
	 * Valid content-types
	 * @CRUDS\Create(
	 * 		type="HiddenType",
	 * 		show=true,
	 * 		update=false,
	 * 		order=0,
	 * 		options={"required"=true, "disabled"=true},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="HiddenType",
	 * 		show=true,
	 * 		update=false,
	 * 		order=0,
	 * 		options={"required"=true, "disabled"=true},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Show(role=false, order=0)
	 * @Annot\AttachValidContentTypes
	 */
	protected $validContentTypes;

	/**
	 * normalizeNameMethod
	 * @var string
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=101,
	 * 		options={"required"=true, "label"="Normaliser le nom", "choices"="auto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=101,
	 * 		options={"required"=true, "label"="Normaliser le nom", "choices"="auto"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=101, label="Normalisation du nom")
	 */
	protected $normalizeNameMethod;

	/**
	 * Inputname
	 * @var string
	 * @CRUDS\Create(
	 * 		show=true,
	 * 		update=true,
	 * 		order=1,
	 * 		options={"required"=true, "label"="Nom", "attr"={"placeholder"="Nom du fichier"}},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		show=true,
	 * 		update=true,
	 * 		order=1,
	 * 		options={"required"=true, "label"="Nom", "attr"={"placeholder"="Nom du fichier"}},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=1, label="Nom du fichier")
	 */
	protected $inputname;

	/**
	 * Name
	 * @var string
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false, "label"="Nom", "attr"={"placeholder"="Nom du fichier"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false, "label"="Nom", "attr"={"placeholder"="Nom du fichier"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=100, label="Nom")
	 */
	protected $name;

	/**
	 * @var string
	 * Nom de l'élément - utilisé également pour la balise <title>
	 * @ORM\Column(name="title", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(show=false, update=false, type="TextType", order=2, options={"by_reference"=false, "required"=true}, contexts={"translation", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="TextType", order=2, options={"by_reference"=false, "required"=true}, contexts={"translation", "expert"})
	 * @CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $title;

	/**
	 * @var string
	 * Sous-titre de l'élément - utilisé en supplément de title
	 * Texte de 1 à 3 lignes
	 * @ORM\Column(name="subtitle", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show=false, update=false, type="SummernoteType", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=4, options={"by_reference"=false, "required"=false}, contexts={"translation", "expert"})
	 * @CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $subtitle;

	/**
	 * @var string
	 * Nom court de l'élément - utilisé pour l'affichage notamment dans les menus
	 * @ORM\Column(name="menutitle", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(show=false, update=false, type="TextType", order=5, options={"by_reference"=false, "required"=false, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."}, contexts={"translation", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="TextType", order=5, options={"by_reference"=false, "required"=false, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."}, contexts={"translation", "expert"})
	 * @CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $menutitle;

	/**
	 * @var string
	 * Accroche, slogan de l'élément
	 * Texte de 1 ligne
	 * @ORM\Column(name="accroche", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show=false, update=false, type="SummernoteType", order=6, options={"by_reference"=false, "required"=false}, contexts={"translation", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=6, options={"by_reference"=false, "required"=false}, contexts={"translation", "expert"})
	 * @CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $accroche;

	/**
	 * @var string
	 * Description courte de l'élément
	 * Texte de 1 à 3 lignes
	 * @ORM\Column(name="description", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=14, options={"label"="Description", "by_reference"=false, "required"=false, "description"="Description de cet élément. Cette description est uniquement utilisée dans l'interface d'administration, afin de mieux repérer ce que vous recherchez."}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=14, options={"label"="Description", "by_reference"=false, "required"=false, "description"="Description de cet élément. Cette description est uniquement utilisée dans l'interface d'administration, afin de mieux repérer ce que vous recherchez."}, contexts={"medium", "expert"})
	 * @CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $description;

	/**
	 * @var string
	 * Texte de l'élément, version courte de fulltext
	 * Texte de 5 ou 6 lignes max
	 * @ORM\Column(name="resume", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show=false, update=false, type="SummernoteType", order=8, options={"by_reference"=false, "required"=false}, contexts={"translation", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=8, options={"by_reference"=false, "required"=false}, contexts={"translation", "expert"})
	 * @CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $resume;

	/**
	 * @var string
	 * Texte complet de l'élément - illimité
	 * @ORM\Column(name="full_text", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show=false, update=false, type="SummernoteType", order=9, options={"by_reference"=false, "required"=false}, contexts={"translation", "expert"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=9, options={"by_reference"=false, "required"=false}, contexts={"translation", "expert"})
	 * @CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $fulltext;

	/**
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPicone"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPicone"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT}
	 * 	)
	 * @CRUDS\Show(role=true, order=600)
	 */
	protected $picone;

	/**
	 * @var array
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		type="TextareaType",
	 * 		options={"required"=false, "by_reference"=false, "property_path"="urlsAsString"},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		type="TextareaType",
	 * 		options={"required"=false, "by_reference"=false, "property_path"="urlsAsString"},
	 * 		contexts={"expert"}
	 * )
	 * @CRUDS\Show(role=true)
	 */
	protected $urls;

	/**
	 * All Groups list
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		options={"multiple"=true, "required"=false, "group_by"="groupedName", "class"="ModelApi\UserBundle\Entity\Group", "query_builder"="qb_findTierAllGroups"},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		options={"multiple"=true, "required"=false, "group_by"="groupedName", "class"="ModelApi\UserBundle\Entity\Group", "query_builder"="qb_findTierAllGroups"},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=1000)
	 */
	protected $groups;

	/**
	 * Is enabled
	 * @var boolean
	 * @ORM\Column(name="visible", type="boolean", nullable=false, unique=false)
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $visibleEvenIfInactive;

	/**
	 * @var string
	 * @ORM\Column(name="color", type="string", length=32, nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		type="ColorpickerType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true, "by_reference"=false},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT}
	 * )
	 * @CRUDS\Update(
	 * 		type="ColorpickerType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=true, "by_reference"=false},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT}
	 * )
	 * @CRUDS\Show(role=true, order=100, type="colordot", order=30)
	 */
	protected $color;

	/**
	 * @var string
	 * @ORM\Column(name="color", type="string", length=32, nullable=true, unique=false)
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $pageweb;


	public function __construct() {
		parent::__construct();
		$this->downloadableGroups = new ArrayCollection();
		$this->sendableGroups = new ArrayCollection();
		$this->currentVersion = null;
		$this->changedCurrentVersion = false;
		$this->versions = new ArrayCollection();
		$this->checkVersion();
		$this->fileUrls = [];
		$this->fileUrl = null;
		$this->uploadFile = null;
		$this->validContentTypes = null;
		$this->fileSize = null;
		$this->fileExtension = null;
		$this->mediatype = null;
		$this->subtype = null;
		$this->setNormalizeNameMethod(FileManager::NORMALIZE_METHODS[2]);
		$this->setCroppable(false);
		return $this;
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getSlug();
	}

	public function getValidContentTypes($asArray = false) {
		if($asArray) {
			if(empty($this->validContentTypes)) return [];
			return preg_split("/[\s,]+/", $this->validContentTypes); 
		}
		return $this->validContentTypes;
	}

	public function setValidContentTypes($validContentTypes) {
		$this->validContentTypes = $validContentTypes;
		return $this;
	}

	/**
	 * Is content-type supported
	 * @return boolean | void
	 */
	public function isContentTypeSupported() {
		if($this->currentVersion instanceOf Fileversion && $this->currentVersion->getFileformat() instanceOf Fileformat) return $this->currentVersion->getFileformat()->isActive();
	}

	/**
	 * Is content-type valid
	 * @return boolean
	 */
	public function isContentTypeValid() {
		if($this instanceOf Video) {
			if(empty($this->getCurrentVersion()) && empty($this->uploadFile) && !empty($this->fileUrl)) return true;
		}
		// if($this->currentVersion instanceOf Fileversion && $this->currentVersion->getFileformat()->isInactive()) return false;
		// $this->checkVersion();
		$types = $this->getValidContentTypes(true);
		if(empty($this->uploadFile) && $this->id != null) {
			// return true; // update: does not need any new uploadFile
			if(empty($types)) return true;
			$mediatype = $this->currentVersion->getFileformat()->getContentType();
		} else if($this->uploadFile instanceOf UploadedFile) {
			$mediatype = $this->uploadFile->getMimeType();
		} else if($this->id != null) {
			// $mediatype = serviceTools::getUploadFileContentType($this->currentVersion->getUploadFile());
			$mediatype = explode('@', $this->currentVersion->stream_type_while_upload)[0];
		} else {
			throw new Exception("Error line ".__LINE__." ".__METHOD__.": this ".$this->getShortname()." ".$this->getNameOrTempname()." requires a uploaded file, but got ".(is_object($this->uploadFile) ? get_class($this->uploadFile) : gettype($this->uploadFile)." ".json_encode($this->uploadFile))."!", 1);
		}
		// if(!count($types) && $this->id !== null) return true;
		if(empty($types)) throw new Exception("Error line ".__LINE__." ".__METHOD__.": valid content types is empty!", 1);
		// echo('<pre><h3>'.__METHOD__.'(): search '.json_encode($mediatype).' in </h3>'); var_dump($types); echo('</pre>');
		return in_array($mediatype, $types);
	}

	/**
	 * Define types
	 * @return File
	 */
	public function defineTypes() {
		$this->mediatype = $this->currentVersion instanceOf Fileversion ? $this->currentVersion->getMediaType() : null;
		$this->setSubtype();
		return $this;
	}

	/**
	 * Define size
	 * @return File
	 */
	public function defineFileSize() {
		$this->fileSize = $this->currentVersion instanceOf Fileversion ? $this->currentVersion->getFileSize() : 0;
		return $this;
	}

	/**
	 * Define extension
	 * @return File
	 */
	public function defineFileExtension() {
		$this->fileExtension = $this->currentVersion instanceOf Fileversion ? $this->currentVersion->getFileExtension() : null;
		return $this;
	}


	/*************************************************************************************/
	/*** NAME
	/*************************************************************************************/

	/**
	 * Set name
	 * @param string $name
	 * @param boolean $updateNested = true
	 * @return self
	 */
	public function setName($name, $updateNested = true) {
		if(empty($name)) $name = $this->getInputname();
		$name = serviceTools::getTextOrNullStripped($name);
		$name = serviceTools::removeSeparator($name);
		FileManager::getValidTransformedFilename($name, $this->normalizeNameMethod);
		parent::setName($name, $updateNested);
		return $this;
	}


	/*************************************************************************************/
	/*** CROP DATA
	/*************************************************************************************/

	/**
	 * Is croppable of current Fileversion
	 * @return boolean
	 */
	public function isCroppable() {
		return $this->currentVersion instanceOf Fileversion ? $this->currentVersion->isCroppable() : false;
	}

	/**
	 * Get croppable of current Fileversion
	 * @return boolean
	 */
	public function getCroppable() {
		return $this->currentVersion instanceOf Fileversion ? $this->currentVersion->getCroppable() : false;
	}

	/**
	 * Set croppable of current Fileversion
	 * @param boolean $croppable = true
	 * @return File
	 */
	public function setCroppable($croppable = true) {
		if($this->currentVersion instanceOf Fileversion) {
			$this->currentVersion->setCroppable($croppable);
		}
		return $this;
	}

	/**
	 * Get croppdata of current Fileversion
	 * @return array
	 */
	public function getCroppdata() {
		return $this->currentVersion instanceOf Fileversion ? $this->currentVersion->getCroppdata() : null;
	}

	/**
	 * Set croppdata of current Fileversion
	 * @param array $croppdata = []
	 * @return File
	 */
	public function setCroppdata($croppdata = []) {
		if($this->currentVersion instanceOf Fileversion) {
			$this->currentVersion->setCroppdata($croppdata);
		}
		return $this;
	}



	/*************************************************************************************/
	/*** FILE (CURRENT VERSION) URLS
	/*************************************************************************************/

	/**
	 * Define file URL
	 * @param boolean $check = false
	 * @return string | null
	 */
	public function defineFileUrl($check = false) {
		$this->fileUrl = $this->currentVersion instanceOf Fileversion ? $this->currentVersion->getFileUrl($check) : null;
		return $this->fileUrl;
	}

	/**
	 * Get file URL
	 * @return string | null
	 */
	public function getFileUrl() {
		return $this->fileUrl;
	}

	/**
	 * Define file URLs
	 * @param boolean $check = false
	 * @return array <string>
	 */
	public function defineFileUrls($check = false) {
		$this->fileUrls = $this->currentVersion instanceOf Fileversion ? $this->currentVersion->getFileUrls($check) : [];
		return $this->fileUrls;
	}

	/**
	 * Get file URLs
	 * @return array <string>
	 */
	public function getFileUrls() {
		return $this->fileUrls;
	}

	// /**
	 // * @ORM\PostLoad()
	 // * @ORM\PostPersist()
	 // * @ORM\PostUpdate()
	 // * @return File
	 // */
	// public function postLoad_File() {
		// $this->defineFileUrl(true);
		// return $this;
	// }

	/**
	 * @ORM\PostLoad()
	 * @return File
	 */
	public function postLoad_File() {
		$this->changedCurrentVersion = false;
		return $this;
	}

	/*************************************************************************************/
	/*** VERSIONS
	/*************************************************************************************/

	/**
	 * Check current version
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return File
	 */
	public function checkVersion() {
		if($this->getCurrentVersion() instanceOf Fileversion) {
			$this->defineFileExtension();
			$this->defineFileSize();
			$this->defineTypes();
			$this->defineNbversions();
			$this->defineFileUrls();
			$this->defineFileUrl();
			if(!is_string($this->getName())) $this->setName($this->currentVersion->getFileName());
		}
		return $this;
	}

	public function setCurrentVersion(Fileversion $currentVersion) {
		// if($this->currentVersion instanceOf Fileversion) $this->addVersion($this->currentVersion);
		if($this->currentVersion !== $currentVersion) $this->changedCurrentVersion = true;
		$this->currentVersion = $currentVersion;
		$this->addVersion($this->getCurrentVersion());
		$this->checkVersion();
		return $this;
	}

	public function getCurrentVersion() {
		return $this->currentVersion;
	}

	public function getVersions() {
		return $this->versions;
	}

	public function getVersionsButCurrent() {
		$currentVersion = $this->getCurrentVersion();
		return $this->versions->filter(function($fileversion) use ($currentVersion) { return $fileversion !== $currentVersion; });
	}

	// public function setVersions(ArrayCollection $fileversions) {
	// 	foreach ($this->versions as $version) {
	// 		if(!$fileversions->contains($version)) $this->removeVersion($version);
	// 	}
	// 	foreach ($fileversions as $version) {
	// 		$this->addVersion($version);
	// 	}
	// }

	public function addVersion(Fileversion $fileversion, $inverse = true) {
		if(!$this->versions->contains($fileversion)) $this->versions->add($fileversion);
		if($inverse) $fileversion->setFile($this, false);
		$this->defineNbversions();
		return $this;
	}

	public function removeVersion(Fileversion $fileversion, $inverse = true) {
		$this->versions->removeElement($fileversion);
		if($inverse) $fileversion->removeFile(false);
		$this->defineNbversions();
		return $this;
	}

	public function setNbversions($nbversions) {
		$this->nbversions = $nbversions;
		return $this;
	}

	public function getNbversions() {
		return $this->nbversions;
	}

	public function defineNbversions() {
		$this->nbversions = $this->getVersions()->count();
		return $this;
	}

	// Reported methods

	// public function getFileUrl() {
	// 	return $this->getCurrentVersion() instanceOf Fileversion ? $this->getCurrentVersion()->getFileUrl() : null;
	// }

	// public function getFileUrls() {
	// 	return $this->getCurrentVersion() instanceOf Fileversion ? $this->getCurrentVersion()->getFileUrls() : null;
	// }

	public function getB64File() {
		return $this->getCurrentVersion() instanceOf Fileversion ? $this->getCurrentVersion()->getB64File() : null;
	}

	public function getOriginalFileDir() {
		return $this->getCurrentVersion() instanceOf Fileversion ? $this->getCurrentVersion()->getOriginalFileDir() : null;
	}

	public function getFileformat() {
		return $this->getCurrentVersion() instanceOf Fileversion ? $this->getCurrentVersion()->getFileformat() : null;
	}

	public function getMediatype() { // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		return $this->mediatype;
	}

	public function getFileSize() { // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		return $this->fileSize;
	}

	public function getFileExtension() { // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		return $this->fileExtension;
	}

	// public function getMediaType() {
	// 	return $this->getCurrentVersion() instanceOf Fileversion ? $this->getCurrentVersion()->getMediaType() : null;
	// }

	public function getFileName() {
		return $this->getCurrentVersion() instanceOf Fileversion ? $this->getCurrentVersion()->getFileName() : $this->getName();
	}

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['original'];
	}

	/**
	 * Get authorized types of media (image, pdf, video, audio…)
	 * @return array
	 */
	public static function getAuthorizedTypes() {
		// return Fileformat::getTypes(); // ---> For all types authorized
		// $Atypes = Fileformat::getTypes();
		$shortname = strtolower((new ReflectionClass(static::class))->getShortName());
		switch ($shortname) {
			case 'image': $Atypes = ['image']; break;
			case 'video': $Atypes = ['video']; break;
			case 'audio': $Atypes = ['audio']; break;
			case 'pdf': $Atypes = ['pdf']; break;
			case 'text': $Atypes = ['text']; break;
			case 'html': $Atypes = ['text']; break;
			case 'twig': $Atypes = ['text']; break;
			case 'pageweb': $Atypes = ['text']; break;
			case 'css': $Atypes = ['text']; break;
			case 'javascript': $Atypes = ['javascript']; break;
			default:
				$Atypes = Fileformat::getTypes(); // ---> For all types authorized
				break;
		}
		return $Atypes;
	}

	/**
	 * is authorized types of media (image, pdf, video…)
	 * @return boolean
	 */
	public function isAuthorizedType() {
		return $this->getCurrentVersion()->isAuthorizedType();
	}


	/*************************************************************************************/
	/*** NAME FOR FILES
	/*************************************************************************************/

	/**
	 * Is name valid
	 * @return boolean
	 */
	public function isNameValid() {
		return FileManager::isValidFilename($this->name);
	}




	/*************************************************************************************/
	/*** DOWNLOADABLE
	/*************************************************************************************/

	// /**
	//  * Set downloadable
	//  * @param boolean $downloadable
	//  * @return ModelMedia
	//  */
	// public function setDownloadable($downloadable) {
	// 	$this->downloadable = $downloadable;
	// 	return $this;
	// }

	// /**
	//  * Get downloadable
	//  * @return boolean
	//  */
	// public function getDownloadable() {
	// 	return $this->downloadable;
	// }

	// /**
	//  * Is downloadable
	//  * @return boolean
	//  */
	// public function isDownloadable() {
	// 	return $this->getDownloadable();
	// }

	// /**
	//  * Toogle downloadable
	//  * @return boolean
	//  */
	// public function toogleDownloadable() {
	// 	$this->downloadable = !$this->downloadable;
	// 	return $this->downloadable;
	// }


	/*************************************************************************************/
	/*** SENDABLE
	/*************************************************************************************/

	// /**
	//  * Set sendable
	//  * @param boolean $sendable
	//  * @return ModelMedia
	//  */
	// public function setSendable($sendable) {
	// 	$this->sendable = $sendable;
	// 	return $this;
	// }

	// /**
	//  * Get sendable
	//  * @return boolean
	//  */
	// public function getSendable() {
	// 	return $this->sendable;
	// }

	// /**
	//  * Is sendable
	//  * @return boolean
	//  */
	// public function isSendable() {
	// 	return $this->getSendable();
	// }

	// /**
	//  * Toogle sendable
	//  * @return boolean
	//  */
	// public function toogleSendable() {
	// 	$this->sendable = !$this->sendable;
	// 	return $this->sendable;
	// }

	/**
	 * Set subtype
	 * @param string $subtype = null (null is auto)
	 * @return File
	 */
	public function setSubtype($subtype = null) {
		$this->subtype = $subtype;
		if(null === $this->subtype) $this->subtype = $this->getCurrentVersion() instanceOf Fileversion ? $this->getCurrentVersion()->getSubtype() : null;
		return $this;
	}

	/**
	 * Get subtype
	 * @return string
	 */
	public function getSubtype() {
		return $this->subtype;
	}

	public function getUploadFile() {
		return $this->uploadFile;
	}

	/**
	 * Set uploadFile
	 * @param string $uploadFile = null
	 * @return File
	 */
	public function setUrlAsVersion($url) {
		return $this->setUploadFile($url);
	}

	/**
	 * Set uploadFile
	 * @param string $uploadFile = null
	 * @return File
	 */
	public function setStringAsVersion($stringAsVersion = null) {
		if(!is_string(static::DEFAULT_ASSTRING_EXTENSION)) throw new Exception("Error ".__METHOD__."(): constant DEFAULT_ASSTRING_EXTENSION must be defined in entity ".json_encode($this->getShortname())."!", 1);
		// if(null === $this->getCurrentVersion() || $stringAsVersion !== $this->getCurrentVersion()->getBinaryFile()) {
		if(is_string($stringAsVersion) && (null === $this->getCurrentVersion() || $stringAsVersion !== $this->getStringAsVersion())) {
			$path = __DIR__.'/../../../../web/tmp/';
			$file = $this->getName().'.'.static::DEFAULT_ASSTRING_EXTENSION;
			serviceTools::createPath($path);
			serviceTools::removeFile($path, $file);
			file_put_contents($path.$file, $stringAsVersion);
			$this->setUploadFile($path.$file);
			$this->stringAsVersion = $stringAsVersion;
			$this->changedCurrentVersion = true;
			// serviceTools::removeFile($path, $file);
		}
		return $this;
	}

	public function getStringAsVersion(Fileversion $version = null) {
		if(empty($version)) $version = $this->getCurrentVersion();
		if($version instanceOf Fileversion) return $this->stringAsVersion = $version->getBinaryFile();
		return $this->stringAsVersion;
	}

	public function changedCurrentVersion() {
		return $this->changedCurrentVersion;
	}

	/**
	 * Set uploadFile
	 * @param string $uploadFile = null
	 * @return File
	 */
	public function setUploadFile($uploadFile = null) {
		// if(!($uploadFile instanceOf UploadedFile)) {
		// 	if(is_string($uploadFile)) echo('<div>Set Upload file: '.$uploadFile.' '.(@file_exists($uploadFile) ? 'EXISTS' : 'DOES NOT EXIST').'!</div>');
		// 		else if(empty($uploadFile)) echo('<div>Set Upload file: EMPTY!</div>');
		// } else {
		// 	echo('<div>Set Upload file: '.get_class($uploadFile).' '.(@file_exists($uploadFile) ? 'EXISTS' : 'DOES NOT EXIST').'!</div>');
		// }
		// die();
		if(empty($this->id) && empty($uploadFile)) throw new Exception("Error line ".__LINE__." ".__METHOD__.": this ".$this->getShortname()." ".$this->getNameOrTempname()." requires a uploaded file, but got ".(is_object($uploadFile) ? get_class($uploadFile) : gettype($uploadFile)." ".json_encode($uploadFile))."!", 1);
		//if(is_string($uploadFile) || $uploadFile instanceOf UploadedFile) {
		if(!empty($uploadFile)) {
			// DevTerminal::Info('     --> Set file as URL: '.json_encode(@file_exists($uploadFile)).' > '.$uploadFile);
			// $this->uploadFile = $uploadFile;
			$currentVersion = new Fileversion($this);
			$currentVersion->setUploadFile($uploadFile);
			$this->uploadFile = $currentVersion->getUploadFile();
			// DevTerminal::Warning('     --> UploadedFile: '.json_encode($this->uploadFile->getClientOriginalName()));
			$this->setCurrentVersion($currentVersion);
			// $this->fileSize = $this->uploadFile->getClientSize();
		}
		return $this;
	}

}




