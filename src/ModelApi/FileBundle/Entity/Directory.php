<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
// use Doctrine\Common\Collections\ArrayCollection;
use Hateoas\Configuration\Annotation as Hateoas;
// use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\ORM\Event\LifecycleEventArgs;

use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Item;
// use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Entity\DirectoryInterface;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
use ModelApi\FileBundle\Service\serviceDirectory;
use ModelApi\FileBundle\Repository\DirectoryRepository;
// UserBundle
// use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
// use ModelApi\UserBundle\Entity\OwnerTierInterface;
// DevprintBundle
// use ModelApi\DevprintsBundle\Service\DevTerminal;

use \ReflectionClass;
use \Exception;

/**
 * Directory
 * 
 * @ORM\Entity(repositoryClass=DirectoryRepository::class)
 * @ORM\Table(name="`directory`", options={"comment":"Directories"})
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_directory",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER")
 */
class Directory extends Basedirectory {

	const DEFAULT_ICON = 'fa-folder';
	const DEFAULT_OWNER_DIRECTORY = '@@@drive_documents@@@';
	const ENTITY_SERVICE = serviceDirectory::class;


	public function __construct() {
		parent::__construct();
		$this->typedir = serviceBasedirectory::TYPEDIR_REGULAR;
		return $this;
	}

	// public function setOwnerdrivedir(Item $ownerdrivedir, $inverse = true) {
	// 	// A Directory does NEVER have directorys!!!
	// 	$this->ownerdrivedir = null;
	// 	return $this;
	// }

	// public function setOwnersystemdir(Item $ownersystemdir, $inverse = true) {
	// 	// A Directory does NEVER have directorys!!!
	// 	$this->ownersystemdir = null;
	// 	return $this;
	// }

	public function setDriveDir(Directory $driveDir = null, $inverse = true, $force = false) {
		// A Directory does NEVER have directorys!!!
		$this->driveDir = null;
		return false;
	}

	public function setSystemDir(Directory $systemDir = null, $inverse = true, $force = false) {
		// A Directory does NEVER have directorys!!!
		$this->systemDir = null;
		return false;
	}





}




