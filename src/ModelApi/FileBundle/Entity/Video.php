<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Inflector\Inflector;

// BaseBundle
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// FileBundle
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceVideo;

use \Exception;

/**
 * Video
 * 
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\VideoRepository")
 * @ORM\Table(
 *      name="`video`",
 *		options={"comment":"Videos - media"}
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_video",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 */
class Video extends File {

	const DEFAULT_ICON = 'fa-file-video-o';
	const DEFAULT_OWNER_DIRECTORY = '/Video';
	const ENTITY_SERVICE = serviceVideo::class;

	const TYPEVIDEO_FILE = 'file';
	const TYPEVIDEO_URL = 'url';

	/**
	 * Upload file
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=5,
	 * 		options={"required"=false, "attr"={"accept" = "auto"}, "label"="Fichier vidéo", "description"="Sélectionnez votre fichier vidéo. Le poids des fichiers vidéo est souvent très lourd. Il se peut que le chargement soit très long, voire que le serveur refuse des fichiers trop volumineux. Dans ce cas, contactez le webmaster pour faire augmenter la capacité de chargement. À noter que des vidéos très lourdes sont susceptibles de ralentir le site de manière significative. Privilégiez le lien vidéo (Youtube, etc.) chaque vois que cela est possible."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=5,
	 * 		options={"required"=false, "attr"={"accept" = "auto"}, "label"="Fichier vidéo", "description"="Sélectionnez votre fichier vidéo. Le poids des fichiers vidéo est souvent très lourd. Il se peut que le chargement soit très long, voire que le serveur refuse des fichiers trop volumineux. Dans ce cas, contactez le webmaster pour faire augmenter la capacité de chargement. À noter que des vidéos très lourdes sont susceptibles de ralentir le site de manière significative. Privilégiez le lien vidéo (Youtube, etc.) chaque vois que cela est possible."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=200)
	 */
	protected $uploadFile;

	/**
	 * @var array
	 * Types de vidéo : Fichier, url ou les 2
	 * @ORM\Column(name="typevideos", type="json_array", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=4,
	 * 		options={
	 * 			"label"="Priorité source",
	 * 			"required"=true,
	 * 			"multiple"=true,
	 * 			"expanded"=false,
	 * 			"description"="Préciser les sources possibles pour la vidéo dans le cas où elle contient plusieurs sources (fichier et lien URL). <strong>Le lien url sera utilisé en priorité afin de préserver les ressources du serveur</strong>.",
	 * 			"choices"="auto"
	 * 		},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=4,
	 * 		options={
	 * 			"label"="Priorité source",
	 * 			"required"=true,
	 * 			"multiple"=true,
	 * 			"expanded"=false,
	 * 			"description"="Préciser les sources possibles pour la vidéo dans le cas où elle contient plusieurs sources (fichier et lien URL). <strong>Le lien url sera utilisé en priorité afin de préserver les ressources du serveur</strong>.",
	 * 			"choices"="auto"
	 * 		},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * )
	 * @CRUDS\Show(role=true)
	 */
	protected $typevideos;
	/**
	 * @var string
	 * Type de vidéo : Fichier, url ou les 2
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show=true,
	 * 		update=true,
	 * 		order=4,
	 * 		options={
	 * 			"label"="Priorité source",
	 * 			"required"=true,
	 * 			"multiple"=false,
	 * 			"expanded"=false,
	 * 			"description"="Dans le cas où la vidéo est définie par les deux sosurces (fichier + lien URL), préciser quelle source sera prioritaire lors de l'affichage sur le site. <strong>Le lien url sera utilisé en priorité afin de préserver les ressources du serveur</strong>.",
	 * 			"choices"="auto"
	 * 		},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show=true,
	 * 		update=true,
	 * 		order=4,
	 * 		options={
	 * 			"label"="Priorité source",
	 * 			"required"=true,
	 * 			"multiple"=false,
	 * 			"expanded"=false,
	 * 			"description"="Dans le cas où la vidéo est définie par les deux sosurces (fichier + lien URL), préciser quelle source sera prioritaire lors de l'affichage sur le site. <strong>Le lien url sera utilisé en priorité afin de préserver les ressources du serveur</strong>.",
	 * 			"choices"="auto"
	 * 		},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * )
	 * @CRUDS\Show(role=true)
	 */
	protected $typevideo;

	/**
	 * @var string
	 * Text - Identifiant du fichier
	 * @ORM\Column(name="url", type="string", nullable=true, unique=false)
	 * @CRUDS\Create(show=true, update=true, type="TextType", order=4, options={"label"="Lien url", "required"=false, "description"="Vous pouvez copier-coller ici le lien url de votre vidéo."}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Update(show=true, update=true, type="TextType", order=4, options={"label"="Lien url", "required"=false, "description"="Vous pouvez copier-coller ici le lien url de votre vidéo."}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Show(role=true)
	 */
	protected $url;

	/**
	 * @var array
	 * @ORM\Column(name="urlinfo", type="json_array", nullable=true, unique=false)
	 */
	protected $urlinfo;

	/**
	 * @var string
	 * Text - Identifiant du fichier
	 * @CRUDS\Create(show=true, update=true, type="TextType", order=2, options={"label"="Identifiant de la vidéo", "required"=true, "description"="Identifiant du fichier <strong>ne pas utiliser d'extension (mp4, mpeg, etc.)</strong>"}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Update(show=true, update=true, type="TextType", order=2, options={"label"="Identifiant de la vidéo", "required"=true, "description"="Identifiant du fichier <strong>ne pas utiliser d'extension (mp4, mpeg, etc.)</strong>"}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Show(role=true)
	 */
	protected $inputname;

	/**
	 * @var string
	 * Text - Nom du fichier
	 * @CRUDS\Create(show=false, update=false, type="TextType", order=3, options={"label"="Nom du fichier", "required"=true, "description"="Nom du fichier vidéo"}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Update(show=false, update=false, type="TextType", order=3, options={"label"="Nom du fichier", "required"=true, "description"="Nom du fichier vidéo"}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Show(role=true)
	 */
	protected $filename;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $name;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $title;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $subtitle;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $menutitle;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $accroche;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $resume;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $fulltext;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $color;


	public function __construct() {
		parent::__construct();
		$this->setNormalizeNameMethod(FileManager::NORMALIZE_METHODS[2]);
		$this->setCroppable(false);
		$this->getTypevideos();
		$this->url = null;
		$this->urlinfo = [];
		return $this;
	}


	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['original','realsize'];
	}

	// /**
	//  * Get authorized types of media (image, pdf, video, audio…)
	//  * @return array
	//  */
	// public static function getAuthorizedTypes() {
	// 	// return Fileformat::getTypes(); // ---> For all types authorized
	// 	return ['video'];
	// }



	/*************************************************************************************/
	/*** OVERRIDE FILE VALIDATION CONSTRAINTS
	/*************************************************************************************/

	/**
	 * Is content-type valid
	 * @return boolean
	 */
	public function isContentTypeValid() {
		if(empty($this->getCurrentVersion()) && empty($this->uploadFile) && !empty($this->url)) return true;
		return parent::isContentTypeValid();
	}

	protected function defineValuesIfOnlyUrl() {
		if(!empty($this->url) && empty($this->uploadFile) && empty($this->currentVersion)) {
			$this->nbversions = $this->versions->count();
			$this->fileUrl = $this->url;
			$parsed = parse_url($this->url);
			$this->name ??= preg_replace('/\\./', '_', $parsed['host']).'_'.$this->urlinfo['id'];
			$this->fileSize = 0;
			$this->fileExtension = null;
			$this->mediatype = null;
			$this->subtype = null;
		}
		return $this;
	}


	/*************************************************************************************/
	/*** UPLOAD FILE
	/*************************************************************************************/

	public function isUploadFileValid() {
		return $this->uploadFile instanceOf UploadedFile || !empty($this->currentVersion) || !empty($this->url);
	}


	/*************************************************************************************/
	/*** URL
	/*************************************************************************************/

	public function getUrl() {
		return $this->url;
	}

	public static function idValidVideoUrlId($id) {
		return preg_match('/^[\\.\\w_-]{9,}$/', $id);
	}

	/**
	 * @see https://stackoverflow.com/questions/9522868/how-do-i-get-a-youtube-video-id-php
	 */
	public function setUrl($url = null) {
		$this->url = $url;
		$this->updateUrlinfo($this->url);
		if(!empty($this->url) && !empty($this->urlinfo)) $this->addTypevideo(static::TYPEVIDEO_FILE);
		return $this;
	}

	public function isValidUrl() {
		if(empty($this->url)) return true;
		return !empty($this->urlinfo);
	}

	public function updateUrlinfo($url = null) {
		$this->urlinfo = [];
		if(!empty($url)) {
			$parsed = parse_url($url);
			if(!$parsed) return $this->urlinfo;
			switch (strtolower($parsed['host'])) {
				case 'vimeo.com':
				case 'rumble.com':
				case 'www.vimeo.com':
				case 'www.rumble.com':
					/**
					 * TEST VIMEO
					 * @see https://vimeo.com/53138786
					 * @see https://rumble.com/vfl17x-baby-groundhog-enjoys-apple-slices-until-hungry-seagull-startles-him.html */
					$path = preg_split('/\\//', $parsed['path'], -1, PREG_SPLIT_NO_EMPTY);
					$name = preg_split('/\\./', $parsed['host']);
					$name = reset($name) === "www" ? $name[1] : reset($name);
					if(count($path) && static::idValidVideoUrlId(reset($path))) {
						$this->urlinfo = [
							'id' => reset($path),
							'url' => $url,
							'plateform' => $name,
							'host' => $parsed['host'],
							'scheme' => $parsed['scheme'],
						];
					}
					break;
				case 'www.youtu.be':
				case 'youtu.be':
					/**
					 * TEST YOUTUBE 2
					 * @see https://youtu.be/_m4eWB0JJbE */
					$path = preg_split('/\\//', $parsed['path'], -1, PREG_SPLIT_NO_EMPTY);
					if(count($path) && static::idValidVideoUrlId(reset($path))) {
						$id = reset($path);
						$this->urlinfo = [
							'id' => reset($path),
							'url' => $url,
							'plateform' => 'youtube',
							'host' => $parsed['host'],
							'scheme' => $parsed['scheme'],
						];
					}
					break;
				case 'youtube.com':
				case 'www.youtube.com':
					/**
					 * TEST YOUTUBE 1
					 * @see https://www.youtube.com/watch?v=_m4eWB0JJbE */
					parse_str(parse_url($url, PHP_URL_QUERY), $querys);
					if(isset($querys['v']) && static::idValidVideoUrlId($querys['v'])) {
						$this->urlinfo = [
							'id' => $querys['v'],
							'url' => $url,
							'plateform' => 'youtube',
							'host' => $parsed['host'],
							'scheme' => $parsed['scheme'],
						];
					}
					break;
				case 'facebook.com':
				case 'www.facebook.com':
					/**
					 * TEST FACEBOOK
					 * @see https://developers.facebook.com/docs/plugins/embedded-video-player */
					$path = preg_split('/\\//', $parsed['path'], -1, PREG_SPLIT_NO_EMPTY);
					$name = preg_split('/\\./', $parsed['host']);
					$name = reset($name) === "www" ? $name[1] : reset($name);
					if(count($path) && static::idValidVideoUrlId(reset($path))) {
						$this->urlinfo = [
							'id' => reset($path),
							'url' => $url,
							'plateform' => 'facebook',
							'host' => $parsed['host'],
							'scheme' => $parsed['scheme'],
						];
					}
					break;
			}
		}
		$this->defineValuesIfOnlyUrl();
		//throw new Exception("URL Info: ".json_encode($this->urlinfo), 1);
		return $this->urlinfo;
	}

	// protected function setUrlinfo($urlinfo) {
	// 	$this->urlinfo = $urlinfo;
	// 	return $this;
	// }

	public function getUrlinfo() {
		return $this->urlinfo;
	}

	public function getEmbedVideo($options = []) {
		if(!array_key_exists('plateform', $this->urlinfo)) return null;
		$deco_begin = '<div style="padding:56.25% 0 0 0;position:relative;">';
		$deco_end = '</div>';
		switch ($this->urlinfo['plateform']) {
			case 'vimeo':
				// return '<iframe src="https://player.vimeo.com/video/'.urlencode($this->urlinfo['id']).'?color=ffffff&title=0&byline=0&portrait=0&badge=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>';
				return $deco_begin.'<iframe src="https://player.vimeo.com/video/'.urlencode($this->urlinfo['id']).'?autoplay=1&color=ffffff&title=0&byline=0&portrait=0&badge=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe><script src="https://player.vimeo.com/api/player.js"></script>'.$deco_end;
				break;
			case 'rumble':
				// return '<iframe src="https://player.vimeo.com/video/'.urlencode($this->urlinfo['id']).'?color=ffffff&title=0&byline=0&portrait=0&badge=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>';
				return $deco_begin.'<iframe src="https://rumble.com/'.urlencode($this->urlinfo['id']).'" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>'.$deco_end;
				break;
			case 'youtube':
				// return '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.urlencode($this->urlinfo['id']).'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
				return $deco_begin.'<iframe style="position:absolute;top:0;left:0;width:100%;height:100%;" src="https://www.youtube.com/embed/'.urlencode($this->urlinfo['id']).'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'.$deco_end;
				break;
			case 'facebook':
				// return '<iframe width="560" height="315" src="https://www.facebook.com/embed/'.urlencode($this->urlinfo['id']).'" title="facebook video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
				return $deco_begin.'<iframe src="https://www.facebook.com/plugins/video.php?height=280&href='.urlencode($this->urlinfo['id']).'%2F&show_text=false&width=560&t=0" width="560" height="280" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>'.$deco_end;
				break;
		}
		return null;
	}

	public function getVideoIcon() {
/*		if(!array_key_exists('plateform', $this->urlinfo)) return $this->getIcon();
		switch ($this->urlinfo['plateform']) {
			case 'vimeo':
				return 'fa-vimeo';
				break;
			case 'youtube':
				return 'fa-youtube-play';
				break;
			case 'facebook':
				return 'fa-facebook';
				break;
			default:
				return $this->getIcon();
				break;
		}*/
		return $this->getIcon();
	}


	public function getIcon() {
		if($this->isUrl()) {
			switch ($this->getTypevideo()) {
				case static::TYPEVIDEO_URL:
				// TYPE URL
					switch ($this->urlinfo['plateform']) {
						case 'vimeo':
							return 'fa-vimeo';
							break;
						case 'youtube':
							return 'fa-youtube-play';
							break;
						case 'facebook':
							return 'fa-facebook';
							break;
						default:
							return $this->icon;
							break;
					}
					break;
				default:
					// TYPE FILE
					return $this->icon;
					break;
			}
		}
		return $this->icon;
	}



	/*************************************************************************************/
	/*** TYPES VIDEO
	/*************************************************************************************/

	protected function getBestTypevideo() {
		$availables = $this->getAvailableTypevideos();
		$typevideos = $this->getTypevideosChoices();
		return empty($availables) ? reset($typevideos) : reset($availables);
	}

	public function getTypevideo() {
		//$this->computeTypevideos(false);
		return count($this->typevideos) > 1 ? $this->getBestTypevideo() : reset($this->typevideos);
	}

	public function setTypevideo(string $typevideo) {
		$this->typevideos = [strtolower($typevideo)];
		$this->computeTypevideos(false);
		return $this;
	}

	public function getAvailableTypevideos() {
		$typevideos = [];
		foreach ($this->getTypevideosChoices(true) as $typevideo) {
			$test = Inflector::camelize('is_'.$typevideo);
			if($this->$test()) $typevideos[] = $typevideo;
		}
		return $typevideos;
	}

	public function getTypevideos() {
		$this->computeTypevideos(empty($this->typevideos));
		return $this->typevideos;
	}

	public function setTypevideos($typevideos) {
		$this->typevideos = [];
		foreach ($typevideos as $typevideo) {
			$this->addTypevideo($typevideo);
		}
		$this->computeTypevideos(empty($this->typevideos));
		return $this;
	}

	public function addTypevideo($typevideo) {
		$typevideo = strtolower($typevideo);
		if(!in_array($typevideo, $this->typevideos)) $this->typevideos[] = $typevideo;
		return $this;
	}

	public function removeTypevideo($typevideo) {
		$this->typevideos = array_filter((array)$this->typevideos, function($tv) use ($typevideo) { return $tv !== strtolower($typevideo); });
		return $this;
	}

	public function getTypevideoChoices($asValues = false) {
		return $this->getTypevideosChoices($asValues);
	}
	public function getTypevideosChoices($asValues = false) {
		// Gives typevideos in order of preference, please
		$list = [
			ucfirst(static::TYPEVIDEO_URL) => strtolower(static::TYPEVIDEO_URL),
			ucfirst(static::TYPEVIDEO_FILE) => strtolower(static::TYPEVIDEO_FILE), 
		];
		return $asValues ? array_values($list) : $list;
	}

	/**
	 * @Annot\PostCreate()
	 */
	public function computeTypevideos($recompute = false) {
		if($recompute || empty($this->typevideos)) {
			$this->typevideos = $this->getTypevideosChoices(true);
		} else {
			$this->typevideos = array_unique(array_filter((array)$this->typevideos, function($typevideo) {
					return in_array($typevideo, $this->getTypevideosChoices());
				}
			));
		}
		foreach ($this->getTypevideosChoices(true) as $typevideo) {
			$test = Inflector::camelize('is_not_'.$typevideo);
			switch ($typevideo) {
				case static::TYPEVIDEO_FILE:
					if($this->$test()) {
						$this->removeTypevideo($typevideo);
					}
					break;
				case static::TYPEVIDEO_URL:
					if($this->$test()) {
						$this->url = null;
						$this->urlinfo = [];
						$this->removeTypevideo($typevideo);
					}
					break;
			}
		}
		if(empty($this->typevideos)) $this->addTypevideo($this->getBestTypevideo());
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 */
	public function prePersistTypevideos() {
		$this->computeTypevideos(false);
		$this->defineValuesIfOnlyUrl();
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdateTypevideos() {
		$this->computeTypevideos(false);
		$this->defineValuesIfOnlyUrl();
		return $this;
	}


	/*************************************************************************************/
	/*** TESTS TYPES VIDEO
	/*************************************************************************************/

	public function isBothFileAndUrl() {
		return $this->isFile() && $this->isUrl();
	}

	public function isFileRequired() {
		return $this->isFile() && empty($this->getCurrentVersion());
	}

	public function isFile() {
		return $this->getCurrentVersion() instanceOf Fileversion;
	}

	public function isNotFile() {
		return !$this->isFile();
	}

	public function isOnlyFile() {
		return $this->getCurrentVersion() instanceOf Fileversion && !$this->isUrl();
	}

	public function isUrl() {
		return !empty($this->url) && !empty($this->urlinfo);
	}

	public function isNotUrl() {
		return !$this->isUrl();
	}

	public function isOnlyUrl() {
		return $this->isUrl() && $this->isNotFile();
	}



}




