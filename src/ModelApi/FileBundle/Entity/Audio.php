<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;

// FileBundle
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Service\serviceAudio;

use \Exception;

/**
 * Audio
 * 
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\AudioRepository")
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(
 *      name="`audio`",
 *		options={"comment":"Audios - media"}
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_audio",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 */
class Audio extends File {

	const DEFAULT_ICON = 'fa-file-audio-o';
	const ENTITY_SERVICE = serviceAudio::class;

	/**
	 * Upload file
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=150,
	 * 		options={"required"=true, "attr"={"accept" = "auto"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=150,
	 * 		options={"required"=false, "attr"={"accept" = "auto"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=200)
	 */
	protected $uploadFile;

	public function __construct() {
		parent::__construct();
		$this->setNormalizeNameMethod(FileManager::NORMALIZE_METHODS[2]);
		$this->setCroppable(false);
		return $this;
	}


	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['original'];
	}



}




