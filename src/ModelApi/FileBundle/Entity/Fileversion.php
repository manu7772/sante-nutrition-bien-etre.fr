<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;
// FileBundle
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Service\serviceFileversion;

use \Exception;
use \DateTime;

/**
 * Fileversion
 *
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\FileversionRepository")
 * @ORM\Table(
 *      name="`fileversion`",
 *		options={"comment":"File versions"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_fileversion",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER")
 */
class Fileversion extends Binary {

	const DEFAULT_ICON = 'fa-file-archive-o';
	const ENTITY_SERVICE = serviceFileversion::class;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\File", inversedBy="versions", cascade={"persist"})
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 */
	protected $file;

	// protected $name;
	protected $authorizedTypes;

	public function __construct(File $file = null) {
		parent::__construct();
		if($file instanceOf File) $this->setFile($file);
		$this->init_BaseEntity();
		$this->initCreated();
		$this->init_ClassDescriptor();
		$this->init_BinaryFile();
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function updateFile() {
		if($this->isCurrentVersion()) $this->file->setUpdated();
		return $this;
	}

	public function isCurrentVersion() {
		return $this->file->getCurrentVersion() === $this;
	}

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return $this->file->getDefaultDeclinationsNames();
	}

	/**
	 * Get authorized types of media (image, pdf, video…)
	 * @return array
	 */
	public function getAuthorizedTypes() {
		$this->authorizedTypes = $this->file instanceOf File ? $this->file->getAuthorizedTypes() : [];
		return $this->authorizedTypes;
	}

	public function getIcon() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Set File
	 * @param File $file
	 * @return fileversion
	 */
	public function setFile(File $file, $inverse = false) {
		$this->file = $file;
		if($inverse) $this->file->addVersion($this, false);
		$this->getAuthorizedTypes();
		return $this;
	}

	/**
	 * Get File
	 * @return File
	 */
	public function getFile() {
		return $this->file;
	}

	/**
	 * @ORM\PreRemove()
	 * Remove File
	 * @return File
	 */
	public function removeFile($inverse = true) {
		if($inverse) $this->file->removeVersion($this, false);
		$this->file = null;
		$this->getAuthorizedTypes();
	}

	/**
	 * Get name
	 * @return string
	 */
	public function getName() {
		return $this->file->getName();
	}



}