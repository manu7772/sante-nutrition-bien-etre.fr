<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;

// BaseBundle
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// FileBundle
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceImage;


use \Exception;

/**
 * Image
 * 
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\ImageRepository")
 * @ORM\Table(
 *      name="`image`",
 *		options={"comment":"Images"}
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_image",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER", label="Image")
 * @Annot\HasTranslatable()
 */
class Image extends File {

	const DEFAULT_ICON = 'fa-file-image-o';
	const DEFAULT_OWNER_DIRECTORY = '/Images';
	const ENTITY_SERVICE = serviceImage::class;

	/**
	 * Upload file
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=150,
	 * 		options={"required"=true, "disabled"=false, "by_reference"=false, "attr"={"accept" = "auto"}},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=150,
	 * 		options={"required"=false, "disabled"=false, "by_reference"=false, "attr"={"accept" = "auto"}},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=200)
	 */
	protected $uploadFile;


	public function __construct() {
		parent::__construct();
		$this->setNormalizeNameMethod(FileManager::NORMALIZE_METHODS[2]);
		$this->setCroppable(false);
		return $this;
	}

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		$ddn = ['original','cropper','optimized_W128','optimized_W512','optimized_W800','optimized_W1024','optimized_W1440','thumbnail_32x32','thumbnail_64x64','thumbnail_128x128','thumbnail_512x512','formated_32x32','formated_64x64','formated_128x128','formated_600x600','formated_800x600'];
		if(preg_match('#^gif$#i', $this->getFileExtension())) $ddn[] = 'realsize';
		return $ddn;
	}




}




