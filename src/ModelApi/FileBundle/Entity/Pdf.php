<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;

// BaseBundle
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// FileBundle
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Service\servicePdf;

/**
 * Pdf
 * 
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\PdfRepository")
 * @ORM\Table(
 *      name="`pdf`",
 *		options={"comment":"Pdfs - media"}
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_pdf",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 */
class Pdf extends File {

	const DEFAULT_ICON = 'fa-file-pdf-o';
	const DEFAULT_OWNER_DIRECTORY = '/Pdf';
	const ENTITY_SERVICE = servicePdf::class;
	const DEFAULT_ASSTRING_EXTENSION = 'pdf';

	/**
	 * Upload file
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=150,
	 * 		options={"required"=true, "label"="Fichier PDF", "attr"={"accept" = "auto"}},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=150,
	 * 		options={"required"=false, "label"="Fichier PDF", "attr"={"accept" = "auto"}},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", label="Fichier PDF", order=200)
	 */
	protected $uploadFile;

	/**
	 * @var string
	 * @ORM\Column(name="readerurl", type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=210,
	 * 		options={"required"=false, "label"="Lien URL", "description"="Si votre fichier PDF est publié sur un site (Calaméo, etc.), vous pouvez indiquer ici le lien vers votre PDF en ligne."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=210,
	 * 		options={"required"=false, "label"="Lien URL", "description"="Si votre fichier PDF est publié sur un site (Calaméo, etc.), vous pouvez indiquer ici le lien vers votre PDF en ligne."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=210)
	 */
	protected $readerurl;


	public function __construct() {
		parent::__construct();
		$this->readerurl = null;
		$this->setNormalizeNameMethod(FileManager::NORMALIZE_METHODS[2]);
		$this->setCroppable(false);
		return $this;
	}

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['original','realsize','optimized_W512','optimized_W800','optimized_W1024','thumbnail_64x64','thumbnail_256x256','thumbnail_64x64','thumbnail_256x256'];
	}

	// /**
	//  * Get authorized types of media (image, pdf, video, audio…)
	//  * @return array
	//  */
	// public static function getAuthorizedTypes() {
	// 	// return Fileformat::getTypes(); // ---> For all types authorized
	// 	return ['pdf'];
	// }

	public function setReaderurl($readerurl) {
		$this->readerurl = $readerurl;
		return $this;
	}

	public function getReaderurl() {
		return $this->readerurl;
	}

}




