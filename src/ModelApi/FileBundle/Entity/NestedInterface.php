<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;

use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Service\serviceBasedirectory;

interface NestedInterface {

	// /**
	//  * Get valid children
	//  * @return array <string>
	//  */
	// public function getValidDirchilds();

	public function initItem(serviceBasedirectory $serviceBasedirectory);

	/**
	 * is DirectoryInterface
	 * @return boolean
	 */
	public function isDirInterface();

	// /**
	//  * Get parent Directorys
	//  * @return ArrayCollection <Basedirectory>
	//  */
	// public function getDirparents();

	// /**
	//  * Get all parent Directorys of hierarchy
	//  * @return ArrayCollection <Basedirectory>
	//  */
	// public function getAllDirparents();

	/**
	 * Has parent Directory
	 * @param Basedirectory $dirparent = null
	 * @param boolean $recursive = false
	 * @return boolean
	 */
	public function hasDirparent(Basedirectory $dirparent = null, $recursive = false);

	// /**
	//  * Add parent Directory
	//  * @param Basedirectory $dirparent
	//  * @param integer position = -1
	//  * @return string (typelink)
	//  */
	// public function addDirparent(Basedirectory $dirparent, $position = -1);

	// /**
	//  * Remove parent Directory
	//  * @param Basedirectory $dirparent
	//  * @return boolean (true if $dirparent has been removed)
	//  */
	// public function removeDirparent(Basedirectory $dirparent);

	/**
	 * Get (solid) parent Directory
	 * @param boolean $onlyValids = true
	 * @return Basedirectory | null
	 */
	public function getParent();

	/**
	 * Can have parent Directory
	 * @param Basedirectory $parent = null
	 * @return boolean
	 */
	public function isChildeable(Basedirectory $parent = null);

	/**
	 * Set solid parent Directory
	 * @param Basedirectory $parent
	 * @return NestedInterface
	 */
	public function setParent(Basedirectory $parent);

	/**
	 * Set current Parent
	 * @param Basedirectory $parent
	 * @param boolean $force = false
	 * @return Item
	 */
	public function setCurrentParent(Basedirectory $parent, $force = false);

	/**
	 * Remove current Parent
	 * @return Item
	 */
	public function resetCurrentParent();

	public function setDriveDir(Directory $driveDir = null, $inverse = true, $force = false);
	public function getDriveDir();

	public function setSystemDir(Directory $systemDir = null, $inverse = true, $force = false);
	public function getSystemDir();

	// public function getDirPath($property_name);
	// public function defineDirPath($property_name, $path);

	// /**
	//  * Get Item's Directory by path
	//  * @param string $path
	//  * @return Basedirectory | null
	//  */
	// public function getOwnDirectory($path);

	/**
	 * Get Owner's Directory'childs by path
	 * @param string $path
	 * @return ArrayCollection <Item>
	 */
	public function getOwnerDirectoryChilds($path);

	/**
	 * Get Owner's Directory by path
	 * @param string &$pathname
	 * @return Basedirectory | null
	 */
	public function getOwnerDirectory($pathname);

	public function onFlushItemdata(OnFlushEventArgs $args);

	public function postFlushItemdata(PostFlushEventArgs $args);


}

