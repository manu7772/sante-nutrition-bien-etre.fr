<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
// FileBundle
use ModelApi\FileBundle\Service\serviceFileformat;

/**
 * Fileformat
 *
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\FileformatRepository")
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(
 *      name="`fileformat`",
 *		options={"comment":"Files formats"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_fileformat",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show="ROLE_SUPER_ADMIN", create="ROLE_SUPER_ADMIN", update="ROLE_SUPER_ADMIN", delete="ROLE_SUPER_ADMIN")
 */
class Fileformat implements CrudInterface {

	const DEFAULT_ICON = 'fa-superscript';
	const ENTITY_SERVICE = serviceFileformat::class;
	// SUBTYPES
	const SUBTYPE_UNKNOWN = "unknown";
	const SUBTYPE_MEDIA = "media";
	const SUBTYPE_TEXT = "text";
	const SHORTCUT_CONTROLS = true;

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	/**
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(name="extension", type="string", length=255)
	 * @CRUDS\Create(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=300,
	 * 		options={"required"=true, "attr"={"placeholder"="Extension"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=300,
	 * 		options={"required"=true, "attr"={"placeholder"="Extension"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=300)
	 */
	protected $extension;

	/**
	 * @var string
	 * @ORM\Column(name="mediatype", type="string", length=64)
	 * @CRUDS\Create(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "attr"={"placeholder"="Type de media"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "attr"={"placeholder"="Type de media"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=100)
	 */
	protected $mediaType;

	/**
	 * @var string
	 * @ORM\Column(name="subtype", type="string", length=32, nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=101,
	 * 		options={"required"=true, "attr"={"placeholder"="Genre de media"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=101,
	 * 		options={"required"=true, "attr"={"placeholder"="Genre de media"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=101)
	 */
	protected $subtype;

	/**
	 * @var string
	 * @ORM\Column(name="content_type", type="string", length=255)
	 * @CRUDS\Create(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=200,
	 * 		options={"required"=true, "attr"={"placeholder"="Content type"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=200,
	 * 		options={"required"=true, "attr"={"placeholder"="Content type"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=200)
	 */
	protected $contentType;


	protected $typeIcons;

	public function __construct() {
		$this->getTypeIcons();
		$this->name = null;
		$this->extension = null;
		$this->icon = $this->getDefaultIcon();
		$this->mediaType = null;
		$this->subtype = null;
		$this->contentType = null;
	}

	public function __toString(){
		return (string)$this->name;
	}

	/**
	 * Get id
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	public function isValid() {
		return !empty($this->name)
			&& !empty($this->extension)
			&& !empty($this->mediaType)
			&& !empty($this->subtype)
			&& !empty($this->contentType)
			;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * Set name
	 * @param string $name
	 * @return Fileformat
	 */
	public function setName($name = null) {
		if(!serviceTools::computeTextOrNull($name)) $name = $this->getContentType().'@'.$this->getExtension();
		$this->name = serviceTools::removeSeparator($name);
		return $this;
	}

	/**
	 * Get name
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set extension
	 * @param string $extension
	 * @return Fileformat
	 */
	public function setExtension($extension) {
		serviceTools::computeTextOrNull($extension);
		$this->extension = $extension;
		return $this;
	}

	/**
	 * Get extension
	 * @return string
	 */
	public function getExtension() {
		return $this->extension;
	}

	/**
	 * Set icon
	 * @param string $icon = null
	 * @param string $type = 'fa'
	 * @return Fileformat
	 */
	public function setIcon($icon = null, $type = 'fa') {
		$icon = null != $icon ? $icon : $this->getDefaultIcon();
		$icons = $this->getTypeIcons();
		if(in_array($icon, $icons)) {
			switch ($type) {
				case 'fa':
				default:
					$this->icon = $icon;
					break;
			}
		}
		return $this;
	}

	/**
	 * Get icon
	 * @return string
	 */
	public function getIcon() {
		return $this->icon;
	}

	/**
	 * Get types of icons
	 * @return array
	 */
	public function getTypeIcons() {
		$this->typeIcons = array('fa-file-o', 'fa-file-text-o', 'fa-file-archive-o', 'fa-file-audio-o', 'fa-file-code-o', 'fa-file-excel-o', 'fa-file-image-o', 'fa-file-movie-o', 'fa-file-pdf-o', 'fa-file-photo-o', 'fa-file-picture-o', 'fa-file-powerpoint-o', 'fa-file-sound-o', 'fa-file-video-o', 'fa-file-word-o', 'fa-file-zip-o');
		// foreach ($typeIcons as $key => $value) {
		// 	$iconCode = '<i class="fa '.$value.' fa-fw"></i> ';
		// 	$this->typeIcons[$value] = $iconCode.str_replace(array('fa-','-o'), '', $value);
		// }
		return $this->typeIcons;
	}

	/**
	 * Get types of icons
	 * @return array
	 */
	public function getDefaultIcon() {
		$icons = $this->getTypeIcons();
		// reset($icons);
		// return key($icons);
		// $icons = array_keys($icons);
		return reset($icons);
	}

	/**
	 * Set contentType
	 * @param string $contentType
	 * @return Fileformat
	 */
	public function setContentType($contentType) {
		serviceTools::computeTextOrNull($contentType);
		$this->contentType = $contentType;
		$this->setMediaType();
		$this->setSubtype();
		return $this;
	}

	/**
	 * Get contentType
	 * @return string
	 */
	public function getContentType() {
		return $this->contentType;
	}

	static public function getMediaTypeByContentType($contentType) {
		$mediaType = null;
		// Logic
		foreach (static::getTypes() as $type) {
			if(preg_match("#^".$type."\\/#", $contentType) || preg_match("#".$type."$#", $contentType)) return $type;
		}
		// others
		$exp = explode(DIRECTORY_SEPARATOR, $contentType);
		if(isset($exp[0]) && strlen($exp[0]) > 0) {
			return strtolower($exp[0]) === "application" && isset($exp[1]) ? $mediaType = $exp[1] : $mediaType = $exp[0];
		}
		return null;
	}	

	static public function getSubTypeByContentType($contentType) {
		$subtype = static::getMediaTypeByContentType($contentType);
		switch (strtolower($subtype)) {
			case 'image':
			case 'video':
			case 'audio':
			case 'pdf':
				return static::SUBTYPE_MEDIA;
				break;
			case 'text':
			case 'html':
			case 'twig':
			case 'javascript':
			case 'php':
			case 'css':
				return static::SUBTYPE_TEXT;
				break;
			default:
				return static::SUBTYPE_UNKNOWN;
				break;
		}
		return static::SUBTYPE_UNKNOWN;
	}	

	static public function getTypes() {
		return array('image','pdf','audio','video','text','html','twig','music','x-music','message','www','chemical','model','paleovu','x-world','drawing','xml','excel','msword','powerpoint','x-director','octet-stream');
	}

	/**
	 * Set type of media (image, pdf, video…)
	 * @param string $mediatype = null
	 * @return Fileformat
	 */
	public function setMediaType($mediatype = null) {
		$this->mediaType = $mediatype;
		if(null === $this->mediaType) $this->mediaType = $this->getMediaTypeByContentType($this->getContentType());
		return $this;
	}

	/**
	 * Get type of media (image, pdf, video…)
	 * @return string | null
	 */
	public function getMediaType() {
		return $this->mediaType;
	}

	/**
	 * Set subtype
	 * @param string $subtype = null (null is auto)
	 * @return Fileformat
	 */
	public function setSubtype($subtype = null) {
		$this->subtype = $subtype;
		if(null === $this->subtype) $this->subtype = $this->getSubTypeByContentType($this->getContentType());
		return $this;
	}

	/**
	 * Get subtype
	 * @return string
	 */
	public function getSubtype() {
		return $this->subtype;
	}




}
