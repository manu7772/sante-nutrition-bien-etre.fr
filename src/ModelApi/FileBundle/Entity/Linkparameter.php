<?php
// namespace ModelApi\FileBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;
// use ModelApi\AnnotBundle\Annotation as Annot;
// use ModelApi\CrudsBundle\Annotation as CRUDS;
// use Doctrine\Common\Collections\ArrayCollection;
// use Hateoas\Configuration\Annotation as Hateoas;
// use Gedmo\Mapping\Annotation as Gedmo;
// // FileBundle
// // use ModelApi\FileBundle\Entity\Nestlink;
// // CrudsBundle
// use ModelApi\CrudsBundle\Entity\CrudInterface;

// use \Exception;
// use \ReflectionClass;

// /**
//  * Linkparameter
//  * 
//  * @ORM\Entity()
//  * @ORM\Table(name="`linkparameter`", options={"comment":"Link parameters"})
//  * @Hateoas\Relation(
//  *      "self",
//  *      href = @Hateoas\Route(
//  *          "get_linkparameter",
//  *          parameters = { "id" = "expr(object.getId())" }
//  *      )
//  * )
//  * @ORM\HasLifecycleCallbacks
//  * @CRUDS\Actions(show=true, create="ROLE_EDOTOR", update="ROLE_EDOTOR", delete="ROLE_EDOTOR", label="Paramètres du lien")
//  */
// class Linkparameter {

// 	const DEFAULT_ICON = 'fa-cogs';
// 	const PTYPES = ['flixslider'];
// 	const SHORTCUT_CONTROLS = true;

// 	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
// 	use \ModelApi\BaseBundle\Traits\DateUpdater;
// 	use \ModelApi\CrudsBundle\Traits\Cruds;

// 	/**
// 	 * Id of Linkparameter
// 	 * @var integer
// 	 * @ORM\Id
// 	 * @ORM\Column(name="id", type="integer")
// 	 * @ORM\GeneratedValue(strategy="AUTO")
// 	 */
// 	protected $id;

// 	/**
// 	 * @var array
// 	 * @ORM\Column(name="typeparameter", type="string", length=16, nullable=false, unique=false)
// 	 */
// 	protected $typeparameter;

// 	/**
// 	 * @var array
// 	 * @ORM\Column(name="parameters", type="json_array", nullable=false, unique=false)
// 	 */
// 	protected $parameters;

// 	/**
// 	 * @var string
// 	 * !!!@ORM\Column(name="fa_icon", type="string", length=64, nullable=false, unique=false)
// 	 */
// 	protected $icon;


// 	public function __construct() {
// 		$this->id = null;
// 		$type = static::PTYPES;
// 		$this->typeparameter = reset($type);
// 		$this->parameters = [];
// 		$this->setIcon(static::DEFAULT_ICON);
// 		return $this;
// 	}

// 	/**
// 	 * Get entity as string
// 	 * @return string
// 	 */
// 	public function __toString() {
// 		return (string) $this->getId();
// 	}

// 	/**
// 	 * Get Id
// 	 * @return integer | null
// 	 */
// 	public function getId() {
// 		return $this->id;
// 	}

// 	/**
// 	 * Get typeparameter
// 	 * @return string
// 	 */
// 	public function getTypeparameter() {
// 		return $this->typeparameter;
// 	}

// 	/**
// 	 * Set typeparameter
// 	 * @param string $typeparameter
// 	 * @return Linkparameter
// 	 */
// 	public function setTypeparameter($typeparameter) {
// 		$this->typeparameter = $typeparameter;
// 		return $this;
// 	}

// 	/**
// 	 * Set default icon if not defined
// 	 * @ORM\PrePersist()
// 	 * @ORM\PreUpdate()
// 	 * @return self
// 	 */
// 	public function defaultIconIfNone() {
// 		if(!is_string($this->getIcon())) $this->setIcon(static::DEFAULT_ICON);
// 		return $this;
// 	}

// 	/**
// 	 * Set icon
// 	 * @param string $icon = null
// 	 * @return self
// 	 */
// 	public function setIcon($icon = null) {
// 		$this->icon = is_string($icon) ? $icon : static::DEFAULT_ICON;
// 		return $this;
// 	}

// 	/**
// 	 * Get icon
// 	 * @return string
// 	 */
// 	public function getIcon() {
// 		return $this->icon;
// 	}

// 	/**
// 	 * Get DEFAULT_ICON
// 	 * @return string
// 	 */
// 	public static function getDEFAULT_ICON() {
// 		return static::DEFAULT_ICON;
// 	}

// 	/**
// 	 * Get DEFAULT_ICON
// 	 * @return string
// 	 */
// 	public static function getDefaultIcon() {
// 		return static::DEFAULT_ICON;
// 	}

// 	/**
// 	 * Get icon for class in element
// 	 * @return string
// 	 */
// 	public function getIconClass() {
// 		return 'fa '.$this->icon;
// 	}

// 	/**
// 	 * Get icon for class in element
// 	 * @return string
// 	 */
// 	public static function getDefaultIconClass() {
// 		return 'fa '.static::DEFAULT_ICON;
// 	}

// 	/**
// 	 * Get icon as html element
// 	 * @return string
// 	 */
// 	public function getIconHtml($fixed_witdh = true, $classes = []) {
// 		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
// 		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
// 		if(is_array($classes)) {
// 			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
// 			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
// 		} else {
// 			$classes = '';
// 		}
// 		return '<i class="'.$this->getIconClass().$classes.'"></i>';
// 	}

// 	/**
// 	 * Get icon as html element
// 	 * @return string
// 	 */
// 	public static function getDefaultIconHtml($fixed_witdh = true, $classes = []) {
// 		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
// 		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
// 		if(is_array($classes)) {
// 			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
// 			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
// 		} else {
// 			$classes = '';
// 		}
// 		return '<i class="'.static::getDefaultIconClass().$classes.'"></i>';
// 	}


// }