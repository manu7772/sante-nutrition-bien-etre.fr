<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Doctrine\ORM\Event\LifecycleEventArgs;
// Gedmo
// use Gedmo\Mapping\Annotation as Gedmo;
// use Gedmo\Translatable\Translatable;
use Website\SiteBundle\Classes\PagewebsData;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// FileBundle
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceTwig;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Photo;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

use \Exception;

/**
 * Twig
 * 
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\TwigRepository")
 * @ORM\Table(
 *      name="`modeltwig`",
 *		options={"comment":"Files TWIG"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_modeltwig",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 */
class Twig extends File {

	const DEFAULT_ICON = 'fa-file-code-o';
	const DEFAULT_OWNER_DIRECTORY = '/Pagewebs';
	const ENTITY_SERVICE = serviceTwig::class;
	const DEFAULT_ASSTRING_EXTENSION = 'twig';

	// use \ModelApi\BaseBundle\Traits\BaseItem;

	// /**
	//  * Upload file
	//  * @CRUDS\Create(
	//  * 		type="FileType",
	//  * 		show="ROLE_USER",
	//  * 		update="ROLE_USER",
	//  * 		order=1000,
	//  * 		options={"required"=false, "attr"={"accept" = "auto"}}
	//  * 	)
	//  * @CRUDS\Update(
	//  * 		type="FileType",
	//  * 		show="ROLE_USER",
	//  * 		update="ROLE_USER",
	//  * 		order=1000,
	//  * 		options={"required"=false, "attr"={"accept" = "auto"}}
	//  * 	)
	//  * @CRUDS\Show(role="ROLE_USER", order=1000)
	//  */
	// protected $uploadFile;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=200,
	 * 		options={"multiple"=false, "required"=true, "group_by"="owner.name", "class"="ModelApi\FileBundle\Entity\Directory", "query_builder"="qb_findForParentType"},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=200,
	 * 		options={"multiple"=false, "required"=false, "group_by"="owner.name", "class"="ModelApi\FileBundle\Entity\Directory", "query_builder"="qb_findForParentType"},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=200)
	 */
	protected $parent;

	// /**
	//  * @ORM\Column(name="title", type="string", nullable=true, unique=false)
	//  * !!!@Annot\Translatable
	//  * @CRUDS\Create(show=false, update=false, type="TextType", order=200, options={"required"=true})
	//  * @CRUDS\Update(show=false, update=false, type="TextType", order=200, options={"required"=true})
	//  * @CRUDS\Show(role=true)
	//  */
	// protected $title;

	// /**
	//  * @ORM\Column(name="menutitle", type="string", nullable=true, unique=false)
	//  * !!!@Annot\Translatable
	//  * @CRUDS\Create(show=false, update=false, type="TextType", order=300, options={"required"=false})
	//  * @CRUDS\Update(show=false, update=false, type="TextType", order=300, options={"required"=false})
	//  * @CRUDS\Show(role=true)
	//  */
	// protected $menutitle;

	/**
	 * @var string
	 * @ORM\Column(name="typepage", type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=5,
	 * 		type="ChoiceType",
	 * 		options={"required"=true, "expanded"=false, "multiple"=false, "choices"="auto", "disabled"=false},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=5,
	 * 		type="ChoiceType",
	 * 		options={"required"=false, "expanded"=false, "multiple"=false, "choices"="auto", "disabled"="object.getStringAsVersion() !== null"},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=5)
	 */
	protected $typepage;

	/**
	 * Name
	 * @var string
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "attr"={"placeholder"="field.name_placeholder"}},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "attr"={"placeholder"="field.name_placeholder"}},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=100)
	 */
	protected $name;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * 	)
	 * @CRUDS\Show(role=true, order=600)
	 */
	protected $photo;

	/**
	 * @var string
	 * Html text - contenu de la page
	 * @CRUDS\Create(
	 * 		type="CodemirrorType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1000,
	 * 		options={"by_reference"=false, "required"=true},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * )
	 * @CRUDS\Update(
	 * 		type="CodemirrorType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1000,
	 * 		options={"by_reference"=false, "required"=true},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * )
	 * @CRUDS\Show(role=true, order=1000)
	 */
	protected $stringAsVersion;

	/**
	 * @var string
	 * Nom complet du fichier
	 * @ORM\Column(name="filename", type="string", nullable=false, unique=false)
	 */
	protected $filename;

	// /**
	//  * @ORM\Column(name="description", type="text", nullable=true, unique=false)
	//  * !!!@Annot\Translatable
	//  * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="TextareaType", order=400, options={"required"=false})
	//  * @CRUDS\Update(show="ROLE_EDITOR", update="ROLE_EDITOR", type="TextareaType", order=400, options={"required"=false})
	//  * @CRUDS\Show(role=true)
	//  */
	// protected $description;

	/**
	 * @var string
	 * Name of template
	 * @ORM\Column(name="templatename", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update=false,
	 * 		order=2,
	 * 		options={"required"=true},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "expert"},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update=false,
	 * 		order=2,
	 * 		options={"required"=true},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "expert"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=2)
	 */
	protected $templatename;

	/**
	 * @var string
	 * @ORM\Column(name="bundle", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=1,
	 * 		type="ChoiceType",
	 * 		options={"required"=true, "expanded"=false, "multiple"=false, "choices"="auto", "disabled"="object.getText() !== null && object.getId() !== null"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "expert"},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=1,
	 * 		type="ChoiceType",
	 * 		options={"required"=true, "expanded"=false, "multiple"=false, "choices"="auto", "disabled"="object.getText() !== null && object.getId() !== null"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "expert"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=1)
	 */
	protected $bundle;

	/**
	 * @var string
	 * !!!@ORM\Id
	 * @ORM\Column(name="bundlepathname", type="string", nullable=true, unique=false)
	 */
	protected $bundlepathname;

	/**
	 * @var array
	 * @ORM\Column(name="templatestructure", type="json_array", nullable=true, unique=false)
	 */
	protected $templatestructure;

	/**
	 * @var array
	 * @Annot\injectBundles(setter="setBundles")
	 */
	protected $bundles;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 * @ORM\JoinColumn(name="menus_directory", nullable=true, unique=false)
	 * @Annot\JoinedOwnDirectory(getter="getMenu", setter="setMenu", altgetter="getDirmenus", altsetter="setDirmenus", path="system/Menus")
	 */
	protected $menu;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="object.getTypepage() === 'header';",
	 * 		update="object.getTypepage() === 'header';",
	 * 		order=130,
	 * 		options={"multiple"=true, "required"=false, "by_reference"=false, "class"="ModelApi\FileBundle\Entity\Menu", "query_builder"="qb_findRootmenus"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * 		!!!options={"multiple"=true, "required"="object.getTypepage() === 'header';", "by_reference"=false, "class"="ModelApi\FileBundle\Entity\Menu", "query_builder"="qb_findRootmenus"},
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="object.getTypepage() === 'header';",
	 * 		update="object.getTypepage() === 'header';",
	 * 		order=130,
	 * 		options={"multiple"=true, "required"=false, "by_reference"=false, "class"="ModelApi\FileBundle\Entity\Menu", "query_builder"="qb_findRootmenus"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * 		!!!options={"multiple"=true, "required"="object.getTypepage() === 'header';", "by_reference"=false, "class"="ModelApi\FileBundle\Entity\Menu", "query_builder"="qb_findRootmenus"},
	 * @CRUDS\Show(role=false, order=130)
	 */
	protected $dirmenus;

	/**
	 * @var integer
	 * Pageweb
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=500,
	 * 		options={"multiple"=false, "required"=false, "class"="ModelApi\PagewebBundle\Entity\Pageweb", "query_builder"="auto"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=500,
	 * 		options={"multiple"=false, "required"=false, "class"="ModelApi\PagewebBundle\Entity\Pageweb", "query_builder"="auto"},
	 * 	)
	 * @CRUDS\Show(role=false, order=500)
	 */
	protected $pageweb;

	/**
	 * @var array
	 * @ORM\Column(name="itemtypes", type="json_array", )
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=600,
	 * 		options={"required"=false, "multiple"=true, "label"="Pour items", "choices"="auto"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=600,
	 * 		options={"required"=false, "multiple"=true, "label"="Pour items", "choices"="auto"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=600, label="Pour Items")
	 */
	protected $itemtypes;
	/**
	 * @Annot\InjectItemtypesList(setter="setItemtypesChoices")
	 */
	protected $itemtypesChoices;

	/**
	 * @ORM\Column(name="defaultjoin", type="boolean", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		label="Ajouter aux nouvelles pages web",
	 * 		options={"description"="Cette section sera ajoutée par défaut aux nouvelles pages web."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		label="Ajouter aux nouvelles pages web",
	 * 		options={"description"="Cette section sera ajoutée par défaut aux nouvelles pages web."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR")
	 */
	protected $defaultjoin;

	protected $selfcontent;

	public function __construct($predefinedTemplatename = null, $predefinedTypepage = null) {
		parent::__construct();
		$this->templatename = is_string($predefinedTemplatename) ? $predefinedTemplatename : null;
		$this->typepage = is_string($predefinedTypepage) ? $predefinedTypepage : null;
		$this->bundlepathname = null;
		$this->description = null;
		$this->photo = null;
		$this->filename = null;
		$this->bundle = null;
		$this->bundles = null;
		$this->templatestructure = null;
		$this->menu = null;
		$this->dirmenus = new ArrayCollection();
		$this->setNormalizeNameMethod(FileManager::NORMALIZE_METHODS[2]);
		$this->setCroppable(false);
		$this->itemtypesChoices = [];
		$this->itemtypes = [];
		$this->defaultjoin = false;
		$this->selfcontent = false;
		$this->setDefaultStringAsVersion();
		return $this;
	}

	public function isSelfcontent() {
		return isset($this->selfcontent) && is_bool($this->selfcontent) ? $this->selfcontent : false;
	}

	public function setSelfcontent($selfcontent) {
		$this->selfcontent = (boolean)$selfcontent;
		return $this;
	}

	public function setDefaultStringAsVersion() {
		$this->stringAsVersion = $this->getDefaultStringAsVersion();
		return $this;
	}

	public function getDefaultStringAsVersion() {
		if(empty($this->typepage)) return static::getDefaultConstantForStringAsVersion();
		$constant = PagewebsData::class.strtoupper('MODEL_CODE_'.$this->getShortname().'_'.$this->typepage);
		return defined($constant) ? constant($constant) : static::getDefaultConstantForStringAsVersion();
	}

	public static function getDefaultConstantForStringAsVersion() {
		return PagewebsData::MODEL_CODE_TWIG;
	}

	/**
	 * Set uploadFile
	 * @param string $uploadFile = null
	 * @return File
	 */
	public function setStringAsVersion($stringAsVersion = null) {
		if(empty($stringAsVersion)) $stringAsVersion = $this->getDefaultStringAsVersion();
		return parent::setStringAsVersion($stringAsVersion);
	}

	public function getStringAsVersion(Fileversion $version = null) {
		if(empty($version) && empty($this->getId())) return $this->getDefaultStringAsVersion();
		return parent::getStringAsVersion($version);
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getSlug();
	}

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['original'];
	}

	/**
	 * Set name
	 * @param string $name
	 * @param boolean $updateNested = true
	 * @return self
	 */
	public function setName($name, $updateNested = true) {
		parent::setName($name, $updateNested); // Set and NORMALIZE name
		$this->setFilename($this->getName());
		// $this->controlAllTemplateAttributes();
		return $this;
	}

	/**
	 * Set filename
	 * @param string $filename // file name WITHOUT extension!!!
	 * @return Twig
	 */
	protected function setFilename($filename) {
		// if(empty($filename)) $filename = $this->getInputname();
		serviceTools::removeSeparator($filename);
		FileManager::getValidTransformedFilename($filename, 'URL').'.'.static::DEFAULT_ASSTRING_EXTENSION;
		$this->filename = $filename;
		$this->defineTemplatename();
		return $this;
	}

	/**
	 * Get filename
	 * @return string
	 */
	public function getFilename() {
		return $this->filename;
	}


	/**
	 * Get title
	 * @param string $title = null
	 * @return self
	 */
	public function setTitle($title = null) {
		$this->title = $title;
		return $this;
	}


	/**
	 * Get menutitle
	 * @param string $menutitle = null
	 * @return self
	 */
	public function setMenutitle($menutitle = null) {
		$this->menutitle = $menutitle;
		return $this;
	}


	/**
	 * Set description
	 * @param string $description = null
	 * @return self
	 */
	public function setDescription($description = null) {
		$this->description = $description;
		return $this;
	}



	/*************************************************************************************/
	/*** MENUS BY DIR
	/*************************************************************************************/

	// /**
	//  * Get Basedirectory of Menus
	//  * @return Basedirectory
	//  */
	// public function getMenu() {
	// 	if(empty($this->menu)) $this->menu = $this->getOwnerDirectory('system/Menus');
	// 	return $this->menu;
	// }

	// public function setMenu(Basedirectory $menu = null) {
	// 	$this->menu = $menu;
	// 	return $this;
	// }

	// public function getDirmenus() {
	// 	if($this->getMenu() instanceOf Basedirectory) {
	// 		return $this->menu->getChilds()->filter(function($item) { return $item instanceOf Menu; });
	// 	}
	// 	return new ArrayCollection();
	// }

	// public function addDirmenu(Menu $menu) {
	// 	if($this->getMenu() instanceOf Basedirectory) return $this->menu->addChild($menu);
	// 	return false;
	// }

	// public function removeDirmenu(Menu $menu) {
	// 	if($this->getMenu() instanceOf Basedirectory) return $this->menu->removeChild($menu);
	// 	return false;
	// }

	/**
	 * Get Basedirectory of Menus
	 * @return Basedirectory
	 */
	public function getMenu() {
		if(empty($this->menu)) $this->menu = $this->getOwnerDirectory('system/Menus');
		return $this->menu;
	}

	public function setMenu() {
		$this->menu = null;
		$this->getMenu();
		return $this;
	}

	public function getDirmenus() {
		if($this->getMenu() instanceOf Basedirectory) {
			return $this->menu->getChilds()->filter(function($item) { return $item instanceOf Menu; });
		}
		if(!($this->dirmenus instanceOf ArrayCollection)) $this->dirmenus = new ArrayCollection();
		return $this->dirmenus;
	}

	public function setDirmenus($dirmenus) {
		// if($this->getMenu() instanceOf Basedirectory) {
			foreach ($this->getDirmenus() as $dirmenu) {
				if(!$dirmenus->contains($dirmenu)) $this->removeDirmenu($dirmenu);
			}
			foreach ($dirmenus as $dirmenu) $this->addDirmenu($dirmenu);
		// }
		return $this;
	}

	public function addDirmenu(Menu $menu) {
		if($this->getMenu() instanceOf Basedirectory) {
			$this->menu->addChild($menu);
			return $this->getDirmenus()->contains($menu);
		}
		if(!$this->getDirmenus()->contains($menu)) $this->dirmenus->add($menu);
		return true;
	}

	public function removeDirmenu(Menu $menu) {
		if($this->getMenu() instanceOf Basedirectory) {
			$this->menu->removeChild($menu);
			return !$this->getDirmenus()->contains($menu);
		}
		$this->getDirmenus()->removeElement($menu);
		return true;
	}


	/*************************************************************************************/
	/*** CONTROL ALL TEMPLATE DATA
	/*************************************************************************************/

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function controlAllTemplateAttributes() {
		$this->defineTemplatename();
		// $this->setBundle();
		// $this->defineBundlepathname();
		return $this;
	}


	/*************************************************************************************/
	/*** BUNDLEPATHNAME
	/*************************************************************************************/

	public function getDefinedBundlepathname() {
		return $this->hasTemplatestructure() ?
			$this->getBundle().':'.$this->getFolder().':'.$this->getFilename() :
			null ;
	}

	public function defineBundlepathname() {
		$this->bundlepathname = $this->getDefinedBundlepathname();
		return $this;
	}

	public function getBundlepathname() {
		return $this->bundlepathname;
	}

	public function isValidBundlepathname() {
		return $this->bundlepathname === $this->getDefinedBundlepathname();
		// return preg_match('/^('.implode('|', $this->getBundlesNames()).'):'.$this->getFolder().':'.$this->getFilename().'$/', $this->bundlepathname);
	}


	/*************************************************************************************/
	/*** TYPEPAGE
	/*************************************************************************************/

	public function getTypepageChoices() {
		return $this->getTypepages();
	}

	public function getTypepages() {
		if(!$this->hasTemplatestructure()) return [];
		$names = [];
		foreach ($this->getTemplatestructure()['root']['children'] as $typepage => $data) $names[$typepage] = $typepage;
		return $names;
	}

	public function defineTypepage() {
		if(null === $this->typepage || !$this->hasTypepage($this->typepage)) {
			$typepages = $this->getTypepages();
			$this->typepage = count($typepages) > 0 ? reset($typepages) : null;
		}
		// DevTerminal::Info('     --> Typepage: '.json_encode($this->getTypepage()).'.');
		$this->defineBundlepathname();
		return $this;
	}

	public function getTypepage() {
		return $this->typepage;
	}

	public function hasTypepage($typepage) {
		$typepages = $this->getTypepages();
		return in_array($typepage, $typepages);
	}

	public function setTypepage($typepage) {
		if($this->hasTypepage($typepage)) {
			$this->typepage = $typepage;
		} else {
			throw new Exception("Error ".__METHOD__."(): page type ".json_encode($typepage)." is not valid. Please, choose in ".json_encode($this->getTypepages()).".", 1);
			$this->defineTypepage();
		}
		$this->defineBundlepathname();
		return $this;
	}

	public function isValidTypepage() {
		// DevTerminal::Info('     --> Bundle: '.json_encode($this->getBundle()).' in '.json_encode($this->getBundles()).' => '.json_encode($this->getTemplatename()).'.');
		// DevTerminal::Info('     --> Typepage check: '.json_encode($this->getTypepage()).' in '.json_encode($this->getTypepages()).'.');
		return $this->hasTypepage($this->getTypepage());
	}

	/*************************************************************************************/
	/*** TEMPLATESTRUCTURE
	/*************************************************************************************/

	/**
	 * Set templatestructure
	 * @return static
	 */
	protected function setTemplatestructure($templatestructure) {
		$this->templatestructure = $templatestructure;
		$this->defineTemplatename();
		return $this;
	}

	/**
	 * Get templatestructure
	 * @return array
	 */
	public function getTemplatestructure() {
		return $this->templatestructure;
	}

	/**
	 * Has templatestructure
	 * @return boolean
	 */
	public function hasTemplatestructure() {
		return null !== $this->templatestructure && count($this->templatestructure) > 0;
	}


	/*************************************************************************************/
	/*** Templatename
	/*************************************************************************************/

	/**
	 * Define template name
	 * @return static
	 */
	public function defineTemplatename() {
		if(!$this->hasTemplatestructure()) {
			$this->templatename = null;
			// return $this;
		} else {
			$this->templatename = $this->getTemplatestructure()['_name'];
		}
		// DevTerminal::Info('     --> Template: '.json_encode($this->getTemplatename()).'.');
		$this->defineTypepage();
		return $this;
	}

	/**
	 * Get template name
	 * @return string
	 */
	public function getTemplatename() {
		// if(!$this->hasTemplatestructure()) $this->templatename = null;
		return $this->templatename;
	}


	/*************************************************************************************/
	/*** FOLDER FOR TEMPLATE
	/*************************************************************************************/

	/**
	 * Get Folder
	 * @return string | null
	 */
	public function getFolder() {
		if(!$this->hasTemplatestructure()) return null;
		$templatestructure = $this->getTemplatestructure();
		return $templatestructure['root']['children'][$this->getTypepage()]['folder'];
	}


	/*************************************************************************************/
	/*** BUNDLES
	/*************************************************************************************/

	/**
	 * Get bundles [shortname => Bundle]
	 * @return array
	 */
	public function getBundles() {
		return $this->bundles;
	}

	/**
	 * Get object bundle by name (or entity's bundle)
	 * @return Bundle
	 */
	public function getObjectBundle($bundlename = null) {
		$bundlename = serviceClasses::getShortnameIfClassname($bundlename);
		return isset($this->bundles[$bundlename]) ? $this->bundles[$bundlename] : null;
	}

	/**
	 * Set Bundles
	 * @return Twig
	 */
	public function setBundles($bundles) {
		// Example:
		// array(2) { ["WebsiteAdminBundle"]=> object ["WebsiteSiteBundle"]=> object }
		$this->bundles = $bundles;

		foreach ($this->bundles as $name => $bundle) {
			// DevTerminal::Info('     --> Injected bundle: '.json_encode($name).' => '.json_encode(get_class($bundle)).'.');
		}
		// Default bundle: public
		if(null === $this->getBundle()) {
			foreach ($this->bundles as $name => $bundle) {
				if($bundle->isPublicBundle()) { $this->setBundle($name); break; }
			}
		}
		// Default: first found
		if(null === $this->getBundle()) {
			$def = array_keys($this->bundles);
			$this->setBundle(end($def));
		}
		// DevTerminal::Info('     --> Injected bundle: '.json_encode($this->getBundle()).'.');
		return $this;
	}

	/**
	 * Set bundle classname
	 * @param string $bundle
	 * @return Twig
	 */
	public function setBundle($bundle) {
		$objectBundle = $this->getObjectBundle($bundle);
		if(null === $objectBundle) throw new Exception("Error ".__METHOD__."(): bundle ".json_encode($bundle)." does not exist. Please, choose in ".json_encode($this->getBundlesNames()).".", 1);
		$this->bundle = $bundle;
		$this->setTemplatestructure($objectBundle->getTemplateStructure());
		return $this;
	}

	/**
	 * Get bundle classname
	 * @return string
	 */
	public function getBundle() {
		return $this->bundle;
	}

	/**
	 * Get bundles choices for form
	 * @return array <string>
	 */
	public function getBundleChoices() {
		$bundles = array();
		foreach ($this->getBundlesNames() as $name) $bundles[$name] = $name;
		return $bundles;
	}

	/**
	 * Get bundles shortnames
	 * @return array <string>
	 */
	public function getBundlesNames() {
		return array_keys($this->bundles);
	}



	/*************************************************************************************/
	/*** PHOTO
	/*************************************************************************************/

	/**
	 * Set URL as photo
	 * @param string $url
	 * @return Twig
	 */
	public function setUrlAsPhoto($url) {
		$photo = new Photo();
		$photo->setUploadFile($url);
		$this->setPhoto($photo);
		return $this;
	}

	/**
	 * Set resource as photo
	 * @param string $resource
	 * @return Twig
	 */
	public function setResourceAsPhoto(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$photo = new Photo();
			$photo->setUploadFile($resource);
			$this->setPhoto($photo);
		}
		return $this;
	}

	public function getResourceAsPhoto() {
		return null;
	}

	/**
	 * Set photo
	 * @param Photo $photo = null
	 * @return Twig
	 */
	public function setPhoto(Photo $photo = null) {
		$this->photo = $photo;
		return $this;
	}

	/**
	 * Get photo
	 * @return Photo | null
	 */
	public function getPhoto() {
		return $this->photo;
	}



	/*************************************************************************************/
	/*** ITEMS TYPES
	/*************************************************************************************/

	/**
	 * Get Items types
	 * @return array
	 */
	public function getItemtypes($asShortnames = false) {
		if($asShortnames) {
			return array_map(function($item) {
				return serviceEntities::humanize($item);
			}, $this->itemtypes);
		}
		return $this->itemtypes;
	}

	public function hasItemtype($itemtype) {
		// if(is_object($itemtype)) $itemtype = $itemtype->getShortname();
		$itemtypes = $this->getItemtypes(true);
		return in_array($itemtype, $itemtypes);
	}

	/**
	 * Set Items types
	 * @param array $itemtypes
	 * @return Twig
	 */
	public function setItemtypes($itemtypes) {
		$this->itemtypes = $itemtypes;
		return $this;
	}

	/**
	 * Get choices for Items types
	 * @return array
	 */
	public function getItemtypesChoices() {
		return $this->itemtypesChoices;
	}

	/**
	 * Set Items types list
	 * @param array $itemtypesChoices
	 * @return Twig
	 */
	public function setItemtypesChoices($itemtypesChoices) {
		$this->itemtypesChoices = $itemtypesChoices;
		return $this;
	}



	/*************************************************************************************/
	/*** DEFAULT JOIN (to new Pagewebs)
	/*************************************************************************************/

	public function setDefaultjoin($defaultjoin) {
		$this->defaultjoin = $defaultjoin;
		return $this;
	}

	public function getDefaultjoin() {
		return $this->defaultjoin;
	}




}