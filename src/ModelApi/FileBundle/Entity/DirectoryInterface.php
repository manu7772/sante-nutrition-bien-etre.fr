<?php
namespace ModelApi\FileBundle\Entity;

// FileBundle
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\OwnerTierInterface;

interface DirectoryInterface {


	public function initItem(serviceBasedirectory $serviceBasedirectory);

	public function initSorts();

	public function getDefaultSortWay();

	public function getDefaultSortField();

	public function getSortsChoices();

	public function getSorts();

	public function isSorted();

	public function isPositionSorted();

	public function getSortWay($fieldname);

	public function initQuerys();

	public function isUniqueQuery();

	public function checkQuerys($exceptionIfErrors = false);

	public function getQueryRepositorysAndMethods();

	public function setQuery($repositoryAndMethod);

	public function addQuery($repositoryAndMethod);

	public function getDefaultQuery();

	public function setDefaultQuery();

	public function getQuery();

	public function getQuerys();

	public function isQuery();

	public function getQueryChoices();

	public function getMixedquery();

	public function isMixedquery();

	public function setMixedquery($mixedquery);

	public function isChildsOrChildsAndQuery();

	public function isOnlyChilds();

	public function getChilds($page = -1, $length = null, $publicFiltered = 'auto');

	public function isPaged();

	public function getNumberPages($page = -1, $length = null);

	public function getchilds_nextPage();

	public function getchilds_previousPage();

	public function hasChilds_nextPage();

	public function hasChilds_previousPage();

	public function addChild(Item $child, $askedTypelink = null, $position = -1);

	public function removeChild(Item $child);

	public function getSolidChilds();

	public function hasSolidChilds();

	public function isChildeable(Basedirectory $parent = null);

	public function hasExcludedirchild(Item $item);

	public function addExcludedirchild(Item $item);

	public function removeExcludedirchild(Item $item);

	public function setExcludedirchilds($items);

	public function getExcludedirchilds();

	public function getOwnerdrivedir();

	public function setOwnerdrivedir(Item $ownerdrivedir, $inverse = true);

	public function getOwnersystemdir();

	public function setOwnersystemdir(Item $ownersystemdir, $inverse = true);

	public function getOwnerdir($refresh = false);

	public function setDirstatic($dirstatic);

	public function isDirstatic();

	public function getDirstatic();

	public function isRootSystem();

	public function isRootDrive();

	public function isSystem();

	public function hasRootDirAttributes();

	public function isCompleteRoot();

	public function isIncompleteRoot();

	public function isCompleteOrIncompleteRoot();

	public function canTypedirBeChanged();

	public function isDeletable();

	public function setName($name);

	public function removeItemdata(Item $item);

	public function getLinkdatasAsUniqueValue();

	public function getChildPosition(Item $item);

	public function getNextPosition();

	public function getPositionOrNext($position);


}

