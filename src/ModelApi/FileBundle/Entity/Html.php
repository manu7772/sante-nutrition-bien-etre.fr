<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

// FileBundle
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceHtml;

use \Exception;

/**
 * Html
 * 
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\HtmlRepository")
 * @ORM\Table(
 *      name="`modelhtml`",
 *		options={"comment":"Files HTML"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_modelhtml",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 */
class Html extends File {

	const DEFAULT_ICON = 'fa-file-code-o';
	const DEFAULT_OWNER_DIRECTORY = '@@@drive_documents@@@';
	const ENTITY_SERVICE = serviceHtml::class;
	const DEFAULT_ASSTRING_EXTENSION = 'html';

	// /**
	//  * Upload file
	//  * @CRUDS\Create(
	//  * 		type="FileType",
	//  * 		show="ROLE_USER",
	//  * 		update="ROLE_USER",
	//  * 		order=1000,
	//  * 		options={"required"=false, "attr"={"accept" = "auto"}}
	//  * 	)
	//  * @CRUDS\Update(
	//  * 		type="FileType",
	//  * 		show="ROLE_USER",
	//  * 		update="ROLE_USER",
	//  * 		order=1000,
	//  * 		options={"required"=false, "attr"={"accept" = "auto"}}
	//  * 	)
	//  * @CRUDS\Show(role="ROLE_USER", order=1000)
	//  */
	// protected $uploadFile;

	/**
	 * Name
	 * @var string
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "attr"={"placeholder"="field.name_placeholder"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "attr"={"placeholder"="field.name_placeholder"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=100)
	 */
	protected $name;

	/**
	 * @var string
	 * Html text - contenu de la page
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=120, options={"required"=false})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=120, options={"required"=false})
	 * @CRUDS\Show(role=true)
	 */
	protected $stringAsVersion;


	public function __construct() {
		parent::__construct();
		$this->setNormalizeNameMethod(FileManager::NORMALIZE_METHODS[2]);
		$this->setCroppable(false);
		return $this;
	}

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['original'];
	}



}