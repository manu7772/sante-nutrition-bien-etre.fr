<?php
// namespace ModelApi\FileBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;
// use Hateoas\Configuration\Annotation as Hateoas;
// use Gedmo\Mapping\Annotation as Gedmo;

// use ModelApi\FileBundle\Entity\Directory;
// use ModelApi\FileBundle\Entity\Menu;
// use ModelApi\FileBundle\Entity\Item;

// use ModelApi\FileBundle\Repository\NestlinkRepository;

// use \Exception;

// /**
//  * Nestlink
//  * 
//  * @ORM\Entity(repositoryClass=NestlinkRepository::class)
//  * @ORM\DiscriminatorColumn(name="class_name", type="string")
//  * @ORM\InheritanceType("JOINED")
//  * @ORM\Table(name="`nestlink`", options={"comment":"Nested links"})
//  * @Hateoas\Relation(
//  *      "self",
//  *      href = @Hateoas\Route(
//  *          "get_nestlink",
//  *          parameters = { "id" = "expr(object.getId())" }
//  *      )
//  * )
//  * @ORM\HasLifecycleCallbacks
//  */
// class Nestlink {

// 	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
// 	use \ModelApi\BaseBundle\Traits\DateUpdater;

// 	const DEFAULT_ICON = 'fa-arrows-h';
// 	const NAME_SOLID = 'solid';
// 	const NAME_SYMBOLIC = 'symbolic';
// 	const NAME_NONE = 'undefined';
//	const SHORTCUT_CONTROLS = true;

// 	/**
// 	 * Id of entity
// 	 * @var string
// 	 * @ORM\Id
// 	 * @ORM\Column(name="id", type="string")
// 	 * @ORM\GeneratedValue(strategy="UUID")
// 	 */
// 	protected $id;

// 	/**
// 	 * @var integer
// 	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", inversedBy="childLinks")
// 	 * @ORM\JoinColumn(name="directory_id", referencedColumnName="id", nullable=false, unique=false)
// 	 * @Gedmo\SortableGroup
// 	 */
// 	protected $directory;

// 	/**
// 	 * @var integer
// 	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Item", inversedBy="parentLinks", cascade={"persist"})
// 	 * @ORM\JoinColumn(name="child_id", referencedColumnName="id", nullable=false, unique=false)
// 	 */
// 	protected $child;

// 	/**
// 	 * @var string
// 	 * @ORM\Column(name="typelink", type="string", length=8, nullable=false, unique=false)
// 	 */
// 	protected $typeLink;

// 	/**
// 	 * Position
// 	 * @var integer
// 	 * @ORM\Column(name="position", type="integer")
// 	 * @Gedmo\SortablePosition
// 	 */
// 	protected $position;

// 	/**
// 	 * Path name
// 	 * @var string
// 	 * @ORM\Column(name="pathname", type="text", nullable=false, unique=false)
// 	 */
// 	protected $pathname;


// 	public function __construct(Basedirectory $directory, Item $child, $asSymbolic = false) {
// 		throw new Exception("Error ".__METHOD__."(): as new version of nested entities system, could not create new Nestlink anyway!", 1);
// 		$this->id = null;
// 		$this->init_ClassDescriptor();
// 		$this->initCreated();
// 		$this->typeLink = null;
// 		if(true === $asSymbolic) $this->setSymbolic(false);
// 			else $this->setSolid(false);
// 		$this->child = $child; // --> IMPORTANT !!
// 		$this->directory = $directory; // --> IMPORTANT !!
// 		if($this->directory->authorizeChildLink($this)->isLastResultSuccess()) {
// 			// $this->setChild($child);
// 			$this->child->addParentLink($this);
// 			$this->getChild();
// 			// $this->setDirectory($directory);
// 			if(!$directory->addChildLink($this)->isLastResultSuccess()) $this->removeAll();
// 			$this->setPosition(-1);
// 		}
// 		if($this->isValid()) {
// 			if($this->isSolid()) $this->child->computeRootparent();
// 			$this->computePathname();
// 		} else {
// 			$this->removeAll();
// 		}
// 		return $this;
// 	}

// 	/**
// 	 * compute pathname
// 	 * @return string
// 	 */
// 	public function computePathname() {
// 		if(!$this->isValid()) return null;
// 		return $this->pathname = $this->directory->getPathname(false).DIRECTORY_SEPARATOR.$this->child->getName();
// 	}

// 	/**
// 	 * Get pathname
// 	 * @return string
// 	 */
// 	public function getPathname($recompute = false) {
// 		if(!$this->isValid()) return null;
// 		if($recompute) return $this->computePathname();
// 		return $this->pathname;
// 	}

// 	*
// 	 * Get path slug
// 	 * @param string $prefix = 'cat'
// 	 * @return array <string>
	 
// 	public function getPathslug($prefix = 'cat') {
// 		if(!$this->isValid()) return null;
// 		if($recompute) return $this->computePathname();
// 		return $this->pathname;
// 	}



// 	/**
// 	 * Get entity as string
// 	 * @return string
// 	 */
// 	public function __toString() {
// 		return (string) $this->getId();
// 	}

// 	/**
// 	 * Get Id
// 	 * @return string
// 	 */
// 	public function getId() {
// 		return $this->id;
// 	}

// 	/**
// 	 * Is valid
// 	 * @return boolean
// 	 */
// 	public function isValid() {
// 		return $this->directory instanceOf Basedirectory && $this->child instanceOf Item;
// 	}

// 	/**
// 	 * Get link type
// 	 * @return string
// 	 */
// 	public function getTypeLink() {
// 		return $this->typeLink;
// 	}

// 	// /**
// 	//  * Set SOLID
// 	//  * @param boolean $compute = true
// 	//  * @return Nestlink
// 	//  */
// 	// public function setSolid($compute = true) {
// 	// 	if(!$this->isSolid()) {
// 	// 		$this->typeLink = static::NAME_SOLID;
// 	// 		if(true === $compute && $this->child instanceOf Item) $this->child->computeRootparent();
// 	// 	}
// 	// 	return $this;
// 	// }

// 	/**
// 	 * Is SOLID
// 	 * @return boolean
// 	 */
// 	public function isSolid() {
// 		return $this->typeLink === static::NAME_SOLID;
// 	}

// 	// /**
// 	//  * Set SYMBOLIC
// 	//  * @param boolean $compute = true
// 	//  * @return Nestlink
// 	//  */
// 	// public function setSymbolic($compute = true) {
// 	// 	if(!$this->isSymbolic()) {
// 	// 		$this->typeLink = static::NAME_SYMBOLIC;
// 	// 		if(true === $compute && $this->child instanceOf Item) $this->child->computeRootparent();
// 	// 	}
// 	// 	return $this;
// 	// }

// 	/**
// 	 * is SYMBOLIC
// 	 * @return boolean
// 	 */
// 	public function isSymbolic() {
// 		return $this->typeLink === static::NAME_SYMBOLIC;
// 	}

// 	/**
// 	 * Get parent directory
// 	 * @return Basedirectory
// 	 */
// 	public function getDirectory() {
// 		return $this->directory;
// 	}

// 	// /**
// 	//  * Set parent directory
// 	//  * @param Basedirectory $directory
// 	//  * @return NestlInk
// 	//  */
// 	// protected function setDirectory(Basedirectory $directory) {
// 	// 	// $this->directory = $directory;
// 	// 	if(!$directory->addChildLink($this)->isLastResultSuccess()) $this->removeAll();
// 	// 	return $this;
// 	// }

// 	/**
// 	 * @ORM\PreRemove()
// 	 * Remove directory and child
// 	 * @return NestlInk
// 	 */
// 	public function removeAll() {
// 		// if($this->isValid()) {
// 			$this->typeLink = static::NAME_SYMBOLIC;
// 			$directory = $this->directory;
// 			$this->directory = null;
// 			$child = $this->child;
// 			$this->child = null;
// 			if(null !== $child) $child->removeParentLink($this);
// 			if(null !== $directory) $directory->removeChildLink($this);
// 			$this->pathname = null;
// 		// }
// 		return $this;
// 	}

// 	/**
// 	 * Get child
// 	 * @param boolean $setAsCurrent = true
// 	 * @return Item
// 	 */
// 	public function getChild($setAsCurrent = true) {
// 		return $this->child;
// 		// stop if directory is not Menu
// 		if($this->directory instanceOf Menu && !$this->directory->isRootmenu()) $this->child->setCurrentPathslug($this->directory->getCurrentPathslug());
// 		// if(!empty($this->child)) $this->child->setCurrentParentLink($this);
// 		return $this->child;
// 	}

// 	// /**
// 	//  * Set child
// 	//  * @param Item $child
// 	//  * @return NestlInk
// 	//  */
// 	// protected function setChild(Item $child) {
// 	// 	// $this->child = $child;
// 	// 	$this->child->addParentLink($this);
// 	// 	$this->getChild();
// 	// 	return $this;
// 	// }

// 	/**
// 	 * Get position
// 	 * @param integer $position
// 	 * @param boolean $inverse = true
// 	 * @return NestlInk
// 	 */
// 	public function setPosition($position, $inverse = true) {
// 		// $this->position = $position;
// 		// if($inverse) $this->child->setPosition($position, $this, false);
// 		return $this;
// 	}

// 	/**
// 	 * Get position
// 	 * @return integer
// 	 */
// 	public function getPosition() {
// 		return $this->position;
// 	}


// }


