<?php
namespace ModelApi\FileBundle\Entity;


// FileBundle
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Form\Type\TempfileType;

use \Exception;

/**
 * Tempfile
 * 
 */
class Tempfile extends File {

	const ENTITY_FORM = TempfileType::class;

	/**
	 * Upload file
	 */
	protected $uploadFile;

	protected $tempParent;


	public function __construct() {
		parent::__construct();
		$this->tempParent = null;
		$this->setNormalizeNameMethod(FileManager::NORMALIZE_METHODS[2]);
		$this->setCroppable(false);
		return $this;
	}


	public function setTempParent(Basedirectory $tempParent = null) {
		$this->tempParent = $tempParent;
		return $this;
	}

	public function getTempParent() {
		return $this->tempParent;
	}

	// /**
	//  * Set uploadFile
	//  * @param string $uploadFile = null
	//  * @return File
	//  */
	// public function setUploadFile($uploadFile = null) {
	// 	$this->uploadFile = $uploadFile;
	// 	// do noting else...
	// 	return $this;
	// }




}




