<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Doctrine\Common\Collections\ArrayCollection;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\Common\Inflector\Inflector;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Tempfile;
use ModelApi\FileBundle\Entity\DirectoryInterface;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
use ModelApi\FileBundle\Repository\BasedirectoryRepository;
use ModelApi\FileBundle\Repository\ItemRepository;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\OwnerTierInterface;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

use \Closure;
use \Exception;
use \DateTime;

/**
 * Basedirectory
 * 
 * @ORM\Entity(repositoryClass=BasedirectoryRepository::class)
 * @ORM\Table(name="`basedirectory`", options={"comment":"Base directories"})
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_basedirectory",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER")
 * @Annot\HasTranslatable()
 */
abstract class Basedirectory extends Item implements DirectoryInterface {

	// use \ModelApi\BaseBundle\Transitional\Traits\Basedirectory_transitional;

	const DEFAULT_ICON = 'fa-folder';
	const DEFAULT_OWNER_DIRECTORY = '@@@drive_documents@@@';
	const ENTITY_SERVICE = serviceBasedirectory::class;

	const SORT_FIELDS = [
		'position' => 'position',
		'id' => 'id',
		'created' => 'created',
		'updated' => 'updated',
		'name' => 'name',
	];
	const POSITION_SORT_FIELD = 'position';
	const DEFAULT_SORT_WAY = 'ASC';
	const SORT_WAYS = ['ASC', 'DESC'];

	const DEFAULT_QUERY_REPOSITORY = ItemRepository::class;
	const DEFAULT_QUERY_METHOD = 'bdq_findListOrdered';
	const QUERY_SEPARATOR = '::';
	const IS_UNIQUE_QUERY = true; // true: can set one unique query / false: can add multiple querys


	/**
	 * @var array
	 * @ORM\ManyToMany(targetEntity="ModelApi\FileBundle\Entity\Item", inversedBy="dirparents", cascade={"persist"}, fetch="EXTRA_LAZY")
	 * @ORM\JoinTable(
	 * 		name="`dirchilds_directory`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 */
	protected $dirchilds;

	/**
	 * @var array
	 * @ORM\Column(name="excludedirchilds", type="json_array", nullable=false, unique=false)
	 */
	protected $excludedirchilds;

	/**
	 * @var array
	 */
	protected $childs;

	/**
	 * @var array
	 * @CRUDS\Updates({
	 * 		@CRUDS\Update(
	 * 			type="EntityType",
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=400,
	 * 			options={"required"=false, "by_reference"=false, "multiple"=true, "group_by"="shortname", "class"="ModelApi\FileBundle\Entity\Item", "attr"={"class"="select-choice", "placeholder"="Pages web"}, "query_builder"="auto", "label"="Éléments"},
	 * 			contexts={"additem"},
	 * 		),
	 * })
	 * @CRUDS\Show(role=true, order=100)
	 * !!!@Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 */
	protected $childs_pagewebs;

	/**
	 * @var array
	 * @CRUDS\Updates({
	 * 		@CRUDS\Update(
	 * 			type="EntityType",
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=400,
	 * 			options={"required"=false, "by_reference"=false, "multiple"=true, "group_by"="shortname", "class"="ModelApi\FileBundle\Entity\Menu", "attr"={"class"="select-choice", "placeholder"="Menus"}, "query_builder"="auto", "label"="Menus"},
	 * 			contexts={"addbasedirectory"},
	 * 		),
	 * })
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $childs_directorys;

	/**
	 * Sorts
	 * @var array
	 * @ORM\Column(name="sorts", type="json_array", nullable=false, unique=false)
	 */
	protected $sorts;

	/**
	 * Querys for childs
	 * @var array
	 * @ORM\Column(name="querys", type="json_array", nullable=false, unique=false)
	 */
	protected $querys;
	/**
	 * Directory is query
	 * @var boolean
	 * @ORM\Column(name="isquery", type="boolean", nullable=false, unique=false)
	 */
	protected $isquery;
	protected $page;
	protected $length;
	protected $publicFiltered;
	protected $mixedquery;





	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\FileBundle\Entity\Item", mappedBy="driveDir", cascade={"persist"})
	 * @ORM\JoinColumn(name="`ownerdrivedir_directory`", nullable=true)
	 */
	protected $ownerdrivedir;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\FileBundle\Entity\Item", mappedBy="systemDir", cascade={"persist"})
	 * @ORM\JoinColumn(name="`ownersystemdir_directory`", nullable=true)
	 */
	protected $ownersystemdir;

	/**
	 * Name
	 * @var string
	 * @CRUDS\Create(
	 * 		contexts={"simple"},
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=true, "by_reference"=false, "attr"={"placeholder"="Nom du dossier"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		contexts={"simple","change_name"},
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=100,
	 * 		options={"required"=true, "by_reference"=false, "attr"={"placeholder"="Nom du dossier"}}
	 * 	)
	 * @CRUDS\Show(label="Nom", role="ROLE_USER", order=100)
	 */
	protected $inputname;

	/**
	 * normalizeNameMethod
	 * @var string
	 * @CRUDS\Create(
	 * 		contexts={"expert"},
	 * 		type="ChoiceType",
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=101,
	 * 		options={"required"=true, "label"="Normaliser le nom", "choices"="auto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		contexts={"expert"},
	 * 		type="ChoiceType",
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=101,
	 * 		options={"required"=true, "label"="Normaliser le nom", "choices"="auto"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=101, label="Normalisation du nom")
	 */
	protected $normalizeNameMethod;

	// *
	//  * Type of directory
	//  * @var string
	//  * @ORM\Column(name="typedir", type="string", length=16, nullable=false, unique=false)
	 
	// protected $typedir;

	/**
	 * Static is directory
	 * @var boolean
	 * @ORM\Column(name="dirstatic", type="boolean", nullable=true, unique=false)
	 */
	protected $dirstatic;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $subtitle;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $accroche;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $description;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $resume;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $fulltext;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $color;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $urls;



	public function __construct() {
		parent::__construct();
		$this->dirchilds = new ArrayCollection();
		$this->excludedirchilds = [];
		$this->childs = new ArrayCollection();
		// $this->linkdatas = [];
		$this->ownerdrivedir = null;
		$this->ownersystemdir = null;
		// $this->typedir = serviceBasedirectory::TYPEDIR_REGULAR;
		$this->dirstatic = false;
		return $this;
	}


	public function isTransited() {
		return !isset($this->childLinks) || !$this->childLinks->count();
	}


	/*************************************************************************************/
	/*** VALIDITY
	/*************************************************************************************/

	/**
	 * Has root attributes (1), but maybe not complete (2)
	 * Returned value :
	 * 		0 : is not a root directory
	 * 		1 : is a root directory
	 * 		2 : is NOT COMPLETE root directory
	 * @return integer
	 */
	public function hasRootDirAttributes() {
		$tests = $count = 0;
		// $rootdirs = $this->getRootDirs();
		// $tests++; if(count($rootdirs) === 0) $count++; // 1
		$tests++; if($this->hasRootPrefix()) $count++; // 2
		$tests++; if($this->ownerdrivedir instanceOf Item || $this->ownersystemdir instanceOf Item) $count++; // 3
		// $tests++; if($this->ownerdrivedir instanceOf Item XOR $this->ownersystemdir instanceOf Item) $count++; // 3
		// $tests++; if(empty($this->parent)) $count++; // 4
		// $tests++; if(empty($this->dirparents)) $count++; // 5
		if($count === 0) return 0;
		if($this->ownerdrivedir instanceOf Item && $this->ownersystemdir instanceOf Item) return 2;
		return $count === $tests ? 1 : 2;
	}

	public function isNotRoot() {
		return $this->hasRootDirAttributes() === 0;
	}

	public function isCompleteRoot() {
		return $this->isValid();
	}

	public function isIncompleteRoot() {
		return $this->hasRootDirAttributes() === 2;
	}

	public function isCompleteOrIncompleteRoot() {
		return $this->hasRootDirAttributes() > 0;
	}


	/*************************************************************************************/
	/*** BEGIN => CHILDS
	/*************************************************************************************/

	/***** SORTS *****/

	public function initSorts() {
		if(!is_array($this->sorts) || empty($this->sorts)) $this->sorts = [$this->getDefaultSortField() => $this->getDefaultSortWay()];
		return $this;
	}

	public function getDefaultSortWay() {
		return static::DEFAULT_SORT_WAY;
	}

	public function getDefaultSortField() {
		$sortfields = static::SORT_FIELDS;
		return $this->isOnlyChilds() ? reset($sortfields) : static::SORT_FIELDS['id'];
	}

	public function getSortsChoices() {
		return static::SORT_FIELDS;
	}

	public function getSorts() {
		$this->initSorts();
		return $this->sorts;
	}

	public function isSorted() {
		return !$this->isQuery();
	}

	public function isPositionSorted() {
		return $this->isOnlyChilds() && count($this->sorts) === 1 && array_key_first($this->sorts) === static::POSITION_SORT_FIELD;
	}

	public function getSortWay($fieldname) {
		return array_key_exists($fieldname, $this->sorts) ? $this->sorts[$fieldname] : null;
	}


	/***** QUERY *****/

	public function initQuerys() {
		if(!is_array($this->querys) || empty($this->querys)) $this->setDefaultQuery();
		$this->checkQuerys(true);
		// $this->isQuery();
		if(!isset($this->page) || !is_integer($this->page)) $this->page = -1;
		if(!isset($this->publicFiltered) || !is_bool($this->publicFiltered)) $this->publicFiltered = $this->isPublicFiltered();
		if(!isset($this->mixedquery) || !is_bool($this->mixedquery)) $this->mixedquery = false;
		return $this->querys;
	}

	public function isUniqueQuery() {
		return static::IS_UNIQUE_QUERY;
	}

	public function checkQuerys($exceptionIfErrors = false) {
		$this->addTestCount(__METHOD__);

		$this->querys = array_unique($this->querys);
		if($this->isUniqueQuery()) $this->querys = array_slice($this->querys, 0, 1);
		foreach ($this->getQueryRepositorysAndMethods() as $query) {
			if(!method_exists($query['repository'], $query['method'])) {
				if($exceptionIfErrors) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this repository ".json_encode(get_class($query['repository']))." does not have method ".json_encode($query['method'])."!", 1);
				return false;
			}
		}
		$this->isQuery();
		return true;
	}

	public function getQueryRepositorysAndMethods() {
		$querys = [];
		foreach ($this->querys as $query) {
			$querys[] = [
				'repository' => $this->serviceBasedirectory->getQueryRepository($query),
				'method' => $this->serviceBasedirectory->getQueryMethod($query),
			];
		}
		return $querys;
	}

	/**
	 * Set query
	 * @param mixed $repositoryAndMethod (true = set default query / string = repository classname::method)
	 * @return Basedirectory
	 */
	public function setQuery($repositoryAndMethod) {
		$memQuery = $this->querys;
		if(!is_string($repositoryAndMethod)) $repositoryAndMethod = $this->getDefaultQuery();
		// $split = explode(static::QUERY_SEPARATOR, $repositoryAndMethod);
		$this->querys = [$repositoryAndMethod];
		if(!$this->checkQuerys(false)) {
			if($this->isDeepControl()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this repository ".json_encode(get_class($this->getQueryRepositorys()))." does not have method ".json_encode($this->getQueryMethods())."!", 1);
			$this->querys = $memQuery;
		}
		return $this;
	}

	/**
	 * Add query
	 * @param mixed $repositoryAndMethod (true = set default query / string = repository classname::method)
	 * @return Basedirectory
	 */
	public function addQuery($repositoryAndMethod) {
		$memQuery = $this->querys;
		if(!is_string($repositoryAndMethod)) $repositoryAndMethod = $this->getDefaultQuery();
		$this->querys[] = $repositoryAndMethod;
		if(!$this->checkQuerys(false)) {
			if($this->isDeepControl()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this repository ".json_encode(get_class($this->getQueryRepositorys()))." does not have method ".json_encode($this->getQueryMethods())."!", 1);
			$this->querys = $memQuery;
		}
		return $this;
	}

	/**
	 * Get default query
	 * @return string
	 */
	public function getDefaultQuery() {
		return static::DEFAULT_QUERY_REPOSITORY.static::QUERY_SEPARATOR.static::DEFAULT_QUERY_METHOD;
	}

	/**
	 * Set default query (remove all querys)
	 * @return Basedirectory
	 */
	public function setDefaultQuery() {
		$this->setQuery(true);
		return $this;
	}

	/**
	 * Get first query
	 * @return string
	 */
	public function getQuery() {
		return reset($this->querys);
	}

	/**
	 * Get querys
	 * @return array
	 */
	public function getQuerys() {
		if(!is_array($this->querys)) {
			$this->initQuerys();
			$this->initSorts();
		}
		if($this->isUniqueQuery()) $this->querys = [$this->getQuery()];
		return $this->querys;
	}

	/**
	 * Is this Basedirectory a query directory?
	 * @return boolean
	 */
	public function isQuery() {
		$defaultQuery = $this->getDefaultQuery();
		$this->getQuerys();
		// if(!is_array($this->getQuerys())) return $this->isquery = false;
		$this->isquery = !(in_array($defaultQuery, $this->querys) && count($this->querys) === 1);
		return $this->isquery;
	}

	/**
	 * Get query choices for form
	 * @return array
	 */
	public function getQueryChoices() {
		return $this->serviceBasedirectory->getBdqueryChoices();
	}

	/**
	 * Get mixedquery (has dirchilds AND childs by query)
	 * @return boolean
	 */
	public function getMixedquery() {
		return $this->mixedquery;
	}

	/**
	 * Is mixedquery (has dirchilds AND childs by query)
	 * @return boolean
	 */
	public function isMixedquery() {
		return $this->mixedquery;
	}

	/**
	 * Set mixedquery
	 * @param boolean $mixedquery
	 * @return Basedirectory
	 */
	public function setMixedquery($mixedquery) {
		$this->mixedquery = $mixedquery;
		if(!$this->isQuery()) $this->mixedquery = false;
		return $this;
	}

	/**
	 * Childs contains dirchilds OR dirchilds whith query
	 * @return boolean
	 */
	public function isChildsOrChildsAndQuery() {
		return $this->isMixedquery() || !$this->isQuery();
	}

	/**
	 * Childs contains ONLY dirchilds
	 * @return boolean
	 */
	public function isOnlyChilds() {
		return !$this->isQuery();
	}

	/***** CHILDS *****/

	/**
	 * Get childs
	 * @param integer $page = -1
	 * @param integer $length = null
	 * @param boolean $publicFiltered = 'auto'
	 * @return ArrayCollection <Item>
	 */
	public function getChilds($page = -1, $length = null, $publicFiltered = 'auto', $asFacticees = false) {
		//$this->addTestCount(__METHOD__);

		if($asFacticees) {
			$page = -1;
			$length = null;
		}
		// if($this->isDeepControl()) $this->getDirchilds();
		if(!($this->dirchilds instanceOf ArrayCollection)) $this->dirchilds = new ArrayCollection();
		if($this->isOnlyChilds() && empty($this->linkdatas)) return new ArrayCollection();
		// if($this->isOnlyChilds() && !$this->dirchilds->count()) return new ArrayCollection();
		serviceBasedirectory::computePagination($page, $length);
		$this->page = $page;
		$this->length = $length;
		$publicFiltered = is_bool($publicFiltered) ? $publicFiltered : $this->isPublicFiltered();
		$options = $this->isChildsOrChildsAndQuery() ? ['includes' => $this->getLinkdatasAsUniqueValue(), 'excludes' => $this->getExcludedirchilds()] : [];
		$repositoryAndMethods = $this->getQueryRepositorysAndMethods();
		$options['multiquerys'] = count($repositoryAndMethods) > 1 && !$this->isUniqueQuery();
		$options['typeResult'] = $asFacticees ? 'facticee' : 'entity';
		// echo('<div>Count Querys: '.count($repositoryAndMethods).'</div>');
		foreach ($repositoryAndMethods as $query) {
			// echo('<pre><h3>Query for childs of '.$this->getName().'</h3>');
			// echo('<div>Repository method: '.get_class($query['repository']).'::'.$query['method'].'</div>');
			$queryChilds = $query['repository']->{$query['method']}($this->page, $this->length, $this->sorts, $publicFiltered, $options);
			// echo('<div>Childs found: '.count($queryChilds).'/'.count($this->linkdatas).' linkdatas</div>');
			// echo('<ul>');
			// foreach ($queryChilds as $child) {
				// echo('<li>'.$child->getId().'#'.$child->getName().' / '.$this->getItemOid($child).'</li>');
			// }
			// echo('</ul>');
			// die('</pre>');
			// echo('</pre>');
			if($this->isUniqueQuery()) break;
		}
		if($this->isPositionSorted()) {
			// Order by linkdatas
			$linkdatas = $this->getLinkdatasAsUniqueValue();
			$result = usort($queryChilds, function($a, $b) use ($linkdatas) {
				if(false === $a_pos = array_search($a->getMicrotimeid(), $linkdatas)) return -1;
				if(false === $b_pos = array_search($b->getMicrotimeid(), $linkdatas)) return 1;
				return $a_pos > $b_pos ? 1 : -1;
			});
			if(!$result) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): sort childs has failed!", 1);
		}
		if($asFacticees) return new ArrayCollection($queryChilds);

		foreach ($queryChilds as $item) $item->setCurrentParent($this);
		$this->childs = new ArrayCollection($queryChilds);
		// $this->childs = $this->childs->filter(function($child) {
		// 	return !$child->isRootDriveOrSystem();
		// });
		if($this->isOnlyChilds() && empty($this->length) && $this->dirchilds->count() > $this->childs->count()) {
			// include not persisted Items
			if($this->dirchilds->count() !== count($this->linkdatas)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): has not persisted childs, but linkdatas is not synchronized!", 1);
			return $this->getDirchilds();
		} else if($this->isOnlyChilds() && empty($this->length)) {
			// echo('<div>Restore Dirchilds...</div>');
			$this->setDirchilds($this->childs);
		}
		return $this->childs;
	}

	public function getChildsAsFacticees($publicFiltered = 'auto') {
		return $this->getChilds(-1, null, $publicFiltered, true);
	}

	public function getElementsForMenu($publicFiltered = 'auto', $asFacticees = true) {
		// $repositoryAndMethods = $this->getQueryRepositorysAndMethods();
		$repository = $this->serviceBasedirectory->getQueryRepository(static::DEFAULT_QUERY_REPOSITORY);
		$options = $this->isChildsOrChildsAndQuery() ? ['includes' => $this->getLinkdatasAsUniqueValue(), 'excludes' => $this->getExcludedirchilds()] : [];
		$options['typeResult'] = $asFacticees ? 'facticee' : 'entity';
		$queryChilds = $repository->bdq_findForMenu($this->sorts, $publicFiltered, $options);
		if($this->isPositionSorted()) {
			// Order by linkdatas
			$linkdatas = $this->getLinkdatasAsUniqueValue();
			$result = usort($queryChilds, function($a, $b) use ($linkdatas) {
				if(false === $a_pos = array_search($a->getMicrotimeid(), $linkdatas)) return -1;
				if(false === $b_pos = array_search($b->getMicrotimeid(), $linkdatas)) return 1;
				return $a_pos > $b_pos ? 1 : -1;
			});
			if(!$result) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): sort childs has failed!", 1);
		}
		return $queryChilds;
	}

	public function getNamedChilds($name, $onlyHards = true) {
		$found = new ArrayCollection();
		$childs = $onlyHards ? $this->getSolidChilds() : $this->dirchilds;
		foreach ($childs as $child) {
			if($child->getName() === $name) $found->add($child);
		}
		return $found;
	}

	public function isPaged() {
		return is_integer($this->length);
	}

	public function getNumberPages($page = -1, $length = null) {
		serviceBasedirectory::computePagination($page, $length);
		// $numberPages = ???
		return $numberPages;
	}

	public function getchilds_nextPage() {
		if(!$this->isPaged()) return new ArrayCollection();
		return $this->getChilds($this->page + 1, $length);
	}

	public function getchilds_previousPage() {
		if(!$this->isPaged()) return new ArrayCollection();
		return $this->getChilds($this->page + 1, $length);
	}

	public function hasChilds_nextPage() {
		if(!$this->isPaged()) return false;
		return true;
	}

	public function hasChilds_previousPage() {
		if(!$this->isPaged() || $this->page < 1) return false;
		return true;
	}

	public function setChilds($childs) {
		foreach ($this->getChilds(-1, null, false) as $child) {
			if(!$childs->contains($child)) $this->removeChild($child);
		}
		foreach ($childs as $child) $this->addChild($child);
		return $this;
	}

	/**
	 * Add child
	 * @param Item $child
	 * @param string $askedTypelink = null
	 * @param integer $position = -1 (-1 = set to last position)
	 * @param boolean $keepOldParentAsSymlink = false
	 * @return string (typelink)
	 */
	public function addChild(Item $child, $askedTypelink = null, $position = -1, $keepOldParentAsSymlink = false) {
		$this->addTestCount(__METHOD__);

		$this->computeDirectoryChildRelation($child, $askedTypelink, $position, $keepOldParentAsSymlink);
		// die('Try add child '.$child->getShortname().' '.json_encode($child->getNameOrTempname()).' => '.json_encode($child->getParentTypelink($this, true)).'.');
		return $child->getParentTypelink($this, true);
	}

	/**
	 * Remove child
	 * @param Item $child
	 * @return boolean
	 */
	public function removeChild(Item $child) {
		$this->computeDirectoryChildRelation($child, serviceBasedirectory::TYPELINK_REMOVE);
		// die('Try remove child '.$child->getShortname().' '.json_encode($child->getNameOrTempname()).' => removed '.json_encode(!$child->hasDirparent($this)).'.');
		return !$child->hasDirparent($this);
		// return !$this->dirchilds->contains($child);
	}

	/**
	 * Remove all childs
	 * @return boolean
	 */
	public function removeChilds() {
		return $this->removeDirchilds();
	}

	/**
	 * Get solid Childs
	 * @param boolean $setAsCurrentParent = true
	 * @return ArrayCollection <Item>
	 */
	public function getSolidChilds($setAsCurrentParent = true) {
		$this->addTestCount(__METHOD__);

		$parent = $this;
		return $this->dirchilds->filter(function($dirchild) use ($setAsCurrentParent) {
			$isSolid = $dirchild->getParent() === $this;
			if($isSolid && $setAsCurrentParent) $dirchild->setCurrentParent($this);
			return $isSolid;
		});
	}

	/**
	 * Has solid Childs
	 * @return boolean
	 */
	public function hasSolidChilds() {
		return !$this->getSolidChilds(false)->isEmpty();
	}

	public function hasChild(Item $child = null) {
		if($this->isOnlyChilds()) {
			return $child instanceOf Item ? $this->hasDirchild($child) : !$this->dirchilds->isEmpty();
		}
		$childs = $this->getChilds();
		return $child instanceOf Item ? $childs->contains($child) : !$childs->isEmpty();
	}


	/*************************************************************************************/
	/*** FORMS CHILDS
	/*************************************************************************************/

	public function getChildsPagewebs() {
		return $this->getChilds(-1, null, false)->filter(function($child) { return $child instanceOf Item; });
	}
	public function setChildsPagewebs($childs_pagewebs) {
		$childs = $this->getChildsPagewebs();
		foreach ($childs as $child) {
			if(!$childs_pagewebs->contains($child)) $this->removeChild($child);
		}
		foreach ($childs_pagewebs as $child) $this->addChild($child);
		return $this;
	}

	public function getChildsDirectorys() {
		return $this->getChilds(-1, null, false)->filter(function($child) { return $child instanceOf Menu; });
	}

	public function setChildsDirectorys($childs_directorys) {
		$childs = $this->getChildsDirectorys();
		foreach ($childs as $child) {
			if(!$childs_directorys->contains($child)) $this->removeChild($child);
		}
		foreach ($childs_directorys as $child) $this->addChild($child);
		return $this;
	}

	/*************************************************************************************/
	/*** RESOLVES & UPDATE NESTED DATA
	/*************************************************************************************/

	// public function tryResolveOwner($findOutOwnerTierInterface = true) {
	// 	return parent::tryResolveOwner($findOutOwnerTierInterface);
	// }

	public function tryResolveOwnerdir() {
		$this->addTestCount(__METHOD__);

		if(!$this->isCompleteOrIncompleteRoot()) {
			if(!empty($this->parent) || $this->tryResolveParent()) {
				$this->parent->tryResolveOwnerdir();
			}
		} else {
			$od = $this->getOwnerdirs();
			$search = 'getName';
			switch ($od->count()) {
				case 0:
					if(preg_match('/^@system/', $this->$search())) {
						$new_owner_systemdirs = $this->serviceBasedirectory->findOwnersystemdirs($this);
						if(count($new_owner_systemdirs) > 0) {
							// $owner = reset($new_owner_systemdirs); throw new Exception("found owner for root ".$this->getShortname()." ".json_encode($this->$search())." > ".$owner->getShortname()." ".json_encode($owner->$search()).".", 1);
							$this->setOwnersystemdir(reset($new_owner_systemdirs), true, true);
						} else {
							// Not found, try owner
							if($this->getOwnerdirs()->isEmpty() && $this->owner instanceOf Item) $this->owner->setSystemDir($this, true, true);
							if($this->getOwnerdirs()->isEmpty()) throw new Exception("Could not retrieve owner of root ".$this->getShortname()." ".json_encode($this->$search())."! Got ".count($new_owner_systemdirs)." response(s).", 1);
						}
						// $this->owner->setSystemDir($this, false);
					}
					if(preg_match('/^@drive/', $this->$search())) {
						$new_owner_drivedirs = $this->serviceBasedirectory->findOwnerdrivedirs($this);
						if(count($new_owner_drivedirs) > 0) {
							// $owner = reset($new_owner_drivedirs); throw new Exception("found owner for root ".$this->getShortname()." ".json_encode($this->$search())." > ".$owner->getShortname()." ".json_encode($owner->$search()).".", 1);
							$this->setOwnerdrivedir(reset($new_owner_drivedirs), true, true);
						} else {
							// Not found, try owner
							if($this->getOwnerdirs()->isEmpty() && $this->owner instanceOf Item) $this->owner->setSystemDir($this, true, true);
							if($this->getOwnerdirs()->isEmpty()) throw new Exception("Could not retrieve owner of root ".$this->getShortname()." ".json_encode($this->$search())."! Got ".count($new_owner_drivedirs)." response(s).", 1);
						}
						// $this->owner->setDriveDir($this, false);
					}
					// throw new Exception("1 Could not retrieve owner of root ".$this->getShortname()." ".json_encode($this->$search())."! Got ".$this->getOwnerdirs()->count()." response(s).", 1);
					break;
				case 1:
					$ownerdir = $od->first();
					if(preg_match('/^@system/', $this->$search())) {
						if($ownerdir->getSystemDir() !== $this) {
							$new_owner_systemdirs = $this->serviceBasedirectory->findOwnersystemdirs($this);
							if(count($new_owner_systemdirs) > 0) {
								// $owner = reset($new_owner_systemdirs); 	throw new Exception("found owner for root ".$this->getShortname()." ".json_encode($this->$search())." > ".$owner->getShortname()." ".json_encode($owner->$search()).".", 1);
								$ownerdir = reset($new_owner_systemdirs);
								$this->setOwnersystemdir($ownerdir, true, true);
								if($ownerdir->getSystemDir() !== $this) throw new Exception("Could not retrieve owner of root ".$this->getShortname()." ".json_encode($this->$search())."! Got ".count($ownerdir)." response(s).", 1);
							} else {
								// Not found, try owner
								if($this->getOwnerdirs()->isEmpty() && $this->owner instanceOf Item) $this->owner->setSystemDir($this, true, true);
								if($this->getOwnerdirs()->isEmpty()) throw new Exception("Could not retrieve owner of root ".$this->getShortname()." ".json_encode($this->$search())."! Got ".count($new_owner_drivedirs)." response(s).", 1);
							}
						}
					}
					if(preg_match('/^@drive/', $this->$search())) {
						if($ownerdir->getDriveDir() !== $this) {
							$new_owner_drivedirs = $this->serviceBasedirectory->findOwnerdrivedirs($this);
							if(count($new_owner_drivedirs) > 0) {
								// $owner = reset($new_owner_drivedirs); 	throw new Exception("found owner for root ".$this->getShortname()." ".json_encode($this->$search())." > ".$owner->getShortname()." ".json_encode($owner->$search()).".", 1);
								$ownerdir = reset($new_owner_drivedirs);
								$this->setOwnerdrivedir($ownerdir, true, true);
								if($ownerdir->getDriveDir() !== $this) throw new Exception("Could not retrieve owner of root ".$this->getShortname()." ".json_encode($this->$search())."! Got ".count($ownerdir)." response(s).", 1);
							} else {
								// Not found, try owner
								if($this->getOwnerdirs()->isEmpty() && $this->owner instanceOf Item) $this->owner->setSystemDir($this, true, true);
								if($this->getOwnerdirs()->isEmpty()) throw new Exception("Could not retrieve owner of root ".$this->getShortname()." ".json_encode($this->$search())."! Got ".count($new_owner_drivedirs)." response(s).", 1);
							}
						}
					}
					// throw new Exception("2 Could not retrieve owner of root ".$this->getShortname()." ".json_encode($this->$search())."! Got ".$this->getOwnerdirs()->count()." response(s).", 1);
					break;
				default:
					if(preg_match('/^@system/', $this->$search())) {
						$this->ownerdrivedir->setDriveDir(null, false, true);
						$this->ownerdrivedir = null;
					} else if(preg_match('/^@drive/', $this->$search())) {
						$this->ownersystemdir->setSystemDir(null, false, true);
						$this->ownersystemdir = null;
					} else {
						// no type found!
					}
					// throw new Exception("3 Could not retrieve owner of root ".$this->getShortname()." ".json_encode($this->$search())."! Got ".$this->getOwnerdirs()->count()." response(s).", 1);
					break;
			}
			// throw new Exception("Error line ".__LINE__." ".__METHOD__."(): System = ".($this->ownersystemdir instanceOf Item ? $this->ownersystemdir->getShortname()." ".json_encode($this->ownersystemdir->getName()) : json_encode(NULL))." / Drive = ".($this->ownerdrivedir instanceOf Item ? $this->ownerdrivedir->getShortname()." ".json_encode($this->ownerdrivedir->getName()) : json_encode(NULL)).".", 1);
			if(!$this->isValidOwnerdir()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Could not retrieve ownerdir of root ".$this->getShortname()." ".json_encode($this->getName())."! Got System = ".($this->ownersystemdir instanceOf Item ? $this->ownersystemdir->getShortname()." ".json_encode($this->ownersystemdir->getName()) : json_encode(NULL))." / Drive = ".($this->ownerdrivedir instanceOf Item ? $this->ownerdrivedir->getShortname()." ".json_encode($this->ownerdrivedir->getName()) : json_encode(NULL)).".", 1);
		}
		$this->getOwnerdir(true);
		return $this->isValidOwnerdir();
	}

	// /**
	//  * Update all paths
	//  * BEWARE: to take car of PATHSLUG, ONLY USE IT IN ONFLUSH CONTEXT, BECAUSE slugs are updated juste before OnFlush!!!
	//  * @return boolean (true if changed)
	//  */
	// public function updateNestedData(OnFlushEventArgs $args = null, $updateChilds = true) {
	// 	parent::updateNestedData($args, false);
	// 	// Update solid dirchilds (hardlink)
	// 	if($updateChilds || $args instanceOf OnFlushEventArgs) {
	// 		foreach ($this->getSolidChilds(false) as $dirchild) {
	// 			$dirchild->updateNestedData($args);
	// 		}
	// 	}
	// 	return $this;
	// }


	/*************************************************************************************/
	/*** COMPUTE ATTACHEMENT BETWEEN PARENT & CHILD
	/*************************************************************************************/

	/**
	 * Compute relation between parent and child
	 * $askedTypelink =
	 * 		- null (auto)
	 * 		- string TYPELINK_HARDLINK
	 * 		- string TYPELINK_SYMLINK
	 * 		- string TYPELINK_REMOVE --> for remove from parent
	 * 		- string TYPELINK_REPLACE --> for replace parent
	 * @param Item $item
	 * @param mixed $askedTypelink = null
	 * @param integer $position = -1
	 * @param boolean $keepOldParentAsSymlink = false
	 * @return Basedirectory
	 */
	public function computeDirectoryChildRelation(Item $child, $askedTypelink = null, $position = -1, $keepOldParentAsSymlink = false) {
		$this->addTestCount(__METHOD__);

		if($child instanceOf Tempfile) DevTerminal::contextException($this, false, "Can not add Tempfile as child!", null, __LINE__, __METHOD__);
		// DEFINE OWNER
		// $owner = $this->getOwnerdir();

		// Same parent
		$is_same = $child->getParent() === $this;

		// Change parent
		if($child->getParent() instanceOf Basedirectory && !$is_same && $askedTypelink !== serviceBasedirectory::TYPELINK_REPLACE) {
			if(!$keepOldParentAsSymlink) {
				$child->getParent()->computeDirectoryChildRelation($child, serviceBasedirectory::TYPELINK_REPLACE);
			} else {
				$child->_bd_removeParent(true);
			}
		}
		// END change parent

		// Check parent (this)
		if($this->isCheckModeContext() && !$this->isValid()) $this->checkAndRepairValidity("Failed to check and repair Item used for new parent ".$this->getShortname()." ".json_encode($this->getNameOrTempname()).".");

		// DEFINE TYPELINK
		$typelink = $this->getTypelinkForChild($child, $askedTypelink);
		switch ($typelink) {
			case serviceBasedirectory::TYPELINK_REPLACE:
				// Replace
				// if($child->hasParent($this)) {
					$this->removeDirchild($child);
					// $child->_bd_removeDirparent($this);
					// $child->_bd_removeParent(true);
					return $this;
				// }
				// END replace
				break;
			case serviceBasedirectory::TYPELINK_REMOVE:
				// throw new Exception("Action : ".serviceBasedirectory::TYPELINK_REMOVE, 1);
				// Remove
				// if($child->hasParent($this) && !($child instanceOf User) && !$child->isCompleteOrIncompleteRoot()) {
				// 	if($this->isDeepControlOrCheckModeContext()) DevTerminal::contextException($child, false, "Can not remove parent ".$this->getShortname()." ".json_encode($this->getNameOrTempname())."! (Owner is ".$this->getOwner()->getShortname()." ".json_encode($this->getOwner()->getNameOrTempname()).")", null, __LINE__, __METHOD__);
				// 	return $this;
				// }
				$this->removeDirchild($child);
				if(!$child->_bd_removeDirparent($this) || $this->hasChild($child)) DevTerminal::contextException($child, false, "Can not remove parent ".$this->getShortname()." ".json_encode($this->getNameOrTempname())."! (Owner is ".$this->getOwner()->getShortname()." ".json_encode($this->getOwner()->getNameOrTempname()).")", null, __LINE__, __METHOD__);
				if(empty($this->parent) || !$child->hasParent($this)) return $this;
				// END remove
				break;
			case serviceBasedirectory::TYPELINK_HARDLINK:
				// Hard link
				$result = $child->_bd_setParent($this, $keepOldParentAsSymlink);
				if($result) {
					if(!$this->hasDirchild($child)) $this->addDirchild($child, $position);
						else $this->setChildPosition($child, $position);
					// if($child->getOwner() !== $this->owner) $child->setOwner($this->owner, $keepOldParentAsSymlink);
				}
				break;
				// END Hard link
			case serviceBasedirectory::TYPELINK_SYMLINK:
				// Symb link
				$this->addDirchild($child, $position);
				if($child->hasParent($this) && !$this->canHaveParent()) $child->_bd_removeParent();
				$child->_bd_addDirparent($this);
				return $this;
				// END Symb link
				break;
			default:
				// can not be child
				$this->removeDirchild($child);
				$child->_bd_removeDirparent($this);
				// END can not be child
				break;
		}
		// Check position if always child
		if($this->dirchilds->contains($child) && $position > -1) $this->setChildPosition($child, $position);

		if($this->isCheckModeContext()) {
			// Check parent (this)
			if(!$this->isValid()) $this->checkAndRepairValidity("Failed to check and repair parent ".$this->getShortname()." ".json_encode($this->getNameOrTempname()).".");
			// Check child
			// if(!$child->isValid()) $child->checkAndRepairValidity("Failed to check and repair child ".$child->getShortname()." ".json_encode($child->getNameOrTempname()).".");
		}

		// if($child->getOwner() !== $old_owner) {
		// 	$child->UpdateHimselfToOwnersAndManagers();
		// }

		$this->updateNestedData(null, $child);
		// $child->updateNestedData();
		// if(!$child->isValid()) $child->checkAndRepairValidity("Failed to check and repair child ".$child->getShortname()." ".json_encode($child->getNameOrTempname()).".");
		// $child->validityEntityReport('Child of parent '.$this->getShortname().' '.json_encode($this->getNameOrTempname()).'.', __LINE__, __METHOD__, true);

		return $this;
		// return $child->getParentTypelink($this, true);
	}


	/***** DIRCHILDS *****/

	// /**
	//  * Get valid children
	//  * @return array <string>
	//  */
	// public function getValidDirchilds() {
	// 	return "all";
	// }

	/** 
	 * Get Dirchilds
	 * @return ArrayCollection <Item>
	 */
	protected function getDirchilds() {
		$this->addTestCount(__METHOD__);

		$this->updateLinkdatas();
		// return $this->dirchilds;
		// order by linkdatas
		$sorteds = array_map(function($linkdatas) {
			$found = null;
			foreach ($this->dirchilds as $dirchild) {
				if($this->getItemOid($linkdatas) === $this->getItemOid($dirchild)) $found = $dirchild;
			}
			if(empty($found)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." is not synchronized linkdatas <-> dirchilds!", 1);
			return $found;
		}, $this->linkdatas);
		if($this->isDeepControl()) {
			foreach ($this->dirchilds as $dirchild) if(!($dirchild instanceOf Item)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this (only dirchilds) ".$this->getShortname()." ".json_encode($this->getName())." is not synchronized linkdatas <-> dirchilds!", 1);
			if($this->dirchilds->count() !== count($this->linkdatas)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this (only dirchilds) ".$this->getShortname()." ".json_encode($this->getName())." is not synchronized linkdatas <-> dirchilds!", 1);
			if($this->dirchilds->count() !== count($sorteds)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this (only dirchilds) ".$this->getShortname()." ".json_encode($this->getName())." is not synchronized linkdatas <-> dirchilds!", 1);
		}
		return $this->dirchilds = new ArrayCollection($sorteds);
	}

	public function getChildsForPathwalk() {
		return $this->getSolidChilds();
	}

	protected function setDirchilds($dirchilds) {
		if($this->isOnlyChilds()) {
			foreach ($this->dirchilds as $dirchild) {
				if(!$dirchilds->contains($dirchild) && !empty($dirchild->getId())) $this->removeDirchild($dirchild);
			}
			foreach ($dirchilds as $dirchild) $this->addDirchild($dirchild);
			$this->updateLinkdatas();
		}
		return $this;
	}

	/**
	 * Add dirchild
	 * @param Item $item
	 * @param integer $position = -1
	 * @return boolean
	 */
	protected function addDirchild(Item $item, $position = -1) {
		$this->addTestCount(__METHOD__, 1000);

		if(!$this->dirchilds->contains($item)) $this->dirchilds->add($item);
		if($this->dirchilds->contains($item)) {
			$this->addItemdata($item, $position);
			return true;
		}
		return false;
	}

	/**
	 * Has Dirchild
	 * @param Item $dirchildLink
	 * @return Basedirectory
	 */
	protected function hasDirchild(Item $item = null) {
		return $item instanceOf Item ? $this->dirchilds->contains($item) : $this->dirchilds->count() > 0;
	}

	/**
	 * Remove Item
	 * @param Item $item
	 * @param boolean $inverse = true
	 * @return boolean
	 */
	protected function removeDirchild(Item $item) {
		// $item->_bd_removeDirparent($this);
		$this->dirchilds->removeElement($item);
		$this->removeItemdata($item);
		return true;
	}

	/**
	 * Remove all Items
	 * @return boolean
	 */
	protected function removeDirchilds() {
		foreach ($this->dirchilds as $dirchild) $this->removeDirchild($dirchild, true);
		return $this->dirchilds->isEmpty();
	}

	// /**
	//  * Check all dirchilds
	//  * @return boolean (false if found duplicates)
	//  */
	// public function checkDirchilds() {
	// 	$count = $this->dirchilds->count();
	// 	$checks = new ArrayCollection();
	// 	$this->dirchilds = $this->dirchilds->filter(
	// 		function($item) use ($checks) {
	// 			if($checks->contains($item)) return false;
	// 			$checks->add($item);
	// 			return true;
	// 		}
	// 	);
	// 	return $count === $this->dirchilds->count();
	// }

	/**
	 * Can have parent Directory
	 * @param Basedirectory $parent = null
	 * @return boolean
	 */
	public function isChildeable(Basedirectory $parent = null) {
		return !$this->isCompleteOrIncompleteRoot();
	}

	/**
	 * Get an available typelink regarding to asked typelink
	 * @param Item $item
	 * @param string $askedTypelink = null
	 * @return string (typelink) | false
	 */
	public function getTypelinkForChild(Item $item, $askedTypelink = null) {
		$this->addTestCount(__METHOD__);

		if($askedTypelink === serviceBasedirectory::TYPELINK_REMOVE) return serviceBasedirectory::TYPELINK_REMOVE;
		if(
			$item === $this
			|| !$item->isChildeable($this)
			|| ($this->isCompleteOrIncompleteRoot() && !($item instanceOf Basedirectory))
			|| $item->isCompleteOrIncompleteRoot()
		) return false;

		$askedTypelink = in_array($askedTypelink, [serviceBasedirectory::TYPELINK_HARDLINK, serviceBasedirectory::TYPELINK_SYMLINK]) ? $askedTypelink : serviceBasedirectory::TYPELINK_HARDLINK;
		if($item instanceOf User) {
			$askedTypelink = serviceBasedirectory::TYPELINK_SYMLINK;
		} else {
			if($item instanceOf Basedirectory) {
				// $askedTypelink = $item->hasParent() ? serviceBasedirectory::TYPELINK_SYMLINK : serviceBasedirectory::TYPELINK_HARDLINK;
			} else if($this === $this->serviceBasedirectory->getLogicParent($item, false)) {
				$askedTypelink = serviceBasedirectory::TYPELINK_HARDLINK;
			} else {
				if($this->owner !== $item->getOwner() || $item->hasParent()) {
					$askedTypelink = serviceBasedirectory::TYPELINK_SYMLINK;
				}
				// if $item is a parent of $this, can only be a symb link
				if($askedTypelink === serviceBasedirectory::TYPELINK_HARDLINK) {
					$allparents = $this->getAllParents();
					if($item instanceOf Basedirectory && $allparents->contains($item)) $askedTypelink = serviceBasedirectory::TYPELINK_SYMLINK;
					// if($item instanceOf Basedirectory && $this->hasDirparent($item, true)) $askedTypelink = serviceBasedirectory::TYPELINK_SYMLINK;
				}
			}
		}
		return $askedTypelink;
	}


	/***** EXCLUDEDIRCHILDS *****/

	public function hasExcludedirchild(Item $item) {
		return in_array($item->getMicrotimeid(), $this->excludedirchilds);
	}

	public function addExcludedirchild(Item $item) {
		if(!$this->hasExcludedirchild($item)) $this->excludedirchilds[] = $item->getMicrotimeid();
		return $this;
	}

	public function removeExcludedirchild(Item $item) {
		$this->excludedirchilds->filter(function($microtimeid) use ($item) {
			return $microtimeid != $item->getMicrotimeid();
		});
		return $this;
	}

	public function setExcludedirchilds($items) {
		$this->excludedirchilds = [];
		foreach ($items as $item) $this->addExcludedirchild($item);
		return $this;
	}

	public function getExcludedirchilds() {
		return $this->excludedirchilds;
	}


	/*************************************************************************************/
	/*** SYSTEM & DRIVE DIRECTORY
	/*************************************************************************************/

	public function getOwnerdrivedir() {
		return $this->ownerdrivedir;
	}

	public function setOwnerdrivedir(Item $ownerdrivedir, $inverse = true, $force = false) {
		$this->addTestCount(__METHOD__);

		if($ownerdrivedir instanceOf Basedirectory) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$ownerdrivedir->getShortname()." ".json_encode($ownerdrivedir->getName())." can not be owner of drive dir!", 1);
		if($this->_is_repair() && !$ownerdrivedir->isValidOwnerAndParent()) {
			$ownerdrivedir->tryResolveOwner();
		}
		if(!$this->canTypedirBeChanged() && !$force) return false;
		// if(!empty($this->ownersystemdir)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." can not be DRIVEDIR because it is allready a SYSTEMDIR!", 1);
		if($this->ownerdrivedir !== $ownerdrivedir) {
			if(!empty($this->ownerdrivedir)) $this->ownerdrivedir->setDriveDir(null, false); // !!! only possible while deepControl or not defaultModeContext
			$this->ownerdrivedir = $ownerdrivedir;
			$this->ownerdrivedir->setDriveDir($this, false); // !!! IMPORTANT: inverse is forced
			// $this->setOwner($this->ownerdrivedir);
			$this->changeOwner($this->ownerdrivedir instanceOf User ?  $this->ownerdrivedir : $this->ownerdrivedir->getOwner());
		}
		// $this->setTypedir(serviceBasedirectory::TYPEDIR_DRIVE);
		$this->refreshName();
		$this->updateNestedData(null, true);
		// if($this->owner !== $this->ownerdrivedir) $this->setOwner($this->ownerdrivedir);
		return true;
	}

	public function getOwnersystemdir() {
		return $this->ownersystemdir;
	}

	public function setOwnersystemdir(Item $ownersystemdir, $inverse = true, $force = false) {
		$this->addTestCount(__METHOD__);

		if($ownersystemdir instanceOf Basedirectory) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$ownersystemdir->getShortname()." ".json_encode($ownersystemdir->getName())." can not be owner of system dir!", 1);
		if($this->_is_repair() && !$ownersystemdir->isValidOwnerAndParent()) {
			$ownersystemdir->tryResolveOwner();
		}
		if(!$this->canTypedirBeChanged() && !$force) return false;
		// if(!empty($this->ownerdrivedir)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." can not be SYSTEMDIR because it is allready a DRIVEDIR!", 1);
		if($this->ownersystemdir !== $ownersystemdir) {
			if(!empty($this->ownersystemdir)) $this->ownersystemdir->setSystemDir(null, false); // !!! only possible while deepControl or not defaultModeContext
			$this->ownersystemdir = $ownersystemdir;
			$this->ownersystemdir->setSystemDir($this, false); // !!! IMPORTANT: inverse is forced
			// $this->setOwner($this->ownersystemdir);
			$this->changeOwner($this->ownersystemdir instanceOf User ?  $this->ownersystemdir : $this->ownersystemdir->getOwner());
		}
		// $this->setTypedir(serviceBasedirectory::TYPEDIR_SYSTEM);
		$this->refreshName();
		$this->updateNestedData(null, true);
		// if($this->owner !== $this->ownersystemdir) $this->setOwner($this->ownersystemdir);
		return true;
	}

	// public function getOwnerdirs() {
	// 	$this->addTestCount(__METHOD__, 50);

	// 	$od = new ArrayCollection();
	// 	if($this->ownersystemdir instanceOf Item) $od->add($this->ownersystemdir);
	// 	if($this->ownerdrivedir instanceOf Item) $od->add($this->ownerdrivedir);
	// 	return $od;
	// }

	public function setDirstatic($dirstatic) {
		$this->dirstatic = (boolean) $dirstatic;
		if($this->isCompleteOrIncompleteRoot()) $this->dirstatic = true;
		return $this;
	}

	public function isDirstatic() {
		return $this->dirstatic;
	}

	public function getDirstatic() {
		return $this->dirstatic;
	}

	public function isRootDrive() {
		$is = !empty($this->ownerdrivedir);
		// $is = in_array($this->typedir, [serviceBasedirectory::TYPEDIR_DRIVE]);
		if($is && !$this->isRoot() && $this->isDeepControl()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." is RootDrive, but is not root!", 1);
		return $is;
	}

	public function isRootSystem() {
		$is = !empty($this->ownersystemdir);
		// $is = in_array($this->typedir, [serviceBasedirectory::TYPEDIR_SYSTEM]);
		if($is && !$this->isRoot() && $this->isDeepControl()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." is RootSystem, but is not root!", 1);
		return $is;
	}

	public function isSystem() {
		$rootparent = $this->getRootparent(true, true);
		return $rootparent instanceOf Basedirectory ? $rootparent->isRootSystem() : false;
		// return in_array($this->typedir, [serviceBasedirectory::TYPEDIR_SUB_SYSTEM,serviceBasedirectory::TYPEDIR_SYSTEM]);
	}

	public function canTypedirBeChanged() {
		if(empty($this->getId())) return true;
		return
			$this->isIncompleteRoot()
			|| $this->isDeepControlOrCheckModeContext()
			;
	}

	// public function setTypedir($typedir) {
	// 	$this->typedir = $typedir;
	// 	if($this->isRootDriveOrSystem()) {
	// 		$this->setDirstatic(true);
	// 	}
	// 	return $this;
	// }

	// public function getTypedir() {
	// 	return $this->typedir;
	// }


	/*************************************************************************************/
	/*** NAME FOR DIRECTORYS
	/*************************************************************************************/

	/**
	 * Get default inputname
	 * @return Item
	 */
	public function defaultInputname() {
		if($this->isCompleteOrIncompleteRoot()) {
			if($this->getOwnersystemdir() instanceOf Item) {
				$this->inputname = serviceBasedirectory::getRootName('system', $this->getOwnersystemdir(), true);
			} else if($this->getOwnerdrivedir() instanceOf Item) {
				$this->inputname = serviceBasedirectory::getRootName('drive', $this->getOwnerdrivedir(), true);
			} else {
				DevTerminal::contextException($this, false, "Could not retrieve ownerdir of root", null, __LINE__, __METHOD__);
			}
		} else {
			if(empty($this->inputname) && !empty($this->name)) $this->inputname = $this->name;
		}
		return $this;
	}

	public function setInputname($inputname) {
		$this->addTestCount(__METHOD__);

		if($this->isCompleteOrIncompleteRoot()) {
			if(serviceTools::computeTextOrNull($inputname)) $this->inputname = $inputname;
			$this->refreshName();
		} else {
			parent::setInputname($inputname);
		}
		// $this->defaultInputname();
		return $this;
	}

	public function getInputname() {
		if(empty($this->inputname) && $this->isCompleteOrIncompleteRoot()) $this->defaultInputname();
		return $this->inputname;
	}

	/**
	 * Set name --> can NOT CHANGE name on drive or system directory
	 * @param string $name
	 * @param boolean $updateNested = true
	 * @return self
	 */
	public function setName($name, $updateNested = true) {
		$this->addTestCount(__METHOD__);

		// $name = $this->removeRootPrefix($name);
		if($this->isCompleteOrIncompleteRoot()) {
			$this->refreshName($updateNested);
		} else {
			if(!$this->isDirstatic() || empty($this->name) || $this->isDeepControlOrCheckModeContext()) parent::setName($name, $updateNested);
		}
		return $this;
	}

	public function refreshName($updateNested = true) {
		$this->addTestCount(__METHOD__);

		if($updateNested) $this->saveChanges(__METHOD__);
		if($this->isCompleteOrIncompleteRoot()) {
			if(!$this->isValidOwnerdir()) $this->tryResolveOwnerdir();
			// if(!($this->getOwnersystemdir() instanceOf Item XOR $this->getOwnerdrivedir() instanceOf Item)) $this->tryResolveOwnerdir();
			if($this->getOwnersystemdir() instanceOf Item) {
				$this->name = serviceBasedirectory::getRootName('system', $this->getOwnersystemdir());
			} else if($this->getOwnerdrivedir() instanceOf Item) {
				$this->name = serviceBasedirectory::getRootName('drive', $this->getOwnerdrivedir());
			} else {
				DevTerminal::contextException($this, false, "Could not retrieve ownerdir of root", null, __LINE__, __METHOD__);
			}
		} else if(empty($this->name)) {
			$this->setName($this->inputname);
		}
		$this->defaultInputname();
		if($updateNested) $this->updateNestedDataIfchanges(__METHOD__);
		return $this;
	}


	/*************************************************************************************/
	/*** CHANGED FIELDS
	/*************************************************************************************/

	protected function getChangeFields() {
		return ['parent','owner','name'];
		// return ['lvl','pathname','pathslug','root','orhpan','rootparent','parent','owner'];
	}



	/*************************************************************************************/
	/*** ROOT PARENT
	/*************************************************************************************/

	// /**
	//  * compute solid root parent Directory
	//  * @param string $priority = 'owner' // ['owner','parent']
	//  * @param boolean $extended = false
	//  * @return boolean (true if changed)
	//  */
	// public function recomputeAllNesteds($priority = 'owner', $extended = false) {
	// 	$this->addTestCount(__METHOD__);

	// 	$changed = parent::recomputeAllNesteds($priority);
	// 	if(count($changed)) {
	// 		// if systemDir for Directorys
	// 		if($this->rootparent instanceOf Directory && $this->rootparent->isRootSystem()) {
	// 			$this->setTypedir(serviceBasedirectory::TYPEDIR_SUB_SYSTEM);
	// 		} else {
	// 			if(preg_match('/^'.serviceBasedirectory::TYPEDIR_SYSTEM.'/', $this->getTypedir())) $this->setTypedir(serviceBasedirectory::TYPEDIR_REGULAR);
	// 		}
	// 		// foreach ($this->getSolidChilds() as $child) {
	// 		// 	$child->recomputeAllNesteds('parent');
	// 		// 	// $child->setRootparent(empty($this->rootparent) ? $this : $this->rootparent);
	// 		// }
	// 	}
	// 	return $changed;
	// }



	/*************************************************************************************/
	/*** LINKDATAS DATA
	/*************************************************************************************/

	/**
	 * Get linkdatas
	 * @return array
	 */
	// protected function getLinkdatas() {
	// 	return $this->linkdatas;
	// }

	/**
	 * Is $oid formated as OID
	 * @return boolean
	 */
	public static function isItemOid($oid) {
		if(!is_string($oid)) return false;
		return preg_match('/^\\w{14}\\.\\w{8}$/', $oid);
	}

	/**
	 * Get formated itemdata of Item
	 * @return array
	 */
	protected function getDataFromItem(Item $item) {
		// return $this->getItemOid($item);
		$item->setCurrentParent($this, false, false);
		return [
			'microtimeid' => $this->getItemOid($item),
			// 'typelink' => $item->getTypelink(),
		];
	}

	/**
	 * get OID of item (Item, string(formated as oid) or array)
	 * @param mixed $item
	 * @return string
	 */
	protected function getItemOid($item) {
		if($item instanceOf Item) return $item->getMicrotimeid();
		if(is_string($item) && static::isItemOid($item)) return $item;
		if(is_array($item) && array_key_exists('microtimeid', $item)) return $item['microtimeid'];
		throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not find Oid with this data: ".(is_object($item) ? get_class($item) : json_encode($item))."!", 1);
	}

	/**
	 * Has itemdata of Item
	 * @return boolean
	 */
	protected function hasItemdata(Item $item) {
		foreach ($this->linkdatas as $itemdata) if($this->getItemOid($itemdata) === $this->getItemOid($item)) return true;
		return false;
	}

	/**
	 * Is linkdatas empty
	 * @return boolean
	 */
	protected function isLinkdatasEmpty() {
		return empty($this->linkdatas);
	}

	/**
	 * Get Item data
	 * @param Item $item
	 * @return array | null
	 */
	protected function getItemdata(Item $item) {
		$oid = $this->getItemOid($item);
		foreach ($this->linkdatas as $position => $itemdata) if($this->getItemOid($itemdata) === $oid) return $itemdata;
		return null;
	}

	/**
	 * Get Item position
	 * @param Item $item
	 * @return integer | null
	 */
	protected function getItemPosition(Item $item) {
		$oid = $this->getItemOid($item);
		foreach ($this->linkdatas as $position => $itemdata) {
			if($this->getItemOid($itemdata) === $oid) return $position;
		}
		return null;
	}

	/**
	 * Add Item in linkdatas
	 * @param Item $itemdata
	 * @param integer $position = -1
	 * @return Basedirectory
	 */
	protected function addItemdata(Item $item, $position = -1) {
		$this->removeItemdata($item);
		if(!is_integer($position)) $position = static::DEFAULT_POSITION;
		// if(!is_integer($position)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): position must be integer. Got ".(is_object($position) ? get_class($position) : json_encode($position))."!", 1);
		if($position >= count($this->linkdatas) || $position < -1) $position = -1;
		switch ($position) {
			case -1:
				array_push($this->linkdatas, $this->getDataFromItem($item));
				break;
			default:
				$pos = 0;
				$linkdatas = [];
				foreach ($this->linkdatas as $itemdata) {
					if($pos++ === $position) array_push($linkdatas, $this->getDataFromItem($item));
					array_push($linkdatas, $itemdata);
				}
				$this->linkdatas = array_values($linkdatas);
				break;
		}
		if(!$this->hasItemdata($item)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Item ".json_encode($item->getName())." was not added!", 1);
		return $this;
	}

	/**
	 * Remove Item from linkdatas
	 * @param Item $item
	 * @return Basedirectory
	 */
	public function removeItemdata(Item $item) {
		$oid = $this->getItemOid($item);
		$this->linkdatas = array_filter($this->linkdatas, function($itemdata) use ($oid) { return $this->getItemOid($itemdata) !== $oid; });
		$this->linkdatas = array_values($this->linkdatas);
		return $this;
	}

	/**
	 * Update all linkdatas with Item or array of Items
	 * @param mixed $items
	 * @return Basedirectory
	 */
	protected function updateLinkdatas($items = null) {
		$this->addTestCount(__METHOD__, 1000);

		if($this->isOnlyChilds()) {
			if($items instanceOf Item) {
				$data = $this->getDataFromItem($items);
				$oid = $this->getItemOid($data);
				if($this->isDeepControl() && $oid !== $this->getItemOid($items)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): oid ".json_encode($oid)." and item oid ".json_encode($this->getItemOid($items))." are not the same!", 1);
				$found = false;
				$this->linkdatas = array_map(function($itemdata) use ($data, $oid, &$found) {
					if($oid === $this->getItemOid($itemdata)) { $found = true; return $data; }
					return $itemdata;
				}, $this->linkdatas);
				if(!$found) $this->addItemdata($items);
				// if($this->isDeepControl()) $this->controlLinkdatas(true);
				$this->linkdatas = array_values($this->linkdatas);
				return $this;
			}
			// if(!is_array($items) || !($items instanceOf ArrayCollection)) $items = $this->childs;
			if(!is_array($items) || !($items instanceOf ArrayCollection)) $items = $this->dirchilds;
			// remove items not present in linkdatas
			$oids = $this->getItemsOids($items);
			$this->linkdatas = array_filter($this->linkdatas, function($linkdatas) use ($oids) {
				return in_array($this->getItemOid($linkdatas), $oids);
			});
			foreach ($items as $item) $this->updateLinkdatas($item);
		} else {
			if($this->isDeepControlOrCheckModeContext()) throw new Exception("Error : query option is not available yet!", 1);
		}
		return $this;
	}

	/**
	 * Get array of unique value from linkdatas (use $name = 'item' to get Entity)
	 * @return array
	 */
	public function getLinkdatasAsUniqueValue() {
		// $this->controlLinkdatas(true);
		return array_map(function($itemdata) { return $this->getItemOid($itemdata); }, $this->linkdatas);
	}

	protected function getItemsOids($items) {
		$oids = [];
		foreach ($items as $item) {
			$oid = $this->getItemOid($item);
			$oids[$oid] = $oid;
		}
		return $oids;
	}

	/*************************************************************************************/
	/*** CONTROL LINKDATAS
	/*************************************************************************************/

	/**
	 * Control Item data
	 * @param boolean $exceptionIfErrors = false
	 * @param boolean $repairBeforeAll = true
	 * @return array (strings of error messages)
	 */
	protected function controlLinkdatas($exceptionIfErrors = false, $repairBeforeAll = true) {
		if($repairBeforeAll) $this->updateLinkdatas();
		$messages = [];
		$oids = [];
		foreach ($this->linkdatas as $position => $itemdata) {
			$oid = $this->getItemOid($itemdata);
			if(!$this->isItemOid($oid)) $messages[] = "Oid ".json_encode($oid)." is not valid!";
			if(in_array($oid, $oids)) $messages[] = "Oid ".json_encode($oid)." found twice!";
			$found = false;
			foreach ($this->dirchilds as $dirchild) {
				if($oid === $this->getItemOid($dirchild)) $found = true;
				$oid2 = $this->getItemOid($dirchild);
				$found2 = false;
				foreach ($this->linkdatas as $itemdata2) {
					if($this->getItemOid($itemdata2) === $oid2) $found2 = true;
				}
				if(!$found2) $this->addItemdata($dirchild);
				$found2 = false;
				foreach ($this->linkdatas as $itemdata2) {
					if($this->getItemOid($itemdata2) === $oid2) $found2 = true;
				}
				if(!$found2) $messages[] = $dirchild->getShortname()." ".$dirchild->getName()." in dirchilds was not found in linkdatas!";
			}
			if(!$found) {
				$messages[] = $oid." in linkdatas was not found in dirchilds!";
			}
		}
		if(count($messages) && $exceptionIfErrors) {
			DevTerminal::launchException("Errors while control linkdatas:".PHP_EOL."<T5>- ".implode(PHP_EOL."<T5>- ", $messages), __LINE__, __METHOD__);
			// throw new Exception("Error line ".__LINE__." ".__METHOD__."(): ERROR: ".implode(' / ERROR: ', $messages), 1);
		}
		return $messages;
	}

	/*************************************************************************************/
	/*** ON/POST FLUSH
	/*************************************************************************************/

	public function onFlushItemdata(OnFlushEventArgs $args = null) {
		parent::onFlushItemdata($args);
		$this->updateLinkdatas();
		// foreach ($this->dirchilds as $dirchild) {
		// 	$dirchild->onFlushItemdata($args);
		// }
		// if($this->isDeepControlOrCheckModeContext()) $this->controlLinkdatas(true);
		return $this;
	}

	// public function postFlushItemdata(PostFlushEventArgs $args = null) {
	// 	parent::postFlushItemdata($args);
	// 	// if($this->isDeepControl()) $this->controlLinkdatas(true);
	// 	return $this;
	// }


	/*************************************************************************************/
	/*** LEVEL
	/*************************************************************************************/

	// /**
	//  * Get Dirchild level
	//  * @param Item $item
	//  * @return integer
	//  */
	// public function getChildLvl(Item $item) {
	// 	return $this->getCurrentLvl() + 1;
	// }



	/*************************************************************************************/
	/*** POSITIONS
	/*************************************************************************************/

	/**
	 * Set Dirchild position
	 * 0 for first, any number, or -1 for last position
	 * @param Item $item
	 * @param integer $position = -1 // last
	 * @return Basedirectory
	 */
	public function setChildPosition(Item $item, $position = -1) {
		$this->addItemdata($item, $position);
		return $this->getItemPosition($item);
	}

	/**
	 * Get Dirchild position (returns -1 if not found)
	 * @param Item $item
	 * @return integer|null
	 */
	public function getChildPosition(Item $item) {
		return $this->getItemPosition($item);
	}


	/**
	 * Get next position
	 * @return integer
	 */
	public function getNextPosition() {
		return count($this->linkdatas);
	}

	public function getPositionOrNext($position) {
		return $position < 0 ? $this->getNextPosition() : $position;
	}



}




