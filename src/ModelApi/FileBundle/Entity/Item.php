<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Doctrine\Common\Collections\ArrayCollection;
use Hateoas\Configuration\Annotation as Hateoas;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Doctrine\Common\EventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
// BaseBundle
use ModelApi\BaseBundle\Entity\DataFillableInterface;
use ModelApi\BaseBundle\Entity\Tag;
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceTools;
// use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Entity\DirectoryInterface;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Tempfile;
use ModelApi\FileBundle\Repository\ItemRepository;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\OwnerTierInterface;
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Entity\GroupableInterface;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// EventBundle
use ModelApi\EventBundle\Entity\Journey;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Service\serviceCruds;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Avatar;

use Website\SiteBundle\Controller\DefaultController;

use \Exception;
use \ReflectionClass;

/**
 * Item
 * 
 * @ORM\Entity(repositoryClass=ItemRepository::class)
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(name="`item`", options={"comment":"Items"})
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_item",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_USER", update="ROLE_USER", delete="ROLE_USER")
 * @Annot\HasTranslatable()
 */
abstract class Item implements NestedInterface, GroupableInterface, CrudInterface, DataFillableInterface {

	const DEFAULT_ICON = 'fa-file';
	const PROCESS_CONTROL = false; // true if DEV
	const DEFAULT_OWNER_DIRECTORY = '@@@default@@@';
	const ENTITY_SERVICE = serviceItem::class;
	const TEMP_NAME = '###temp_name###';
	const DEFAULT_POSITION = 0;

	const SHORTCUT_CONTROLS = false;
	const TEST_COUNT_VALIDS = 50;

	const TYPE_TEMPFILE = 'TYPE_TEMPFILE';
	const TYPE_ROOT = 'TYPE_ROOT';
	const TYPE_MENU = 'TYPE_MENU';
	const TYPE_DIRECTORY = 'TYPE_DIRECTORY';
	const TYPE_USER = 'TYPE_USER';
	const TYPE_OTI = 'TYPE_OTI';
	const TYPE_ENTREPRISE = 'TYPE_ENTREPRISE';
	const TYPE_TIER = 'TYPE_TIER';
	const TYPE_ITEM = 'TYPE_ITEM';


	// const TYPELINK_UNDEFINED = 'undefined';		// undefined
	// const TYPELINK_HARDLINK = 'hardlink'; 		// Hard link
	// const TYPELINK_SYMLINK = 'symlink';			// Symbolic link
	// const TYPELINK_PROCESSED = 'processed';		// if is Query or other
	// const TYPELINK_ERROR = 'error';				// error


	// use \ModelApi\BaseBundle\Transitional\Traits\Item_transitional;

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\BaseItem;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\BaseBundle\Traits\DataFiller;
	use \ModelApi\UserBundle\Traits\Groupables;
	use \ModelApi\CrudsBundle\Traits\Cruds;
	use \ModelApi\FileBundle\Traits\NestedUtilities;

	/**
	 * Id of Item
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", contexts={"all","list"}, order=10)
	 */
	protected $id;
	protected $jstreeId;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\EventBundle\Entity\Journey", mappedBy="items")
	 * @ORM\JoinTable(
	 * 		name="`item_journey`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 */
	protected $journeys;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\BaseBundle\Entity\Tag", inversedBy="items")
	 * @ORM\JoinTable(
	 * 		name="`item_tag`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true, "label"="Tags", "description"="Définissez quelques tags (entre 4 et 8 env.) pour un meilleur référencement du site internet.", "group_by"="parent", "class"="ModelApi\BaseBundle\Entity\Tag", "attr"={"class"="select-choice"}, "query_builder"="auto"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true, "label"="Tags", "description"="Définissez quelques tags (entre 4 et 8 env.) pour un meilleur référencement du site internet.", "group_by"="parent", "class"="ModelApi\BaseBundle\Entity\Tag", "attr"={"class"="select-choice"}, "query_builder"="auto"},
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $tags;

	/**
	 * @ORM\ManyToMany(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", mappedBy="dirchilds", cascade={"persist"})
	 * @ORM\JoinTable(
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=200)
	 */
	protected $dirparents;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 * @CRUDS\Show(role="ROLE_EDITOR", order=200)
	 */
	protected $parent;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 */
	protected $rootparent;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\UserBundle\Entity\Tier", cascade={"persist"})
	 * @ORM\JoinColumn(name="`items_tierowner`", nullable=true)
	 * @CRUDS\Create(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update=false,
	 * 		options={"required"=true, "multiple"=false, "group_by"="shortname", "label"="Créateur/propriétaire", "class"="ModelApi\UserBundle\Entity\Tier"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		options={"required"=true, "multiple"=false, "group_by"="shortname", "label"="Créateur/propriétaire", "class"="ModelApi\UserBundle\Entity\Tier"},
	 * 		contexts={"change_owner"},
	 * 	)
	 * @CRUDS\Show(role=true, order=1)
	 * @Annot\AttachUser(context="user", replace=false)
	 */
	protected $owner;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Item", cascade={"persist"})
	 * @ORM\JoinColumn(name="`items_tierownerdir`", nullable=true)
	 * @CRUDS\Show(role=true, order=1)
	 */
	protected $ownerdir;

	protected $owners;

	/**
	 * @ORM\ManyToMany(targetEntity="ModelApi\UserBundle\Entity\Tier", fetch="EXTRA_LAZY")
	 * @ORM\JoinTable(
	 * 		name="`item_manager`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 * @CRUDS\Show(role=true, order=2)
	 */
	protected $itemmanagers;

	/**
	 * Paramètres dirchilds
	 * @var array
	 * @ORM\Column(name="`linkdatas`", type="json_array", nullable=false, unique=false)
	 */
	protected $linkdatas;


	/*************************************************************************************/
	/*** BEGIN : CURRENT DIR RELATIVE DATA
	/*************************************************************************************/

	/**
	 * Current parent
	 * @var integer
	 */
	protected $currentParent;

	/**
	 * Child position regarding to hardlink parent
	 * @var integer
	 */
	protected $position;

	/**
	 * Child level regarding to hardlink parent
	 * @var integer
	 */
	protected $currentLvl;

	/**
	 * Current path of names
	 * @var string
	 */
	protected $currentPathname;

	/**
	 * Current path of slugs
	 * @var string
	 */
	protected $currentPathslug;


	/*************************************************************************************/
	/*** END : CURRENT DIR RELATIVE DATA
	/*************************************************************************************/


	/**
	 * Path name
	 * @var integer
	 * @ORM\Column(name="lvl", type="integer", nullable=false, unique=false)
	 */
	protected $lvl;

	/**
	 * Name that was input by user
	 * @var string
	 * @ORM\Column(name="inputname", type="string", nullable=false, unique=false)
	 * @CRUDS\Creates(
	 * 		@CRUDS\Create(
	 * 			show=true,
	 * 			update=true,
	 * 			order=1,
	 * 			options={"required"=true, "label"="Identifiant", "by_reference"=false, "attr"={"placeholder"="field.name"}, "description"="Nom informatique de l'élément. N'est pas utilisé sur le site, <strong class='text-danger'>évitez de le modifier, car cela modifie l'URL d'accès, et cela peut donc <u>briser</u> les liens externes et certains liens internes.</strong>."},
	 * 		),
	 * 		@CRUDS\Create(
	 * 			show=true,
	 * 			update=true,
	 * 			order=1,
	 * 			options={"required"=true, "label"="Identifiant", "by_reference"=false, "attr"={"placeholder"="field.name"}, "description"="Nom informatique de l'élément. N'est pas utilisé sur le site, <strong class='text-danger'>évitez de le modifier, car cela modifie l'URL d'accès, et cela peut donc <u>briser</u> les liens externes et certains liens internes.</strong>."},
	 * 			contexts={"public"},
	 * 		),
	 * 	)
	 * @CRUDS\Updates(
	 * 		@CRUDS\Update(
	 * 			show=true,
	 * 			update=true,
	 * 			order=1,
	 * 			options={"required"=true, "label"="Identifiant", "by_reference"=false, "attr"={"placeholder"="field.name"}, "description"="Nom informatique de l'élément. N'est pas utilisé sur le site, <strong class='text-danger'>évitez de le modifier, car cela modifie l'URL d'accès, et cela peut donc <u>briser</u> les liens externes et certains liens internes.</strong>."},
	 * 		),
	 * 		@CRUDS\Update(
	 * 			show=true,
	 * 			update=true,
	 * 			order=1,
	 * 			options={"required"=true, "label"="Identifiant", "by_reference"=false, "attr"={"placeholder"="field.name"}, "description"="Nom informatique de l'élément. N'est pas utilisé sur le site, <strong class='text-danger'>évitez de le modifier, car cela modifie l'URL d'accès, et cela peut donc <u>briser</u> les liens externes et certains liens internes.</strong>."},
	 * 			contexts={"public"},
	 * 		),
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $inputname;

	/**
	 * Name normalized
	 * @var string
	 * @ORM\Column(name="name", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 	)
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $name;

	/**
	 * normalizeNameMethod
	 * @var string
	 * @ORM\Column(name="norm_name_method", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=102,
	 * 		options={"required"=true, "choices"="auto"},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=102,
	 * 		options={"required"=true, "choices"="auto"},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Show(role=false, order=102)
	 */
	protected $normalizeNameMethod;

	/**
	 * Path name
	 * @var string
	 * @ORM\Column(name="pathname", type="text", nullable=false, unique=false)
	 */
	protected $pathname;

	/**
	 * Path slug
	 * @var string
	 * @ORM\Column(name="pathslug", type="text", nullable=true, unique=false)
	 */
	protected $pathslug;

	/**
	 * Actions non autorisées
	 * @var array
	 * @ORM\Column(name="static", type="json_array", nullable=false, unique=false)
	 */
	protected $static;

	/**
	 * Is directory
	 * @var boolean
	 * @ORM\Column(name="dirinterface", type="boolean", nullable=true, unique=false)
	 */
	protected $dirinterface;

	/**
	 * Root (has no solid parent link)
	 * @var boolean
	 * @ORM\Column(name="root", type="boolean", nullable=true, unique=false)
	 */
	protected $root;

	/**
	 * Orphan (has no parent link - solid or symbolic)
	 * @var boolean
	 * @ORM\Column(name="orphan", type="boolean", nullable=true, unique=false)
	 */
	protected $orphan;

	/**
	 * Slug
	 * @var string
	 * @ORM\Column(name="slug", type="string", length=128, unique=true)
	 * @Gedmo\Slug(fields={"name"})
	 */
	protected $slug;


	protected $processControls;
	protected $jstreeState;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\FileBundle\Entity\Directory", cascade={"persist"}, inversedBy="ownerdrivedir")
	 * @ORM\JoinColumn(nullable=true)
	 */
	protected $driveDir;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\FileBundle\Entity\Directory", cascade={"persist"}, inversedBy="ownersystemdir")
	 * @ORM\JoinColumn(nullable=true)
	 */
	protected $systemDir;

	/**
	 * @var integer
	 * Pageweb
	 * @ORM\ManyToOne(targetEntity="ModelApi\PagewebBundle\Entity\Pageweb")
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=500,
	 * 		options={"multiple"=false, "required"=false, "choices"="auto", "description"="<strong>Modèle de mise en page</strong> utilisé pour afficher cet élément"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=500,
	 * 		options={"multiple"=false, "required"=false, "choices"="auto", "description"="<strong>Modèle de mise en page</strong> utilisé pour afficher cet élément"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=500)
	 * @Annot\ChooseInitPageweb(setter="setPageweb", getter="getPageweb")
	 */
	protected $pageweb;
	/**
	 * @Annot\InjectPagewebList(setter="setPagewebChoices")
	 */
	protected $pagewebChoices;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Avatar", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=200,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPicone", "description"="Image carrée utilisée dans les listes en vignettes. Évitez les fichiers lourds. 400x400px suffisent, 200 à 500Ko."},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=200,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPicone", "description"="Image carrée utilisée dans les listes en vignettes. Évitez les fichiers lourds. 400x400px suffisent, 200 à 500Ko."},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Show(role=true, order=200)
	 */
	protected $picone;

	/**
	 * @var array
	 * @ORM\ManyToMany(targetEntity="ModelApi\BaseBundle\Entity\Typitem", mappedBy="items")
	 * @ORM\JoinTable(
	 * 		name="`item_typitem`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 */
	protected $categorys;
	protected $categorysChoices;


	// DEFINE TRANSLATABLE FIELDS

	/**
	 * @var string
	 * Nom de l'élément - utilisé également pour la balise <title>
	 * @ORM\Column(name="title", type="string", nullable=false, unique=false)
	 * @Annot\Translatable
	 */
	protected $title;

	/**
	 * @var string
	 * Sous-titre de l'élément - utilisé en supplément de title
	 * Texte de 1 à 3 lignes
	 * @ORM\Column(name="subtitle", type="text", nullable=true, unique=false)
	 * @Annot\Translatable
	 */
	protected $subtitle;

	/**
	 * @var string
	 * Nom court de l'élément - utilisé pour l'affichage notamment dans les menus
	 * @ORM\Column(name="menutitle", type="string", nullable=false, unique=false)
	 * @Annot\Translatable
	 */
	protected $menutitle;

	/**
	 * @var string
	 * Accroche, slogan de l'élément
	 * Texte de 1 ligne
	 * @ORM\Column(name="accroche", type="text", nullable=true, unique=false)
	 * @Annot\Translatable
	 */
	protected $accroche;

	/**
	 * @var string
	 * Texte de l'élément, version courte de fulltext
	 * Texte de 5 ou 6 lignes max
	 * @ORM\Column(name="resume", type="text", nullable=true, unique=false)
	 * !!!@CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=8, options={"by_reference"=false, "required"=false, "description"="Texte résumé (optionnel)"}, contexts={"simple"})
	 * !!!@CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=8, options={"by_reference"=false, "required"=false, "description"="Texte résumé (optionnel)"}, contexts={"simple"})
	 * !!!@CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $resume;

	/**
	 * @var string
	 * Texte complet de l'élément - illimité
	 * @ORM\Column(name="full_text", type="text", nullable=true, unique=false)
	 * !!!@CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=9, options={"by_reference"=false, "required"=false, "description"="Grand texte"}, contexts={"simple"})
	 * !!!@CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=9, options={"by_reference"=false, "required"=false, "description"="Grand texte"}, contexts={"simple"})
	 * !!!@CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $fulltext;

	public $testCount;

	protected $serviceBasedirectory = null;

	// protected $is_to_remove = false;


	public function __construct() {
		$this->id = null;
		$this->jstreeId = null;
		$this->journeys = new ArrayCollection();
		$this->tags = new ArrayCollection();
		$this->dirparents = new ArrayCollection();
		$this->parent = null;
		$this->rootparent = null;
		$this->owner = null;
		$this->ownerdir = null;
		$this->owners = new ArrayCollection();
		$this->linkdatas = [];
		$this->itemmanagers = new ArrayCollection();
		// $getParent->previousParentLink = null;
		$this->name = null;
		// $this->memoName();
		$this->inputname = null;
		$this->lvl = null;
		$this->static = [];
		$this->dirinterface = $this->isDirInterface();
		$this->root = null;
		$this->orphan = null;
		$this->slug = null;
		$this->setNormalizeNameMethod(); // No normalization
		// $this->ComputeRelatedParent();
		// $this->setDeepControl(false);
		// $this->initJstree_states();
		$this->driveDir = null;
		$this->systemDir = null;
		$this->pageweb = null;
		$this->pagewebChoices = null;
		$this->picone = null;
		$this->categorys = new ArrayCollection();
		$this->categorysChoices = new ArrayCollection();
		// $this->diapos = new ArrayCollection();
		$this->pathname = null;
		$this->pathslug = null;
		$this->position = 0;
		$this->currentParent = null;
		$this->currentLvl = null;
		$this->currentPathname = null;
		$this->currentPathslug = null;
		$this->testCount = [];
		$this->changePlacementMode = false;
		// $this->is_to_remove = false;
		$this->init_Groupables();
		return $this;
	}


	public function _is_repair() {
		// return $this->isDeepControlOrCheckModeContext();
		return $this->isCheckModeContext();
	}

	// public function setToRemove($is_to_remove = true) {
	// 	$this->is_to_remove = $is_to_remove;
	// 	return $this;
	// }

	// public function isToRemove() {
	// 	return $this->is_to_remove;
	// }


	/*************************************************************************************/
	/*** LINKDATAS DATA
	/*************************************************************************************/

	/**
	 * Get linkdatas
	 * @return array
	 */
	protected function getLinkdatas() {
		return $this->linkdatas ?? [];
	}

	/*public function restoreLinkdatas() {
		$this->linkdatas = $this instanceOf Basedirectory ? $this->linkdata : [];
	}*/


	/*************************************************************************************/
	/*** VALIDITY
	/*************************************************************************************/

	public function printItem($title = "Validity check", $type = 'Info') {
		// throw new Exception("Temporarly disabled!", 1);
		$this->serviceBasedirectory->printItem($this, $title, $type);
	}

	public function isValid() {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		if($this instanceOf User) $this->updateNestedData(null, false);
		if(!$this->isValidName()) return false;
		if(!$this->isValidInputname()) return false;
		if(!$this->isValidPathname()) return false;
		if(!$this->isValidPathslug()) return false;
		if(!$this->isValidRootparent()) return false;
		if(!$this->isValidOwner()) return false;
		if(!$this->isValidOwners()) return false;
		if(!$this->isValidOwnerdir()) return false;
		if(!$this->isValidParent()) return false;
		if(!$this->isValidRoot()) return false;
		if(!$this->isValidOrphan()) return false;
		if(!$this->isValidOwnerdirs()) return false;
		if(!$this->isValidRootDirs()) return false;
		return true;
	}

	/**
	 * Has root attributes (1), but maybe not complete (2)
	 * Returned value :
	 * 		0 : is not a root directory
	 * 		1 : is VALID root directory
	 * 		2 : is NOT COMPLETE root directory
	 * @return integer
	 */
	public function hasRootDirAttributes() {
		return false;
	}

	public function isNotRoot() {
		return true;
	}

	public function isCompleteRoot() {
		return false;
	}

	public function isIncompleteRoot() {
		return false;
	}

	public function isCompleteOrIncompleteRoot() {
		return false;
	}

	public function getValidationChecklist() {
		// owner
		if($this instanceOf User) {
			$owner = 'empty';
		} else {
			$owner = 'instance of OwnerTierInterface';
			if(!$this->isCompleteOrIncompleteRoot() && !empty($this->parent) && $this->parent->getOwner() instanceOf OwnerTierInterface) $owner = $this->parent->getOwner()->getShortname().' '.json_encode($this->parent->getOwner()->getName());
		}

		$pathname = 'null';
		$pathslug = 'null';
		if($this->canHaveParent()) {
			$allparents = $this->getAllParents(true);
			$parentsNames = [];
			$parentsSlugs = [];
			foreach ($allparents as $parent) {
				$parentsNames[] = $parent->getName();
				$parentsSlugs[] = $parent->getSlug();
			}
			$pathname = implode(DIRECTORY_SEPARATOR, $parentsNames);
			$pathslug = implode(DIRECTORY_SEPARATOR, $parentsSlugs);
		}

		// root
		$root = $this->canHaveParent() ? json_encode(!$this->root) : json_encode($this->root);
		// orphan
		$orphan = $this->isCompleteOrIncompleteRoot() ? json_encode($this->orphan) : 'true or false';
		// ownerdir
		$ownerdir = $this instanceOf User ? 'null' : 'instance of Item, but Basedirectory';
		// parent
		$parent = $this->canHaveParent() ? 'instance of Basedirectory' : 'null';
		// rootdirs
		$rootdirs = $this instanceOf Directory ? 'no dirs' : 'any dirs';
		// LIST
		return [
			'id' => 'Id',
			'name' => 'Name',
			'inputname' => 'Input name',
			'pathname' => 'Path name (should be '.$pathname.')',
			'pathslug' => 'Path slug (should be '.$pathslug.')',
			'owner' => 'Owner (should be '.$owner.')',
			'root' => 'Root (should be '.$root.')',
			'orphan' => 'Orphan (should be '.$orphan.')',
			'lvl' => 'Level',
			'ownerdir' => 'Owner dir (should be '.$ownerdir.')',
			'ownerdirs' => 'Owner dirs',
			'parent' => 'Parent (should be '.$parent.')',
			'rootparent' => 'Root Parent',
			'dirparents' => 'Dir Parents',
			'rootDirs' => 'Root dirs (should contain '.$rootdirs.')',
			'driveDir' => 'Drive dir',
			'systemDir' => 'System dir',
		];
	}

	public function isValidName() {
		// if($this->isToRemove()) return true;
		if(empty($this->name)) return false;
		if($this->isCompleteOrIncompleteRoot()) {
			if(!$this->hasRootPrefix()) return false;
			// $this->ownerdir = $this->ownersystemdir instanceOf Item ? $this->ownersystemdir : $this->ownerdrivedir;
			$suffix = serviceBasedirectory::ROOT_NAME_WITH_OWNERNAME ? '_'.$this->ownerdir->getName() : '';
			if(!empty($this->ownersystemdir)) return preg_match('/^@system'.$suffix.'$/', $this->name);
			if(!empty($this->ownerdrivedir)) return preg_match('/^@drive'.$suffix.'$/', $this->name);
			return false;
		}
		return !$this->hasRootPrefix();
	}

	public function isValidInputname() {
		// if($this->isToRemove()) return true;
		return !empty($this->inputname);
	}

	public function isValidRootDirs() {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		if($this instanceOf Directory) return $this->getRootDirs()->isEmpty();
		return true;
	}

	public function isValidRootparent() {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		if($this instanceOf User || $this->isCompleteOrIncompleteRoot()) return empty($this->rootparent);
		if(!($this->rootparent instanceOf Basedirectory)) return false;
		if(empty($this->parent)) return false;
		$parent = $this->parent;
		while ($parent) {
			if(empty($parent->getParent())) break;
			$parent = $parent->getParent();
		}
		if(!$parent->isCompleteOrIncompleteRoot()) return false;
		return $parent === $this->rootparent && $this->rootparent !== $this && $this->owner === $this->rootparent->getOwner();
	}

	public function isValidOwnerdirs() {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		$rootparent = $this->getRootparent(true);
		$ownerdirs = new ArrayCollection();
		// $ownerdir = null;
		if(!empty($rootparent)) {
			if($rootparent->getOwnersystemdir() instanceOf Item) $ownerdirs->add($rootparent->getOwnersystemdir());
			if($rootparent->getOwnerdrivedir() instanceOf Item) $ownerdirs->add($rootparent->getOwnerdrivedir());
			// $ownerdir = $ownerdirs->first() instanceOf Item ? $ownerdirs->first() : null;
		}
		// $ownerdirs = $this->getOwnerdirs();
		$result = $this instanceOf User ? $ownerdirs->isEmpty() : $ownerdirs->first() instanceOf Item;
		if($ownerdirs->count() > 1) $result = false;
		return $result;
	}

	public function isValidOwnerdir() {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		if($this instanceOf User) return empty($this->ownerdir);
		if(!($this->ownerdir instanceOf Item)) return false;
		if($this->ownerdir instanceOf Basedirectory) return false;
		if($this->ownerdir === $this) return false;
		$rootparent = $this->getRootparent(true);
		return $this->ownerdir === $rootparent->getOwnerdir(true);
	}

	public function isValidOwner() {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		if($this instanceOf User) return empty($this->owner);
		if($this->isCompleteOrIncompleteRoot()) return $this->owner instanceOf User;
		// return $this->owner instanceOf OwnerTierInterface && $this->owner !== $this && $this->owner === $this->parent->getOwner() && $this->rootparent->getOwner();
		return $this->owner instanceOf User && $this->owner !== $this && $this->isValidOwnerAndParent() && $this->owner === $this->rootparent->getOwner();
	}

	public function isValidOwners() {
		// if($this->isToRemove()) return true;
		// $this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		$this->checkOwners();
		if($this instanceOf User) return $this->owners->isEmpty();
		$origin = $this->owners->count();
		$owners = new ArrayCollection();
		foreach ($this->owners as $owner) {
			if($owner === $this) return false;
			if($owners->contains($owner)) return false;
			$owners->add($owner);
		}
		// if(!$this->compareOwners()) return false;
		//$this->owners = $owners;
		return $origin === $owners->count();
	}

	public function isValidParent() {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		if(!$this->canHaveParent()) return empty($this->parent);
		return $this->parent instanceOf Basedirectory && $this->parent !== $this && $this->isValidOwnerAndParent();
	}

	public function isValidOwnerAndParent($lauchException = false) {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		$result = true;
		if($this->canHaveParent()) {
			// Items & Directorys
			if(empty($this->parent)) {
				$result = false;
			} else if($this->owner instanceOf User) {
				$result = $this->owner === $this->parent->getOwner();
				// if(!$result) throw new Exception("What The Fuck!!??? 1 #".$this->getId()." owner ".$this->owner->getId()." / ".spl_object_hash($this->owner)." > parent owner ".$this->parent->getOwner()->getId()." / ".spl_object_hash($this->parent->getOwner())." !", 1);
			} else if($this->owner instanceOf Item) {
				$parent_ownerdir = $this->parent->getOwnersystemdir() instanceOf Item ? $this->parent->getOwnersystemdir() : $this->parent->getOwnerdrivedir();
				$result = $this->owner === $parent_ownerdir;
			} else {
				if($this->isCheckModeContext() && !empty($this->owner)) throw new Exception("What The Fuck!!???", 1);
				$result = false;
			}
		} else if($this->isCompleteOrIncompleteRoot()) {
			// Root dir
			if(empty($this->owner)) $result = false;
			if(!empty($this->parent)) $result = false;
			$owner = empty($this->ownerdir) || $this->ownerdir instanceOf User ? $this->ownerdir : $this->ownerdir->getOwner();
			if($this->owner !== $owner) $result = false;
		} else if($this instanceOf User) {
			// User
			$result = empty($this->parent) && empty($this->owner);
		}
		if(!$result && $lauchException) DevTerminal::contextException($this, false, "Owner (".(empty($this->owner) ? json_encode(null) : $this->owner->getShortname()." ".json_encode($this->owner->getNameOrTempname())).") and parent (".(empty($this->parent) ? json_encode(null) : $this->parent->getShortname()." ".json_encode($this->parent->getNameOrTempname()))." / Parent owner : ".(empty($this->parent) || empty($this->parent->getOwner()) ? json_encode(null) : $this->parent->getOwner()->getShortname()." ".json_encode($this->parent->getOwner()->getNameOrTempname())).") are not synchronized!", null, __LINE__, __METHOD__);
		return $result;
	}

	public function isValidRoot() {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		return $this->canHaveParent() ? !$this->root : $this->root;
	}

	public function isValidOrphan() {
		// if($this->isToRemove()) return true;
		$this->addTestCount(__METHOD__, static::TEST_COUNT_VALIDS);

		return $this->isCompleteOrIncompleteRoot() ? $this->orphan : true;
	}

	public function isValidPathname() {
		// if($this->isToRemove()) return true;
		return $this->canHaveParent() ? is_string($this->pathname) && !empty($this->pathname) : empty($this->pathname);
	}

	public function isValidPathslug($absolute = false) {
		// if($this->isToRemove()) return true;
		if(!$absolute && empty($this->getId())) return true;
		return $this->canHaveParent() ? is_string($this->pathslug) && !empty($this->pathslug) : empty($this->pathslug);
	}

	protected function addTestCount($name, $max = 100) {
		return $this;
		if($this->isDeepControl()) {
			$factor = 1;
			if($this->isCompleteOrIncompleteRoot()) $factor = 25;
			if($this->isCheckModeContext()) $factor = 50;
			$max = $max * $factor;
			// BEGIN TEST COUNT
			$this->testCount[$name] ??= 0;
			if($this->testCount[$name]++ >= $max) {
				// debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 100);
				$trace = array_reverse(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 100));
				$cpt = count($trace) - 1;
				echo('<ul>');
				foreach ($trace as $num => $trace) {
					echo('<li>'.$cpt--.'. Trace '.$num.' / line '.($trace['line'] ?? '?').' => '.($trace['class'] ?? '??class?? ').($trace['type'] ?? '??type?? ').($trace['function'] ?? '??function?? ').'</li>');
				}
				echo('</ul>');
				throw new Exception($name."(): ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." Id#".json_encode($this->getId())." count more than ".($this->testCount[$name] - 1)." iterations! PUBLIC FILTERED : ".json_encode($this->isPublicFiltered())." / MODE CONTEXT : ".json_encode($this->getModeContext()).".", 1);
				
				// DevTerminal::contextException($this, false, $name."(): count more than ".($this->testCount[$name] - 1)." iterations! PUBLIC FILTERED : ".json_encode($this->isPublicFiltered())." / MODE CONTEXT : ".json_encode($this->getModeContext()).".", null, __LINE__, __METHOD__);
			}
			// END TEST COUNT
		}
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string) $this->slug;
	}

	/**
	 * Get Id
	 * @return integer | null
	 */
	public function getId() {
		return $this->id;
	}

	public function getRegularId() {
		return $this->id;
	}



	/*************************************************************************************/
	/*** DIR INTERFACE
	/*************************************************************************************/

	/**
	 * Get dirinterface
	 * @return boolean
	 */
	public function getDirinterface() {
		return $this->dirinterface = $this instanceOf DirectoryInterface;
	}

	/**
	 * is DirectoryInterface
	 * @return boolean
	 */
	public function isDirInterface() {
		return $this->getDirinterface();
	}



	/*************************************************************************************/
	/*** PRE REMOVE
	/*************************************************************************************/

	public function removeAll(LifecycleEventArgs $args) {
		$this->removeJourneys();
		$this->removeTags();
		$this->removeParent(false);
		$this->removeDirparents(true);
		$this->owner = null;
		$this->rootparent = null;
		$this->owners = new ArrayCollection();
		$this->itemmanagers = new ArrayCollection();
		return $this;
	}

	/*************************************************************************************/
	/*** JSTREE
	/*************************************************************************************/

	public function getJstreeIcon() {
		return 'fa '.static::DEFAULT_ICON;
	}

	/**
	 * Get Id
	 * @ORM\PostPersist()
	 * @return string
	 */
	public function getJstreeId() {
		return $this->jstreeId = empty($this->parent) ? $this->getId() : $this->parent->getJstreeId().'_'.$this->getId();
		// if(null === $this->jstreeId && null !== $this->id) $this->jstreeId = preg_replace('/^\d\.(\d+)\s(\d+)$/', "$2$1", microtime()).'_'.$this->getShortname().'_'.$this->id;
		// return $this->jstreeId;
	}

	public function getJstreetype() {
		// if(!$this->isUndefined()) return $this->getShortname();
		if(!empty($this->parent)) return $this->getShortname();
		return $this->isDirInterface() ? 'Root' : 'Orphan';
	}

	public function getJstreeData() {
		$this->addTestCount(__METHOD__);

		$currentParent = $this->getCurrentParent();
		return array(
			// 'link' => [
			// 	'id' => $this->getIdLink(),
			// 	'type' => $this->getTypelink(true),
			// 	'parent_id' => $currentParent instanceOf Basedirectory ? $currentParent->getId() : null,
			// 	'position' => $this->getCurrentPosition(),
			// 	'shortname' => $this->getCurrentParentLink() instanceOf Nestlink ? $this->getCurrentParentLink()->getShortname() : null,
			// ],
			'nested_type' => $this->isDirInterface() ? 'DirectoryInterface' : 'NestedInterface',
			// 'valid_children' => $this->getValidChildren(),
			'can_have_children' => $this->isDirInterface(),
			'name' => $this->getName(),
			'instances' => $this->getInstances(),
		);
	}

	public function getJstreeState() {
		$this->addTestCount(__METHOD__);

		return $this->jstreeState = [
			'opened' => $this->getLvl() < 2,
			'disabled' => false,
			'selected' => false,
			'deletable' => $this->isDeletable(),
		];
	}

	public function getLiattr() {
		return array(
			'title' => $this->getName(),
		);
	}

	public function getAattr() {
		$attrs = ['style' => $this->isSymbolic() ? 'font-style: italic; color: #488;' : 'font-style: normal'];
		if($this->isInactive()) $attrs['class'] = 'text-muted';
		// image view
		// switch ($this->getShortname()) {
		// 	case 'Image':
		// 		$attrs = array_merge($attrs, [
		// 			'data-view-image' => "'".$this->getFileUrls()['original']."'",
		// 			'data-view-image-name' => "'".$this->getName()."'",
		// 		]);
		// 		break;
		// 	default:
		// 		# code...
		// 		break;
		// }
		return $attrs;
	}




	/*************************************************************************************/
	/*** JOURNEYS
	/*************************************************************************************/

	public function getJourneys($indexedDaymark = false) {
		$this->addTestCount(__METHOD__);

		if($indexedDaymark) {
			$journeys = new ArrayCollection();
			foreach ($this->journeys as $journey) {
				// if(isset($journeys[$journey->getDaymark()])) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): found more then only on Journey with daymark ".json_encode($journey->getDaymark())."!", 1);
				if(!isset($journeys[$journey->getDaymark()]))
					$journeys->set($journey->getDaymark(), $journey);
			}
			return $journeys;
		}
		return $this->journeys;
	}

	public function addJourney(Journey $journey, $inverse = true) {
		$this->addTestCount(__METHOD__);

		if(!$this->journeys->contains($journey)) $this->journeys->add($journey);
		if($inverse) $journey->addItem($this, false);
		return $this;
	}

	public function removeJourney(Journey $journey, $inverse = true) {
		if($inverse) $journey->removeItem($this, false);
		$this->journeys->removeElement($journey);
		return $this;
	}

	public function removeJourneys() {
		foreach ($this->journeys as $journey) {
			$this->removeJourney($journey);
		}
		return $this;
	}




	/*************************************************************************************/
	/*** TAGS
	/*************************************************************************************/

	/** 
	 * Get tags
	 * @param integer $numer = null
	 * @return ArrayCollection <Tag>
	 */
	public function getTags($number = null) {
		$this->addTestCount(__METHOD__);

		return $this->tags;
		if(!is_integer($number)) $number = 0;
		return $number !== 0 ? $this->tags->slice(0, $number) : $this->tags;
	}

	/** 
	 * Get tags with all his children
	 * @param integer $numer = null
	 * @return ArrayCollection <Tag>
	 */
	public function getAllTags($number = null) {
		$this->addTestCount(__METHOD__);

		if(!is_integer($number)) $number = 0;
		$alltags = clone $this->tags;
		if($number > 0 && $alltags->count() >= $number) return $alltags->slice(0, $number);
		foreach ($alltags as $tag) {
			foreach ($tag->getAllChilds() as $child) {
				if(!$alltags->contains($child)) $alltags->add($child);
			}
		}
		return $number !== 0 ? $alltags->slice(0, $number) : $alltags;
	}

	public function setTags($tags) {
		foreach ($this->tags as $tag) {
			if(!$tags->contains($tag)) $this->removeTag($tag, true);
		}
		foreach ($tags as $tag) $this->addTag($tag, true);
		return $this;
	}

	public function addTag(Tag $tag, $inverse = true) {
		if(!$this->tags->contains($tag)) $this->tags->add($tag);
		if($inverse) $tag->addItem($this, false);
		return $this;
	}

	public function removeTag(Tag $tag, $inverse = true) {
		if($inverse) $tag->removeItem($this, false);
		$this->tags->removeElement($tag);
		return $this;
	}

	public function removeTags() {
		foreach ($this->tags as $tag) $this->removeTag($tag, true);
		return $this;
	}



	/*************************************************************************************/
	/*** TYPELINKS
	/*************************************************************************************/

	/**
	 * Get typelink name by index
	 * @param integer $index
	 * @param string
	 */
	public static function getTypelinkNameByIndex($index) {
		return serviceBasedirectory::getTypelinks()[$index];
	}

	/**
	 * Get typelink index by name
	 * @param string $typelink
	 * @param integer
	 */
	public static function getTypelinkIndexByName($name) {
		$typelinks = serviceBasedirectory::getTypelinks();
		return array_search($name, $typelinks);
	}

	public function getParentTypelink(Basedirectory $parent, $asResult = true) {
		$this->addTestCount(__METHOD__);

		if($this->parent === $parent) return $asResult ? serviceBasedirectory::TYPELINK_HARDLINK : static::getTypelinkIndexByName(serviceBasedirectory::TYPELINK_HARDLINK);
		if($this->dirparents->contains($parent)) return $asResult ? serviceBasedirectory::TYPELINK_SYMLINK : static::getTypelinkIndexByName(serviceBasedirectory::TYPELINK_SYMLINK);
		return $asResult ? false : static::getTypelinkIndexByName(serviceBasedirectory::TYPELINK_UNDEFINED); // NO parent, NO typelink
		// return $asResult ? serviceBasedirectory::TYPELINK_UNDEFINED : static::getTypelinkIndexByName(serviceBasedirectory::TYPELINK_UNDEFINED); // NO parent, NO typelink
	}


	/*************************************************************************************/
	/*** CURRENT TYPELINK
	/*************************************************************************************/


	public function getTypelink($asText = false) {
		$this->addTestCount(__METHOD__);

		$currentParent = $this->getCurrentParent();
		if(null == $currentParent) return $asText ? serviceBasedirectory::TYPELINK_UNDEFINED : static::getTypelinkIndexByName(serviceBasedirectory::TYPELINK_UNDEFINED); // NO parent, NO typelink
		if($currentParent === $this->parent) return $asText ? serviceBasedirectory::TYPELINK_HARDLINK : static::getTypelinkIndexByName(serviceBasedirectory::TYPELINK_HARDLINK);
		if($this->hasDirparent($currentParent, false)) return $asText ? serviceBasedirectory::TYPELINK_SYMLINK : static::getTypelinkIndexByName(serviceBasedirectory::TYPELINK_SYMLINK);
		return $asText ? serviceBasedirectory::TYPELINK_PROCESSED : static::getTypelinkIndexByName(serviceBasedirectory::TYPELINK_PROCESSED);
	}

	/**
	 * Is solid (for current parent)
	 * @return boolean
	 */
	public function isSolid() {
		return $this->getTypelink() === 1;
	}

	/**
	 * Is symbolic (for current parent)
	 * @return boolean
	 */
	public function isSymbolic() {
		return $this->getTypelink() === 2;
	}

	/**
	 * Is precessed (is result of query, for current parent)
	 * @return boolean
	 */
	public function isProcessed() {
		return $this->getTypelink() === 3;
	}


	/*************************************************************************************/
	/*** BEGIN : CURRENT DIR RELATIVE DATA
	/*************************************************************************************/

	/**
	 * Get current parent
	 * @return Basedirectory | null
	 */
	public function getCurrentParent() {
		$this->addTestCount(__METHOD__);

		if($this->currentParent instanceOf Basedirectory) return $this->currentParent;
		return $this->resetCurrentParent();
	}

	/**
	 * Set current Parent
	 * @param Basedirectory $parent = null
	 * @param boolean $refresh = false
	 * @param boolean $contol = false
	 * @return Basedirectory
	 */
	public function setCurrentParent(Basedirectory $parent = null, $refresh = false, $contol = false) {
		// $this->addTestCount(__METHOD__);

		if(!($parent instanceOf Basedirectory)) $parent = $this->parent;
		if(!$refresh && $parent === $this->currentParent) return $this->currentParent;
		if($parent instanceOf Basedirectory) {
			// Set new current parent
			// if(!$parent->isValid()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this parent is not valid!", 1);
			// if($contol && !$this->hasDirparent($parent) && !$parent->isProcessed()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this entity does not have this parent!", 1);
			$this->currentParent = $parent;
		} else {
			// Has no current parent
			$this->currentParent = $this->parent;
		}
		if($this->currentParent instanceOf Basedirectory) {
			$this->position = $this->currentParent->getChildPosition($this);
			$this->currentLvl = $this->currentParent->getCurrentLvl();
		} else {
			$this->position = -1;
			$this->currentLvl = $this->lvl;
		}
		return $this->currentParent;
	}

	/**
	 * Remove current Parent
	 * @return Item
	 */
	public function resetCurrentParent() {
		$this->addTestCount(__METHOD__);

		return $this->setCurrentParent($this->parent, true);
	}

	/**
	 * Get position
	 * @return integer (-1 has no position)
	 */
	public function getPosition() {
		$this->addTestCount(__METHOD__);

		return $this->position;
	}
	public function getCurrentPosition() {
		return $this->getPosition();
	}

	/**
	 * Get position
	 * @param integer $position
	 * @param Basedirectory $parent = null
	 * @param boolean $inverse = true
	 * @return integer
	 */
	public function setPosition($position, Basedirectory $parent = null) {
		$this->addTestCount(__METHOD__);

		if(!($parent instanceOf Basedirectory)) {
			$currentParent = $this->getCurrentParent();
			$parent = $currentParent instanceOf Basedirectory ? $currentParent : $this->getParent();
		}
		return $this->position = $parent instanceOf Basedirectory ? $this->parent->setChildPosition($this, $position) : -1;
	}

	/**
	 * Get link id
	 * @return integer | null
	 */
	public function getIdLink() {
		$this->addTestCount(__METHOD__);

		$currentParent = $this->getCurrentParent();
		return $currentParent instanceOf Basedirectory ? $currentParent->getId().'_'.$this->getId() : $this->getId();
	}

	public function getCurrentLvl() {
		return $this->currentLvl;
	}


	/*************************************************************************************/
	/*** END : CURRENT DIR RELATIVE DATA
	/*************************************************************************************/


	/*************************************************************************************/
	/*** HIERARCHY
	/*************************************************************************************/


	/**
	 * Is root (has no parent)
	 * @return boolean
	 */
	public function isRoot() {
		$this->addTestCount(__METHOD__);

		return $this->root = empty($this->parent);
	}

	/**
	 * Get root
	 * @return boolean
	 */
	public function getRoot() {
		return $this->isRoot();
	}
 
	/**
	 * Has Child
	 * @param Item $child = null
	 * @return false
	 */
	public function hasChild(Item $child = null) {
		return false;
	}

	/**
	 * Get number of childs
	 * @return 0
	 */
	public function getCountChilds() {
		return 0;
	}

	// public function isRootDriveOrSystem() {
	// 	return false;
	// }



	/*************************************************************************************/
	/*** LEVEL
	/*************************************************************************************/

	/**
	 * Get level
	 * @return integer
	 */
	public function getLvl() {
		return $this->lvl;
	}


	/*************************************************************************************/
	/*** CHANGED FIELDS
	/*************************************************************************************/

	protected function getChangeFields() {
		return ['parent','owner'];
		// return ['lvl','pathname','pathslug','root','orphan','rootparent','parent','owner'];
	}

	protected function saveChanges($name) {
		$this->addTestCount(__METHOD__);

		$fields = $this->getChangeFields();
		$this->changes[$name] = [];
		foreach ($fields as $field) {
			$method = Inflector::camelize('get_'.$field);
			if(!method_exists($this, $method)) $method = Inflector::camelize('is_'.$field);
			if(method_exists($this, $method)) {
				$this->changes[$name][$field] = $this->$method();
			}
		}
	}

	protected function updateNestedDataIfchanges($name) {
		$this->addTestCount(__METHOD__);

		$fields = $this->getChangeFields();
		if(!isset($this->changes[$name]) || empty($this->changes[$name])) return [];
		$changed = [];
		foreach ($fields as $field) {
			$method = Inflector::camelize('get_'.$field);
			if(!method_exists($this, $method)) $method = Inflector::camelize('is_'.$field);
			if(method_exists($this, $method)) {
				if($this->$method() !== $this->changes[$name][$field]) $changed[$field] = [
					'old' => $this->changes[$name][$field],
					'new' => $this->$method()
				];
			}
		}
		if(count($changed) > 0) $this->updateNestedData();
		return count($changed) > 0;
	}


	/*************************************************************************************/
	/*** RESOLVES & UPDATE NESTED DATA
	/*************************************************************************************/

	public function tryResolveOwner() {
		$this->addTestCount(__METHOD__, 20);

		if($this->isValidOwnerAndParent(false)) return true;
		// User
		if($this instanceOf User) {
			if(!empty($this->owner)) $this->changeOwner(null);
			if(!empty($this->parent)) {
				$this->parent->removeChild($this);
				$this->_bd_removeParent(true);
			}
		} else {
			if($this->isCompleteOrIncompleteRoot()) {
				// ROOT directory
				if(!$this->isValidOwnerdir()) $this->tryResolveOwnerdir();
				$this->getOwnerdir(true);
				if($this->ownerdir instanceOf User) {
					$this->changeOwner($this->ownerdir);
				} else {
					if(!($this->ownerdir->getOwner() instanceOf OwnerTierInterface)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." ownerdir owner is not OwnerTierInterface!", 1);
					$this->changeOwner($this->ownerdir->getOwner());
				}
			} else {
				// All other Items
				if(empty($this->parent)) $this->tryResolveParent();
				if($this->owner !== $this->rootparent->getOwner()) {
					$this->changeOwner($this->rootparent->getOwner());
					$this->checkOwners();
				}
			}
		}
		$this->isValidOwnerAndParent(true);
		return $this->isValidOwner();
	}

	public function tryResolveOwnerdir() {
		$this->addTestCount(__METHOD__, 20);

		if(!empty($this->parent) || $this->tryResolveParent()) {
			$this->parent->tryResolveOwnerdir();
		}
		return $this->isValidOwnerdir();
	}

	public function tryResolveParent() {
		$this->addTestCount(__METHOD__, 20);

		if(!$this->canHaveParent()) {
			if($this instanceOf User && !empty($this->owner)) $this->changeOwner(null);
			if(!empty($this->parent)) $this->parent->removeChild($this);
			if(!empty($this->parent)) $this->_bd_removeParent(true);
			// return $this->isValidParent();
		} else {
			if(empty($this->owner)) DevTerminal::contextException($this, false, "Owner for ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." is empty, so could not resolve parent!", null, __LINE__, __METHOD__);
			if(!$this->isValidOwnerAndParent(false)) {
				$parent = $this->serviceBasedirectory->getLogicParent($this, true);
				// echo('<div class="text-warning">'.__METHOD__.'(): parent found '.(null === $parent ? 'NULL' : $parent->getShortname().' '.json_encode($parent->getName())).' for '.$this->getShortname().' '.json_encode($this->getName()).'</div>');
				$this->setParent($parent, -1, false);
				// echo('<div class="text-warning">'.__METHOD__.'(): parent decided '.(null === $this->parent ? 'NULL' : $this->parent->getShortname().' '.json_encode($this->parent->getName())).' for '.$this->getShortname().' '.json_encode($this->getName()).'</div>');
			}
		}
		return $this->isValidOwnerAndParent(false);
		// return $this->isValidParent();
	}

	public function getNestedType() {
		if($this instanceOf Tempfile) return static::TYPE_TEMPFILE;
		if($this->isCompleteOrIncompleteRoot()) return static::TYPE_ROOT;
		if($this instanceOf Menu) return static::TYPE_MENU;
		if($this instanceOf Basedirectory) return static::TYPE_DIRECTORY;
		if($this instanceOf User) return static::TYPE_USER;
		if($this instanceOf OwnerTierInterface) return static::TYPE_OTI;
		if($this instanceOf Entreprise) return static::TYPE_ENTREPRISE;
		if($this instanceOf Tier) return static::TYPE_TIER;
		return static::TYPE_ITEM;
	}

	public function changePlacement(Item $newPlacement, $position = null, $keepOldParentAsSymlink = true) {
		$this->addTestCount(__METHOD__, 20);
		if($this->changePlacementMode) return $this;
		$this->changePlacementMode = true;

		// if(empty($newPlacement)) $newPlacement = $this->owner;
		$position ??= static::DEFAULT_POSITION;
		if($newPlacement->getNestedType() === static::TYPE_TEMPFILE) throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." can not be used for new placement!", 1);
		if($this->getNestedType() === static::TYPE_TEMPFILE) throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$this->getShortname()." ".json_encode($this->getName())." can not be used for new placement!", 1);

		// Check replacement Item
		if($newPlacement->isCheckModeContext() && !$newPlacement->isValid()) $newPlacement->checkAndRepairValidity("Failed to check and repair Item used for new replacement ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname()).".");

		if(empty($newPlacement->getId())) {
			if(!$newPlacement->isValidOwnerAndParent(false)) throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$this->getShortname()." ".json_encode($this->getName())." new placement ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." is not valid (owner <-> parent)!", 1);
		} else {
			if(!$newPlacement->isValid()) throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$this->getShortname()." ".json_encode($this->getName())." new placement ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." is not valid!", 1);
		}
		
		switch ($this->getNestedType()) {
			case static::TYPE_ROOT:
				switch ($newPlacement->getNestedType()) {
					case static::TYPE_ROOT:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						break;
					case static::TYPE_MENU:
					case static::TYPE_DIRECTORY:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						break;
					case static::TYPE_USER:
					// case static::TYPE_OTI:
					case static::TYPE_ENTREPRISE:
					case static::TYPE_TIER:
					case static::TYPE_ITEM:
						if(preg_match('/^@system/', $this->getName())) {
							$this->setOwnersystemdir(reset($newPlacement), true, true);
						} else if(preg_match('/^@drive/', $this->getName())) {
							$this->setOwnerdrivedir(reset($newPlacement), true, true);
						}
						break;
					default:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." nested type ".json_encode($newPlacement->getNestedType())." is not valid!", 1);
						break;
				}
				break;
			case static::TYPE_MENU:
				switch ($newPlacement->getNestedType()) {
					case static::TYPE_ROOT:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$this->getShortname()." ".json_encode($this->getName())." can NOT have system of directorys ".(empty($newPlacement) ? 'NULL' : $newPlacement->getShortname()." ".json_encode($newPlacement->getName()))." is not valid!", 1);
						break;
					case static::TYPE_MENU:
					case static::TYPE_DIRECTORY:
						$this->changeOwner($newPlacement->getOwner());
						$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $newPlacement->getOwnerdir(), true);
						if($parent instanceOf Basedirectory) {
							$namedChilds = $parent->getNamedChilds($this->getName(), true);
							if($namedChilds->count()) {
								// $parent->addChild($this, serviceBasedirectory::TYPELINK_SYMLINK, $position, $keepOldParentAsSymlink);
								throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$parent->getShortname()." ".json_encode($parent->getNameOrTempname())." because parent has allready this named item !", 1);
							} else {
								$this->setParent($parent, $position, $keepOldParentAsSymlink);
							}
						} else {
							throw new Exception("Error line ".__LINE__." ".__METHOD__."() could not find (nore create) parent directory for ".$this->getShortname()." ".json_encode($this->getName())." with ".(empty($newPlacement) ? 'NULL' : $newPlacement->getShortname()." ".json_encode($newPlacement->getName()))." is not valid!", 1);
						}
						$this->setParent($newPlacement, $position, $keepOldParentAsSymlink);
						break;
					case static::TYPE_USER:
						$this->changeOwner($newPlacement);
						$mem_owner = $this->owner;
						$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $this->owner, true);
						if($parent instanceOf Basedirectory) $this->setParent($parent, $position, $keepOldParentAsSymlink);
						if($mem_owner !== $this->owner) throw new Exception("Error line ".__LINE__." ".__METHOD__."() after setParent(), changed owner ".(empty($mem_owner) ? 'NULL' : $mem_owner->getShortname()." ".json_encode($mem_owner->getName()))." to ".(empty($this->owner) ? 'NULL' : $this->owner->getShortname()." ".json_encode($this->owner->getName()))."!", 1);
						if($this->owner !== $newPlacement) {
							$dirparent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $newPlacement, true);
							if($dirparent instanceOf Basedirectory) $this->addDirparent($dirparent, $position);
						}
						break;
					// case static::TYPE_OTI:
					// 	# code...
					// 	break;
					case static::TYPE_ENTREPRISE:
						# code...
						break;
					case static::TYPE_TIER:
						# code...
						break;
					case static::TYPE_ITEM:
						# code...
						break;
					default:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." nested type ".json_encode($newPlacement->getNestedType())." is not valid!", 1);
						break;
				}
				break;
			case static::TYPE_DIRECTORY:
				switch ($newPlacement->getNestedType()) {
					case static::TYPE_ROOT:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$this->getShortname()." ".json_encode($this->getName())." can NOT have system of directorys ".(empty($newPlacement) ? 'NULL' : $newPlacement->getShortname()." ".json_encode($newPlacement->getName()))." is not valid!", 1);
						break;
					case static::TYPE_MENU:
						$this->changeOwner($newPlacement->getOwner());
						$this->setParent($newPlacement, $position, $keepOldParentAsSymlink);
						break;
					case static::TYPE_DIRECTORY:
						$this->changeOwner($newPlacement->getOwner());
						$this->setParent($newPlacement, $position, $keepOldParentAsSymlink);
						break;
					case static::TYPE_USER:
						$this->changeOwner($newPlacement);
						$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $this->owner, true);
						if($parent instanceOf Basedirectory && $this->parent !== $parent) $this->setParent($parent, $position, $keepOldParentAsSymlink);
						if($mem_owner !== $this->owner) throw new Exception("Error line ".__LINE__." ".__METHOD__."() after setParent(), changed owner ".(empty($mem_owner) ? 'NULL' : $mem_owner->getShortname()." ".json_encode($mem_owner->getName()))." to ".(empty($this->owner) ? 'NULL' : $this->owner->getShortname()." ".json_encode($this->owner->getName()))."!", 1);
						break;
					// case static::TYPE_OTI:
					// 	throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
					// 	break;
					case static::TYPE_ENTREPRISE:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						break;
					case static::TYPE_TIER:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						break;
					case static::TYPE_ITEM:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						break;
					default:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." nested type ".json_encode($newPlacement->getNestedType())." is not valid!", 1);
						break;
				}
				break;
			case static::TYPE_USER:
				switch ($newPlacement->getNestedType()) {
					case static::TYPE_ROOT:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						// if(preg_match('/^@system/', $this->getName())) {
						// 	$newPlacement->setOwnersystemdir(reset($this), true, true);
						// } else if(preg_match('/^@drive/', $this->getName())) {
						// 	$newPlacement->setOwnerdrivedir(reset($this), true, true);
						// }
						break;
					case static::TYPE_MENU:
					case static::TYPE_DIRECTORY:
					case static::TYPE_USER:
					// case static::TYPE_OTI:
					case static::TYPE_ENTREPRISE:
					case static::TYPE_TIER:
					case static::TYPE_ITEM:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						break;
					default:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." nested type ".json_encode($newPlacement->getNestedType())." is not valid!", 1);
						break;
				}
				break;
			// case static::TYPE_OTI:
			// 	switch ($newPlacement->getNestedType()) {
			// 		case static::TYPE_ROOT:
			// 			throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
			// 			break;
			// 		case static::TYPE_MENU:
			// 			# code...
			// 			break;
			// 		case static::TYPE_DIRECTORY:
			// 			# code...
			// 			break;
			// 		case static::TYPE_USER:
			// 			# code...
			// 			break;
			// 		case static::TYPE_OTI:
			// 			# code...
			// 			break;
			// 		case static::TYPE_ENTREPRISE:
			// 			# code...
			// 			break;
			// 		case static::TYPE_TIER:
			// 			# code...
			// 			break;
			// 		case static::TYPE_ITEM:
			// 			# code...
			// 			break;
			// 		default:
			// 			throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." nested type ".json_encode($newPlacement->getNestedType())." is not valid!", 1);
			// 			break;
			// 	}
			// 	break;
			case static::TYPE_ENTREPRISE:
				switch ($newPlacement->getNestedType()) {
					case static::TYPE_ROOT:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						break;
					case static::TYPE_MENU:
						# code...
						break;
					case static::TYPE_DIRECTORY:
						# code...
						break;
					case static::TYPE_USER:
						$this->changeOwner($newPlacement);
						$mem_owner = $this->owner;
						$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $this->owner, true);
						if($parent instanceOf Basedirectory && $this->parent !== $parent) $this->setParent($parent, $position, $keepOldParentAsSymlink);
						if($mem_owner !== $this->owner) throw new Exception("Error line ".__LINE__." ".__METHOD__."() after setParent(), changed owner ".(empty($mem_owner) ? 'NULL' : $mem_owner->getShortname()." ".json_encode($mem_owner->getName()))." to ".(empty($this->owner) ? 'NULL' : $this->owner->getShortname()." ".json_encode($this->owner->getName()))."!", 1);
						$dirparent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $newPlacement, true);
						if($dirparent instanceOf Basedirectory && !$this->hasParent($dirparent)) $this->setParent($dirparent, $position);
						break;
					// case static::TYPE_OTI:
					// 	# code...
					// 	break;
					case static::TYPE_ENTREPRISE:
						# code...
						break;
					case static::TYPE_TIER:
						# code...
						break;
					case static::TYPE_ITEM:
						# code...
						break;
					default:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." nested type ".json_encode($newPlacement->getNestedType())." is not valid!", 1);
						break;
				}
				break;
			case static::TYPE_TIER:
				switch ($newPlacement->getNestedType()) {
					case static::TYPE_ROOT:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						break;
					case static::TYPE_MENU:
						# code...
						break;
					case static::TYPE_DIRECTORY:
						# code...
						break;
					case static::TYPE_USER:
						$this->changeOwner($newPlacement);
						$mem_owner = $this->owner;
						$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $this->owner, true);
						if($parent instanceOf Basedirectory && $this->parent !== $parent) $this->setParent($parent, $position, $keepOldParentAsSymlink);
						if($mem_owner !== $this->owner) throw new Exception("Error line ".__LINE__." ".__METHOD__."() after setParent(), changed owner ".(empty($mem_owner) ? 'NULL' : $mem_owner->getShortname()." ".json_encode($mem_owner->getName()))." to ".(empty($this->owner) ? 'NULL' : $this->owner->getShortname()." ".json_encode($this->owner->getName()))."!", 1);
						$dirparent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $newPlacement, true);
						if($dirparent instanceOf Basedirectory && !$this->hasParent($dirparent)) $this->setParent($dirparent, $position);
						break;
					// case static::TYPE_OTI:
					// 	# code...
					// 	break;
					case static::TYPE_ENTREPRISE:
						# code...
						break;
					case static::TYPE_TIER:
						# code...
						break;
					case static::TYPE_ITEM:
						# code...
						break;
					default:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." nested type ".json_encode($newPlacement->getNestedType())." is not valid!", 1);
						break;
				}
				break;
			case static::TYPE_ITEM:
				switch ($newPlacement->getNestedType()) {
					case static::TYPE_ROOT:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() can NOT SET ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." INTO ".$newPlacement->getShortname()." ".json_encode($newPlacement->getNameOrTempname())." !", 1);
						break;
					case static::TYPE_MENU:
						$this->changeOwner($newPlacement->getOwner());
						$mem_owner = $this->owner;
						$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $this->owner, true);
						if($parent instanceOf Basedirectory && $this->parent !== $parent) $this->setParent($parent, $position, $keepOldParentAsSymlink);
						if($mem_owner !== $this->owner) throw new Exception("Error line ".__LINE__." ".__METHOD__."() after setParent(), changed owner ".(empty($mem_owner) ? 'NULL' : $mem_owner->getShortname()." ".json_encode($mem_owner->getName()))." to ".(empty($this->owner) ? 'NULL' : $this->owner->getShortname()." ".json_encode($this->owner->getName()))."!", 1);
						$dirparent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $newPlacement, true);
						if($dirparent instanceOf Basedirectory && !$this->hasDirparent($dirparent)) $this->addDirparent($dirparent, $position);
						break;
					case static::TYPE_DIRECTORY:
						$this->changeOwner($newPlacement->getOwner());
						$mem_owner = $this->owner;
						$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $this->owner, true);
						if($parent instanceOf Basedirectory && $this->parent !== $parent) $this->setParent($parent, $position, $keepOldParentAsSymlink);
						if($mem_owner !== $this->owner) throw new Exception("Error line ".__LINE__." ".__METHOD__."() after setParent(), changed owner ".(empty($mem_owner) ? 'NULL' : $mem_owner->getShortname()." ".json_encode($mem_owner->getName()))." to ".(empty($this->owner) ? 'NULL' : $this->owner->getShortname()." ".json_encode($this->owner->getName()))."!", 1);
						$dirparent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $newPlacement, true);
						if($dirparent instanceOf Basedirectory && !$this->hasDirparent($dirparent)) $this->addDirparent($dirparent, $position);
						break;
					case static::TYPE_USER:
						$this->changeOwner($newPlacement);
						$mem_owner = $this->owner;
						$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $this->owner, true);
						if($parent instanceOf Basedirectory && $this->parent !== $parent) $this->setParent($parent, $position, $keepOldParentAsSymlink);
						if($mem_owner !== $this->owner) throw new Exception("Error line ".__LINE__." ".__METHOD__."() after setParent(), changed owner ".(empty($mem_owner) ? 'NULL' : $mem_owner->getShortname()." ".json_encode($mem_owner->getName()))." to ".(empty($this->owner) ? 'NULL' : $this->owner->getShortname()." ".json_encode($this->owner->getName()))."!", 1);
						// echo('<div>Owner: '.(empty($this->owner) ? 'NULL' : $this->owner->getShortname().' '.json_encode($this->owner->getNameOrTempname())).'</div>');
						// echo('<div>Environment: '.(empty($this->contextEnvironment) ? 'NULL' : $this->contextEnvironment->getShortname().' '.json_encode($this->contextEnvironment->getNameOrTempname())).'</div>');
						if($this->contextEnvironment instanceOf Tier && $this->contextEnvironment !== $this->owner) {
							$dirparent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $this->contextEnvironment, true);
							$added = false;
							if($dirparent instanceOf Basedirectory && !$this->hasDirparent($dirparent)) $added = $this->addDirparent($dirparent, $position);
							// echo('<div>Dir parent (added: '.json_encode($added).'): '.(empty($dirparent) ? 'NULL' : $dirparent->getShortname().' '.json_encode($dirparent->getNameOrTempname())).'</div>');
						}
						break;
					// case static::TYPE_OTI:
					case static::TYPE_ENTREPRISE:
					case static::TYPE_TIER:
					case static::TYPE_ITEM:
						$this->changeOwner($newPlacement->getOwner());
						$mem_owner = $this->owner;
						$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $this->owner, true);
						if($parent instanceOf Basedirectory && $this->parent !== $parent) $this->setParent($parent, $position, $keepOldParentAsSymlink);
						if($mem_owner !== $this->owner) throw new Exception("Error line ".__LINE__." ".__METHOD__."() after setParent(), changed owner ".(empty($mem_owner) ? 'NULL' : $mem_owner->getShortname()." ".json_encode($mem_owner->getName()))." to ".(empty($this->owner) ? 'NULL' : $this->owner->getShortname()." ".json_encode($this->owner->getName()))."!", 1);
						$dirparent = $this->serviceBasedirectory->getDefaultOwnerDirectory($this, $newPlacement, true);
						if($dirparent instanceOf Basedirectory && !$this->hasDirparent($dirparent)) $this->addDirparent($dirparent, $position);
						break;
					default:
						throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$newPlacement->getShortname()." ".json_encode($newPlacement->getName())." nested type ".json_encode($newPlacement->getNestedType())." is not valid!", 1);
						break;
				}
				break;
			default:
				throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$this->getShortname()." ".json_encode($this->getName())." nested type ".json_encode($this->getNestedType())." is not valid!", 1);
				break;
		}
		$this->updateNestedData(null, true);
		$this->isValidOwnerAndParent(true);
		$this->changePlacementMode = false;
		return $this;
	}

	public function getLogicPlacement(Item $item, $createIfNotFound = true) {
		if(!$this->canHaveParent()) return null;
	}

	/**
	 * Update all paths
	 * BEWARE: to take car of PATHSLUG, ONLY USE IT IN ONFLUSH CONTEXT, BECAUSE slugs are updated juste before OnFlush!!!
	 * @param EventArgs $args = null
	 * @param mixed $updateChilds = true (boolean or array|ArrayCollection|Entity of <Item>)
	 * @return boolean (true if changed)
	 */
	public function updateNestedData(EventArgs $args = null, $updateChilds = true) {
		$this->addTestCount(__METHOD__, 100);

		if($args instanceOf EventArgs) {
			// OnFlush/PostFlush : updates only pathslug and pathname
			$this->updatePaths();
			if(!$updateChilds) return $this;
		} else {
			if(empty($this->owner) && empty($this->parent) && $this->canHaveParent()) return $this;
			// Check parent's data
			if(!empty($this->parent)) {
				if($this->owner !== $this->parent->getOwner()) $this->changeOwner($this->parent->getOwner());
				if($this->rootparent !== $this->parent->getRootparent(true)) $this->rootparent = $this->parent->getRootparent(true);
			}
			// Owner and parent must be valid and synchronized!
			$this->isValidOwnerAndParent(true);
			if($this->owner instanceOf Item && !($this->owner instanceOf OwnerTierInterface)) {
				$this->changeOwner($this->owner->getOwner());
			}			
			if($this->canHaveParent()) {
				// Has parent
				$this->pathname = $this->parent->getPathname(true);
				if(!empty($this->getId())) $this->pathslug = $this->parent->getPathslug(true);
				$this->rootparent = $this->parent->getRootparent(true);
				$this->root = empty($this->parent);
				$this->lvl = $this->parent->getLvl() + 1;
				$this->orphan = empty($this->parent) && $this->dirparents->isEmpty();
				// ***** FULL CONTROL *****
				if($this->isDeepControlOrCheckModeContext()) {
					$allparents = $this->getAllParents(true);
					$parentsNames = [];
					foreach ($allparents as $parent) $parentsNames[] = $parent->getName();
					if($this->pathname !== implode(DIRECTORY_SEPARATOR, $parentsNames)) {
						if($this->isCheckModeContext()) {
							$this->pathname = implode(DIRECTORY_SEPARATOR, $parentsNames);
						} else {
							DevTerminal::contextException($this, false, "Pathname ".json_encode($this->pathname)." is wrong! Should be ".json_encode(implode(DIRECTORY_SEPARATOR, $parentsNames)).".", null, __LINE__, __METHOD__);
						}
					}
					if(!empty($this->getId())) {
						// Do not test if not persisted parent in database
						$test = true;
						foreach ($allparents as $parent) {
							if(empty($parent->getId())) { $test = false; break; }
							$parentsSlugs[] = $parent->getSlug();
						}
						if($test && $this->pathslug !== implode(DIRECTORY_SEPARATOR, $parentsSlugs)) {
							if($this->isCheckModeContext()) {
								$this->pathslug = implode(DIRECTORY_SEPARATOR, $parentsSlugs);
							} else {
								DevTerminal::contextException($this, false, "Pathslug ".json_encode($this->pathslug)." is wrong! Should be ".json_encode(implode(DIRECTORY_SEPARATOR, $parentsSlugs)).".", null, __LINE__, __METHOD__);
							}
						}
					}
					if($this->rootparent !== $allparents->first()) {
						if($this->_is_repair())
							$this->rootparent = $allparents->first();
							else
							DevTerminal::contextException($this, false, "Rootparent ".(empty($this->rootparent) ? json_encode(null) : $this->rootparent->getShortname()." ".json_encode($this->rootparent->getNameOrTempname()))." is wrong! Should be ".(empty($allparents->first()) ? json_encode(null) : $allparents->first()->getShortname()." ".json_encode($allparents->first()->getNameOrTempname())).".", null, __LINE__, __METHOD__);
					}
					if($this->lvl !== $allparents->count()) {
						if($this->_is_repair())
							$this->lvl = $allparents->count();
							else
							DevTerminal::contextException($this, false, "Level ".json_encode($this->lvl)." is wrong! Should be ".json_encode($allparents->count()).".", null, __LINE__, __METHOD__);
					}
				}
			} else {
				// can not have parent
				$this->pathslug = "";
				$this->pathname = "";
				$this->rootparent = null;
				$this->root = empty($this->parent);
				$this->lvl = 0;
				$this->orphan = empty($this->parent) && $this->dirparents->isEmpty();
				if($this->isCompleteOrIncompleteRoot()) $this->refreshName();
			}
			// OWNERS
		}
		$this->checkOwners();
		// Update childs
		if($this instanceOf Basedirectory) {
			if($updateChilds instanceOf Item) $updateChilds = new ArrayCollection([$updateChilds]);
			if(is_array($updateChilds)) $updateChilds = new ArrayCollection($updateChilds);
			if(is_bool($updateChilds)) {
				$updateChilds = true === $updateChilds ? $this->getSolidChilds(false) : new ArrayCollection();
			}
			if(!$updateChilds->isEmpty()) {
				foreach ($updateChilds as $child) if($this->dirchilds->contains($child)) {
					// if($child->getOwner() !== $this->owner) $child->setOwner($this->owner);
					$child->updateNestedData($args);
				}
			}
		}
		return $this;
	}


	/*************************************************************************************/
	/*** COMPUTE ATTACHEMENT BETWEEN PARENT & CHILD
	/*************************************************************************************/

	public function checkAndRepairValidity($errorMessage = null, $completeCheck = true) {
		$this->addTestCount(__METHOD__, 20);

		if($this->getConstraintViolationList()->count() === 0) return true;

		// if(!$this->tryResolveOwner()) $this->validityEntityReport("ERRORS WHILE TRY RESOLVE OWNER.", __LINE__, __METHOD__, true);
		// if($this->canHaveParent() && !$this->tryResolveParent()) $this->validityEntityReport("ERRORS WHILE TRY RESOLVE PARENT.", __LINE__, __METHOD__, true);
		// if($this->isCompleteOrIncompleteRoot() && !$this->tryResolveOwnerdir()) $this->validityEntityReport("ERRORS WHILE TRY RESOLVE OWNERDIR.", __LINE__, __METHOD__, true);

		$this->tryResolveOwner();
		$this->tryResolveParent();
		$this->tryResolveOwnerdir();
		$this->checkOwners();
		$this->updateNestedData(null, true);

		$this->validityEntityReport($errorMessage ?? "ERRORS WHILE CHECK AND REPAIR", __LINE__, __METHOD__, true, $completeCheck);
		return $this->getConstraintViolationList()->count() === 0;
	}

	/**
	 * Compute relation between parent and child
	 * $askedTypelink =
	 * 		- null (auto)
	 * 		- string TYPELINK_HARDLINK
	 * 		- string TYPELINK_SYMLINK
	 * 		- string TYPELINK_REMOVE --> for remove from parent
	 * @param Item $item
	 * @param mixed $askedTypelink = null
	 * @param integer $position = -1
	 * @param boolean $keepOldParentAsSymlink = false
	 * @return Item
	 */
	public function computeDirectoryChildRelation(Item $child, $askedTypelink = null, $position = -1, $keepOldParentAsSymlink = false) {
		$this->addTestCount(__METHOD__);

		if(!($this->serviceBasedirectory instanceOf serviceBasedirectory)) return $this;
		$parent = $this->serviceBasedirectory->getDefaultOwnerDirectory($child, $this, true);
		if($parent instanceOf Basedirectory) return $parent->computeDirectoryChildRelation($child, $askedTypelink, $position, $keepOldParentAsSymlink);
		if(!($child instanceOf User)) DevTerminal::contextException($this, false, "Could not find any parent for child ".$child->getShortname()." ".json_encode($child->getName())."!", null, __LINE__, __METHOD__);
		return $this;
	}


	/*************************************************************************************/
	/*** PARENT
	/*************************************************************************************/

	/**
	 * Get solid root parent Directory
	 * @return Directory | null
	 */
	public function getRootparent($orSelfIfIsRootParent = false) {
		// $this->addTestCount(__METHOD__, 100);

		return $this->isCompleteOrIncompleteRoot() && $orSelfIfIsRootParent ? $this : $this->rootparent;
	}

	public function getOwnerdirs() {
		$this->addTestCount(__METHOD__, 30);

		if($this->isCompleteOrIncompleteRoot()) {
			$od = new ArrayCollection();
			if($this->ownersystemdir instanceOf Item) $od->add($this->ownersystemdir);
			if($this->ownerdrivedir instanceOf Item) $od->add($this->ownerdrivedir);
			return $od;
		}
		if($this->rootparent instanceOf Basedirectory && !$this->rootparent->isCompleteOrIncompleteRoot()) throw new Exception("Error line ".__LINE__." ".__METHOD__."() ".$this->getShortname()." ".json_encode($this->getNameOrTempname())." has invalid rootparent ".$this->rootparent->getShortname()." ".json_encode($this->rootparent->getNameOrTempname())."!", 1);
		return empty($this->rootparent) ? new ArrayCollection() : $this->rootparent->getOwnerdirs();
	}

	/**
	 * Get hardlink parent Directory
	 * @return Directory | null
	 */
	public function getParent() {
		return $this->parent;
	}

	/**
	 * Set hardlink parent Directory
	 * @param Basedirectory $parent
	 * @param integer $position = -1
	 * @return boolean
	 */
	public function setParent(Basedirectory $parent, $position = -1, $keepOldParentAsSymlink = false) {
		$this->addTestCount(__METHOD__, 8);

		if($this->isCompleteOrIncompleteRoot()) return false;
		$old_parent = $this->getParent();
		$parent->computeDirectoryChildRelation($this, serviceBasedirectory::TYPELINK_HARDLINK, $position, $keepOldParentAsSymlink);
		$test = empty($old_parent) || ($keepOldParentAsSymlink && $this->hasDirparent($old_parent)) || (!$keepOldParentAsSymlink && !$this->hasDirparent($old_parent));
		return $this->hasParent($parent) && $test;
	}

	/**
	 * Set hardlink parent Directory --> DIRECT FROM BASEDIRECTORY
	 * @param Basedirectory $parent
	 * @param boolean $keepOldParentAsSymlink = false
	 * @return boolean
	 */
	public function _bd_setParent(Basedirectory $parent, $keepOldParentAsSymlink = false) {
		$this->addTestCount(__METHOD__, 8);

		if(!$this->hasParent($parent)) {
			if(!empty($this->parent)) $this->_bd_removeParent($keepOldParentAsSymlink);
			$this->parent = $parent;
			$this->_bd_addDirparent($this->parent);
		}
		// if($this->owner !== $this->parent->getOwner()) $this->setOwner($this->parent->getOwner());
		return $this->parent === $parent;
	}

	/**
	 * Has hardlink parent
	 * @param Basedirectory $parent = null
	 * @return boolean
	 */
	public function hasParent(Basedirectory $parent = null) {
		if($parent instanceOf Basedirectory) return $this->parent === $parent;
		return $this->parent instanceOf Basedirectory;
	}

	/**
	 * Remove parent Directory
	 * @param boolean $keepOldParentAsSymlink = false
	 * @return Item
	 */
	public function removeParent($keepOldParentAsSymlink = false) {
		$this->addTestCount(__METHOD__, 8);

		if($this->parent instanceOf Basedirectory) $this->parent->computeDirectoryChildRelation($this, serviceBasedirectory::TYPELINK_REMOVE, -1, $keepOldParentAsSymlink);
		return empty($this->parent);
	}

	/**
	 * Remove parent Directory --> DIRECT FROM BASEDIRECTORY
	 * @param boolean $keepOldParentAsSymlink = false
	 * @return Item
	 */
	public function _bd_removeParent($keepOldParentAsSymlink = false) {
		$this->addTestCount(__METHOD__);

		if(!empty($this->parent)) {
			if(!$keepOldParentAsSymlink) $this->dirparents->removeElement($this->parent);
			$this->parent = null;
		}
		if($this->isCompleteOrIncompleteRoot()) {
			$this->getOwnerdir(true);
			$this->changeOwner($this->ownerdir->getOwner());
		} else {
			$this->changeOwner(null);
		}
		return !$this->hasParent();
	}

	/**
	 * Can this have a parent
	 * @return boolean
	 */
	public function canHaveParent() {
		if($this instanceOf User || $this->isCompleteOrIncompleteRoot()) return false;
		return true;
	}


	/*************************************************************************************/
	/*** DIR PARENTS
	/*************************************************************************************/

	/**
	 * Get all parent Basedirectorys
	 * @return ArrayCollection <Basedirectory>
	 */
	public function getDirparents() {
		return $this->dirparents;
	}

	/**
	 * Get all parent Directorys of hierarchy
	 * @return ArrayCollection <Basedirectory>
	 */
	protected function getAllDirparents() {
		$this->addTestCount(__METHOD__);

		$alldirparents = [];
		foreach ($this->getDirparents() as $dirparent) {
			$alldirparents = array_unique(array_merge($alldirparents, $dirparent->getAllDirparents()->toArray()));
		}
		return new ArrayCollection($alldirparents);
	}

	/**
	 * Add parent Directory
	 * @param Basedirectory $dirparent
	 * @param integer position = -1
	 * @return string (typelink)
	 */
	public function addDirparent(Basedirectory $dirparent, $position = -1) {
		$this->addTestCount(__METHOD__);

		if($this->isCompleteOrIncompleteRoot()) return false;
		if(empty($this->parent) && $this->canHaveParent()) return $this->setParent($dirparent);
		// $dirparent->addChild($this, serviceBasedirectory::TYPELINK_SYMLINK, $position);
		$dirparent->computeDirectoryChildRelation($this, serviceBasedirectory::TYPELINK_SYMLINK, $position);
		return $this->hasDirparent($dirparent);
	}

	/**
	 * Add parent Directory --> DIRECT FROM BASEDIRECTORY
	 * @param Basedirectory $dirparent
	 * @return Item
	 */
	public function _bd_addDirparent(Basedirectory $dirparent) {
		$this->addTestCount(__METHOD__);

		if(!$this->dirparents->contains($dirparent)) $this->dirparents->add($dirparent);
		return $this;
	}

	/**
	 * Remove parent Directory
	 * @param Basedirectory $dirparent
	 * @param boolean $force = false
	 * @return boolean (true if $dirparent has been removed)
	 */
	public function removeDirparent(Basedirectory $dirparent, $force = false) {
		$this->addTestCount(__METHOD__);

		if($this->parent !== $dirparent || !$this->canHaveParent() || $force) {
			$dirparent->computeDirectoryChildRelation($this, serviceBasedirectory::TYPELINK_REMOVE);
		}
		return !$this->dirparents->contains($dirparent);
	}

	/**
	 * Remove parent Directorys
	 * @param boolean $force = false
	 * @return boolean (true if $dirparent has been removed)
	 */
	public function removeDirparents($force = false) {
		$this->addTestCount(__METHOD__);

		foreach ($this->dirparents as $dirparent) {
			$this->removeDirparent($dirparent, $force);
		}
		return $this->dirparents->isEmpty();
	}

	// /**
	//  * Check all dirparents
	//  * @return boolean (false if found duplicates)
	//  */
	// public function checkDirparents() {
	// 	$count = $this->dirparents->count();
	// 	$checks = new ArrayCollection();
	// 	$this->dirparents = $this->dirparents->filter(
	// 		function($item) use ($checks) {
	// 			if($checks->contains($item)) return false;
	// 			$checks->add($item);
	// 			return true;
	// 		}
	// 	);
	// 	return $count === $this->dirparents->count();
	// }

	/**
	 * Remove parent Directory --> DIRECT FROM BASEDIRECTORY
	 * @param Basedirectory $dirparent
	 * @return boolean
	 */
	public function _bd_removeDirparent(Basedirectory $dirparent) {
		$this->addTestCount(__METHOD__);

		$this->dirparents->removeElement($dirparent);
		if($this->parent === $dirparent) $this->parent = null;
		return !$this->dirparents->contains($dirparent) && $this->parent !== $dirparent;
	}

	/**
	 * Remove parent Directorys --> DIRECT FROM BASEDIRECTORY
	 * @return boolean (true if $dirparent has been removed)
	 */
	public function _bd_removeDirparents() {
		$this->addTestCount(__METHOD__);

		foreach ($this->dirparents as $dirparent) {
			$this->_bd_removeDirparent($dirparent);
		}
		return $this->dirparents->isEmpty();
	}

	/**
	 * Has parent Directory
	 * @param Basedirectory $dirparent = null
	 * @param boolean $recursive = false
	 * @return boolean
	 */
	public function hasDirparent(Basedirectory $dirparent = null, $recursive = false) {
		$this->addTestCount(__METHOD__);

		if(empty($dirparent)) return !$this->dirparents->isEmpty();
		return $recursive ? $this->getAllDirparents()->contains($dirparent) : $this->dirparents->contains($dirparent);
	}


	/*************************************************************************************/
	/*** PARENT
	/*************************************************************************************/

	/**
	 * Can have parent Directory
	 * @param Basedirectory $parent = null
	 * @return string | false
	 */
	public function isChildeable(Basedirectory $parent = null) {
		$this->addTestCount(__METHOD__);

		// If has already $parent as parent
		// if($this->hasDirparent($parent, false)) return false;
		if($parent->isCompleteOrIncompleteRoot()) return false;
		if($parent instanceOf Menu) {
			// Menu accepts only Items with Pageweb
			if(empty($this->pageweb) && !($this instanceOf Pageweb)) return false;
			// Menu accepts only Menus and Items, but Directorys
			if($this instanceOf Directory) return false;
		}
		return true;
	}


	/*************************************************************************************/
	/*** NAME FOR ITEMS
	/*************************************************************************************/

	/**
	 * Get default inputname
	 * @return Item
	 */
	public function defaultInputname() {
		$this->addTestCount(__METHOD__);

		if(empty($this->inputname) && !empty($this->name)) $this->inputname = $this->name;
		return $this;
	}

	public function setInputname($inputname) {
		$this->addTestCount(__METHOD__);
		
		if(serviceTools::computeTextOrNull($inputname)) {
			$this->inputname = $inputname;
			$this->setName($this->inputname);
		}
		return $this;
	}

	/**
	 * Set name
	 * @param string $name
	 * @param boolean $updateNested = true
	 * @return self
	 */
	public function setName($name, $updateNested = true) {
		$this->addTestCount(__METHOD__);

		if($updateNested) $this->saveChanges(__METHOD__);
		$name = $this->removeRootPrefix($name);
		static::normalizeName($name);
		$this->name = $name;
		$this->defaultInputname();
		if($updateNested) $this->updateNestedDataIfchanges(__METHOD__);
		return $this;
	}

	public static function normalizeName(&$name) {
		$name = serviceTools::getTextOrNullStripped($name);
		$name = serviceTools::removeSeparator($name);
		return $name;
	}

	public function hasRootPrefix() {
		return preg_match('/^@(drive|system)/', $this->getName());
	}

	protected function removeRootPrefix($name) {
		$name = preg_replace('/^@(drive|system)/i', '', $name);
		return strlen($name) < 1 ? (empty($this->getId()) ? 'New ' : 'Name ').$this->getShortname() : $name;
	}

	public function getNameOrTempname() {
		return empty($this->getName()) ? static::TEMP_NAME : $this->getName();
	}

	public function getInputname() {
		return $this->inputname;
	}

	/**
	 * Get name (for jstree)
	 * @return string
	 */
	public function getText() {
		return $this->getName();
	}


	/*************************************************************************************/
	/*** NORMALIZE NAME
	/*************************************************************************************/

	/**
	 * Get normalize name method
	 * @return string
	 */
	public function getNormalizeNameMethod() {
		return $this->normalizeNameMethod;
	}

	/**
	 * Get choices for normalize name method
	 * @return array
	 */
	public function getNormalizeNameMethodChoices() {
		return FileManager::getNormalizeChoices();
	}

	/**
	 * Set normalize name method
	 * @param string $normalizeNameMethod = null
	 * @return Item
	 */
	public function setNormalizeNameMethod($normalizeNameMethod = null) {
		$this->addTestCount(__METHOD__);

		$this->normalizeNameMethod = FileManager::getDefaultNormalizeMethod($normalizeNameMethod);
		return $this;
	}


	/*************************************************************************************/
	/*** PATH NAME & SLUG
	/*************************************************************************************/

	/**
	 * Get Menu parents
	 * @param booolean $byPageweb = false
	 * @param booolean $reverse = false
	 * @return array <Menu>
	 */
	public function getLogicCatParents($byPageweb = false, $reverse = false) {
		$this->addTestCount(__METHOD__);

		$parents = [];
		$current = $this;
		// $parents[] = $current;
		$pageweb = $this->getPageweb();
		if($byPageweb && $pageweb instanceOf Pageweb) {
			$current = $pageweb;
			// $parents[] = $current;
			if(!count($current->getLogicCatParents(false))) $current = $this;
		}
		foreach ($current->getDirparents() as $directory) {
			if($directory instanceOf Menu) {
				$current = $directory;
				$parents[] = $current;
				$parents = array_merge($parents, $current->getLogicCatParents(false));
				break;
			}
		}
		return $reverse ? array_reverse($parents) : $parents;
	}

	// /**
	//  * Get first Menu parent
	//  * @param booolean $byPageweb = false
	//  * @return Menu
	//  */
	// public function getOneLogicalCatParent($byPageweb = false, $getLast = false) {
	// 	$parents = $this->getLogicCatParents($byPageweb, $getLast);
	// 	return count($parents) ? reset($parents) : null;
	// }

	/**
	 * Get best logical cats
	 *@return array <string>
	 */
	public function getCats() {
		$parents = $this->getLogicCatParents(true, true);
		$params = [];
		$i = 1;
		foreach ($parents as $parent) {
			if(!($parent instanceOf Menu) || !$parent->isRootmenu()) {
				if(in_array($parent->getSlug(), $params) || in_array($this->slug, $params)) $params = [];
				$params['cat'.$i++] = $parent->getSlug();
			}
		}
		if(!isset($params['cat'.($i-1)]) || $params['cat'.($i-1)] !== $this->slug) $params['cat'.$i] = $this->slug;
		return $params;
	}

	/**
	 * Get path slug
	 * @param string $prefix = 'cat'
	 * @param integer $limit = 3
	 * @return array <string>
	 */
	public function getCurrentPathslugAsParams($prefix = null, $limit = 5) {
		$prefix ??= DefaultController::CAT_NAME;
		$parentslugs = [];
		$parent = $this;
		$parentslugs[] = $parent->getSlug();
		while ($limit-- > 0) {
			$parent = $parent->getCurrentParent();
			if(!($parent instanceOf Basedirectory) || in_array($parent->getSlug(), $parentslugs) || ($parent instanceOf Menu && $parent->isRootmenu()) || $parent->getLvl() < 2) break;
			$parentslugs[] = $parent->getSlug();
		}
		$key = count($parentslugs);
		$pathslug = [];
		foreach ($parentslugs as $slug) $pathslug[$prefix.$key--] = $slug;
		return $pathslug;
	}

	/**
	 * Get path slug by Pageweb
	 * @param Pageweb $pageweb
	 * @param string $prefix = 'cat'
	 * @param integer $limit = 3
	 * @return array <string>
	 */
	public function getCurrentPathslugByPageweb(Pageweb $pageweb, $prefix = null, $limit = 5) {
		$prefix ??= DefaultController::CAT_NAME;
		$pwPath = $pageweb->getCurrentPathslugAsParams($prefix, $limit - 1);
		$pwPath[$prefix.(count($pwPath) + 1)] = $this->slug;
		return $pwPath;
	}

	public function getCurrentPathslug($as_array = false) {
		$currentParent = $this->getCurrentParent();
		$this->currentPathslug = $currentParent instanceOf Basedirectory ? $currentParent->getPathslug(true) : '';
		return $as_array ? explodepreg_split('/\\'.DIRECTORY_SEPARATOR.'/', $this->currentPathslug, -1, PREG_SPLIT_NO_EMPTY) : $this->currentPathslug;
	}

	public function getCurrentPathname($as_array = false) {
		$currentParent = $this->getCurrentParent();
		$this->currentPathslug = $currentParent instanceOf Basedirectory ? $currentParent->getPathname(true) : '';
		return $as_array ? explodepreg_split('/\\'.DIRECTORY_SEPARATOR.'/', $this->currentPathslug, -1, PREG_SPLIT_NO_EMPTY) : $this->currentPathslug;
	}

	/**
	 * Update pathname & pathslug
	 * @return Item
	 */
	public function updatePaths($updateChilds = false, $recompute = true) {
		if($this->canHaveParent()) {
			// Has parent
			if($recompute) {
				// if($this->isDeepControlOrCheckModeContext()) {
					// ***** FULL CONTROL *****
					$allparents = $this->getAllParents(true);
					// if($this->_is_repair()) $allparents->first()->checkAndRepairValidity();
					$parentsSlugs = [];
					$parentsNames = [];
					foreach ($allparents as $parent) {
						$parentsSlugs[] = $parent->getSlug();
						$parentsNames[] = $parent->getName();
						if($parent->getName() === static::TEMP_NAME) DevTerminal::contextException($this, false, "Far parent ".$parent->getShortname()." ".json_encode($parent->getNameOrTempname())." is wrong!", null, __LINE__, __METHOD__);
					}
					// if($this->_is_repair()) {
						$this->pathslug = implode(DIRECTORY_SEPARATOR, $parentsSlugs);
						$this->pathname = implode(DIRECTORY_SEPARATOR, $parentsNames);
					// }
					// if($this->pathslug !== implode(DIRECTORY_SEPARATOR, $parentsSlugs)) DevTerminal::contextException($this, false, "Pathslug ".json_encode($this->pathslug)." is wrong! Should be ".json_encode(implode(DIRECTORY_SEPARATOR, $parentsSlugs)).".", null, __LINE__, __METHOD__);
					// if($this->pathname !== implode(DIRECTORY_SEPARATOR, $parentsNames)) DevTerminal::contextException($this, false, "Pathname ".json_encode($this->pathname)." is wrong! Should be ".json_encode(implode(DIRECTORY_SEPARATOR, $parentsNames)).".", null, __LINE__, __METHOD__);
				// }
					} else {
						$this->pathslug = (string) $this->parent->getPathslug(true);
						$this->pathname = (string) $this->parent->getPathname(true);
					}
		} else {
			// can not have parent
			$this->pathslug = "";
			$this->pathname = "";
		}
		if($updateChilds && $this instanceOf Basedirectory) {
			foreach ($this->getSolidChilds(false) as $child) {
				$child->updatePaths(true, $recompute);
			}
		}
		return $this;
	}

	/**
	 * Get slug path
	 * @param boolean $addHimself = false
	 * @return string
	 */
	public function getPathslug($addHimself = false) {

		if(!is_string($this->pathslug)) $this->pathslug = "";
		if($addHimself) return empty($this->pathslug) ? $this->slug : $this->pathslug.DIRECTORY_SEPARATOR.$this->slug;
		return $this->pathslug;
	}

	/**
	 * Get slug paths
	 * @param boolean $addHimself = false
	 * @return array <string>
	 */
	public function getPathslugs($addHimself = false, $includeParent = true) {
		$pathslugs = [];
		foreach ($this->getDirparents() as $dirparent) {
			if($includeParent || $dirparent !== $this->parent) {
				$this->setCurrentParent($dirparent);
				$path = $this->getCurrentPathslug();
				$pathslugs[$path] = $path.($addHimself ? DIRECTORY_SEPARATOR.$this->slug : '');
				// $pathslugs[$dirparent->getOwnerdir()->getSlug()] = $path.($addHimself ? DIRECTORY_SEPARATOR.$this->slug : '');
			}
		}
		return $pathslugs;
	}
	public function getAllPathslugs($addHimself = false, $includeParent = true) {
		return $this->getPathslugs($addHimself, $includeParent);
	}

	/**
	 * Get slug path as array
	 * @param boolean $addHimself = false
	 * @param boolean $reverse = false
	 * @return array
	 */
	public function getPathslugAsArray($addHimself = false, $reverse = false) {

		$pathslug = $this->getPathslug($addHimself);
		$pathslug = preg_split('/\\'.DIRECTORY_SEPARATOR.'/', $pathslug, -1, PREG_SPLIT_NO_EMPTY);
		return $reverse ? array_reverse($pathslug) : $pathslug;
	}

	/**
	 * Get name path
	 * @param boolean $addHimself = false
	 * @return string
	 */
	public function getPathname($addHimself = false) {
		if($addHimself) return empty($this->pathname) ? $this->name : $this->pathname.DIRECTORY_SEPARATOR.$this->name;
		return $this->pathname;
	}

	/**
	 * Get name paths
	 * @param boolean $addHimself = false
	 * @return array <string>
	 */
	public function getPathnames($addHimself = false, $includeParent = true) {
		$pathnames = [];
		foreach ($this->getDirparents() as $dirparent) {
			if($includeParent || $dirparent !== $this->parent) {
				$this->setCurrentParent($dirparent);
				$path = $this->getCurrentPathname();
				$pathnames[$path] = $path.($addHimself ? DIRECTORY_SEPARATOR.$this->name : '');
				// $pathnames[$dirparent->getOwnerdir()->getName()] = $path.($addHimself ? DIRECTORY_SEPARATOR.$this->name : '');
			}
		}
		return $pathnames;
	}
	public function getAllPathnames($addHimself = false, $includeParent = true) {
		return $this->getPathnames($addHimself, $includeParent);
	}

	/**
	 * Get name path as array
	 * @param boolean $addHimself = false
	 * @param boolean $reverse = false
	 * @return array
	 */
	public function getPathnameAsArray($addHimself = false, $reverse = false) {
		$pathname = $this->getPathname($addHimself);
		$pathname = preg_split('/\\'.DIRECTORY_SEPARATOR.'/', $pathname, -1, PREG_SPLIT_NO_EMPTY);
		return $reverse ? array_reverse($pathname) : $pathname;
	}

	// /**
	//  * Update slug path
	//  * BEWARE: ONLY USED IN ONFLUSH CONTEXT, BECAUSE slugs are updated juste before OnFlush!!!
	//  * @return boolean (true if changed)
	//  */
	// public function updatePathslug($refresh = false) {
	// 	$this->addTestCount(__METHOD__);

	// 	$pathslug = $this->pathslug;
	// 	if(!$this->canHaveParent() || empty($this->parent)) {
	// 		$this->pathslug = "";
	// 	} else if($refresh) {
	// 		$parentsSlugs = [];
	// 		foreach ($this->getAllParents(true) as $parent) $parentsSlugs[] = $parent->getSlug();
	// 		$this->pathslug = implode(DIRECTORY_SEPARATOR, $parentsSlugs);
	// 	} else {
	// 		$this->pathslug = $this->parent->getPathslug(true);
	// 	}
	// 	return $pathslug !== $this->pathslug;
	// }

	// /**
	//  * Update Slug path
	//  * @return boolean (true if changed)
	//  */
	// public function updatePathname($refresh = false) {
	// 	$this->addTestCount(__METHOD__);

	// 	$pathname = $this->pathname;
	// 	if(!$this->canHaveParent() || empty($this->parent)) {
	// 		$this->pathname = "";
	// 	} else if($refresh) {
	// 		$parentsNames = [];
	// 		foreach ($this->getAllParents(true) as $parent) $parentsNames[] = $parent->getName();
	// 		$this->pathname = implode(DIRECTORY_SEPARATOR, $parentsNames);
	// 	} else {
	// 		$this->pathname = $this->parent->getPathname(true);
	// 	}
	// 	return $pathname !== $this->pathname;
	// }


	/*************************************************************************************/
	/*** ON/POST FLUSH
	/*************************************************************************************/

	public function onFlushItemdata(OnFlushEventArgs $args = null) {
		$this->addTestCount(__METHOD__);

		$this->updateNestedData($args, !($args instanceOf OnFlushEventArgs));
		return $this;
	}

	public function postFlushItemdata(PostFlushEventArgs $args = null) {
		$this->addTestCount(__METHOD__);

		$this->updateNestedData($args, !($args instanceOf PostFlushEventArgs));
		return $this;
	}


	/*************************************************************************************/
	/*** SLUG
	/*************************************************************************************/

	/**
	 * Get slug
	 * @return string 
	 */
	public function getSlug() {
		return $this->slug;
	}

	// /**
	//  * Set slug
	//  * @param string $slug
	//  * @return Item
	//  */
	// public function setSlug($slug) {
	// 	$this->slug = $slug;
	// 	$this->updatePaths(true, true);
	// 	// die('-> DONE: set slug '.json_encode($this->slug).' new pathslug: '.json_encode($this->pathslug).'!');
	// 	return $this;
	// }


	/*************************************************************************************/
	/*** ROOT DIRECTORYS
	/*************************************************************************************/

	public function hasDirs() {
		return $this->getRootDirs()->count() > 0;
		// $rootDirs = new ArrayCollection();
		// if($this->driveDir instanceOf Directory) $rootDirs->add($this->driveDir);
		// if($this->systemDir instanceOf Directory) $rootDirs->add($this->systemDir);
		// return $rootDirs->count() > 0;
	}

	public function getDriveDir() {
		return $this->driveDir;
	}

	public function setDriveDir(Directory $driveDir = null, $inverse = true, $force = false) {
		$this->addTestCount(__METHOD__);

		$result = true;
		$this->driveDir = $driveDir;
		if($inverse) $result = $this->driveDir->setOwnerdrivedir($this, false, $force);
		if(!$result) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." could not get this drive dir!", 1);
		if(!empty($this->systemDir) && $this->systemDir === $this->driveDir) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." has same drive and system dirs!", 1);
		return $result;
	}

	public function getSystemDir() {
		return $this->systemDir;
	}

	public function setSystemDir(Directory $systemDir = null, $inverse = true, $force = false) {
		$this->addTestCount(__METHOD__);

		$result = true;
		$this->systemDir = $systemDir;
		if($inverse) $result = $this->systemDir->setOwnersystemdir($this, false, $force);
		if(!$result) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." could not get this system dir!", 1);
		if(!empty($this->driveDir) && $this->driveDir === $this->systemDir) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this ".$this->getShortname()." ".json_encode($this->getName())." has same drive and system dirs!", 1);
		return $result;
	}

	public function getRootDirs() {
		$this->addTestCount(__METHOD__);

		$rootDirs = new ArrayCollection();
		if($this->driveDir instanceOf Directory) $rootDirs->add($this->driveDir);
		if($this->systemDir instanceOf Directory) $rootDirs->add($this->systemDir);
		return $rootDirs;
	}

	public function getOwnerdir($refresh = false) {
		// $this->addTestCount(__METHOD__, 100);

		if(!$refresh) return $this->ownerdir;
		if($this->isCompleteOrIncompleteRoot()) {
			return $this->ownerdir = $this->ownersystemdir instanceOf Item ? $this->ownersystemdir : $this->ownerdrivedir;
		}
		$rootparent = $this->getRootparent(true);
		return $this->ownerdir = !empty($rootparent) ? $rootparent->getOwnerdir() : null;
	}


	/*************************************************************************************/
	/*** PAGEWEB
	/*************************************************************************************/

	public function getPageweb() {
		if($this->pageweb instanceOf Pageweb) {
			$this->pageweb->setTempitem($this);
			$this->pageweb->setCurrentParent($this->getCurrentParent());
		}
		return $this->pageweb;
	}

	public function setPageweb(Pageweb $pageweb = null) {
		$this->pageweb = $pageweb;
		return $this;
	}

	public function getPagewebChoices() {
		return $this->pagewebChoices;
	}

	public function setPagewebChoices($pagewebChoices) {
		$this->pagewebChoices = $pagewebChoices;
		return $this;
	}


	/*************************/
	/*** MENU              ***/
	/*************************/

	public function isMenuable() {
		return $this->pageweb instanceOf Pageweb;
	}


	/*************************************************************************************/
	/*** PICONE
	/*************************************************************************************/

	/**
	 * Set URL as picone
	 * @param string $url
	 * @return Item
	 */
	public function setUrlAsPicone($url) {
		$this->addTestCount(__METHOD__);

		$picone = new Avatar();
		$picone->setUploadFile($url);
		$this->setPicone($picone);
		return $this;
	}

	/**
	 * Set resource as picone
	 * @param string $resource
	 * @return Item
	 */
	public function setResourceAsPicone(UploadedFile $resource = null) {
		$this->addTestCount(__METHOD__);

		if($resource instanceOf UploadedFile) {
			$picone = new Avatar();
			$picone->setUploadFile($resource);
			$this->setPicone($picone);
		}
		return $this;
	}

	public function getResourceAsPicone() {
		return null;
	}

	/**
	 * Set picone
	 * @param Picone $picone = null
	 * @return Item
	 */
	public function setPicone(Avatar $picone = null) {
		$this->picone = $picone;
		return $this;
	}

	/**
	 * Get picone
	 * @return Picone | null
	 */
	public function getPicone() {
		return $this->picone;
	}


	/*************************************************************************************/
	/*** RELATED TIERS
	/*************************************************************************************/

	// /**
	//  * Get related Entreprises
	//  * @param boolean $excludeSelfOwner = false
	//  * @return ArrayCollection <Entreprise>
	//  */
	// public function getRelEntreprises($excludeSelfOwner = false) {
	// 	$entreprises = new ArrayCollection($this->owner instanceOf Entreprise ? [$this->owner] : []);
	// 	foreach ($this->getDirparents() as $parent) {
	// 		$entreprise = $parent->getOwner();
	// 		if($entreprise instanceOf Entreprise && !$entreprises->contains($entreprise)) $entreprises->add($entreprise);
	// 	}
	// 	if($excludeSelfOwner && !empty($this->owner)) $entreprises->removeElement($this->owner);
	// 	return $entreprises;
	// }

	// /**
	//  * Get related Users
	//  * @param boolean $excludeSelfOwner = false
	//  * @return ArrayCollection <User>
	//  */
	// public function getRelUsers($excludeSelfOwner = false) {
	// 	$users = new ArrayCollection($this->owner instanceOf User ? [$this->owner] : []);
	// 	foreach ($this->getDirparents() as $parent) {
	// 		$entreprise = $parent->getOwner();
	// 		if($entreprise instanceOf User && !$users->contains($entreprise)) $users->add($entreprise);
	// 	}
	// 	if($excludeSelfOwner && !empty($this->owner)) $users->removeElement($this->owner);
	// 	return $users;
	// }

	// /**
	//  * Get related Tiers
	//  * @param boolean $excludeSelfOwner = false
	//  * @return ArrayCollection <Tier>
	//  */
	// public function getRelTiers($excludeSelfOwner = false) {
	// 	$tiers = new ArrayCollection($this->owner instanceOf Tier ? [$this->owner] : []);
	// 	foreach ($this->getDirparents() as $parent) {
	// 		$entreprise = $parent->getOwner();
	// 		if($entreprise instanceOf Tier && !$tiers->contains($entreprise)) $tiers->add($entreprise);
	// 	}
	// 	if($excludeSelfOwner && !empty($this->owner)) $tiers->removeElement($this->owner);
	// 	return $tiers;
	// }



	/*************************************************************************************/
	/*** CATEGORYS
	/*************************************************************************************/

	/**
	 * Get categorys
	 * @return ArrayCollection <Typitem>
	 */
	public function getCategorys() {
		return $this->categorys;
	}

	/**
	 * Get categorys
	 * @return ArrayCollection <Typitem>
	 */
	public function getCategoryNames() {
		$names = [];
		foreach ($this->categorys as $category) {
			$names[] = $category->getName();
		}
		return array_unique($names);
	}

	/**
	 * Set categorys
	 * @param ArrayCollection <Typitem> $categorys
	 * @return Item
	 */
	public function setCategorys($categorys) {
		$this->addTestCount(__METHOD__);

		foreach ($this->categorys as $category) {
			if(!$categorys->contains($category)) $this->removeCategory($category);
		}
		foreach ($categorys as $category) $this->addCategory($category);
		return $this;
	}

	/**
	 * Add category
	 * @param Typitem $category
	 * @return Item
	 */
	public function addCategory(Typitem $category, $inverse = true) {
		$this->addTestCount(__METHOD__);

		if(!$this->categorys->contains($category)) $this->categorys->add($category);
		if($inverse) $category->addItem($this, false);
		return $this;
	}

	/**
	 * Remove category
	 * @param Typitem $category
	 * @return Item
	 */
	public function removeCategory(Typitem $category, $inverse = true) {
		$this->addTestCount(__METHOD__);

		$this->categorys->removeElement($category);
		if($inverse) $category->removeItem($this, false);
		return $this;
	}

	/**
	 * Get categorys Choices
	 * @return array
	 */
	public function getCategorysChoices() {
		return $this->categorysChoices;
	}

	/**
	 * Set categorys Choices
	 * @param array
	 * @return Item
	 */
	public function setCategorysChoices($categorysChoices) {
		$this->categorysChoices = $categorysChoices;
		return $this;
	}





}