<?php
namespace ModelApi\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;

// FileBundle
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Service\serviceText;

use \Exception;

/**
 * Text
 * 
 * @ORM\Entity(repositoryClass="ModelApi\FileBundle\Repository\TextRepository")
 * @ORM\Table(
 *      name="`text`",
 *		options={"comment":"Files TEXT model"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_text",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 */
class Text extends File {

	const DEFAULT_ICON = 'fa-file-text-o';
	const DEFAULT_OWNER_DIRECTORY = '@@@drive_documents@@@';
	const ENTITY_SERVICE = serviceText::class;
	const DEFAULT_ASSTRING_EXTENSION = 'txt';

	// /**
	//  * Upload file
	//  * @CRUDS\Create(
	//  * 		type="FileType",
	//  * 		show="ROLE_USER",
	//  * 		update="ROLE_USER",
	//  * 		order=1000,
	//  * 		options={"required"=false, "attr"={"accept" = "auto"}}
	//  * 	)
	//  * @CRUDS\Update(
	//  * 		type="FileType",
	//  * 		show="ROLE_USER",
	//  * 		update="ROLE_USER",
	//  * 		order=1000,
	//  * 		options={"required"=false, "attr"={"accept" = "auto"}}
	//  * 	)
	//  * @CRUDS\Show(role="ROLE_USER", order=1000)
	//  */
	// protected $uploadFile;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $name;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $title;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $subtitle;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $menutitle;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $accroche;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $resume;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $fulltext;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $color;

	/**
	 * @var string
	 * Text - contenu du fichier
	 * @CRUDS\Create(show=true, update=true, type="SummernoteType", order=5, options={"label"="Texte enrichi", "required"=false, "description"="Éditeur de texte enrichi"}, contexts={"permanent"})
	 * @CRUDS\Update(show=true, update=true, type="SummernoteType", order=5, options={"label"="Texte enrichi", "required"=false, "description"="Éditeur de texte enrichi"}, contexts={"permanent"})
	 * @CRUDS\Show(role=true)
	 */
	protected $stringAsVersion;

	/**
	 * @var string
	 * Text - Identifiant du fichier
	 * @CRUDS\Create(show=true, update=true, type="TextType", order=1, options={"label"="Nom du fichier texte", "required"=true, "description"="Nom du fichier <strong>sans extension</strong>"}, contexts={"permanent"})
	 * @CRUDS\Update(show=true, update=true, type="TextType", order=1, options={"label"="Nom du fichier texte", "required"=true, "description"="Nom du fichier <strong>sans extension</strong>"}, contexts={"permanent"})
	 * @CRUDS\Show(role=true)
	 */
	protected $inputname;

	/**
	 * @var string
	 * Text - Nom du fichier
	 * @CRUDS\Create(show=false, update=false, type="TextType", order=1, options={"label"="Nom du fichier", "required"=true, "description"="Nom du fichier dans le système>"}, contexts={"permanent"})
	 * @CRUDS\Update(show=true, update="ROLE_SUPER_ADMIN", type="TextType", order=1, options={"label"="Nom du fichier", "required"=true, "description"="Nom du fichier dans le système>"}, contexts={"permanent"})
	 * @CRUDS\Show(role=true)
	 */
	protected $filename;


	public function __construct() {
		parent::__construct();
		$this->setNormalizeNameMethod(FileManager::NORMALIZE_METHODS[2]);
		$this->setCroppable(false);
		return $this;
	}

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['original'];
	}




}