<?php
// namespace ModelApi\FileBundle\Component;

// use ArrayIterator;
// use Closure;
// use const ARRAY_FILTER_USE_BOTH;
// use function array_filter;
// use function array_key_exists;
// use function array_keys;
// use function array_map;
// use function array_reverse;
// use function array_search;
// use function array_slice;
// use function array_values;
// use function count;
// use function current;
// use function end;
// use function in_array;
// use function key;
// use function next;
// use function reset;
// use function spl_object_hash;
// use function uasort;

// use Doctrine\ORM\EntityManager;
// use Doctrine\Common\Inflector\Inflector;
// use Doctrine\Common\Collections\Expr\ClosureExpressionVisitor;
// use Doctrine\Common\Collections\Criteria;

// use Doctrine\Common\Collections\Collection;
// use Doctrine\Common\Collections\Selectable;
// use Doctrine\Common\Collections\ArrayCollection;

// use ModelApi\FileBundle\Entity\Item;
// use ModelApi\FileBundle\Entity\Basedirectory;

// use \DateTime;
// use \Exception;

// class Linkdata implements Collection, Selectable {

// 	const LINKTYPE_UNDEFINED = 'undefined';		// undefined
// 	const LINKTYPE_HARDLINK = 'hardlink'; 		// Hard link
// 	const LINKTYPE_SYMLINK = 'symlink';			// Symbolic link
// 	const LINKTYPE_PROCESSED = 'processed';		// if is Query or other
// 	const LINKTYPE_ERROR = 'error';				// error
// 	const STRUCTURE = [
// 		'sortby' => ['sorted' => false, 'list' => []], // list is aray of [fieldname => way]
// 		'query' => ['processed' => false, 'exclusiveProcessed' => true, 'list' => []], // list is array of [repository (class), method]
// 		'items' => [], // items
// 	];
// 	const ITEM_STRUCTURE = [
// 		'item' => null,
// 		'position' => null,
// 		'linktype' => null,
// 		'date' => null,
// 	];

// 	protected $basedirectory;
// 	protected $entityManager;
// 	protected $_initialized;
// 	protected $data;


// 	public function __construct(Basedirectory $basedirectory, EntityManager $entityManager, $json_array = null) {
// 		$this->_initialized = false;
// 		// $this->initData();
// 		$this->entityManager = $entityManager;
// 		$this->setDirectory($basedirectory);
// 		if(is_array($json_array) && !empty($json_array)) $this->fromJson_array($json_array);
// 		return $this;
// 	}


// 	/*************************************************************************************/
// 	/*** INITIALIZE
// 	/*************************************************************************************/

// 	protected function initData() {
// 		$this->data = static::STRUCTURE;
// 		return $this;
// 	}

// 	protected function setInitialized() {
// 		$this->_initialized = true;
// 	}

// 	public function isInitialized() {
// 		return $this->_initialized;
// 	}

// 	protected function initialize() {
// 		if(!$this->isInitialized()) {
// 			if(!($this->basedirectory instanceOf Basedirectory)) throw new Exception("Error ".__METHOD__."(): Basedirectory is missing!", 1);
// 			if(!($this->entityManager instanceOf EntityManager)) throw new Exception("Error ".__METHOD__."(): EntityManager is missing!", 1);
// 			$this->initData();
// 			$this->checkAll();
// 		}
// 		return $this;
// 	}


// 	/*************************************************************************************/
// 	/*** CHECK ALL
// 	/*************************************************************************************/

// 	/**
// 	 * Check all data
// 	 * @return Linkdata
// 	 */
// 	protected function checkAll() {
// 		// Get from Basedirectory
// 		// if($synchWithBasedir) $this->updateFromBasedirectoryToSelf();
// 		// Checks
// 		$this->checkProcessed();
// 		$this->checkSorted();
// 		$this->checkPositions();
// 		// Put into Basedirectory
// 		// if($synchWithBasedir) $this->updateFromSelfToDirectory();
// 		return $this;
// 	}


// 	/*************************************************************************************/
// 	/*** GLOBAL
// 	/*************************************************************************************/

// 	protected function updateFromBasedirectoryToSelf() {
// 		$this->setItems($this->basedirectory->getDirchilds(), false);
// 		return $this->checkAll();
// 	}

// 	protected function updateFromSelfToDirectory() {
// 		$this->basedirectory->setDirchilds($this->toCollection());
// 		return $this->checkAll();
// 	}


// 	/*************************************************************************************/
// 	/*** BASEDIRECTORY
// 	/*************************************************************************************/

// 	/**
// 	 * Get Basedirectory
// 	 * @return Basedirectory
// 	 */
// 	public function getDirectory() {
// 		return $this->basedirectory;
// 	}

// 	/**
// 	 * Set Basedirectory
// 	 * @param Basedirectory $basedirectory
// 	 * @return Linkdata
// 	 */
// 	private function setDirectory(Basedirectory $basedirectory) {
// 		if($this->basedirectory instanceOf Basedirectory && $basedirectory !== $this->basedirectory) throw new Exception("Error ".__METHOD__."(): can not change Basedirectory! Never!", 1);
// 		$this->basedirectory = $basedirectory;
// 		$this->basedirectory->setLinkdata($this);
// 		$this->checkAll();
// 		return $this;
// 	}

// 	/*************************************************************************************/
// 	/*** ITEMS
// 	/*************************************************************************************/

// 	public function getItems() {
// 		return $this->toCollection();
// 	}

// 	public function setItems($items, $keepLinkdataPositions = true) {
// 		// $this->data['items'] = (array)$items;
// 		if($keepLinkdataPositions) $positions_items = $this->toArray();
// 		$this->removeItems();
// 		foreach ($items as $item) {
// 			$this->addItem($item);
// 		}
// 		if($keepLinkdataPositions) $this->checkPositions($positions_items);
// 		return $this;
// 	}

// 	/**
// 	 * Add Item
// 	 * @param Item $item
// 	 * @param integer $position = -1
// 	 * @return Linkdata
// 	 */
// 	public function addItem(Item $item, $position = -1) {
// 		// Create Item structure
// 		$new_item = static::ITEM_STRUCTURE;
// 		$item->setCurrentParent($this->basedirectory);
// 		$new_item['item'] = $item;
// 		$new_item['linktype'] = $item->getLinktype();
// 		$new_item['position'] = $this->getPositionOrNext($position);
// 		$new_item['date'] = new DateTime();
// 		$this->setItemData($new_item);
// 		$this->checkPositions();
// 		return $this;
// 	}

// 	/**
// 	 * Remove Item
// 	 * @param Item $item
// 	 * @return Linkdata
// 	 */
// 	public function removeItem(Item $item) {
// 		$this->data['items'] = array_filter($this->data['items'], function($element) use ($item) {
// 			return $element !== $item;
// 		});
// 		$this->checkPositions();
// 		return $this;
// 	}

// 	/**
// 	 * Remove all Items
// 	 * @return Linkdata
// 	 */
// 	public function removeItems() {
// 		$this->data['items'] = [];
// 		return $this;
// 	}

// 	/**
// 	 * Has Item
// 	 * @param Item $item
// 	 * @return boolean
// 	 */
// 	public function hasItem(Item $item) {
// 		$oid = static::getItemOid($item);
// 		return array_key_exists($oid, $this->data['items']);
// 	}

// 	/**
// 	 * Get Item data
// 	 * @param Item $item
// 	 * @return array | null
// 	 */
// 	public function getItemData(Item $item) {
// 		$oid = static::getItemOid($item);
// 		return array_key_exists($oid, $this->data['items']) ? $this->data['items'][$oid] : null;
// 	}

// 	/**
// 	 * Set Item data
// 	 * @param array $itemdata
// 	 * @return Linkdata
// 	 */
// 	public function setItemData($itemdata) {
// 		$this->controlItemdata($itemdata, true);
// 		$oid = static::getItemOid($itemdata['item']);
// 		return $this->data['items'][$oid] = $itemdata;
// 	}

// 	// protected function isValidItems($items) {
// 	// 	$result = true;
// 	// 	foreach ($items as $item) {
// 	// 		if(!($item instanceOf Item)) $result = false;
// 	// 	}
// 	// 	return $result;
// 	// }

// 	public static function getItemOid(Item $item) {
// 		return spl_object_hash($item);
// 	}

// 	/**
// 	 * Control Item data
// 	 * @param mixed $itemdata (array of data or Item or OID)
// 	 * @param boolean $exceptionIfErrors = false
// 	 * @return boolean
// 	 */
// 	protected function controlItemdata($itemdata, $exceptionIfErrors = false) {
// 		$valid = true;
// 		$messages = [];
// 		if(is_string($itemdata)) {
// 			if(array_key_exists($itemdata, $this->data['items'])) $itemdata = $this->data['items'][$itemdata];
// 				else $messages[] = "item must be instance of Item! Got ".json_encode($itemdata['item'])." instead!";
// 		} else if($itemdata instanceOf Item) {
// 			$itemdata = $this->getItemData($itemdata);
// 		}
// 		// CONTROL
// 		if(!is_array($itemdata)) {
// 			$messages[] = "data is not array!";
// 		} else {
// 			if(!isset($itemdata['date']) || !($itemdata['date'] instanceOf DateTime)) $itemdata['date'] = new DateTime();
// 			if(!isset($itemdata['item']) || !($itemdata['item'] instanceOf Item)) {
// 				$messages[] = "item is not instance of Item!";
// 			} else {
// 				$itemdata['linktype'] = $itemdata['item']->getLinktype();
// 				if($itemdata['item']->getCurrenParent() !== $this->basedirectory) {
// 					$messages[] = "item's current parent is the wrong parent. This was corrected.";
// 					$itemdata['item']->setCurrentParent($this->basedirectory);
// 				}
// 			}
// 			if(count(array_intersect($itemdata, static::ITEM_STRUCTURE)) !== count(static::ITEM_STRUCTURE)) $messages[] = "elements are missing. Needed ".json_encode(array_keys(count(static::ITEM_STRUCTURE))).", got ".json_encode(array_keys($itemdata))." instead!";
// 			if($itemdata['position'] < 0) $messages[] = "position must be a positive integer! Got ".json_encode($itemdata['position'])." instead!";
// 			// if($itemdata['item'] instanceOf Item) $messages[] = "item must be instance of Item! Got ".json_encode($itemdata['item'])." instead!";
// 		}
// 		if(count($messages)) {
// 			if($exceptionIfErrors) throw new Exception("Error ".__METHOD__."(): ERROR: ".implode(' / ERROR: ', $messages), 1);
// 			return false;
// 		}
// 		return true;
// 	}


// 	/*************************************************************************************/
// 	/*** LINKTYPES
// 	/*************************************************************************************/

// 	/**
// 	 * Get list of linktypes
// 	 * @param array <string>
// 	 */
// 	public static function getLinktypes() {
// 		return [
// 			0 => static::LINKTYPE_UNDEFINED,
// 			1 => static::LINKTYPE_HARDLINK,
// 			2 => static::LINKTYPE_SYMLINK,
// 			3 => static::LINKTYPE_PROCESSED,
// 			4 => static::LINKTYPE_ERROR,
// 		];
// 	}

// 	/**
// 	 * Get linktype name by index
// 	 * @param integer $index
// 	 * @param string
// 	 */
// 	public static function getLinktypeByIndex($index) {
// 		$linktypes = static::getLinktypes();
// 		return $linktypes[$index] ?? null;
// 		// return array_key_exists($index, $linktypes) ? $linktypes[$index] : null;
// 	}

// 	/**
// 	 * Get linktype index by name
// 	 * @param string $linktype
// 	 * @param integer
// 	 */
// 	public static function getIndexByName($linktype) {
// 		$linktypes = static::getLinktypes();
// 		return array_search($linktype);
// 	}


// 	/*************************************************************************************/
// 	/*** POSITIONS
// 	/*************************************************************************************/

// 	/**
// 	 * Set Item position / Returns true position
// 	 * 0 for first, any number, or -1 for last position
// 	 * @param Item $item
// 	 * @param integer $position = -1 // last
// 	 * @return interger
// 	 */
// 	public function setItemPosition(Item $item, $position = -1) {
// 		$itemdata = $this->getItemData($item);
// 		$itemdata['position'] = $this->getPositionOrNext($position);
// 		$itemdata['date'] = new DateTime();
// 		$this->setItemData($itemdata);
// 		return $this->getItemPosition($item);
// 	}

// 	/**
// 	 * Get Item position (returns -1 if not found)
// 	 * @param Item $item
// 	 * @return integer
// 	 */
// 	public function getItemPosition(Item $item) {
// 		$itemdata = $this->getItemData($item);
// 		return is_array($itemdata) ? $itemdata['position'] : -1;
// 	}

// 	/**
// 	 * Get next position
// 	 * @return integer
// 	 */
// 	public function getNextPosition() {
// 		$nextposition = 0;
// 		foreach ($this->data['items'] as $oid => $itemdata) {
// 			if(!is_integer($itemdata['position']) || $itemdata['position'] < 0) throw new Exception("Error ".__METHOD__."(): error on control, one position is ".json_encode($itemdata['position'])." not a positive integer!", 1);
// 			if($itemdata['position'] >= $nextposition) $nextposition = $itemdata['position'] + 1;
// 		}
// 		return $nextposition;
// 	}

// 	public function getPositionOrNext($position) {
// 		return $position < 0 ? $this->getNextPosition() : $position;
// 	}

// 	/**
// 	 * Reorder all Items' positions
// 	 * @param array | ArrayCollection $positions_items = array of {position: Item}
// 	 * @return Linkdata
// 	 */
// 	public function checkPositions($positions_items = []) {
// 		if($this->isSorted()) {
// 			$items = $this->getItems();
// 			$count = $items->count();
// 			$items = $this->sortByParams($items, $this->data['sortby']['list']);
// 			if($count != $items->count()) throw new Exception("Error ".__METHOD__."(): sort by params did not return the correct number of elements ", 1);
// 			$index = 0;
// 			foreach ($this->data['items'] as $oid => $itemdata) {
// 				foreach ($items as $item) {
// 					if(static::getItemOid($item) === $oid) $this->data['items']['oid']['position'] = $index++;
// 				}
// 			}
// 		} else {
// 			if($positions_items instanceOf ArrayCollection) $positions_items = $positions_items->toArray();
// 			if(is_array($positions_items) && count($positions_items)) {
// 				foreach ($positions_items as $position => $item) {
// 					$oid = static::getItemOid($item);
// 					if(array_key_exists($oid, $this->data['items'])) {
// 						if($this->data['items'][$oid]['item'] !== $item) throw new Exception("Error ".__METHOD__."(): Item and OID does not match!!", 1);
// 						$this->data['items'][$oid]['position'] = $this->getPositionOrNext($position);
// 						$this->data['items'][$oid]['date'] = new DateTime();
// 					}
// 				}
// 			}
// 			$items = $this->data['items'];
// 			$result = usort($items, function($a, $b) {
// 				if($a['position'] == $b['position']) {
// 					return $a['date'] < $b['date'] ? -1 : 1;
// 				}
// 				return $a['position'] < $b['position'] ? -1 : 1;
// 			});
// 			if(!$result) throw new Exception("Error ".__METHOD__."(): sort positions has failed!", 1);
// 			$this->data['items'] = $items;
// 		}
// 		// reorder
// 		$i = 0;
// 		foreach ($this->data['items'] as $oid => $itemdata) {
// 			$this->data['items'][$oid]['position'] = $i++;
// 		}
// 		// foreach ($this->data['items'] as $oid => $itemdata) {
// 		// 	if(!is_integer($itemdata['position']) || $itemdata['position'] < 0) throw new Exception("Error ".__METHOD__."(): error on control, one position is ".json_encode($itemdata['position'])." not a positive integer!", 1);
// 		// }
// 		return $this;
// 	}

// 	public static function sortByParams($items, $params) {
// 		$tests = [];
// 		$addedParams = ['created' => 'DESC', 'id' => 'ASC'];
// 		$params = array_merge($params, $addedParams);
// 		foreach ($params as $fieldname => $way) {
// 			$getter = Inflector::camelize('get_'.$fieldname);
// 			foreach ($items as $item) {
// 				if(method_exists($item, $getter)) {
// 					$val = static::indexifyValue($item->$getter());
// 					$tests[$fieldname][$val] ??= new ArrayCollection();
// 					if(!$tests[$fieldname][$val]->contains($item)) $tests[$fieldname][$val]->add($item);
// 				}
// 			}
// 		}
// 		if(strtoupper($way) === 'DESC') {
// 			ksort($tests[$fieldname]);
// 		} else {
// 			krsort($tests[$fieldname]);
// 		}
// 		$index = 0;
// 		$sorteds = new ArrayCollection();
// 		foreach ($tests as $fieldname => $value_items) {
// 			foreach ($value_items as $value => $listItems) {
// 				if($listItems->count() == 1) {
// 					$item = $listItems->first();
// 					if(!$sorteds->contains($item)) $sorteds->set($index++, $item);
// 				} else {
// 					foreach (static::sortByParams($sorteds, []) as $reSortItem) {
// 						if(!$sorteds->contains($reSortItem)) $sorteds->set($index++, $reSortItem);
// 					}
// 				}
// 			}
// 		}
// 		return $sorteds;
// 	}

// 	public static function indexifyValue($value) {
// 		if($value instanceOf DateTime) return $value->getTimestamp();
// 		if(is_object($value)) {
// 			$methods = ['__toString','getName','getSlug','getId'];
// 			foreach ($methods as $method) {
// 				if(method_exists($value, $method)) return $value->$method();
// 			}
// 			throw new Exception("Error ".__METHOD__."(): no method found to get a string value of object ".get_class($value)."!", 1);
// 		}
// 		return $value;
// 	}


// 	/*************************************************************************************/
// 	/*** SORTED
// 	/*************************************************************************************/

// 	/**
// 	 * Is sorted
// 	 * @return boolean
// 	 */
// 	public function isSorted() {
// 		$this->checkSorted();
// 		return $this->data['sortby']['sorted'];
// 	}

// 	/**
// 	 * Check sorted
// 	 * @return Linkdata
// 	 */
// 	protected function checkSorted() {
// 		if(empty($this->data['sortby']['list']) && $this->data['sortby']['sorted']) $this->data['sortby']['sorted'] = false;
// 		return $this;
// 	}

// 	/**
// 	 * Set sorted
// 	 * @param boolean $sorted
// 	 * @return boolean
// 	 */
// 	public function setSorted($sorted) {
// 		$this->data['sortby']['sorted'] = (boolean)$sorted;
// 		return $this->isSorted();
// 	}


// 	/*************************************************************************************/
// 	/*** QUERYS
// 	/*************************************************************************************/

// 	/**
// 	 * Is processed (query) -> WITH items listed in Basedirectory Dirchilds relation
// 	 * @return boolean
// 	 */
// 	public function isProcessed() {
// 		$this->checkProcessed();
// 		return $this->data['query']['processed'];
// 	}
// 	public function isQuery() { return $this->isProcessed(); }

// 	/**
// 	 * Is EXCLUSIVE processed (query) -> if true : WITHOUT items listed in Basedirectory Dirchilds relation
// 	 * @return boolean
// 	 */
// 	public function isExclusiveProcessed() {
// 		$this->checkProcessed();
// 		return $this->data['query']['exclusiveProcessed'] && $this->isProcessed();
// 	}
// 	public function isExclusiveQuery() { return $this->isExclusiveProcessed(); }

// 	/**
// 	 * Check processed (query)
// 	 * @return Linkdata
// 	 */
// 	protected function checkProcessed() {
// 		if(empty($this->data['query']['list']) && $this->data['query']['processed']) $this->data['query']['processed'] = false;
// 		return $this;
// 	}

// 	/**
// 	 * Set processed (query)
// 	 * @param boolean $processed
// 	 * @return boolean
// 	 */
// 	public function setProcessed($processed) {
// 		$this->data['query']['processed'] = (boolean)$processed;
// 		return $this->isProcessed();
// 	}
// 	public function setQuery($query) { return $this->setProcessed($query); }

// 	/**
// 	 * Set EXCLUSIVE processed (query)
// 	 * @param boolean $exclusiveProcessed
// 	 * @return boolean
// 	 */
// 	public function setExclusiveProcessed($exclusiveProcessed) {
// 		$this->data['query']['exclusiveProcessed'] = (boolean)$exclusiveProcessed;
// 		return $this->isProcessed();
// 	}
// 	public function setExclusiveQuery($query) { return $this->setExclusiveProcessed($query); }


// 	/*************************************************************************************/
// 	/*** JSON_ARRAY
// 	/*************************************************************************************/

// 	/**
// 	 * Get array of data for json_array ORM persist to database
// 	 * Array structure:
// 	 * 		'sortby' => (same data)
// 	 * 		'query' => (same data)
// 	 * 		'items' => {position => {id: item id, linktype: linktype}}
// 	 * @return array <Item>
// 	 */
// 	public function toJson_array() {
// 		$this->checkAll();
// 		$json_array = [
// 			'sortby' => $this->data['sortby'],
// 			'query' => $this->data['query'],
// 			'items' => [],
// 		];
// 		foreach ($this->data['items'] as $oid => $itemdata) {
// 			$json_array['items'][$itemdata['position']] = [
// 				'item' => $itemdata['item']->getId(),
// 				'position' => $itemdata['position'],
// 				'linktype' => $itemdata['linktype'],
// 				'date' => $itemdata['date']->format(serviceContext::CONTEXT_FORMAT_DATE),
// 			];
// 		}
// 		return $json_array;
// 	}

// 	/**
// 	 * Set array of data from json_array ORM database
// 	 * @return array <Item>
// 	 */
// 	protected function fromJson_array($array) {
// 		$dirchilds = [];
// 		foreach ($this->basedirectory->getDirchilds() as $dirchild) {
// 			$dirchilds[$dirchild->getId()] = $dirchild;
// 		}
// 		foreach (static::STRUCTURE as $key => $value) {
// 			switch ($key) {
// 				case 'items':
// 					$this->data[$key] = [];
// 					foreach ($array[$key] as $position => $itemdata) {
// 						if(array_key_exists($itemdata['id'], $dirchilds)) {
// 							$item = $dirchilds[$itemdata['id']];
// 							$oid = static::getItemOid($item);
// 							foreach (static::ITEM_STRUCTURE as $name => $null) {
// 								switch ($name) {
// 									case 'item':
// 										$this->data[$key][$oid][$name] = $item;
// 										break;
// 									case 'date':
// 										$this->data[$key][$oid][$name] = new DateTime($itemdata[$name]);
// 										break;
// 									default:
// 										$this->data[$key][$oid][$name] = $itemdata[$name];
// 										break;
// 								}
// 							}
// 						} else {
// 							// item does not exist (deleted or anything else...)
// 						}
// 					}
// 					break;
// 				default:
// 					$this->data[$key] = $array[$key];
// 					break;
// 			}
// 		}
// 		$this->checkAll();
// 	}


// 	/*************************************************************************************/
// 	/*** ARRAYCOLLECTION METHODS
// 	/*************************************************************************************/

// 	/**
// 	 * Get array of Items ['position' => Item]
// 	 * @return array <Item>
// 	 */
// 	public function toArray() {
// 		$array = [];
// 		$this->checkPositions();
// 		foreach ($this->data['items'] as $oid => $itemdata) {
// 			if(isset($array[intval($itemdata['position'])])) throw new Exception("Error ".__METHOD__."(): position ".intval($itemdata['position'])." already exists!", 1);
// 			$array[intval($itemdata['position'])] = $itemdata['item'];
// 			$itemdata['item']->setCurrentParent($this->basedirectory);
// 		}
// 		ksort($array, SORT_NUMERIC);
// 		return $array;
// 	}

// 	/**
// 	 * Get position indexed ArrayCollection of Items
// 	 * @return ArrayCollection <Item>
// 	 */
// 	public function toCollection() {
// 		return new ArrayCollection($this->toArray());
// 	}

// 	/**
// 	 */
// 	public function first() {
// 		$item = reset($this->data['items']);
// 		return is_array($item) ? $item['item'] : $item;
// 	}

// 	/**
// 	 */
// 	public function last() {
// 		$item = end($this->data['items']);
// 		return is_array($item) ? $item['item'] : $item;
// 	}

// 	/**
// 	 */
// 	public function key() {
// 		$item = key($this->data['items']);
// 		return is_array($item) ? $item['item'] : $item;
// 	}

// 	/**
// 	 */
// 	public function next() {
// 		$item = next($this->data['items']);
// 		return is_array($item) ? $item['item'] : $item;
// 	}

// 	/**
// 	 */
// 	public function current() {
// 		$item = current($this->data['items']);
// 		return is_array($item) ? $item['item'] : $item;
// 	}

// 	/**
// 	 */
// 	public function remove($key) {
// 		if(!$this->containsKey($key)) return null;
// 		$removed = $this->data['items'][$key];
// 		unset($this->data['items'][$key]);
// 		return $removed['item'];
// 	}

// 	/**
// 	 */
// 	public function removeElement($element) {
// 		$count = count($this->data['items']);
// 		$this->data['items'] = array_filter($this->data['items'], function ($item) use ($element) {
// 			return $item['item'] !== $element;
// 		});
// 		return $count != count($this->data['items']);
// 	}

// 	/**
// 	 * Required by interface ArrayAccess.
// 	 * @psalm-param TKey $offset
// 	 */
// 	public function offsetExists($offset) {
// 		return $this->containsKey($offset);
// 	}

// 	/**
// 	 * Required by interface ArrayAccess.
// 	 * @psalm-param TKey $offset
// 	 */
// 	public function offsetGet($offset) {
// 		return $this->get($offset);
// 	}

// 	/**
// 	 * Required by interface ArrayAccess.
// 	 */
// 	public function offsetSet($offset, $value) {
// 		if (!isset($offset)) {
// 			$this->add($value);
// 			return;
// 		}
// 		$this->set($offset, $value);
// 	}

// 	/**
// 	 * Required by interface ArrayAccess.
// 	 * @psalm-param TKey $offset
// 	 */
// 	public function offsetUnset($offset) {
// 		$this->remove($offset);
// 	}

// 	/**
// 	 */
// 	public function containsKey($key) {
// 		return isset($this->data['items'][$key]) || array_key_exists($key, $this->data['items']);
// 	}

// 	/**
// 	 */
// 	public function contains($element) {
// 		return in_array($element, $this->data['items'], true);
// 	}

// 	/**
// 	 */
// 	public function exists(Closure $p) {
// 		foreach ($this->data['items'] as $key => $element) {
// 			if ($p($key, $element['item'])) return true;
// 		}
// 		return false;
// 	}

// 	/**
// 	 */
// 	public function indexOf($element) {
// 		$array = $this->toArray();
// 		return array_search($element, $array, true);
// 	}

// 	/**
// 	 */
// 	public function get($key) {
// 		$array = $this->toArray();
// 		return $array[$key] ?? null;
// 	}

// 	/**
// 	 * Get list of oid
// 	 */
// 	public function getKeys() {
// 		return array_keys($this->data['items']);
// 	}

// 	/**
// 	 * Get list of just Items, without other information
// 	 * @return array <Item>
// 	 */
// 	public function getValues() {
// 		$array = $this->toArray();
// 		return array_values($array);
// 	}

// 	/**
// 	 */
// 	public function count() {
// 		return count($this->data['items']);
// 	}

// 	/**
// 	 * @param integer $key (position / -1 for last)
// 	 * @param mixed $value
// 	 */
// 	public function set($key, $value) {
// 		return $this->addItem($element, $key);
// 	}

// 	/**
// 	 * @psalm-suppress InvalidPropertyAssignmentValue
// 	 * This breaks assumptions about the template type, but it would
// 	 * be a backwards-incompatible change to remove this method
// 	 * @param mixed $element
// 	 */
// 	public function add($element) {
// 		return $this->addItem($element);
// 	}

// 	/**
// 	 */
// 	public function isEmpty() {
// 		return empty($this->data['items']);
// 	}

// 	/**
// 	 * Required by interface IteratorAggregate.
// 	 */
// 	public function getIterator() {
// 		return new ArrayIterator($this->data['items']);
// 	}

// 	/**
// 	 * @return static
// 	 * @psalm-template U
// 	 * @psalm-param Closure(T=):U $func
// 	 * @psalm-return static<TKey, U>
// 	 */
// 	public function map(Closure $func) {
// 		return $this->toCollection()->map($func);
// 	}

// 	/**
// 	 * @return static
// 	 * @psalm-return static<TKey,T>
// 	 */
// 	public function filter(Closure $p) {
// 		return $this->toCollection()->filter($p);
// 	}

// 	/**
// 	 */
// 	public function forAll(Closure $p, $stopIfResultFalse = false) {
// 		$result = true;
// 		foreach ($this->data['items'] as $key => $element) {
// 			if(!$p($key, $element['item'])) {
// 				$result = false;
// 				if($stopIfResultFalse) return $result;
// 			}
// 		}
// 		return $result;
// 		// return $this->toCollection()->forAll($p, $stopIfResultFalse);
// 	}

// 	/**
// 	 */
// 	public function partition(Closure $p) {
// 		return $this->toCollection()->partition($p);
// 	}

// 	/**
// 	 * Returns a string representation of this object.
// 	 * @return string
// 	 */
// 	public function __toString() {
// 		return self::class . '@' . spl_object_hash($this);
// 	}

// 	/**
// 	 */
// 	public function clear() {
// 		$this->data['items'] = [];
// 	}

// 	/**
// 	 */
// 	public function slice($offset, $length = null) {
// 		return array_slice($this->data['items'], $offset, $length, true);
// 	}

// 	/**
// 	 */
// 	public function matching(Criteria $criteria) {
// 		$expr     = $criteria->getWhereExpression();
// 		$filtered = $this->data['items'];
// 		if ($expr) {
// 			$visitor  = new ClosureExpressionVisitor();
// 			$filter   = $visitor->dispatch($expr);
// 			$filtered = array_filter($filtered, $filter);
// 		}
// 		$orderings = $criteria->getOrderings();
// 		if ($orderings) {
// 			$next = null;
// 			foreach (array_reverse($orderings) as $field => $ordering) {
// 				$next = ClosureExpressionVisitor::sortByField($field, $ordering === Criteria::DESC ? -1 : 1, $next);
// 			}
// 			uasort($filtered, $next);
// 		}
// 		$offset = $criteria->getFirstResult();
// 		$length = $criteria->getMaxResults();
// 		if ($offset || $length) {
// 			$filtered = array_slice($filtered, (int) $offset, $length);
// 		}
// 		return new ArrayCollection($filtered);
// 	}



// }