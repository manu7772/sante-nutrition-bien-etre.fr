<?php

namespace ModelApi\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Response;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceCache;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;

use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceDirectory;
use ModelApi\FileBundle\Service\serviceMenu;

use \Exception;

class TwigController extends Controller {

	// const REFRESH_MENU = true;
	// const CACHED = true;

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get Twig content",
	 *    output = { "type" = "html/text" },
	 *    views = { "default", "twig" }
	 * )
	 * @Rest\View()
	 * @Rest\Get("/twig/content/{bddid}", name="apifile-twig-getcontent")
	 */
	public function getTwigcontentAction($bddid, Request $request) {
		// $bddid = $request->request->get('bddid');
		$serviceEntities = $this->get(serviceEntities::class);
		if(!$serviceEntities->isValidBddid($bddid, false)) throw new Exception("Error ".json_encode($bddid), 1);
		if($serviceEntities->isValidBddid($bddid, true)) $twig = $serviceEntities->findByBddid($bddid);
		$twig->setSelfcontent(true);
		switch ($twig->getShortname()) {
			case 'Twig':
				$data = [ucfirst($twig->getTypepage()) => $twig];
				break;
			default: // Pageweb
				$data = [$twig->getShortname() => $twig];
				break;
		}
		
		return ['response' => $this->render($twig->getBundlepathname(), $data)];
	}

	// http://local.vetoalliance/app_dev.php/api/twig/content/bddid@---Item---File---Twig---165



}
