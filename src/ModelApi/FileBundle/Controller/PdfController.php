<?php

namespace ModelApi\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;

use ModelApi\FileBundle\Entity\Pdf;
// use ModelApi\FileBundle\Entity\File;
// use ModelApi\FileBundle\Entity\Directory;

// use ModelApi\FileBundle\Service\FileManager;

class PdfController extends Controller {

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Pdf",
	 *    description="Get Pdfs",
	 *    output = { "class" = Pdf::class, "collection" = true, "groups" = {"Pdf"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base"})
	 * @Rest\Get("/pdfs", name="modelapifile-getpdfs")
	 */
	public function getPdfsAction(Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$pdfs = $em->getRepository(Pdf::class)->findAll();
		return $pdfs;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Pdf",
	 *    description="Get Pdf by Id",
	 *    output = { "class" = Pdf::class, "collection" = false, "groups" = {"Pdf"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","Pdf"})
	 * @Rest\Get("/pdf/{id}", name="modelapifile-getpdf")
	 */
	public function getPdfAction($id, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$pdf = $em->getRepository(Pdf::class)->find($id);
		return $pdf;
	}



}
