<?php

namespace ModelApi\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;

// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\User;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class DefaultController extends Controller {

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get Items",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","Item","File","Directory","jstree"})
	 * @Rest\Get("/items", name="modelapifile-getitems")
	 */
	public function getItemsAction(Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$items = $em->getRepository(Item::class)->findAll();
		return $items;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Update root menus",
	 *    output = { "class" = Menu::class, "collection" = true, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base"})
	 * @Rest\Get("/menus/updateroots", name="modelapifile-updaterootmenus")
	 */
	public function updateRootmenusAction(Request $request) {
		return $this->get(serviceMenu::class)->updateCacheRootMenus();
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get Item",
	 *    output = { "class" = Item::class, "collection" = false, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","Item","File","Directory"})
	 * @Rest\Get("/item/{id}", name="modelapifile-getitem")
	 */
	public function getItemAction($id, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$item = $em->getRepository(Item::class)->find($id);
		if(!($this->getUser() instanceOf User) || !$this->getUser()->isSuperadmin()) throw new AccessDeniedHttpException("Access denied!");
		if(empty($item)) throw new NotFoundHttpException("Item not found!");
		return $item;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get Item",
	 *    output = { "class" = Item::class, "collection" = false, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","Item","File","Directory","validity"})
	 * @Rest\Post("/item-validity/{id}", name="modelapifile-getitem-validity")
	 */
	public function getItemValidityAction($id, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$item = $em->getRepository(Item::class)->find($id);
		if(!($this->getUser() instanceOf User) || !$this->getUser()->isSuperadmin()) throw new AccessDeniedHttpException("Access denied!");
		if(empty($item)) throw new NotFoundHttpException("Item not found!");
		return $item;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Check Item",
	 *    output = { "class" = Item::class, "collection" = false, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","Item","File","Directory","validity"})
	 * @Rest\Post("/action-on-item/{id}/{action}", name="modelapifile-action-on-item")
	 */
	public function actionOnItemAction(Item $id, $action = 'check', Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		// $item = $em->getRepository(Item::class)->find($id);
		if(!($this->getUser() instanceOf User) || !$this->getUser()->isSuperadmin()) throw new AccessDeniedHttpException("Access denied!");
		if(empty($id)) throw new NotFoundHttpException("Item not found!");
		$id->updateDate();
		$em->flush();
		return $id;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get doublons of Item",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","Item","File","Directory"})
	 * @Rest\Post("/get-doublons/{id}", name="modelapifile-get-doublons")
	 * !!!@Rest\Get("/get-doublons/{id}", name="modelapifile-get-doublons")
	 */
	public function getDoublonsAction($id, Request $request) {
		if(!($this->getUser() instanceOf User)) throw new AccessDeniedHttpException("Access denied!");
		if(empty($id)) throw new NotFoundHttpException("Item not found!");
		return $this->get(serviceBasedirectory::class)->findDoublons('entities', $id);
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Resolve doublons of Item",
	 *    output = { "class" = Item::class, "collection" = false, "groups" = {"File"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","Item","File","Directory"})
	 * @Rest\Post("/resolve-doublons/{id}", name="modelapifile-resolve-doublons", defaults={"id" = null})
	 */
	public function resolveDoublonsAction($id = null, Request $request) {
		$request_id = $request->request->get('id');
		if(!empty($request_id)) $id = $request_id;
		$em = $this->getDoctrine()->getEntityManager();
		$item = $em->getRepository(Item::class)->find($id);
		if(!($this->getUser() instanceOf User)) throw new AccessDeniedHttpException("Access denied!");
		if(empty($item)) throw new NotFoundHttpException("Item not found!");
		$result = $this->get(serviceBasedirectory::class)->resolveDoublons($item);
		if(!$result) return ['message' => "Aucune suppression n'a été faite."];
		return $item;
	}



	/**
	 * @Route("/create/file/{name}", name="modelapifile-create-file", methods={"GET"})
	 */
	public function createFileAction($name, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();

		$file = new File();
		$file->setName($name);

		$em->persist($file);
		$em->flush();

		return $this->redirectToRoute('modelapifile-getitems');
	}

	/**
	 * @Route("/remove/file/{slug}", name="modelapifile-remove-file", methods={"GET"})
	 */
	public function removeFileAction($slug, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$file = $em->getRepository('ModelApiFileBundle:File')->findOneBySlug($slug);

		if(!empty($file)) {
			$em->remove($file);
			$em->flush();
		}

		return $this->redirectToRoute('modelapifile-getitems');
	}

	/**
	 * @Route("/create/directory/{name}", name="modelapifile-create-directory", methods={"GET"})
	 */
	public function createDirectoryAction($name, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();

		$directory = new Directory();
		$directory->setName($name);

		$em->persist($directory);
		$em->flush();

		return $this->redirectToRoute('modelapifile-getitems');
	}

	/**
	 * @Route("/remove/directory/{slug}", name="modelapifile-remove-directory", methods={"GET"})
	 */
	public function removeDirectoryAction($slug, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$directory = $em->getRepository('ModelApiFileBundle:Directory')->findOneBySlug($slug);

		if(!empty($directory)) {
			$em->remove($directory);
			$em->flush();
		}

		return $this->redirectToRoute('modelapifile-getitems');
	}

	/**
	 * @Route("/directory-add-file/{slug}", name="modelapifile-directory-add-file", methods={"POST"})
	 */
	public function directoryAddFileAction($slug, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$item = $em->getRepository(Item::class)->findOneBySlug($slug);
		$directory = $em->getRepository('ModelApiFileBundle:Directory')->find($request->request->get('directory'));
		$directory->addChild($item);
		$em->flush();

		return $this->redirectToRoute('modelapifile-getitems');
	}

	/**
	 * @Route("/directory-remove-file/{id}/{slug}", name="modelapifile-directory-remove-file", methods={"GET"})
	 */
	public function directoryRemoveFileAction($id, $slug, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$directory = $em->getRepository('ModelApiFileBundle:Directory')->find($id);
		$item = $em->getRepository(Item::class)->findOneBySlug($slug);
		$directory->removeChild($item);
		$em->flush();

		return $this->redirectToRoute('modelapifile-getitems');
	}



}
