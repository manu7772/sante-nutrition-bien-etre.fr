<?php

namespace ModelApi\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;

use ModelApi\FileBundle\Entity\Image;
// use ModelApi\FileBundle\Entity\File;
// use ModelApi\FileBundle\Entity\Directory;

// use ModelApi\FileBundle\Service\FileManager;

class ImageController extends Controller {

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Image",
	 *    description="Get Images",
	 *    output = { "class" = Image::class, "collection" = true, "groups" = {"Image"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base"})
	 * @Rest\Get("/images", name="modelapifile-getimages")
	 */
	public function getImagesAction(Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$images = $em->getRepository(Image::class)->findAll();
		return $images;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Image",
	 *    description="Get Image by Id",
	 *    output = { "class" = Image::class, "collection" = false, "groups" = {"Image"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","Image"})
	 * @Rest\Get("/image/{id}", name="modelapifile-getimage")
	 */
	public function getImageAction($id, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$image = $em->getRepository(Image::class)->find($id);
		return $image;
	}



}
