<?php

namespace ModelApi\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Response;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceForms;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Tempfile;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Service\serviceImage;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Service\servicePdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Service\serviceVideo;
use ModelApi\FileBundle\Entity\Audio;
use ModelApi\FileBundle\Service\serviceAudio;

use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
use ModelApi\FileBundle\Service\serviceDirectory;
use ModelApi\FileBundle\Service\serviceMenu;

use \Exception;

class BasedirectoryController extends Controller {

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get directory's childs",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","detail"})
	 * @Rest\Get("/basedirectory/childs/{id}", name="api-basedirectory-getdirectorychilds")
	 */
	public function getDirectoryChildsAction($id, Request $request) {
		return $this->get(serviceBasedirectory::class)->getRepository()->find($id)->getChilds();
	}

	protected function addEntityAndForm($type, Basedirectory $parent, &$data) {
		switch ($type) {
			case 'Image':
				$data['entity'] = $this->get(serviceImage::class)->createNew();
				break;
			case 'Pdf':
				$data['entity'] = $this->get(servicePdf::class)->createNew();
				break;
			case 'Video':
				$data['entity'] = $this->get(serviceVideo::class)->createNew();
				break;
			default:
				// Generic file
				$data['entity'] = $this->get(FileManager::class)->createNew();
				break;
		}
		//$data['entity']->addDirparent($parent);
		$url = ['route' => 'api-basedirectory-createchild-post', 'params' => ['parent' => $parent->getId(), 'type' => $type]];
		$data['form'] = $this->get(serviceForms::class)->getCreateForm($data['entity'], $this->getUser(), ['popup'], ['ACTION_ROUTE' => $url, 'SUBMITS' => true, 'SUBMITS' => false, 'LARGE_FOOTER' => false]);
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get HTML Form for create directory's childs",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","detail"})
	 * @Rest\Get("/basedirectory/create-child/form/{parent}/{type}", defaults={"type"="undefined"}, name="api-basedirectory-createchild-form")
	 */
	public function getChildFormAction($parent, $type = 'undefined', Request $request) {
		$parent = $this->get(serviceBasedirectory::class)->getRepository()->find($parent);
		$data = [];
		$this->addEntityAndForm($type, $parent, $data);
		$data['form_view'] = $data['form']->createView();
		return $this->get('twig')->createTemplate('{{ form(form_view) }}')->render($data);
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Post Form for create directory's childs",
	 *    output = { "class" = Item::class, "collection" = false, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"base","detail"})
	 * @Rest\Post("/basedirectory/create-child/post/{parent}/{type}", defaults={"type"="undefined"}, name="api-basedirectory-createchild-post")
	 */
	public function postChildFormAction($parent, $type = 'undefined', Request $request) {
		$test = $request->request->all();
		if(empty($test)) throw new Exception("Request data is empty!", 1);
		$parent = $this->get(serviceBasedirectory::class)->getRepository()->find($parent);
		$data = [];
		$this->addEntityAndForm($type, $parent, $data);
		$data['form']->handleRequest($request);
		$file = $data['form']->getData();
		$parent->addChild($file);
		if($file instanceOf Tempfile) {
			$file = $this->get(FileManager::class)->createFileByTempfile($file);
			$errors = $this->get('validator')->validate($file);
			$valid = !count($errors);
		} else {
			$valid = $data['form']->isSubmitted() && $data['form']->isValid();
		}
		if($valid) {
			$em = $this->get(serviceEntities::class)->getEntityManager();
			$em->persist($file);
			$em->flush();
			return $this->redirectToRoute('api-basedirectory-getdirectorychilds', ['id' => $parent->getId()]);
		} else {
			$data['form_view'] = $data['form']->createView();
			return $this->get('twig')->createTemplate('{{ form(form_view) }}')->render($data);
		}

	}


}
