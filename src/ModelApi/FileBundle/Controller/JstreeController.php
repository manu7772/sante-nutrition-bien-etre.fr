<?php

namespace ModelApi\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Response;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceCache;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;

use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceDirectory;
use ModelApi\FileBundle\Service\serviceMenu;

use \Exception;

class JstreeController extends Controller {

	// const REFRESH_MENU = true;
	// const CACHED = true;

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get Items for Jstree",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"Item"} },
	 *    views = { "default", "item" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/item", name="apifile-jstree-getitems")
	 */
	public function getItemsAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		return $serviceEntities->getRepository(Item::class)->findAll();
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get root Items for Jstree",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"Item"} },
	 *    views = { "default", "item" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/root-item", name="apifile-jstree-getnesteditems")
	 */
	public function getRootItemsAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		return $serviceEntities->getRepository(Item::class)->findByRoot(true);
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get nested (but Directorys) Items for Jstree",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"Item"} },
	 *    views = { "default", "item" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/root-nested-item", name="apifile-jstree-getrootnesteditems")
	 */
	public function getRootNestedItemsAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		return $serviceEntities->getRepository(Item::class)->findBy(['root' => true, 'dirinterface' => false]);
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get Files for Jstree",
	 *    output = { "class" = File::class, "collection" = true, "groups" = {"File"} },
	 *    views = { "default", "item" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/file", name="apifile-jstree-getfiles")
	 */
	public function getFilesAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		return $serviceEntities->getRepository(File::class)->findAll();
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get root Files for Jstree",
	 *    output = { "class" = File::class, "collection" = true, "groups" = {"File"} },
	 *    views = { "default", "item" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/root-file", name="apifile-jstree-getrootfiles")
	 */
	public function getRootFilesAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		return $serviceEntities->getRepository(File::class)->findByRoot(true);
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get one Item",
	 *    output = { "class" = Item::class, "collection" = false, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/item/{id}", name="apifile-jstree-getitem")
	 */
	public function getItemAction($id, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		if($serviceEntities->isValidBddid($id, false)) return $serviceEntities->findByBddid($id);
		return $serviceEntities->getRepository(Item::class)->find($id);
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get one Directory",
	 *    output = { "class" = Directory::class, "collection" = false, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/directory/{id}", name="apifile-jstree-getdirectory")
	 */
	public function getDirectoryAction($id, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		if($serviceEntities->isValidBddid($id, false)) return $serviceEntities->findByBddid($id);
		return $serviceEntities->getRepository(Basedirectory::class)->find($id);
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get Directory's childs for Jstree",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/directory-childs/{id}", name="apifile-jstree-getdirectorychilds")
	 */
	public function getDirectoryChildsAction($id, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		if($serviceEntities->isValidBddid($id, false)) $id = $serviceEntities->getBddidEntityId($id);
		return $this->getCachedChildsOfDirectory($id, true);
	}

	protected function getCachedChildsOfDirectory($parentBasedirOrId, $refresh = false) {
		$serviceEntities = $this->get(serviceEntities::class);
		return $this->get(serviceCache::class)->getCacheSerialized(
			'JSTREE_childs_of_'.($parentBasedirOrId instanceOf Basedirectory ? $parentBasedirOrId->getId() : $parentBasedirOrId),
			function() use ($parentBasedirOrId, $serviceEntities) {
				$directory = $parentBasedirOrId instanceOf Basedirectory ? $parentBasedirOrId : $serviceEntities->getRepository(Basedirectory::class)->find($parentBasedirOrId);
				$directory->checkChildsPositions();
				return $directory->getChilds();
			},
			['groups' => ['jstree'], 'format' => 'json'], null, null, null, null, $refresh
		);
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Get tree representation of systemfile",
	 *    output = { "class" = Item::class, "collection" = true, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"jstree"})
	 * @Rest\Get("/jstree/systemfile/{path}", name="apifile-jstree-systemfile")
	 */
	public function getSystemfileAction($path, Request $request) {
		return serviceTools::getTreeAsArray(__DIR__.'/../../../../web/'.urldecode($path));
	}

	// /**
	//  * @ApiDoc(
	//  *    resource=true,
	//  *    section="File",
	//  *    description="Change Item name",
	//  *    output = { "class" = Item::class, "collection" = false, "groups" = {"Item"} },
	//  *    views = { "default", "file" }
	//  * )
	//  * @Rest\View(serializerGroups={"jstree"})
	//  * @Rest\Post("/jstree/rename/{id}", name="apifile-jstree-rename-item")
	//  */
	// public function renameItemAction($id, Request $request) {
	// 	$serviceEntities = $this->get(serviceEntities::class);
	// 	$item = $serviceEntities->getRepository(Item::class)->find($id);
	// 	if(!empty($item)) {
	// 		$name = $request->request->get('name');
	// 		$item->setName($name);
	// 		if($this->get('validator')->validate($item)) {
	// 			$serviceEntities->getEntityManager()->flush();
	// 		} else {
	// 			throw new Exception("Name is invalid!", 1);
	// 		}
	// 	}
	// 	return $item;
	// }

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Remove Item from Directory",
	 * )
	 * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
	 * @Rest\Delete("/jstree/item/{shortname}/{id}", name="apifile-jstree-item-delete")
	 */
	public function itemDeleteAction($shortname, $id, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		// $shortname = $request->request->get('shortname');
		// $id = $request->request->get('id');
		$classname = $serviceEntities->getClassnameByShortname($shortname);
		// $new_item->setFilledError('TEST');
		switch ($shortname) {
			case 'Nestlink':
				$nestlink = $serviceEntities->getRepository($classname)->find($id);
				if(!empty($nestlink)) {
					$this->get(serviceDirectory::class)->deleteChild($nestlink, true);
					$em->flush();
					// if($nestlink->isSymbolic()) {
					// 	// remove symbolic link only
					// 	$em->remove($nestlink);
					// 	$em->flush();
					// } else if($nestlink->isSolid()) {
					// 	// remove all links and softdelete Item
					// 	$item = $nestlink->getChild();
					// 	$item->removeParents();
					// 	$item->setSoftdeleted();
					// 	$em->flush();
					// }
					$this->getCachedChildsOfDirectory($id, true);
				} else {
					// throw new Exception("Item not found", 1);
				}
				break;
			default: // items
				throw new Exception("Error Processing Request: ".json_encode($shortname), 1);
				break;
		}
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Create Item",
	 *    output = { "class" = Item::class, "collection" = false, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"jstree","jstree_updated"})
	 * @Rest\Post("/jstree/item/create", name="apifile-jstree-item-create")
	 */
	public function itemCreateAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		$item = $request->request->all();
		if(!isset($item['classname'])) $item['classname'] = $serviceEntities->getClassnameByShortname($item['shortname']);
		$new_item = $serviceEntities->getEntityService($item['shortname'])->createNew();
		// $new_item->setFilledError('TEST');
		$parent = $serviceEntities->findByBddid($item['parent']);
		if(!empty($parent)) {
			$parent->addChild($new_item);
			$serviceEntities->compileSerializedEntity($item);
			$new_item->fillWithData($item);
			if($this->get('validator')->validate($item)) {
				$em->persist($new_item);
				$em->flush();
				$this->getCachedChildsOfDirectory($parent, true);
				// if(static::REFRESH_MENU) $this->get(serviceMenu::class)->refreshMenusCache();
			} else {
				$new_item->setFilledError($item['shortname'].' is invalid!');
			}
		} else {
			$new_item->setFilledError('Ce '.$item['shortname'].' doit être placé dans un dossier. Aucun dossier spécifié !');
		}
		return $new_item;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Create Item",
	 *    output = { "class" = Item::class, "collection" = false, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"jstree","jstree_updated"})
	 * @Rest\Post("/jstree/item/move", name="apifile-jstree-item-move")
	 */
	public function itemMoveAction(Request $request) {
		// throw new Exception('Test !', 1);
		$serviceEntities = $this->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		$data = $request->request->all();
		// if(!isset($data['node']['original']['classname'])) $data['node']['original']['classname'] = $serviceEntities->getClassnameByShortname($data['node']['original']['shortname']);
		$nestlink = $serviceEntities->getRepository(Nestlink::class)->find($data['node']['id']);
		// $item = $serviceEntities->getRepository($data['node']['original']['classname'])->find($data['node']['original']['regular_id']);
		if(empty($nestlink)) throw new Exception('Ce '.$data['node']['original']['shortname'].' n\'a pu être trouvé !', 1);
		$item = $nestlink->getChild();
		if($data['node']['original']['regular_id'] !== $item->getId()) throw new Exception('Le lien et l\'élément ne correspondent pas !', 1);
		if($data['parent'] === $data['old_parent']) {
			// just change position in same Basedirectory
			// $old_parent = $serviceEntities->getRepository(Basedirectory::class)->getChildByNestlink($data['parent']);
			$new_parent = $nestlink->getDirectory();
			// $this->getCachedChildsOfDirectory($new_parent, true);
			// if($data['position'] !== $data['old_position']) $item->setPosition($data['position'], $nestlink);
			if($data['position'] !== $data['old_position']) $nestlink->setPosition($data['position']);
		} else {
			$new_parent_Nl = preg_match('/^\\d+$/', $data['parent']) ? $serviceEntities->getRepository(Basedirectory::class)->find($data['parent']) : $serviceEntities->getRepository(Nestlink::class)->find($data['parent']);
			if(empty($new_parent_Nl)) throw new Exception('Le nouveau parent de ce '.$data['node']['original']['shortname'].' n\'a pu être trouvé !', 1);
			// if($new_parent->checkChildsPositions()) {
				// $em->flush();
			// }
			$new_parent = $new_parent_Nl instanceOf Nestlink ? $new_parent_Nl->getDirectory() : $new_parent_Nl;
			$new_parent->addChild($item);
			if(!$new_parent->isLastResultSuccess()) throw new Exception('Le nouveau parent a rejeté ce '.$data['node']['original']['shortname'].' !', 1);
			// $this->getCachedChildsOfDirectory($new_parent, true);
			$item->setPosition($data['position']);
		}
		if($this->get('validator')->validate($item)) {
			$em->flush();
			$this->getCachedChildsOfDirectory($new_parent, true);
			// if(static::REFRESH_MENU) $this->get(serviceMenu::class)->refreshMenusCache();
		} else {
			$item->setFilledError($data['node']['original']['shortname'].' is invalid!');
			throw new Exception($data['node']['original']['shortname'].' is invalid!', 1);
		}
		return $item;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Create Item",
	 *    output = { "class" = Item::class, "collection" = false, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"jstree","jstree_updated"})
	 * @Rest\Post("/jstree/item/paste", name="apifile-jstree-item-paste")
	 */
	public function itemPasteAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		$parent = $request->request->get('parent');
		$child = $request->request->get('child');
		// if(!isset($data['node']['original']['classname'])) $data['node']['original']['classname'] = $serviceEntities->getClassnameByShortname($data['node']['original']['shortname']);
		$parent = $serviceEntities->getRepository(Basedirectory::class)->find($parent['original']['regular_id']);
		$child = $serviceEntities->getRepository(Item::class)->find($child['original']['regular_id']);
		if(!empty($parent) && !empty($child)) {
			$parent->addChild($child);
			if($parent->isLastResultError()) throw new Exception('Le nouveau parent a rejeté ce '.$child->getShortname().' !', 1);
			$this->getCachedChildsOfDirectory($parent, true);
			$em->flush();
			// if(static::REFRESH_MENU) $this->get(serviceMenu::class)->refreshMenusCache();
		} else {
			throw new Exception("Une erreur s'est produite.", 1);
		}
		return $child;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="File",
	 *    description="Update and create Items",
	 *    output = { "class" = Item::class, "groups" = {"Item"} },
	 *    views = { "default", "file" }
	 * )
	 * @Rest\View(serializerGroups={"jstree","jstree_updated"})
	 * @Rest\Post("/jstree/post/items", name="apifile-jstree-post-items")
	 */
	public function postItemsAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$em = $serviceEntities->getEntityManager();
		$items = $request->request->all();
		$oneItem = false;
		// if not array of items but just one item...
		if(isset($items['classname'])) {
			$items = [$items];
			$oneItem = true;
		}
		foreach ($items as $key => $item) {
			$id = null;
			if(isset($item['regular_id'])) {
				$id = $item['regular_id'];
			} else if(isset($item['id'])) {
				if(preg_match('/\\d+/', $item['id'])) $id = $item['id'];
				if(preg_match('/^\\d+_\\w+_\\d+$/', $item['id'])) {
					$expl = preg_split('/_/', $item['id']);
					$id = end($expl);
				}
				$id = intval($id);
				if($id <= 0) $id = null;
			}
			if(null === $id) {
				// new Item
				if(!isset($item['classname'])) $item['classname'] = $serviceEntities->getClassnameByShortname($item['shortname']);
				$new_item = $serviceEntities->getEntityService($item['shortname'])->createNew();
				$serviceEntities->compileSerializedEntity($item);
				$new_item->fillWithData($item);
				if($this->get('validator')->validate($item)) {
					$em->persist($new_item);
					$em->flush();
				} else {
					$new_item->setFilledError($item['shortname'].' is invalid!');
				}
				$items[$key] = $new_item;
			} else {
				// update Item
				$update_item = $serviceEntities->getRepository(Item::class)->find($id);
				// echo('<pre><h3>n°'.$key.' = '.$update_item->getShortname().' #'.$update_item->getId().' '.(empty($update_item) ? 'NOT FOUND' : 'FOUND').'</h3>');var_dump($item);die('</pre>');
				$serviceEntities->compileSerializedEntity($item);
				$update_item->fillWithData($item);
				if($this->get('validator')->validate($item)) {
					$em->flush();
					// if(static::REFRESH_MENU) $this->get(serviceMenu::class)->refreshMenusCache();
				} else {
					$update_item->setFilledError($item['shortname'].' is invalid!');
				}
				$items[$key] = $update_item;
			}
		}
		return $oneItem ? reset($items) : $items;
	}




}
