<?php
namespace ModelApi\FileBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
// BaseBundle
use ModelApi\BaseBundle\Entity\Facticee;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceUserrequest;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Tempfile;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\serviceItem;
// use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\Entreprise;
// PagewebBundle
use ModelApi\PagewebBundle\Service\TwigDatabaseLoader;
// BinaryBundle
use ModelApi\BinaryBundle\Service\serviceBinaryFile;

use ModelApi\DevprintsBundle\Service\DevTerminal;

use \Exception;
use \DateTime;

class ItemSubscriber implements EventSubscriber {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	// protected $FileManager;
	protected $clearTwigCache;
	protected $items_to_update;


	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->clearTwigCache = false;
		$this->items_to_update = [];
		return $this;
	}


	/**
	 * Master running Events
	 */
	public function getSubscribedEvents() {
		return array(
			BaseAnnotation::postNew,
			// BaseAnnotation::preCreateForm,
			// BaseAnnotation::preUpdateForm,
			// Events::postLoad,
			Events::prePersist,
			// Events::postPersist,
			// Events::preUpdate,
			// Events::postUpdate,
			Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			Events::onFlush,
			Events::postFlush,
			// Events::onClear,
		);
	}


	public function postNew(LifecycleEventArgs $args) {
		$item = $args->getEntity();
		if($item instanceOf Item && $item instanceOf NestedInterface && !($item instanceOf Basedirectory)) $this->container->get(serviceBasedirectory::class)->checkDirectorys($item);
	}

	public function prePersist(LifecycleEventArgs $args) {
		$item = $args->getEntity();
		if($item instanceOf Tempfile || $item instanceOf Facticee) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): can not persist ".get_class($item)." object!", 1);
		if($item instanceOf Item) {
			// Attach Fileformat
			if($item instanceOf File) {
				$fileversion = $item->getCurrentVersion();
				if($fileversion instanceOf Fileversion) {
					$this->container->get(serviceBinaryFile::class)
						->setBaseDeclinations($fileversion, false)
						->AttachFileformat($fileversion)
						;
					$item->checkVersion();
				}
			}
		}
		return $this;
	}

	public function PreRemove(LifecycleEventArgs $args) {
		// Clear twig cache
		$item = $args->getEntity();
		if($item instanceOf Item) $item->removeAll($args);
		return $this;
	}

	public function onFlush(OnFlushEventArgs $args) {
		$em = $args->getEntityManager();
		$uow = $em->getUnitOfWork();
		$this->clearTwigCache = false;
		// BEGIN: Update items paths
		foreach ($uow->getScheduledEntityInsertions() as $item) {
			// DO NOT PERSIST MODELS!!!
			if(isset($item->_is_model)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this entity ".$item->getShortname()." ".json_encode($item->getName())." is a MODEL, so can not be persisted in database!", 1);
			// Twig
			if($item instanceOf Twig && $item->changedCurrentVersion()) $this->clearTwigCache = true;
			// TwgTranslations
			if(isset($item->_tm)) $item->_tm->onFlushActions($args);
			// adminvalidated
			if(method_exists($item, 'isAdminvalidated') && !$item->isAdminvalidated()) {
				$this->container->get(serviceUserrequest::class)->sendUserrequest($item);
			}
			/*if($item instanceOf Item && !$item->isValidOwners()) {
				DevTerminal::EOL();
				DevTerminal::Info('Owners of '.$item->getShortname().' '.$item->getName());
				foreach ($item->getOwners() as $owner) {
					DevTerminal::Success('- '.json_encode($item->getId()).'/'.$item->getName().' --- '.$owner->getId().'/'.$owner->getName());
				}
				DevTerminal::Info('OK');
				die();
			}*/
		}
		foreach ($uow->getScheduledEntityUpdates() as $item) {
			// Twig
			if($item instanceOf Twig && $item->changedCurrentVersion()) $this->clearTwigCache = true;
			// TwgTranslations
			if(isset($item->_tm)) $item->_tm->onFlushActions($args);
			// adminvalidated
			if(method_exists($item, 'isAdminvalidated') && !$item->isAdminvalidated()) {
				$this->container->get(serviceUserrequest::class)->sendUserrequest($item);
			}
		}
		foreach ($uow->getScheduledEntityDeletions() as $item) {
			if($item instanceOf Twig) $this->clearTwigCache = true;
		}
		return $this;
	}

	public function postFlush(PostFlushEventArgs $args) {
		if($this->clearTwigCache) $this->container->get(TwigDatabaseLoader::class)->clearTwigCache(null, 'Action while postFlush Twig/Pageweb.');
		$this->clearTwigCache = false;
		return $this;
	}




}