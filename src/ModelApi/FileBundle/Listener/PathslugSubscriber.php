<?php
namespace ModelApi\FileBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Common\Collections\ArrayCollection;
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\serviceItem;
// use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// PagewebBundle
use ModelApi\PagewebBundle\Service\TwigDatabaseLoader;
// BinaryBundle
use ModelApi\BinaryBundle\Service\serviceBinaryFile;

use \Exception;

class PathslugSubscriber implements EventSubscriber {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	// protected $FileManager;
	protected $clearTwigCache;
	protected $items_to_update;


	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		// $this->clearTwigCache = false;
		$this->items_to_update = [];
		return $this;
	}


	/**
	 * Master running Events
	 */
	public function getSubscribedEvents() {
		return array(
			// BaseAnnotation::postNew,
			// BaseAnnotation::preCreateForm,
			// BaseAnnotation::preUpdateForm,
			// Events::postLoad,
			// Events::prePersist,
			// Events::postPersist,
			// Events::preUpdate,
			// Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			Events::onFlush,
			// Events::postFlush,
			// Events::onClear,
		);
	}



	public function onFlush(OnFlushEventArgs $args) {
		$em = $args->getEntityManager();
		$uow = $em->getUnitOfWork();

		// BEGIN: Update items paths
		$this->items_to_update = [];
		foreach ($uow->getScheduledEntityInsertions() as $item) {
			if($item instanceOf Item) $this->items_to_update[spl_object_hash($item)] = $item;
		}

		foreach ($uow->getScheduledEntityUpdates() as $item) {
			if($item instanceOf Item) {
				$changeset = $uow->getEntityChangeSet($item);
				$changes = array_intersect(['name','slug','updated'], array_keys($changeset));
				if(count($changes) > 0) $this->items_to_update[spl_object_hash($item)] = $item;
			}
		}

		usort($this->items_to_update, function($a, $b) {
			if($a->getLvl() == $b->getLvl()) return 0;
			return $a->getLvl() > $b->getLvl() ? 1 : -1;
		});

		if(count($this->items_to_update)) {
			// echo('<h3>On flush</h3>');
			// echo('<ul>');
			foreach ($this->items_to_update as $item) {
				// $item->onFlushItemdata($args);
				$item->updatePaths(true, true);
				// echo('<li>Lvl '.$item->getLvl().' > '.$item->getShortname().' <span style="color: red;">'.json_encode($item->getSlug()).'</span> / '.json_encode($item->getName()).' / pathslug '.json_encode($item->getPathslug()).' valid: '.json_encode($item->isValidPathslug(true)).'</li>');

				// if(!$item->isValid()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): item ".$item->getShortname()." ".json_encode($item->getName())." is not valid!", 1);
				if(!$item->isValidPathslug(true)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): item ".$item->getShortname()." ".json_encode($item->getName())." <u>pathslug</u> is not valid!", 1);
				// if(!$item->checkDirparents()) DevTerminal::launchException('<<<!!!!!>>> Errors: found duplicates dirparents for '.$item->getShortname().' '.json_encode($item->getName()).'!', __LINE__, __METHOD__);
				// if($item instanceOf Basedirectory && !$item->checkDirchilds()) DevTerminal::launchException('<<<!!!!!>>> Errors: found duplicates dirchilds for '.$item->getShortname().' '.json_encode($item->getName()).'!', __LINE__, __METHOD__);

				$uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($item)), $item);
				// $uow->computeChangeSet($em->getClassMetadata(get_class($item)), $item);
				// $treatedItemPaths = $this->container->get(serviceItem::class)->updateItemPaths($item);
				// foreach ($treatedItemPaths as $treatedItemPath) {
				// 	$uow->computeChangeSet($em->getClassMetadata(get_class($treatedItemPath)), $treatedItemPath);
				// }
			}
			// echo('</ul>');
			// die();
			// $uow->computeChangeSets();
		}

		return $this;
	}





}