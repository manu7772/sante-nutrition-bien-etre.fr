<?php
namespace ModelApi\FileBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Directory;

// use \ReflectionClass;

class DirectoryAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Directory::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}