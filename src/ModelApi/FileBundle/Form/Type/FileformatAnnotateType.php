<?php
namespace ModelApi\FileBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Fileformat;

// use \ReflectionClass;

class FileformatAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Fileformat::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}