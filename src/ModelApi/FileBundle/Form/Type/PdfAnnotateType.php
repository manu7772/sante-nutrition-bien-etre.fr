<?php
namespace ModelApi\FileBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Pdf;

// use \ReflectionClass;

class PdfAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Pdf::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}