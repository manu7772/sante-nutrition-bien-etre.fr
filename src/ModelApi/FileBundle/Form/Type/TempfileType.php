<?php
namespace ModelApi\FileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Form\Util\StringUtil;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyAccess\createPropertyAccessorBuilder;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\ORM\Mapping\Column;

use ModelApi\AnnotBundle\Annotation\AddModelTransformer;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
// use ModelApi\BaseBundle\Form\Type\LocaltextCreateType;
// use ModelApi\BaseBundle\Form\Type\LocaltextareaEditType;
// use ModelApi\BaseBundle\Form\Type\switcheryType;
// use ModelApi\BaseBundle\Form\Type\ColorpickerType;
use ModelApi\BaseBundle\Form\Type\SummernoteType;
use ModelApi\BaseBundle\Form\Type\TransTextType;
use ModelApi\BaseBundle\Form\Type\TransTextareaType;
use ModelApi\BaseBundle\Form\Type\TransSummernoteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType as OriginalEntityType;
use ModelApi\BaseBundle\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as OriginalChoiceType;
use ModelApi\BaseBundle\Form\Type\ChoiceType;

// FileBundle
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Tempfile;

// use \ReflectionClass;

class TempfileType extends AbstractType {

	const ENTITY_CLASSNAME = Tempfile::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$entity = $builder->getData();
		$builder
			->add('inputname', TextType::class, array(
				'label' => 'Nom',
			))
			->add('uploadFile', FileType::class, array(
				'label' => 'Fichier',
				'multiple' => false,
				'attr' => ['accept' => $entity->getValidContentTypes(false)],
			))
			->add('tempParent', HiddenType::class, array(
				'required' => true,
			))
			->add('submit', SubmitType::class, array(
				'label' => 'Enregistrer',
				'attr' => array('class' => "btn btn-md btn-info btn-block")
			))
		;
	}


	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			// 'data_class' => static::ENTITY_CLASSNAME,
			'method' => 'POST',
			'action' => 'wsa_file_post'
		));
	}

	// /**
	//  * {@inheritdoc}
	//  */
	// public function getBlockPrefix() {
	// 	return $this->name;
	// }

	// /**
	//  * @return string
	//  */
	// public function getName() {
	// 	return $this->name;
	// }


}