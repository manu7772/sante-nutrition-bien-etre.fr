<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Service\serviceItem;

// use \DateTime;
use \ReflectionClass;

class serviceImage extends serviceItem {

	const ENTITY_CLASS = Image::class;

	// /**
	//  * Get new entity
	//  * @param array $options = []
	//  * @param callable $closure = null
	//  * @return entity | null
	//  */
	// public function createNew($options = [], $closure = null) {
	// 	return $this->serviceEntities->createNew(static::ENTITY_CLASS, $options, $closure);
	// }


	// public function findOrphanImages($asArray = false) {
	// 	return $this->serviceEntities->getRepository(Image::class)->findOrphanImages($asArray);
	// }


}