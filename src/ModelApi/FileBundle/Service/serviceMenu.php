<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use JMS\Serializer\SerializationContext;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Service\serviceBasedirectory;

use ModelApi\AnnotBundle\Annotation\CachedChilds;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \DateTime;
use \ReflectionClass;

class serviceMenu extends serviceBasedirectory {

	const ENTITY_CLASS = Menu::class;
	const PATH_POSTFIX = 'Path';
	// const GLOBAL_CHILDS_CACHE_NAME = 'cache.menu';
	const FSCACHE_NAME = "cached_menu";
	// const USE_BDD_CACHE = true;
	// const CACHE_ONLY_ROOT_MENU = true;

	public function __construct(ContainerInterface $container) {
		parent::__construct($container);
		return $this;
	}


	/*************************************************************************************************/
	/*** SFCRON TASK
	/*************************************************************************************************/

	public function sfcron_task(DateTime $start, $force = false) {
		if($force || preg_match('/5$/', $start->format('i'))) {
			$this->refreshMenusCache();
			return ['Refresh menus data' => true];
		}
		return ['Refresh menus data' => false];
	}


	public function refreshMenusCache() {
		// $rootMenus = $this->getRepository(static::ENTITY_CLASS)->findBy(['rootmenu' => 1]);
		$rootMenus = $this->getRepository(static::ENTITY_CLASS)->findAll(); // Let Annotation decides if only root menus or not
		foreach ($rootMenus as $menu) $menu->getCachedchilds(true);
		$this->container->get(serviceFlashbag::class)->addFlashToastr('success', 'Tous menus mis à jour.');
		return $this;
	}





}