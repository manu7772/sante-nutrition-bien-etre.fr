<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\FileManager;

// use \DateTime;
use \ReflectionClass;

class serviceFileversion implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Fileversion::class;

	protected $container;
	protected $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}

	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString() {
		$class = new ReflectionClass(static::class);
		return $class->getShortName();
	}

	public function getEntityManager() {
		return $this->serviceEntities->getEntityManager();
	}

	public function getRepository() {
		return $this->serviceEntities->getRepository(static::ENTITY_CLASS);
	}

	public function getModel($classname = null, $asEntityIfPossible = false) {
		if(null === $classname) $classname = static::ENTITY_CLASS;
		return $this->container->get(serviceEntities::class)->getModel($classname, $asEntityIfPossible);
	}

	/**
	 * Get new entity
	 * @param array $options = []
	 * @param callable $closure = null
	 * @return entity | null
	 */
	public function createNew($options = [], $closure = null) {
		return $this->serviceEntities->createNew(static::ENTITY_CLASS, $options, $closure);
	}


	/**
	 * Replace slug requests by id in code
	 * @param string | Fileversion $entity
	 * @param boolean $modifyEntity = true
	 * @return array
	 */
	public function checksluglinks($entity, $modifyEntity = true, $getHtmlMarks = false) {
		if(!is_string($entity)) {
			if(!($entity instanceOf Fileversion) || $entity->getFileformat()->getMediaType() !== 'text') return ['code' => null, 'found' => 0, 'result' => false];
			$code = $getHtmlMarks ? htmlentities($entity->getBinaryfile()) : $entity->getBinaryfile();
		} else {
			$code = $getHtmlMarks ? htmlentities($entity) : $entity;
		}
		// Check code...
		$found = 0;
		$serviceEntities = $this->serviceEntities;
		$pattern = "/(['\"])([a-z0-9][-a-z0-9]{1,128}[a-z0-9])(['\"])/";
		$code = preg_replace_callback($pattern, function($matches) use ($serviceEntities, $getHtmlMarks, &$found) {
			$search = $serviceEntities::isValidBddid($matches[2], false) ? $serviceEntities->findByBddid($matches[2]) : $serviceEntities->getRepository(Item::class)->findOneBySlug($matches[2]);
			if($getHtmlMarks) {
				// Get HTML code for found slugs dump
				if(empty($search)) return $matches[1].'<i style="color:darkblue;">'.$matches[2].'</i>'.$matches[3];
				$found++;
				return '<i style="color:red;" title="'.$matches[2].'">'.$search->getId().'</i>';
			}
			if(empty($search)) return $matches[1].$matches[2].$matches[3];
			$found++;
			return $search->getId();
		}, $code, -1);
		// Flush
		if($modifyEntity && !$getHtmlMarks && $entity instanceOf Fileversion) {
			$entity->setBinaryfile($code);
			$this->getEntityManager()->flush();
			// $this->container->get(serviceFlashbag::class)->addFlashToastr('success', 'Les recherches par slugs/Bddid dans le code ont été modifiés et remplacés par des recherches par ID.');
		}
		return ['code' => $code, 'found' => $found, 'result' => true];
	}


}