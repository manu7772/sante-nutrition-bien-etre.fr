<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;
// use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Inflector\Inflector;

use Doctrine\ORM\Event\PostFlushEventArgs;

// AnnotBundle
use ModelApi\AnnotBundle\Annotation\JoinedOwnDirectory;
use ModelApi\AnnotBundle\Service\serviceAnnotation;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Repository\ItemRepository;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceDirectory;
use ModelApi\FileBundle\Annotation\BasedirQuery;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\OwnerTierInterface;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

// use \DateTime;
use \ReflectionClass;
use \ReflectionProperty;
use \ReflectionMethod;
use \Exception;
use \DateTime;

class serviceBasedirectory extends serviceItem {

	const ENTITY_CLASS = Basedirectory::class;
	const PATH_POSTFIX = 'Path';
	const CACHE_NAME = 'serviceBasedirectory';
	const DEFAULT_TYPEDIR = ['typedir' => 'public', 'dirstatic' => true];

	// SUCCESS
	const FILE_SUCCESS = 0; // Success
	const FILE_NULL_SUCCESS = 1; // nothing done but success
	// ERRORS
	// 1XX CHILD
	const FILE_ERROR_HAS_CHILD = 100; // Le parent possède déjà cet enfant
	const FILE_ERROR_CLASS_CHILD = 101; // Classe de l'enfant incorrecte
	const FILE_ERROR_IS_HIMSELF = 102; // L'enfant proposé est le Basedirectory lui-même
	const FILE_ERROR_IS_UNAVAILABLE = 103; // L'enfant proposé n'est pas éligible pour ce Directory
	const FILE_ERROR_NOT_CHILDEABLE = 104; // L'enfant proposé ne peut pas être enfant
	// 3XX
	const FILE_ERROR_ROOT_DIRECTORY = 300; // Directory root (drive ou system) can not have parent
	// 5XX
	const FILE_ERROR_UNKNOWN = 500; // Erreur indéterminée

	// TYPEDIR
	const TYPEDIR_REGULAR = 'regular';
	const TYPEDIR_PUBLIC = 'public';
	const TYPEDIR_DRIVE = 'drive';
	const TYPEDIR_MENU = 'menu';
	const TYPEDIR_SYSTEM = 'system';
	const TYPEDIR_SUB_SYSTEM = 'system_@';

	// TYPELINKS
	const TYPELINK_UNDEFINED = 'undefined';		// undefined
	const TYPELINK_HARDLINK = 'hardlink'; 		// Hard link
	const TYPELINK_SYMLINK = 'symlink';			// Symbolic link
	const TYPELINK_PROCESSED = 'processed';		// if is Query or other
	const TYPELINK_ERROR = 'error';				// error
	const TYPELINK_REMOVE = 'remove';			// ACTION --> for remove
	const TYPELINK_REPLACE = 'replace';			// ACTION --> for replace

	const ROOT_NAMES = ['drive','system'];
	const ROOT_NAME_WITH_OWNERNAME = false;		// Root name contains owner's name ?

	const DEFAULT_PAGE_LENGTH = 25;


	protected $serviceKernel;
	protected $treatedOldDirectorys;
	// protected $createdDirectorysSoNeedFlush;

	public function __construct(ContainerInterface $container) {
		parent::__construct($container);
		$this->serviceKernel = $this->container->get(serviceKernel::class);
		$this->treatedOldDirectorys = new ArrayCollection();
		// $this->createdDirectorysSoNeedFlush = new ArrayCollection();
		return $this;
	}

	// public function isCreatedDirectorysSoNeedFlush() {
	// 	return $this->createdDirectorysSoNeedFlush->count() > 0;
	// }

	// public function FilterResponseEvent(FilterResponseEvent $event) {
	// 	if($this->isCreatedDirectorysSoNeedFlush()) {
	// 		// Created directorys
	// 	}
	// }

	// /**
	//  * Master running Events
	//  */
	// public function getSubscribedEvents() {
	// 	return array(
	// 		// BaseAnnotation::postNew,
	// 		// BaseAnnotation::preCreateForm,
	// 		// BaseAnnotation::preUpdateForm,
	// 		// Events::postLoad,
	// 		// Events::prePersist,
	// 		// Events::postPersist,
	// 		// Events::preUpdate,
	// 		// Events::postUpdate,
	// 		// Events::preRemove,
	// 		// Events::postRemove,
	// 		// Events::loadClassMetadata,
	// 		// Events::onClassMetadataNotFound,
	// 		// Events::preFlush,
	// 		// Events::onFlush,
	// 		Events::postFlush,
	// 		// Events::onClear,
	// 	);
	// }

	// public function postFlush(PostFlushEventArgs $args) {
	// 	$this->createdDirectorysSoNeedFlush = new ArrayCollection();
	// }


	/*************************************************************************************/
	/*** JSTree functions
	/*************************************************************************************/

	public function deleteChild($parent, $flush = true, $forceDelete = false) {
		$em = $this->serviceEntities->parentManager();
		throw new Exception("Error line ".__LINE__." ".__METHOD__."(): obsolete!!! Please review code here!", 1);
		
		switch ($parent->getShortname()) {
			case 'Nestlink':
				if($parent->isSymbolic()) {
					// remove symbolic link only
					$em->remove($parent);
					if($flush) $em->flush();
				} else if($parent->isSolid()) {
					// remove all links and softdelete Item
					$item = $parent->getChild();
					$item->removeParents();
					if($forceDelete) {
						$em->remove($item);
					} else {
						$item->setSoftdeleted();
					}
					if($flush) $em->flush();
				}
				break;
			default:
				# code...
				break;
		}
		return $this;
	}


	/*************************************************************************************/
	/*** LINKDATA STRUCTURE
	/*************************************************************************************/

	public static function getItemStructure() {
		return array_merge(static::ITEM_STRUCTURE, static::ITEM_FILEDS_STRUCTURE);
	}

	public static function getItemFieldsStructure() {
		return static::ITEM_FILEDS_STRUCTURE;
	}


	/*************************************************************************************/
	/*** TYPELINKS
	/*************************************************************************************/

	/**
	 * Get list of typelinks
	 * @param array <string>
	 */
	public static function getTypelinks() {
		return [
			0 => static::TYPELINK_UNDEFINED,
			1 => static::TYPELINK_HARDLINK,
			2 => static::TYPELINK_SYMLINK,
			3 => static::TYPELINK_PROCESSED,
			4 => static::TYPELINK_ERROR,
			5 => static::TYPELINK_REMOVE,
			6 => static::TYPELINK_REPLACE,
		];
	}

	/*************************************************************************************/
	/*** DOUBLONS
	/*************************************************************************************/

	public function findDoublons($moreInformations = false, $id = null) {
		$doublons = $this->getRepository()->findDoublons($moreInformations, $id);
		if('entities' === $moreInformations) return new ArrayCollection($doublons);
		$sorteds = [];
		foreach ($doublons as $dir) {
			$sorteds[$dir['owner_id'].'/'.$dir['pathslug'].'/'.$dir['name']] ??= [];
			$sorteds[$dir['owner_id'].'/'.$dir['pathslug'].'/'.$dir['name']][$dir['id']] = $dir;
		}
		return $sorteds;
	}

	public function resolveDoublons(Item $item = null) {
		$repository = $this->getRepository();
		if(empty($item)) {
			$result = true;
			$doublons = $this->findDoublons(true);
			foreach ($doublons as $pathid => $dbls) if(count($dbls) > 1) {
				$first = reset($dbls);
				$doublon = $repository->find($first['id']);
				// echo('<div>- Resolve doublon '.$doublon->getShortname().' '.json_encode($doublon->getName()).' Id#'.json_encode($doublon->getId()).' : '.json_encode(array_keys($dbls)).'</div>');
				$test = $this->resolveDoublons($doublon);
				// echo('<div>- Resolve doublon '.$doublon->getShortname().' '.json_encode($doublon->getName()).' Id#'.json_encode($doublon->getId()).' : result is '.json_encode($test).'</div>');
				if(!$test) $result = false;
				unset($doublon);
			}
			// die('<h2>END of '.count($doublons).'</h2>');
			return $result;
		}
		$doublons = new ArrayCollection($this->getRepository()->findDoublons('entities', $item));
		if($doublons->count() < 2 || !$doublons->contains($item)) return false;
		$doublons = $doublons->filter(function($doublon) use ($item) {
			return $doublon !== $item;
		});
		$em = $this->getEntityManager();
		// $common_parent = $item->getParent();
		// if(!($common_parent instanceOf Basedirectory)) throw new Exception("Error Processing Request", 1);  - Resolve doublon Directory "@drive" Id#20 : [20,95,352,453]

		foreach ($doublons as $key => $doublon) {
			// $doublon->setToRemove();
			$dirparents = $doublon->getDirparents();
			// $parent = $doublon->getParent();
			// if(!$dirparents->contains($parent)) throw new Exception("Doublon's dirparents do not contain parent ".$parent->getShortName()." ".json_encode($parent->getName()).".", 1);
			
			// if($doublon->getParent() !== $common_parent) throw new Exception("This doublon has a wrong parent", 1);
			// $doublon->removeDirparents(true);
			// foreach ($dirparents as $dirparent) {
				// if($dirparent->hasChild($doublon)) throw new Exception("Parent still have doublon as child.", 1);
			// }
			// if(!$doublon->removeParent()) throw new Exception("Could not remove parent.", 1);
			// transfert solid childs to main item
			if($doublon->isOnlyChilds()) {
				foreach ($doublon->getSolidChilds(false) as $child) {
					// $child->changePlacement($item, null, false);
					// $child->setOwner($item->getOwner(), false);
					$child->removeParent(false);
					if(!$child->setParent($item, -1, false)) throw new Exception("Could not change parent of child.", 1);
					if($child->hasParent($doublon)) throw new Exception("Child has already deleted parent.", 1);

					// $child->removeDirparent($doublon);
					// if($child->getParent() === $doublon) throw new Exception("Child has already deleted parent", 1);
					// if($child->hasDirparent($doublon)) throw new Exception("Child has already deleted dirparent", 1);
				}
				foreach ($doublon->getChilds() as $child) {
					if(!$child->removeDirparent($doublon)) throw new Exception("Child has already deleted dirparent.", 1);
				}
				if($doublon->hasChild()) {
					$child_names = '';
					foreach ($doublon->getChilds() as $_child) {
						$child_names .= '- '.$_child->getName().'<br>';
					}
					throw new Exception("This doublon still have ".$doublon->getChilds()->count()." child(s) :<br>".$child_names, 1);
				}
			}

			// remove all rest symlink childs
			$childs = $doublon->getChilds();
			$doublon->removeChilds();
			foreach ($childs as $child) {
				if($child->hasDirparent($doublon)) throw new Exception("Child should not have this dirparent", 1);
				if($child->hasParent($doublon)) throw new Exception("Child should not have this parent", 1);
			}
			if($doublon->hasChild()) throw new Exception("Les éléments de ".$doublon->getShortName()." ".json_encode($doublon->getName())." n'ont pas pu être déplacés.", 1);
			// if($doublon->hasParent()) throw new Exception("Le ".$doublon->getShortName()." ".json_encode($doublon->getName())." n'a pu être retiré de son dossier.", 1);
			// if($doublon->hasDirparent()) throw new Exception("Le ".$doublon->getShortName()." ".json_encode($doublon->getName())." n'a pu être retiré de tous ses dossiers.", 1);
			// $em->remove($doublon);
		}
		// $item->setParent($common_parent, null, true);
		// if(!$common_parent->hasChild($item)) throw new Exception("Mains item has not common parent as parent", 1);
		// $common_parent->addChild($item, serviceBasedirectory::TYPELINK_HARDLINK);
		try {
			$em->flush();
		} catch (Exception $e) {
			
		}
		$em->clear();
		// Then remove doublons
		$doublons = $doublons->toArray();
		foreach ($doublons as $key => $doublon) {
			$doublons[$key] = $doublon->getId();
			unset($doublon);
		}
		$doublons = $repository->findById($doublons);
		foreach ($doublons as $doublon) {
			$em->remove($doublon);
		}
		try {
			$em->flush();
		} catch (Exception $e) {
			
		}
		$em->clear();
		foreach ($doublons as $key => $doublon) unset($doublon);

		return true;
	}

	/*************************************************************************************/
	/*** LOST CHILDS
	/*************************************************************************************/

	public function findLostchilds($ids = null) {
		// options
		$options = [
			'lostchilds' => true,
			'typeResult' => ItemRepository::TYPE_RESULT_FACTICEE,
		];
		return array_filter($this->getRepository()->findAsFacticee(false, $ids, $options), function($facticee) {
			return !empty($facticee->getLostchilds());
		});
		// $directorys = !empty($id) ? $this->getRepository()->findById($id) : $this->getRepository()->findAll();
		// return array_filter($directorys, function($dir) {
		// 	$childs1 = $this->container->get(serviceItem::class)->getRepository()->findItemsByParent($dir, ['typeResult' => ItemRepository::TYPE_RESULT_FACTICEE]);
		// 	$childs2 = $dir->getContentDirectory(null, [], false, true);
		// 	$childs3 = $dir->getChildsAsFacticees();
		// 	return count($childs1) !== count($childs3) or count($childs2) !== count($childs3);
		// });
	}

	public function resolveLostchilds($ids = null) {
		$lostchilds = $this->findLostchilds($ids);
		foreach ($lostchilds as $key => $directory) {
			$realdir = $directory->getRealEntity();
			$realdir->tryResolveOwner();
			$lostchilds[$key] = $realdir;
			foreach ($directory->getLostchilds() as $child) {
				$realchild = $child->getRealEntity();
				$realchild->tryResolveOwner();
				$realdir->addChild($realchild, serviceBasedirectory::TYPELINK_HARDLINK);
			}
			$this->getEntityManager()->flush();
			$this->getEntityManager()->clear(); // IMPORTANT
		}
		return $lostchilds;
		// $directorys = $this->getRepository()->getIds();
		// if(!is_array($ids) && !empty($ids)) $ids = [$ids];
		// if(!empty($ids)) $directorys = array_intersect($ids, $directorys);
		// if(empty($directorys)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): list of id is empty!", 1);
		// // echo('<pre>'); var_dump($directorys); die('</pre>');
		// foreach ($directorys as $dir) {
		// 	$childs = $this->container->get(serviceItem::class)->getRepository()->findItemsByParent($dir, ['typeResult' => ItemRepository::TYPE_RESULT_ENTITY]);
		// 	foreach ($childs as $child) {
		// 		$dir->addChild($child, serviceBasedirectory::TYPELINK_HARDLINK);
		// 	}
		// }
		// $this->getEntityManager()->flush();
		// return $directorys;
	}


	public function getForCheckAsFacticee($ids = []) {
		// options
		$options = [
			'lostchilds' => true,
			'typeResult' => ItemRepository::TYPE_RESULT_FACTICEE,
		];
		// ALL
		$entities['entities'] = $this->getRepository()->findAsFacticee(false, $ids, $options);
		$entities['entities_count'] = count($entities['entities']);
		// LOST CHILDS
		$entities['lostchilds'] = array_filter($entities['entities'], function($facticee) {
			return !empty($facticee->getLostchilds());
		});
		$entities['lostchilds_count'] = count($entities['lostchilds']);
		// DOUBLONS
		$entities['doublons'] = $this->findDoublons(true);
		$entities['doublons_count'] = count($entities['doublons']);
		// Return all
		return $entities;
	}


	/*************************************************************************************/
	/*** REQUESTS
	/*************************************************************************************/

	public function findRootDirectorys() {
		return $this->getRepository()->findRootDirectorys();
	}

	public function findNotRootDirectorys() {
		return $this->getRepository()->findNotRootDirectorys();
	}



	// /**
	//  * Change typelink to name / Change to default if not found
	//  * @param mixed $typelink
	//  */
	// public static function typelinkNameOrDefault(&$askedTypelink) {
	// 	return in_array($askedTypelink, [static::TYPELINK_HARDLINK ,static::TYPELINK_SYMLINK]) ? $askedTypelink : static::TYPELINK_HARDLINK;
	// }

	public static function getRootName($rootname, $entity, $forInputname = false) {
		if(!in_array($rootname, static::ROOT_NAMES)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): root name parameter must be in ".json_encode(static::ROOT_NAMES).". Got ".json_encode(is_object($rootname) ? get_class($rootname) : $rootname)."!", 1);
		return static::ROOT_NAME_WITH_OWNERNAME || $forInputname ? '@'.$rootname.'_'.$entity->getNameOrTempname() : '@'.$rootname;
	}

	/**
	 * Extract base root name from path
	 * @param mexid $rootname (string or array of root names)
	 * @return string | null
	 */
	public static function extractRootType($rootname) {
		if(is_array($rootname)) $rootname = reset($rootname);
		$rootname = preg_replace('/^@?('.implode('|', static::ROOT_NAMES).')(.*)$/i', '$1', $rootname);
		if(!in_array($rootname, static::ROOT_NAMES)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not find root type by this name: ".json_encode(is_object($rootname) ? get_class($rootname) : $rootname)."!", 1);
		return $rootname;
	}

	/**
	 * Normalize path of directorys for $entity's files
	 * $pathname can be:
	 * 		- path string
	 * 		- null or empty (so use default root)
	 * 		- array <string>
	 * @param mixed &$pathname
	 * @param NestedInterface $ownerdir
	 * @param boolean $asArray = true
	 * @param boolean $defaultRoot = "drive"
	 */
	public static function normalizePath(&$pathname, NestedInterface $ownerdir, $asArray = true, $defaultRoot = "drive") {
		$defaultRoot = strtolower($defaultRoot);
		if(!in_array($defaultRoot, static::ROOT_NAMES)) $defaultRoot = 'drive';
		if(is_array($pathname)) static::pathArrayToString($pathname);
		if(empty(trim($pathname)) || !is_string($pathname)) $pathname = $defaultRoot;
		$pathname = preg_split('/(\\'.DIRECTORY_SEPARATOR.'+)/', $pathname, -1, PREG_SPLIT_NO_EMPTY);
		// Add root if not found
		if(!preg_match('/^@?('.implode('|', static::ROOT_NAMES).')/', $pathname[0])) {
			array_unshift($pathname, $defaultRoot);
		}
		$pathname[0] = preg_replace('/^@?('.implode('|', static::ROOT_NAMES).')(.*)$/i', '@$1'.(static::ROOT_NAME_WITH_OWNERNAME ? '_'.$ownerdir->getNameOrTempname() : ''), $pathname[0]);
		if(!$asArray) static::pathArrayToString($pathname);
		// echo('<pre>'); var_dump($pathname); die('</pre>');
	}

	public static function pathArrayToString(&$patharray) {
		$patharray = implode(DIRECTORY_SEPARATOR, $patharray);
	}

	// /**
	//  * Get System or Drive child Directory (use Repository, then relations)
	//  * @param NestedInterface $entity
	//  * @param string $pathname
	//  * @param boolean $createIfNotFound = true
	//  * @return array <'directory' => $directory, 'createds' = ArrayCollection <Basedirectory>>
	//  */
	// public function findOneByEntityAndPath(NestedInterface $entity, $pathname, $createIfNotFound = true) {
	// 	// static::normalizePath($pathname, $entity, true);
	// 	// if(!empty($entity->getId())) {
	// 		$directory = $this->getRepository()->findOneByEntityAndPath($pathname, $entity);
	// 		if($directory instanceOf Basedirectory) return $directory;
	// 	// }
	// 	return $this->getDirByPath($entity, $pathname, $createIfNotFound);
	// }

	public function getDoublons(NestedInterface $ownerdir, $pathname) {
		if(empty($ownerdir->getId())) return [];
		return $this->getRepository()->findByEntityAndPath($pathname, $ownerdir);
	}

	/**
	 * Get System or Drive child Directory (uses Repository query if possible, then path walk)
	 * @param NestedInterface $ownerdir
	 * @param string $pathname
	 * @param boolean $createIfNotFound = true
	 * @return Basedirectory
	 */
	public function getDirByPath(NestedInterface $ownerdir, $pathname, $createIfNotFound = true) {
		if(!empty($ownerdir->getId())) {
			// 1 - Try by query
			$directory = $this->getRepository()->findOneByEntityAndPath($pathname, $ownerdir);
			// $directorys = $this->getRepository()->findByEntityAndPath($pathname, $ownerdir);
			// if(count($directorys) > 1) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): on ".$ownerdir->getShortName()." ".json_encode($ownerdir->getNameOrTempname()).", with pathname ".json_encode($pathname)." gave ".count($directorys)." responses!", 1);
			// $directory = reset($directorys);
			// $directory = end($directorys);
			// if($directory instanceOf Basedirectory) echo("<h2>found directory by QUERY ".json_encode($pathname)." => ".$directory->getName()." with own path ".json_encode($directory->getPathname(true))."</h2>");
			if($directory instanceOf Basedirectory) return $directory;
		}
		// die("<h2 style='color: red;'>DID NOT find directory by QUERY ".json_encode($pathname)." on ".$ownerdir->getShortname()." ".json_encode($ownerdir->getName())."!</h2>");
		// 2 - Then try by path walk
		$directory = $this->getDirByPathWalk($ownerdir, $pathname);
		// if($directory instanceOf Basedirectory) echo("<h2>found directory by WALK ".json_encode($pathname)." => ".$directory->getName()." with own path ".json_encode($directory->getPathname(true))."</h2>");
		if($createIfNotFound && !($directory instanceOf Basedirectory)) {
			// Directory not found: create it			
			$directory = $this->constructDirectorysByPathname($ownerdir, $pathname);
		}
		// if($directory instanceOf Basedirectory) {
		// 	$directory->validityEntityReport(null, __LINE__, __METHOD__, true);
		// }
		return $directory;
	}

	// /**
	//  * Get System or Drive child Directory (uses Repository query if possible, then path walk)
	//  * @param NestedInterface $entity
	//  * @param string $pathname
	//  * @param boolean $createIfNotFound = true
	//  * @param serviceBasedirectory $serviceBasedirectory = null
	//  * @return Basedirectory
	//  */
	// public static function get_static_DirByPath(NestedInterface $entity, $pathname, $createIfNotFound = true, serviceBasedirectory $serviceBasedirectory = null) {
	// 	if($serviceBasedirectory instanceOf serviceBasedirectory) return $serviceBasedirectory->getDirByPath($entity, $pathname, $createIfNotFound);
	// 	return static::getDirByPathWalk($entity, $pathname);
	// }

	/**
	 * Get System or Drive child Directory (uses only path walk - DOES NOT use service/entity Repository)
	 * @param NestedInterface $entity
	 * @param string $pathname
	 * @return Basedirectory
	 */
	public static function getDirByPathWalk(NestedInterface $entity, $pathname) {
		static::normalizePath($pathname, $entity, true);
		$found = $directory = static::extractRootType($pathname) === 'system' ? $entity->getSystemDir() : $entity->getDriveDir();
		if($directory instanceOf Directory) {
			// echo('<pre><h3>'.__FUNCTION__.'(): find by path walk (Root type: '.static::extractRootType($pathname).') => found '.json_encode($directory->getName()).'!</h3>'); var_dump($pathname); echo('</pre>');
			// echo('<pre><h3>'.json_encode($directory->getName()).' DIRchilds :</h3><ul>'); foreach ($directory->getChildsForPathwalk() as $child) { echo('<li>'.$child->getName().'</li>'); } echo('</ul></pre>');
			// echo('<pre><h3>'.json_encode($directory->getName()).' childs :</h3><ul>'); foreach ($directory->getChildsForPathwalk() as $child) { echo('<li>'.$child->getName().'</li>'); } echo('</ul></pre>');
			// die('<p>ok</p>');
			// Search...
			while (is_string($name = next($pathname))) {
				// if($serviceBasedirectory instanceOf serviceBasedirectory) $directory->initItem($serviceBasedirectory);
				$childs = $directory->getChildsForPathwalk();
				$found = null;
				foreach ($childs as $child) {
					if($child instanceOf Basedirectory && $child->getName() === $name) {
						$found = $directory = $child;
						break;
					}
				}
				if(empty($found)) return null;
			}
			// echo('<pre><h3>'.__FUNCTION__.'(): find by path walk (Root type: '.static::extractRootType($pathname).') => found '.json_encode($directory->getName()).'!</h3>'); var_dump($pathname); echo('</pre>');
			return $directory;
		}
		return null;
	}

	/********************************************************************************************************************/
	/*** DIRECTORYS HIERARCHY TOOLS
	/********************************************************************************************************************/

	protected static function getPathsAndData($array, $typetree = null) {
		// return $array;
		$paths = [];
		foreach ($array as $name => $data) {
			if(preg_match('/^@?system/', $name)) {
				$typetree = 'system';
				$defaultValues = ['dirstatic' => true];
			} else if(preg_match('/^@?drive/', $name)) {
				$typetree = 'drive';
				$defaultValues = static::DEFAULT_TYPEDIR;
			} else {
				$typetree ??= 'drive';
				$defaultValues = $typetree === 'system' ? ['dirstatic' => true] : static::DEFAULT_TYPEDIR;
			}
			// $defaultValues = preg_match('/^@?(system|drive)/', $name) ? ['dirstatic' => true] : static::DEFAULT_TYPEDIR;
			$paths[$name] = $data['_values'] ?? $defaultValues;
			if(isset($data['_childs'])) {
				foreach (static::getPathsAndData($data['_childs'], $typetree) as $name2 => $data2) $paths[$name.DIRECTORY_SEPARATOR.$name2] = $data2;
			} else {
				$data = array_filter($data, function($key) { return preg_match('/^[^_].+/', $key); }, ARRAY_FILTER_USE_KEY);
				foreach (static::getPathsAndData($data, $typetree) as $name2 => $data2) $paths[$name.DIRECTORY_SEPARATOR.$name2] = $data2;
			}
		}
		return $paths;
	}

	/*************************************************************************************/
	/*** CHECK OWNER DIRECTORYS
	/*************************************************************************************/

	/**
	 * Get $entity schema of directorys
	 * @param mixed $entity
	 * @param boolean $refresh = false
	 * @return array <string>
	 */
	protected function getEntitySchemaDirectorys($entity, $refresh = false) {
		if($this->container->get(serviceKernel::class)->isDev()) $refresh = true;
		return $this->container->get(serviceCache::class)->getCacheData(
			'schemaDirectories_'.serviceClasses::getShortname($entity, true),
			function() use ($entity) {
				$shortnames = serviceClasses::getParentClasses($entity, true, true, true);
				if(count($shortnames)) {
					$entities_parameters = $this->container->getParameter('entities_parameters');
					foreach ($shortnames as $shortname) {
						if(!empty($entities_parameters[$shortname] ?? null) && !empty($entities_parameters[$shortname]['schema_directories'] ?? null)) {
							return $entities_parameters[$shortname]['schema_directories'];
						}
					}
				}
				return [];
			},
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
	}

	/**
	 * Get all directory paths for an $entity class (including Owned Directorys)
	 * @param mixed $entity
	 * @param boolean $refresh = false
	 * @return array <string>
	 */
	public function getAllSchemaDirectorys($entity, $refresh = false) {
		return $this->container->get(serviceCache::class)->getCacheData(
			'all_schemaDirectories_'.serviceClasses::getShortname($entity, true),
			function() use ($entity) {
				// 1. parameters
				$parametersPaths = $this->getEntitySchemaDirectorys($entity);
				$parametersPaths = empty($parametersPaths) ? [] : static::getPathsAndData($parametersPaths);
				// 2. JoinedOwnDirectory Annotations
				$annotations = serviceClasses::getPropertysAnnotation($entity, JoinedOwnDirectory::class, false);
				if(count($annotations)) {
					foreach ($annotations as $property => $annotation) $parametersPaths = array_merge(static::getPathsAndData($annotation->getHierarchizePath()), $parametersPaths);
				}
				return $parametersPaths;
			},
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
	}

	public function checkAttachedJoinedOwnDirectorys(NestedInterface $entity) {
		$annotations = serviceClasses::getPropertysAnnotation($entity, JoinedOwnDirectory::class, false);
		foreach ($annotations as $property => $annotation) {
			$dir = $this->getDirByPath($annotation->entity, $annotation->ownPath, true);
			$setter = $annotation->setter;
			if(empty($setter)) $setter = 'set'.ucfirst($property);
			$annotation->entity->$setter($dir);
		}
		return $this;
	}

	/**
	 * Check (and create if missing) path of directorys for $entity's property
	 * @param mixed $property (can be string, RelfexionProperty or JoinedOwnDirectory annotation)
	 * @param NestedInterface $entity = null (can be null only if $property is instance of JoinedOwnDirectory)
	 * @return array <'directory' => $directory, 'createds' = ArrayCollection <Basedirectory>>
	 */
	public function checkOwnDirectory($property, NestedInterface $entity = null) {
		if($property instanceOf ReflectionProperty) $property = $property->name;
		$annotation = $property instanceOf JoinedOwnDirectory ? $property : serviceClasses::getPropertyAnnotation($entity, $property, JoinedOwnDirectory::class, false);
		if($annotation instanceOf JoinedOwnDirectory) {
			return $this->getDirByPath($annotation->entity, $annotation->ownPath, true);
		}
		throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not find JoinedOwnDirectory annotation!", 1);
	}

	/**
	 * Create Directorys by path
	 * @param NestedInterface $entity
	 * @param mixed $pathname
	 * @return Basedirectory (found or created)
	 */
	public function constructDirectorysByPathname(NestedInterface $entity, $pathname) {
		if($entity->isDirectorysChecked() || $entity instanceOf Basedirectory) return null;
		// Control pathname
		// if(is_string($pathname) && !preg_match('/^@?('.static::ROOT_NAMES.')/', $pathname)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): root of path ".json_encode($pathname)." for ".$entity->getShortname()." ".json_encode($entity->getName())." is not valid! Expected beginnig by ".json_encode(static::ROOT_NAMES).".", 1);

		static::normalizePath($pathname, $entity, true);
		$defineValues = static::DEFAULT_TYPEDIR;
		// if(!$entity->isDefaultModeContext()) {
			// $names = array_keys($pathname);
			// DevTerminal::Info('<T5>- Search pathname: '.implode(' / ', $pathname));
		// }
		// echo('<pre><h3>Construct path directorys for '.$entity->getShortname().' '.json_encode($entity->getName()).'</h3>'); var_dump($pathname); die('</pre>');
		$this->serviceDirectory = $this->container->get(serviceDirectory::class);
		// Root
		$root = array_shift($pathname);
		$entityManager = $this->serviceEntities->getEntityManager();
		if(preg_match('/^@system/', $root)) {
			$sysOrDrivDir = $entity->getSystemDir();
			if(empty($sysOrDrivDir)) {
				$sysOrDrivDir = $this->serviceDirectory->createNew(null, function($new_dir) use ($entity) {
					$entity->setSystemDir($new_dir);
				});
				// $entity->setSystemDir($sysOrDrivDir);
				// $sysOrDrivDir->checkAndRepairValidity();
				$sysOrDrivDir->validityEntityReport('Created new SYSTEM '.$sysOrDrivDir->getShortname().' '.json_encode($sysOrDrivDir->getName()).' for '.$entity->getShortname().' '.json_encode($entity->getName()).'.', __LINE__, __METHOD__, true);
				// $entityManager->persist($sysOrDrivDir);
				// if(!$entity->isDefaultModeContext()) DevTerminal::Warning('<T7>Created new SYSTEM '.$sysOrDrivDir->getShortname().' '.json_encode($sysOrDrivDir->getName()).' for '.$entity->getShortname().' '.json_encode($entity->getName()).'.');
			}
		} else if(preg_match('/^@drive/', $root)) {
			$sysOrDrivDir = $entity->getDriveDir();
			if(empty($sysOrDrivDir)) {
				$sysOrDrivDir = $this->serviceDirectory->createNew(null, function($new_dir) use ($entity) {
					$entity->setDriveDir($new_dir);
				});
				// $entity->setDriveDir($sysOrDrivDir);
				// $sysOrDrivDir->checkAndRepairValidity();
				$sysOrDrivDir->validityEntityReport('Created new DRIVE '.$sysOrDrivDir->getShortname().' '.json_encode($sysOrDrivDir->getName()).' for '.$entity->getShortname().' '.json_encode($entity->getName()).'.', __LINE__, __METHOD__, true);
				// $entityManager->persist($sysOrDrivDir);
				// if(!$entity->isDefaultModeContext()) DevTerminal::Warning('<T7>Created new DRIVE '.$sysOrDrivDir->getShortname().' '.json_encode($sysOrDrivDir->getName()).' for '.$entity->getShortname().' '.json_encode($entity->getName()).'.');
			}
		} else {
			throw new Exception("Error line ".__LINE__." ".__METHOD__."(): root path ".json_encode($root)." for ".$entity->getShortname()." ".json_encode($entity->getName())." is not valid! Expected ".json_encode(static::ROOT_NAMES).".", 1);
		}
		if(empty($sysOrDrivDir)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): root directory named ".json_encode($root)." for ".$entity->getShortname()." ".json_encode($entity->getName())." could not be find nore be created!", 1);
		// path
		$validator = $this->container->get('validator');
		$found = false;
		$creatingPath = false;
		$current = $sysOrDrivDir;
		foreach ($pathname as $altname => $dirname) {
			if(!is_string($dirname) || empty($dirname)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): name should be defined as not null string, but got ".json_encode($dirname)."!", 1);
			$oneDefineValues = $defineValues;
			if(is_array($dirname)) {
				$oneDefineValues = $dirname;
				if(!is_string($altname)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): name should be defined as string in the key, but got ".json_encode($altname)."!", 1);
				$dirname = $altname;
			}
			if(!$creatingPath) {
				foreach ($current->getChildsForPathwalk() as $dirchild) {
					if($dirchild->getName() === $dirname && $dirchild instanceOf Basedirectory) {
						$current = $dirchild;
						$found = true;
						break;
					}
				}
			}
			if(!$found || $creatingPath) {
				$creatingPath = true;
				// Not found: create
				$new_dir = $this->serviceDirectory->createNew(null, function($new_dir) use ($current, $dirname) {
					$new_dir->setInputname($dirname);
					$new_dir->setParent($current);
				});
				// $new_dir->setInputname($dirname);
				// $new_dir->setParent($current);
				// $new_dir->checkAndRepairValidity();
				$new_dir->validityEntityReport('Created new SINGLE '.$new_dir->getShortname().' '.json_encode($new_dir->getName()).' for '.$entity->getShortname().' '.json_encode($entity->getName()).'.', __LINE__, __METHOD__, true);
				// $entityManager->persist($new_dir);
				// if(!$entity->isDefaultModeContext()) DevTerminal::Warning('<T7>Created new SINGLE '.$new_dir->getShortname().' '.json_encode($new_dir->getName()).' for '.$entity->getShortname().' '.json_encode($entity->getName()).'.');
				// $new_dir->setName($dirname);
				// $current->addChild($new_dir);
				if(is_array($oneDefineValues)) {
					foreach ($oneDefineValues as $property => $value) {
						$setter = "set".ucfirst($property);
						if(!method_exists($new_dir, $setter)) $setter = "add".ucfirst($property);
						if(method_exists($new_dir, $setter)) $new_dir->$setter($value);
					}
				}
				$current = $new_dir;
			}
		}
		return $current;
	}

	/**
	 * Check if NestedInterface has his default directories and create if not
	 * @param NestedInterface $entity
	 * @return serviceBasedirectory
	 */
	public function checkDirectorys(NestedInterface $entity) {
		if($entity->isDirectorysChecked() || $entity instanceOf Basedirectory) return $this;
		// Check schema
		$allSchemaDirectories = $this->getAllSchemaDirectorys($entity);
		// echo('<pre><h3>Schemas for '.$entity->getShortname().'</h3>'); var_dump(array_keys($allSchemaDirectories)); echo('</pre>');
		foreach ($allSchemaDirectories as $pathname => $data) {
			// echo('<h3>Construct for '.json_encode($entity->getShortname()).': '.$pathname.'</h3>');
			$this->constructDirectorysByPathname($entity, $pathname);
		}
		// Check JoinedOwnDirectory
		// $this->attachJoinedOwnDirectory($entity);
		// Set checked
		$entity->setDirectorysChecked();
		return $this;
	}

	public function attachJoinedOwnDirectoryByAnnotation(JoinedOwnDirectory $annotation, $getter = null, $setter = null, $force = false, $retrieveData = 'replace') {
		$getter ??= $this->container->get(serviceAnnotation::class)->getGetter($annotation->property, $annotation->entity, $annotation);
		$old_directory = $annotation->entity->$getter();
		if($force || !($old_directory instanceOf Basedirectory)) {
			// echo('<div>Event: '.$annotation->__toString().' on '.$annotation->entity->getShortname().' '.json_encode($annotation->entity->getName()).' Id#'.json_encode($annotation->entity->getId()).'</div>');
			$directory = $annotation->entity->getOwnerDirectory($annotation->path, true);
			if(empty($directory)) throw new Exception("Could not find nore create own directory!", 1);
			if($directory !== $old_directory) {
				$setter ??= $this->container->get(serviceAnnotation::class)->getSetter($annotation->property, $annotation->entity, $annotation);
				if($retrieveData) {
					$altgetter = $annotation->altgetter;
					// $data = is_string($altgetter) ? $annotation->entity->$altgetter() : new ArrayCollection();
					$data = $annotation->entity->$altgetter();
					$altsetter = $annotation->altsetter;
					$annotation->entity->$setter($directory);
					if(!$data->isEmpty()) {
						switch ($retrieveData) {
							case 'mix':
								foreach ($data as $item) {
									if($item->hasParent($old_directory)) {
										$item->setParent($directory, -1, false);
									} else {
										$item->addDirparent($directory);
										$item->removeDirparent($old_directory);
									}
								}
								break;
							default: // "replace"
								$annotation->entity->$altsetter($data);
								break;
						}
					}
				} else {
					$annotation->entity->$setter($directory);
				}
			}
		}
		return $this;
	}

	public function attachJoinedOwnDirectory($entity) {
		$annotations = serviceClasses::getPropertysAnnotation($entity, JoinedOwnDirectory::class, false);
		foreach ($annotations as $annotation) $this->attachJoinedOwnDirectoryByAnnotation($annotation);
		return $this;
	}



	/*************************************************************************************/
	/*** BASEDIRECTORY QUERYS Annotations
	/*************************************************************************************/

	/**
	 * Get list of BasedirQuery annotations with repository class, method => annotation
	 * @param boolean $refresh = false
	 * @return array
	 */
	public function getBdquerysAnnotations($refresh = false) {
		$annotations = $this->container->get(serviceCache::class)->getCacheData(
			'BasedirQuery',
			function() {
				$reader = new AnnotationReader();
				$repositorys = array_merge([ItemRepository::class], serviceClasses::getSubclasses(ItemRepository::class));
				foreach ($repositorys as $repo_class) {
					$methods = get_class_methods($repo_class);
					foreach ($methods as $method) {
						// $RM = new ReflectionMethod($method);
						$annot = $reader->getMethodAnnotation($method, BasedirQuery::class);
						if($annot instanceOf BasedirQuery) {
							$annotations[$repo_class.Basedirectory::QUERY_SEPARATOR.$method] = [];
							foreach (get_object_vars(BasedirQuery::class) as $name => $value) {
								$annotations[$repo_class.Basedirectory::QUERY_SEPARATOR.$method][$name] = $annot->$name;
							}
						}
					}
				}
				return $annotations;
			},
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
		return array_map(function($attrs) {
			$basedirQuery = new BasedirQuery();
			foreach ($attrs as $attr => $value) $basedirQuery->$attr = $value;
			return $basedirQuery;
		}, $annotations);
	}


	/**
	 * Get choices of repository shortname::method + '()' => class::method
	 * @return array
	 */
	public function getBdqueryChoices() {
		$choices = [];
		foreach ($this->getBdquerysAnnotations() as $repoMethod => $values) {
			$expl = explode(Basedirectory::QUERY_SEPARATOR, $repoMethod);
			$RC = new ReflectionClass($expl[0]);
			foreach ($values as $name => $value) $choices[$RC->getShortName().Basedirectory::QUERY_SEPARATOR.$expl[1].'()'] = $expl[0].Basedirectory::QUERY_SEPARATOR.$expl[1];
		}
		return $choices;
	}


	public function getQueryRepository($query) {
		$expl = explode(Basedirectory::QUERY_SEPARATOR, $query);
		return $this->serviceEntities->getRepository($expl[0]);
	}

	public function getQueryMethod($query) {
		$expl = explode(Basedirectory::QUERY_SEPARATOR, $query);
		$repository = $this->serviceEntities->getRepository($expl[0]);
		if(!method_exists($repository, $expl[1])) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this repository ".json_encode(get_class($repository))." does not have method ".json_encode($expl[1])."!", 1);
		return $expl[1];
	}



	/*************************************************************************************/
	/*** PAGINATION
	/*************************************************************************************/
	/**
	 * @see https://www.doctrine-project.org/projects/doctrine-orm/en/2.8/tutorials/pagination.html
	 */

	/**
	 * Compute pagination parameters / Returns has pagination
	 * @param integer &$page = -1
	 * @param integer &$length = null
	 * @return boolean
	 */
	public static function computePagination(&$page = -1, &$length = null) {
		if($page < 0) $length = null;
		if($page >= 0 && $length < 1) $length = static::DEFAULT_PAGE_LENGTH;
		return $page >= 0;
	}

	public static function paginateResults(ArrayCollection $results, &$page = -1, &$length = null) {
		$paginate = static::computePagination($page, $length);
		if($paginate && $page >= 0) {
			return $results->slice($page * $length, $length);
		}
		return $results;
	}




}