<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Behat\Transliterator\Transliterator;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceForms;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\Partenaire;
use ModelApi\UserBundle\Entity\OwnerTierInterface;
use ModelApi\UserBundle\Service\serviceTier;
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Service\serviceRoles;
// CrudsBundle
use ModelApi\CrudsBundle\Service\serviceCruds;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Fileformat;
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Entity\Tempfile;
use ModelApi\FileBundle\Service\serviceDirectory;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// BinaryBundle
use ModelApi\BinaryBundle\Service\serviceBinaryFile;
// AnnotBundle
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
// use ModelApi\AnnotBundle\Annotation\StoreDirContent;
use ModelApi\AnnotBundle\Annotation\EventAnnotationInterface;
use ModelApi\AnnotBundle\Service\serviceAnnotation;
use ModelApi\AnnotBundle\Classes\DirectoryCollection;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

use \ReflectionClass;
use \Exception;

class FileManager implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = File::class;
	const ENTITY_CREATE_TYPE = null;
	const ENTITY_UPDATE_TYPE = null;

	const DEFAULT_ACTION = 'simple';

	// SUCCESS
	const FILE_SUCCESS = 0;
	// ERRORS
	const FILE_ERROR_HAS_CHILD = 100; // Le parent possède déjà cet enfant
	const FILE_ERROR_CLASS_CHILD = 101; // Classe de l'enfant incorrecte
	const FILE_ERROR_UNKNOWN = 500; // Erreur indéterminée

	const VALID_FILENAME_EXPR = '[_-\\.A-Za-z]+';
	const NORMALIZE_METHODS = ['Aucune', 'URL', 'URL sans extension', 'Minuscules', 'Majuscules', 'URL Majuscules'];
	const NORMALIZE_DEFAULT_METHOD = 'Aucune';

	const UNIQUE_INDEX_SEPARATOR = '-';

	protected $container;
	protected $serviceEntities;
	protected $serviceDirectory;
	protected $serviceCruds;
	protected $lastResult;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->serviceCruds = $this->container->get(serviceCruds::class);
		// $this->serviceDirectory = $this->container->get(serviceDirectory::class);
		$this->lastResult = static::FILE_SUCCESS;
		return $this;
	}


	/********************************************************************************************************************/
	/*** CONTROL
	/********************************************************************************************************************/

	// public function controlGlobalCoherence(Item $item) {
	// 	$messages = [];
	// 	if($this->hasSimilarPathnames($item)) $messages[] = "this Item ".$item->getName()." has still a pathname that is not unique!";
	// 	return $messages;
	// }

	// public function getRepository() {
	// 	// if(!is_string(static::ENTITY_CLASS)) return null;
	// 	return $this->serviceEntities->getRepository(static::ENTITY_CLASS);
	// }

	/**
	 * Get new entity
	 * @param array $options = []
	 * @param callable $beforePostNewEventClosure = null
	 * @return entity | null
	 */
	public function createNew($options = [], callable $beforePostNewEventClosure = null) {
		$classname = Tempfile::class;
		if(null != $options) {
			if(is_array($options)) {
				$parameters = [];
				foreach ($options as $key => $parameter) {
					$com = is_string($key) ? '"' : '';
					$parameters[] = '$options['.$com.$key.$com.']';
				}
				$eval = "return new \$classname(".implode(', ', $parameters).");";
				$entity = eval($eval);
			} else {
				$entity = new $classname($options);
			}
		} else {
			$entity = new $classname();
		}
		if(is_callable($beforePostNewEventClosure)) $beforePostNewEventClosure($entity);
		$this->serviceEntities->applyEvent($entity, BaseAnnotation::postNew);
		// if($entity instanceOf Item) $this->applyEvent($entity, ItemAnnotation::postNew);
		return $entity;
		// throw new Exception("Error line ".__LINE__." ".__METHOD__."(): ENTITY_CLASS must be defined or at least instantiable, so please, use the good service.", 1);
	}

	// public function delete($entity, $flush = true, $forceDelete = false) {
	// 	throw new Exception("Error line ".__LINE__." ".__METHOD__."(): ENTITY_CLASS must be defined or at least instantiable, so please, use the good service.", 1);
	// }

	// public function undelete($entity, $flush = true) {
	// 	throw new Exception("Error line ".__LINE__." ".__METHOD__."(): ENTITY_CLASS must be defined or at least instantiable, so please, use the good service.", 1);
	// }

	public function findByTier(Tier $tier = null, $asArray = false) {
		// if(!serviceClasses::isInstantiable(static::ENTITY_CLASS))
			// throw new Exception("Error line ".__LINE__." ".__METHOD__."(): ENTITY_CLASS must be defined or at least instantiable, so please, use the good service.", 1);
		// $repo = $this->getRepository();
		return $this->getRepository()->findByTier($tier, $asArray);
	}

	public function findByEnvironment($asArray = false) {
		$tier = $this->container->get(serviceContext::class)->getEnvironment();
		if(!($tier instanceOf Tier)) return [];
		return $this->getRepository()->findByTier($tier, $asArray);
	}

	public function findOrphansByTier(Tier $tier, $asArray = false) {
		return $this->getRepository()->findOrphansByTier($tier, $asArray);
	}




	/********************************************************************************************************************/
	/*** TEMPFILE
	/********************************************************************************************************************/

	// public function hasSimilarPathnames(Item $item) {
	// 	$founds = $this->serviceEntities->getRepository(Item::class)->findSimilarPathnames($item);
	// 	return count($founds) > 0;
	// }

	public function createFileByTempfile(Tempfile $tempfile) {
		$uploadFile = $tempfile->getUploadFile();
		if(is_string($uploadFile)) {
			// Transform stream as UploadedFile
			$uploadFile = serviceTools::getUploadFileInstance($uploadFile);
		}
		if($uploadFile instanceOf UploadedFile) {
			$classnames = $this->container->get(serviceBinaryFile::class)->getFileClassesByUploadedFile($uploadFile);
			$classname = reset($classnames);
			if(is_string($classname) && $this->serviceEntities->entityExists($classname)) {
				$file = $this->serviceEntities->createNew($classname, [], function($file) use ($tempfile) {
					if(!empty($tempfile->getOwner())) $file->setOwner($tempfile->getOwner());
				});
				$file->setInputname($tempfile->getInputname());
				$file->setUploadFile($tempfile->getUploadFile());
				$fileversion = $file->getCurrentVersion();
				if($fileversion instanceOf Fileversion) {
					$this->container->get(serviceBinaryFile::class)
						->setBaseDeclinations($fileversion, false)
						->AttachFileformat($fileversion)
						;
					$file->checkVersion();
				}

				// $file->setInputname($tempfile->getInputname());
			} else {
				throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not create file with this invalid format!", 1);
			}
		} else {
			throw new Exception("Error line ".__LINE__." ".__METHOD__."(): uploadFile is not valid!", 1);
		}
		return $file;

		// $new_valid = null;
		// // echo('<pre><h3>Search type mime:</h3>'); var_dump($uploadFile->getMimeType()); echo('</pre>');
		// foreach ($this->serviceEntities->getSubentities(File::class) as $class) {
		// 	$new = $this->serviceEntities->createNew($class); // Get model ???
		// 	$this->serviceEntities->preCreateForForm($new);
		// 	// echo('<pre><h3>Valid content types for '.$new->getShortName().'</h3>'); var_dump($new->getValidContentTypes(true)); echo('</pre>');
		// 	if(in_array($uploadFile->getMimeType(), $new->getValidContentTypes(true))) {
		// 		$new_valid = $new;
		// 		break;
		// 	} else {
		// 		unset($new);
		// 	}
		// }
		// // echo('<pre><h3>Found '.$new_valid->getShortName().'</h3>'); var_dump($new_valid->getValidContentTypes(true)); echo('</pre>');
		// // echo('<pre>');var_dump($this->serviceEntities->getSubentities(File::class));echo('</pre>');
		// // $entity = $this->serviceEntities->cloneEntity($tempfile, $new_valid);
		// $new_valid->setInputname($tempfile->getInputname());
		// // $parent = $this->container->get(serviceBasedirectory::class)->getRepository()->find($tempfile->getTempParent()->getId());
		// $new_valid->setUploadFile($uploadFile);
		// // if($parent instanceOf Basedirectory) $new_valid->setParent($parent);
		// // if($tempfile->getTempParent() instanceOf Basedirectory) $tempfile->getTempParent()->addChild($new_valid);
		// // $new_valid->setOwner($tempfile->getOwner());
		// // die('OK');
		// return $new_valid;
	}


	/********************************************************************************************************************/
	/*** DIRECTORYS
	/********************************************************************************************************************/

	/**
	 * Get NestedInterface's Directorys by pathname
	 * @param string $pathname
	 * @param NestedInterface $entity = null
	 * @param boolean $addChilds = false
	 * @return Array <Directory>
	 */
	public function getEntityDirectorys($pathname, NestedInterface $entity, $addChilds = false) {
		return $this->serviceEntities->getRepository(Basedirectory::class)->findByEntityAndPath($pathname, $entity, $addChilds);
	}

	/**
	 * Get first NestedInterface's Directory by pathname
	 * @param string $pathname
	 * @param NestedInterface $entity = null
	 * @param boolean $addChilds = false
	 * @return Directory | null
	 */
	public function getEntityFirstDirectory($pathname, NestedInterface $entity, $addChilds = false) {
		return $this->serviceEntities->getRepository(Basedirectory::class)->findOneByEntityAndPath($pathname, $entity, $addChilds);
	}


	/********************************************************************************************************************/
	/*** FILE naming strategy
	/********************************************************************************************************************/

	public static function isValidFilename($string) {
		return preg_match('/'.static::VALID_FILENAME_EXPR.'/', $string);
	}

	public static function getDefaultNormalizeMethod($normalizeMethod = null) {
		$normalizeMethods = static::NORMALIZE_METHODS;
		return in_array($normalizeMethod, $normalizeMethods) ? $normalizeMethod : reset($normalizeMethods);
	}

	public static function getValidTransformedFilename(&$string, $normalizeMethod = null, $separator = null) {
		if(null == $separator) $separator = static::UNIQUE_INDEX_SEPARATOR;
		// Basic common normalization:
		$normalizeMethod = static::getDefaultNormalizeMethod($normalizeMethod);
		if(empty($string)) throw new Exception("Error transforming name using ".json_encode($normalizeMethod).": string is empty (got ".json_encode($string).")!", 1);
		$init_string = $string;
		serviceTools::cleanFilename($string);
		// $string = preg_replace('/[\\.\\s\\/:]+$/', '', trim($string));
		// $string = preg_replace('/[\\s\\/:]+/', '_', $string);
		// $ext = serviceTools::getSplitedFileExtension($string);
		switch ($normalizeMethod) {
			case 'URL':
				// --> URL
				static::urlize($string, $separator);
				break;
			case 'URL Majuscules':
				// --> URL Majuscules
				static::urluppercase($string, $separator);
				break;
			case 'URL sans extension':
				// --> URL sans extension
				serviceTools::removeFileExtension($string);
				static::urlize($string, $separator);
				break;
			case 'Minuscules':
				// --> Minuscules
				static::normalize($string);
				break;
			case 'Majuscules':
				// --> Majuscules
				static::denormalize($string);
				// preg_replace('/\\s+/', '', static::denormalize($string));
				break;
			default:
				// --> Aucune
				// do nothing
				break;
		}
		if(empty($string)) throw new Exception("Error transforming name ".json_encode($init_string)." using ".json_encode($normalizeMethod).": result is ".json_encode($string)."!", 1);
		return $string;
	}

	public static function urlize(&$string, $separator = null) {
		$string = Transliterator::urlize($string, $separator);
		return $string;
	}

	public static function normalize(&$string) {
		$cam = new CamelCaseToSnakeCaseNameConverter();
		$string = $cam->normalize($string);
		return $string;
	}

	public static function denormalize(&$string) {
		$cam = new CamelCaseToSnakeCaseNameConverter();
		$string = ucfirst($cam->denormalize($string));
		return $string;
	}

	public static function urluppercase(&$string, $separator = null) {
		$string = strtoupper(Transliterator::urlize($string, $separator));
		return $string;
	}

	public static function getNormalizeChoices() {
		$choices = [];
		foreach (static::NORMALIZE_METHODS as $method) {
			$choices[$method] = $method;
		}
		return $choices;
	}

	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString() {
		$class = new ReflectionClass(static::class);
		return $class->getShortName();
	}

	public function findOrphanFiles($class = null, $asArray = false) {
		return $this->serviceEntities->getRepository(File::class)->findOrphanFiles($class, $asArray);
	}

	public function getFileformatsByMediaTypes($types, $onlyEnabled = true) {
		$bys = ['mediaType' => $types];
		if($onlyEnabled) $bys['enabled'] = 1;
		return $this->serviceEntities->getRepository(Fileformat::class)->findBy($bys);
	}

	public function getContentTypesByMediaTypes($types, $onlyEnabled = true) {
		$fileformats = $this->getFileformatsByMediaTypes($types, $onlyEnabled);
		$contentTypes = [];
		foreach ($fileformats as $format) {
			$contentTypes[] = $format->getContentType();
		}
		return array_unique($contentTypes);
	}

	public function getModel($classname = null, $asEntityIfPossible = false) {
		$classname = $this->serviceEntities->getClassnameByAnything($classname);
		if(null === $classname) return null;
		return $this->serviceEntities->getModel($classname, $asEntityIfPossible);
	}



}