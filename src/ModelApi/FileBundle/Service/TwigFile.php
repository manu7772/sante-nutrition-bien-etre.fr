<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use \Twig_Extension;
use \Twig_SimpleFilter;
use \Twig_SimpleFunction;
use JMS\Serializer\SerializationContext;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;

use ModelApi\AnnotBundle\Annotation\JoinedOwnDirectory;

use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\serviceFileversion;

// use \DateTime;
use \ReflectionClass;
use \ReflectionMethod;
use \Exception;

class TwigFile extends Twig_Extension implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	private $container;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	public function getFunctions() {
		return array(
			// functions
			new Twig_SimpleFunction('fileurl', array($this, 'getFileUrl')),
			new Twig_SimpleFunction('project_dir', array($this, 'getProjectDir')),
			new Twig_SimpleFunction('base_dir', array($this, 'getBaseDir')),
			new Twig_SimpleFunction('action_color', array($this, 'getActionColor')),
			// new Twig_SimpleFunction('icon', array($this, 'getIcon')),
			// FORM for add images in Diapos
			new Twig_SimpleFunction('getFormAddDiapo', array($this, 'getFormAddDiapo')),
			new Twig_SimpleFunction('getCheckedSlugCode', array($this, 'getCheckedSlugCode')),
			// text
			new Twig_SimpleFunction('striptagstoreadable', array($this, 'striptagstoreadable')),
			new Twig_SimpleFunction('striptagstoextract', array($this, 'striptagstoextract')),
			// Get Annotation
			new Twig_SimpleFunction('getOwnerDirAnnotation', array($this, 'getOwnerDirAnnotation')),
		);
	}

	public function getFilters() {
		return array(
			// filters
			// new Twig_SimpleFilter('field', array($this, 'getField')),
			// text
			new Twig_SimpleFilter('striptagstoreadable', array($this, 'striptagstoreadable')),
			new Twig_SimpleFilter('striptagstoextract', array($this, 'striptagstoextract')),
		);
	}



	////////////////////////////// FUNCTIONS /////////////////

	public function getProjectDir($path = null) {
		return $this->container->get(serviceKernel::class)->getProjectDir($path);
	}

	public function getBaseDir($path = null) {
		return $this->container->get(serviceKernel::class)->getBaseDir($path);
	}

	/**
	 * Get best file URL for image of entity
	 * @param object|array $entity
	 * @param string $type = 'optimized'
	 * @param integer $Xsize = 800
	 * @return string | null
	 */
	public function getFileUrl($entity, $type = 'optimized', $Xsize = 800, $forceType = false) {
		if(!is_array($entity) && !is_object($entity)) return null;
		$file_urls = is_array($entity) ? (isset($entity['fileUrls']) ? $entity['fileUrls'] : $entity['file_urls']) : $entity->getFileUrls();
		$sizefound = 0;
		$type = strtolower(preg_replace('/s$/i', '', $type));
		$url = $file_urls['original'] ?? null;
		if($type === 'realsize' && isset($file_urls['realsize'])) {
			$url = $file_urls['realsize'];
		} else if($type !== 'original') {
			foreach ($file_urls as $format => $fileUrl) {
				$spl = preg_split('#_W?#', $format);
				$base = preg_replace('/s$/i', '', $spl[0]);
				if($type === $base) {
					switch ($base) {
						case 'optimized':
							$width = (integer)$spl[1];
							if($Xsize < 1) {
								if($width >= $sizefound) { $sizefound = $width; $url = $fileUrl; }
							} else if ($width >= $sizefound && $sizefound < $Xsize) {
								$sizefound = $width; $url = $fileUrl;
							} else if ($sizefound > $width && $width >= $Xsize) {
								$sizefound = $width; $url = $fileUrl;
							} else if ($sizefound < 1 && $forceType) {
								$sizefound = $width; $url = $fileUrl;
							}
							break;
						default:
							$width = (integer)(preg_split('/x/i', $spl[1])[0]);
							if($Xsize < 1) {
								if($width >= $sizefound) { $sizefound = $width; $url = $fileUrl; }
							} else if ($width >= $sizefound && $sizefound < $Xsize) {
								$sizefound = $width; $url = $fileUrl;
							} else if ($sizefound > $width && $width >= $Xsize) {
								$sizefound = $width; $url = $fileUrl;
							} else if ($sizefound < 1 && $forceType) {
								$sizefound = $width; $url = $fileUrl;
							}
							break;
					}
				}
			}
		}
		return $url;
	}

	public function getActionColor($colors = 'default', $entity = null, $default = 'default') {
		if(is_string($colors)) return $colors;
		$final_color = $default;
		if(is_array($entity) && isset($entity['id'])) {
			// Entities as ARRAY
			if(is_array($colors)) {
				foreach ($colors as $color => $items) {
					if(isset($items['id']) && $items['id'] === $entity['id']) {
						$final_color = $color;
					} else {
						foreach ($items as $item) {
							if($item['id'] === $entity['id']) {
								$final_color = $color;
								break;
							}
						}
					}
				}
			}			
		}
		if(!is_object($entity) && !is_string($entity)) return $final_color;
		// Entities as OBJECT
		if(is_array($colors)) {
			foreach ($colors as $color => $items) {
				if(is_array($items)) $items = new ArrayCollection($items);
				// echo('<pre><h4>'.$color.'</h4>');var_dump(get_class($items));echo('</pre>');
				if($items instanceOf PersistentCollection || $items instanceOf ArrayCollection) {
					$final_color = $items->contains($entity) ? $color : $default;
					break;
				} else if(is_object($items) && $items === $entity) {
					$final_color = $color;
					break;
				}
			}
		}
		return $final_color;
	}

	public function getFormAddDiapo($bddid) {
		
	}

	public function getCheckedSlugCode(Fileversion $fileversion) {
		return $this->container->get(serviceFileversion::class)->checksluglinks($fileversion, false, true);
	}

	public function getOwnerDirAnnotation($entity, $property) {
		$annotation = serviceClasses::getPropertyAnnotation($entity, $property, JoinedOwnDirectory::class, false);
		if(!($annotation instanceOf JoinedOwnDirectory)) return null;
		if(is_object($entity)) $annotation->setEntity($entity);
		return $annotation;
	}

	// public function getIcon($entity, $asHtml = true, $asDefault = false, $classes = null) {
	// 	$icon = 'fa-question';
	// 	$prefix = 'fa';
	// 	if(is_string($entity)) {
	// 		$classname =  $this->container->get(serviceEntities::class)->getClassnameByAnything($entity);
	// 		if(null !== $classname) {
	// 			$icon = $classname::DEFAULT_ICON;
	// 			$RC = new ReflectionClass($classname);
	// 			if($RC->isInstantiable()) {
	// 				$constructor = $RC->getMethod('__construct');
	// 				if($constructor->getNumberOfParameters() < 1) $entity = new $classname();
	// 			}
	// 		}
	// 	}
	// 	if(is_object($entity)) {
	// 		// Entity has methods
	// 		if($asHtml && !$asDefault && method_exists($entity, 'getIconHtml')) return $entity->getIconHtml(false, $classes);
	// 		if($asHtml && $asDefault && method_exists($entity, 'getDefaultIconHtml')) return $entity->getDefaultIconHtml(false, $classes);
	// 		// Entity has icon
	// 		if(defined('DEFAULT_ICON') || method_exists($entity, 'getIcon')) {
	// 			if($asDefault) {
	// 				$icon = method_exists($entity, 'getDefaultIcon') ? $entity->getDefaultIcon() : $entity::DEFAULT_ICON;
	// 			} else {
	// 				$icon = method_exists($entity, 'getIcon') ? $entity->getIcon() : $entity::DEFAULT_ICON;
	// 			}
	// 		}
	// 	}
	// 	if(!$asHtml) return $icon;
	// 	if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
	// 	if(is_array($classes)) {
	// 		$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
	// 	} else {
	// 		$classes = '';
	// 	}
	// 	if(preg_match('/^fa-/', $icon)) $prefix = 'fa';
	// 	return '<i class="'.$prefix.' '.$icon.$classes.'"></i>';
	// }


	////////////////////////////// FILTERS /////////////////

	// public function getField($entity, $field) {
	// 	if(isset($entity->$field)) return $entity->$field;
	// 	$getter = 'get'.ucfirst($field);
	// 	return method_exists($entity, $getter) ? $entity->$getter() : null;
	// }

	/**
	 * Get a readable stripped string of html code
	 * @param string $string
	 * @param boolean $specialChars = false
	 * @return string
	 */
	public function striptagstoreadable($string, $specialChars = false) {
		$string = preg_replace(['/<br\s*\/?>/','/\s+/','/&nbsp;/'], " ", $string);
		if(true === $specialChars) $string = htmlspecialchars_decode($string, ENT_NOQUOTES);
		return strip_tags($string);
	}

	/**
	 * Get a readable stripped sub string of html code
	 * @param string $string
	 * @param integer $length = 60
	 * @param boolean $specialChars = false
	 * @return string
	 */
	public function striptagstoextract($string, $length = 60, $specialChars = false) {
		$string = $this->striptagstoreadable($string, $specialChars);
		$newString = trim(substr($string, 0, $length));
		return strlen($newString) < strlen($string) ? $newString.'…' : $newString;
	}






}