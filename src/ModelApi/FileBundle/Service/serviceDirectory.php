<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Service\serviceBasedirectory;

// use \DateTime;
use \ReflectionClass;

class serviceDirectory extends serviceBasedirectory {

	const ENTITY_CLASS = Directory::class;
	const PATH_POSTFIX = 'Path';


}