<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Fileformat;
use ModelApi\FileBundle\Service\FileManager;

// use \DateTime;
use \ReflectionClass;

class serviceFileformat implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Fileformat::class;

	private $container;
	private $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
	}

}