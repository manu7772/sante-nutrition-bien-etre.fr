<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;
// use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\OnClearEventArgs;
// AnnotBundle
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
use ModelApi\AnnotBundle\Service\serviceAnnotation;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Tempfile;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\OwnerTierInterface;
use ModelApi\UserBundle\Service\serviceUser;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

// use \DateTime;
use \ReflectionClass;
use \Exception;

class serviceItem implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Item::class;
	const OWNEDS_DIRECTORY_NAME = 'system/Owneds';
	const MANAGERS_DIRECTORY_NAME = 'system/Manageds';
	const DRIVEDOC_DIRECTORY_NAME = 'drive/Documents';

	protected $container;
	protected $serviceEntities;
	protected $treatedItemPaths;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}

	public function getCurrentUserOrNull() {
		return $this->container->get(serviceUser::class)->getCurrentUserOrNull();
	}

	public function getMainUserAdmin() {
		return $this->container->get(serviceUser::class)->getMainUserAdmin();
	}

	public function findItems($classnames = null, $asArray = false) {
		return $this->getRepository()->findItems($classnames, $asArray);
	}

	public function findOrphanItems($classnames = null, $asArray = false) {
		return $this->getRepository()->findOrphanItems($classnames, $asArray);
	}

	public function completeRoute($route) {
		$route['_route_items'] = $this->getRepository()->findForBreadcrumbs($route['_route_params']);
		return $route;
	}

	public function countItemsByClass($shortnames = [], Tier $tier = null) {
		return $this->getRepository()->countItemsByClass($shortnames, $tier);
	}


	/********************************************************************************************************************/
	/*** OWNER & PARENT
	/********************************************************************************************************************/

	/**
	 * Get default parent for Item
	 * @param Item $item
	 * @param boolean $createIfNotFound = true
	 * @return Basedirectory | null 
	 */
	public function getLogicParent(Item $item, $createIfNotFound = true) {
		if(!$item->canHaveParent()) return null;
		if(!($item->getOwner() instanceOf Item)) $item->tryResolveOwner(false);
		if(!($item->getOwner() instanceOf Item)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): can not find parent when item has no owner!", 1);
		// if(!($item->getOwner() instanceOf OwnerTierInterface)) $item->computeOwnerToOwnerTierInterface(true);
		$owner = $item->getOwner()->getOwnerdir();
		if(empty($owner)) $owner = $item->getOwner();
		return $this->getDefaultOwnerDirectory($item, $owner, $createIfNotFound);
		// $directory = $this->getDefaultOwnerDirectory($item, $item->getOwner(), $createIfNotFound);
		// if($directory instanceOf Basedirectory) {
		// 	return $directory === $item ? $directory->getParent() : $directory;
		// }
		// return null;
	}

	public function getDefaultOwnerDirectory($itemOrClassname, Item $owner, $createIfNotFound = true, $searchInSchemas = true) {
		$defaults = [
			'@@@default@@@' => static::OWNEDS_DIRECTORY_NAME,
			'@@@drive_documents@@@' => static::DRIVEDOC_DIRECTORY_NAME,
		];
		$classname = is_object($itemOrClassname) ? serviceEntities::removeProxyPrefix(get_class($itemOrClassname)) : serviceEntities::removeProxyPrefix($itemOrClassname);
		if(!$this->serviceEntities->entityExists($classname)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): parameter is not an Entity (object or class)", 1);
		// foreach (serviceClasses::getParentClasses($itemOrClassname, true, true, false, false) as $shortname => $classname) {
		if(false === $classname::DEFAULT_OWNER_DIRECTORY || $classname === User::class) return null; // --> false : do nothing
		$pathname = $classname::DEFAULT_OWNER_DIRECTORY;
		if(array_key_exists($pathname, $defaults)) $pathname = $defaults[$pathname];
		if($searchInSchemas && is_object($itemOrClassname) && $itemOrClassname instanceOf Basedirectory) {
			// Could be part of schema directorys
			$schemas = $this->container->get(serviceBasedirectory::class)->getAllSchemaDirectorys($owner);
			foreach ($schemas as $path => $data) {
				if(preg_match('/\\'.DIRECTORY_SEPARATOR.$itemOrClassname->getName().'$/', $path)) {
					$name = explode(DIRECTORY_SEPARATOR, $path);
					array_pop($name);
					$pathname = implode(DIRECTORY_SEPARATOR, $name);
					// static::pathArrayToString($name);
					// $pathname = $name;
					if(!$this->isDefaultModeContext()) DevTerminal::SuccessPlus('<T5>- found '.json_encode($path).' with '.json_encode($itemOrClassname->getName()).' Search => '.$pathname);
					// echo('<h3 style="color: blue;">Found schema path: '.json_encode($path).' with '.json_encode($itemOrClassname->getName()).' Search => '.$pathname.'.</h3>');
				}
			}
		}
		return $this->container->get(serviceBasedirectory::class)->getDirByPath($owner, $pathname, $createIfNotFound);
	}

	/*************************************************************************************/
	/*** REQUESTS
	/*************************************************************************************/

	public function findOwnersystemdirs(Basedirectory $rootdirectory) {
		return $this->getEntityManager()->getRepository(Item::class)->findBy(['systemDir' => $rootdirectory]);
	}

	public function findOwnerdrivedirs(Basedirectory $rootdirectory) {
		return $this->getEntityManager()->getRepository(Item::class)->findBy(['driveDir' => $rootdirectory]);
	}



	public function removeNotOwnedDirsFromRoots(User $user, $directorys = null, $recursive = true) {
		if(empty($directorys)) {
			$directorys = $user->getRootDirs();
		}
		if($directorys instanceOf Basedirectory) $directorys = [$directorys];
		foreach ($directorys as $directory) if($directory instanceOf Directory) {
			foreach ($directory->getChilds(-1, null, false) as $child) {
				// if($directory->isCompleteOrIncompleteRoot() && !$child instanceOf Basedirectory) {

				// 	throw new Exception("Error line ".__LINE__." ".__METHOD__."(): User ".$user->getUsername()." has Drive dir with child that is not a Directory: ".$child->getShortname()." ".json_encode($child->getName())."!", 1);
				// }
				if($child instanceOf Directory && !$child->hasParent($directory)) {
					// $child->removeDirparent($directory);
					$directory->removeChild($child);
					$child->checkAndRepairValidity();
					DevTerminal::Success('<T5>- User '.json_encode($user->getShortname()).' removed not owned from '.json_encode($directory->getPathname(true)).': '.$child->getShortname().' '.json_encode($child->getName()).' of '.$child->getOwner()->getShortname().' '.json_encode($child->getOwner()->getUsername()).'.');
				} else if($recursive && $child instanceOf Basedirectory) {
					$this->removeNotOwnedDirsFromRoots($user, [$child]);
				}
			}
		}
		return $this;
	}

	public function getGrantedItems(Tier $environment, $classes = [], $actions = [], $asArrayResult = true, $getCounts = false) {
		// return $this->getRepository()->findGrantsChoices($environment);
		return $this->getRepository()->findGrantedItems($environment, $classes, $actions, $asArrayResult, $getCounts);
	}


	/*************************************************************************************/
	/*** REQUESTS FOR OWNERS/MANAGERS/...
	/*************************************************************************************/

	public function getDiffuseurs($item, $classes = null) {
		$diffuseurs = new ArrayCollection();
		foreach ($this->container->get(serviceEntities::class)->getRepository(Item::class)->getDiffuseurs($item, $classes) as $diffuseur) {
			//if(empty($classes) || in_array($diffuseur->getShortname(), $classes) || in_array($diffuseur->getClassname(), $classes)) {
				if(!$diffuseurs->contains($diffuseur)) $diffuseurs->add($diffuseur);
			//}
		}
		return $diffuseurs;
	}


	// public function getManagers(Item $item) {
	// 	return $this->getRepository()->getManagers($item);
	// }


	/********************************************************************************************************************/
	/*** CHECK & REPAIR ITEM
	/********************************************************************************************************************/

	// public function validate(Item $item, $message = '', $line = null, $method = null, $reportErrors = false) {
	// 	$line ??= __LINE__;
	// 	$method ??= __METHOD__;
	// 	$errors = $this->container->get('validator')->validate($item);
	// 	if($reportErrors) return $errors;
	// 	if($errors->count() > 0) {
	// 		DevTerminal::contextException($item, false, (empty($item->getId()) ? "Created " : "Updated ").$item->getShortname()." ".json_encode($item->getName())." has errors! ".(empty($message) ? '' : PHP_EOL.$message), $errors, $line, $method);
	// 		// throw new Exception("Error line ".__LINE__." ".__METHOD__."(): created directory NAME: ".json_encode($item->getName())." has errors!", 1);
	// 	}
	// 	return $errors->count() <= 0;
	// }

	public function getErrors(Item $item) {
		return $this->container->get('validator')->validate($item);
	}

	public function recomputeItem(Item $item) {
		// if(!($item->__computed ?? false)) {
			// $item->setDeepControl(false);

			// $this->printItem($item, 'To recompute');
			// $item->updateNestedData();
			$item->checkAndRepairValidity();
			if($item instanceOf User) $this->removeNotOwnedDirsFromRoots($item);
			$this->container->get(serviceBasedirectory::class)->checkDirectorys($item);
			// $this->printItem($item, 'Recomputed', 'Success');

			// $item->isValidOwnerAndParent(true);
			$item->validityEntityReport("Errors while recompute Item.", __LINE__, __METHOD__);
			// $this->validate($item, "Errors while recompute Item.", __LINE__, __METHOD__);
			// $item->__computed = true;
			return $this;
		// }
		return $this;
	}

	public function resolveJoinedowndirectory() {
		// $serviceAnnotation = $this->container->get(serviceAnnotation::class);
		// $old_modeContext = $serviceAnnotation->getModeContext();
		// $serviceAnnotation->setModeContext('check');
		$items = $this->getRepository()->findAll();
		// $serviceBasedirectory = $this->container->get(serviceBasedirectory::class);
		foreach ($items as $item) {
			$this->serviceEntities->applyEvent($item, BaseAnnotation::checkItem);
			// $serviceBasedirectory->attachJoinedOwnDirectoryByAnnotation($propertyAnnotation, $getter, $setter, true);
		}
		$this->serviceEntities->getEntityManager()->flush();
		// $serviceAnnotation->setModeContext($old_modeContext);
		return $this;
	}


	public function searchSameOwnerdirs($directorys) {
		$memos = [];
		foreach ($directorys as $directory) if($directory instanceOf Basedirectory) {
			if($directory->isCompleteOrIncompleteRoot()) {
				if(!preg_match('/^@(system|drive)/', $directory->getName())) DevTerminal::launchEntityValidateException($directory, "name ".json_encode($directory->getName())." is not valid!", null, __LINE__, __METHOD__);
				if(!empty($directory->getOwnerdrivedir()) && !empty($directory->getOwnersystemdir())) DevTerminal::launchEntityValidateException($directory, "has both system and drive owner dirs!", null, __LINE__, __METHOD__);
				if(empty($directory->getOwnersystemdir()) && empty($directory->getOwnerdrivedir())) DevTerminal::launchEntityValidateException($directory, "Has no system nore drive owner dir!", null, __LINE__, __METHOD__);
				if(!empty($directory->getOwnerdrivedir())) {
					$index = $directory->getOwnersystemdir().'_system_'.$directory->getId();
					if(array_key_exists($index, $memos)) {
						DevTerminal::validityEntityReport($memos[$index]);
						DevTerminal::launchEntityValidateException($directory, "two root directorys have same owner SYSTEM dir!", null, __LINE__, __METHOD__);
					}
					$memos[$index] = $directory;
				}
				if(!empty($directory->getOwnerdrivedir())) {
					$index = $directory->getOwnerdrivedir().'_drive_'.$directory->getId();
					if(array_key_exists($index, $memos)) {
						DevTerminal::validityEntityReport($memos[$index]);
						DevTerminal::launchEntityValidateException($directory, "two root directorys have same owner DRIVE dir!", null, __LINE__, __METHOD__);
					}
					$memos[$index] = $directory;
				}
			} else {
				if(preg_match('/^@(system|drive)/', $directory->getName())) DevTerminal::launchEntityValidateException($directory, "name ".json_encode($directory->getName())." is not valid!", null, __LINE__, __METHOD__);
				if(!empty($directory->getOwnersystemdir())) DevTerminal::launchEntityValidateException($directory, "Owner system dir is not empty!", null, __LINE__, __METHOD__);
				if(!empty($directory->getOwnerdrivedir())) DevTerminal::launchEntityValidateException($directory, "Owner drive dir is not empty!", null, __LINE__, __METHOD__);
			}
		}
		// DevTerminal::EOL(1);
		// DevTerminal::Error('<T5>ALL IS OK', 2);
		// die();
	}

	public function printItem(Item $item, $title = null, $type = 'Info') {
		$title ??= 'Informations';
		if(!$item->isDefaultModeContext()
			// && in_array($this->getId(), [112,94])
		) {
			$error_track = ' <#<INVALID>>>';
			$check_track = ' <S<valid>>>';
			$item_ownerdir = $item->getOwnerdir();
			$item_owner_ownerdir = empty($item->getOwner()) ? null : $item->getOwner()->getOwnerdir();
			$item_parent_ownerdir = empty($item->getParent()) ? null : $item->getParent()->getOwnerdir();
			$item_rootparent_ownerdir = empty($item->getRootparent()) ? null : $item->getRootparent()->getOwnerdir();
			$item_parent_owner = empty($item->getParent()) ? null : $item->getParent()->getOwner();
			DevTerminal::EOL(1);
			$valid = !$item->isValidName() ? $error_track : $check_track;
			DevTerminal::$type('<T5>'.$title.' Id#'.json_encode($item->getId()).' '.$item->getShortname().' '.json_encode($item->getName()).$valid.' / inputname: '.json_encode($item->getInputname()).':');
			$valid = !$item->isValidOwnerdir() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Ownerdir              -> '.(empty($item_ownerdir) ? 'not found' : $item_ownerdir->getShortname().' '.json_encode($item_ownerdir->getName())).$valid);
			$valid = $item->canHaveParent() && !$item_ownerdir->isValidParent() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Ownerdir parent       -> '.(empty($item_ownerdir) || empty($item_ownerdir->getParent()) ? 'not found' : $item_ownerdir->getParent()->getShortname().' '.json_encode($item_ownerdir->getParent()->getName())).$valid);
			$valid = $item->canHaveParent() && !$item_ownerdir->isValidOwner() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Ownerdir owner        -> '.(empty($item_ownerdir) || empty($item_ownerdir->getOwner()) ? 'not found' : $item_ownerdir->getOwner()->getShortname().' '.json_encode($item_ownerdir->getOwner()->getName())).$valid);
			DevTerminal::$type('<T5>- ----------------------------------------');
			$valid = !$item->isValidOwner() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Owner                 -> '.(empty($item->getOwner()) ? 'not found' : $item->getOwner()->getShortname().' '.json_encode($item->getOwner()->getName())).$valid);
			$valid = !$item->getOwner()->isValidParent() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Owner parent          -> '.(empty($item->getOwner()) || empty($item->getOwner()->getParent()) ? 'not found' : $item->getOwner()->getParent()->getShortname().' '.json_encode($item->getOwner()->getParent()->getName())).$valid);
			$valid = !$item->getOwner()->isValidOwnerdir() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Owner ownerdir        -> '.(empty($item->getOwner()) || empty($item_owner_ownerdir) ? 'not found' : $item_owner_ownerdir->getShortname().' '.json_encode($item_owner_ownerdir->getName())).$valid);
			DevTerminal::$type('<T5>- ----------------------------------------');
			$valid = !$item->isValidRootparent() ? $error_track : $check_track;
			if($valid && !empty($item->getRootparent())) {
				DevTerminal::$type('<T5>- Root parent           -> '.(empty($item->getRootparent()) ? 'not found' : $item->getRootparent()->getShortname().' '.json_encode($item->getRootparent()->getName())).$valid);
				$valid = !$item->getRootparent()->isValidOwner() ? $error_track : $check_track;
				DevTerminal::$type('<T5>- Root parent owner     -> '.(empty($item->getRootparent()) || empty($item->getRootparent()->getOwner()) ? 'not found' : $item->getRootparent()->getOwner()->getShortname().' '.json_encode($item->getRootparent()->getOwner()->getName())).$valid);
				$valid = !$item->getRootparent()->isValidOwnerdir() ? $error_track : $check_track;
				DevTerminal::$type('<T5>- Root parent ownerdir  -> '.(empty($item->getRootparent()) || empty($item_rootparent_ownerdir) ? 'not found' : $item_rootparent_ownerdir->getShortname().' '.json_encode($item_rootparent_ownerdir->getName())).$valid);
			}
			DevTerminal::$type('<T5>- ----------------------------------------');
			$valid = !$item->isValidParent() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Parent                -> '.(empty($item->getParent()) ? 'not found' : $item->getParent()->getShortname().' '.json_encode($item->getParent()->getName())).$valid);
			if($valid && !empty($item->getRootparent())) {
				$valid = !$item->getParent()->isValidOwner() ? $error_track : $check_track;
				DevTerminal::$type('<T5>- Parent owner          -> '.(empty($item->getParent()) || empty($item_parent_owner) ? 'not found' : $item_parent_owner->getShortname().' '.json_encode($item_parent_owner->getName())).$valid);
				$valid = !$item->getParent()->isValidOwnerdir() ? $error_track : $check_track;
				DevTerminal::$type('<T5>- Parent ownerdir       -> '.(empty($item->getParent()) || empty($item_parent_ownerdir) ? 'not found' : $item_parent_ownerdir->getShortname().' '.json_encode($item_parent_ownerdir->getName())).$valid);
			}
			$valid = !$item->isValidRoot() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Root                  -> '.json_encode($item->isRoot()).$valid);
			$valid = !$item->isValidOrphan() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Orphan                -> '.json_encode($item->isOrphan()).$valid);
			DevTerminal::$type('<T5>- Level                 -> '.json_encode($item->getLvl()));
			$valid = !$item->isValidPathname() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- pathname              -> '.json_encode($item->getPathname()).$valid);
			$valid = !$item->isValidPathslug() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- pathslug              -> '.json_encode($item->getPathslug()).$valid);
			DevTerminal::$type('<T5>- ----------------------------------------');
			$valid = !$item->isValidRootDirs() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Rootdirs:'.($item->getRootdirs()->isEmpty() ? ' EMPTY!' : '').$valid);
			foreach ($item->getRootdirs() as $key => $rootdir) {
				DevTerminal::$type('<T5>- Rootdir '.$key.'             -> '.$rootdir->getShortname().' '.json_encode($rootdir->getName()));
			}
			DevTerminal::$type('<T5>- ----------------------------------------');
			$valid = !$item->isValidOwners() ? $error_track : $check_track;
			DevTerminal::$type('<T5>- Owners:'.($item->getOwners()->isEmpty() ? ' EMPTY!' : '').$valid);
			foreach ($item->getOwners() as $key => $owner) {
				DevTerminal::$type('<T5>- Owner '.$key.'               -> '.$owner->getShortname().' '.json_encode($owner->getName()));
			}
			// if($item->getOwners()->isEmpty()) {
			// 	$item->checkOwners();
			// 	DevTerminal::$type('<T5>- CHECKED Owners:'.($item->getOwners()->isEmpty() ? ' EMPTY!' : ''));
			// 	foreach ($item->getOwners() as $key => $owner) {
			// 		DevTerminal::$type('<T5>- Owner '.$key.'         -> '.$owner->getShortname().' '.json_encode($owner->getName()));
			// 	}
			// }
			DevTerminal::EOL(1);
		}
	}

}