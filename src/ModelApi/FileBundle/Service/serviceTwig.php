<?php
namespace ModelApi\FileBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Service\serviceItem;
// PagewebBundle
use ModelApi\PagewebBundle\Service\TwigDatabaseLoader;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
// use \DateTime;
use \ReflectionClass;

class serviceTwig extends serviceItem {

	const ENTITY_CLASS = Twig::class;

	// /**
	//  * Get new entity
	//  * @param array $options = []
	//  * @param callable $closure = null
	//  * @return entity | null
	//  */
	// public function createNew($options = [], $closure = null) {
	// 	return $this->serviceEntities->createNew(static::ENTITY_CLASS, $options, $closure);
	// }


	/**
	 * Get Twig's Directorys by pathname
	 * @param string &$pathname
	 * @param Twig $twig = null
	 * @param boolean $addChilds = false
	 * @return Array <Directory>
	 */
	public function getTwigDirectorys(&$pathname, Twig $twig, $addChilds = false) {
		return $this->container->get(FileManager::class)->getEntityDirectorys($pathname, $twig, $addChilds);
	}

	/**
	 * Get Twig's Directory by pathname
	 * @param string &$pathname
	 * @param Twig $twig = null
	 * @param boolean $addChilds = false
	 * @return Directory | null
	 */
	public function getTwigDirectory(&$pathname, Twig $twig, $addChilds = false) {
		$directorys = $this->getTwigDirectorys($pathname, $twig, $addChilds);
		return count($directorys) ? reset($directorys) : null;
	}


	public function getAllCheckeds($repair = null) {
		$checkeds = $this->getRepository()->findAll();
		foreach ($checkeds as $key => $checked) {
			$checkeds[$key] = $this->getChecked($checked, !(empty($repair)) && ($checked->getSlug() === $repair || $checked->getId() === $repair));
		}
		return $checkeds;
	}

	public function getChecked($entity, $repair = false) {
		if(is_integer($entity)) $entity = $this->getRepository()->find($entity);
		if(is_string($entity)) $entity = $this->getRepository()->findOneBySlug($entity);
		if(!($entity instanceOf Twig)) throw new NotFoundHttpException(static::ENTITY_CLASS." not found");
		$this->serviceEntities->preUpdateForForm($entity);
		$status = 0;
		// Check
		$validator = $this->container->get('validator');
		$violations = $validator->validate($entity);
		if(count($violations)) $status = 2;
		// if Flush
		$checked = $repair;
		$repaired = false;
		if($repair && $status > 1) {
			$checked = true;
			foreach ($violations as $violation) {
				switch ($violation->getPropertyPath()) {
					case 'bundlepathnameValid':
						$entity->defineBundlepathname();
						break;
				}
			}
			$violations = $validator->validate($entity);
			if(count($violations)) {
				// Errors found again!!!
				$repaired = false;
				$status = 3;
			} else {
				// Errors repaired!
				$this->serviceEntities->getEntityManager()->flush();
				$repaired = true;
				$status = 1;
				// $this->container->get(TwigDatabaseLoader::class)->clearTwigCache();
			}
		}
		return [
			'entity' => $entity,
			'status' => $status,
			'checked' => $checked,
			'repaired' => $repaired,
			'violations' => $violations,
		];
	}

}