<?php
namespace ModelApi\EventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;

use ModelApi\EventBundle\Entity\Event;


class DefaultController extends Controller {

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Event",
	 *    description="Get Events",
	 *    output = { "class" = Event::class, "collection" = true, "groups" = {"Event"} },
	 *    views = { "default", "Event" }
	 * )
	 * @Rest\View(serializerGroups={"base","Event"})
	 * @Rest\Get("/events", name="modelapi-getevents")
	 */
	public function getItemsAction(Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$events = $em->getRepository(Event::class)->findAll();
		return $events;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Event",
	 *    description="Get Eventdates by start",
	 *    output = { "class" = Event::class, "collection" = true, "groups" = {"Event", "Eventdate"} },
	 *    views = { "default", "Event", "Eventdate" }
	 * )
	 * @Rest\View(serializerGroups={"base","Event","Eventdate"})
	 * @Rest\Get("/eventdates/bystart/{start}", name="modelapi-geteventdates-bystart")
	 */
	public function getEventdatesbyStartAction(Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$events = $em->getRepository(Event::class)->findAll();
		return $events;
	}



}