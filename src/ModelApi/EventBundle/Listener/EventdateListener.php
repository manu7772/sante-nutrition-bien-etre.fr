<?php
namespace ModelApi\EventBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Service\serviceEvent;
use ModelApi\EventBundle\Service\serviceEventdate;
// MarketBundle
use ModelApi\MarketBundle\Entity\Baseprod;
use ModelApi\MarketBundle\Service\serviceBaseprod;

// use ModelApi\DevprintsBundle\Service\DevTerminal;

use \Exception;

class EventdateListener {

	protected $container;
	protected $clearAgendaCache;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->clearAgendaCache = false;
		return $this;
	}

	public function onFlush(OnFlushEventArgs $event) {
		$this->em = $event->getEntityManager();
		$this->uow = $this->em->getUnitOfWork();
		// Attach Season if new Eventdate
		foreach ($this->uow->getScheduledEntityInsertions() as $item) {
			
		}
		$this->clearAgendaCache = false;
		foreach (array_merge($this->uow->getScheduledEntityInsertions(), $this->uow->getScheduledEntityUpdates(), $this->uow->getScheduledEntityDeletions()) as $item) {
			if($item instanceOf Eventdate) {
				$this->clearAgendaCache = true;
				break;
			}
		}
		return $this;
	}

	public function postFlush(PostFlushEventArgs $event) {
		if($this->clearAgendaCache) $this->container->get(serviceEvent::class)->recreateMainsCalendarCaches();
		$this->clearAgendaCache = false;
		return $this;
	}



}