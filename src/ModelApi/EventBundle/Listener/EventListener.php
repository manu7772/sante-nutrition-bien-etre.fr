<?php
namespace ModelApi\EventBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Service\serviceEvent;
use ModelApi\EventBundle\Service\serviceJourney;
// MarketBundle
use ModelApi\MarketBundle\Entity\Baseprod;
use ModelApi\MarketBundle\Service\serviceBaseprod;

// use ModelApi\DevprintsBundle\Service\DevTerminal;

use \Exception;

class EventListener {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	protected $serviceEntities;
	// protected $serviceEvent;
	// protected $serviceBaseprod;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		// $this->serviceEvent = $this->container->get(serviceEvent::class);
		// $this->serviceBaseprod = $this->container->get(serviceBaseprod::class);
		return $this;
	}

	// /**
	//  * @ORM\PostLoad()
	//  */
	// public function postLoad(Event $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	/**
	 * @ORM\PrePersist()
	 */
	public function prePersist(Event $entity, LifecycleEventArgs $args) {
		$entity->checkAllDates();
		$baseprods = $this->serviceEntities->getRepository(Baseprod::class)->findAll();
		foreach ($baseprods as $baseprod) {
			$classes = $baseprod->getAutoevents(false);
			if(in_array($entity->getClassname(), $classes)) $baseprod->addAddedAutoEvent($entity);
		}
		$this->container->get(serviceJourney::class)->checkEventToJourneys($entity);
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdate(Event $entity, PreUpdateEventArgs $args) {
		$entity->checkAllDates();
		$this->container->get(serviceJourney::class)->checkEventToJourneys($entity);
		return $this;
	}

	// /**
	//  * @ORM\PostPersist()
	//  */
	// public function postPersist(Event $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	// /**
	//  * @ORM\PostUpdate()
	//  */
	// public function postUpdate(Event $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	/**
	 * @ORM\PreRemove()
	 */
	public function preRemove(Event $entity, LifecycleEventArgs $args) {
		$entity->setProducts(new ArrayCollection());
		$entity->setAddedAutoProducts(new ArrayCollection());
		return $this;
	}

	// /**
	//  * @ORM\PostRemove()
	//  */
	// public function postRemove(Event $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }






}