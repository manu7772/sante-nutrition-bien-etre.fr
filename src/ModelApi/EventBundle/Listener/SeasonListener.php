<?php
namespace ModelApi\EventBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
// use ModelApi\AnnotBundle\Annotation as Annot;
// use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

// EventBundle
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Entity\Season;
use ModelApi\EventBundle\Service\serviceSeason;

use \Exception;

class SeasonListener {

	protected $container;
	protected $serviceSeason;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceSeason = $this->container->get(serviceSeason::class);
		return $this;
	}

	// /**
	//  * @ORM\PostLoad()
	//  */
	// public function postLoad(Season $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	// /**
	//  * @ORM\PrePersist()
	//  */
	// public function prePersist(Season $entity, LifecycleEventArgs $args) {
	// 	// Remove all deleted dates
	// 	// $this->serviceSeason->updateSeasonDates($entity);
	// 	return $this;
	// }

	// /**
	//  * @ORM\PreUpdate()
	//  */
	// public function preUpdate(Season $entity, PreUpdateEventArgs $args) {
	// 	// Remove all deleted dates
	// 	// $this->serviceSeason->updateSeasonDates($entity);
	// 	return $this;
	// }

	// /**
	//  * @ORM\PostPersist()
	//  */
	// public function postPersist(Season $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	// /**
	//  * @ORM\PostUpdate()
	//  */
	// public function postUpdate(Season $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	/**
	 * @ORM\PreRemove()
	 */
	public function preRemove(Season $entity, LifecycleEventArgs $args) {
		// die('PreRemove!!!');
		$em = $args->getEntityManager();
		// $uow = $em->getUnitOfWork();
		// Remove all dates of season
		foreach ($entity->getJourneys() as $journey) {
			foreach ($journey->getEventdates() as $eventdate) {
				$em->remove($eventdate);
			}
			$em->remove($journey);
			// $uow->computeChangeSet($em->getClassMetadata(get_class($journey)), $journey);
		}
		// $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
		return $this;
	}

	// /**
	//  * @ORM\PostRemove()
	//  */
	// public function postRemove(Season $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }






}