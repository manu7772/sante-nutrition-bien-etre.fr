<?php
namespace ModelApi\EventBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
// use ModelApi\AnnotBundle\Annotation as Annot;
// use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

// EventBundle
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Entity\Parc;
use ModelApi\EventBundle\Service\serviceJourney;
use ModelApi\EventBundle\Service\serviceSeason;
use ModelApi\EventBundle\Service\serviceEventdate;

use \Exception;
use \DateTime;

class JourneyListener {

	protected $container;
	protected $postPersistToggleReflush;
	protected $postupdateToggleReflush;
	// protected $serviceJourney;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->postPersistToggleReflush = true;
		$this->postupdateToggleReflush = true;
		// $this->serviceJourney = $this->container->get(serviceJourney::class);
		return $this;
	}

	// /**
	//  * @ORM\PostLoad()
	//  */
	// public function postLoad(Journey $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }

	/**
	 * @ORM\PrePersist()
	 */
	public function prePersist(Journey $entity, LifecycleEventArgs $args) {
		// $em = $args->getEntityManager();
		// $uow = $em->getUnitOfWork();

		$entity->checkItems(true);
		$entity->computeYear();

		$parc = $entity->getParc();
		if($parc instanceOf Parc && !$parc->hasDaymark($entity->getDaymark())) {
			$eventdate = $this->container->get(serviceEventdate::class)->createNew();
			// echo('<pre><h3>Journey START</h3>');var_dump($entity->getStart());echo('<pre>');
			$eventdate->setStart($entity->getStart(), false);
			// echo('<pre><h3>Eventdate START</h3>');var_dump($eventdate->getStart());echo('<pre>');
			$eventdate->setEnd($entity->getEnd(), false);
			$parc->addEventdate($eventdate);
			// echo('<pre><h3>Eventdate START</h3>');var_dump($eventdate->getStart());echo('<pre>');
			$entity->addEventdate($eventdate);
			// echo('<pre><h3>Journey START</h3>');var_dump($entity->getStart());die('<pre>');
			// $entity->setParcstart($parc->getStart());
			// $entity->setParcend($parc->getEnd());
			// $em->persist($eventdate);
			// $uow->computeChangeSet($em->getClassMetadata(get_class($eventdate)), $eventdate);
		}
		// if($entity->getSeasons()->isEmpty()) {
		// 	// $this->container->get(serviceSeason::class)->checkSeasons(null, false);
		// 	$this->container->get(serviceJourney::class)->checkJourneysSeasons([$entity], false);
		// 	// foreach ($entity->getSeasons() as $season) $uow->computeChangeSet($em->getClassMetadata(get_class($season)), $season);
		// 	// $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
		// }
		// $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdate(Journey $entity, PreUpdateEventArgs $args) {
		$em = $args->getEntityManager();
		// $uow = $em->getUnitOfWork();

		$entity->checkItems(true);
		$entity->computeYear();

		if($args->hasChangedField('parc')) {
			$new_parc = $entity->getParc();
			$old_parc = $args->getOldValue('parc');
			if($old_parc instanceOf Parc) {
				$eventdates = $old_parc->getAndRemoveEventdateByDaymark($entity->getDaymark(), true);
				if($eventdates->count() > 1) throw new Exception("Error ".__METHOD__."(): more than one eventdate found. Please check your code!", 1);
				$eventdate = $eventdates->first();
				if($eventdate instanceOf Eventdate && $eventdate->getType() === 'Parc') $em->remove($eventdate);
				// $new_eventdate = $this->container->get(serviceEventdate::class)->createNew();
				// $uow->persist($new_eventdate);
				// $new_eventdate->setStart($entity->getStart(), false);
				// $new_eventdate->setEnd($entity->getEnd(), false);
				// $new_parc->addEventdate($new_eventdate);
				// $entity->addEventdate($new_eventdate);
			}
			if($new_parc instanceOf Parc) $this->prePersist($entity, $args);
		}
		// Enabled / Disabled --> propagation on Eventdates
		// if($args->hasChangedField('enabled') || $args->hasChangedField('softdeleted') || $args->hasChangedField('visibleEvenIfInactive')) {
		// 	$enable = ($entity->isActive() || $entity->getVisibleEvenIfInactive()) && !$entity->isSoftdeleted();
		// 	$CMDEventdate = $em->getClassMetadata(Eventdate::class);
		// 	foreach ($entity->getEventdates() as $eventdate) {
		// 		if($enable && $eventdate->isDisabled()) {
		// 			// $uow->scheduleForUpdate($eventdate);
		// 			$eventdate->setEnabled();
		// 			// $uow->recomputeSingleEntityChangeSet($CMDEventdate, $eventdate);
		// 			// $uow->computeChangeSet($CMDEventdate, $eventdate);
		// 		} else if(!$enable && $eventdate->isEnabled()) {
		// 			// $uow->scheduleForUpdate($eventdate);
		// 			$eventdate->setDisabled();
		// 			// $uow->recomputeSingleEntityChangeSet($CMDEventdate, $eventdate);
		// 			// $uow->computeChangeSet($CMDEventdate, $eventdate);
		// 		}
		// 	}
		// }
		return $this;
	}

	/**
	 * @ORM\PostPersist()
	 */
	public function postPersist(Journey $entity, LifecycleEventArgs $args) {
		if($entity->getSeasons()->isEmpty()) {
			// $this->container->get(serviceSeason::class)->checkSeasons(null, false);
			$this->container->get(serviceJourney::class)->checkJourneysSeasons([$entity], $this->postPersistToggleReflush);
			$this->postPersistToggleReflush = !$this->postPersistToggleReflush;
			// foreach ($entity->getSeasons() as $season) $uow->computeChangeSet($em->getClassMetadata(get_class($season)), $season);
			// $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
		}
		return $this;
	}

	/**
	 * @ORM\PostUpdate()
	 */
	public function postUpdate(Journey $entity, LifecycleEventArgs $args) {
		if($this->postupdateToggleReflush) {
			$em = $args->getEntityManager();
			$enable = $entity->isActive();
			if($entity->getVisibleEvenIfInactive()) $enable = true;
			if($entity->isSoftdeleted()) $enable = false;
			// if($entity->getSoftdeleted() instanceOf DateTime && $entity->getSoftdeleted() <= new DateTime('NOW')) $enable = false;
			$CMDEventdate = $em->getClassMetadata(Eventdate::class);
			foreach ($entity->getEventdates() as $eventdate) {
				if($enable) $eventdate->setEnabled();
					else $eventdate->setDisabled();
			}
			$this->postupdateToggleReflush = !$this->postupdateToggleReflush;
			$em->flush();
		}
		return $this;
	}

	/**
	 * @ORM\PreRemove()
	 */
	public function preRemove(Journey $entity, LifecycleEventArgs $args) {
		$em = $args->getEntityManager();
		foreach ($entity->getEventdates() as $eventdate) {
			$em->remove($eventdate);
		}
		return $this;
	}

	// /**
	//  * @ORM\PostRemove()
	//  */
	// public function postRemove(Journey $entity, LifecycleEventArgs $args) {
	// 	return $this;
	// }






}