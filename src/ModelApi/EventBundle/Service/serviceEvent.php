<?php
namespace ModelApi\EventBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use JMS\Serializer\SerializationContext;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceKernel;
// use ModelApi\BaseBundle\Service\serviceBaseMain;
// use ModelApi\BaseBundle\Service\serviceEntities;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
// FileBundle
// use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Service\serviceItem;
// EventBundle
use ModelApi\EventBundle\Entity\Calendar;
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Entity\Eventdate;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \Symfony\Component\Finder\Finder;

use \DateTime;
use \ReflectionClass;

class serviceEvent extends serviceItem {

	const ENTITY_CLASS = Event::class;
	const DIR_CALENDAR = 'cache_calendar';

	protected $serviceCache;

	public function __construct(ContainerInterface $container) {
		parent::__construct($container);
		$this->serviceCache = $this->container->get(serviceCache::class);
		return $this;
	}


	public function removePastEventdates($events) {
		$now = new DateTime();
		if($events instanceOf Event) $events = [$events];
		foreach ($events as $event) if($event instanceOf Event) {
			foreach ($event->getProducts() as $product) {
				if($product->getEnd() instanceOf DateTime && $product->getEnd() < $now) {
					$event->removeProduct($product);
				}
			}
		}
	}

	public function getDateIntervalByYearAndMonth($year = null, $month = null, $asObjects = true, $notObsoleteMonths = false) {
		// $serviceContext = $this->container->get(serviceContext::class);
		if(!is_string($year) && !preg_match('/^20(1|2|3)\\d$/', $year)) {
			$year = serviceContext::getContextYear();
			// $year = serviceContext::getCurrentYear();
		}
		// Has year, so try ton get month
		if(!is_integer($month)) $month = (integer)$month;
		if(empty($month) || $month < 1 || $month > 12) {
			$m1 = '01'; $m2 = '12';
		} else {
			$month = $month < 10 ? '0'.$month : ''.$month;
			$m1 = $month; $m2 = $month;
		}
		// Remove passed months / by datetime
		if(true === $notObsoleteMonths) $notObsoleteMonths = serviceContext::getCurrentDate();
		if(is_string($notObsoleteMonths)) $notObsoleteMonths = new DateTime($notObsoleteMonths);
		if($notObsoleteMonths instanceOf DateTime) {
			$limitY = $notObsoleteMonths->format("Y");
			if((integer)$limitY = (integer)$year) {
				$notObsoleteMonths = $notObsoleteMonths->format('m');
				if(is_string($notObsoleteMonths) && (integer)$notObsoleteMonths > (integer)$m1) $m1 = $notObsoleteMonths;
			}
		}
		// last day of month
		$d1 = '01';
		$d2 = new DateTime($year.'-'.$m2.'-01'); 
		$d2 = $d2->format('t');
		$start = $asObjects ? new DateTime($year.'-'.$m1.'-'.$d1.'T00:00:00') : $year.'-'.$m1.'-'.$d1.'T00:00:00';
		$end = $asObjects ? new DateTime($year.'-'.$m2.'-'.$d2.'T23:59:59') : $year.'-'.$m2.'-'.$d2.'T23:59:59';
		return ['start' => $start, 'end' => $end];
	}


	/*************************************************************************************************/
	/*** SFCRON TASK
	/*************************************************************************************************/

	public function sfcron_task(DateTime $start, $force = false) {
		if($force || preg_match('/0$/', $start->format('i'))) {
			$this->recreateMainsCalendarCaches();
			return ['Refresh calendars data' => true];
		}
		return ['Refresh calendars data' => false];
	}

	/*************************************************************************************************/
	/*** DAYMARKS
	/*************************************************************************************************/

	public static function getDaymarkFormat($dateOrString, $exceptionIfInvalid = true) {
		$daymark = $dateOrString instanceOf DateTime ? $dateOrString->format('Y-m-d') : $dateOrString;
		if(!static::isValidDaymark($daymark)) {
			if($exceptionIfInvalid) throw new Exception("Error ".__METHOD__."(): daymark ".json_encode($daymark)." is invalid!", 1);
			return false;
		}
		return $daymark;
	}

	public static function isValidDaymark($daymark) {
		return is_string($daymark) && preg_match('/^[\\d]{4}-[\\d]{2}-[\\d]{2}$/', $daymark);
	}




	/*************************************************************************************************/
	/*** CACHE FOR CALENDAR
	/*************************************************************************************************/

	public function recreateMainsCalendarCaches() {
		// $serviceLanguage = $this->container->get(serviceLanguage::class);
		// $locales = $serviceLanguage->getLanguagesNames();
		// $serviceContext = $this->container->get(serviceContext::class);
		// foreach ($locales as $locale) {
			// Annually
			$this->getCachedCalendar(serviceContext::getCurrentYear(), null, true, true);
			$this->getCachedCalendar(serviceContext::getCurrentYear(), null, true, false);
			// Monthly
			for ($i=1; $i <= 12; $i++) {
				$month = $i < 10 ? '0'.$i : ''.$i;
				$this->getCachedCalendar(serviceContext::getCurrentYear(), $month, true, false);
			}
		// }
		$this->container->get(serviceFlashbag::class)->addFlashToastr('success', 'Tous agendas mis à jour.');
		return $this;
	}

	public function getBaseCalendarCacheName($year = null, $month = null, $notObsoleteMonths = false) {
		$date = $this->getDateIntervalByYearAndMonth($year, $month, false, $notObsoleteMonths);
		// $date['contextdate'] = $this->container->get(serviceContext::class)->getContextDate(false);
		return preg_replace('/[:]+/', '', implode('-', $date));
	}

	public function clearCachedCalendar($year = null, $month = null, $notObsoleteMonths = false, $hard = false) {
		$cacheId = $this->getBaseCalendarCacheName($year, $month, $notObsoleteMonths);
		$this->serviceCache->clearCacheData(
			$cacheId,
			static::DIR_CALENDAR,
			serviceCache::CONTEXT_LANG,
			null,
			$hard
		);
		return $this;
	}

	public function getCachedCalendar($year = null, $month = null, $refresh = false, $notObsoleteMonths = false) {
		$cacheId = $this->getBaseCalendarCacheName($year, $month, $notObsoleteMonths);
		$repository = $this->serviceEntities->getRepository(Eventdate::class);
		$cacheCalendar =  $this->serviceCache->getCacheSerialized(
			$cacheId,
			function() use ($repository, $year, $month, $notObsoleteMonths) {
				return $repository->findParcsForCalendars($year, $month, $notObsoleteMonths);
			},
			['groups' => ['calendar'], 'format' => 'json'],
			static::DIR_CALENDAR,
			serviceCache::CONTEXT_LANG,
			null,
			Calendar::CACHE_LIFETIME,
			$refresh
		);
		return new Calendar($cacheCalendar);
	}


}