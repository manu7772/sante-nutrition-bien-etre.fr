<?php
namespace ModelApi\EventBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\OnClearEventArgs;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Service\serviceSeason;
use ModelApi\EventBundle\Form\Type\JourneyParcsType;

// use \DateTime;
use \ReflectionClass;
use \Exception;

class serviceJourney implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Journey::class;

	protected $container;
	protected $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}

	public function checkJourneysParcs($journeys = null) {
		if(empty($journeys)) $journeys = $this->getRepository()->findAll();
		foreach ($journeys as $journey) {
			$journey->getParc();
		}
		return $this;
	}

	public function checkJourneysSeasons($journeys = null, $flush = true) {
		// if(empty($journeys)) $journeys = $this->getRepository()->findAll();
		return $this->container->get(serviceSeason::class)->checkSeasons(null, $journeys, $flush);
	}

	public function createNewWithDaymark($daymark, $addEventdates = []) {
		if($addEventdates instanceOf Eventdate) $addEventdates = [$addEventdates];
		if(count($addEventdates)) return $this->createNew([$daymark], function($jny) use ($addEventdates) {
			foreach ($addEventdates as $eventdate) $jny->addEventdate($eventdate);
		});
		return $this->createNew([$daymark]);
	}

	public function checkEventToJourneys(Event $event) {
		// Add Journeys
		$persist = [];
		// $journeys = $this->getRepository()->findDaymarksIndexed(array_keys($event->getAllDates()));
		$journeys = $event->getJourneys(true);
		foreach ($event->getEventdates() as $eventdate) {
			foreach ($eventdate->getAllDates() as $daymark => $date) {
				if(!array_key_exists($daymark, $journeys)) {
					// Journey does not exist: create it
					$journey = $this->getRepository()->findOneByDaymark($daymark);
					if(empty($journey)) {
						$journey = $this->createNewWithDaymark($daymark);
						$journey->addEventdate($eventdate);
						if($journey->isValid()) {
							$journeys[$journey->getDaymark()] = $journey;
							$persist[] = $journey->getDaymark();
						} else {
							throw new Exception("Error creating Journey ".json_encode($journey->getDaymark()).": ".$journey->getReasonInvalidity(), 1);
						}
					} else {
						$journey->addEventdate($eventdate);
						$journeys[$journey->getDaymark()] = $journey;
					}
				} else {
					$journeys[$daymark]->addEventdate($eventdate);
				}
				// $journeys[$daymark]->computeYear();
			}
		}
		foreach ($persist as $daymark) {
			$this->getEntityManager()->persist($journeys[$daymark]);
		}
		// Remove Journeys
		foreach ($event->getEventdates() as $eventdate) {
			foreach ($eventdate->getJourneys() as $journey) {
				$journey->checkDate($eventdate);
				if(!$journey->isValid()) $this->delete($journey, false, true);
			}
		}
		return $this;
	}

}