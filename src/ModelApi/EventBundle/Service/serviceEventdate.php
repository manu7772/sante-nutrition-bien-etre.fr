<?php
namespace ModelApi\EventBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use JMS\Serializer\SerializationContext;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceKernel;
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Entity\Season;
use ModelApi\EventBundle\Service\serviceEvent;
use ModelApi\EventBundle\Service\serviceSeason;

use \DateTime;
use \ReflectionClass;

class serviceEventdate implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Eventdate::class;

	protected $container;
	protected $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}



	public function checkEventdates($eventdates = null) {
		$em = $this->getEntityManager();
		$limit = $this->container->get(serviceSeason::class)->getOlderDate();
		// Remove Evendates not in season
		$olds = $this->getRepository()->findOlderThan($limit);
		foreach ($olds as $eventdate) {
			$em->remove($eventdate);
		}
		$em->flush();
		// 
		$eventdates ??= [];
		foreach ($eventdates as $eventdate) {
			$this->checkEventdate($eventdate, false);
		}
		if(count($eventdates)) $em->flush();
		return $this;
	}

	public function checkEventdate(Eventdate $eventdate, $flush = true) {
		// 
		return $this;
	}


}