<?php
namespace ModelApi\EventBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\OnClearEventArgs;

use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
// EventBundle
use ModelApi\EventBundle\Entity\Season;
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Service\serviceEventdate;
use ModelApi\EventBundle\Service\serviceJourney;

use \DateTime;
use \ReflectionClass;
use \Exception;

class serviceSeason implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Season::class;

	protected $container;
	protected $serviceEntities;
	protected $serviceJourney;
	protected $config;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->serviceJourney = $this->container->get(serviceJourney::class);
		$this->config = null;
		return $this;
	}

	public static function isYearValid($year) {
		return preg_match('#^(19|20)\\d{2}$#', (string)$year);
	}

	public function createNewSeason($year = null, $returnIfExist = false) {
		if(!static::isYearValid($year)) $year = $this->getRepository()->getNextYear();
		$test = $this->getRepository()->findOneByYear($year);
		if(!empty($test)) return $returnIfExist ? $test : false;
		// not found : create new
		$dates = $this->getParameterSeasonDates($year);
		return $this->createNew([$dates['start'], $dates['end']]);
	}

	public function getSeasonDatesInfo($year = null) {
		if(!empty($this->config)) return $this->config;
		$this->config = [];
		$contextYear = $this->isYearValid($year) ? $year : $this->container->get(serviceContext::class)->getContextDate('Y');
		$dates = $this->getServiceParameter('dates');
		foreach ($dates as $key => $date) {
			$time = in_array($key, ['end']) ? '23:59:59.999' : '00:00:00.000';
			$date = preg_replace('#(\\s\\/_:\\.)+#', '-', $date);
			$this->config['datetimes'][$key] = new DateTime($contextYear.'-'.$date.'T'.$time);
			$this->config['dates'][$key] = $this->config['datetimes'][$key]->format('m-d');
		}
		// Si même jour pour start et end, on recule end d'une journée
		if($this->config['dates']['start'] === $this->config['dates']['end']) {
			$this->config['datetimes']['end']->modify('-1 day');
			$this->config['datetimes']['end']->setTime('23:59:59.999');
			$this->config['dates']['end'] = $this->config['datetimes']['end']->format('m-d');
		}
		$this->config['seeason_in_same_year'] = $this->config['datetimes']['start'] < $this->config['datetimes']['end'];
		return $this->config;
	}

	public function getParameterSeasonDates($start = null) {
		if($start instanceOf DateTime) $year = $start->format('Y');
		if(is_string($start)) {
			if($this->isYearValid($start)) {
				$year = $start;
			} else {
				$year = new DateTime($start);
				$year = $year->format('Y');
			}
		}
		if(!$this->isYearValid($year)) throw new Exception("Error ".__METHOD__."(): start year parameter is invalid!", 1);
		$dates = $this->getSeasonDatesInfo();
		$seasonDates = [];
		if($dates['seeason_in_same_year']) {
			$seasonDates['start'] = new DateTime($year.'-'.$dates['dates']['start'].'T00:00:00.000');
			$seasonDates['end'] = new DateTime($year.'-'.$dates['dates']['end'].'T23:59:59.999');
		} else {
			$year = intval($year);
			$seasonDates['start'] = new DateTime($year.'-'.$dates['dates']['start'].'T00:00:00.000');
			$seasonDates['end'] = new DateTime(($year+1).'-'.$dates['dates']['end'].'T23:59:59.999');
		}
		// echo('<div class="well"><p>Season '.$year." dates :</p><p>Start : ".$seasonDates['start']->format('d/m/Y')."</p><p>End : ".$seasonDates['end']->format('d/m/Y')."</p></div>");
		return $seasonDates;
	}

	/**
	 * Get start date of older season
	 * @param mixed $default = '2000-01-01'
	 * @return DateTime
	 */
	public function getOlderDate($default = '2000-01-01') {
		$season = $this->getRepository()->findOlderSeasonDate();
		if(empty($season)) return $default instanceOf DateTime ? $default : new DateTime($default);
		return $season;
	}

	public function checkSeasons($id = null, $journeys = null, $flush = true) {
		if($id instanceOf Season) {
			$seasons = [$id];
		} else {
			$seasons = empty($id) ? $this->getRepository()->findAll() : $this->getRepository()->findById($id);
		}
		if(empty($journeys)) $journeys = $this->serviceJourney->getRepository()->findAll();
		$used = new ArrayCollection();
		$used_years = [];
		foreach ($seasons as $season) {
			$used_years[] = $season->getYear();
			foreach ($journeys as $journey) {
				if($season->isJourneyValidForSeason($journey)) {
					$season->addJourney($journey);
					if(!$used->contains($journey)) $used->add($journey);
						else throw new Exception("Error ".__METHOD__."(): one Journey is allready in one other Season!", 1);
				}
			}
		}
		$new_seasons = [];
		if($used->count() < count($journeys)) {
			// some journeys have no season!!!
			foreach ($journeys as $journey) {
				$year = $journey->getStart()->format('Y');
				if(!$used->contains($journey) && !in_array($year, $used_years)) {
					if(array_key_exists($year, $new_seasons)) {
						$test = $new_seasons[$year];
					} else {
						$test = $this->createNewSeason($year, false);
						if(false === $test) throw new Exception("Error ".__METHOD__."(): try to create new Season that allready exists!", 1);
						// if(array_key_exists($year, $new_seasons)) throw new Exception("Error ".__METHOD__."(): try to create new Season that has allready been created!", 1);
						$new_seasons[$year] = $test;
					}
					$new_seasons[$year]->addJourney($journey);
				}
			}
		}
		if($flush) $this->getEntityManager()->flush();
		return ['found' => $seasons, 'new' => $new_seasons];
	}

	public function checkSeason(Saison $season, $flush = true) {
		return $this->checkSeasons($season, $flush);
	}

	public function getCurrentSeasons($date = null) {
		if(!($date instanceOf DateTime)) {
			if(empty($date) || (is_string($date) && in_array($date, ['auto','context']))) $date = $this->container->get(serviceContext::class)->getContextDate(true);
			if(is_string($date)) $date = new DateTime($date);
		}
		if(!($date instanceOf DateTime)) throw new Exception("Error ".__METHOD__."(): date ".json_encode($date)." is invalid!", 1);
		return $this->getRepository()->findSeasonsInDate($date);
	}


	public function getSeasonsReport() {
		// $dates = $this->getSeasonDatesInfo();
		// $years = $this->serviceJourney->getRepository()->findYears();
		// $seasons = $this->getRepository()->findAll();
		$report = [
			'need_check' => false,
			'seasons' => [],
		];
		// Seasons
		foreach ($this->serviceJourney->getRepository()->findYears() as $year) {
			$season = $this->getRepository()->findOneByYear($year);
			$report['need_check'] = empty($season);
			$report['seasons'][$year] = [
				'search' => $this->getParameterSeasonDates($year),
				'found' => $season,
			];
		}
		// Journeys
		foreach ($report['seasons'] as $year => $data) {
			$report['seasons'][$year]['journeys'] = $this->serviceJourney->getRepository()->findByDateInterval($data['search']['start'], $data['search']['end']);
		}
		// Eventdates
		foreach ($report['seasons'] as $year => $data) {
			$report['seasons'][$year]['nb_eventdates'] = $this->container->get(serviceEventdate::class)->getRepository()->countByDateInterval($data['search']['start'], $data['search']['end']);
		}
		return $report;
	}






	// public function joinSeasonsAllDates(Season $season) {
	// 	// Add all existing dates
	// }

	// public function createSeasonsAllDates(Season $season) {
	// 	// Create all new dates
	// }

	// public function updateSeasonDates(Season $season) {
	// 	// Remove all deleted dates
	// 	$this->joinSeasonsAllDates($season);
	// 	$this->createSeasonsAllDates($season);
	// }

	// public function removeSeasonsAllDates(Season $season) {
	// 	// Remove all dates of season
	// 	foreach ($season->getJourneys() as $journey) {
	// 		$this->getEntityManager()->remove($journey);
	// 	}
	// 	return $this;
	// }


}