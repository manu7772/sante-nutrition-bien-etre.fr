<?php
namespace ModelApi\EventBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\DeserializationContext;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\DependencyInjection\ContainerInterface;

// use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcherInterface;

use ModelApi\BaseBundle\Representation\Pagination;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Repository\BasentityRepository;

// UserBundle
// use ModelApi\UserBundle\Entity\Tier;
// use ModelApi\UserBundle\Entity\User;
// FileBundle
use ModelApi\FileBundle\Repository\ItemRepository;
// EventBundle
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Entity\Animation;
use ModelApi\EventBundle\Service\serviceEvent;

use \LogicException;
use \ReflectionClass;
use \DateTime;

/**
 * EventdateRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EventdateRepository extends BasentityRepository {

	const ELEMENT = 'eventdate'; // entity


	public function findParcsByDates($start = null, $end = null) {
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$qb = $this->qb_findParcsByDates($qb, $start, $end, ['Parc']);
		return $qb->getQuery()->getResult();
	}

	public function findParcsForCalendars($year = null, $month = null, $notObsoleteMonths = false) {
		$date = $this->container->get(serviceEvent::class)->getDateIntervalByYearAndMonth($year, $month, true, $notObsoleteMonths);
		// $date['start'] = new DateTime($year.'-05-01T00:00:00');
		// $date['end'] = new DateTime($year.'-12-31T23:59:59');
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$qb = $this->qb_findParcsByDates($qb, $date['start'], $date['end'], [], ['Atelier','Visite']);
		// return $qb->getQuery()->getResult();
		return $this->compileResults($qb->getQuery()->getResult());
	}

	public function countByDateInterval(DateTime $start = null, DateTime $end = null) {
		// $countQuery = "SELECT COUNT(element.id) FROM ".get_class($entity)." element WHERE ".$ID."element.".$property." = ".$value.implode(" ", $groupedFields);
		// $qb = $entityManager->createQueryBuilder();
		// $qb->select('count(account.id)');
		// $qb->from('ZaysoCoreBundle:Account','account');
		// $count = $qb->getQuery()->getSingleScalarResult();
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$qb->select('count('.static::ELEMENT.'.id)');
		$whr = "where";
		if($start instanceOf DateTime) {
			$qb->$whr(static::ELEMENT.'.start > :startdate')->setParameter('startdate', $start);
			$whr = "andWhere";
		}
		if($end instanceOf DateTime) {
			$qb->$whr(static::ELEMENT.'.start < :enddate')->setParameter('enddate', $end);
			$whr = "andWhere";
		}
		return $qb->getQuery()->getSingleScalarResult();
	}

	public function findOlderThan($date) {
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$qb->where(static::ELEMENT.'.start < :date')
			->setParameter('date', $date);
		return $qb->getQuery()->getResult();
	}


	protected function compileResults($results) {
		$sorteds = [];
		foreach ($results as $eventdate) {
			if(!empty($eventdate->getEvent()) && $eventdate->getEvent()->isVisibleagenda()) {
				$year = (string)$eventdate->getStart()->format('Y');
				$month = (string)$eventdate->getStart()->format('m');
				$day = (string)$eventdate->getStart()->format('d');
				if(!isset($sorteds[$year])) $sorteds[$year] = ['dates' => [], 'first' => null, 'last' => null];
				if(!isset($sorteds[$year]['dates'][$month])) $sorteds[$year]['dates'][$month] = ['dates' => [], 'first' => null, 'last' => null];
				if(!isset($sorteds[$year]['dates'][$month]['dates'][$day])) $sorteds[$year]['dates'][$month]['dates'][$day] = ['eventdates' => [], 'parc' => null, 'start' => null, 'end' => null];
				if(!isset($sorteds[$year]['dates'][$month]['dates'][$day]['eventdates'][$eventdate->getType()])) $sorteds[$year]['dates'][$month]['dates'][$day]['eventdates'][$eventdate->getType()] = [];
				$sorteds[$year]['dates'][$month]['dates'][$day]['eventdates'][$eventdate->getType()][] = $eventdate;
				// Add PARC if is parc type
				if($eventdate->getType() === 'Parc' && !isset($sorteds[$year]['dates'][$month]['dates'][$day]['parc'])) $sorteds[$year]['dates'][$month]['dates'][$day]['parc'] = $eventdate->getEvent();
				// start DAY
				if(null === $sorteds[$year]['dates'][$month]['dates'][$day]['start'] || $sorteds[$year]['dates'][$month]['dates'][$day]['start'] > $eventdate->getStart()) $sorteds[$year]['dates'][$month]['dates'][$day]['start'] = $eventdate->getStart();
				if(null === $sorteds[$year]['dates'][$month]['dates'][$day]['end'] || $sorteds[$year]['dates'][$month]['dates'][$day]['end'] < $eventdate->getEnd()) $sorteds[$year]['dates'][$month]['dates'][$day]['end'] = $eventdate->getEnd();
				// first YEAR
				if(null === $sorteds[$year]['first'] || $sorteds[$year]['first']['start'] > $eventdate->getStart()) $sorteds[$year]['first'] = ['start' => $eventdate->getStart(), 'end' => $eventdate->getEnd(), 'eventdate' => $eventdate];
				// last YEAR
				if(null === $sorteds[$year]['last'] || $sorteds[$year]['last']['end'] < $eventdate->getStart()) $sorteds[$year]['last'] = ['start' => $eventdate->getStart(), 'end' => $eventdate->getEnd(), 'eventdate' => $eventdate];
				// first MONTH
				if(null === $sorteds[$year]['dates'][$month]['first'] || $sorteds[$year]['dates'][$month]['first']['start'] > $eventdate->getStart()) $sorteds[$year]['dates'][$month]['first'] = ['start' => $eventdate->getStart(), 'end' => $eventdate->getEnd(), 'eventdate' => $eventdate];
				// last MONTH
				if(null === $sorteds[$year]['dates'][$month]['last'] || $sorteds[$year]['dates'][$month]['last']['end'] < $eventdate->getStart()) $sorteds[$year]['dates'][$month]['last'] = ['start' => $eventdate->getStart(), 'end' => $eventdate->getEnd(), 'eventdate' => $eventdate];
			}
		}
		return $sorteds;
	}


	public function qb_findParcsByDates(QueryBuilder $qb, $start = null, $end = null, $includeTypes = [], $excludeTypes = []) {
		if(is_string($start)) $start = new DateTime($start);
		if(!($start instanceOf DateTime)) {
			$start = new DateTime(serviceContext::DEFAULT_DATE);
		}
		if(is_string($end)) $end = new DateTime($end);
		if(!($end instanceOf DateTime)) {
			$end = clone $start;
			$end->setTime(23, 59, 59);
		}
		$qb->where(static::ELEMENT.'.start BETWEEN :start AND :end')
			->setParameter('start', $start)
			->setParameter('end', $end)
			->leftJoin(static::ELEMENT.'.event', 'event')
			->addSelect('event')
			;
		// include types
		foreach ($includeTypes as $type) $qb->andWhere(static::ELEMENT.'.type = :'.$type)->setParameter($type, $type);
		// exclude types
		foreach ($excludeTypes as $type) $qb->andWhere(static::ELEMENT.'.type != :'.$type)->setParameter($type, $type);
		// exclude inactives
		$this->qb_removeNotActive($qb);
		// order
		$qb->orderBy(static::ELEMENT.'.start','ASC');
		return $qb;
	}

}