<?php
namespace ModelApi\EventBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Entity\Animation;
use ModelApi\EventBundle\Entity\Atelier;
use ModelApi\EventBundle\Entity\Jeux;
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Entity\Parc;
use ModelApi\EventBundle\Entity\Season;
use ModelApi\EventBundle\Entity\Eventdate;

// use \ReflectionClass;

class EventEventdatesType extends AbstractType {

	const ENTITY_CLASSNAME = Eventdate::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}