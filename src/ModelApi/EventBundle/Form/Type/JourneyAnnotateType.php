<?php
namespace ModelApi\EventBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// EventBundle
use ModelApi\EventBundle\Entity\Journey;

// use \ReflectionClass;

class JourneyAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Journey::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}