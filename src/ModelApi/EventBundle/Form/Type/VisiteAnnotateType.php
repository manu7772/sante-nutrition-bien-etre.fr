<?php
namespace ModelApi\EventBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// EventBundle
use ModelApi\EventBundle\Entity\Visite;

// use \ReflectionClass;

class VisiteAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Visite::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}