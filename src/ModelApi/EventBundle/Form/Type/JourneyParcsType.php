<?php
namespace ModelApi\EventBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
// use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType as OriginalEntityType;
use ModelApi\BaseBundle\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as OriginalChoiceType;
use ModelApi\BaseBundle\Form\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// EventBundle
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Entity\Parc;

// use \ReflectionClass;

class JourneyParcsType extends AbstractType {

	const ENTITY_CLASSNAME = Journey::class;

	protected $container;
	protected $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		// $entity = $builder->getData();
		$builder->add("changeparc", OriginalEntityType::class, [
			'label' => 'Journée Parc',
			'expanded' => false,
			'multiple' => false,
			'required' => true,
			'class' => Parc::class,
		]);
		$builder->add('submit', SubmitType::class, array(
			'label' => 'Enregistrer',
			'attr' => array('class' => "btn btn-md btn-info btn-block")
			)
		);
	}


}