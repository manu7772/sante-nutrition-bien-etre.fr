<?php
namespace ModelApi\EventBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// EventBundle
use ModelApi\EventBundle\Entity\Jeux;

// use \ReflectionClass;

class JeuxAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Jeux::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}