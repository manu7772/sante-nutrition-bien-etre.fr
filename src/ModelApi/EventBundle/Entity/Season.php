<?php
namespace ModelApi\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Service\serviceSeason;
use ModelApi\EventBundle\Form\Type\JourneyAnnotateType;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

use \Exception;
use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ModelApi\EventBundle\Repository\SeasonRepository")
 * @ORM\EntityListeners({"ModelApi\EventBundle\Listener\SeasonListener"})
 * @ORM\Table(
 *      name="season",
 *		options={"comment":"Season"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_season",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_ADMIN", update="ROLE_ADMIN", delete="ROLE_ADMIN")
 * @UniqueEntity(fields={"year"}, message="Cette saison existe déjà !")
 */
class Season implements CrudInterface {

	const DEFAULT_ICON = 'fa-tree';
	const ENTITY_SERVICE = serviceSeason::class;
	const SHORTCUT_CONTROLS = true;

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1,
	 * 		options={"required"=true, "by_reference"=false, "label"="Nom de saison"},
	 * 		contexts={"create_season"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1,
	 * 		options={"required"=true, "by_reference"=false, "label"="Nom de saison"},
	 * 		contexts={"update_season"}
	 * )
	 * @CRUDS\Show(role=true, order=1)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(name="year", type="string", length=4, nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update=false,
	 * 		order=2,
	 * 		options={"required"=true, "by_reference"=false, "label"="Année de saison"},
	 * 		contexts={"create_season"},
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update=false,
	 * 		order=2,
	 * 		options={"required"=true, "by_reference"=false, "label"="Année de saison"},
	 * 		contexts={"update_season"},
	 * )
	 * @CRUDS\Show(role=true, order=2)
	 */
	protected $year;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\EventBundle\Entity\Journey", mappedBy="seasons", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 * @ORM\OrderBy({"start" = "ASC"})
	 * @CRUDS\Create(
	 * 		type="CollectionType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=100,
	 * 		options={
	 * 			"required"=true,
	 * 			"by_reference"=false,
	 * 			"allow_add"=true,
	 * 			"allow_delete"=true,
	 * 			"label"="Journées",
	 * 			"entry_type"=JourneyAnnotateType::class,
	 * 			"entry_options"={"label": true, "cruds_action"="create", "cruds_context"={"create_season"}},
	 * 			"attr"={"data-collection-type"=true, "data-season"=true},
	 * 		},
	 * 		contexts={"create_season"},
	 * )
	 * @CRUDS\Update(
	 * 		type="CollectionType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=100,
	 * 		options={
	 * 			"required"=true,
	 * 			"by_reference"=false,
	 * 			"allow_add"=true,
	 * 			"allow_delete"=true,
	 * 			"label"="Journées",
	 * 			"entry_type"=JourneyAnnotateType::class,
	 * 			"entry_options"={"label": true, "cruds_action"="update", "cruds_context"={"update_season"}},
	 * 			"attr"={"data-collection-type"=true, "data-season"=true},
	 * 		},
	 * 		contexts={"update_season"},
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $journeys;

	/**
	 * Begin date / Official start date
	 * @var DateTime
	 * @ORM\Column(name="start", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="DateType",
	 * 		show="ROLE_ADMIN",
	 * 		update=false,
	 * 		order=50,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Début de saison"},
	 * 		contexts={"create_season"},
	 * )
	 * @CRUDS\Update(
	 * 		type="DateType",
	 * 		show="ROLE_ADMIN",
	 * 		update=false,
	 * 		order=50,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Début de saison"},
	 * 		contexts={"update_season"},
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=50)
	 */
	protected $start;

	/**
	 * End date / Official end date
	 * @var DateTime
	 * @ORM\Column(name="end", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="DateType",
	 * 		show="ROLE_ADMIN",
	 * 		update=false,
	 * 		order=51,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Fin de saison"},
	 * 		contexts={"create_season"},
	 * )
	 * @CRUDS\Update(
	 * 		type="DateType",
	 * 		show="ROLE_ADMIN",
	 * 		update=false,
	 * 		order=51,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Fin de saison"},
	 * 		contexts={"update_season"},
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=51)
	 */
	protected $end;

	/**
	 * Real Start date / Start date of very first event of season
	 * @var DateTime
	 * @ORM\Column(name="realstart", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=52)
	 */
	protected $realstart;

	/**
	 * Real End date / End date of very last event of season
	 * @var DateTime
	 * @ORM\Column(name="realend", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=53)
	 */
	protected $realend;

	/**
	 * @CRUDS\Create(
	 * 		type="ColorpickerType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=5,
	 * 		options={"required"=true, "by_reference"=false, "label"="Couleur de saison"},
	 * 		contexts={"create_season"},
	 * )
	 * @CRUDS\Update(
	 * 		type="ColorpickerType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=5,
	 * 		options={"required"=true, "by_reference"=false, "label"="Couleur de saison"},
	 * 		contexts={"update_season"},
	 * )
	 * @CRUDS\Show(role=true, order=5)
	 */
	protected $color;


	/*************************************************************************************************/
	/*** FORM FOR NEW SEASON
	/*************************************************************************************************/

	protected $ns_dates;





	// protected $duration;

	public function __construct(DateTime $start = null, DateTime $end = null) {
		$this->id = null;
		$this->name = null;
		$this->year = null;
		$this->journeys = new ArrayCollection();
		if($start instanceOf DateTime) {
			$this->setYear($start->format('Y'));
			$this->setStart($start);
		} else {
			$start = new DateTime();
			$this->setYear($start->format('Y'));
			$this->setStart(new DateTime($this->getYear().'-01-01T00:00:00.000'));
		}
		if($end instanceOf DateTime) {
			$this->setYear($end->format('Y'));
			$this->setEnd($end);
		} else {
			$end = new DateTime();
			$this->setYear($end->format('Y'));
			$this->setEnd(new DateTime($this->getYear().'-12-31T23:59:59.999'));
		}
		$this->realstart = $this->setRealstart(clone $this->start);
		$this->realend = $this->setRealend(clone $this->end);
		// $this->duration = null;
		return $this;
	}

	/**
	 * Get as string
	 * @return string 
	 */
	public function __toString() {
		return (string)$this->id;
	}

	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}



	/*************************************************************************************************/
	/*** CHECKS
	/*************************************************************************************************/

	/**
	 * Check all dates
	 * @return Season
	 */
	public function checkAllDates() {
		$this->checkStartEnd(true);
		$this->checkRealStartEnd();
		if(!serviceSeason::isYearValid($this->getYear())) throw new Exception("Error ".__METHOD__."(): year is not valid! Got ".json_encode($this->getYear())."!", 1);
		return $this;
	}

	/**
	 * Check start and end dates
	 * @param $complete = false
	 * @return Season
	 */
	public function checkStartEnd($complete = false) {
		if($complete) {
			if(empty($this->getStart())) throw new Exception("Error ".__METHOD__."(): start date is empty!", 1);
			if(empty($this->getEnd())) throw new Exception("Error ".__METHOD__."(): end date is empty!", 1);
			$year = (integer)$this->getYear();
			if($this->getStart()->format('Y') !== (string)$year) throw new Exception("Error ".__METHOD__."(): start date must be the same year than Year data!", 1);
			if($this->getEnd()->format('Y') !== (string)$year && $this->getEnd()->format('Y') !== (string)($year + 1)) throw new Exception("Error ".__METHOD__."(): end date must be the same or +1 year than Year data!", 1);
		}
		if($this->getEnd() instanceOf DateTime && $this->getStart() instanceOf DateTime) {
			if($this->getStart() > $this->getEnd()) throw new Exception("Error ".__METHOD__."(): start (".$this->getStart()->format('d/m/Y').") date can not be after end (".$this->getEnd()->format('d/m/Y').") date!", 1);
			if($this->getEnd() < $this->getStart()) throw new Exception("Error ".__METHOD__."(): end (".$this->getEnd()->format('d/m/Y').") date can not be before start (".$this->getStart()->format('d/m/Y').") date!", 1);
		}
		return $this;
	}

	/**
	 * Check real start and end dates
	 * @return Season
	 */
	public function checkRealStartEnd() {
		$journeys = $this->getJourneys()->filter(function($journey) {
			return $journey->isActive();
		});
		$this->realstart = null;
		$this->realend = null;
		foreach ($journeys as $journey) {
			if(empty($this->realstart) || $this->realstart > $journey->getStart()) $this->setRealstart(clone $journey->getStart());
			if(empty($this->realend) || $this->realend < $journey->getEnd()) $this->setRealend(clone $journey->getEnd());
		}
		if(empty($this->realstart)) $this->setRealstart(clone $this->getStart());
		if(empty($this->realend)) $this->setRealend(clone $this->getEnd());
		return $this;
	}




	/*************************************************************************************************/
	/*** NAME & YEAR
	/*************************************************************************************************/

	public function setName($name) {
		$old_name = $this->name;
		$name = serviceTools::getTextOrNullStripped($name);
		$name = serviceTools::removeSeparator($name);
		if($old_name !== $name) {
			$this->name = $name;
		}
		return $this;
	}

	public function getName() {
		return $this->name;
	}

	protected function setYear($year) {
		// Never change year if entity persisted
		if(!empty($this->year) && !empty($this->getId())) return $this;
		$year = (string)$year;
		if(!serviceSeason::isYearValid($year)) throw new Exception("Error ".__METHOD__."(): year is not valid! Got ".json_encode($year)."!", 1);
		$this->year = $year;
		if(empty($this->name)) $this->setName($this->year);
		return $this;
	}

	public function getYear() {
		return $this->year;
	}


	/*************************************************************************************************/
	/*** START & END
	/*************************************************************************************************/

	/**
	 * Set start
	 * @param DateTime $start
	 * @return Season
	 */
	public function setStart(DateTime $start) {
		// if($this->getEnd() instanceOf DateTime && $start > $this->getEnd()) throw new Exception("Error ".__METHOD__."(): start date can not be after end date!", 1);
		$this->start = $start;
		$this->setYear($this->start->format('Y'));
		$this->checkStartEnd(false);
		return $this;
	}

	/**
	 * Get start
	 * @return DateTime
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * Set end
	 * @param DateTime $end
	 * @return Season
	 */
	public function setEnd(DateTime $end) {
		// if($this->getStart() instanceOf DateTime && $end < $this->getStart()) throw new Exception("Error ".__METHOD__."(): end date can not be before start date!", 1);
		$this->end = $end;
		$this->checkStartEnd(false);
		return $this;
	}

	/**
	 * Get end
	 * @return DateTime
	 */
	public function getEnd() {
		return $this->end;
	}

	/**
	 * Set realstart
	 * @param DateTime $realstart
	 * @return Season
	 */
	public function setRealstart(DateTime $realstart) {
		// if($this->getRealend() instanceOf DateTime && $realstart > $this->getRealend()) throw new Exception("Error ".__METHOD__."(): real start date can not be after real end date!", 1);
		$this->realstart = $realstart;
		return $this;
	}

	/**
	 * Get realstart
	 * @return DateTime
	 */
	public function getRealstart() {
		return $this->realstart;
	}

	/**
	 * Set realend
	 * @param DateTime $realend
	 * @return Season
	 */
	public function setRealend(DateTime $realend) {
		// if($this->getStart() instanceOf DateTime && $realend < $this->getStart()) throw new Exception("Error ".__METHOD__."(): real end date can not be before real start date!", 1);
		$this->realend = $realend;
		return $this;
	}

	/**
	 * Get realend
	 * @return DateTime
	 */
	public function getRealend() {
		return $this->realend;
	}



	/*************************************************************************************************/
	/*** JOURNEYS
	/*************************************************************************************************/

	public function isJourneyValidForSeason(Journey $journey) {
		return $journey->getEnd() > $this->getStart() && $journey->getStart() < $this->getEnd();
	}

	public function getJourneys() {
		return $this->journeys;
	}

	public function addJourney(Journey $journey, $inverse = true) {
		if(!$this->isJourneyValidForSeason($journey)) {
			return $this->removeJourney($journey);
		}
		if(!$this->journeys->contains($journey)) $this->journeys->add($journey);
		if($inverse) $journey->addSeason($this, false);
		return $this;
	}

	public function removeJourney(Journey $journey, $inverse = true) {
		if($inverse) $journey->removeSeason($this, false);
		$this->journeys->removeElement($journey);
		return $this;
	}

	public function checkJourneys() {
		foreach ($this->journeys as $journey) {
			if(!$this->isJourneyValidForSeason($journey)) return false;
		}
		return true;
	}





}




