<?php
namespace ModelApi\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// EventBundleBaseBundle
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceTools;
// EventBundle
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Service\serviceEvent;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// MarketBundle
use ModelApi\MarketBundle\Entity\Baseprod;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Thumbnail;
use ModelApi\BinaryBundle\Entity\Photo;

use \Exception;
use \DateTime;

/**
 * Event
 *
 * @ORM\Entity(repositoryClass="ModelApi\EventBundle\Repository\EventRepository")
 * @ORM\EntityListeners({"ModelApi\EventBundle\Listener\EventListener"})
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * @ORM\Table(
 *      name="`event`",
 *		options={"comment":"Events"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_event",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @Annot\HasTranslatable()
 */
abstract class Event extends Item {

	const DEFAULT_ICON = 'fa-calendar-o';
	const DEFAULT_OWNER_DIRECTORY = '@@@default@@@';
	const ENTITY_SERVICE = serviceEvent::class;

	// use \ModelApi\BaseBundle\Traits\BaseItem;

	/**
	 * @CRUDS\Show(role=true, order=1, type="graphic_md")
	 */
	protected $graphic;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1,
	 * 		options={"required"=true},
	 * 		contexts={"expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1,
	 * 		options={"required"=true},
	 * 		contexts={"expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(name="title", type="text", nullable=false, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=3,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=3,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=3)
	 */
	protected $title;

	/**
	 * @var string
	 * @ORM\Column(name="subtitle", type="text", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=5,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=5,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=5)
	 */
	protected $subtitle;

	/**
	 * @var string
	 * @ORM\Column(name="menutitle", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=6,
	 * 		options={"required"=true, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=6,
	 * 		options={"required"=true, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=6)
	 */
	protected $menutitle;

	/**
	 * @var string
	 * @ORM\Column(name="description", type="text", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		type="SummernoteType",
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=8,
	 * 		options={"required"=false, "description"="<strong>Description</strong> : uniquement pour l'administration. Non visible sur le site public."},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		type="SummernoteType",
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=8,
	 * 		options={"required"=false, "description"="<strong>Description</strong> : uniquement pour l'administration. Non visible sur le site public."},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=8)
	 */
	protected $description;

	/**
	 * @var string
	 * @ORM\Column(name="accroche", type="text", nullable=true, unique=false)
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=7,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=7,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=7)
	 */
	protected $accroche;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true, unique=false)
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(
	 * 		type="SummernoteType",
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=10,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		type="SummernoteType",
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=10,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=10)
	 */
	protected $alertmsg;

	/**
	 * @var boolean
	 * Mettre en avant cet Event (Gras, etc.)
	 * @ORM\Column(type="boolean", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false, "description"="Cet évènement sera mis en avant sur le site chaque fois que cela se présente possible (mis en gras, surligné, etc.)"},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false, "description"="Cet évènement sera mis en avant sur le site chaque fois que cela se présente possible (mis en gras, surligné, etc.)"},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $prime;

	/**
	 * @var integer
	 * Réservations
	 * @ORM\Column(type="integer", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $reservations;

	/**
	 * @var integer
	 * Réservations max
	 * @ORM\Column(type="integer", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $maxreservations;

	/**
	 * @var integer
	 * Réservations min
	 * @ORM\Column(type="integer", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $minreservations;

	/**
	 * @var integer
	 * @ORM\OneToMany(targetEntity="ModelApi\EventBundle\Entity\Eventdate", mappedBy="event", orphanRemoval=true, cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 * @ORM\OrderBy({"start" = "ASC"})
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $eventdates;
	/**
	 * @CRUDS\Show(role=true, order=100, type="integer")
	 */
	protected $nbeventdates;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true, unique=false)
	 */
	protected $deprecated;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=100)
	 */
	protected $visibleagenda;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\MarketBundle\Entity\Baseprod", mappedBy="events")
	 * @ORM\OrderBy({"sortprod"="ASC"})
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $products;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\MarketBundle\Entity\Baseprod", mappedBy="addedAutoEvents")
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $addedAutoProducts;

	/**
	 * @var string
	 * @ORM\Column(name="type", type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $type;
	protected $longtype;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Thumbnail", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=109,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsThumbnail"},
	 * 		contexts={"medium", "expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=109,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsThumbnail"},
	 * 		contexts={"medium", "expert"}
	 * 	)
	 * @CRUDS\Show(role=true, order=109)
	 */
	protected $thumbnail;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $photo;


	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=109,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsEntete"},
	 * 		contexts={"medium", "expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=109,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsEntete"},
	 * 		contexts={"medium", "expert"}
	 * 	)
	 * @CRUDS\Show(role=true, order=109)
	 */
	protected $entete;

	/**
	 * Begin date
	 * @var DateTime
	 * @ORM\Column(name="start", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="TimeType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=50,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Début journée"},
	 * )
	 * @CRUDS\Update(
	 * 		type="TimeType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=50,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Début journée"},
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=50)
	 */
	protected $start;

	/**
	 * End date
	 * @var DateTime
	 * @ORM\Column(name="end", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="TimeType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=51,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Fin journée"},
	 * )
	 * @CRUDS\Update(
	 * 		type="TimeType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=51,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Fin journée"},
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=51)
	 */
	protected $end;

	/**
	 * Most current begin date
	 * @var Integer
	 * @ORM\Column(name="moststart", type="integer", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $moststart;

	/**
	 * Most current end date
	 * @var Integer
	 * @ORM\Column(name="mostend", type="integer", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $mostend;

	/**
	 * Min during in minutes
	 * @var Integer
	 * @ORM\Column(name="timemin", type="integer", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $timemin;

	/**
	 * Max during in minutes
	 * @var Integer
	 * @ORM\Column(name="timemax", type="integer", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $timemax;

	/**
	 * Is visible even if inactive
	 * @var boolean
	 * @ORM\Column(name="visible", type="boolean", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=100)
	 */
	protected $visibleEvenIfInactive;


	public function __construct() {
		parent::__construct();
		$this->accroche = null;
		$this->alertmsg = null;
		$this->prime = false;
		$this->reservations = 0;
		$this->maxreservations = 0;
		$this->minreservations = 0;
		$this->entete = null;
		$this->moststart = null;
		$this->mostend = null;
		$this->timemin = 30;
		$this->timemax = 30;
		// $this->duration = 0;
		$this->eventdates = new ArrayCollection();
		$this->deprecated = false;
		$this->visibleagenda = true;
		$this->products = new ArrayCollection();
		$this->addedAutoProducts = new ArrayCollection();
		$this->type = $this->getShortname(true);
		$this->thumbnail = null;
		$this->photo = null;
		$this->start = new DateTime();
		$this->start->setTime(10,00,00);
		$this->end = new DateTime();
		$this->end->setTime(17,00,00);
		return $this;
	}

	public function getGraphic() {
		// if($this->avatar !== null) return $this->avatar;
		if($this->thumbnail !== null) return $this->thumbnail;
		return null;
	}

	/**
	 * Get title
	 * @param string $title = null
	 * @return self
	 */
	public function setTitle($title = null) {
		$this->title = $title;
		return $this;
	}


	/**
	 * Get menutitle
	 * @param string $menutitle = null
	 * @return self
	 */
	public function setMenutitle($menutitle = null) {
		$this->menutitle = $menutitle;
		return $this;
	}


	/**
	 * Set description
	 * @param string $description = null
	 * @return self
	 */
	public function setDescription($description = null) {
		$this->description = $description;
		return $this;
	}


	/**
	 * Get accroche
	 * @return string
	 */
	public function getAccroche() {
		return $this->accroche;
	}

	/**
	 * Set accroche
	 * @param string $accroche = null
	 * @return Event
	 */
	public function setAccroche($accroche = null) {
		$this->accroche = $accroche;
		return $this;
	}

	/**
	 * Get alertmsg
	 * @return string
	 */
	public function getAlertmsg() {
		return $this->alertmsg;
	}

	/**
	 * Set alertmsg
	 * @param string $alertmsg = null
	 * @return Event
	 */
	public function setAlertmsg($alertmsg = null) {
		$this->alertmsg = $alertmsg;
		return $this;
	}

	/**
	 * Get prime
	 * @return boolean
	 */
	public function getPrime() {
		return $this->prime;
	}

	/**
	 * Set prime
	 * @param boolean $prime = true
	 * @return Event
	 */
	public function setPrime($prime = true) {
		$this->prime = $prime;
		return $this;
	}

	/**
	 * Get reservations
	 * @return integer
	 */
	public function getReservations() {
		return $this->reservations;
	}

	/**
	 * Set reservations
	 * @param integer $reservations = true
	 * @return Event
	 */
	public function setReservations($reservations) {
		$this->reservations = $reservations;
		return $this;
	}

	/**
	 * Get maxreservations
	 * @return integer
	 */
	public function getMaxreservations() {
		return $this->maxreservations;
	}

	/**
	 * Set maxreservations
	 * @param integer $maxreservations = true
	 * @return Event
	 */
	public function setMaxreservations($maxreservations) {
		$this->maxreservations = $maxreservations;
		return $this;
	}

	/**
	 * Get minreservations
	 * @return integer
	 */
	public function getMinreservations() {
		return $this->minreservations;
	}

	/**
	 * Set minreservations
	 * @param integer $minreservations = true
	 * @return Event
	 */
	public function setMinreservations($minreservations) {
		$this->minreservations = $minreservations;
		return $this;
	}

	/**
	 * Verify minreservations not > maxreservations
	 * @return Event
	 */
	public function verifMinreservations() {
		if($this->getMinreservations() > $this->getMaxreservations()) $this->setMinreservations($this->getMaxreservations());
		$this->checkMostStartEnds();
		$this->computeTimes();
		$this->isDeprecated();
		return $this;
	}

	/*************************************************************************************************/
	/*** START & END
	/*************************************************************************************************/

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function checkAllDates() {
		if(empty($this->getStart())) $this->setStart($this->getMoststart());
		$this->checkStartEnd(true);
		$this->verifMinreservations();
		return $this;
	}

	/**
	 * Check start and end dates
	 * @param $complete = false
	 * @return Season
	 */
	public function checkStartEnd($complete = false) {
		if($complete) {
			if(empty($this->getStart())) throw new Exception("Error ".__METHOD__."(): start date is empty!", 1);
			if(empty($this->getEnd())) throw new Exception("Error ".__METHOD__."(): end date is empty!", 1);
		}
		if($this->getEnd() instanceOf DateTime && $this->getStart() instanceOf DateTime) {
			if(intval($this->getStart()->format('Hi')) > intval($this->getEnd()->format('Hi'))) throw new Exception("Error ".__METHOD__."(): start (".$this->getStart()->format('H:i').") time can not be after end (".$this->getEnd()->format('H:i').") time!", 1);
			// if(intval($this->getEnd()->format('Hi')) < intval($this->getStart()->format('Hi'))) throw new Exception("Error ".__METHOD__."(): end (".$this->getEnd()->format('H:i').") time can not be before start (".$this->getStart()->format('H:i').") time!", 1);
		}
		return $this;
	}

	/**
	 * Set start
	 * @param DateTime $start
	 * @return Season
	 */
	public function setStart(DateTime $start) {
		$test = intval($this->getStart()->format('Hi')) > 0 && intval($this->getEnd()->format('Hi')) > 0;
		$this->start = $start;
		if($test) $this->checkStartEnd(false);
		return $this;
	}

	/**
	 * Get start
	 * @return DateTime
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * Set end
	 * @param DateTime $end
	 * @return Season
	 */
	public function setEnd(DateTime $end) {
		$test = intval($this->getStart()->format('Hi')) > 0 && intval($this->getEnd()->format('Hi')) > 0;
		$this->end = $end;
		if($test) $this->checkStartEnd(false);
		return $this;
	}

	/**
	 * Get end
	 * @return DateTime
	 */
	public function getEnd() {
		return $this->end;
	}

	public function checkMostStartEnds($date = 'NOW') {
		if(is_string($date) && strtoupper($date) === 'NOW') $date = serviceContext::DEFAULT_DATE;
		$starts = [];
		$ends = [];
		foreach ($this->getEventdates($date) as $eventdate) {
			// $start = $eventdate->getStart()->format(serviceContext::CONTEXT_FORMAT_DATE);
			$start = $eventdate->getStart()->format('H:i:s');
			$starts[$start] ??= 0;
			$starts[$start]++;
			// $end = $eventdate->getEnd()->format(serviceContext::CONTEXT_FORMAT_DATE);
			$end = $eventdate->getEnd()->format('H:i:s');
			$ends[$end] ??= 0;
			$ends[$end]++;
		}
		$max_start = count($starts) ? array_keys($starts, max($starts)) : [];
		$this->moststart = null;
		if(is_string(reset($max_start))) {
			$time = explode(':', reset($max_start));
			$this->moststart = 3600 * $time[0] + 60 * $time[1] + $time[2];
		}
		$max_end = count($ends) ? array_keys($ends, max($ends)) : [];
		$this->mostend = null;
		if(is_string(reset($max_end))) {
			$time = explode(':', reset($max_end));
			$this->mostend = 3600 * $time[0] + 60 * $time[1] + $time[2];
		}
		return $this;
	}

	public function getMoststart($format = null, $date = null) {
		if(is_string($date) || $date instanceOf DateTime) $this->checkMostStartEnds($date);
		$moststart = $this->moststart;
		date_default_timezone_set('UTC');
		if(is_string($format)) {
			if('auto' === strtolower($format)) $format = 'H:i:s';
			$moststart = date($format, $moststart);
		}
		return $moststart;
	}

	public function getMostend($format = null, $date = null) {
		if(is_string($date) || $date instanceOf DateTime) $this->checkMostStartEnds($date);
		$mostend = $this->mostend;
		date_default_timezone_set('UTC');
		if(is_string($format)) {
			if('auto' === strtolower($format)) $format = 'H:i:s';
			$mostend = date($format, $mostend);
		}
		return $mostend;
	}


	public function setTimemin($timemin) {
		$this->timemin = $timemin;
		return $this;
	}

	public function getTimemin() {
		return $this->timemin;
	}

	public function setTimemax($timemax) {
		$this->timemax = $timemax;
		return $this;
	}

	public function getTimemax() {
		return $this->timemax;
	}

	public function computeTimes() {
		if($this->timemax < $this->timemin) $this->timemax = $this->timemin;
	}



	public function isDeprecated($contextdate = null) {
		$this->deprecated = $this->getEventdates($contextdate)->isEmpty();
		return $this->deprecated;
	}

	public function getDeprecated() {
		return $this->isDeprecated();
	}

	public function isVisibleagenda() {
		return $this->visibleagenda && $this->isVisible();
	}

	public function getVisibleagenda() {
		return $this->visibleagenda;
	}

	public function setVisibleagenda($visibleagenda) {
		$this->visibleagenda = $visibleagenda;
		return $this;
	}

	/**
	 * Add Eventdate
	 * Fix start and end with mosts if not defined
	 * @param Eventdate $eventdate
	 * @param boolean $inverse = true
	 * @return Event
	 */
	public function addEventdate(Eventdate $eventdate, $inverse = true) {
		serviceTools::pickTime($eventdate->getStart(), $this->getStart());
		serviceTools::pickTime($eventdate->getEnd(), $this->getEnd());
		if(!$this->eventdates->contains($eventdate)) $this->eventdates->add($eventdate);
		if($inverse) $eventdate->setEvent($this, false);
		return $this;
	}

	/**
	 * Get Eventdates
	 * @param mixed $contextdate = null
	 * @param $onlyActive = true
	 * @return ArrayCollection <Eventdate>
	 */
	public function getEventdates($contextdate = null, $onlyActive = true) {
		if(null === $contextdate) return $this->eventdates;
		if(is_bool($contextdate) && true === $contextdate) {
			$contextdate = $this->getContextdate();
		} else if(is_string($contextdate)) {
			$contextdate = new DateTime($contextdate);
		} else {
			$contextdate = $this->getContextdate($contextdate);
		}
		return $this->eventdates->filter(function($eventdate) use ($contextdate, $onlyActive) {
			return !$eventdate->isDeprecated($contextdate) && ($eventdate->isActive() || $onlyActive);
		});
	}
	public function getNbeventdates($contextdate = null) {
		return $this->getEventdates($contextdate)->count();
	}

	/**
	 * Remove Eventdate
	 * @param Eventdate $eventdate
	 * @param boolean $inverse = true
	 * @return ???
	 */
	public function removeEventdate(Eventdate $eventdate, $inverse = true) {
		if($inverse) $eventdate->setEvent(null, false);
		return $this->eventdates->removeElement($eventdate);
	}

	/**
	 * Set Eventdates
	 * @param ArrayCollection $eventdates
	 * @return Event
	 */
	public function setEventdates(ArrayCollection $eventdates) {
		foreach ($this->eventdates as $eventdate) {
			if(!$eventdates->contains($eventdate)) $this->removeEventdate($eventdate);
		}
		foreach ($eventdates as $eventdate) {
			$this->addEventdate($eventdate);
		}
		return $this;
	}

	public function getNextEventdate($contextdate = null) {
		// if(is_string($contextdate)) $contextdate = new DateTime($contextdate);
		$contextdate = $this->getContextdate($contextdate);
		$eventdates = $this->getEventdates($contextdate);
		return $eventdates->isEmpty() ? null : $eventdates->first();
	}

	public function getNextYear($contextdate = null) {
		$contextdate = $this->getNextEventdate($contextdate);
		return $contextdate instanceOf Eventdate ? $contextdate->getStart()->format('Y') : null;
	}

	public function getNextMonth($contextdate = null) {
		$contextdate = $this->getNextEventdate($contextdate);
		return $contextdate instanceOf Eventdate ? $contextdate->getStart()->format('m') : null;
	}

	public function isObsolete($contextdate = null) {
		return null === $this->getNextEventdate($contextdate);
	}

	public function getAllDates($contextdate = null, $asDaymark = false) {
		$contextdate = $this->getContextdate($contextdate);
		$dates = [];
		foreach ($this->getEventdates($contextdate) as $eventdate) {
			foreach ($eventdate->getAllDates(false) as $daymark => $date) {
				$dates[$daymark] ??= [];
				$dates[$daymark][] = $date;
			}
		}
		return $asDaymark ? array_keys($dates) : $dates;
	}

	public function getDaymarks($contextdate = null) {
		return $this->getAllDates($contextdate, true);
	}

	public function hasDaymark($daymark, $onlybyStart = false) {
		foreach ($this->getEventdates() as $eventdate) {
			if($eventdate->hasDaymark($daymark, $onlybyStart)) return true;
		}
		return false;
	}

	public function getAndRemoveEventdateByDaymark($daymark, $onlyFirstAsStart = true) {
		// Find by daymark
		$eventdates = $this->eventdates->filter(function($eventdate) use ($daymark, $onlyFirstAsStart) {
			return $eventdate->hasDaymark($daymark, $onlyFirstAsStart);
		});
		if($onlyFirstAsStart && $eventdates->count() > 1) throw new Exception("Error ".__METHOD__."(): more than one eventdate found. Please check your code!", 1);
		// Remove
		foreach ($eventdates as $eventdate) {
			$this->removeEventdate($eventdate);
		}
		return $eventdates;
	}



	/**
	 * Get AddedAuto Products
	 * @return ArrayCollection <Baseprod>
	 */
	public function getAddedAutoProducts() {
		return $this->addedAutoProducts;
	}

	/**
	 * Add Product
	 * @param Baseprod $product
	 * @return Event
	 */
	public function addAddedAutoProduct(Baseprod $product, $inverse = true) {
		if($inverse) $product->addAddedAutoEvent($this, false);
		if(!$this->addedAutoProducts->contains($product)) $this->addedAutoProducts->add($product);
		return $this;
	}

	/**
	 * Remove Baseprod
	 * @param Baseprod $product
	 * @return ???
	 */
	public function removeAddedAutoProduct(Baseprod $product, $inverse = true) {
		if($inverse) $product->removeAddedAutoEvent($this, false);
		return $this->addedAutoProducts->removeElement($product);
	}

	/**
	 * Set Products
	 * @param ArrayCollection $products
	 * @return Event
	 */
	public function setAddedAutoProducts(ArrayCollection $products) {
		foreach ($this->addedAutoProducts as $product) {
			if(!$products->contains($product)) $this->removeAddedAutoProduct($product);
		}
		foreach ($products as $product) {
			$this->addAddedAutoProduct($product);
		}
		return $this;
	}





	/**
	 * Get Products + AddedAuto Products
	 * @return ArrayCollection <Baseprod>
	 */
	public function getAllProducts() {
		$products = clone $this->products;
		foreach ($this->getAddedAutoProducts() as $addedAutoProduct) {
			if(!$products->contains($addedAutoProduct)) $products->add($addedAutoProduct);
		}
		return $products;
	}





	/**
	 * Get Products
	 * @return ArrayCollection <Baseprod>
	 */
	public function getProducts() {
		return $this->products;
	}

	/**
	 * Add Baseprod
	 * @param Baseprod $product
	 * @return Event
	 */
	public function addProduct(Baseprod $product, $inverse = true) {
		if($inverse) $product->addEvent($this, false);
		if(!$this->products->contains($product)) $this->products->add($product);
		return $this;
	}

	/**
	 * Remove Baseprod
	 * @param Baseprod $product
	 * @return ???
	 */
	public function removeProduct(Baseprod $product, $inverse = true) {
		if($inverse) $product->removeEvent($this, false);
		return $this->products->removeElement($product);
	}

	/**
	 * Set Products
	 * @param ArrayCollection $products
	 * @return Event
	 */
	public function setProducts(ArrayCollection $products) {
		foreach ($this->products as $product) {
			if(!$products->contains($product)) $this->removeProduct($product);
		}
		foreach ($products as $product) {
			$this->addProduct($product);
		}
		return $this;
	}








	/**
	 * Get type
	 * @return string 
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Set type
	 * @param string $type
	 * @return Event
	 */
	public function setType($type) {
		$this->type = $type;
		return $this;
	}

	/**
	 * Get longtype
	 * @return string 
	 */
	public function getLongtype() {
		$type = $this->getShortname();
		$type .= is_string($this->getType()) ? '_'.$this->getType() : '';
		return $type;
	}

	/**
	 * Get last access for one day if $start or $end
	 * @param DateTime $date
	 * @return DateTime | null
	 */
	public function getLastaccess(DateTime $date) {
		$lastaccess = null;
		foreach ($this->getEventdates() as $eventdate) {
			$edla = $eventdate->getLastaccess();
			if($date->format('Y-m-d') === $edla->format('Y-m-d') && ($lastaccess === null || $lastaccess < $edla)) $lastaccess = $edla;
		}
		return $lastaccess;
	}


	/*************************************************************************************/
	/*** THUMBNAIL
	/*************************************************************************************/

	/**
	 * Set URL as thumbnail
	 * @param string $url
	 * @return Language
	 */
	public function setUrlAsThumbnail($url) {
		if(!$this->hasThumbnail()) { $this->setThumbnail(new Thumbnail()); }
		$this->getThumbnail()->setUploadFile($url);
	}

	/**
	 * Set resource as thumbnail
	 * @param string $resource
	 * @return ModelTier
	 */
	public function setResourceAsThumbnail(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$thumbnail = new Thumbnail();
			$thumbnail->setUploadFile($resource);
			$this->setThumbnail($thumbnail);
		}
		return $this;
	}

	public function getResourceAsThumbnail() {
		return null;
	}

	/**
	 * Set thumbnail
	 * @param Thumbnail $thumbnail = null
	 * @return Language
	 */
	public function setThumbnail(Thumbnail $thumbnail = null) {
		$this->thumbnail = $thumbnail;
		return $this;
	}

	/**
	 * Get thumbnail
	 * @return Thumbnail | null
	 */
	public function getThumbnail() {
		return $this->thumbnail;
	}

	/**
	 * Has thumbnail
	 * @return boolean
	 */
	public function hasThumbnail() {
		return $this->getThumbnail() instanceOf Thumbnail;
	}


	/*************************************************************************************/
	/*** PHOTO
	/*************************************************************************************/

	/**
	 * Set URL as photo
	 * @param string $url
	 * @return Event
	 */
	public function setUrlAsPhoto($url) {
		$photo = new Photo();
		$photo->setUploadFile($url);
		$this->setPhoto($photo);
	}

	/**
	 * Set resource as photo
	 * @param string $resource
	 * @return Event
	 */
	public function setResourceAsPhoto(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$photo = new Photo();
			$photo->setUploadFile($resource);
			$this->setPhoto($photo);
		}
		return $this;
	}

	public function getResourceAsPhoto() {
		return null;
	}

	/**
	 * Set photo
	 * @param Photo $photo = null
	 * @return Event
	 */
	public function setPhoto(Photo $photo = null) {
		$this->photo = $photo;
		return $this;
	}

	/**
	 * Get photo
	 * @return Photo | null
	 */
	public function getPhoto() {
		return $this->photo;
	}


	/*************************************************************************************/
	/*** ENTETE
	/*************************************************************************************/

	/**
	 * Set URL as entete
	 * @param string $url
	 * @return ModelTier
	 */
	public function setUrlAsEntete($url) {
		$entete = new Photo();
		$entete->setUploadFile($url);
		$this->setEntete($entete);
	}

	/**
	 * Set resource as entete
	 * @param string $resource
	 * @return ModelTier
	 */
	public function setResourceAsEntete(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$entete = new Photo();
			$entete->setUploadFile($resource);
			$this->setEntete($entete);
		}
		return $this;
	}

	public function getResourceAsEntete() {
		return null;
	}

	/**
	 * Set entete
	 * @param Entete $entete = null
	 * @return ModelTier
	 */
	public function setEntete(Photo $entete = null) {
		$this->entete = $entete;
		return $this;
	}

	/**
	 * Get entete
	 * @return Entete | null
	 */
	public function getEntete() {
		return $this->entete;
	}

	/**
	 * Has entete
	 * @return boolean
	 */
	public function hasEntete() {
		return $this->getEntete() instanceOf Photo;
	}


}