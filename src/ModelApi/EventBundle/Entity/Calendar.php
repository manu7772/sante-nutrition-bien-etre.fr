<?php
namespace ModelApi\EventBundle\Entity;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;

use \DateTime;

class Calendar {

	const GLOBAL_CALENDAR_CACHE_NAME = 'calendars';
	// const CACHED_CALENDAR_NAME = "cached_calendar";
	const CACHE_LIFETIME = 3600;

	protected $data;
	protected $compiled;

	public function __construct($data) {
		$this->data = $data;
		$this->compiled = null;
		return $this;
	}

	public function getData() {
		return $this->data;
	}

	public function getCompiled($month = null, $year = null, $recompile = false) {
		if(null === $year || !isset($this->data[$year])) {
			$years = array_keys($this->data);
			$year = reset($years);
		}
		if(isset($this->data[$year])) {
			if(null === $month) {
				$months = array_keys($this->data[$year]['dates']);
				$month = reset($months);
			}
			if(is_integer($month)) $month = $month < 10 ? '0'.$month : ''.$month;
			if(isset($this->data[$year]['dates'][$month])) {
				if(!isset($this->compiled[$year]) || !isset($this->compiled[$year][$month]) || $recompile) {
					$compiled = ['days' => []];
					$firstDate = reset($this->data[$year]['dates'][$month]['dates']);
					$start = is_string($firstDate['start']) ? new DateTime($firstDate['start']) : $firstDate['start'];
					$compiled['month'] = $start->format('m');
					/** @see https://www.w3schools.com/php/func_date_strftime.asp */
					$compiled['title'] = utf8_encode(strftime("%B %G", $start->getTimestamp()));
					// $formatter = new IntlDateFormatter('de_DE', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
					$day = serviceTools::getPreviousDayOfMonth($start);
					foreach ([1,2,3,4,5,6] as $line) {
						if(!isset($compiled['days'][$line])) $compiled['days'][$line] = [];
						foreach ([1,2,3,4,5,6,7] as $col) {
							if(!isset($compiled['days'][$line][$col])) $compiled['days'][$line][$col] = null;
							$tmp = $day->format('Y-m-d');
							$tmp = explode('-', $tmp);
							if($tmp[1] === $compiled['month']) $compiled['days'][$line][$col] = isset($this->data[$year]['dates'][$month]['dates'][$tmp[2]]) ? $this->data[$year]['dates'][$month]['dates'][$tmp[2]] : clone $day;
							// events
							if(is_array($compiled['days'][$line][$col])) {
								$compiled['days'][$line][$col]['_events'] = [];
								foreach ($compiled['days'][$line][$col]['eventdates'] as $type => $eventdates) {
									if(!in_array($type, ['Parc','Atelier'])) {
										foreach ($eventdates as $eventdate) {
											if(is_array($eventdate)) {
												// if($eventdate['Event']['active'])
												$compiled['days'][$line][$col]['_events'][$eventdate['event']['id']] = $eventdate['event'];
											} else {
												// if($eventdate->getEvent()->isActive() && $eventdate->getEvent->isVisibleagenda()) {
													$compiled['days'][$line][$col]['_events'][$eventdate->getEvent()->getId()] = $eventdate->getEvent();
												// }
											}
										}
									}
								}
							}
							// increment date
							$day->modify('+1 day');
						}
					}
					// echo('<pre>');var_dump($compiled);die('</pre>');
					$this->compiled[$year][$month] = $compiled;
					return $this->compiled[$year][$month];
				}
			}
		}
		return [];
	}


}