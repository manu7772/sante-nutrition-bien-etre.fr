<?php
namespace ModelApi\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
// use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Pdf;
// EventBundle
use ModelApi\EventBundle\Entity\Event;

use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ModelApi\EventBundle\Repository\ParcRepository")
 * @ORM\Table(
 *      name="parc",
 *		options={"comment":"Parc"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_parc",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @Annot\HasTranslatable()
 */
class Parc extends Event {

	const DEFAULT_ICON = 'fa-leaf';
	const DEFAULT_OWNER_DIRECTORY = '@@@default@@@';
	const ENTITY_SERVICE = serviceAtelier::class;

	/**
	 * @var string
	 * @ORM\Column(name="age", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $age;

	/**
	 * @var string
	 * @ORM\Column(name="during", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $during;

	/**
	 * @var string
	 * @ORM\Column(name="weather", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $weather;

	/**
	 * @CRUDS\Create(
	 * 		type="TimeType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=50,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Début journée", "description"="<strong>Début journée</strong> : heure d'ouverture du parc."},
	 * )
	 * @CRUDS\Update(
	 * 		type="TimeType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=50,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Début journée", "description"="<strong>Début journée</strong> : heure d'ouverture du parc."},
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=50)
	 */
	protected $start;

	/**
	 * @CRUDS\Create(
	 * 		type="TimeType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=51,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Fin journée", "description"="<strong>Fin journée</strong> : heure de fermeture du parc."},
	 * )
	 * @CRUDS\Update(
	 * 		type="TimeType",
	 * 		show="ROLE_ADMIN",
	 * 		update="ROLE_ADMIN",
	 * 		order=51,
	 * 		options={"required"=true, "by_reference"=false, "widget"="single_text", "label"="Fin journée", "description"="<strong>Fin journée</strong> : heure de fermeture du parc."},
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=51)
	 */
	protected $end;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $name;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $title;

	/**
	 * !!!@CRUDS\Create(show=false, update=false)
	 * !!!@CRUDS\Update(show=false, update=false)
	 */
	protected $menutitle;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $prime;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $subtitle;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $accroche;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false, "description"="<strong>Résumé de texte</strong> : version courte du texte principal."},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false, "description"="<strong>Résumé de texte</strong> : version courte du texte principal."},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $resume;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false, "description"="<strong>Long texte</strong> : texte complet."},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false, "description"="<strong>Long texte</strong> : texte complet."},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $fulltext;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $alertmsg;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $visibleEvenIfInactive;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $products;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $visibleagenda;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $entete;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $photo;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $picone;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $tags;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $pageweb;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $urls;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $groups;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $type;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $reservations;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $maxreservations;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $minreservations;



	public function __construct() {
		parent::__construct();
		$this->age = null;
		$this->during = null;
		$this->weather = null;
	}


	public function checkMostStartEnds($date = 'NOW') {
		parent::checkMostStartEnds($date);
		$during = ($this->mostend - $this->moststart) / 60; // during in minutes
		$this->setTimemin($during);
		$this->setTimemax($during);
		return $this;
	}


	public function getPdf() {
		$pdfs = $this->getOwnerDirectory('system/Pdf');
		if($pdfs instanceOf Basedirectory) {
			$pdfs = $pdfs->getChilds()->filter(function($item) { return $item instanceOf Pdf; });
			return $pdfs->isEmpty() ? null : $pdfs->first();
		}
		return null;
	}



	/**
	 * Get age
	 * @return string 
	 */
	public function getAge() {
		return $this->age;
	}

	/**
	 * Set age
	 * @param string $age
	 * @return Parc
	 */
	public function setAge($age) {
		$this->age = $age;
		return $this;
	}

	/**
	 * Get during
	 * @return string 
	 */
	public function getDuring() {
		return $this->during;
	}

	/**
	 * Set during
	 * @param string $during
	 * @return Parc
	 */
	public function setDuring($during) {
		$this->during = $during;
		return $this;
	}

	/**
	 * Get weather
	 * @return string 
	 */
	public function getWeather() {
		return $this->weather;
	}

	/**
	 * Set weather
	 * @param string $weather
	 * @return Parc
	 */
	public function setWeather($weather) {
		$this->weather = $weather;
		return $this;
	}



	// /**
	//  * Add Eventdate
	//  * Fix start and end with mosts if not defined
	//  * @param Eventdate $eventdate
	//  * @param boolean $inverse = true
	//  * @return Event
	//  */
	// public function addEventdate(Eventdate $eventdate, $inverse = true) {
	// 	$eventdate->setStart($this->getStart());
	// 	$eventdate->setEnd($this->getEnd());
	// 	parent::addEventdate($eventdate, $inverse);
	// 	return $this;
	// }



	/**
	 * Check start and end dates
	 * @param $complete = false
	 * @return Season
	 */
	public function checkStartEnd($complete = false) {
		if($complete) {
			if(empty($this->getStart())) throw new Exception("Error ".__METHOD__."(): start date is empty!", 1);
			if(empty($this->getEnd())) throw new Exception("Error ".__METHOD__."(): end date is empty!", 1);
		}
		if($this->getEnd() instanceOf DateTime && $this->getStart() instanceOf DateTime) {
			if(intval($this->getStart()->format('Hi')) > intval($this->getEnd()->format('Hi'))) throw new Exception("Error ".__METHOD__."(): in Parcs, start (".$this->getStart()->format('H:i').") time can not be after end (".$this->getEnd()->format('H:i').") time!", 1);
			// if(intval($this->getEnd()->format('Hi')) < intval($this->getStart()->format('Hi'))) throw new Exception("Error ".__METHOD__."(): end (".$this->getEnd()->format('H:i').") time can not be before start (".$this->getStart()->format('H:i').") time!", 1);
		}
		return $this;
	}

	/**
	 * Set start
	 * @param DateTime $start
	 * @return Season
	 */
	public function setStart(DateTime $start) {
		$test = intval($this->getStart()->format('Hi')) > 0 && intval($this->getEnd()->format('Hi')) > 0;
		$this->start = $this->end instanceOf DateTime ? clone $this->end : new DateTime();
		serviceTools::pickTime($this->start, $start);
		if($test) $this->checkStartEnd(false);
		return $this;
	}

	/**
	 * Set end
	 * @param DateTime $end
	 * @return Season
	 */
	public function setEnd(DateTime $end) {
		$test = intval($this->getStart()->format('Hi')) > 0 && intval($this->getEnd()->format('Hi')) > 0;
		$this->end = $this->start instanceOf DateTime ? clone $this->start : new DateTime();
		serviceTools::pickTime($this->end, $end);
		if($test) $this->checkStartEnd(false);
		return $this;
	}

}




