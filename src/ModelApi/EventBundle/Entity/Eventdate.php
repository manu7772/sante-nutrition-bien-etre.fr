<?php
namespace ModelApi\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Entity\Season;
use ModelApi\EventBundle\Service\serviceEvent;

use \DateTime;
use \Exception;

/**
 * @ORM\Entity(repositoryClass="ModelApi\EventBundle\Repository\EventdateRepository")
 * @ORM\EntityListeners({"ModelApi\EventBundle\Listener\EventdateListener"})
 * @ORM\Table(
 *      name="eventdate",
 *		options={"comment":"Eventdates"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_eventdate",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 */
class Eventdate implements CrudInterface {

	const DEFAULT_ICON = 'fa-calendar';
	// const DEFAULT_OWNER_DIRECTORY = '@@@default@@@';
	const ENTITY_SERVICE = serviceEventdate::class;
	const SHORTCUT_CONTROLS = true;

	const FORMAT_CDATE = 'd/m/Y';
	const DEFAULT_H_START = ['H' => 10, 'i' => 0, 's' => 0];
	const DEFAULT_H_END = ['H' => 17, 'i' => 0, 's' => 0];

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\BaseItem;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\EventBundle\Entity\Journey", mappedBy="eventdates")
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 */
	protected $journeys;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $name;

	/**
	 * @var string
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $title;

	/**
	 * @var string
	 * @ORM\Column(name="menutitle", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=100,
	 * 		options={"required"=true, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $menutitle;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 */
	protected $description;

	/**
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=100,
	 * )
	 */
	protected $subtitle;

	/**
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=100,
	 * )
	 */
	protected $resume;

	/**
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=100,
	 * )
	 */
	protected $fulltext;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true, unique=false)
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 */
	protected $accroche;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 */
	protected $place;

	/**
	 * Begin date
	 * @var DateTime
	 * @ORM\Column(name="start", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 * @CRUDS\Show(role=true, order=1)
	 */
	protected $start;

	/**
	 * End date
	 * @var DateTime
	 * @ORM\Column(name="end", type="datetime", nullable=true, unique=false)
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 * @CRUDS\Show(role=true, order=2)
	 */
	protected $end;

	protected $duration;

	/**
	 * @CRUDS\Create(
	 * 		type="TextType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1,
	 * 		options={"by_reference"=false, "required"=true, "label"="Date (jj/mm/aaaa)", "attr"={"class"="date-picker"}},
	 * 		contexts={"permananent","create_event"},
	 * )
	 * @CRUDS\Update(
	 * 		type="TextType",
	 * 		show="ROLE_EDITOR",
	 * 		update=false,
	 * 		order=1,
	 * 		options={"by_reference"=false, "required"=true, "label"="Date (jj/mm/aaaa)", "attr"={"class"="date-picker"}},
	 * 		contexts={"permananent","create_event","update_event"},
	 * )
	 * @CRUDS\Show(role=true, order=1, type="datetime")
	 */
	protected $cdate;

	/**
	 * @CRUDS\Create(
	 * 		type="TimeType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=2,
	 * 		options={"by_reference"=false, "required"=true, "label"="Heure de début", "widget"="single_text"},
	 * 		contexts={"permananent","create_event","update_event"}
	 * )
	 * @CRUDS\Show(role=true, order=2, type="datetime")
	 */
	protected $cstart;

	/**
	 * @CRUDS\Create(
	 * 		type="TimeType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=3,
	 * 		options={"by_reference"=false, "required"=true, "label"="Heure de fin", "widget"="single_text"},
	 * 		contexts={"permananent","create_event","update_event"}
	 * )
	 * @CRUDS\Show(role=true, order=3, type="datetime")
	 */
	protected $cend;

	/**
	 * For all the day
	 * @var Boolean
	 * @ORM\Column(name="allday", type="boolean", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $allday;

	/**
	 * @var integer
	 * @ORM\ManyToOne(targetEntity="ModelApi\EventBundle\Entity\Event", inversedBy="eventdates", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=102,
	 * 		options={"multiple"=false, "required"=true, "class"="ModelApi\EventBundle\Entity\Event", "query_builder"="auto"},
	 * 		contexts={"medium"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=102,
	 * 		options={"multiple"=false, "required"=true, "class"="ModelApi\EventBundle\Entity\Event", "query_builder"="auto"},
	 * 		contexts={"medium"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=102)
	 */
	protected $event;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=32, nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $type;

	/**
	 * @var array
	 * @ORM\ManyToOne(targetEntity="ModelApi\EventBundle\Entity\Eventdate", inversedBy="childs", cascade={"persist"})
	 */
	protected $parent;

	/**
	 * @var array
	 * @ORM\OneToMany(targetEntity="ModelApi\EventBundle\Entity\Eventdate", mappedBy="parent", orphanRemoval=true)
	 * @ORM\JoinTable(name="eventdate_childs",
	 *     joinColumns={@ORM\JoinColumn(name="childs_id", referencedColumnName="id")},
	 *     inverseJoinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")}
	 * )
	 */
	protected $childs;

	/**
	 * color
	 * @var string
	 * @ORM\Column(name="color", type="string", length=32, nullable=true, unique=false)
	 * @CRUDS\Creates({
	 * 		@CRUDS\Create(
	 * 			type="ColorpickerType",
	 * 			show="ROLE_EDITOR",
	 * 			update="ROLE_EDITOR",
	 * 			order=100,
	 * 			options={"required"=true},
	 * 		),
	 * 		@CRUDS\Create(
	 * 			type="ColorpickerType",
	 * 			show=false,
	 * 			update=false,
	 * 			options={"required"=true},
	 * 			contexts={"create_event"},
	 * 		)
	 * })
	 * @CRUDS\Updates({
	 * 		@CRUDS\Update(
	 * 			type="ColorpickerType",
	 * 			show="ROLE_EDITOR",
	 * 			update="ROLE_EDITOR",
	 * 			order=100,
	 * 			options={"required"=true},
	 * 		),
	 * 		@CRUDS\Update(
	 * 			type="ColorpickerType",
	 * 			show=false,
	 * 			update=false,
	 * 			options={"required"=true},
	 * 			contexts={"update_event"},
	 * 		)
	 * })
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $color;

	/**
	 * @var integer
	 * Réservations
	 * @ORM\Column(type="integer", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $reservations;

	/**
	 * @var integer
	 * Réservations max
	 * @ORM\Column(type="integer", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $maxreservations;

	/**
	 * @var integer
	 * Réservations min
	 * @ORM\Column(type="integer", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $minreservations;

	/**
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $urls;

	protected $lastaccess;

	public function __construct() {
		// parent::__construct();
		$this->journeys = new ArrayCollection();
		$this->name = null;
		$this->title = null;
		$this->menutitle = null;
		$this->description = null;
		$this->accroche = null;
		$this->place = null;
		$this->initStartEnd(true);
		$this->duration = 0;
		$this->allday = false;
		$this->event = null;
		$this->type = null;
		$this->parent = null;
		$this->childs = new ArrayCollection();
		$this->color = null;
		$this->reservations = 0;
		$this->maxreservations = 0;
		$this->minreservations = 0;
		$this->lastaccess = null;
		// Init traits
		// $this->init_BaseEntity();
		// $this->init_BaseItem();
		// $this->init_ClassDescriptor();
		// $this->initCreated();
		return $this;
	}

	/**
	 * Get as string
	 * @return string 
	 */
	public function __toString() {
		return (string)$this->id;
	}

	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}


	/*************************************************************************************/
	/*** JOURNEYS
	/*************************************************************************************/

	public function getJourneys() {
		return $this->journeys;
	}

	public function addJourney(Journey $journey, $inverse = true) {
		if(!$this->journeys->contains($journey)) $this->journeys->add($journey);
		if($inverse) $journey->addEventdate($this, false);
		return $this;
	}

	public function removeJourney(Journey $journey, $inverse = true) {
		if($inverse) $journey->removeEventdate($this, false);
		$this->journeys->removeElement($journey);
		return $this;
	}


	/*************************************************************************************/
	/*** TYPE
	/*************************************************************************************/

	/**
	 * Set type
	 * @param string $type
	 * @return Eventdate
	 */
	protected function setType($type) {
		$this->type = $type;
		return $this;
	}

	/**
	 * Get type
	 * @return string 
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Is type
	 * @param mixed $type
	 * @return string 
	 */
	public function isType($type) {
		return is_array($type) ? in_array($this->type, $type) : $this->type === $type;
	}



	/**
	 * Get name
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set name
	 * @param string $name
	 * @return Eventdate
	 */
	public function setName($name) {
		$old_name = $this->name;
		$name = serviceTools::getTextOrNullStripped($name);
		$name = serviceTools::removeSeparator($name);
		if($old_name !== $name) {
			$this->name = $name;
		}
		return $this;
	}

	/**
	 * Get title
	 * @return string 
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Set title
	 * @param string $title
	 * @return Eventdate
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	/**
	 * Get menutitle
	 * @return string 
	 */
	public function getMenutitle() {
		return $this->menutitle;
	}

	/**
	 * Set menutitle
	 * @param string $menutitle
	 * @return Eventdate
	 */
	public function setMenutitle($menutitle) {
		$this->menutitle = $menutitle;
		return $this;
	}

	// /**
	//  * Set menutitle if not defined
	//  * @ORM\PrePersist()
	//  * @ORM\PreUpdate()
	//  * @return Eventdate
	//  */
	// public function prePersist() {
	// 	if(!is_string(serviceTools::getTextOrNull($this->title))) $this->title = $this->getName();
	// 	if(!is_string(serviceTools::getTextOrNull($this->menutitle))) $this->menutitle = $this->getTitle();
	// 	return $this;
	// }

	/**
	 * Get description
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set description
	 * @param string $description = null
	 * @return Eventdate
	 */
	public function setDescription($description = null) {
		$this->description = $description;
		return $this;
	}

	/**
	 * Get accroche
	 * @return string
	 */
	public function getAccroche() {
		return $this->accroche;
	}

	/**
	 * Set accroche
	 * @param string $accroche = null
	 * @return Eventdate
	 */
	public function setAccroche($accroche = null) {
		$this->accroche = $accroche;
		return $this;
	}

	/**
	 * Get place
	 * @return string
	 */
	public function getPlace() {
		return $this->place;
	}

	/**
	 * Set place
	 * @param string $place = null
	 * @return Eventdate
	 */
	public function setPlace($place = null) {
		$this->place = $place;
		return $this;
	}



	public function initStartEnd($force = false) {
		if(!($this->start instanceOf DateTime) || $force) {
			$start = new DateTime();
			$start->setTime(Eventdate::DEFAULT_H_START['H'], Eventdate::DEFAULT_H_START['i'], Eventdate::DEFAULT_H_START['s']);
			$this->start = $start;
		}
		if(!($this->end instanceOf DateTime) || $force) {
			$end = new DateTime();
			$end->setTime(Eventdate::DEFAULT_H_END['H'], Eventdate::DEFAULT_H_END['i'], Eventdate::DEFAULT_H_END['s']);
			$this->end = $end;
		}
		return $this;
	}

	/**
	 * Set start
	 * @param DateTime $start
	 * @return Eventdate
	 */
	public function setStart(DateTime $start, $control = true) {
		if($control) {
			if($this->end instanceOf DateTime && $start instanceOf DateTime && $start > $this->end) throw new Exception("Error ".__METHOD__."(): start date can not be after end date!", 1);
		}
		if($this->isAllday() && $this->getStart()->format('His') == 0) {
			// don't change
		} else {
			$this->start = $start;
		}
		return $this;
	}

	/**
	 * Get start
	 * BEWARE: if ALLDAY, start is done with hour to 00:00:00 !!!
	 * USE getRealStart() to get real value of start.
	 * @return DateTime
	 */
	public function getStart() {
		if($this->isAllday()) {
			$start = clone $this->start;
			$start->setTime(0,0,0);
			return $start;
		}
		return $this->start;
	}
	public function getRealStart() {
		return $this->start;
	}

	/**
	 * Set end
	 * @param DateTime $end = null
	 * @param boolean $control = true
	 * @return Eventdate
	 */
	public function setEnd(DateTime $end = null, $control = true) {
		if($control) {
			if($this->start instanceOf DateTime && $end instanceOf DateTime && $end < $this->start) throw new Exception("Error ".__METHOD__."(): end date can not be before start date!", 1);
		}
		if($this->isAllday()) {
		// if($this->isAllday() && (!($this->end instanceOf DateTime) || $this->getEnd(true)->format('His') == 0)) {
			// don't change
			die('<p>Fonctionnalité AllDay à terminer, SVP. Conctactez l\'administrateur du site.</p>');
		} else {
			$this->end = $end;
		}
		return $this;
	}

	/**
	 * Get end
	 * BEWARE: if ALLDAY, end is done with hour to 23:59:59.999 !!!
	 * USE getRealEnd() to get real value of end.
	 * @param boolean $dateAnyway = false
	 * @param boolean $tomorrowZerotIfAllday = true
	 * @return DateTime | null
	 */
	public function getEnd($dateAnyway = false, $tomorrowZerotIfAllday = true) {
		if(!($this->end instanceOf DateTime) && $this->start instanceOf DateTime && ($this->isAllday() || $dateAnyway)) {
			$date = clone $this->start;
			if($tomorrowZerotIfAllday) $date->modify('+1 day')->setTime(0,0,0);
				else $date->setTime(23,59,59.999);
			return $date;
		}
		return $this->end;
	}
	public function getRealEnd() {
		return $this->end;
	}


	public function getAllDates($asDaymark = false, $onlyFirstAsStart = false) {
		$dates = [];
		if(empty($this->start)) return $dates;
		$current = clone $this->start;
		$end = serviceEvent::getDaymarkFormat($this->getEnd(true, false));
		$control = 0;
		do {
			$daymark = serviceEvent::getDaymarkFormat($current);
			$dates[$daymark] = clone $current;
			if($daymark == $end || $onlyFirstAsStart) break;
			$current->modify('+1 day');
			if($control++ > 365) throw new Exception("Error ".__METHOD__."(): counting dates may have infinite loop (exceeded 1 year loops)!", 1);
		} while ($daymark !== $end);
		return $asDaymark ? array_keys($dates) : $dates;
	}



	/*************************************************************************************************/
	/*** FORM DATE, START, END
	/*************************************************************************************************/

	public function getCdate() {
		return $this->start instanceOf DateTime ? $this->start->format(static::FORMAT_CDATE) : new DateTime('tomorrow');
	}

	public function setCdate($cdate) {
		if(!empty($this->getId()) && $this->start instanceOf DateTime) return $this;
		if(is_string($cdate)) {
			switch (static::FORMAT_CDATE) {
				case 'd/m/Y':
					$cdate = preg_replace('#^(\\d{2})\\/(\\d{2})\\/(\\d{4})$#', '$2/$1/$3', $cdate);
					break;
			}
			$cdate = new DateTime($cdate);
		}
		$this->initStartEnd(false);
		serviceTools::pickDate($this->start, $cdate);
		serviceTools::pickDate($this->end, $cdate);
		return $this;
	}

	public function getCstart() {
		return $this->start;
	}

	public function setCstart(DateTime $cstart) {
		$this->initStartEnd(false);
		serviceTools::pickTime($this->start, $cstart);
		return $this;
	}

	public function getCend() {
		return $this->end;
	}

	public function setCend(DateTime $cend) {
		$this->initStartEnd(false);
		serviceTools::pickTime($this->end, $cend);
		return $this;
	}


	/*************************************************************************************************/
	/*** DAYMARKS
	/*************************************************************************************************/

	/**
	 * Get Daymarks
	 * @return array
	 */
	public function getDaymarks($onlyFirstAsStart = false) {
		return $this->getAllDates(true, $onlyFirstAsStart);
	}

	/**
	 * Get main Daymark (by start date)
	 * @return string
	 */
	public function getDaymark() {
		$daymarks = $this->getAllDates(true, true);
		return reset($daymarks);
	}

	public function hasDaymark($daymark, $onlyFirstAsStart = false) {
		$daymarks = $this->getDaymarks($onlyFirstAsStart);
		return in_array($daymark, $daymarks);
	}

	public function getDuration() {
		$this->duration = $this->getEnd(true, false)->diff($this->getStart());
		return $this->duration;
	}

	/**
	 * Get allday
	 * @return boolean 
	 */
	public function getAllday() {
		return $this->allday;
	}

	/**
	 * Is allday
	 * @return boolean 
	 */
	public function isAllday() {
		return $this->allday;
	}

	/**
	 * Set allday
	 * @param boolean $allday = true
	 * @return Eventdate
	 */
	public function setAllday($allday = true) {
		$this->allday = $allday;
		return $this;
	}





	/**
	 * Get Event
	 * @return Event
	 */
	public function getEvent() {
		return $this->event;
	}

	/**
	 * Set Event
	 * @param Event $event
	 * @param boolean $inverse = true
	 * @return Eventdate
	 */
	public function setEvent(Event $event = null, $inverse = true, $resetTimeToMost = false) {
		if($event instanceOf Event) {
			if($this->event instanceOf Event) $this->removeEvent();
			if($inverse) $event->addEventdate($this, false);
			$this->setName($event->getName());
			$this->setMenutitle($event->getMenutitle());
			$this->setDescription($event->getDescription());
			$this->setAccroche($event->getAccroche());
			$this->setColor($event->getColor());
			// $this->setReservations($event->getReservations());
			$this->setMaxreservations($event->getMaxreservations());
			$this->setMinreservations($event->getMinreservations());
			// start & end
			// if(!($this->start instanceOf DateTime && $this->end instanceOf DateTime)) {
			// 	$event->checkMostStartEnds();
				if(!($this->start instanceOf DateTime)) $this->start = clone $event->getMoststart();
				if(!($this->end instanceOf DateTime)) $this->end = clone $event->getMostend();
			// }
			if($resetTimeToMost) {
				$event->checkMostStartEnds();
				// Start time
				$mostStart = $event->getMoststart('auto');
				$mostStart = explode(':', $mostStart);
				$this->start->setTime($mostStart[0],$mostStart[1],$mostStart[2]);
				// End time
				if($this->end instanceOf DateTime) {
					$mostEnd = $event->getMostend('auto');
					$mostEnd = explode(':', $mostEnd);
					$this->end->setTime($mostEnd[0],$mostEnd[1],$mostEnd[2]);
				}
			}
			$this->setType($event->getShortName());
			// switch ($event->getShortName()) {
			// 	case 'Concert':
			// 		$this->setPlace($event->getPlace());
			// 		break;
			// }
		} else if($this->event instanceOf Event) {
			return $this->removeEvent();
		}
		$this->event = $event;
		return $this;
	}

	public function removeEvent($inverse = true) {
		if(!empty($this->event) && $inverse) $this->event->removeEventdate($this, false);
		$this->event = null;
		return $this;
	}


	/**
	 * Get last access if $start or $end
	 * @return DateTime | null
	 */
	public function getLastaccess() {
		if(!($this->getEvent() instanceOf Event)) return null;
		switch ($this->getEvent()->getLongtype()) {
			case 'Visite_free':
				return $this->getEnd(true);
				break;
			default:
				return $this->getStart();
				break;
		}
	}


	/**
	 * Is deprecated
	 * @param mixed $date = null
	 * @return boolean
	 */
	public function isDeprecated($date = null) {
		if(is_string($date)) $date = new DateTime($date);
		if(!($date instanceOf DateTime)) $date = new DateTime();
		return $this->getEnd(true) < $date;
	}



	/**
	 * Get parent Eventdate
	 * @return Eventdate | null
	 */
	public function getParent() {
		return $this->parent;
	}

	/**
	 * Set parent Eventdate
	 * @param Eventdate $parent = null
	 * @return Eventdate
	 */
	public function setParent(Eventdate $parent = null) {
		$this->parent = $parent;
		if($this->parent instanceOf Eventdate) {
			$this->setEvent($parent->getEvent());
		}
		return $this;
	}

	/**
	 * Add Child Eventdate
	 * @param Eventdate $child
	 * @return Eventdate
	 */
	public function addChild(Eventdate $child) {
		if(!$this->childs->contains($child)) $this->childs->add($child);
		$child->setParent($this);
		return $this;
	}

	/**
	 * Set Childs Eventdate
	 * @param ArrayCollection <Eventdate> $childs
	 * @return Eventdate
	 */
	public function setChilds(ArrayCollection $childs) {
		foreach ($this->childs as $child) {
			if(!$childs->contains($child)) $this->removeChild($child);
		}
		foreach ($childs as $child) {
			$this->addChild($child);
		}
		return $this;
	}

	/**
	 * Get Childs Eventdate
	 * @return ArrayCollection
	 */
	public function getChilds() {
		return $this->childs;
	}

	/**
	 * Remove Child Eventdate
	 * @param Eventdate $child
	 * @return Eventdate
	 */
	public function removeChild(Eventdate $child) {
		$child->setParent(null);
		return $this->childs->removeElement($child);
	}




	/**
	 * Get color
	 * @return string 
	 */
	public function getColor() {
		return $this->color;
	}

	/**
	 * Set color
	 * @param string $color
	 * @return Eventdate
	 */
	public function setColor($color) {
		$this->color = $color;
		return $this;
	}

	/**
	 * Get reservations
	 * @return integer
	 */
	public function getReservations() {
		return $this->reservations;
	}

	/**
	 * Set reservations
	 * @param integer $reservations = true
	 * @return Eventdate
	 */
	public function setReservations($reservations) {
		$this->reservations = $reservations;
		return $this;
	}

	/**
	 * Get maxreservations
	 * @return integer
	 */
	public function getMaxreservations() {
		return $this->maxreservations;
	}

	/**
	 * Set maxreservations
	 * @param integer $maxreservations = true
	 * @return Eventdate
	 */
	public function setMaxreservations($maxreservations) {
		$this->maxreservations = $maxreservations;
		return $this;
	}

	/**
	 * Get minreservations
	 * @return integer
	 */
	public function getMinreservations() {
		return $this->minreservations;
	}

	/**
	 * Set minreservations
	 * @param integer $minreservations = true
	 * @return Eventdate
	 */
	public function setMinreservations($minreservations) {
		$this->minreservations = $minreservations;
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * Verify minreservations not > maxreservations
	 * @return Eventdate
	 */
	public function verifMinreservations() {
		if($this->getMinreservations() > $this->getMaxreservations()) $this->setMinreservations($this->getMaxreservations());
	}

}