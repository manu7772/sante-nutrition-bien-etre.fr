<?php
namespace ModelApi\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Service\serviceVisite;

use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ModelApi\EventBundle\Repository\VisiteRepository")
 * @ORM\Table(
 *      name="visite",
 *		options={"comment":"Visites pédagogiques"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_visite",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @Annot\HasTranslatable()
 */
class Visite extends Event {

	const DEFAULT_ICON = 'fa-fort-awesome';
	const DEFAULT_OWNER_DIRECTORY = '@@@default@@@';
	const ENTITY_SERVICE = serviceVisite::class;

	/**
	 * @var string
	 * @ORM\Column(name="age", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=11,
	 * 		options={"required"=false, "description"="À partir de quel âge et jusqu'à quel âge, pour cet évènement."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=11,
	 * 		options={"required"=false, "description"="À partir de quel âge et jusqu'à quel âge, pour cet évènement."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Show(role=true, order=11)
	 */
	protected $age;

	/**
	 * @var string
	 * @ORM\Column(name="during", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=12,
	 * 		options={"required"=false, "description"="Durée de cet évènement."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=12,
	 * 		options={"required"=false, "description"="Durée de cet évènement."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Show(role=true, order=12)
	 */
	protected $during;

	/**
	 * @var string
	 * @ORM\Column(name="weather", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=13,
	 * 		options={"required"=false, "description"="Conditions climatiques pour cet évènement : sous abri, annulé en cas de forte pluie, etc."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=13,
	 * 		options={"required"=false, "description"="Conditions climatiques pour cet évènement : sous abri, annulé en cas de forte pluie, etc."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Show(role=true, order=13)
	 */
	protected $weather;

	/**
	 * @var array
	 * @ORM\ManyToOne(targetEntity="ModelApi\EventBundle\Entity\Visite", inversedBy="childs")
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		contexts={"all"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=102,
	 * 		options={"multiple"=false, "required"=false, "class"="ModelApi\EventBundle\Entity\Visite", "query_builder"="auto"},
	 * 		contexts={"simple", "medium", "expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		contexts={"all"},
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=102,
	 * 		options={"multiple"=false, "required"=false, "class"="ModelApi\EventBundle\Entity\Visite", "query_builder"="auto"},
	 * 		contexts={"simple", "medium", "expert"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=102)
	 */
	protected $parentvisite;

	/**
	 * @var array
	 * @ORM\OneToMany(targetEntity="ModelApi\EventBundle\Entity\Visite", mappedBy="parentvisite", orphanRemoval=true)
	 * @ORM\JoinTable(name="visite_childs",
	 *     joinColumns={@ORM\JoinColumn(name="childs_id", referencedColumnName="id")},
	 *     inverseJoinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")}
	 * )
	 * @CRUDS\Show(role="ROLE_USER", order=102)
	 */
	protected $childs;

	public function __construct() {
		parent::__construct();
		$this->age = null;
		$this->during = null;
		$this->weather = null;
		$this->parentvisite = null;
		$this->childs = new ArrayCollection();
	}


	/**
	 * Get enabled
	 * @return boolean 
	 */
	public function getEnabled() {
		if($this->getParentvisite() instanceOf Visite && $this->getParentvisite()->isDisabled()) return false;
		return $this->enabled;
	}


	/**
	 * Get age
	 * @return string 
	 */
	public function getAge() {
		return $this->age;
	}

	/**
	 * Set age
	 * @param string $age
	 * @return Visite
	 */
	public function setAge($age) {
		$this->age = $age;
		return $this;
	}

	/**
	 * Get during
	 * @return string 
	 */
	public function getDuring() {
		return $this->during;
	}

	/**
	 * Set during
	 * @param string $during
	 * @return Visite
	 */
	public function setDuring($during) {
		$this->during = $during;
		return $this;
	}

	/**
	 * Get weather
	 * @return string 
	 */
	public function getWeather() {
		return $this->weather;
	}

	/**
	 * Set weather
	 * @param string $weather
	 * @return Visite
	 */
	public function setWeather($weather) {
		$this->weather = $weather;
		return $this;
	}


	/**
	 * Get parentvisite Visite
	 * @return Visite | null
	 */
	public function getParentvisite() {
		return $this->parentvisite;
	}

	/**
	 * Has parentvisite Visite
	 * @return boolean
	 */
	public function hasParentvisite() {
		return $this->parentvisite instanceOf Visite;
	}

	/**
	 * Set parentvisite Visite
	 * @param Visite $parentvisite = null
	 * @return Visite
	 */
	public function setParentvisite(Visite $parentvisite = null, $inverse = true) {
		if($inverse && $this->parentvisite instanceOf Visite) {
			$this->parentvisite->removeChild($this, false);
		}
		$this->parentvisite = $parentvisite;
		if($this->parentvisite instanceOf Visite) {
			if($inverse) $this->parentvisite->addChild($this, false);
		}
		return $this;
	}

	/**
	 * Add Child Visite
	 * @param Visite $child
	 * @return Visite
	 */
	public function addChild(Visite $child, $inverse = true) {
		if(!$this->childs->contains($child)) $this->childs->add($child);
		if($inverse) $child->setParentvisite($this, false);
		return $this;
	}

	/**
	 * Set Childs Visite
	 * @param ArrayCollection <Visite> $childs
	 * @return Visite
	 */
	public function setChilds(ArrayCollection $childs) {
		foreach ($this->childs as $child) {
			if(!$childs->contains($child)) $this->removeChild($child);
		}
		foreach ($childs as $child) {
			$this->addChild($child);
		}
		return $this;
	}

	/**
	 * Get Childs Visite
	 * @return ArrayCollection
	 */
	public function getChilds() {
		return $this->childs;
	}

	/**
	 * Remove Child Visite
	 * @param Visite $child
	 * @return Visite
	 */
	public function removeChild(Visite $child, $inverse = true) {
		if($inverse) $child->setParentvisite(null, false);
		return $this->childs->removeElement($child);
	}


}




