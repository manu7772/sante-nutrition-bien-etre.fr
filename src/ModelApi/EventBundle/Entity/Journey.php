<?php
namespace ModelApi\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Entity\Parc;
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Entity\Season;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

use \DateTime;
use \Exception;

/**
 * @ORM\Entity(repositoryClass="ModelApi\EventBundle\Repository\JourneyRepository")
 * @ORM\EntityListeners({"ModelApi\EventBundle\Listener\JourneyListener"})
 * @ORM\Table(
 *      name="journey",
 *		options={"comment":"Journey"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_journey",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @UniqueEntity(fields={"daymark"}, message="Cette journée existe déjà !")
 */
class Journey implements CrudInterface {

	const DEFAULT_ICON = 'fa-pagelines';
	const ENTITY_SERVICE = serviceJourney::class;
	const REQUIRE_PARC = true;
	const UNIQUE_PARC = true;
	const SHORTCUT_CONTROLS = true;

	const DEFAULT_H_START = ['H' => 10, 'i' => 0, 's' => 0];
	const DEFAULT_H_END = ['H' => 17, 'i' => 0, 's' => 0];
	const FORMAT_CDATE = 'd/m/Y';

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN")
	 */
	protected $id;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\EventBundle\Entity\Season", inversedBy="journeys", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 */
	protected $seasons;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\EventBundle\Entity\Eventdate", inversedBy="journeys", orphanRemoval=true, cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 */
	protected $eventdates;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\FileBundle\Entity\Item", inversedBy="journeys")
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 */
	protected $items;

	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\EventBundle\Entity\Parc")
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type=EntityType::class,
	 * 		order=10,
	 * 		contexts={"journey_changeparc","new_season"},
	 * 		options={
	 * 			"multiple"=false,
	 * 			"required"=true,
	 * 			"class"="ModelApi\EventBundle\Entity\Parc",
	 * 			"label"="Parc",
	 * 			"query_builder"="qb_activeParcs",
	 * 		},
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type=EntityType::class,
	 * 		order=1,
	 * 		contexts={"journey_changeparc","new_season"},
	 * 		options={
	 * 			"multiple"=false,
	 * 			"required"=true,
	 * 			"class"="ModelApi\EventBundle\Entity\Parc",
	 * 			"label"="Parc",
	 * 			"query_builder"="qb_activeParcs",
	 * 		},
	 * )
	 * @CRUDS\Show(role=false)
	 * !!!@Annot\AddModelTransformer
	 */
	protected $parc;

	/**
	 * @var integer
	 * @ORM\Column(name="daymark", type="string", length=10, nullable=false, unique=false)
	 * @CRUDS\Show(role=true, order=100, type="string")
	 */
	protected $daymark;

	/**
	 * @var integer
	 * @ORM\Column(name="year", type="string", length=4, nullable=false, unique=false)
	 * @CRUDS\Show(role=true, order=100, type="string")
	 */
	protected $year;

	/**
	 * Begin date
	 * @var DateTime
	 * @ORM\Column(name="start", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=1,
	 * 		options={"by_reference"=false, "required"=true, "label"="Début"},
	 * 		contexts={"journey_changeparc"}
	 * )
	 * @CRUDS\Show(role=true, order=1, type="datetime")
	 */
	protected $start;

	/**
	 * End date
	 * @var DateTime
	 * @ORM\Column(name="end", type="datetime", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=2,
	 * 		options={"by_reference"=false, "required"=true, "label"="Fin"},
	 * 		contexts={"journey_changeparc"}
	 * )
	 * @CRUDS\Show(role=true, order=2, type="datetime")
	 */
	protected $end;

	/**
	 * Begin date
	 * @var DateTime
	 * @ORM\Column(name="parcstart", type="datetime", nullable=true, unique=false)
	 * @CRUDS\Show(role=true, order=102, type="datetime")
	 */
	protected $parcstart;

	/**
	 * End date
	 * @var DateTime
	 * @ORM\Column(name="parcend", type="datetime", nullable=true, unique=false)
	 * @CRUDS\Show(role=true, order=103, type="datetime")
	 */
	protected $parcend;

	/**
	 * @CRUDS\Create(
	 * 		type="TextType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1,
	 * 		options={"by_reference"=false, "required"=true, "label"="Date (jj/mm/aaaa)", "attr"={"class"="date-picker"}},
	 * 		contexts={"journey_changeparc","new_season"},
	 * )
	 * @CRUDS\Show(role=true, order=1, type="datetime")
	 */
	protected $cdate;
	/**
	 * @CRUDS\Create(
	 * 		type="TimeType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=2,
	 * 		options={"by_reference"=false, "required"=true, "label"="Heure de début", "widget"="single_text"},
	 * 		contexts={"journey_changeparc","new_season"},
	 * )
	 * @CRUDS\Show(role=true, order=2, type="datetime")
	 */
	protected $cstart;
	/**
	 * @CRUDS\Create(
	 * 		type="TimeType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=3,
	 * 		options={"by_reference"=false, "required"=true, "label"="Heure de fin", "widget"="single_text"},
	 * 		contexts={"journey_changeparc","new_season"},
	 * )
	 * @CRUDS\Show(role=true, order=3, type="datetime")
	 */
	protected $cend;

	/**
	 * @CRUDS\Create(show=false, update=false, contexts={"journey_changeparc","new_season"})
	 * @CRUDS\Update(show=false, update=false, contexts={"journey_changeparc","new_season"})
	 */
	protected $color;


	public function __construct() {
		$this->id = null;
		$this->seasons = new ArrayCollection();
		$this->eventdates = new ArrayCollection();
		$this->items = new ArrayCollection();
		$this->parc = null;
		$this->daymark = null;
		$this->year = null;
		$this->initStartEnd(true);
		// $this->start = new DateTime();
		// $this->start->setTime(static::DEFAULT_H_START['H'], static::DEFAULT_H_START['i'], static::DEFAULT_H_START['s']);
		$this->parcstart = clone $this->getStart();
		// $this->end = clone $this->start;
		// $this->end->setTime(static::DEFAULT_H_END['H'], static::DEFAULT_H_END['i'], static::DEFAULT_H_END['s']);
		$this->parcend = clone $this->getEnd();
		// $this->setDaymark($this->start);
		// form: day/hours
		// $this->cdate = null;
		// $this->cstart = null;
		// $this->cend = null;
		// $this->init_BaseEntity();
		// $this->init_ClassDescriptor();
		// $this->initCreated();
	}

	/**
	 * Get as string
	 * @return string 
	 */
	public function __toString() {
		return (string)$this->daymark;
	}

	/**
	 * @ORM\PostLoad()
	 */
	public function PostLoad_Journey() {
		$this->getParc();
		return $this;
	}

	// /**
	//  * @ORM\PrePersist()
	//  * @ORM\PreUpdate()
	//  */
	// public function PrePersistUpdate_Journey() {
	// 	$this->checkItems(true);
	// 	$this->computeYear();
	// 	return $this;
	// }

	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	public function isValid() {
		// return $this->eventdates->isEmpty()
		// 	&& static::isValidDaymark($this->daymark)
		// 	&& (!$this->getParcs()->isEmpty() || !static::REQUIRE_PARC)
		return empty($this->getReasonInvalidity());
	}

	public function getReasonInvalidity() {
		if($this->eventdates->isEmpty()) return "No event date found";
		if(!static::isValidDaymark($this->daymark)) return "Daymark is invalid";
		if(static::REQUIRE_PARC && empty($this->getParc())) return "Parc element needed";
		return null;
	}


	public function computeStartEnds() {
		// if($this->eventdates->isEmpty()) return $this;
		// $this->start = null;
		// $this->parcstart = null;
		// $this->end = null;
		// $this->parcend = null;
		$this->checkAllDates(false);
		foreach ($this->eventdates as $eventdate) {
			if(empty($this->getStart()) || $eventdate->getStart() < $this->getStart()) {
				// $this->setStart(clone $eventdate->getStart());
				serviceTools::pickTime($this->start, $eventdate->getStart());
			}
			if(empty($this->getEnd()) || (!empty($eventdate->getEnd()) && $eventdate->getEnd() > $this->getEnd())) {
				// $this->setEnd(clone $eventdate->getEnd());
				serviceTools::pickTime($this->end, $eventdate->getEnd());
			}
			if($eventdate->getType() === 'Parc') {
				// Define parcstart / parcend
				if(empty($this->getParcstart()) || $eventdate->getStart() < $this->getParcstart()) {
					// $this->setParcstart(clone $eventdate->getStart());
					serviceTools::pickTime($this->parcstart, $eventdate->getStart());
				}
				if(empty($this->getParcend()) || $eventdate->getEnd() < $this->getParcend()) {
					// $this->setParcend(clone $eventdate->getEnd());
					serviceTools::pickTime($this->parcend, $eventdate->getEnd());
				}
			}
		}
		return $this;
	}


	/*************************************************************************************************/
	/*** PARCSTART & PARCEND
	/*************************************************************************************************/

	/**
	 * Get Parc start
	 * @return DateTime | null
	 */
	public function getParcstart() {
		return $this->parcstart;
	}

	protected function setParcstart($parcstart = null) {
		$this->parcstart = $parcstart;
		return $this;
	}

	/**
	 * Get Parc end
	 * @return DateTime | null
	 */
	public function getParcend() {
		return $this->parcend;
	}

	protected function setParcend($parcend = null) {
		$this->parcend = $parcend;
		return $this;
	}




	/*************************************************************************************************/
	/*** START & END
	/*************************************************************************************************/

	/**
	 * Set start
	 * @param DateTime $start
	 * @return Eventdate
	 */
	public function setStart(DateTime $start) {
		$this->setDaymark($start);
		if($this->getEnd() instanceOf DateTime && $start > $this->getEnd()) throw new Exception("Error ".__METHOD__."(): start date can not be after end date!", 1);
		$this->start = $start;
		return $this;
	}

	/**
	 * Get start
	 * @return DateTime
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * Set end
	 * @param DateTime $end
	 * @return Eventdate
	 */
	public function setEnd(DateTime $end = null) {
		if($this->getStart() instanceOf DateTime && $end instanceOf DateTime && $end < $this->getStart()) throw new Exception("Error ".__METHOD__."(): end date can not be before start date!", 1);
		$this->end = $end;
		return $this;
	}

	/**
	 * Get end
	 * @param boolean $dateAnyway = false
	 * @param boolean $tomorrowZerotIfAllday = false
	 * @return DateTime
	 */
	public function getEnd($dateAnyway = false, $tomorrowZerotIfAllday = false) {
		if(!$dateAnyway) return $this->end;
		$date = $this->end;
		if(null === $date && $this->getStart() instanceOf DateTime) {
			$date = clone $this->getStart();
			if($tomorrowZerotIfAllday) $date->modify('+1 day')->setTime(0,0,0);
				else $date->setTime(23,59,59);
		}
		return $date;
	}

	public function initStartEnd($force = false) {
		if(!($this->start instanceOf DateTime) || $force) {
			$start = new DateTime();
			$start->setTime(static::DEFAULT_H_START['H'], static::DEFAULT_H_START['i'], static::DEFAULT_H_START['s']);
			$this->setStart($start);
		}
		if(!($this->end instanceOf DateTime) || $force) {
			$end = new DateTime();
			$end->setTime(static::DEFAULT_H_END['H'], static::DEFAULT_H_END['i'], static::DEFAULT_H_END['s']);
			$this->setEnd($end);
		}
		return $this;
	}

	public function getCdate() {
		return $this->start->format(static::FORMAT_CDATE);
	}

	public function setCdate($cdate) {
		if(!empty($this->getId()) && $this->getStart() instanceOf DateTime) return $this;
		if(is_string($cdate)) {
			switch (static::FORMAT_CDATE) {
				case 'd/m/Y':
					$cdate = preg_replace('#^(\\d{2})\\/(\\d{2})\\/(\\d{4})$#', '$2/$1/$3', $cdate);
					break;
			}
			$cdate = new DateTime($cdate);
		}
		$this->initStartEnd(false);
		serviceTools::pickDate($this->start, $cdate);
		serviceTools::pickDate($this->end, $cdate);
		$this->setDaymark($this->start);
		return $this;
	}

	public function getCstart() {
		return $this->start;
	}

	public function setCstart(DateTime $cstart) {
		$this->initStartEnd(false);
		serviceTools::pickTime($this->start, $cstart);
		$this->setDaymark($this->start);
		return $this;
	}

	public function getCend() {
		return $this->end;
	}

	public function setCend(DateTime $cend) {
		$this->initStartEnd(false);
		serviceTools::pickTime($this->end, $cend);
		$this->setDaymark($this->start);
		return $this;
	}




	/*************************************************************************************************/
	/*** DAYMARK & YEAR
	/*************************************************************************************************/

	public static function getDaymarkFormat($dateOrString) {
		$daymark = $dateOrString instanceOf DateTime ? $dateOrString->format('Y-m-d') : $dateOrString;
		if(!static::isValidDaymark($daymark)) throw new Exception("Error ".__METHOD__."(): daymark ".json_encode($daymark)." is invalid!", 1);
		return $daymark;
	}

	public static function isValidDaymark($daymark) {
		if(!is_string($daymark) || !preg_match('/^[\\d]{4}-[\\d]{2}-[\\d]{2}$/', $daymark)) return false;
		$dd = explode('-', $daymark);
		return checkdate((integer)$dd[1], (integer)$dd[2], (integer)$dd[0]);
	}

	public function getDaymark() {
		return $this->daymark;
	}

	public function getJourneydate() {
		return new DateTime($this->getDaymark());
	}

	public function setDaymark($daymark) {
		$daymark = static::getDaymarkFormat($daymark);
		if(!empty($this->id) && !empty($this->daymark) && $this->daymark !== $daymark) throw new Exception("Error ".__METHOD__."(): daymark is allready defined and can never be changed!", 1);
		$this->daymark = $daymark;
		$this->computeYear();
		return $this;
	}

	public function computeYear() {
		if(empty($this->daymark)) return $this;
		$dates = explode('-', $this->daymark);
		$year = reset($dates);
		if(!empty($this->id) && !empty($this->year) && $this->year !== $year) throw new Exception("Error ".__METHOD__."(): year is allready defined and can never be changed!", 1);
		$this->year = $year;
		return $this;
	}

	public function getYear() {
		return $this->year;
	}


	/*************************************************************************************************/
	/*** ITEMS
	/*************************************************************************************************/

	/*** PARCS ***/

	public function getParc() {
		$this->parcs = $this->getParcs();
		$parc = $this->parcs->isEmpty() ? null : $this->parcs->first();
		if(empty($parc) && !empty($this->parc)) $this->setParc($this->parc);
		if(empty($this->parc)) $this->parc = $parc;
		return $this->parc;
	}

	public function setParc(Parc $parc) {
		$this->parc = $parc;
		$this->setParcstart($this->parc->getStart());
		$this->setParcend($this->parc->getEnd());
		$this->setParcs([$this->parc]);
		return $this;
	}

	public function getParcs() {
		$this->parcs = $this->items->filter(function($item) { return $item instanceOf Parc; });
		return $this->parcs;
	}

	public function setParcs($parcs) {
		$this->removeParcs();
		if(static::UNIQUE_PARC) {
			if(count($parcs)) $this->addItem(reset($parcs));
		} else {
			foreach ($parcs as $parc) $this->addItem($parc);
		}
		$this->getParcs(); // control
		return $this;
	}

	public function removeParcs() {
		foreach ($this->getItems() as $item) {
			if($item instanceOf Parc) $this->removeParc($item);
		}
		return $this;
	}

	public function removeParc(Parc $parc) {
		$this->removeItem($parc);
		return $this;
	}

	public function getNoparcs() {
		return $this->items->filter(function($item) { return !($item instanceOf Parc); });
	}

	// public function parcAddModelTransformer(FormBuilderInterface $formBuilder) {
	// 	$entity = $this;
	// 	$formBuilder->addModelTransformer(
	// 		new CallbackTransformer(
	// 			function (Parc $data = null) use ($entity) {
	// 				return $entity->getParc();
	// 			},
	// 			function (Parc $data) {
	// 				// $this->setParc($data);
	// 				return $data;
	// 			}
	// 		)
	// 	);
	// 	return $this;
	// }


	public function getItems() {
		return $this->items;
	}

	public function getEvents() {
		return $this->items->filter(function($item) { return $item instanceOf Event; });
	}

	public function getItemsNotEvents() {
		return $this->items->filter(function($item) { return !($item instanceOf Event); });
	}

	public function addItem(Item $item) {
		$eventdates = new ArrayCollection();
		foreach ($item->getEventdates() as $eventdate) {
			if($this->isEventdateIntegrable($eventdate)) {
				$eventdates->add($eventdate);
				$this->addEventdate($eventdate, true, false);
			}
		}
		if($eventdates->isEmpty()) return false;
		$this->checkItems(true);
		return true;
	}

	public function removeItem(Item $item) {
		if(!$this->items->contains($item)) return false;
		$eventdates = new ArrayCollection();
		foreach ($item->getEventdates() as $eventdate) {
			if($this->isEventdateIntegrable($eventdate)) {
				$eventdates->add($eventdate);
				$this->removeEventdate($eventdate, true, false);
			}
		}
		if($eventdates->isEmpty()) return false;
		$this->checkItems(true);
		return true;
	}

	/**
	 * Check Items by Eventdates
	 * 1. Check all dates of all Eventdates
	 * 2. Memorise all Events of all valid Eventdates
	 * 3. Remove all Events that are not memorized
	 * 4. Add all memorized items
	 * @param boolean $computeStartEnds = true
	 * @return Journey
	 */
	public function checkItems($computeStartEnds = true) {
		$this->checkAllDates(false);
		$events = new ArrayCollection();
		// get all events
		foreach ($this->getEventdates() as $eventdate) {
			$event = $eventdate->getEvent();
			if($event instanceOf Event && !$events->contains($event)) $events->add($event);
		}
		foreach ($this->items as $item) {
			if($item instanceOf Event && !$events->contains($item)) {
				$this->items->removeElement($item);
				// $this->parcs->removeElement($item);
				// if($this->changeparc === $item) $this->changeparc = null;
				foreach ($item->getEventdates() as $eventdate) {
					if($this->isEventdateIntegrable($eventdate)) $eventdate->setEvent(null);
				}
				$item->removeJourney($this, false);
			}
		}
		foreach ($events as $event) {
			if(!$this->items->contains($event)) $this->items->add($event);
			// if($event instanceOf Parc && !$this->parcs->contains($event)) $this->parcs->add($event);
			foreach ($event->getEventdates() as $eventdate) {
				if($this->isEventdateIntegrable($eventdate)) {
					// if($this->eventdates->isEmpty()) $this->setStart($eventdate->getStart());
					if(!$this->eventdates->contains($eventdate)) $this->eventdates->add($eventdate);
					$eventdate->addJourney($this, false);
				}
			}
		}
		if($computeStartEnds) $this->computeStartEnds();
		return $this;
	}


	/*************************************************************************************************/
	/*** SEASONS
	/*************************************************************************************************/

	public function getSeasons() {
		return $this->seasons;
	}

	public function addSeason(Season $season, $inverse = true) {
		if(!$this->seasons->contains($season)) $this->seasons->add($season);
		if($inverse) $season->addJourney($this, false);
		return $this;
	}

	public function removeSeason(Season $season, $inverse = true) {
		if($inverse) $season->removeJourney($this, false);
		$this->seasons->removeElement($season);
		return $this;
	}




	/*************************************************************************************************/
	/*** EVENTDATES
	/*************************************************************************************************/

	public function isEventdateIntegrable(Eventdate $eventdate) {
		if(empty($this->getDaymark())) return true;
		return $eventdate->hasDaymark($this->getDaymark());
	}

	public function checkAllDates($checkItems = true) {
		$falseFound = false;
		foreach ($this->eventdates as $eventdate) $falseFound = $falseFound || $this->checkDate($eventdate, false);
		if($falseFound && $checkItems) $this->checkItems(true);
		return $this;
	}

	public function checkDate(Eventdate $eventdate, $checkItems = true) {
		if(!$this->isEventdateIntegrable($eventdate)) {
			$this->removeEventdate($eventdate, true, $checkItems);
			return false;
		}
		return true;
	}

	public function getEventdates() {
		return $this->eventdates;
	}

	public function addEventdate(Eventdate $eventdate, $inverse = true, $checkItems = true) {
		// echo('<pre><h3>Eventdate START</h3>');var_dump($eventdate->getStart());echo('<pre>');
		if($this->eventdates->isEmpty()) {
			$this->setEnd($eventdate->getEnd(true, false));
			$this->setStart($eventdate->getStart());
		}
		if($this->checkDate($eventdate, false)) {
			if(!$this->eventdates->contains($eventdate)) $this->eventdates->add($eventdate);
			if($inverse) $eventdate->addJourney($this, false);
		}
		if($checkItems) $this->checkItems(true);
		return $this;
	}

	public function removeEventdate(Eventdate $eventdate, $inverse = true, $checkItems = true) {
		if($inverse) $eventdate->removeJourney($this, false);
		if($this->eventdates->contains($eventdate)) {
			$this->eventdates->removeElement($eventdate);
			if($checkItems) $this->checkItems(true);
		}
		return $this;
	}

	public function getNextEventdate($contextdate = null) {
		// if(is_string($contextdate)) $contextdate = new DateTime($contextdate);
		$contextdate = $this->getContextdate($contextdate);
		$eventdates = $this->getEventdates($contextdate);
		return $eventdates->isEmpty() ? null : $eventdates->first();
	}

	public function getNextYear($contextdate = null) {
		$contextdate = $this->getNextEventdate($contextdate);
		return $contextdate instanceOf Eventdate ? $contextdate->getStart()->format('Y') : null;
	}

	public function getNextMonth($contextdate = null) {
		$contextdate = $this->getNextEventdate($contextdate);
		return $contextdate instanceOf Eventdate ? $contextdate->getStart()->format('m') : null;
	}

	public function isObsolete($contextdate = null) {
		return null === $this->getNextEventdate($contextdate);
	}


}




