<?php
namespace ModelApi\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
// use Doctrine\Common\Collections\ArrayCollection;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Service\serviceAnimation;

use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ModelApi\EventBundle\Repository\AnimationRepository")
 * @ORM\Table(
 *      name="animation",
 *		options={"comment":"Animations pédagogiques"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_animation",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @Annot\HasTranslatable()
 */
class Animation extends Event {

	const DEFAULT_ICON = 'fa-rocket';
	const DEFAULT_OWNER_DIRECTORY = '@@@default@@@';
	const ENTITY_SERVICE = serviceAnimation::class;

	/**
	 * @var string
	 * @ORM\Column(name="age", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=11,
	 * 		options={"required"=false, "description"="À partir de quel âge et jusqu'à quel âge, pour cet évènement."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=11,
	 * 		options={"required"=false, "description"="À partir de quel âge et jusqu'à quel âge, pour cet évènement."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Show(role=true, order=11)
	 */
	protected $age;

	/**
	 * @var string
	 * @ORM\Column(name="during", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=12,
	 * 		options={"required"=false, "description"="Durée de cet évènement."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=12,
	 * 		options={"required"=false, "description"="Durée de cet évènement."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Show(role=true, order=12)
	 */
	protected $during;

	/**
	 * @var string
	 * @ORM\Column(name="weather", type="string", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=13,
	 * 		options={"required"=false, "description"="Conditions climatiques pour cet évènement : sous abri, annulé en cas de forte pluie, etc."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		order=13,
	 * 		options={"required"=false, "description"="Conditions climatiques pour cet évènement : sous abri, annulé en cas de forte pluie, etc."},
	 * 		contexts={"permanent"}
	 * )
	 * @CRUDS\Show(role=true, order=13)
	 */
	protected $weather;

	public function __construct() {
		parent::__construct();
		$this->age = null;
		$this->during = null;
		$this->weather = null;
		$this->timemin = 0;
		$this->timemax = 0;
	}

	/**
	 * Get age
	 * @return string 
	 */
	public function getAge() {
		return $this->age;
	}

	/**
	 * Set age
	 * @param string $age
	 * @return Animation
	 */
	public function setAge($age) {
		$this->age = $age;
		return $this;
	}

	/**
	 * Get during
	 * @return string 
	 */
	public function getDuring() {
		return $this->during;
	}

	/**
	 * Set during
	 * @param string $during
	 * @return Animation
	 */
	public function setDuring($during) {
		$this->during = $during;
		return $this;
	}

	/**
	 * Get weather
	 * @return string 
	 */
	public function getWeather() {
		return $this->weather;
	}

	/**
	 * Set weather
	 * @param string $weather
	 * @return Animation
	 */
	public function setWeather($weather) {
		$this->weather = $weather;
		return $this;
	}



}




