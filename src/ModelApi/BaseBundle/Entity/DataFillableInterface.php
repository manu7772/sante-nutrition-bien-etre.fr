<?php
namespace ModelApi\BaseBundle\Entity;


interface DataFillableInterface {

	/**
	 * Get corresponding attribute name
	 * @return string
	 */
	public static function getFieldname($attribute);

	/**
	 * Get forbidden fields for fill operation
	 * @return array
	 */
	public static function getFillForbiddens();

	/**
	 * Is field forbidden for fill operation
	 * @return boolean
	 */
	public static function isFillForbidden($attribute);

	/**
	 * Is field authorized for fill operation
	 * @return boolean
	 */
	public static function isFillAuthorized($attribute);

	/**
	 * Set error message while filled
	 * @param string $message = null
	 * @return self
	 */
	public function setFilledError($message = null);

	/**
	 * Get message if error while filled. If no error, returns null
	 * @return string | null
	 */
	public function getFilledError();

	/**
	 * Fill entity with data
	 * @param array $data
	 * @param string $preferedSetterPrefix = 'add'
	 * @param boolean $authorizeDirectAttribution = true
	 * @return self
	 */
	public function fillWithData($data, $preferedSetterPrefix = 'add', $authorizeDirectAttribution = true);



}

