<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Util\TokenGenerator;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceUserrequest;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
// UserBundle
use ModelApi\UserBundle\Entity\User;

use \Exception;
use \DateTime;

/**
 * Userrequest
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\UserrequestRepository")
 * @ORM\Table(
 *      name="`userrequest`",
 *		options={"comment":"Requêtes utilisateur"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_userrequest",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show="ROLE_ADMIN", create="ROLE_SUPER_ADMIN", update="ROLE_SUPER_ADMIN", delete="ROLE_SUPER_ADMIN")
 */
class Userrequest implements CrudInterface {

	const DEFAULT_ICON = 'fa-bullhorn';
	const ENTITY_SERVICE = serviceUserrequest::class;

	const SHORTCUT_CONTROLS = true;

	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="string")
	 * @ORM\GeneratedValue(strategy="NONE")
	 */
	protected $id;

	/**
	 * Send date
	 * @var DateTime
	 * @ORM\Column(name="sendat", type="datetime", nullable=true, unique=false)
	 */
	protected $sendat;

	/**
	 * userrequest token
	 * @var string
	 * @ORM\Column(name="reqtoken", type="string", nullable=false, unique=true)
	 */
	protected $reqtoken;

	/**
	 * @var integer
	 * Utilisateur pour request
	 * @ORM\ManyToOne(targetEntity="ModelApi\UserBundle\Entity\User", inversedBy="userrequests")
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 */
	protected $requser;

	/**
	 * @var array
	 * Éléments attachés/concernés
	 * @ORM\Column(name="attachedelements", type="json_array", nullable=true)
	 */
	protected $attachedelements;

	/**
	 * @var string
	 * Request name
	 * @ORM\Column(name="reqname", type="string", nullable=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $reqname;

	/**
	 * @var integer
	 * Request status
	 * @ORM\Column(name="reqstatus", type="integer", nullable=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $reqstatus;

	/**
	 * @var array
	 * Historique actions
	 * @ORM\Column(name="historic", type="json_array", nullable=false)
	 */
	protected $historic;

	/**
	 * @var string
	 * @ORM\Column(name="fa_icon", type="string", length=64, nullable=false, unique=false)
	 */
	protected $icon;

	protected $requester;

	public function __construct(User $user, $reqname = null, User $requester = null, $attachedelements = []) {
		$this->id = null;
		$this->reqtoken = TokenGenerator::generateToken();
		$this->sendat = null;
		$this->requester = $requester;
		$this->historic = [];
		$this->attachedelements = $attachedelements;
		$this->setRequser($user);
		$this->setReqname($reqname);
		$this->resetReqstatus($this->requser);
		$this->setIcon(static::DEFAULT_ICON);
	}

	public function getId() {
		return $this->id;
	}

	public function getReqtoken() {
		return $this->reqtoken;
	}

	/**
	 * @ORM\PrePersist()
	 */
	public function prePersist() {
		$this->id = implode('@', [$this->requser->getUsername(), $this->reqname]);
		return $this;
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getId();
	}

	public function isValid() {
		return $this->isValidRequser()
			&& $this->isValidReqname()
			&& $this->isValidReqstatus()
			;
	}

	public function isValidRequser() {
		return $this->requser instanceOf User;
	}

	public function isValidReqname() {
		$validNames = serviceUserrequest::getUserrequestNames();
		return in_array($this->reqname, $validNames);
	}

	public function isValidReqstatus() {
		$validStatus = array_keys(serviceUserrequest::REQUEST_STATUS);
		return in_array($this->reqstatus, $validStatus);
	}

	public function getSendat() {
		return $this->sendat;
	}

	public function isSendat() {
		return $this->sendat instanceOf DateTime;
	}

	public function setSendat(DateTime $date) {
		$this->sendat = $date;
		return $this;
	}

	public function updateSendat() {
		return $this->setSendat(new DateTime());
	}

	/**
	 * Get requser
	 * @return User
	 */
	public function getRequser() {
		return $this->requser;
	}

	/**
	 * Set requser
	 * @param User $requser
	 * @return ModelUserrequest
	 */
	public function setRequser(User $requser, $inverse = true) {
		if(!empty($this->requser) && $this->requser !== $requser) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): user is already defined!", 1);
		$this->requser = $requser;
		if($inverse) $requser->addUserrequests($this, false);
		return $this;
	}

	/**
	 * Get reqname
	 * @return string
	 */
	public function getReqname() {
		return $this->reqname;
	}

	/**
	 * Set reqname
	 * @param string $reqname
	 * @return ModelUserrequest
	 */
	public function setReqname($reqname) {
		if(!empty($this->reqname) && $this->reqname !== $reqname) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): request name is already defined!", 1);
		$this->reqname = strtolower($reqname);
		return $this;
	}

	public static function getDefaultReqstatus() {
		return array_key_first(serviceUserrequest::REQUEST_STATUS);
	}

	public static function getDefaultReqstatusName() {
		return reset(serviceUserrequest::REQUEST_STATUS);
	}

	/**
	 * Get reqstatus
	 * @return integer
	 */
	public function getReqstatus() {
		return $this->reqstatus;
	}

	/**
	 * Set reqstatus
	 * @param integer $reqstatus
	 * @return ModelUserrequest
	 */
	public function setReqstatus($user, $reqstatus, $message = null) {
		serviceUserrequest::numerizeReqstatus($reqstatus);
		$this->reqstatus = $reqstatus;
		$this->addHistoric($user, $message);
		return $this;
	}

	/**
	 * Set reqstatus
	 * @param integer $reqstatus
	 * @return ModelUserrequest
	 */
	public function resetReqstatus($user = null) {
		$this->setReqstatus($user, static::getDefaultReqstatus(), null);
		return $this;
	}

	public function setValidated($user, $message = null) {
		$this->setReqstatus(1, $message);
		return $this;
	}

	public function setRejected($user, $message = null) {
		$this->setReqstatus(2, $message);
		return $this;
	}

	public function getAttachedelements() {
		return $this->attachedelements;
	}

	public function setAttachedelements($attachedelements = []) {
		$this->attachedelements = $attachedelements;
		return $this;
	}

	public function getHistoric() {
		return $this->historic;
	}

	public function addHistoric($user = null, $message = null, DateTime $date = null) {
		if(empty($user) && $this->reqstatus === static::getDefaultReqstatus()) $user = $this->requser;
		if($user instanceOf User) $user = $user->getUsername();
		if(!is_string($user)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): user first parameter must be User or username! Got ".(is_object($user) ? get_class($user) : json_encode($user))."!", 1);
		$date ??= new DateTime();
		$this->historic[$date->getTimestamp()] = [
			'user' => $user,
			'status' => $this->reqstatus,
			'date' => $date->format(DATE_ATOM),
			'message' => is_string($message) ? $message : null,
			'attachedelements' => json_encode($this->attachedelements),
		];
	}



	/**
	 * Set default icon if not defined
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return self
	 */
	public function defaultIconIfNone() {
		if(!is_string($this->getIcon())) $this->setIcon(static::DEFAULT_ICON);
		return $this;
	}

	/**
	 * Set icon
	 * @param string $icon = null
	 * @return self
	 */
	public function setIcon($icon = null) {
		$this->icon = is_string($icon) ? $icon : static::DEFAULT_ICON;
		return $this;
	}

	/**
	 * Get icon
	 * @return string
	 */
	public function getIcon() {
		return $this->icon;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDEFAULT_ICON() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDefaultIcon() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public function getIconClass() {
		return 'fa '.$this->icon;
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public static function getDefaultIconClass() {
		return 'fa '.static::DEFAULT_ICON;
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public function getIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.$this->getIconClass().$classes.'"></i>';
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public static function getDefaultIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.static::getDefaultIconClass().$classes.'"></i>';
	}

}