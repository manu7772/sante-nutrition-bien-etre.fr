<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceBloger;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// BinaryBundle
// use ModelApi\BinaryBundle\Entity\Thumbnail;
use ModelApi\BinaryBundle\Entity\Photo;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;

use \Exception;
use \DateTime;

/**
 * Bloger
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\BlogerRepository")
 * @ORM\Table(
 *      name="`bloger`",
 *		options={"comment":"Bloger"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_bloger",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
abstract class Bloger extends Item {

	const DEFAULT_ICON = 'fa-rss';
	const DEFAULT_OWNER_DIRECTORY = '/Public';
	const ENTITY_SERVICE = serviceBloger::class;
	const DEFAULT_CATEGORY = 'public';


	/**
	 * @var array
	 * @ORM\ManyToOne(targetEntity="ModelApi\UserBundle\Entity\Tier")
	 * @ORM\JoinColumn(name="bloger_maintier", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true},
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $mainrecipient;

	/**
	 * @var array
	 * @ORM\ManyToMany(targetEntity="ModelApi\UserBundle\Entity\Tier", inversedBy="blogers")
	 * @ORM\JoinTable(
	 * 		name="`bloger_tier`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true},
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $recipients;

	/**
	 * @var array
	 * @ORM\ManyToOne(targetEntity="ModelApi\UserBundle\Entity\Tier")
	 * @ORM\JoinColumn(name="bloger_maincontact", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true},
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $maincontact;

	/**
	 * @var array
	 * @ORM\ManyToMany(targetEntity="ModelApi\UserBundle\Entity\Tier")
	 * @ORM\JoinTable(
	 * 		name="`bloger_contact`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true},
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $contacts;

	// /**
	//  * @CRUDS\Create(show=false,update=false)
	//  * @CRUDS\Update(show=false,update=false)
	//  */
	// protected $inputname;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $urls;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $entete;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $photo;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $picone;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $color;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $menutitle;



	public function __construct() {
		parent::__construct();
		$this->mainrecipient = null;
		$this->recipients = new ArrayCollection();
		$this->maincontact = null;
		$this->contacts = new ArrayCollection();
		return $this;
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getName();
	}

	/**
	 * Set title
	 * @param string $title = null
	 * @return self
	 */
	public function setTitle($title = null) {
		parent::setTitle($title);
		// $this->setInputname($this->title);
		return $this;
	}



	public function addRecipient(Tier $recipient, $inverse = true) {
		if(!$this->recipients->contains($recipient)) $this->recipients->add($recipient);
		if($inverse) $recipient->addBloger($this, false);
		return $this;
	}

	public function removeRecipient(Tier $recipient, $inverse = true) {
		$this->recipients->removeElement($recipient);
		if($inverse) $recipient->removeBloger($this, false);
		return $this;
	}

	public function setRecipients($recipients) {
		foreach ($this->recipients as $recipient) {
			if(!$recipients->contains($recipient)) $this->removeRecipient($recipient);
		}
		foreach ($recipients as $recipient) $this->addRecipient($recipient);
		return $this;
	}

	public function getRecipients() {
		return $this->recipients;
	}

	public function setMainrecipient(Tier $mainrecipient = null, $inverse = true) {
		$this->mainrecipient = $mainrecipient;
		if($mainrecipient instanceOf Tier) {
			$this->addRecipient($mainrecipient);
			if($inverse) $mainrecipient->addMainbloger($this, false);
		} else {
			$this->removeRecipient($mainrecipient);
			if($inverse) $mainrecipient->removeMainbloger($this, false);
		}
		return $this;
	}

	public function getMainrecipient() {
		return $this->mainrecipient;
	}



	public function addContact(Tier $contact) {
		if(!$this->contacts->contains($contact)) $this->contacts->add($contact);
		if(empty($this->maincontact)) $this->maincontact = $contact;
		return $this;
	}

	public function removeContact(Tier $contact) {
		$this->contacts->removeElement($contact);
		if($this->maincontact === $contact) $this->maincontact = $this->contacts->isEmpty() ? null : $this->contacts->first();
		return $this;
	}

	public function setContacts($contacts) {
		foreach ($this->contacts as $contact) {
			if(!$contacts->contains($contact)) $this->removeContact($contact);
		}
		foreach ($contacts as $contact) $this->addContact($contact);
		return $this;
	}

	public function getContacts() {
		return $this->contacts;
	}

	public function setMaincontact(Tier $maincontact = null) {
		$this->maincontact = $maincontact;
		if($maincontact instanceOf Tier) {
			$this->addContact($maincontact);
		} else {
			$this->removeContact($maincontact);
		}
		return $this;
	}

	public function getMaincontact() {
		return $this->maincontact;
	}



}