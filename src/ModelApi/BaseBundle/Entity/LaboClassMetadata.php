<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Common\Inflector\Inflector;
// BaseBundle
use ModelApi\BaseBundle\Entity\LaboClassMetadata;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;

class LaboClassMetadata {

	protected $classes;
	protected $interfaces;
	protected $traits;
	protected $allInstances;


	public function __construct(ClassMetadata $classMetadata) {
		$this->classMetadata = $classMetadata;
		$this->classes = array_merge(serviceClasses::getParentClasses($this->classMetadata->name, true, true, true), serviceClasses::getParentClasses($this->classMetadata->name, true, true, false));
		$this->interfaces = array_merge(serviceClasses::getInterfaces($this->classMetadata->name, true), serviceClasses::getInterfaces($this->classMetadata->name, false));
		$this->traits = array_merge(serviceClasses::getTraitNames($this->classMetadata->name, true), serviceClasses::getTraitNames($this->classMetadata->name, false));
		$this->allInstances = array_merge($this->classes, $this->interfaces, $this->traits);
		return $this;
	}

	public function __get($property) {
		return $this->classMetadata->$property;
		// $method = Inflector::camelize('get_'.$property);
		// return $this->classMetadata->$method();
	}

	public function __call($name, $arguments) {
		return call_user_func_array(array($this->classMetadata, $name), $arguments);
		// return $this->classMetadata->$name();
	}

	public function isInstanceOf($classes) {
		if(is_string($classes)) $classes = [$classes];
		// echo('<pre><h3>'.$this->classMetadata->name.'</h3>'); var_dump($classes); echo('</pre>');
		return count(array_intersect($classes, $this->allInstances));
		// foreach ($classes as $class) {
			// if($this instanceOf $class) return true;
			// if(is_a($this->classMetadata->name, $class)) return true;
		// }
		// return false;
	}

	public function getShortname($lower = false) {
		$shortname = $this->classMetadata->getReflectionClass()->getShortName();
		return $lower ? strtolower($shortname) : $shortname;
	}

	public function getClassname() {
		return $this->classMetadata->getReflectionClass()->getName();
	}

	public function getClassIdentifier() {
		$classes = serviceClasses::getParentClasses($this->classMetadata->getReflectionClass()->getName(), true, true, true, true);
		return serviceEntities::getBddid($classes, false);
	}

}