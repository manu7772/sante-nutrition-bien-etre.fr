<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\Bloger;
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceMessage;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Photo;
// use ModelApi\BinaryBundle\Entity\Thumbnail;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;

use \Exception;
use \DateTime;

/**
 * Message
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\MessageRepository")
 * @ORM\Table(
 *      name="`message`",
 *		options={"comment":"Message"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_message",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
class Message extends Bloger {

	const DEFAULT_ICON = 'fa-envelope-o';
	const DEFAULT_OWNER_DIRECTORY = 'system/Messages';
	const ENTITY_SERVICE = serviceMessage::class;


	/**
	 * @var array
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true}
	 * )
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $categorys;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=true,
	 * 		update=true,
	 * 		order=1,
	 * 		options={"required"=true, "multiple"=true, "choice_label"="listingname"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=true,
	 * 		update=true,
	 * 		order=1,
	 * 		options={"required"=true, "multiple"=true, "choice_label"="listingname"},
	 * 	)
	 * @CRUDS\Show(role=true, order=1)
	 */
	protected $recipients;

	/**
	 * @CRUDS\Create(
	 * 		type="HiddenType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1,
	 * 		options={"required"=true},
	 * 		contexts={"private_message"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="HiddenType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=1,
	 * 		options={"required"=true},
	 * 		contexts={"private_message"},
	 * 	)
	 * @CRUDS\Show(role=true, order=1)
	 * @Annot\Bddid()
	 */
	protected $privaterecipient;

	/**
	 * @var DateTime
	 * @ORM\Column(name="publication", type="datetime", nullable=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_SUPER_ADMIN",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $publication;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $photo;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $subtitle;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $accroche;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $description;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $resume;




	public function __construct() {
		parent::__construct();
		$this->setPublication();
		$this->privaterecipient = null;
		$this->photo = null;
		return $this;
	}



	/**
	 * Get privaterecipient
	 * @param mixed $privaterecipient = null (Object or Bddid)
	 * @return Message
	 */
	public function setPrivaterecipient($privaterecipient = null) {
		$this->privaterecipient = $privaterecipient;
		if($privaterecipient instanceOf Tier) $this->setMainrecipient($privaterecipient);
		return $this;
	}

	/**
	 * Get privaterecipient
	 * @return
	 */
	public function getPrivaterecipient() {
		return $this->privaterecipient;
	}


	/*************************************************************************************/
	/*** PUBLICATION
	/*************************************************************************************/

	public function setPublication(DateTime $publication = null) {
		if(!($publication instanceOf DateTime) && !($this->publication instanceOf DateTime)) {
			$this->publication = new DateTime();
		} else {
			$this->publication = $publication;
		}
		return $this;
	}

	public function getPublication() {
		return $this->publication;
	}

	public function isPublished($contextdate = null) {
		// if(null === $contextdate) return $this->publication < new DateTime();
		if(is_bool($contextdate) && true === $contextdate) {
			$contextdate = $this->getContextdate();
		} else if(is_string($contextdate)) {
			$contextdate = new DateTime($contextdate);
		} else {
			$contextdate = $this->getContextdate($contextdate);
		}
		return $this->publication < $contextdate;
	}

	/*************************************************************************************/
	/*** PHOTO
	/*************************************************************************************/

	/**
	 * Set URL as photo
	 * @param string $url
	 * @return Event
	 */
	public function setUrlAsPhoto($url) {
		$photo = new Photo();
		$photo->setUploadFile($url);
		$this->setPhoto($photo);
	}

	/**
	 * Set resource as photo
	 * @param string $resource
	 * @return Event
	 */
	public function setResourceAsPhoto(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$photo = new Photo();
			$photo->setUploadFile($resource);
			$this->setPhoto($photo);
		}
		return $this;
	}

	public function getResourceAsPhoto() {
		return null;
	}

	/**
	 * Set photo
	 * @param Photo $photo = null
	 * @return Event
	 */
	public function setPhoto(Photo $photo = null) {
		$this->photo = $photo;
		return $this;
	}

	/**
	 * Get photo
	 * @return Photo | null
	 */
	public function getPhoto() {
		return $this->photo;
	}




}