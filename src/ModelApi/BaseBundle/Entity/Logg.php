<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceLogg;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Service\serviceContext;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\userBundle\Service\serviceUser;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

use \Exception;
use \DateTime;

/**
 * Logg
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\LoggRepository")
 * @ORM\Table(
 *      name="`logg`",
 *		options={"comment":"Logs"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_logg",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
class Logg implements CrudInterface {

	const DEFAULT_ICON = 'fa-plug';
	const ERROR_ICON = 'fa-times';
	const ENTITY_SERVICE = serviceLogg::class;
	const SEPARATOR = '_';
	// STATUS NAMES
	const INITIALIZED = 'initialized';
	const ENDED = 'ended';
	const NOT_ENDED = 'not_ended';
	// TEST
	const TEST_FULLY_CORRECT = ['GetResponseEvent', 'FilterControllerEvent', 'FilterResponseEvent'];
	const SHORTCUT_CONTROLS = true;

	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * Creation date
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false, unique=false)
	 */
	protected $created;

	/**
	 * End date
	 * @var DateTime
	 * @ORM\Column(name="ended", type="datetime", nullable=true, unique=false)
	 */
	protected $ended;

	/**
	 * @var float
	 * @ORM\Column(name="during", type="float", nullable=false, unique=false)
	 */
	protected $during;

	/**
	 * @var boolean
	 * @ORM\Column(name="closed", type="boolean", nullable=false, unique=false)
	 */
	protected $closed;

	/**
	 * @var array
	 * @ORM\Column(name="context", type="json_array", nullable=false, unique=false)
	 */
	protected $context;

	/**
	 * @var boolean
	 * @ORM\Column(name="xmlHttp", type="boolean", nullable=false, unique=false)
	 */
	protected $xmlHttp;

	/**
	 * @var array
	 * @ORM\Column(name="status", type="json_array", nullable=false, unique=false)
	 */
	protected $status;

	/**
	 * @var array
	 * @ORM\Column(name="errors", type="json_array", nullable=true, unique=false)
	 */
	protected $errors;

	/**
	 * @var string
	 * @ORM\Column(name="ipaddrmain", type="string", length=64, nullable=false, unique=false)
	 */
	protected $ipaddrmain;

	/**
	 * @var array
	 * @ORM\Column(name="ipaddrlist", type="json_array", nullable=false, unique=false)
	 */
	protected $ipaddrlist;

	/**
	 * @var string
	 * @ORM\Column(name="method", type="string", length=16, nullable=false, unique=false)
	 */
	protected $method;

	/**
	 * @var string
	 * @ORM\Column(name="uri", type="string", nullable=false, unique=false)
	 */
	protected $uri;

	/**
	 * @var string
	 * @ORM\Column(name="scrips", type="string", nullable=false, unique=false)
	 */
	protected $scrips;

	/**
	 * @var string
	 * @ORM\Column(name="port", type="string", length=16, nullable=false, unique=false)
	 */
	protected $port;

	/**
	 * @var string
	 * @ORM\Column(name="uid", type="string", nullable=false, unique=false)
	 */
	protected $uid;

	/**
	 * @var string
	 * @ORM\Column(name="route", type="string", nullable=false, unique=false)
	 */
	protected $route;

	/**
	 * @var array
	 * @ORM\Column(name="route_params", type="json_array", nullable=false, unique=false)
	 */
	protected $route_params;

	/**
	 * @var array
	 * @ORM\Column(name="request", type="json_array", nullable=false, unique=false)
	 */
	protected $request;

	/**
	 * @var string
	 * @ORM\Column(name="bundle", type="string", nullable=true, unique=false)
	 */
	protected $bundle;

	/**
	 * @var string
	 * @ORM\Column(name="locale", type="string", length=8, nullable=true, unique=false)
	 */
	protected $locale;

	/**
	 * @var string
	 * @ORM\Column(name="env", type="string", length=8, nullable=false, unique=false)
	 */
	protected $env;

	/**
	 * @var string
	 * @ORM\Column(name="userid", type="integer", nullable=true, unique=false)
	 */
	protected $userid;

	/**
	 * @Annot\InsertLoggUser()
	 */
	protected $user;

	/**
	 * @var string
	 * @ORM\Column(name="fa_icon", type="string", length=64, nullable=false, unique=false)
	 */
	protected $icon;

	protected $serviceContext = null;
	protected $serviceLogg = null;
	protected $current = false;

	public function __construct() {
		$this->serviceContext = null;
		$this->serviceLogg = null;
		$this->current = true;
		$this->id = null;
		$this->context = [];
		$this->errors = null;
		$this->status = [];
		$this->xmlHttp = null;
		$this->route = null;
		$this->route_params = [];
		$this->bundle = null;
		$this->locale = null;
		$this->env = null;
		$this->ipaddrmain = null;
		$this->ipaddrlist = [];
		$this->request = [];
		$this->method = null;
		$this->uri = null;
		$this->scrips = null;
		$this->port = null;
		$this->uid = null;
		$this->userid = null;
		$this->user = null;
		$this->initialized = false;
		$this->closed = false;
		$this->setIcon(static::ERROR_ICON);
		$this->created = new DateTime();
		$this->ended = null;
		$this->during = -1;
		// $this->init_ClassDescriptor();
		// $this->initialize();
		return $this;
	}

	/**
	 * @Annot\InitializeLogg()
	 */
	public function initialize(serviceLogg $serviceLogg) {
		$this->serviceLogg = $serviceLogg;
		$action_name = $this->serviceLogg->whenContextInitialized($this, function(serviceContext $serviceContext) {
			// echo('<div>Init Logg => service Context initialisé ? '.($serviceContext->isInitialized() ? 'OUI' : 'NON').'</div>');
			$this->serviceContext = $serviceContext;
			$this->serviceContext->completeContext();
			$this->context = $serviceContext->getAllContextNamedElements(true, true);
			$user = $serviceContext->getUser();
			if($user instanceOf User) $this->setUser($user);
			$this->setInitialized();
		});
		$this->xmlHttp = $this->serviceLogg->isXmlHttpRequest();
		$this->route = $this->serviceLogg->getRoute();
		$this->route_params = $this->serviceLogg->getRoute_params();
		$this->bundle = $this->serviceLogg->getBundleName(true);
		$this->locale = $this->serviceLogg->getLocale();
		$this->env = $this->serviceLogg->getEnv();
		$ips = $this->serviceLogg->getIps();
		$this->ipaddrmain = reset($ips);
		$this->ipaddrlist = $ips;
		$this->method = array_key_exists('REQUEST_METHOD', $_SERVER) ? $_SERVER['REQUEST_METHOD'] : null;
		$this->uri = array_key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI'] : null;
		$this->scrips = array_key_exists('SCRIPT_NAME', $_SERVER) ? $_SERVER['SCRIPT_NAME'] : null;
		$this->port = array_key_exists('SERVER_PORT', $_SERVER) ? $_SERVER['SERVER_PORT'] : null;
		$this->uid = array_key_exists('UNIQUE_ID', $_SERVER) ? $_SERVER['UNIQUE_ID'] : null;
		// REQUEST
		$request = $this->serviceLogg->getRequest();
		if(!($request instanceOf Request)) throw new Exception("Error ".__METHOD__."(): Processing Request. Request does not exist!", 1);
		$this->request = [];
		$parameters = ['HTTP_ACCEPT','HTTP_ACCEPT_LANGUAGE','HTTP_HOST','HTTP_REFERER','HTTP_USER_AGENT','PATH_INFO','REMOTE_PORT','REQUEST_SCHEME','SERVER_PROTOCOL'];
		foreach ($parameters as $parameter) {
			$this->request[$parameter] = array_key_exists($parameter, $_SERVER) ? $_SERVER[$parameter] : null;
		}
		$attributes = ['_controller','_firewall_context','_fos_rest_zone'];
		foreach ($attributes as $attr) {
			$this->request[$attr] = $request->attributes->get($attr);
		}
		// $this->setInitialized();
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function refreshUser() {
		if($this->serviceContext instanceOf serviceContext) {
			$user = $this->serviceContext->getUser();
			if($user instanceOf User) $this->setUser($user);
		} else if($this->serviceLogg instanceOf serviceLogg) {
			$user = $this->serviceLogg->getUser();
			if($user instanceOf User) $this->setUser($user);
		}
		return $this;
	}

	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getId();
	}

	public function isInitialized() {
		$names = array_keys($this->status);
		return $this->initialized && reset($names) === static::INITIALIZED;
	}

	public function setInitialized() {
		$this->initialized = true;
		$this->addStatus(static::INITIALIZED);
		return $this;
	}

	/**
	 * Set closed
	 * @param boolean $closed = true
	 * @return Logg 
	 */
	public function setClosed($closed = true) {
		if($this->isInitialized()) {
			// if($this->context instanceOf serviceContext) $this->setUser($this->context->getUser());
			$this->closed = $closed;
			if($this->isClosed()) {
				$this->setIcon(static::DEFAULT_ICON);
				$this->ended = new DateTime();
				$this->addStatus(static::ENDED);
			} else {
				$this->setIcon(static::ERROR_ICON);
				// $this->ended = null;
				$this->addStatus(static::NOT_ENDED);
			}
		} else {
			throw new Exception("Error ".__METHOD__."(): try to close Logg while not initialized!", 1);
		}
		return $this;
	}

	/**
	 * Get closed
	 * @return boolean
	 */
	public function getClosed() {
		return $this->closed;
	}

	/**
	 * Is closed
	 * @return boolean
	 */
	public function isClosed() {
		return $this->closed;
	}

	/**
	 * Is fully correct
	 * @return boolean
	 */
	public function isFullyCorrect() {
		$test = array_intersect(array_keys($this->status), $this->getTestFullyCorrect());
		return count($test) === count($this->getTestFullyCorrect());
	}

	protected function getTestFullyCorrect() {
		return array_merge(static::TEST_FULLY_CORRECT, [static::INITIALIZED, static::ENDED]);
	}

	/**
	 * Is ended
	 * @return boolean
	 */
	public function isEnded() {
		return $this->ended instanceOf DateTime && $this->getLastStatus() === static::ENDED;
	}

	/**
	 * Is current
	 * @return boolean
	 */
	public function isCurrent() {
		return $this->current;
	}

	/**
	 * Is dev
	 * @return boolean
	 */
	public function isDev() {
		return strtolower($this->env) === 'dev';
	}

	/**
	 * Is prod
	 * @return boolean
	 */
	public function isProd() {
		return strtolower($this->env) === 'prod';
	}

	/**
	 * Is test
	 * @return boolean
	 */
	public function isTest() {
		return strtolower($this->env) === 'test';
	}

	public function getNewStatusName($status) {
		if(array_key_exists($status, $this->status)) {
			$end = 1;
			$exp = explode(static::SEPARATOR, $status);
			if(count($exp) > 1 && preg_match('/^\\d+$/', end($exp))) {
				$end = intval(array_pop($exp)) + 1;
			}
			$status = count($exp) > 1 ? implode(static::SEPARATOR, $exp) : reset($exp);
			return $this->getNewStatusName($status.static::SEPARATOR.$end);
		}
		return $status;
	}

	/**
	 * Add status
	 * @param string $status
	 * @return Logg 
	 */
	public function addStatus($status) {
		$date = new DateTime();
		$status = $this->getNewStatusName($status);
		$this->status[$status] = hrtime();
		$this->defineDuring();
		return $this;
	}

	/**
	 * Get status
	 * @return array
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * Get last status name
	 * @return string
	 */
	public function getLastStatusName() {
		$names = array_keys($this->status);
		return end($names);
	}

	/**
	 * Get Errors
	 * @return array | null
	 */
	public function getErrors() {
		return $this->errors;
	}

	/**
	 * Has Errors
	 * @return boolean
	 */
	public function hasErrors() {
		return count($this->errors) > 0;
	}

	/**
	 * Add Error
	 * @param Response $response
	 * @return Logg
	 */
	public function addError($response) {
		if(!is_array($this->errors)) $this->errors = [];
		if($response instanceOf Response) {
			$this->errors[] = ['error' => $response->getStatusCode(), 'message' => $response->__toString(), 'date' => $response->getDate()];
		} else if($response instanceOf Exception) {
			// echo('<pre>'); var_dump($response); die('</pre>');
			$date = new DateTime();
			$date->setTimezone(new \DateTimeZone('UTC'));
			$this->errors[] = ['error' => $response->getCode(), 'message' => $response->getMessage(), 'date' => $date->format('D, d M Y H:i:s').' GMT'];
		} else if(is_array($response)) {
			if(array_key_exists('error', $response) && array_key_exists('message', $response)) {
				if(!array_key_exists('date', $response)) {
					$date->setTimezone(new \DateTimeZone('UTC'));
					$response['date'] = $date->format('D, d M Y H:i:s').' GMT';
				}
				$this->errors[] = $response;
			}
			// else die("Error ".__METHOD__."(): array needs almost error and message data! Got ".json_encode(array_keys($response))."!");
			// else throw new Exception("Error ".__METHOD__."(): array needs almost error and message data! Got ".json_encode(array_keys($response))."!", 1);
		} else {
			// $test = is_object($response) ? get_class($response) : gettype($response);
			// die("Error ".__METHOD__."(): response data must be Response or array with error and message data! Got ".$test."!");
			// throw new Exception("Error ".__METHOD__."(): response data must be Response or array with error and message data! Got ".$test."!", 1);
		}
		return $this;
	}

	/**
	 * Get first status
	 * @return string | null
	 */
	public function getFirstStatus($name = true) {
		$keys = array_keys($this->status);
		$firstStatus = reset($keys);
		if(empty($firstStatus)) return null;
		return $name ? $firstStatus : $this->status[$firstStatus];
	}

	/**
	 * Get last status
	 * @return string | null
	 */
	public function getLastStatus($name = true) {
		$keys = array_keys($this->status);
		$lastStatus = end($keys);
		if(empty($lastStatus)) return null;
		return $name ? $lastStatus : $this->status[$lastStatus];
	}

	/**
	 * Get during
	 * @return float
	 */
	public function getDuring() {
		return $this->during;
	}

	public function defineDuring() {
		$first = $this->getFirstStatus(false);
		$last = $this->getLastStatus(false);
		if(null === $first || null === $last || json_encode($first) === json_encode($last)) {
			$this->during = -1;
		} else {
			$sec = $last[0] - $first[0];
			$ms = abs($last[1] - $first[1]);
			$this->during = floatval($sec.'.'.$ms);
		}
		return $this;
	}

	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Get ended
	 * @return DateTime | null
	 */
	public function getEnded() {
		return $this->ended;
	}

	/**
	 * Get context
	 * @return array 
	 */
	public function getContext() {
		return $this->context;
	}

	/**
	 * Get xmlHttp
	 * @return boolean 
	 */
	public function getXmlHttp() {
		return $this->xmlHttp;
	}

	/**
	 * Is xmlHttp
	 * @return boolean 
	 */
	public function isXmlHttp() {
		return $this->xmlHttp;
	}

	/**
	 * Get route
	 * @return string
	 */
	public function getRoute() {
		return $this->route;
	}

	/**
	 * Get route params
	 * @return array
	 */
	public function getRoute_params() {
		return $this->route_params;
	}

	/**
	 * Get bundle name
	 * @return string
	 */
	public function getBundle() {
		return $this->bundle;
	}

	/**
	 * Get locale
	 * @return string
	 */
	public function getLocale() {
		return $this->locale;
	}

	/**
	 * Get environment
	 * @return string
	 */
	public function getEnv() {
		return $this->env;
	}

	/**
	 * Get ipaddrmain
	 * @return string
	 */
	public function getIpaddrmain() {
		return $this->ipaddrmain;
	}

	/**
	 * Get ipaddrlist
	 * @return array
	 */
	public function getIpaddrlist() {
		return $this->ipaddrlist;
	}

	/**
	 * Get method
	 * @return string
	 */
	public function getMethod() {
		return $this->method;
	}

	/**
	 * Get uri
	 * @return string
	 */
	public function getUri() {
		return $this->uri;
	}

	/**
	 * Get scrips
	 * @return string
	 */
	public function getScrips() {
		return $this->scrips;
	}

	/**
	 * Get port
	 * @return string
	 */
	public function getPort() {
		return $this->port;
	}

	/**
	 * Get uid
	 * @return string
	 */
	public function getUid() {
		return $this->uid;
	}

	/**
	 * Get User ID
	 * @return string
	 */
	public function getUserid() {
		return $this->userid;
	}

	/**
	 * Set User ID
	 * @param integer $userid = null
	 * @return Logg
	 */
	protected function setUserid($userid = null) {
		if(empty($userid)) return $this;
		if(empty($this->userid)) $this->userid = $userid;
			else if($this->userid !== $userid) throw new Exception("Error ".__METHOD__."(): while refreshing User (with serviceContext). Users are different! old ".$this->userid." should not be replaced by ".$userid."!", 1);
		return $this;
	}

	/**
	 * Set User
	 * @param User $user = null
	 * @return Logg
	 */
	public function setUser(User $user = null) {
		if(empty($user)) return $this;
		$this->user = $user;
		$this->setUserid($user->getId());
		return $this;
	}

	/**
	 * Get User
	 * @return User | null
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Get request attributes and parameters
	 * @return array
	 */
	public function getRequest() {
		return $this->request;
	}




	/**
	 * Set default icon if not defined
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return self
	 */
	public function defaultIconIfNone() {
		if(!is_string($this->getIcon())) $this->setIcon(static::DEFAULT_ICON);
		return $this;
	}

	/**
	 * Set icon
	 * @param string $icon = null
	 * @return self
	 */
	public function setIcon($icon = null) {
		$this->icon = is_string($icon) ? $icon : static::DEFAULT_ICON;
		return $this;
	}

	/**
	 * Get icon
	 * @return string
	 */
	public function getIcon() {
		return $this->icon;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDEFAULT_ICON() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDefaultIcon() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public function getIconClass() {
		return 'fa '.$this->icon;
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public static function getDefaultIconClass() {
		return 'fa '.static::DEFAULT_ICON;
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public function getIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.$this->getIconClass().$classes.'"></i>';
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public static function getDefaultIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.static::getDefaultIconClass().$classes.'"></i>';
	}


}