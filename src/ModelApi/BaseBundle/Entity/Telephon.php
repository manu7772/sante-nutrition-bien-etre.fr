<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceTelephon;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// BinaryBundle
// use ModelApi\BinaryBundle\Entity\Thumbnail;
use ModelApi\BinaryBundle\Entity\Photo;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;

use \Exception;
use \DateTime;

/**
 * Telephon
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\TelephonRepository")
 * @ORM\Table(
 *      name="`telephon`",
 *		options={"comment":"Telephon"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_telephon",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
class Telephon extends Item {

	const DEFAULT_ICON = 'fa-phone-square';
	const DEFAULT_OWNER_DIRECTORY = 'system/Telephons';
	const ENTITY_SERVICE = serviceTelephon::class;
	const DEFAULT_CATEGORY = 'public';


	/**
	 * @var array
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=true, "description"="Catégorie du numéro de téléphone", "choices"="auto", "label"="Catégorie"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=true, "description"="Catégorie du numéro de téléphone", "choices"="auto", "label"="Catégorie"}
	 * )
	 * @CRUDS\Show(role=true, order=99)
	 * @Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 * @Annot\Typitem(groups={"telephon_cat"}, description="Catégorie du numéro de téléphone")
	 */
	protected $categorys;

	/**
	 * @var string
	 * Libellé du numéro de téléphone
	 * @CRUDS\Create(show="ROLE_USER", update="ROLE_USER", type="TextType", order=2, options={"by_reference"=false, "required"=true, "label"="Libellé", "description"="Libellé du numéro de téléphone"}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_USER", update="ROLE_USER", type="TextType", order=2, options={"by_reference"=false, "required"=true, "label"="Libellé", "description"="Libellé du numéro de téléphone"}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $inputname;

	/**
	 * @var string
	 * @ORM\Column(name="phonenumber", type="string", nullable=false, unique=false)
	 * Numéro de téléphone
	 * @CRUDS\Create(show="ROLE_USER", update="ROLE_USER", type="TextType", order=2, options={"by_reference"=false, "required"=true, "label"="Numéro", "description"="Numéro de téléphone. <br>Les formats valides sont :<br>0 000 (000) 000-0000,<br>000 (000) 000-0000,<br>0-000-000-000-0000,<br>000 (000) 000-0000,<br>000-000-000-0000,<br>000-00-0-000-0000,<br>0000-00-0-000-0000,<br>+000-000-000-0000,<br>0 (000) 000-0000,<br>+0-000-000-0000,<br>0-000-000-0000,<br>000-000-0000,<br>(000) 000-0000,<br>000-0000,<br>+0000000000,<br>0000000000,<br>000000000000,<br>+000000000000."}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_USER", update="ROLE_USER", type="TextType", order=2, options={"by_reference"=false, "required"=true, "label"="Numéro", "description"="Numéro de téléphone. <br>Les formats valides sont :<br>0 000 (000) 000-0000,<br>000 (000) 000-0000,<br>0-000-000-000-0000,<br>000 (000) 000-0000,<br>000-000-000-0000,<br>000-00-0-000-0000,<br>0000-00-0-000-0000,<br>+000-000-000-0000,<br>0 (000) 000-0000,<br>+0-000-000-0000,<br>0-000-000-0000,<br>000-000-0000,<br>(000) 000-0000,<br>000-0000,<br>+0000000000,<br>0000000000,<br>000000000000,<br>+000000000000."}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $phonenumber;


	// /**
	//  * @CRUDS\Create(show=false,update=false)
	//  * @CRUDS\Update(show=false,update=false)
	//  */
	// protected $inputname;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $photo;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $urls;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $entete;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $picone;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $color;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $menutitle;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $pageweb;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $tags;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $title;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $fulltext;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $resume;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $description;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $accroche;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $subtitle;



	public function __construct() {
		parent::__construct();
		$this->phonenumber = null;
		$this->photo = null;
		return $this;
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getName();
	}

	public function getPhonenumber($magnify = false) {
		if(empty($this->phonenumber) && !$this->isDefaultModeContext()) $this->setPhonenumber('0000000000');
		return static::magnifyTel($this->phonenumber);
		// return $magnify ? static::magnifyTel($this->phonenumber) : $this->phonenumber;
	}

	public function getPhonenumberHumanized() {
		return static::magnifyTel($this->phonenumber);
	}

	public static function magnifyTel($phonenumber, $group_by = 2) {
		return implode(' ', str_split($phonenumber, $group_by));
	}

	/**
	 * Set phonenumber
	 * @param string $phonenumber = null
	 * @return self
	 */
	public function setPhonenumber($phonenumber = null) {
		// $this->phonenumber = $phonenumber;
		$this->phonenumber = preg_replace('/\\s+/', '', $phonenumber);
		// $this->phonenumber = preg_replace(['/\\s+/','/\\+\\s+/','/-\\s+/','/\\(\\s+/','/\\s+\\)/'], [' ','+','-','(',')'], $phonenumber);
		return $this;
	}

	/**
	 * Is phone number (phonenumber) valid?
	 * @see https://stackoverflow.com/questions/32663386/using-preg-match-to-validate-phone-number-format
	 * @return boolean
	 */
	public function isValidPhonenumber() {
		// return preg_match('/^\\d{10}/', $this->getPhonenumber());

		// return preg_match('/^(\\+\\d{2})?\\d{9,15}$/', $this->getPhonenumber());

		return
			preg_match('/\d?(\s?|-?|\+?|\.?)((\(\d{1,4}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)\d{3}(-|\.|\s)\d{4}/', $this->getPhonenumber())
			||
			preg_match('/([0-9]{8,13})/', preg_replace('/\\s/', '', $this->getPhonenumber()))
			||
			(preg_match('/^\+?\d+$/', $this->getPhonenumber()) && strlen($this->getPhonenumber()) >= 8 && strlen($this->getPhonenumber()) <= 13)
			;
	}


	/*************************************************************************************/
	/*** PHOTO
	/*************************************************************************************/

	/**
	 * Set URL as photo
	 * @param string $url
	 * @return Telephon
	 */
	public function setUrlAsPhoto($url) {
		$photo = new Photo();
		$photo->setUploadFile($url);
		$this->setPhoto($photo);
	}

	/**
	 * Set resource as photo
	 * @param string $resource
	 * @return Telephon
	 */
	public function setResourceAsPhoto(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$photo = new Photo();
			$photo->setUploadFile($resource);
			$this->setPhoto($photo);
		}
		return $this;
	}

	public function getResourceAsPhoto() {
		return null;
	}

	/**
	 * Set photo
	 * @param Photo $photo = null
	 * @return Telephon
	 */
	public function setPhoto(Photo $photo = null) {
		$this->photo = $photo;
		return $this;
	}

	/**
	 * Get photo
	 * @return Photo | null
	 */
	public function getPhoto() {
		return $this->photo;
	}




}