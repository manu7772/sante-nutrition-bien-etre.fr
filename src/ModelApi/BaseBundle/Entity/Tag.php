<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTag;
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

use \Exception;
use \DateTime;

/**
 * Tag
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\TagRepository")
 * @ORM\Table(
 *      name="`tag`",
 *		options={"comment":"Reseaux sociaux"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_tag",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
class Tag implements CrudInterface {

	const DEFAULT_ICON = 'fa-tag';
	const ENTITY_SERVICE = serviceTag::class;
	const SHORTCUT_CONTROLS = true;

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	/**
	 * Tag Id
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", nullable=false, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $name;

	// /**
	//  * @var string
	//  * @ORM\Column(name="fa_icon", type="string", length=64, nullable=false, unique=false)
	//  */
	// protected $icon;

	/**
	 * @var integer
	 * Parent
	 * @ORM\ManyToOne(targetEntity="ModelApi\BaseBundle\Entity\Tag", inversedBy="childs")
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=102,
	 * 		options={"multiple"=false, "required"=false, "group_by"="rootParent", "class"="ModelApi\BaseBundle\Entity\Tag", "query_builder"="auto"},
	 * 		contexts={"permanent"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=102,
	 * 		options={"multiple"=false, "required"=false, "group_by"="rootParent", "class"="ModelApi\BaseBundle\Entity\Tag", "query_builder"="auto"},
	 * 		contexts={"permanent"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_USER", order=102)
	 */
	protected $parent;

	/**
	 * Root Tag
	 * @var integer
	 * @ORM\ManyToOne(targetEntity="ModelApi\BaseBundle\Entity\Tag")
	 */
	protected $rootParent;

	/**
	 * @var array
	 * All parents
	 * @CRUDS\Show(role="ROLE_USER", order=102)
	 */
	protected $allParents;

	/**
	 * @var integer
	 * Level
	 * @ORM\Column(name="lvl", type="integer")
	 * @CRUDS\Show(role="ROLE_USER", order=103)
	 */
	protected $lvl;

	/**
	 * @var array
	 * Direct childs
	 * @ORM\OneToMany(targetEntity="ModelApi\BaseBundle\Entity\Tag", mappedBy="parent")
	 * @ORM\JoinTable(
	 * 		name="`tag_child`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 * @ORM\OrderBy({"name" = "ASC"})
	 */
	protected $childs;

	// /**
	//  * @var array
	//  * All childs
	//  * @ORM\ManyToMany(targetEntity="ModelApi\BaseBundle\Entity\Tag")
	//  */
	protected $allChilds;

	// ChoicelabelType
	// protected $childs;

	/**
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\FileBundle\Entity\Item", mappedBy="tags")
	 * @ORM\JoinTable(
	 * 		name="`tag_item`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 */
	protected $items;


	public function __construct() {
		$this->id = null;
		$this->name = null;
		$this->parent = null;
		$this->rootParent = null;
		$this->allParents = new ArrayCollection();
		$this->lvl = 0;
		$this->childs = new ArrayCollection();
		$this->allChilds = new ArrayCollection();
		$this->items = new ArrayCollection();
		return $this;
	}



	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getName();
	}

	/**
	 * Is Tag valid?
	 * @return boolean
	 */
	
	public function isValid() {
		$this->isOwnerValid() && $this->isRecursivityValid();
	}

	public function isOwnerValid() {
		$root = $this->getRootParent();
		if($root instanceOf Tag) {
			if($root->getOwner() instanceOf Tier && !($this->getOwner() instanceOf Tier)) return false;
		}
		return true;
	}

	public function isRecursivityValid() {
		if($this->hasParent($this, true)) return false;
		if($this->hasChild($this, true)) return false;
	}

	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get name
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set name
	 * @param string $name
	 * @return Tag
	 */
	public function setName($name) {
		$this->name = serviceTools::removeSeparator($name);
		return $this;
	}

	public function getJstreetype() {
		return $this->hasParent() ? $this->getShortname() : 'Root';
	}

	public function getJstreeData() {
		$parent = $this->getParent();
		return array(
			'valid_children' => [$this->getShortname()],
			'can_have_children' => true,
			'name' => $this->getName(),
			'shortname' => $this->getShortname(),
			'parent_id' => $parent instanceOf Tag ? $parent->getId() : null,
			'instances' => $this->getInstances(),
		);
	}

	public function getJstreeState() {
		return [
			'opened' => $this->getLvl() < 4,
			// 'opened' => !$this->hasParent(),
			'disabled' => false,
			'selected' => false,
			'deletable' => !$this->hasChild(),
		];
	}

	public function getLiattr() {
		return [
			'title' => $this->getLongname(),
		];
	}

	public function getAattr() {
		return [];
	}

	/**
	 * {@inheritdoc}
	 * Get name for Jstree
	 * @return string
	 */
	public function getText() {
		return $this->name;
	}



	/*************************************************************************************/
	/*** PARENT GROUPS
	/*************************************************************************************/


	/**
	 * Is root Tag
	 * @return boolean
	 */
	public function isRoot() {
		return !$this->hasParent();
	}

	/**
	 * Has parent Tag
	 * @param Tag $tag = null
	 * @param boolean $recursive = false
	 * @return boolean
	 */
	public function hasParent(Tag $tag = null) {
		if($tag instanceOf Tag) return $tag === $this->getParent();
		return $this->parent instanceOf Tag;
	}

	/**
	 * Set parent Tag
	 * @param Tag $tag = null
	 * @param boolean $reverse = true
	 * @return static
	 */
	public function setParent(Tag $tag = null, $reverse = true) {
		if($tag === $this->parent) return $this;
		if($this->parent instanceOf Tag) $this->parent->removeChild($this, false);
		if($tag instanceOf Tag) {
			$this->parent = $tag;
			if($reverse) $this->parent->addChild($this, false);
		}
		$this->parent = $tag;
		$this->computeAllParents();
		return $this;
	}

	/**
	 * Get parent Tag
	 * @return Tag
	 */
	public function getParent() {
		return $this->parent;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * Get root parent
	 * @return Tag | null
	 */
	public function getRootParent() {
		$this->rootParent = $this->getParent() instanceOf Tag ? $this->getAllParents()->last() : null;
		return $this->rootParent;
	}

	/**
	 * @ORM\PostLoad()
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * Compute all parent Tags
	 * @return static
	 */
	public function computeAllParents() {
		$this->allParents = new ArrayCollection();
		$parent = $this;
		while (null !== $parent = $parent->getParent()) {
			if($this->allParents->contains($parent)) throw new Exception("Error ".__METHOD__."(): this Tag contains parent ".json_encode($parent->getName())." twice!", 1);
			$this->allParents->add($parent);
		}
		return $this;
	}

	/**
	 * Get all parent Tags
	 * @param boolean $inverse = false
	 * @param boolean $recompute = false
	 * @return Tag
	 */
	public function getAllParents($inverse = false, $recompute = false) {
		if($recompute) $this->computeAllParents();
		if($inverse) return new ArrayCollection(array_reverse($this->allParents->toArray()));
		return $this->allParents;
	}

	/**
	 * Get all parent Tags (alias)
	 * @param boolean $inverse = false
	 * @param boolean $recompute = false
	 * @return ArrayCollection <Tag> 
	 */
	public function getParents($inverse = false, $recompute = false) {
		return $this->getAllParents($inverse, $recompute);
	}

	public function getLvl($recompute = false) {
		if($recompute) $this->computeAllParents();
		$this->lvl = $this->allParents->count();
		return $this->lvl;
	}


	/*************************************************************************************/
	/*** CHILD GROUPS
	/*************************************************************************************/

	/**
	 * Set Childs
	 * @param ArrayCollection $childs
	 * @return static
	 */
	public function setChilds($childs) {
		foreach ($this->childs as $child) {
			if(!$childs->contains($child)) $this->removeChild($child);
		}
		foreach ($childs as $child) $this->addChild($child);
		return $this;
	}

	/**
	 * Add child Tag
	 * @param Tag $child
	 * @param boolean $reverse = true
	 * @return static
	 */
	public function addChild(Tag $child, $reverse = true) {
		if(!$this->hasParent($child, true)) {
			if(!$this->hasChild($child, false)) $this->childs->add($child);
			if(!$this->hasChild($child, true)) $this->allChilds->add($child);
			if($reverse) {
				// if($child->getParent() !== $this && $child->getParent() instanceOf Tag) $child->getParent()->removeChild($child, false);
				$child->setParent($this, false);
			}
		}
		return $this;
	}

	/**
	 * Remove child Tag
	 * @param Tag $tag
	 * @return static
	 */
	public function removeChild(Tag $tag, $reverse = true) {
		$this->childs->removeElement($tag);
		$this->allChilds->removeElement($tag);
		if($reverse) $tag->setParent(null);
		return $this;
	}

	/**
	 * Get child Tags
	 * @return ArrayCollection <Tag>
	 */
	public function getChilds() {
		return $this->childs;
	}

	/**
	 * Get number of child Tags
	 * @return integer
	 */
	public function getCountChilds() {
		return $this->childs->count();
	}

	/**
	 * 
	 * 
	 * @return static
	 */
	public function computeAllChilds() {
		$this->allChilds = clone $this->getChilds();
		foreach($this->allChilds as $tag){
			$this->allChilds = new ArrayCollection(array_unique(array_merge($this->allChilds->toArray(), $tag->getAllChilds()->toArray())));
		}
		return $this;
	}

	/**
	 * Get ALL child Tags
	 * @return ArrayCollection <Tag>
	 */
	public function getAllChilds($recompute = false) {
		$this->computeAllChilds();
		return $this->allChilds;
	}

	/**
	 * Has child Tag?
	 * @param Tag $tag
	 * @param boolean $recursive = false
	 * @return boolean
	 */
	public function hasChild(Tag $tag = null, $recursive = false) {
		if(!($tag instanceOf Tag)) return !$this->getChilds()->isEmpty();
		$tags = $recursive ? $this->getAllChilds() : $this->getChilds();
		return $tags->contains($tag);
	}



	/*************************************************************************************/
	/*** ITEMS
	/*************************************************************************************/

	public function getItems() {
		// $items = new ArrayCollection(); for ($i=0; $i < 120; $i++) $items->add($this->items->first()); return $items;
		return $this->items;
	}

	public function getAllItems() {
		$allItems = clone $this->items;
		foreach ($this->getSubItems(true) as $item) {
			if(!$allItems->contains($item)) $allItems->add($item);
		}
		return $allItems;
	}

	public function getSubItems($allParents = true) {
		$subItems = new ArrayCollection();
		$parent = $this->getParent();
		if(empty($parent)) return $subItems;
		$subItems = $parent->getItems();
		if(!$allParents) return $subItems;
		while (null !== $parent = $parent->getParent()) {
			foreach ($parent->getItems() as $item) {
				if(!$subItems->contains($item)) $subItems->add($item);
			}
		}
		return $subItems;
		// return new ArrayCollection(array_unique($subItems->toArray()));
	}

	public function setItems($items) {
		foreach ($this->items as $item) {
			if(!$items->contains($item)) $this->removeItem($item, true);
		}
		foreach ($items as $item) $this->addItem($item, true);
		return $this;
	}

	public function addItem(Item $item, $inverse = true) {
		if(!$this->items->contains($item)) $this->items->add($item);
		if($inverse) $item->addTag($this, false);
		return $this;
	}

	public function removeItem(Item $item, $inverse = true) {
		if($inverse) $item->removeTag($this, false);
		$this->items->removeElement($item);
		return $this;
	}

	public function removeItems() {
		foreach ($this->items as $item) $this->removeItem($item, true);
		return $this;
	}




}