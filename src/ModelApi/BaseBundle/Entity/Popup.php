<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Service\servicePopup;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;

use \Exception;
use \DateTime;

/**
 * Popup
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\PopupRepository")
 * @ORM\Table(
 *      name="`popup`",
 *		options={"comment":"Popups website"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_popup",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @Annot\HasTranslatable()
 */
class Popup extends Item {

	const DEFAULT_ICON = 'fa-window-restore';
	const ENTITY_SERVICE = servicePopup::class;
	const DEFAULT_OWNER_DIRECTORY = '/Popups';
	// const DEFAULT_OWNER_DIRECTORY = null;
	const DEFAULT_ROLE = 'Tous';

	const TYPES_POPUP = [
		'Toastr' => [
			"Toastr succès" => 'Toastr@success',
			"Toastr information" => 'Toastr@info',
			"Toastr important" => 'Toastr@warning',
			"Toastr erreur" => 'Toastr@error',
		],
		'Sweet' => [
			"Sweet basique" => 'Sweet@primary',
			"Sweet information" => 'Sweet@info',
			"Sweet succès" => 'Sweet@success',
			"Sweet important" => 'Sweet@warning',
			"Sweet erreur" => 'Sweet@danger',
		],
		'Bootstrap' => [
			"Bootstrap grande" => 'Bootstrap@modal-lg',
			"Bootstrap petite" => 'Bootstrap@modal-sm',
		],
	];

	/**
	 * Popup Id
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @CRUDS\Create(show="
	 * 		ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type="TextType",
	 * 		order=3,
	 * 		options={"by_reference"="false", "required"=false, "label"="Balise TITLE", "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."},
	 * 		contexts={"medium"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		type="TextType",
	 * 		order=3,
	 * 		options={"by_reference"="false", "required"=false, "label"="Balise TITLE", "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."},
	 * 		contexts={"medium"}
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $menutitle;

	/**
	 * @CRUDS\Create(show="
	 * 		ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type="SummernoteType",
	 * 		order=4,
	 * 		options={"by_reference"="false", "required"=false, "description"="<strong>Sous-titre</strong> : texte placé sous le titre principal (qui est généralement dans la balise H1)."},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		type="SummernoteType",
	 * 		order=4,
	 * 		options={"by_reference"="false", "required"=false, "description"="<strong>Sous-titre</strong> : texte placé sous le titre principal (qui est généralement dans la balise H1)."},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $subtitle;

	/**
	 * @CRUDS\Create(show="
	 * 		ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type="SummernoteType",
	 * 		order=5,
	 * 		options={"by_reference"="false", "required"=false, "description"="<strong>Description</strong> : uniquement pour l'administration. Non visible sur le site public."},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		type="SummernoteType",
	 * 		order=5,
	 * 		options={"by_reference"="false", "required"=false, "description"="<strong>Description</strong> : uniquement pour l'administration. Non visible sur le site public."},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $description;

	/**
	 * @CRUDS\Create(show="
	 * 		ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type="SummernoteType",
	 * 		order=6,
	 * 		options={"by_reference"="false", "required"=false, "description"="<strong>Accroche</strong> : texte d'accroche. Utilisé avant le texte principal de la page, et généralement dans les liens qui dirigent vers la page de cet élément."},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		type="SummernoteType",
	 * 		order=6,
	 * 		options={"by_reference"="false", "required"=false, "description"="<strong>Accroche</strong> : texte d'accroche. Utilisé avant le texte principal de la page, et généralement dans les liens qui dirigent vers la page de cet élément."},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $accroche;


	/**
	 * @var string
	 * @ORM\Column(name="role", type="string", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=false, "choices"="auto"},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "multiple"=false, "choices"="auto"},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $role;

	/**
	 * @annot\injectRoles(setter="setRoles")
	 */
	protected $roles;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true, unique=false)
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(
	 * 		type="SummernoteType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=203,
	 * 		options={"required"=false},
	 * )
	 * @CRUDS\Update(
	 * 		type="SummernoteType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=203,
	 * 		options={"required"=false},
	 * )
	 * @CRUDS\Show(role=true, order=203)
	 */
	protected $alertmsg;


	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $groups;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $pageweb;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $urls;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $color;

	/**
	 * @var integer
	 * Life in session in minutes
	 * @ORM\Column(name="lifesession", type="integer", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=99,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=99,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=99)
	 */
	protected $lifesession;

	/**
	 * @var string
	 * @ORM\Column(name="typepopup", type="string", length=32, nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=false, "choices"="auto"},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=false, "choices"="auto"},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=99)
	 */
	protected $typepopup;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true, unique=false)
	 * Mettre en avant cet Event (Gras, etc.)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=204,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=204,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=204)
	 */
	protected $prime;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true, unique=false)
	 * Automatic dates by ref Item is has
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=205,
	 * 		options={"required"=false},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=205,
	 * 		options={"required"=false},
	 * 		contexts={"medium"}
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=205)
	 */
	protected $automatic;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true, unique=false)
	 * Only one time by session
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=205,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=205,
	 * 		options={"required"=false},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=205)
	 */
	protected $onetime;

	/**
	 * @var integer
	 * Parent
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Item")
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=102,
	 * 		options={"multiple"=false, "required"=false, "group_by"="rootParent", "class"="ModelApi\FileBundle\Entity\Item", "query_builder"="auto"},
	 * 		contexts={"medium"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=102,
	 * 		options={"multiple"=false, "required"=false, "group_by"="rootParent", "class"="ModelApi\FileBundle\Entity\Item", "query_builder"="auto"},
	 * 		contexts={"medium"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=102)
	 */
	protected $refitem;

	/**
	 * @var integer
	 * Parent
	 * @ORM\ManyToMany(targetEntity="ModelApi\PagewebBundle\Entity\Pageweb")
	 * @ORM\JoinTable(
	 * 		name="`popup_pageweb`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=102,
	 * 		options={"multiple"=true, "required"=true, "class"="ModelApi\PagewebBundle\Entity\Pageweb", "query_builder"="auto"},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=102,
	 * 		options={"multiple"=true, "required"=true, "class"="ModelApi\PagewebBundle\Entity\Pageweb", "query_builder"="auto"},
	 * 		contexts={"simple"}
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=102)
	 * @Annot\preferedPageweb(setter="addPageweb")
	 */
	protected $pagewebs;

	/**
	 * Begin date
	 * @var DateTime
	 * @ORM\Column(name="start", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=150,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=150,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=150)
	 */
	protected $start;

	/**
	 * End date
	 * @var DateTime
	 * @ORM\Column(name="end", type="datetime", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=151,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=151,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=151)
	 */
	protected $end;

	/**
	 * Begin date
	 * @var DateTime
	 * @ORM\Column(name="truestart", type="datetime", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=152,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=152,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Show(role=true, order=152)
	 */
	protected $truestart;

	/**
	 * End date
	 * @var DateTime
	 * @ORM\Column(name="trueend", type="datetime", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=153,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=153,
	 * 		options={"required"=false}
	 * )
	 * @CRUDS\Show(role=true, order=153)
	 */
	protected $trueend;

	/**
	 * How many hours screens popup before start (can be negative)
	 * @var Integer
	 * @ORM\Column(name="hours_start", type="integer", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=152,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=152,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=152)
	 */
	protected $hoursstart;

	/**
	 * How many hours stops screen popup after (or before if negative) end (can be negative)
	 * @var Integer
	 * @ORM\Column(name="hours_end", type="integer", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=153,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=153,
	 * 		options={"required"=true},
	 * 		contexts={"simple"}
	 * )
	 * @CRUDS\Show(role=true, order=153)
	 */
	protected $hoursend;


	public function __construct() {
		parent::__construct();
		$this->id = null;
		$this->role = null;
		$this->roles = [];
		$this->onetime = true;
		$this->refitem = null;
		$this->automatic = true;
		$this->prime = false;
		$this->lifesession = 15; // minutes
		$this->setTypepopup();
		$this->start = new DateTime();
		$this->end = null;
		$this->truestart = null;
		$this->trueend = null;
		$this->hoursstart = 0;
		$this->hoursend = 0;
		$this->pagewebs = new ArrayCollection();
		return $this;
	}

	/**
	 * Get choices for Roles
	 * @return array
	 */
	public function getRoleChoices() {
		if(!isset($this->roles[static::DEFAULT_ROLE])) $this->roles[static::DEFAULT_ROLE] = null;
		return $this->roles;
	}

	public function setRoles($roles) {
		$this->roles = $roles;
		if(!isset($this->roles[static::DEFAULT_ROLE])) $this->roles[static::DEFAULT_ROLE] = null;
		return $this;
	}

	public function setRole($role = null) {
		$roles = $this->getRoleChoices();
		if(!in_array($role, $roles)) $role = $roles[static::DEFAULT_ROLE];
		$this->role = $role;
		return $this;
	}

	public function getRole() {
		return $this->role;
	}

	/**
	 * Get choices for Typepopup
	 * @return array
	 */
	public function getTypepopupChoices() {
		return static::TYPES_POPUP;
	}

	/**
	 * Set Typepopup
	 * @param string $typepopup = null
	 * @return Popup
	 */
	public function setTypepopup($typepopup = null) {
		$this->typepopup = is_string($typepopup) ? $typepopup : $this->getDefaultTypepopup();
		return $this;
	}

	/**
	 * Get Typepopup
	 * @return string
	 */
	public function getTypepopup() {
		return $this->typepopup;
	}

	/**
	 * Get Typepopup
	 * @return string
	 */
	public function getOptionTypepopup() {
		$typeoption = explode('@', $this->typepopup);
		return end($typeoption);
	}

	/**
	 * Get Master Typepopup
	 * @return string | null
	 */
	public function getMasterTypepopup() {
		foreach (array_keys(static::TYPES_POPUP) as $type) {
			if(in_array($this->typepopup, static::TYPES_POPUP[$type])) return $type;
		}
		return null;
	}

	// /**
	//  * Get Master Typepopup
	//  * @return string | null
	//  */
	// public function getMasterTypepopup($typepopup = null) {
	// 	if(is_object($typepopup) && method_exists($typepopup, 'getTypepopup')) $typepopup = $typepopup->getTypepopup();
	// 	if(!is_string($typepopup)) $typepopup = $this->typepopup;
	// 	foreach (array_keys(static::TYPES_POPUP) as $type) {
	// 		if(in_array($typepopup, static::TYPES_POPUP[$type])) return $type;
	// 	}
	// 	return null;
	// }

	/**
	 * Get default Typepopup
	 * @return string
	 */
	public function getDefaultTypepopup() {
		$one = $this->getTypepopupChoices();
		while (is_array($one)) $one = reset($one);
		return $one;
	}






	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getName();
	}


	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get title
	 * @param string $title = null
	 * @return self
	 */
	public function setTitle($title = null) {
		$this->title = $title;
		return $this;
	}


	/**
	 * Get menutitle
	 * @param string $menutitle = null
	 * @return self
	 */
	public function setMenutitle($menutitle = null) {
		$this->menutitle = $menutitle;
		return $this;
	}

	/**
	 * Set description
	 * @param string $description = null
	 * @return self
	 */
	public function setDescription($description = null) {
		$this->description = $description;
		return $this;
	}



	public function getPrime() {
		return $this->prime;
	}

	public function setPrime($prime = true) {
		$this->prime = (boolean)$prime;
		return $this;
	}

	public function isLifesession() {
		return $this->getLifesession() > 0 && !$this->isOnetime();
	}

	public function getLifesession($asModifier = false) {
		$lifesession = $this->isOnetime() ? 3600 * 24 : $this->lifesession;
		return $asModifier ? '+'.$lifesession.' MINUTES' : $this->lifesession;
	}

	public function setLifesession($lifesession = 15) {
		$this->lifesession = (integer)$lifesession;
		if($this->lifesession < 0) $this->lifesession = 0;
		return $this;
	}

	public function isOnetime() {
		return $this->onetime;
	}

	public function getOnetime() {
		return $this->onetime;
	}

	public function setOnetime($onetime = true) {
		$this->onetime = (boolean)$onetime;
		return $this;
	}

	/**
	 * Get alertmsg
	 * @return string
	 */
	public function getAlertmsg() {
		return $this->alertmsg;
	}

	/**
	 * Set alertmsg
	 * @param string $alertmsg = null
	 * @return Event
	 */
	public function setAlertmsg($alertmsg = null) {
		$this->alertmsg = $alertmsg;
		return $this;
	}

	/**
	 * Get accroche
	 * @return string
	 */
	public function getAccroche() {
		return $this->accroche;
	}

	/**
	 * Set accroche
	 * @param string $accroche = null
	 * @return Event
	 */
	public function setAccroche($accroche = null) {
		$this->accroche = $accroche;
		return $this;
	}


	/*************************************************************************************/
	/*** PAGEWEBS
	/*************************************************************************************/

	public function getPagewebs() {
		return $this->pagewebs;
	}

	public function addPageweb(Pageweb $pageweb) {
		if(!$this->pagewebs->contains($pageweb)) $this->pagewebs->add($pageweb);
		return $this;
	}

	public function removePageweb(Pageweb $pageweb) {
		return $this->pagewebs->removeElement($pageweb);
	}



	/*************************************************************************************/
	/*** REFERENT ITEM
	/*************************************************************************************/


	/**
	 * Set Referent Item
	 * @param Item $item = null
	 * @return static
	 */
	public function setRefitem(Item $item = null) {
		$this->refitem = $item;
		$this->checkdates();
		return $this;
	}

	/**
	 * Get Referent Item
	 * @return Item | null
	 */
	public function getRefitem() {
		return $this->refitem;
	}

	/**
	 * Has Referent Item
	 * @return boolean
	 */
	public function hasRefitem() {
		return $this->refitem instanceOf Item;
	}




	/*************************************************************************************/
	/*** DATES / START / END
	/*************************************************************************************/

	public function checkdates() {

	}

	/**
	 * Set start
	 * @param DateTime $start
	 * @return Eventdate
	 */
	public function setStart(DateTime $start) {
		$this->start = $start;
		return $this;
	}

	/**
	 * Get start
	 * @return DateTime
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * Get truestart
	 * @return DateTime
	 */
	public function getTruestart() {
		return $this->truestart;
	}

	/**
	 * Define truestart
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return Popup
	 */
	public function defineTruestart() {
		$this->truestart = clone $this->getStart();
		$sign = $this->getHoursstart() > 0 ? '+' : '';
		if($this->getHoursstart() !== 0) $this->truestart->modify($sign.$this->getHoursstart().' hour');
		return $this;
	}

	/**
	 * Set end
	 * @param DateTime $end
	 * @return Eventdate
	 */
	public function setEnd(DateTime $end = null) {
		$this->end = $end;
		return $this;
	}

	/**
	 * Get end
	 * @return DateTime | null
	 */
	public function getEnd() {
		return $this->end;
	}

	/**
	 * Get trueend
	 * @return DateTime | null
	 */
	public function getTrueend() {
		return $this->trueend;
	}

	/**
	 * Define trueend
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return Popup
	 */
	public function defineTrueend() {
		$this->trueend = null;
		if($this->getEnd() instanceOf DateTime) {
			$this->trueend = clone $this->getEnd();
			$sign = $this->getHoursend() > 0 ? '+' : '';
			if($this->getHoursend() !== 0) $this->trueend->modify($sign.$this->getHoursend().' hour');
		}
		return $this;
	}


	public function isShow($date = 'NOW') {
		if(!($date instanceOf DateTime)) $date = new DateTime($date);
		return $date >= $this->truestart && $date < $this->trueend;
	}


	public function setHoursstart($hoursstart = 0) {
		$this->hoursstart = $hoursstart;
		return $this;
	}

	public function getHoursstart() {
		return $this->hoursstart;
	}

	public function setHoursend($hoursend = 0) {
		$this->hoursend = $hoursend;
		return $this;
	}

	public function getHoursend() {
		return $this->hoursend;
	}


	public function setAutomatic($automatic = true) {
		$this->automatic = $automatic;
		// if(!$this->hasRefitem()) $this->automatic = false;
		return $this;
	}

	public function getAutomatic() {
		return $this->automatic;
	}

	public function isAutomatic() {
		return $this->hasRefitem() ? $this->automatic : false;
	}

}