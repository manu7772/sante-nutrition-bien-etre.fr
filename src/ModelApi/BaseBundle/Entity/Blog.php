<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\Bloger;
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceBlog;
// use ModelApi\BinaryBundle\Entity\Thumbnail;
use ModelApi\BinaryBundle\Entity\Photo;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;

use \Exception;
use \DateTime;

/**
 * Blog
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\BlogRepository")
 * @ORM\Table(
 *      name="`blog`",
 *		options={"comment":"Blog"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_blog",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
class Blog extends Bloger {

	const DEFAULT_ICON = 'fa-rss';
	const DEFAULT_OWNER_DIRECTORY = '/Public';
	const ENTITY_SERVICE = serviceBlog::class;


	/**
	 * @var array
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=true, "description"="Type d'affichage de l'annonce", "choices"="auto"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=true, "description"="Type d'affichage de l'annonce", "choices"="auto"}
	 * )
	 * @CRUDS\Show(role=true, order=99)
	 * @Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 * @Annot\Typitem(groups={"blog_cat"}, description="Catégorie du blog")
	 */
	protected $categorys;

	/**
	 * @var DateTime
	 * @ORM\Column(name="publication", type="datetime", nullable=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $publication;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $photo;


	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=109,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsEntete"},
	 * 		contexts={"medium", "expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=109,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsEntete"},
	 * 		contexts={"medium", "expert"}
	 * 	)
	 * @CRUDS\Show(role=true, order=109)
	 */
	protected $entete;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true, "description"="Ajoutez des <strong>tags</strong> pour améliorer le réféncement !"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true, "description"="Ajoutez des <strong>tags</strong> pour améliorer le réféncement !"},
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $tags;


	public function __construct() {
		parent::__construct();
		$this->setPublication();
		$this->photo = null;
		$this->entete = null;
		return $this;
	}



	/*************************************************************************************/
	/*** PUBLICATION
	/*************************************************************************************/

	public function setPublication(DateTime $publication = null) {
		if(!($publication instanceOf DateTime) && !($this->publication instanceOf DateTime)) {
			$this->publication = new DateTime();
		} else {
			$this->publication = $publication;
		}
		return $this;
	}

	public function getPublication() {
		return $this->publication;
	}

	public function isPublished($contextdate = null) {
		if(!$this->isActive()) return false;
		// if(null === $contextdate) return $this->publication < new DateTime();
		if(is_bool($contextdate) && true === $contextdate) {
			$contextdate = $this->getContextdate();
		} else if(is_string($contextdate)) {
			$contextdate = new DateTime($contextdate);
		} else {
			$contextdate = $this->getContextdate($contextdate);
		}
		return $this->publication < $contextdate;
	}

	/*************************************************************************************/
	/*** PHOTO
	/*************************************************************************************/

	/**
	 * Set URL as photo
	 * @param string $url
	 * @return Event
	 */
	public function setUrlAsPhoto($url) {
		$photo = new Photo();
		$photo->setUploadFile($url);
		$this->setPhoto($photo);
	}

	/**
	 * Set resource as photo
	 * @param string $resource
	 * @return Event
	 */
	public function setResourceAsPhoto(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$photo = new Photo();
			$photo->setUploadFile($resource);
			$this->setPhoto($photo);
		}
		return $this;
	}

	public function getResourceAsPhoto() {
		return null;
	}

	/**
	 * Set photo
	 * @param Photo $photo = null
	 * @return Event
	 */
	public function setPhoto(Photo $photo = null) {
		$this->photo = $photo;
		return $this;
	}

	/**
	 * Get photo
	 * @return Photo | null
	 */
	public function getPhoto() {
		return $this->photo;
	}


	/*************************************************************************************/
	/*** ENTETE
	/*************************************************************************************/

	/**
	 * Set URL as entete
	 * @param string $url
	 * @return Blog
	 */
	public function setUrlAsEntete($url) {
		$entete = new Photo();
		$entete->setUploadFile($url);
		$this->setEntete($entete);
	}

	/**
	 * Set resource as entete
	 * @param string $resource
	 * @return Blog
	 */
	public function setResourceAsEntete(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$entete = new Photo();
			$entete->setUploadFile($resource);
			$this->setEntete($entete);
		}
		return $this;
	}

	public function getResourceAsEntete() {
		return null;
	}

	/**
	 * Set entete
	 * @param Entete $entete = null
	 * @return Blog
	 */
	public function setEntete(Photo $entete = null) {
		$this->entete = $entete;
		return $this;
	}

	/**
	 * Get entete
	 * @return Entete | null
	 */
	public function getEntete() {
		return $this->entete;
	}

	/**
	 * Has entete
	 * @return boolean
	 */
	public function hasEntete() {
		return $this->getEntete() instanceOf Photo;
	}


}