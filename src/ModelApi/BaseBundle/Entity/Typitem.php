<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTypitem;
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

use \Exception;
use \DateTime;

/**
 * Typitem
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\TypitemRepository")
 * !!!@ORM\EntityListeners({"ModelApi\BaseBundle\Listener\TypitemListener"})
 * @ORM\Table(
 *      name="`typitem`",
 *		options={"comment":"Types d'Items"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_typitem",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
class Typitem implements CrudInterface {

	const DEFAULT_ICON = 'fa-bandcamp';
	const ENTITY_SERVICE = serviceTypitem::class;
	const SHORTCUT_CONTROLS = true;

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	/**
	 * Typitem Id
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var array
	 * @ORM\ManyToMany(targetEntity="ModelApi\FileBundle\Entity\Item", inversedBy="categorys", cascade={"persist"})
	 * @ORM\JoinTable(
	 * 		name="`typitem_item`",
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false) }
	 * )
	 */
	protected $items;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=100, options={"required"=true})
	 * @CRUDS\Update(show="ROLE_EDITOR", update="ROLE_EDITOR", order=100, options={"required"=true})
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(name="groupitem", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(type="ChoiceType", show="ROLE_EDITOR", update="ROLE_EDITOR", order=100, options={"required"=true, "multiple"=false, "choices"="auto"})
	 * @CRUDS\Update(type="ChoiceType", show="ROLE_EDITOR", update=false, order=100, options={"required"=true, "multiple"=false, "choices"="auto"})
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $groupitem;
	protected $groupitemChoices;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 */
	protected $color;


	public function __construct() {
		$this->id = null;
		$this->items = new ArrayCollection();
		$this->name = null;
		$this->groupitem = null;
		$this->groupitemChoices = [];
		return $this;
	}

	/**
	 * Get choices for groupitem
	 * @return array
	 */
	public function getGroupitemChoices() {
		return $this->groupitemChoices;
	}

	/**
	 * Set choices for groupitem
	 * @return Typitem
	 */
	public function setGroupitemChoices($groupitemChoices) {
		$this->groupitemChoices = $groupitemChoices;
		return $this;
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getName();
	}

	/**
	 * Is Typitem valid?
	 * @return boolean
	 */
	
	public function isValid() {
		$this->isOwnerValid() && $this->isRecursivityValid();
	}

	public function isOwnerValid() {
		$root = $this->getRootParent();
		if($root instanceOf Typitem) {
			if($root->getOwner() instanceOf Tier && !($this->getOwner() instanceOf Tier)) return false;
		}
		return true;
	}

	public function isRecursivityValid() {
		if($this->hasParent($this, true)) return false;
		if($this->hasChild($this, true)) return false;
	}

	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get name
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set name
	 * @param string $name
	 * @return Typitem
	 */
	public function setName($name) {
		$this->name = serviceTools::removeSeparator($name);
		return $this;
	}

	/**
	 * Get groupitem
	 * @return string 
	 */
	public function getGroupitem() {
		return $this->groupitem;
	}

	/**
	 * Set groupitem
	 * @param string $groupitem
	 * @return Typitem
	 */
	public function setGroupitem($groupitem) {
		$this->groupitem = $groupitem;
		return $this;
	}


	/*************************************************************************************/
	/*** ITEMS
	/*************************************************************************************/


	public function getItems() {
		return $this->items;
	}

	// public function setItems($items) {
	// 	foreach ($this->items as $item) {
	// 		if(!$items->contains($item)) $this->removeItem($item);
	// 	}
	// 	foreach ($items as $item) $this->addItem($item, false);
	// 	return $this;
	// }

	public function addItem(Item $item, $inverse = true) {
		if(!$this->items->contains($item)) $this->items->add($item);
		if($inverse) $item->addCategory($this, false);
		return $this;
	}

	public function removeItem(Item $item, $inverse = true) {
		$this->items->removeElement($item);
		if($inverse) $item->removeCategory($this, false);
		return $this;
	}





}