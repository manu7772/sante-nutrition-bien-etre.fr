<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceReseausocial;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// UserBundle
// use ModelApi\UserBundle\Entity\Tier;

use \Exception;
use \DateTime;

/**
 * Reseausocial
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\ReseausocialRepository")
 * @ORM\Table(
 *      name="`reseausocial`",
 *		options={"comment":"Reseaux sociaux"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_reseausocial",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 * @ORM\HasLifecycleCallbacks
 */
class Reseausocial extends Item {

	const DEFAULT_ICON = 'fa-link';
	const BASE_URL = '^(http|https):\\/\\/';
	const DEFAULT_OWNER_DIRECTORY = 'system/Socials';
	const ENTITY_SERVICE = serviceReseausocial::class;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="url", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "by_reference"=false}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "by_reference"=false}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $url;

	// /**
	//  * @ORM\ManyToOne(targetEntity="ModelApi\UserBundle\Entity\Tier", inversedBy="reseausocials")
	//  * @ORM\JoinColumn(nullable=false)
	//  * @Annot\AttachUser(context="environment")
	//  */
	// protected $tier;

	/**
	 * @var string
	 * @ORM\Column(name="type_reseau", type="string", length=64, nullable=false, unique=false)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $typeReseau;

	/**
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=400,
	 * 		options={"required"=false, "attr"={"placeholder"="field.tags"}},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=400,
	 * 		options={"required"=false, "attr"={"placeholder"="field.tags"}},
	 * 		contexts={"expert"}
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $tags;

	/**
	 * Name that was input by user
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 */
	protected $inputname;

	/**
	 * normalizeNameMethod
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 */
	protected $normalizeNameMethod;

	/**
	 * @var integer
	 * Pageweb
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 */
	protected $pageweb;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 */
	protected $picone;


	// DEFINE TRANSLATABLE FIELDS

	/**
	 * @var string
	 * Nom de l'élément - utilisé également pour la balise <title>
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 */
	protected $title;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 */
	protected $name;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 */
	protected $color;

	/**
	 * @var string
	 * Sous-titre de l'élément - utilisé en supplément de title
	 * Texte de 1 à 3 lignes
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 * @Annot\Translatable
	 */
	protected $subtitle;

	/**
	 * @var string
	 * Nom court de l'élément - utilisé pour l'affichage notamment dans les menus
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 * @Annot\Translatable
	 */
	protected $menutitle;

	/**
	 * @var string
	 * Accroche, slogan de l'élément
	 * Texte de 1 ligne
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 * @Annot\Translatable
	 */
	protected $accroche;

	/**
	 * @var string
	 * Description courte de l'élément
	 * Texte de 1 à 3 lignes
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 * @Annot\Translatable
	 */
	protected $description;

	/**
	 * @var string
	 * Texte de l'élément, version courte de fulltext
	 * Texte de 5 ou 6 lignes max
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 * @Annot\Translatable
	 */
	protected $resume;

	/**
	 * @var string
	 * Texte complet de l'élément - illimité
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 * @Annot\Translatable
	 */
	protected $fulltext;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 */
	protected $urls;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false)
	 */
	protected $groups;


	public function __construct() {
		parent::__construct();
		$this->id = null;
		$this->url = null;
		$this->typeReseau = null;
		$this->name = null;
		$this->title = null;
		// $this->tier = null;
		return $this;
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return $this->getTypeReseau().'-'.$this->getId();
	}

	public function isValid() {
		return
			parent::isValid()
			&& !empty($this->getUrl())
			&& !empty($this->getTypeReseau())
			&& !empty($this->getIcon())
			&& !empty($this->getColor())
			// && !empty($this->getName())
			&& !empty($this->getTitle())
		;
	}

	// public function setTier(Tier $tier) {
	// 	$this->tier = $tier;
	// 	return $this;
	// }

	// public function getTier() {
	// 	return $this->tier;
	// }

	// public function removeTier($inverse = true) {
	// 	if(!empty($this->tier)) $this->tier->removeReseausocial($this, false);
	// 	$this->tier = null;
	// 	return $this;
	// }

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @ORM\PostLoad()
	 */
	public function checkData() {
		if(empty($this->url) && preg_match('#'.static::BASE_URL.'#', $this->name)) $this->url = $this->name;
		if(empty($this->url) && preg_match('#'.static::BASE_URL.'#', $this->inputname)) $this->url = $this->inputname;
		$this->setTypeReseau();
		return $this;
	}

	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get url
	 * @return string 
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Set url
	 * @param string $url
	 * @return Reseausocial
	 */
	public function setUrl($url) {
		$this->url = $url;
		$this->setTypeReseau();
		return $this;
	}

	public function isValidUrl() {
		return !empty($this->url) && !empty($this->typeReseau);
	}

	/**
	 * Get typeReseau
	 * @return string 
	 */
	public function getTypeReseau() {
		// if(empty($this->typeReseau)) $this->setTypeReseau();
		return $this->typeReseau;
	}

	/**
	 * Set typeReseau
	 * @param string $typeReseau
	 * @return Reseausocial
	 */
	public function setTypeReseau($typeReseau = null) {
		if(is_string($typeReseau)) {
			$this->typeReseau = $typeReseau;
		} else {
			foreach ($this->getTypeReseaus() as $name => $typeReseau) {
				if(preg_match($typeReseau['preg'], $this->getUrl())) {
					$this->setIcon($typeReseau['icon']);
					$this->setTypeReseau($name);
					break;
				}
			}
		}
		$this->completeNames(true);
		return $this;
	}

	/**
	 * !!!@ORM\PostLoad
	 */
	public function completeNames($refresh = false) {
		if(!empty($this->typeReseau)) {
			if($refresh) {
				$this->setName($this->url);
				$this->setTitle($this->getName());
			} else {
				if(empty($this->getName())) $this->setName($this->url);
				if(empty($this->getTitle())) $this->setTitle($this->getName());
			}
		}
		return $this;
	}



	public function getTypeReseaus() {
		return array(
			// 'facebook' => array('icon' => 'facebook-official'),
			'facebook' => array(
				'icon' => 'fa-facebook',
				'preg' => '#'.static::BASE_URL.'.*(facebook)#',
			),
			'instagram' => array(
				'icon' => 'fa-instagram',
				'preg' => '#'.static::BASE_URL.'.*(instagram)#',
			),
			'google-plus' => array(
				'icon' => 'fa-google-plus',
				'preg' => '#'.static::BASE_URL.'.*(google)#',
			),
			'skype' => array(
				'icon' => 'fa-skype',
				'preg' => '#'.static::BASE_URL.'.*(skype)#',
			),
			'slack' => array(
				'icon' => 'fa-slack',
				'preg' => '#'.static::BASE_URL.'.*(slack)#',
			),
			'linkedin' => array(
				'icon' => 'fa-linkedin',
				'preg' => '#'.static::BASE_URL.'.*(linkedin)#',
			),
			'snapshat' => array(
				'icon' => 'fa-snapshat',
				'preg' => '#'.static::BASE_URL.'.*(snapshat)#',
			),
			'soundcloud' => array(
				'icon' => 'fa-soundcloud',
				'preg' => '#'.static::BASE_URL.'.*(soundcloud)#',
			),
			'spotify' => array(
				'icon' => 'fa-spotify',
				'preg' => '#'.static::BASE_URL.'.*(spotify)#',
			),
			'trello' => array(
				'icon' => 'fa-trello',
				'preg' => '#'.static::BASE_URL.'.*(trello)#',
			),
			'tripadvisor' => array(
				'icon' => 'fa-tripadvisor',
				'preg' => '#'.static::BASE_URL.'.*(tripadvisor)#',
			),
			'tumblr' => array(
				'icon' => 'fa-tumblr',
				'preg' => '#'.static::BASE_URL.'.*(tumblr)#',
			),
			'twitter' => array(
				'icon' => 'fa-twitter',
				'preg' => '#'.static::BASE_URL.'.*(twitter)#',
			),
			'vimeo' => array(
				'icon' => 'fa-vimeo',
				'preg' => '#'.static::BASE_URL.'.*(vimeo)#',
			),
			'whatsapp' => array(
				'icon' => 'fa-whatsapp',
				'preg' => '#'.static::BASE_URL.'.*(whatsapp)#',
			),
			'youtube' => array(
				'icon' => 'fa-youtube',
				'preg' => '#'.static::BASE_URL.'.*(youtube)#',
			),
			'paypal' => array(
				'icon' => 'fa-paypal',
				'preg' => '#'.static::BASE_URL.'.*(paypal)#',
			),
			'pinterest' => array(
				'icon' => 'fa-pinterest',
				'preg' => '#'.static::BASE_URL.'.*(pinterest)#',
			),
			'flickr' => array(
				'icon' => 'fa-flickr',
				'preg' => '#'.static::BASE_URL.'.*(flickr)#',
			),
			'dropbox' => array(
				'icon' => 'fa-dropbox',
				'preg' => '#'.static::BASE_URL.'.*(dropbox)#',
			),
			'github' => array(
				'icon' => 'fa-github',
				'preg' => '#'.static::BASE_URL.'.*(github)#',
			),
			'vk' => array(
				'icon' => 'fa-vk',
				'preg' => '#'.static::BASE_URL.'.*(vk)#',
			),
			'unknown' => array(
				'icon' => static::DEFAULT_ICON,
				'preg' => '#'.static::BASE_URL.'.*(.+)#',
			),
		);
	}



}