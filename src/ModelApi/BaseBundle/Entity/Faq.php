<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
// use Doctrine\Common\Collections\ArrayCollection;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceFaq;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

use \Exception;
use \DateTime;

/**
 * Faq
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\FaqRepository")
 * @ORM\Table(
 *      name="`faq`",
 *		options={"comment":"Questions/Reponses"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_faq",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
class Faq implements CrudInterface {

	const DEFAULT_ICON = 'fa-question-circle';
	const ENTITY_SERVICE = serviceFaq::class;

	const SHORTCUT_CONTROLS = true;

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;
	use \ModelApi\CrudsBundle\Traits\Cruds;

	/**
	 * Id of entity
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @Annot\Translatable
	 * @ORM\Column(name="question", type="text", nullable=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $question;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @ORM\Column(name="response", type="text", nullable=true)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $response;

	/**
	 * @var string
	 * @ORM\Column(name="category", type="string", nullable=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $category;

	/**
	 * @var string
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=true}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $color;



	public function __construct() {
		$this->question = null;
		$this->response = null;
		$this->category = null;
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getId();
	}


	/**
	 * Get question
	 * @return string
	 */
	public function getQuestion() {
		return $this->question;
	}

	/**
	 * Set question
	 * @param string $question
	 * @return ModelFaq
	 */
	public function setQuestion($question) {
		$this->question = $question;
		return $this;
	}

	/**
	 * Get response
	 * @return string
	 */
	public function getResponse() {
		return $this->response;
	}

	/**
	 * Set response
	 * @param string $response
	 * @return ModelFaq
	 */
	public function setResponse($response) {
		$this->response = $response;
		return $this;
	}

	/**
	 * Get category
	 * @return string
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * Set category
	 * @param string $category
	 * @return ModelFaq
	 */
	public function setCategory($category) {
		$this->category = $category;
		return $this;
	}



}