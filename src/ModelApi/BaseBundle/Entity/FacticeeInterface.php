<?php
namespace ModelApi\BaseBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;
// use ModelApi\AnnotBundle\Annotation as Annot;
// use ModelApi\CrudsBundle\Annotation as CRUDS;
// use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\FacticeeInterface;
use ModelApi\BaseBundle\Service\serviceFacticee;
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;

use \Exception;
use \DateTime;

/**
 * Facticee
 *
 * @Annot\HasTranslatable()
 */
Interface FacticeeInterface {

	public function _getData();

	public function _resetData($data = null);

	public function isValid($exceptionIfInvalid = false);

	public function isServiceRegistred();

	public static function getFieldsRequireds();
	public static function getAllRequireds();

	public function __isset(string $name): bool;

	public function __get($name);

	public function __set($name, $value);

	public function __unset($name);

	public function isFacticee();

	public function getId();

	public function getBddid();

	public function getShortname();

	public function getClassname();

	public function getMicrotimeid();

	public function getRealEntity();

}