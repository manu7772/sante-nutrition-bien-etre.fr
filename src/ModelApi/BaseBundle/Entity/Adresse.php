<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceAdresse;
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// BinaryBundle
// use ModelApi\BinaryBundle\Entity\Thumbnail;
use ModelApi\BinaryBundle\Entity\Photo;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;

use \Exception;
use \DateTime;

/**
 * Adresse
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\AdresseRepository")
 * @ORM\Table(
 *      name="`adresse`",
 *		options={"comment":"Adresse"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_adresse",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
class Adresse extends Item {

	const DEFAULT_ICON = 'fa-address-card';
	const DEFAULT_OWNER_DIRECTORY = 'system/Adresses';
	const ENTITY_SERVICE = serviceAdresse::class;
	const DEFAULT_CATEGORY = 'public';


	/**
	 * @var array
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=true, "description"="Catégorie de l'adresse", "choices"="auto", "label"="Categorie"},
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=true, "description"="Catégorie de l'adresse", "choices"="auto", "label"="Categorie"},
	 * )
	 * @CRUDS\Show(role=true, order=99)
	 * @Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 * @Annot\Typitem(groups={"adresse_cat"}, description="Catégorie de l'adresse")
	 */
	protected $categorys;

	/**
	 * @var string
	 * Libellé de l'adresse
	 * @CRUDS\Create(
	 * 		type="TextType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=2,
	 * 		options={"by_reference"=false, "required"=true, "label"="Libellé", "description"="Libellé de l'adresse"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="TextType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=2,
	 * 		options={"by_reference"=false, "required"=true, "label"="Libellé", "description"="Libellé de l'adresse"},
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $inputname;

	/**
	 * @var string
	 * @ORM\Column(name="address", type="text", nullable=false, unique=false)
	 * Adresse complète
	 * @CRUDS\Create(
	 * 		type="SummernoteType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=9,
	 * 		options={"by_reference"=false, "required"=false, "label"="Adresse", "description"="Adresse comoplète. Utilisez les retours à la ligne et le texte enrichi pour une meilleure présentation."},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="SummernoteType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=9,
	 * 		options={"by_reference"=false, "required"=false, "label"="Adresse", "description"="Adresse comoplète. Utilisez les retours à la ligne et le texte enrichi pour une meilleure présentation."},
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $address;

	/**
	 * @var string
	 * @ORM\Column(name="elocation", type="string", nullable=true, unique=false)
	 */
	protected $elocation;

	/**
	 * @var string
	 * @ORM\Column(name="gmapref", type="string", nullable=true, unique=false)
	 */
	protected $gmapref;

	// /**
	//  * @CRUDS\Create(show=false,update=false)
	//  * @CRUDS\Update(show=false,update=false)
	//  */
	// protected $inputname;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto"}
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $photo;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $title;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $urls;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $entete;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $picone;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $color;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $menutitle;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $pageweb;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $tags;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $fulltext;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $resume;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $description;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $accroche;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $subtitle;




	public function __construct() {
		parent::__construct();
		$this->address = null;
		$this->elocation = null;
		$this->gmapref = null;
		$this->photo = null;
		return $this;
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getName();
	}

	/**
	 * Set title
	 * @param string $title = null
	 * @return self
	 */
	public function setTitle($title = null) {
		parent::setTitle($title);
		// $this->setInputname($this->title);
		return $this;
	}


	public function setAddress($address) {
		$address = serviceTools::getTextOrNullStripped($address);
		$this->address = $address;
		return $this;
	}

	public function getAddress() {
		return $this->address;
	}



	/**
	 * Get elocation
	 * @return string|null
	 */
	public function getElocation() {
		return $this->elocation;
	}

	/**
	 * Set elocation
	 * @param string $elocation = null
	 * @return Entreprise
	 */
	public function setElocation($elocation = null) {
		$this->elocation = $elocation;
		return $this;
	}

	/**
	 * Get gmapref
	 * @return string|null
	 */
	public function getGmapref() {
		return $this->gmapref;
	}

	/**
	 * Set gmapref
	 * @param string $gmapref = null
	 * @return Entreprise
	 */
	public function setGmapref($gmapref) {
		$this->gmapref = $gmapref;
		return $this;
	}


	/*************************************************************************************/
	/*** PHOTO
	/*************************************************************************************/

	/**
	 * Set URL as photo
	 * @param string $url
	 * @return Adresse
	 */
	public function setUrlAsPhoto($url) {
		$photo = new Photo();
		$photo->setUploadFile($url);
		$this->setPhoto($photo);
	}

	/**
	 * Set resource as photo
	 * @param string $resource
	 * @return Adresse
	 */
	public function setResourceAsPhoto(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$photo = new Photo();
			$photo->setUploadFile($resource);
			$this->setPhoto($photo);
		}
		return $this;
	}

	public function getResourceAsPhoto() {
		return null;
	}

	/**
	 * Set photo
	 * @param Photo $photo = null
	 * @return Adresse
	 */
	public function setPhoto(Photo $photo = null) {
		$this->photo = $photo;
		return $this;
	}

	/**
	 * Get photo
	 * @return Photo | null
	 */
	public function getPhoto() {
		return $this->photo;
	}




}