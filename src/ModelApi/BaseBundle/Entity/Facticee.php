<?php
namespace ModelApi\BaseBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;
// use ModelApi\AnnotBundle\Annotation as Annot;
// use ModelApi\CrudsBundle\Annotation as CRUDS;
// use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Inflector\Inflector;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\FacticeeInterface;
use ModelApi\BaseBundle\Entity\LaboClassMetadata;
use ModelApi\BaseBundle\Service\serviceFacticee;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceClasses;
// use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;

use Website\SiteBundle\Controller\DefaultController;

use \Exception;
use \DateTime;

/**
 * Facticee
 *
 * !!!@Annot\HasTranslatable()
 */
class Facticee implements FacticeeInterface {

	const DEFAULT_ICON = 'fa-smile-o';
	const ENTITY_SERVICE = serviceFacticee::class;
	const SHORTCUT_CONTROLS = true;

	use \ModelApi\BaseBundle\Traits\BaseEntity;
	// use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	// use \ModelApi\BaseBundle\Traits\DateUpdater;
	// use \ModelApi\CrudsBundle\Traits\Cruds;

	protected $data;
	protected $origin_data;
	protected $implements;
	protected $parentClasses;
	protected $classesAndImplements;

	protected $serviceFacticee;


	public function __construct($data = [], serviceFacticee $serviceFacticee) {
		$this->init_BaseEntity();
		$this->serviceFacticee = $serviceFacticee;
		$this->data = [];
		$this->origin_data = [];
		$this->_resetData($data, false);
		// echo('<pre>'); var_dump($this->data); echo('</pre>');
		$this->serviceFacticee->addFacticee($this);
		$this->isValid(true);
		$this->implements = class_implements($this->data['classname']);
		$this->parentClasses = serviceClasses::getParentClasses($this->data['classname'], true, true);
		$this->classesAndImplements = array_unique(array_merge($this->implements, $this->parentClasses));
		return $this;
	}

	public function __toString() {
		return (string)$this->getBddid();
	}

	public function __call($name, $arguments) {
		$striped = lcfirst(preg_replace('/^(get|is|has)/', '', $name));
		// echo('<div>- Call '.$name.' > '.$striped.'</div>');
		switch ($name) {
			case 'isNew':
				if(!array_key_exists('publication', $this->data)) return false;
				//echo('<div>Called method: '.$name.' with arguments '.json_encode($arguments).'</div>');
				$arguments[0] ??= 7;
				$arguments[1] ??= null;
				$pub = clone $this->data['publication'];
				if(is_integer($arguments[0])) $pub->modify('+'.$arguments[0].' days');
					else if(is_string($arguments[0])) $pub->modify($arguments[0]);
				return $this->getContextdate($arguments[1]) <= $pub;
				break;
			case 'isSoftdeleted':
				if(!array_key_exists($striped, $this->data)) return false;
				return serviceEntities::isSoftdeleted($this->data[$striped]);
				break;
			// case 'getId': return array_key_exists($striped, $this->data) ? $this->data[$striped] : null;
			// case 'getOwner': return array_key_exists($striped, $this->data) ? $this->data[$striped] : null;
			// case 'getOwnerdir': return array_key_exists($striped, $this->data) ? $this->data[$striped] : null;
			// case 'getParent': return array_key_exists($striped, $this->data) ? $this->data[$striped] : null;
			default:
				return array_key_exists($striped, $this->data) ? $this->data[$striped] : null;
				// if(array_key_exists($striped, $this->data)) return $this->data[$striped];
				// property not found
				// echo('<pre><h1>Data of '.$this->data['shortname'].' '.json_encode($this->data['name']).'</h1>'); var_dump($this->data); echo('</pre>');
				$trace = debug_backtrace();
				trigger_error('Méthode non-définie via __call() sur '.json_encode($this->data['shortname']).' : '.json_encode($name).' dans '.$trace[0]['file'].' à la ligne '.$trace[0]['line'], E_USER_NOTICE);
				break;
		}
	}

	public function __isset(string $property): bool {
		return array_key_exists($property, $this->data);
	}

	/**
	 * @see https://www.php.net/manual/fr/language.oop5.overloading.php#object.get
	 */
	public function __get($property) {
		// echo('<div>Get property: '.$property.'</div>');
		switch ($property) {
			case 'childs':
				return $this->getChilds();
				break;
			default:
				if(array_key_exists($property, $this->data)) return $this->data[$property];
				break;
		}
		$trace = debug_backtrace();
		trigger_error('Propriété non-définie via __get() sur '.json_encode($this->data['shortname']).' : '.json_encode($property).' dans '.$trace[0]['file'].' à la ligne '.$trace[0]['line'], E_USER_NOTICE);
		return null;
	}

	public function __set($property, $value) {
		$this->data[$property] = $value;
	}

	public function __unset($property) {
		unset($this->data[$property]);
	}

	public function isInstanceOf($classes) {
		if(is_string($classes)) $classes = [$classes];
		return count(array_intersect((array)$classes, $this->classesAndImplements)) > 0;
	}

	public function _getData() {
		return $this->data;
	}

	public function _getOrigin_data() {
		return $this->origin_data;
	}

	public function _resetData($data = null, $valid = true) {
		if(empty($data)) {
			if(empty($this->origin_data)) throw new Exception("Error Facticee: data can not be empty!", 1);
			$data = $this->origin_data;
		} else {
			$this->data = $data;
			if(empty($this->origin_data)) $this->origin_data = $this->data;
		}
		$this->data = $this->_getCompiledData($this->data);
		if($valid) $this->isValid(true);
		return $this;
	}

	public function _updateData($data, $replace = true, $valid = true) {
		foreach ($data as $field => $value) {
			// if($replace || !array_key_exists($field, $this->data)) $this->data[$field] = $value;
			if($replace || !array_key_exists($field, $this->data)) {
				switch (gettype($value)) {
					case 'array':
						if(empty($value)) break;
						if(array_key_exists('id', $value)) $this->data[$field] = $value;
							else $this->data[$field][] = $value;
						break;
					case 'object':
						foreach ($value as $val) if(!empty($val) && !$this->data[$field]->contains($val)) $this->data[$field]->add($val);
						break;
					default:
						$this->data[$field] = $value;
						break;
				}
			}
		}
		if(empty($this->origin_data)) $this->origin_data = $this->data;
		$this->data = $this->_getCompiledData($this->data);
		if($valid) $this->isValid(true);
		return $this;
	}

	protected function _getCompiledData($data) {
		if(empty($data)) throw new Exception("Error on compile Facticee: data can not be empty!", 1);
		foreach ($data as $field => $values) {
			if(is_array($values)) {
				$first = reset($values);
				if(array_key_exists('id', $values)) {
					$data[$field] = $this->serviceFacticee->getFacticee($values);
				} else if(is_array($first) && array_key_exists('id', $first)) {
					$data[$field] = new ArrayCollection();
					foreach($values as $val) $data[$field]->add($this->serviceFacticee->getFacticee($val));
				} else {
					$method = Inflector::camelize('set_'.$field);
					if(method_exists($this, $method)) $this->$method($values);
				}
			} else {
				$method = Inflector::camelize('set_'.$field);
				if(method_exists($this, $method)) $this->$method($values);
			}
		}
		$data['currentParent'] ??= null;
		return $data;
	}

	public function isValid($exceptionIfInvalid = false) {
		$valid = $this->isServiceRegistred() ? true : 'not registered';
		foreach (static::getAllRequireds() as $field) {
			if(!array_key_exists($field, $this->data)) $valid = $field.' is missing';
		}
		if(is_string($valid) && $exceptionIfInvalid) throw new Exception("Error Facticee: data is not valid, ".$valid, 1);
		return !is_string($valid);
	}

	public function isServiceRegistred() {
		return $this->serviceFacticee->hasFacticee($this);
	}

	public static function getFieldsRequireds() {
		return [
			'id',
			'shortname',
			'microtimeid',
		];
	}

	public static function getAllRequireds() {
		return array_unique(array_merge(static::getFieldsRequireds(), [
			'bddid',
			'classname',
		]));
	}

	public function isFacticee() {
		return true;
	}

	public function getId() {
		return $this->data['id'];
	}

	public function getBddid() {
		return $this->data['bddid'];
	}

	public function getShortname($lower = false) {
		return $lower ? strtolower($this->data['shortname']) : $this->data['shortname'];
	}

	public function getClassname() {
		return $this->data['classname'];
	}

	public function getMicrotimeid() {
		return $this->data['microtimeid'];
	}

	public function getRealEntity() {
		return $this->serviceFacticee->getRealEntity($this);
	}

	public function getPathname($addHimself = false) {
		if(!array_key_exists('pathname', $this->data)) return null;
		if($addHimself) return empty($this->pathname) ? $this->data['name'] : $this->data['pathname'].DIRECTORY_SEPARATOR.$this->data['name'];
		return $this->data['pathname'];
	}

	public function getPathslug($addHimself = false) {
		if(!array_key_exists('pathslug', $this->data)) return null;
		if($addHimself) return empty($this->pathslug) ? $this->data['slug'] : $this->data['pathslug'].DIRECTORY_SEPARATOR.$this->data['slug'];
		return $this->data['pathslug'];
	}

	public function hasDirs() {
		if(!array_key_exists('driveDir', $this->data) || !array_key_exists('systemDir', $this->data)) return false;
		return !empty($this->data['driveDir']) || !empty($this->data['systemDir']);
	}

	public function getSolidchilds() {
		if(!$this->isInstanceOf(Basedirectory::class)) return new ArrayCollection();
		if(empty($childs = $this->getChilds())) return new ArrayCollection();
		return $childs->filter(function($item) {
			if(empty($item->getParent())) return false;
			return $item->getParent()->getId() === $this->getId();
		});
	}

	public function getChilds() {
		$this->data['childs'] = is_array($this->data['childs']) ?
			array_filter($this->data['childs'], function($child) { return $child instanceOf static; }):
			$this->data['childs']->filter(function($child) { return $child instanceOf static; });
		foreach ($this->data['childs'] as $child) {
			$child->setCurrentParent($this);
		}
		return $this->data['childs'];
	}

	public function setCurrentParent($currentParent = null) {
		$this->data['currentParent'] = $currentParent;
		return $this;
	}

	/**
	 * Get path slug
	 * @param string $prefix = 'cat'
	 * @param integer $limit = 3
	 * @return array <string>
	 */
	public function getCurrentPathslugAsParams($prefix = null, $limit = 5) {
		$prefix ??= DefaultController::CAT_NAME;
		$parentslugs = [];
		$parent = $this;
		$parentslugs[] = $parent->getSlug();
		while ($limit-- > 0) {
			$parent = !empty($parent) ? $parent->getCurrentParent() : null;
			if(!empty($parent)) {
				if(!($parent->isInstanceOf(Basedirectory::class)) || in_array($parent->getSlug(), $parentslugs) || $parent->isRootmenu() || $parent->getLvl() < 2) break;
				$parentslugs[] = $parent->getSlug();
			}
		}
		$key = count($parentslugs);
		$pathslug = [];
		foreach ($parentslugs as $slug) $pathslug[$prefix.$key--] = $slug;
		return $pathslug;
	}

	public function isRootmenu() {
		if(!$this->isInstanceOf(Menu::class)) return false;
		if(empty($this->getCurrentParent())) return false;
		return !$this->getCurrentParent()->isInstanceOf(Menu::class);
	}

	public function isMenuable() {
		if($this->isInstanceOf([Pageweb::class, Menu::class])) return true;
		if(!empty($this->data['pageweb']) && $this->data["pageweb"]->isInstanceOf(Pageweb::class)) return true;
		return false;
	}

}