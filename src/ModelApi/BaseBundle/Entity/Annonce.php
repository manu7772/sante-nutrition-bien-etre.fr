<?php
namespace ModelApi\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Hateoas\Configuration\Annotation as Hateoas;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;
// BaseBundle
use ModelApi\BaseBundle\Entity\Bloger;
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceAnnonce;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// use ModelApi\BinaryBundle\Entity\Thumbnail;
use ModelApi\BinaryBundle\Entity\Photo;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// UserwebBundle
use ModelApi\UserwebBundle\Entity\User;
use ModelApi\UserwebBundle\Entity\Tier;
use ModelApi\UserwebBundle\Entity\Entreprise;

use \Exception;
use \DateTime;

/**
 * Annonce
 *
 * @ORM\Entity(repositoryClass="ModelApi\BaseBundle\Repository\AnnonceRepository")
 * @ORM\Table(
 *      name="`annonce`",
 *		options={"comment":"Annonce"},
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_annonce",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @Annot\HasTranslatable()
 * @CRUDS\Actions(show=true, create="ROLE_EDITOR", update="ROLE_EDITOR", delete="ROLE_EDITOR")
 */
class Annonce extends Bloger {

	const DEFAULT_ICON = 'fa-newspaper-o';
	const DEFAULT_OWNER_DIRECTORY = '/Public';
	const ENTITY_SERVICE = serviceAnnonce::class;

	/**
	 * Name that was input by user
	 * @var string
	 * @ORM\Column(name="inputname", type="string", nullable=false, unique=false)
	 * @CRUDS\Creates(
	 * 		@CRUDS\Create(
	 * 			show=true,
	 * 			update=true,
	 * 			order=1,
	 * 			options={"required"=true, "label"="Identifiant", "by_reference"=false, "attr"={"placeholder"="Titre de l'annonce"}, "description"="Nom informatique de l'annonce. N'est pas utilisé sur le site, <strong class='text-danger'>évitez de le modifier, car cela modifie l'URL d'accès, et cela peut donc <u>briser</u> les liens externes et certains liens internes.</strong>."},
	 * 		),
	 * 		@CRUDS\Create(
	 * 			show=true,
	 * 			update=true,
	 * 			order=1,
	 * 			options={"required"=true, "label"="Identifiant", "by_reference"=false, "attr"={"placeholder"="Titre de l'annonce"}, "description"="Nom informatique de l'annonce. N'est pas utilisé sur le site, <strong class='text-danger'>évitez de le modifier, car cela modifie l'URL d'accès, et cela peut donc <u>briser</u> les liens externes et certains liens internes.</strong>."},
	 * 			contexts={"public"},
	 * 		),
	 * 	)
	 * @CRUDS\Updates(
	 * 		@CRUDS\Update(
	 * 			show=true,
	 * 			update=true,
	 * 			order=1,
	 * 			options={"required"=true, "label"="Identifiant", "by_reference"=false, "attr"={"placeholder"="Titre de l'annonce"}, "description"="Nom informatique de l'annonce. N'est pas utilisé sur le site, <strong class='text-danger'>évitez de le modifier, car cela modifie l'URL d'accès, et cela peut donc <u>briser</u> les liens externes et certains liens internes.</strong>."},
	 * 		),
	 * 		@CRUDS\Update(
	 * 			show=true,
	 * 			update=true,
	 * 			order=1,
	 * 			options={"required"=true, "label"="Identifiant", "by_reference"=false, "attr"={"placeholder"="Titre de l'annonce"}, "description"="Nom informatique de l'annonce. N'est pas utilisé sur le site, <strong class='text-danger'>évitez de le modifier, car cela modifie l'URL d'accès, et cela peut donc <u>briser</u> les liens externes et certains liens internes.</strong>."},
	 * 			contexts={"public"},
	 * 		),
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $inputname;

	// /**
	//  * @CRUDS\Create(
	//  * 		type="EntityType",
	//  * 		show="ROLE_EDITOR",
	//  * 		update="ROLE_EDITOR",
	//  * 		order=1,
	//  * 		options={"required"=true, "multiple"=false, "description"="Auteur de l'annonce", "query_builder"="auto"}
	//  * )
	//  * @CRUDS\Update(
	//  * 		type="EntityType",
	//  * 		show="ROLE_EDITOR",
	//  * 		update="ROLE_EDITOR",
	//  * 		order=1,
	//  * 		options={"required"=true, "multiple"=false, "description"="Auteur de l'annonce", "query_builder"="auto"}
	//  * )
	//  * @CRUDS\Show(role=true, order=1)
	//  */
	// protected $owner;

	/**
	 * @var integer
	 * @ORM\ManyToOne(targetEntity="ModelApi\BaseBundle\Entity\Typitem")
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=false, "description"="Type d'affichage de l'annonce", "choices"="auto"}
	 * )
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show=false,
	 * 		update=false,
	 * 		order=99,
	 * 		options={"required"=true, "multiple"=false, "description"="Type d'affichage de l'annonce", "choices"="auto"}
	 * )
	 * @CRUDS\Show(role=true, order=99)
	 * @Annot\Typitem(groups={"annonce_affichage"}, description="Types d'affichage des annonces")
	 * @Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 */
	protected $affichage;
	protected $affichageChoices;

	/**
	 * @var array
	 * @CRUDS\Creates(
	 * 		@CRUDS\Create(
	 * 			type="ChoiceType",
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "multiple"=true, "description"="Catégories de l'annonce", "choices"="auto"},
	 * 		),
	 * 		@CRUDS\Create(
	 * 			type="ChoiceType",
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "label"="Type", "multiple"=true, "expanded"=true, "description"="Type d'annonce", "choices"="auto"},
	 * 			contexts={"public"},
	 * 		),
	 * )
	 * @CRUDS\Updates(
	 * 		@CRUDS\Update(
	 * 			type="ChoiceType",
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "multiple"=true, "description"="Catégories de l'annonce", "choices"="auto"},
	 * 		),
	 * 		@CRUDS\Update(
	 * 			type="ChoiceType",
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "label"="Type", "multiple"=true, "expanded"=true, "description"="Type d'annonce", "choices"="auto"},
	 * 			contexts={"public"},
	 * 		),
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 * @Annot\Typitem(groups={"annonce_cat"}, description="Catégories des annonces")
	 * @Annot\AddModelTransformer(method="annot::transformEntitiesAsChoices")
	 */
	protected $categorys;

	/**
	 * @var DateTime
	 * @ORM\Column(name="geozone", type="string", length=5, nullable=true)
	 * @CRUDS\Creates(
	 * 		@CRUDS\Create(
	 * 			show=false,
	 * 			update=false,
	 * 			order=100,
	 * 			options={"required"=true, "description"="Zone de l'annonce"},
	 * 		),
	 * 		@CRUDS\Create(
	 * 			show=false,
	 * 			update=false,
	 * 			order=100,
	 * 			options={"required"=true, "label"="Code postal", "description"="Code postal du lieu de la mission. Veuillez indiquer un nombre à 5 chiffres svp."},
	 * 			contexts={"public"},
	 * 		),
	 * )
	 * @CRUDS\Updates(
	 * 		@CRUDS\Update(
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "description"="Zone de l'annonce"},
	 * 		),
	 * 		@CRUDS\Update(
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "label"="Code postal", "description"="Code postal du lieu de la mission. Veuillez indiquer un nombre à 5 chiffres svp."},
	 * 			contexts={"public"},
	 * 		),
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $geozone;

	/**
	 * @var DateTime
	 * @ORM\Column(name="publication", type="datetime", nullable=false)
	 * @CRUDS\Creates(
	 * 		@CRUDS\Create(
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "description"="Date/heure de publication de l'annonce"},
	 * 		),
	 * 		@CRUDS\Create(
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "description"="Date/heure de publication de l'annonce. Si vous mettez une date future, l'annonce n'apparaîtra qu'à partir de cette date/heure. Par défaut, la validité d'une annonce est valable 3 mois, mais vous pouvez à tout moment la prolonger."},
	 * 			contexts={"public"},
	 * 		),
	 * )
	 * @CRUDS\Updates(
	 * 		@CRUDS\Update(
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "description"="Date/heure de publication de l'annonce"},
	 * 		),
	 * 		@CRUDS\Update(
	 * 			show="ROLE_USER",
	 * 			update="ROLE_USER",
	 * 			order=100,
	 * 			options={"required"=true, "description"="Date/heure de publication de l'annonce. Si vous mettez une date future, l'annonce n'apparaîtra qu'à partir de cette date/heure. Par défaut, la validité d'une annonce est valable 3 mois, mais vous pouvez à tout moment la prolonger."},
	 * 			contexts={"public"},
	 * 		),
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $publication;

	/**
	 * @var DateTime
	 * @ORM\Column(name="peremption", type="datetime", nullable=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_SUPER_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "description"="Date/heure de fin de publication de l'annonce. Si vous ne pouvez pas la modifier, modifiez la date de publication ci-dessus : elle se réajustera à + 3 mois après la nouvelle date de publication lors de l'enregistrement."}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_ADMIN",
	 * 		order=100,
	 * 		options={"required"=true, "description"="Date/heure de fin de publication de l'annonce. Si vous ne pouvez pas la modifier, modifiez la date de publication ci-dessus : elle se réajustera à + 3 mois après la nouvelle date de publication lors de l'enregistrement."}
	 * )
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $peremption;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto", "description"="Vous pouvez joindre une photo ou une illustration à votre annonce."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		order=110,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto", "description"="Vous pouvez joindre une photo ou une illustration à votre annonce."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Show(role=true, order=110)
	 */
	protected $photo;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $subtitle;

	/**
	 * @CRUDS\Create(show=false,update=false)
	 * @CRUDS\Update(show=false,update=false)
	 */
	protected $accroche;

	/**
	 */
	protected $description;

	/**
	 * @var string
	 * Texte de l'élément, version courte de fulltext
	 * Texte de 5 ou 6 lignes max
	 * @ORM\Column(name="resume", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=8, options={"by_reference"=false, "required"=false, "description"="Texte résumé (optionnel)"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=8, options={"by_reference"=false, "required"=false, "description"="Texte résumé (optionnel)"})
	 * @CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $resume;

	/**
	 * @var string
	 * Texte complet de l'élément - illimité
	 * @ORM\Column(name="full_text", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		type="SummernoteType",
	 * 		order=9,
	 * 		options={"by_reference"=false, "required"=true, "description"="Grand texte de l'annonce."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_USER",
	 * 		update="ROLE_USER",
	 * 		type="SummernoteType",
	 * 		order=9,
	 * 		options={"by_reference"=false, "required"=true, "description"="Grand texte de l'annonce."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * )
	 * @CRUDS\Show(role=true)
	 * @Annot\Translatable
	 */
	protected $fulltext;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true, "description"="Ajoutez des <strong>tags</strong> pour améliorer le réféncement !"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=400,
	 * 		options={"required"=false, "multiple"=true, "description"="Ajoutez des <strong>tags</strong> pour améliorer le réféncement !"},
	 * 	)
	 * @CRUDS\Show(role=true, order=100)
	 */
	protected $tags;



	public function __construct() {
		parent::__construct();
		$this->setPublication();
		$this->geozone = null;
		$this->affichage = null;
		$this->setAffichageChoices = [];
		$this->photo = null;
		return $this;
	}

	public function isCategorysValid() {
		return !$this->categorys->isEmpty();
	}

	/*************************************************************************************/
	/*** GEOZONE
	/*************************************************************************************/

	/**
	 * Is geozone valid
	 * @return boolean
	 */
	public function isGeozoneValid() {
		return empty($this->geozone) || preg_match('/^\\d{5}$/', $this->geozone);
	}

	/**
	 * Get geozone
	 * @return string
	 */
	public function getGeozone() {
		return $this->geozone;
	}

	/**
	 * Set geozone
	 * @param string $geozone
	 * @return Item
	 */
	public function setGeozone($geozone) {
		$this->geozone = $geozone;
		return $this;
	}

	/*************************************************************************************/
	/*** AFFICHAGE
	/*************************************************************************************/

	/**
	 * Get affichage
	 * @return Typitem
	 */
	public function getAffichage() {
		return $this->affichage;
	}

	/**
	 * Set affichage
	 * @param Typitem $affichage
	 * @return Item
	 */
	public function setAffichage(Typitem $affichage) {
		$this->affichage = $affichage;
		return $this;
	}

	/**
	 * Get affichage Choices
	 * @return array
	 */
	public function getAffichageChoices() {
		return $this->affichageChoices;
	}

	/**
	 * Set affichage Choices
	 * @param array $affichageChoices
	 * @return Item
	 */
	public function setAffichageChoices($affichageChoices) {
		$this->affichageChoices = $affichageChoices;
		return $this;
	}



	/*************************************************************************************/
	/*** PROGRESS
	/*************************************************************************************/

	public function isNewByRatio($ratio = 10, $contextdate = null) {
		if(!$this->isPublished($contextdate)) return false;
		return $this->progressRatio(100, false, $contextdate) <= $ratio;
	}

	public function isNew($days = 7, $contextdate = null) {
		if(!$this->isPublished($contextdate)) return false;
		$pub = clone $this->publication;
		if(is_integer($days)) $pub->modify('+'.$days.' days');
			else if(is_string($days)) $pub->modify($days);
		return $this->getContextdate($contextdate) <= $pub;
	}

	public function progressRatio($base = 100, $asInteger = true, $contextdate = null) {
		$TS_context = $this->getContextdate($contextdate)->getTimestamp();
		$TS_publi = $this->publication->getTimestamp();
		$TS_perem = $this->getPeremption()->getTimestamp();
		if($TS_context <= $TS_publi) {
			$progressRatio = floatval(0);
		} else if($TS_context >= $TS_perem) {
			$progressRatio = floatval($base);
		} else {
			$progressRatio = ($TS_context - $TS_publi) / ($TS_perem - $TS_publi) * $base;
		}
		return true === $asInteger ? intval(round($progressRatio)) : round($progressRatio, (is_integer($asInteger) ? $asInteger : 2));
	}

	public function progressColor($prefix = '', $contextdate = null, $colors = []) {
		if(empty($colors)) $colors = [0 => 'info', 20 => 'success', 70 => 'warning', 100 => 'danger'];
		$progressRatio = $this->progressRatio(100, false, $contextdate);
		$progressColor = reset($colors);
		foreach ($colors as $value => $color) {
			if($progressRatio >= $value) $progressColor = $color;
		}
		return $prefix.$progressColor;
	}

	public function isPublished($contextdate = null) {
		if(!$this->isActive() || $this->isPeremption($contextdate)) return false;
		return $this->publication < $this->getContextdate($contextdate);
	}

	public function isPeremption($contextdate = null) {
		return $this->getPeremption() <= $this->getContextdate($contextdate);
	}

	public function isSoonPeremption($before = 72, $contextdate = null) {
		$peremption = clone $this->getPeremption();
		if(is_integer($before)) $peremption->modify('-'.$before.' hours');
			else if(is_string($before)) $peremption->modify($before);
		return $peremption <= $this->getContextdate($contextdate);
	}


	/*************************************************************************************/
	/*** PUBLICATION
	/*************************************************************************************/

	public function setPublication(DateTime $publication = null) {
		if(!($publication instanceOf DateTime) && !($this->publication instanceOf DateTime)) {
			$this->publication = new DateTime();
		} else {
			$this->publication = $publication;
		}
		$this->setPeremption();
		$this->blockPeremption = true;
		return $this;
	}

	public function getPublication() {
		return $this->publication;
	}


	/*************************************************************************************/
	/*** PEREMPTION
	/*************************************************************************************/

	public function setPeremption(DateTime $peremption = null) {
		if(!isset($this->blockPeremption) || true !== $this->blockPeremption) {
			if($peremption instanceOf DateTime) {
				$this->peremption = $peremption;
			} else {
				$publication = $this->getPublication();
				$this->peremption = clone $publication;
				$this->peremption->modify('+3 months + 1 day')->setTime(4,0,0);
			}
		}
		unset($this->blockPeremption);
		return $this;
	}

	public function getPeremption() {
		if(!($this->peremption instanceOf DateTime) || $this->peremption < $this->publication) $this->setPeremption();
		return $this->peremption;
	}


	/*************************************************************************************/
	/*** PHOTO
	/*************************************************************************************/

	/**
	 * Set URL as photo
	 * @param string $url
	 * @return Event
	 */
	public function setUrlAsPhoto($url) {
		$photo = new Photo();
		$photo->setUploadFile($url);
		$this->setPhoto($photo);
	}

	/**
	 * Set resource as photo
	 * @param string $resource
	 * @return Event
	 */
	public function setResourceAsPhoto(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$photo = new Photo();
			$photo->setUploadFile($resource);
			$this->setPhoto($photo);
		}
		return $this;
	}

	public function getResourceAsPhoto() {
		return null;
	}

	/**
	 * Set photo
	 * @param Photo $photo = null
	 * @return Event
	 */
	public function setPhoto(Photo $photo = null) {
		$this->photo = $photo;
		return $this;
	}

	/**
	 * Get photo
	 * @return Photo | null
	 */
	public function getPhoto() {
		return $this->photo;
	}




}