<?php
namespace ModelApi\BaseBundle\Repository;

// FileBundle
use ModelApi\FileBundle\Repository\ItemRepository;

use \LogicException;
use \ReflectionClass;
use \DateTime;

/**
 * TelephonRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TelephonRepository extends ItemRepository {

	const ELEMENT = 'telephon'; // entity



}