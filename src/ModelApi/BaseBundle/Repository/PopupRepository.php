<?php
namespace ModelApi\BaseBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\DependencyInjection\ContainerInterface;

// use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcherInterface;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Representation\Pagination;
use ModelApi\BaseBundle\Repository\BasentityRepository;
use ModelApi\BaseBundle\Entity\Popup;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;

use \LogicException;
use \ReflectionClass;
use \DateTime;

/**
 * PopupRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PopupRepository extends BasentityRepository {

	const ELEMENT = 'popup'; // entity


	public function getDateds($date = 'now') {
		if(!($date instanceOf DateTime)) $date = new DateTime($date);
		$qb = $this->createQueryBuilder(static::ELEMENT);
		$this->qb_removeNotActive($qb, static::ELEMENT);
		$qb->andWhere(static::ELEMENT.'.truestart < :date')
			->andWhere(static::ELEMENT.'.trueend > :date')
			->setParameter('date', $date)
			;
		$qb->orderBy(static::ELEMENT.'.truestart', 'DESC');
		return $qb->getQuery()->getResult();
	}



	/********************************************************************************************************************/
	/*** QUERY BUILDERS
	/********************************************************************************************************************/

	public function qb_findAll(QueryBuilder $qb = null, $entity = null, Tier $tier = null) {
		if(!($qb instanceOf QueryBuilder)) $qb = $this->createQueryBuilder(static::ELEMENT);
		// $qb->orderBy(static::ELEMENT.'.name'); // ordre alpha
		return $qb;	
	}

}