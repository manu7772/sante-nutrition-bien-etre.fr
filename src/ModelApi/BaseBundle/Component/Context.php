<?php
namespace ModelApi\BaseBundle\Component;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Doctrine\ORM\Mapping as ORM;
// use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Interfaces\extendedBundleInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Service\serviceItem;
// UserBundle
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Service\serviceEntreprise;
use ModelApi\UserBundle\Service\serviceGrants;

use ModelApi\InternationalBundle\Service\serviceLanguage;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
use ModelApi\InternationalBundle\Service\serviceTimezone;

use \ReflectionClass;
use \Exception;
use \DateTime;
// use \ArrayAccess;


class Context {

	const DEFAULT_VERSION = null;
	const REGISTER_ALL = false;

	protected $data;
	protected $complete;
	protected $errors;
	protected $name;
	protected $name_locked;
	protected $parameterBag;
	protected $bundle;
	protected $bundle_name;
	protected $user;
	protected $environment;
	protected $entreprise;
	protected $date;
	protected $locale;
	protected $choicesEnvironments;
	protected $countersEnvironments;
	protected $VERSION;

	public function __construct($params = null, $doComplete = true) {
		$this->data = [];
		$this->complete = null;
		$this->errors = [];
		$this->name = null;
		$this->name_locked = false;
		$this->bundle = null;
		$this->request = null;
		$this->parameterBag = null;
		$this->bundle_name = null;
		$this->user = null;
		$this->environment = null;
		$this->entreprise = null;
		$this->date = serviceContext::DEFAULT_DATE;
		$this->locale = null;
		$this->VERSION = null;
		$this->choicesEnvironments = [];
		$this->countersEnvironments = [];
		$this->getChoicesDates();
		// name (auto)
		$this->setName(serviceTools::geUniquid($this));
		// if params...
		if(!empty($params)) {
			$this->initialize($params, $doComplete);
		}
		return $this;
	}

	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString() {
		return serviceClasses::getShortname(static::class);
	}


	/***********************************/
	/*** INITIALIZE
	/***********************************/

	/**
	 * Initialize Context with $params
	 * @param mixed $params
	 * @param boolean $doComplete = true
	 * @return boolean (true if complete)
	 */
	public function initialize($params, $doComplete = true) {
		// echo('<p>Params: '.(gettype($params) === 'object' ? get_class($params) : gettype($params)).' => iterable: '.json_encode(is_iterable($params)).' => array: '.json_encode(is_array($params)).'</p>');
		if($params instanceOf ContainerInterface) {
			// use ContainerInterface
			return $this->initialize([
				'container' => $params,
				// 'parameterBag' => $params->getParameterBag(),
				// 'environment' => $params->get(serviceContext::class)->getDefaultEnvironment(),
				// 'user' => $params->get(serviceContext::class)->getDefaultUser(),
				// 'entreprise' => $params->get(serviceContext::class)->getDefaultEntreprise(),
				'bundle' => $params->get(serviceKernel::class)->getCurrentBundle(),
				'request' => $params->get(serviceKernel::class)->getRequest(),
			], $doComplete);
		} else if(is_iterable($params)) {
			if(!isset($params['container']) || !($params['container'] instanceOf ContainerInterface)) throw new Exception("Error ".__METHOD__."(): data must contain ContainerInterface (named \"container\")!", 1);
			// params
			foreach ($params as $name => $value) {
				$setter = Inflector::camelize('set_'.$name);
				if(method_exists($this, $setter)) $this->$setter($value);
			}
			$this->VERSION = $this->parameter('version', static::DEFAULT_VERSION);
		} else {
			throw new Exception("Error ".__METHOD__."(): data must contain ContainerInterface or iterable element!", 1);
		}
		return $doComplete ? $this->completeContext() : $this->isComplete();
	}

	/**
	 * Complete missing data of context
	 * @return boolean (true if complete succeded)
	 */
	public function completeContext() {
		$this->getUser();
		if(!$this->isComplete()) {
			$serviceContext = $this->container->get(serviceContext::class);
			if(empty($this->getParameterBag())) $this->setParameterBag($this->container->getParameterBag());
			if(empty($this->getEnvironment())) {
				$defaultEnvironment = $serviceContext->getDefaultEnvironment();
				if(!empty($defaultEnvironment)) $this->setEnvironment($defaultEnvironment);
			}
			// if(empty($this->getUser())) $this->setUser($this->container->get(serviceUser::class)->getCurrentUserOrNull());
			if(empty($this->getEntreprise())) $this->setEntreprise($serviceContext->getDefaultEntreprise());
			if(empty($this->getBundle())) $this->setBundle($this->container->get(serviceKernel::class)->getCurrentBundle());
			if(empty($this->getRequest())) $this->setRequest($this->container->get(serviceKernel::class)->getRequest());
			// Locale & Language
			if(empty($this->getLocale())) $this->setLocale($this->getDefaultLocale());
			// Do actions in context
			if($this->isComplete() && $this === $serviceContext->getCurrentContext()) $serviceContext->doActionsWhenInitialized();
		}
		return $this->isComplete();
	}

	protected function setContainer(ContainerInterface $container) {
		$this->container = $container;
		return $this;
	}

	protected function getContainer() {
		return $this->container;
	}


	/***********************************/
	/*** VERSION
	/***********************************/

	public function getVersion() {
		return $this->VERSION;
	}

	public function version_egale_a($version) { return version_compare($this->VERSION, $version, '='); }
	public function version_eq($version) { return $this->version_egale_a($version); }

	public function version_differente_de($version) { return version_compare($this->VERSION, $version, '!='); }
	public function version_ne($version) { return $this->version_differente_de($version); }

	public function version_superieure_a($version) { return version_compare($this->VERSION, $version, '>'); }
	public function version_gt($version) { return $this->version_superieure_a($version); }

	public function version_superieure_ou_egale_a($version) { return version_compare($this->VERSION, $version, '>='); }
	public function version_ge($version) { return $this->version_superieure_ou_egale_a($version); }

	public function version_inferieure_a($version) { return version_compare($this->VERSION, $version, '<'); }
	public function version_lt($version) { return $this->version_inferieure_a($version); }

	public function version_inferieure_ou_egale_a($version) { return version_compare($this->VERSION, $version, '<='); }
	public function version_le($version) { return $this->version_inferieure_ou_egale_a($version); }



	/***********************************/
	/*** DATA (for SESSION)
	/***********************************/

	protected function getString($item) {
		if(is_string($item)) return $item;
		if(is_bool($item)) return $item ? 'true' : 'false';
		if(is_numeric($item)) return (string)$item;
		if(is_null($item)) return 'null';
		if(is_object($item)) {
			if($item instanceOf DateTime) return $item->format(serviceContext::CONTEXT_FORMAT_DATE);
			if(method_exists($item, 'getBddid')) return $item->getBddid();
			if(method_exists($item, '__toString')) return serviceClasses::getShortname($item).'@'.$item->__toString();
			return serviceClasses::getShortname($item);
		}
		// LAST CHANCE
		return (string)$item;
	}

	// public function getData() {
	// 	$this->data = [
	// 		'id' => $this->getId(),
	// 		'name' => $this->getName(),
	// 	];
	// 	foreach ($this->getId(true) as $attr => $item) $this->data[$attr] = $item;
	// 	return $this->data;
	// }

	public function getDataForSession($exceptionIfNotComplete = true) {
		if($exceptionIfNotComplete && !$this->isComplete()) throw new Exception("Error ".__METHOD__."(): can not generate data for session because ".json_encode($this->getErrors()), 1);
		$environment = $this->getEnvironment();
		$entreprise = $this->getEntreprise();
		return [
			serviceContext::CONTEXT_ENVIRONMENT_TOKEN_NAME => $environment instanceOf Tier ? $environment->getBddid() : null,
			serviceContext::CONTEXT_ENTREPRISE_TOKEN_NAME => $entreprise instanceOf Tier ? $entreprise->getBddid() : null,
			serviceContext::CONTEXT_LOCALE_TOKEN_NAME => $this->getLocale(),
			serviceContext::CONTEXT_DATE_TOKEN_NAME => $this->getDate(false),
		];
	}

	public function getSessionDataNames() {
		$data = $this->getDataForSession(false);
		return array_keys($data);
	}

	public function putSessionData($data) {
		foreach ($data as $name => $value) {
			switch ($name) {
				case serviceContext::CONTEXT_ENVIRONMENT_TOKEN_NAME:
					if(!$this->isEnvironmentBddid($value) && serviceEntities::isValidBddid($value, false)) {
						$serviceEntities ??= $this->container->get(serviceEntities::class);
						$repositoryMethod = $serviceEntities->hasBddidShortname($value, serviceClasses::getShortname(Tier::class)) ? 'findForSession' : 'find';
						$env = $serviceEntities->findByBddid($value, $repositoryMethod);
						//if(!$env->hasAssociateUser($this->user)) throw new Exception("Error ".__METHOD__."(): this user ".$this->user->getUsername()." can not manage this ".$env->getShortname()." ".$env->getName()."!", 1);
						$this->setEnvironment($env);
					}
					break;
				case serviceContext::CONTEXT_ENTREPRISE_TOKEN_NAME:
					if(!$this->isEntrepriseBddid($value) && serviceEntities::isValidBddid($value, false)) {
						$serviceEntities ??= $this->container->get(serviceEntities::class);
						$repositoryMethod = $serviceEntities->hasBddidShortname($value, serviceClasses::getShortname(Tier::class)) ? 'findForSession' : 'find';
						$this->setEntreprise($serviceEntities->findByBddid($value, $repositoryMethod));
					}
					break;
				case serviceContext::CONTEXT_LOCALE_TOKEN_NAME:
					if(null != $value) $this->setLocale($value);
					break;
				case serviceContext::CONTEXT_DATE_TOKEN_NAME:
					if(null != $value) $this->setDate($value);
					break;
			}
		}
		return $this->isComplete();
	}

	public function getId($asArray = false) {
		$id = [];
		foreach (serviceContext::CONTEXT_ID_MADEBY as $attr) {
			$getter = Inflector::camelize('get_'.$attr);
			switch ($attr) {
				case 'date':
					$item = $this->$getter(false);
					break;
				case 'bundle':
					// if(!($this->$getter() instanceOf Bundle))
						$item = $this->getBundleNameAnyway();
					break;
				default:
					$item = $this->$getter();
					break;
			}
			$id[] = $this->getString($item);
		}
		if(!count($id)) throw new Exception("Error ".__METHOD__."(): no data found for creating id!", 1);
		return $asArray ? $id : 'ctx_'.implode('_', $id);
	}


	/***********************************/
	/*** CONTROLS
	/***********************************/

	/**
	 * Is Context valid
	 * @return boolean
	 */
	public function isValid() {
		return !$this->hasErrors();
	}

	/**
	 * Is Context complete
	 * @return boolean
	 */
	public function isComplete($exceptionIfNotComplete = false) {
		$this->complete = !$this->hasErrors();
		if($exceptionIfNotComplete && !$this->complete) throw new Exception("Error ".__METHOD__."(): could not complete context!", 1);
		return $this->complete;
	}

	/**
	 * Get errors
	 * @param boolean $count = false (if true, returns only number of errors)
	 * @return array | integer
	 */
	public function getErrors($count = false) {
		$this->errors = [];
		if(!($this->getContainer() instanceOf ContainerInterface)) $this->errors['container'] = 'Container is missing';
		if(!($this->getParameterBag() instanceOf ParameterBagInterface)) $this->errors['parameterBag'] = 'ParameterBag is missing';
		if(!($this->getEnvironment() instanceOf Tier)) $this->errors['environment'] = 'Environment is missing';
		if(!($this->getEntreprise() instanceOf Entreprise)) $this->errors['entreprise'] = 'Entreprise is missing';
		if(!$this->isValidDate($this->getDate(false))) $this->errors['date'] = 'Date is not valid. Got '.json_encode($this->getDate(false));
		if(empty($this->getVersion())) $this->errors['version'] = 'Version is not valid. Got '.json_encode($this->getVersion());
		if(!$this->isRequestLess()) {
			if(!$this->isBundleLess()) {
				if(!($this->getBundle() instanceOf Bundle)) $this->errors['bundle'] = 'Bundle is missing';
			}
			if(!($this->getRequest() instanceOf Request)) $this->errors['request'] = 'Request is missing';
			if(empty($this->getLocale())) $this->errors['locale'] = 'Locale is missing';
			if(empty($this->getDefaultLocale())) $this->errors['default_locale'] = 'Default locale is missing';
			if(empty($this->getLanguage())) $this->errors['language'] = 'Language is missing';
			if(empty($this->getDefaultLanguage())) $this->errors['default_language'] = 'Default language is missing';
		}
		return $count ? count($this->errors) : $this->errors;
	}

	/**
	 * Has errors
	 * @return boolean
	 */
	public function hasErrors() {
		return $this->getErrors(true) > 0;
	}

	/**
	 * Get warnings
	 * @param boolean $count = false (if true, returns only number of warnings)
	 * @return array | integer
	 */
	public function getWarnings($count = false) {
		$this->warnings = [];
		if(!$this->isPublic() && !($this->getUser() instanceOf User) && !$this->isRequestLess()) $this->warnings['user'] = 'No user connected';
		return $count ? count($this->warnings) : $this->warnings;
	}

	/**
	 * Has warnings
	 * @return boolean
	 */
	public function hasWarnings() {
		return $this->getWarnings(true) > 0;
	}


	/***********************************/
	/*** REQUEST
	/***********************************/

	public function getRequest() {
		return $this->request;
	}

	protected function setRequest(Request $request = null) {
		$this->request = $request;
		return $this;
	}

	public function isRequestLess() {
		return !$this->request instanceOf Request;
	}

	public function isBundleLess() {
		return !$this->container->get(serviceKernel::class)->hasBundle();
	}

	public function hasRequest() {
		return $this->request instanceOf Request;
	}





	/***********************************/
	/*** CONTEXT --> ENVIRONMENT (User or Entreprise)
	/***********************************/

	/**
	 * Get environment
	 * @return Tier
	 */
	public function getEnvironment() {
		if($this->isUserEnvironmentButNotCurrent()) $this->environment = $this->user;
		return $this->environment;
	}

	/**
	 * Get environment as string
	 * @return string
	 */
	public function getEnvironmentAsString() {
		if(null !== $this->getEnvironment()) return $this->getEnvironment()->getName();
		return null;
	}

	/**
	 * Set environment
	 * @param tier $tier
	 * @return Context
	 */
	public function setEnvironment(Tier $tier) {
		$this->environment = $tier;
		return $this;
	}

	/**
	 * Is $bddid same of environment entity
	 * @return boolean
	 */
	public function isEnvironmentBddid($bddid = null) {
		if(!serviceEntities::isValidBddid($bddid, false) || !($this->environment instanceOf Tier)) return false;
		if($bddid instanceOf Tier) $bddid = $bddid->getBddid();
		return $this->environment->getBddid() === $bddid;
	}

	/**
	 * Is environment = (any) User
	 * @return boolean
	 */
	public function isUserEnv() {
		return $this->getEnvironment() instanceOf User;
	}
	public function isUserEnvironment() { return $this->isUserEnv(); }

	/**
	 * Is environment = User but not logged user
	 * @return boolean
	 */
	public function isUserEnvironmentButNotCurrent() {
		return $this->environment instanceOf User && $this->getUser() instanceOf User && $this->environment !== $this->user;
	}

	/**
	 * Is environment = $tier
	 * @return boolean
	 */
	public function isCurrentEnvironment($tier) {
		if(!($this->environment instanceOf Tier)) return false;
		// Not object but array
		if(is_array($tier)) return $this->environment->getId() === intval($tier['id']);
		return $tier instanceOf Tier ? $this->environment === $tier : false;
	}

	public function getCountersEnvironments(Tier $tier = null, $classes = null) {
		$tier = $tier instanceOf Tier ? $tier : $this->environment;
		if($this->isRequestLess() || !($tier instanceOf Tier)) return [];
		$tier_index = $tier->getId();
		if(!isset($this->countersEnvironments[$tier_index])) {
			$classes ??= ['User','Entreprise','Annonce','Blog','Message','Pdf','Popup','Video'];
			// if($tier instanceOf User) array_unshift($classes, 'User');
			$items = $this->container->get(serviceItem::class)->getGrantedItems($tier, $classes, [], true, true);
			foreach ($items as $item) {
				$this->countersEnvironments[$tier_index][$item['shortname']] = $item;
			}
			foreach ($classes as $shortname) {
				if(!isset($this->countersEnvironments[$tier_index][$shortname])) $this->countersEnvironments[$tier_index][$shortname] = ['count' => 0, 'shortname' => $shortname];
					else $this->countersEnvironments[$tier_index][$shortname]['count'] = intval($this->countersEnvironments[$tier_index][$shortname]['count']);
			}
		}
		return $this->countersEnvironments[$tier_index];
	}

	public function getUserChoicesEnvironments(User $user = null, $classes = null, $asArrayResult = true) {
		$user = $user instanceOf User ? $user : $this->getUser();
		if($this->isRequestLess() || !($user instanceOf User)) return [];
		$user_index = $user->getId().'_'.json_encode($asArrayResult);
		if(!isset($this->choicesEnvironments[$user_index])) {
			$classes ??= ['User','Entreprise'];
			$items = $this->container->get(serviceItem::class)->getGrantedItems($user, $classes, [], $asArrayResult);
			// return $items;
			$this->choicesEnvironments[$user_index] = [];
			foreach ($items as $item) {
				$data = [
					'id' => $item instanceOf Item ? $item->getId() : $item['id'],
					'shortname' => $item instanceOf Item ? $item->getShortname() : $item['shortname'],
				];
				$this->choicesEnvironments[$user_index][$data['shortname']] ??= new ArrayCollection();
				$this->choicesEnvironments[$user_index][$data['shortname']]->set($data['id'], $item);
			}
			foreach ($classes as $shortname) {
				if(!isset($this->choicesEnvironments[$user_index][$shortname])) $this->choicesEnvironments[$user_index][$shortname] = new ArrayCollection();
			}
		}
		return $this->choicesEnvironments[$user_index];
	}

	public function getGrantedItems(User $user = null, $classes = ['User','Entreprise'], $actions = [], $asArrayResult = true, $getCounts = false) {
		return $this->container->get(serviceItem::class)->getGrantedItems($user instanceOf User ? $user : $this->getUser(), $classes, $actions, $asArrayResult, $getCounts);
	}

	// public function getManagers(Item $item) {
	// 	return $this->container->get(serviceItem::class)->getManagers($item);
	// }

	/***********************************/
	/*** CONTEXT --> ENTREPRISE
	/***********************************/

	/**
	 * Get entreprise
	 * @return Entreprise
	 */
	public function getEntreprise() {
		return $this->entreprise;
	}

	/**
	 * Set entreprise
	 * @param Entreprise $entreprise
	 * @return Entreprise
	 */
	public function setEntreprise(Entreprise $entreprise) {
		$this->entreprise = $entreprise;
		return $this;
	}

	/**
	 * Is $bddid same of entreprise entity
	 * @return boolean
	 */
	public function isEntrepriseBddid($bddid = null) {
		if(!serviceEntities::isValidBddid($bddid, false) || !($this->entreprise instanceOf Entreprise)) return false;
		if($bddid instanceOf Entreprise) $bddid = $bddid->getBddid();
		return $this->entreprise->getBddid() === $bddid;
	}

	/**
	 * Is entreprise = Entreprise entity
	 * @return boolean
	 */
	public function isEntrepriseEnv() {
		return $this->getEnvironment() instanceOf Entreprise;
	}
	public function isEntrepriseEnvironment() { return $this->isEntrepriseEnv(); }

	public function getChoicesEntreprises(User $user = null) {
		if($this->isRequestLess()) return new ArrayCollection();
		$user = $user instanceOf User ? $user : $this->getUser();
		$this->choicesEntreprises = new ArrayCollection();
		if(!$user instanceOf User) return $this->choicesEntreprises;
		foreach ($this->container->get(serviceEntreprise::class)->getRepository()->findAll() as $entreprise) {
			if($this->container->get(serviceGrants::class)->isGrantedEntity($entreprise, $user)) $this->choicesEntreprises->add($entreprise);
		}
		return $this->choicesEntreprises;
	}

	public function isDefaultEntreprise() {
		return $this->container->get(serviceEntreprise::class)->getDefaultEntreprise() === $this->getEntreprise();
	}

	public function isCurrentEntreprise(Entreprise $entreprise) {
		return $this->getEntreprise() === $entreprise;
	}

	public function getEntrepriseAsString() {
		if(null !== $this->getEntreprise()) return $this->getEntreprise()->getName();
		return null;
	}

	public function isUserEntrepriseEnv() {
		return $this->getEnvironment() instanceOf Entreprise && !$this->getEnvironment()->isPrefered();
	}

	public function isMainEntrepriseEnv() {
		return $this->getEnvironment() instanceOf Entreprise && $this->getEnvironment()->isPrefered();
	}

	/***********************************/
	/*** TESTS
	/***********************************/

	/**
	 * Is User SUPER_ADMIN
	 * @return boolean
	 */
	public function isSuperadmin() {
		$user = $this->getUser();
		return $user instanceOf User ? $user->isSuperadmin() : false;
	}

	/**
	 * Get ENVIRONMENT
	 * @return string
	 */
	public function getEnv() {
		return $this->container->get(serviceKernel::class)->getEnv();
	}

	/**
	 * Is DEV Context
	 * @return boolean
	 */
	public function isDev() {
		return $this->container->get(serviceKernel::class)->isDev();
	}

	/**
	 * Is DEV Context or User is SUPER_ADMIN
	 * @return boolean
	 */
	public function isDevOrSadmin() {
		return $this->isDev() || $this->isSuperadmin();
	}

	/**
	 * Is PROD Context
	 * @return boolean
	 */
	public function isProd() {
		return $this->container->get(serviceKernel::class)->isProd();
	}

	/**
	 * Is TEST Context
	 * @return boolean
	 */
	public function isTest() {
		return $this->container->get(serviceKernel::class)->isTest();
	}

	/**
	 * Is NORMAL Context
	 * @return boolean
	 */
	public function isNormalContext() {
		return $this->isDefaultDate() && $this->isDefaultEntreprise();
	}

	/**
	 * Is environment = current User (connected) entity
	 * @return boolean
	 */
	public function isCurrentUserEnv() {
		return $this->getEnvironment() === $this->getUser();
	}


	/***********************************/
	/*** MARKETPLACE
	/***********************************/

	/**
	 * Is Marketplace module enabled for all website
	 * @return boolean
	 */
	public function isGlobalMarketplace() {
		return $this->param('modules/marketplace/enabled');
	}

	/**
	 * Is entity (or Context Environment) granted for Marketplace module
	 * @param object $entity = null
	 * @return boolean
	 */
	public function isMarketplace(Entreprise $entity = null) {
		if(!$this->isGlobalMarketplace()) return false;
		if(!($entity instanceOf Entreprise)) $entity = $this->getEnvironment();
		if(!($entity instanceOf Entreprise)) return false;
		return $entity->getPreference('MarketplaceBundle/active', false, false, true);
	}


	/***********************************/
	/*** NAME
	/***********************************/

	/**
	 * Set name (if not locktd)
	 * @param string $name
	 * @return Context
	 */
	public function setName($name) {
		if($this->isNameUnlocked()) {
			$this->name = (string)$name;
			if(empty($this->name)) throw new Exception("Error ".__METHOD__."(): name can not be null. Got ".json_encode($name)."!", 1);
		}
		return $this;
	}

	/**
	 * Get name
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set name_locked
	 * @param boolean $name_locked = true
	 * @return Context
	 */
	public function setNameLocked($name_locked = true) {
		$this->name_locked = (boolean)$name_locked;
		return $this;
	}

	/**
	 * Lock name
	 * @return string
	 */
	public function lockName() {
		$this->setNameLocked(true);
		return $this;
	}

	/**
	 * Unlock name
	 * @return string
	 */
	public function unlockName() {
		$this->setNameLocked(false);
		return $this;
	}

	/**
	 * Is name locked
	 * @return boolean
	 */
	public function isNameLocked() {
		return $this->name_locked;
	}

	/**
	 * Is name unlocked
	 * @return boolean
	 */
	public function isNameUnlocked() {
		return !$this->name_locked;
	}

	public function getAllContextNamedElements($named = false, $withUrls = false) {
		$environment = $this->getEnvironment();
		$environment_bddid = $environment instanceOf Tier ? ($named ? $environment->getBddid() : $environment) : null;
		$entreprise = $this->getEntreprise();
		$entreprise_bddid = $entreprise instanceOf Tier ? ($named ? $entreprise->getBddid() : $entreprise) : null;
		$user = $this->getUser();
		$user_bddid = $user instanceOf User ? ($named ? $user->getBddid() : $user) : null;
		$data = [
			'_CEnvironment' => $environment_bddid,
			'_CEntreprise' => $entreprise_bddid,
			'_CUser' => $user_bddid,
			'_CBundle' => $named ? $this->getBundleName() : $this->getBundle(),
			'_CDate' => $named ? $this->getDateAsAtom() : $this->getDate(false),
			'_CAtom' => $this->getDateAsAtom(),
			'_CTimezone' => $this->getTimeZone(),
			'_CPublic' => $this->isPublic(),
			'_CLocale' => $this->getLocale(),
			'_CDefaultlocale' => $this->getDefaultLocale(),
			'_CLocales' => $this->getLocales(),
			'_CLanguage' => $named ? $this->getLanguageName() : $this->getLanguage(),
			'_CLanguages' => $named ? $this->getLanguagesNames() : $this->getLanguages(),
			'_CMarketplace' => $this->isMarketplace(),
			'_CEvents' => $this->isEvents(),
			'_CSeo' => $this->isSeo(),
			'_CErrors' => $this->getErrors(),
			'_CWarnings' => $this->getWarnings(),
		];
		if($withUrls) {
			$target = $this->isPublic() ? ' target="_blank"' : '';
			$items = [
				'environment' => '<a title="Aucun environnement"><i>Aucun environnement</i></a>',
				'entreprise' => '<a title="Aucune entreprise"><i>Aucune entreprise</i></a>',
				'user' => '<a title="Aucun utilisateur"><i>Non connecté</i></a>',
			];
			foreach ($items as $name => $message) {
				$data['_C'.ucfirst($name).'_show'] = $$name instanceOf Tier ?
					'<a href="'.$this->container->get('router')->generate('wsa_'.$$name->getShortname(true).'_show', ['id' => $$name->getId()], false).'"'.$target.'><i class="fa '.$$name->getIcon().' fa-fw" style="color: '.$$name->getColor().';"></i>'.$$name->getName().'</a>':
					$message;
			}
			// $entity = $this->getEnvironment();
			// $data['_CEnvironment_show'] = '<a href="'.$this->container->get('router')->generate('wsa_'.$entity->getShortname(true).'_show', ['id' => $entity->getId()], false).'"'.$target.'><i class="fa '.$entity->getIcon().' fa-fw" style="color: '.$entity->getColor().';"></i>'.$entity->getName().'</a>';
			// $entity = $this->getEntreprise();
			// $data['_CEntreprise_show'] = '<a href="'.$this->container->get('router')->generate('wsa_'.$entity->getShortname(true).'_show', ['id' => $entity->getId()], false).'"'.$target.'><i class="fa '.$entity->getIcon().' fa-fw" style="color: '.$entity->getColor().';"></i>'.$entity->getName().'</a>';
			// $data['_CUser_show'] = $user instanceOf User ?
			// 	'<a href="'.$this->container->get('router')->generate('wsa_'.$user->getShortname(true).'_show', ['id' => $user->getId()], false).'"'.$target.'><i class="fa '.$user->getIcon().' fa-fw" style="color: '.$user->getColor().';"></i>'.$user->getName().'</a>':
			// 	'<a title="Aucun utilisateur"><i>Non connecté</i></a>';
		}
		return $data;
	}


	/***********************************/
	/*** USER
	/***********************************/

	// /**
	//  * Set User
	//  * @param User $user = null
	//  * @return Context
	//  */
	// public function setUser(User $user = null) {
	// 	$this->user = $user;
	// 	return $this;
	// }

	/**
	 * Get User
	 * @return User | null
	 */
	public function getUser() {
		$this->user = $this->container->get(serviceContext::class)->getDefaultUser();
		return $this->user;
	}
	public function getContextUser() { return $this->getUser(); }

	/**
	 * Has User
	 * @return boolean
	 */
	public function hasUser() {
		return $this->getUser() instanceOf User;
	}


	/***********************************/
	/*** TIMEZONE & DATES
	/***********************************/

	/**
	 * Get date as object or format
	 * 	- if $asObjectOrFormat is DateTime, returns same DateTime
	 * 	- if $asObjectOrFormat is true, returns object
	 * 	- if $asObjectOrFormat is false, returns context date (DateTime)
	 * 	- if $asObjectOrFormat is string (format), returns date formated string
	 * @param boolean $asObjectOrFormat = true
	 * @return mixed
	 */
	public function getDate($asObjectOrFormat = true) {
		if($asObjectOrFormat instanceOf DateTime) return $asObjectOrFormat;
		if(!$asObjectOrFormat) return $this->date;
		$date = new DateTime(serviceContext::DEFAULT_DATE);
		serviceContext::modifyDate($date, $this->date);
		if(is_string($asObjectOrFormat)) {
			switch ($asObjectOrFormat) {
				case 'DATE_ATOM':
				case 'ATOM':
					return $date->format(serviceContext::CONTEXT_FORMAT_DATE);
					break;
				default:
					return $date->format($asObjectOrFormat);
					break;
			}
		}
		return $date;
	}
	public function getContextDate($asObjectOrFormat = true) {
		return $this->getDate($asObjectOrFormat);
	}

	/**
	 * Get Context date as ATOM
	 * @return string
	 */
	public function getDateAsAtom() {
		return $this->getDate('DATE_ATOM');
	// 	return $this->getDate('ATOM');
	}
	public function getContextDateAsAtom() { return $this->getDateAsAtom(); }

	/**
	 * Set date
	 * @param string $date
	 * @return string
	 */
	public function setDate($date) {
		if(!is_string($date)) throw new Exception("Error ".__METHOD__."(): date parameter must be string (date string or date modifyer)", 1);
		if(!$this->isValidDate($date)) throw new Exception("Error ".__METHOD__."(): date parameter is not valid. Please choose it in ".json_encode($this->getValidDates())."!", 1);
		$this->date = $date;
		return $this;
	}

	/**
	 * Get TimeZone
	 * @return string
	 */
	public function getTimeZone() {
		$serviceTimezone = $this->container->get(serviceTimezone::class);
		if($this->getUser() instanceOf User) {
			$timezone = $this->getUser()->getTimezone();
			if($serviceTimezone->isValidTimezone($timezone)) return $timezone;
		}
		return $serviceTimezone->getDefaultTimezone();
		// return $this->param('timeZone');
	}

	/**
	 * Get Context date as string
	 * @return string
	 */
	public function getDateAsString() {
		$choicesDates = $this->getChoicesDates();
		return array_key_exists($this->date, $choicesDates) ? $choicesDates[$this->date] : $this->date;
	}
	public function getContextDateAsString() { return $this->getDateAsString(); }

	/**
	 * Get all Context DateTime as array
	 * @return array
	 */
	public function getAllContextDatetime() {
		return [
			'_CDate' => $this->getDate(),
			'_CAtom' => $this->getDateAsAtom(),
			'_CTimezone' => $this->getTimeZone(),
		];
	}

	/**
	 * Context date is default date
	 * @return boolean
	 */
	public function isDefaultDate() {
		return $this->date === serviceContext::DEFAULT_DATE;
	}

	/**
	 * Is $date the same day of Context date
	 * @param mixed $date (string or DateTime)
	 * @return boolean
	 */
	public function isContextToday($date) {
		if(is_string($date)) $date = new DateTime($date);
		if(!($date instanceOf DateTime)) {
			$typ = is_object($date) ? 'instance of '.get_class($date) : gettype($date).' type';
			throw new Exception("Error ".__METHOD__."(): date parameter must be DateTime or valid string for DateTime construction, but got ".$date."!", 1);
		}
		return $date->format('Ymd') === $this->getDate('Ymd');
	}

	/**
	 * Is $date deprecated, regarding to Context date
	 * @param mixed $date (string or DateTime)
	 * @param boolean $includeAllDay = true
	 * @return boolean
	 */
	public function isContextDeprecated($date, $includeAllDay = true) {
		if(is_string($date)) $date = new DateTime($date);
		$contextDate = clone $this->getDate();
		if($includeAllDay) $contextDate->setTime(0,0,0);
		return $date < $contextDate;
	}

	/**
	 * Get Context day
	 * @param string | boolean $modify = false
	 * @return string
	 */
	public function getContextDay($modify = false) {
		if(!is_string($modify)) return $this->getDate("d");
		$date = clone $this->getDate();
		return $date->modify($modify)->format("d");
	}

	/**
	 * Get Context month
	 * @param string | boolean $modify = false
	 * @return string
	 */
	public function getContextMonth($modify = false) {
		if(!is_string($modify)) return $this->getDate("m");
		$date = clone $this->getDate();
		return $date->modify($modify)->format("m");
	}

	/**
	 * Get Context year
	 * @param string | boolean $modify = false
	 * @return string
	 */
	public function getContextYear($modify = false) {
		if(!is_string($modify)) return $this->getDate("Y");
		$date = clone $this->getDate();
		return $date->modify($modify)->format("Y");
	}

	/**
	 * Get current date
	 * @return string
	 */
	public static function getCurrentDate() {
		return serviceContext::getCurrentDate();
	}

	/**
	 * Get current year
	 * @return string
	 */
	public static function getCurrentYear() {
		return serviceContext::getCurrentYear();
	}

	/**
	 * Get dates for form choice
	 * @see https://www.php.net/manual/fr/datetime.formats.relative.php
	 */
	public function getChoicesDates() {
		return [
			"NOW" => 'date.now',
			"today" => 'date.now_at_0',
			"-1 DAY" => 'date.hier',
			"yesterday" => 'date.hier_at_0',
			"+1 DAY" => 'date.demain',
			"+2 DAY" => 'date.apres_demain',
			"+1 DAY|wednesday" => 'date.mercredi_pro',
			"+1 DAY|saturday" => 'date.samedi_pro',
			"+1 DAY|sunday" => 'date.dimanche_pro',
		];
	}

	public function getValidDates() {
		$dates = $this->getChoicesDates();
		return array_keys($dates);
	}

	public function isValidDate($date) {
		$valid = true;
		try {
			serviceContext::modifyDate(new DateTime(serviceContext::DEFAULT_DATE), $date);
		} catch (Exception $e) {
			$valid = false;
		}
		return $valid;
		// old test
		// $dates = $this->getChoicesDates();
		// return in_array($date, $dates);
	}

	public function isTimetraveller() {
		return $this->param('modules/timetraveller/enabled');
	}


	/***********************************/
	/*** PARAMETERBAG
	/***********************************/

	/**
	 * Set ParameterBag
	 * @param ParameterBagInterface $parameterBag = null
	 * @return Context
	 */
	protected function setParameterBag(ParameterBagInterface $parameterBag) {
		$this->parameterBag = $parameterBag;
		return $this;
	}

	/**
	 * Get ParameterBag
	 * @return ParameterBagInterface
	 */
	public function getParameterBag() {
		return $this->parameterBag;
	}

	public function all($all = false) {
		$params = $this->parameterBag->all();
		return $all ? $params : array_filter($params, function($key) { return preg_match('#^\\w+$#', $key); }, ARRAY_FILTER_USE_KEY);
	}

	/**
	 * Get parameter from ParameterBag
	 * @param string $name (separate path with . or /)
	 * @param boolean $defaultIfNotFound = null
	 * @return mixed
	 */
	public function param($name, $defaultIfNotFound = null) {
		$names = preg_split('#[\\.\\/]#', $name);
		$name = array_shift($names);
		if(empty($this->parameterBag)) $this->parameterBag = $this->container->getParameterBag();
		if(!$this->parameterBag->has($name)) return $defaultIfNotFound;
		$param = $this->parameterBag->get($name);
		foreach ($names as $value) {
			if(!empty($value)) {
				if(!isset($param[$value])) return $defaultIfNotFound;
				$param = $param[$value];
			}
		}
		return $param;
	}

	public function parameter($name, $defaultIfNotFound = null) {
		return $this->param($name, $defaultIfNotFound);
	}


	/***********************************/
	/*** BUNDLE
	/***********************************/

	/**
	 * Is registerable for session
	 * @return boolean
	 */
	public function isRegister() {
		return static::REGISTER_ALL || $this->bundle instanceOf extendedBundleInterface;
	}

	/**
	 * Set Bundle & Bundle name
	 * @param Bundle $bundle = null
	 * @return Context
	 */
	protected function setBundle(Bundle $bundle = null) {
		$this->bundle = $bundle instanceOf Bundle ? $bundle : null;
		$this->bundle_name = $this->bundle instanceOf Bundle ? get_class($this->bundle) : null;
		return $this;
	}

	/**
	 * Get Bundle
	 * @return Bundle | null
	 */
	public function getBundle() {
		return $this->bundle;
	}
	public function getCurrentBundle() { return $this->getBundle(); }

	/**
	 * Get Bundle name
	 * @param boolean $short = true
	 * @return string | null
	 */
	public function getBundleName($short = true) {
		if(empty($this->bundle_name)) return null;
		return $short ? serviceClasses::getShortname($this->bundle_name) : $this->bundle_name;
	}
	public function getCurrentBundleName() { return $this->getBundleName(); }

	/**
	 * Get Bundle name even if not registerable or not found
	 * @return string
	 */
	public function getBundleNameAnyway() {
		return $this->container->get(serviceKernel::class)->getBundleNameAnyway(true);
	}

	/**
	 * Is public context
	 * @return boolean
	 */
	public function isPublic() {
		if($this->isRequestLess() || empty($this->bundle)) return false;
		return $this->bundle instanceOf extendedBundleInterface ? $this->bundle->isPublicBundle() : false;
	}

	/**
	 * Is templatable
	 * @return boolean
	 */
	public function isTemplatable(User $user = null) {
		if($this->isRequestLess() || !($this->bundle instanceOf extendedBundleInterface)) return false;
		if(!($user instanceOf User)) $user = $this->getUser();
		return $this->container->get(serviceGrants::class)->isGranted($this->bundle->getRoleTemplatable(), $user);
	}


	/*************************************************************************************/
	/*** GRANTS
	/*************************************************************************************/

	public function isGranted($roles, User $user) {
		return $this->container->get(serviceGrants::class)->isGranted($roles, $user);
	}


	/*************************************************************************************/
	/*** INTERNATIONAL
	/*************************************************************************************/

	public function isTranslator() {
		return $this->param('modules/translations/enabled');
	}

	public function isTranslatorAndShow() {
		return $this->param('modules/translations/enabled') && $this->param('modules/translations/show');
	}

	public function getTranslationDomain() {
		$domain = $this->bundle instanceOf extendedBundleInterface ? $this->bundle->getTranslationDomain() : null;
		return empty($domain) ? serviceTwgTranslation::DEFAULT_DOMAIN : $domain;
	}


	/*************************************************************************************/
	/*** EVENTS
	/*************************************************************************************/

	public function isEvents() {
		return $this->param('modules/events/enabled');
	}


	/*************************************************************************************/
	/*** LANGUAGE
	/*************************************************************************************/

	/**
	 * Get all Languages
	 * @return Array <Language>
	 */
	public function getLanguages() {
		if($this->isRequestLess()) return $this->container->get(serviceLanguage::class)->getLanguages(false);
		return $this->container->get(serviceLanguage::class)->getLanguages($this->isPublic());
	}

	/**
	 * Get all Languages names
	 * @return Array <string>
	 */
	public function getLanguagesNames() {
		if($this->isRequestLess()) return $this->container->get(serviceLanguage::class)->getLanguagesNames(false);
		return $this->container->get(serviceLanguage::class)->getLanguagesNames($this->isPublic());
	}

	/**
	 * Get all Languages names
	 * @return Array <string>
	 */
	public function getLocales() {
		if($this->isRequestLess()) return $this->container->get(serviceLanguage::class)->getLocales(false);
		return $this->container->get(serviceLanguage::class)->getLocales($this->isPublic());
	}

	/**
	 * Get locale
	 * @return string
	 */
	public function getLocale() {
		return $this->locale;
	}
	public function getCurrentLocale() { return $this->getLocale(); }

	/**
	 * Set locale
	 * @param string $locale
	 * @return Context
	 */
	public function setLocale($locale) {
		if(!serviceLanguage::isValidLocale($locale)) throw new Exception("Error ".__METHOD__."(): local ".json_encode($locale)." is not valid!", 1);
		$locales = $this->getLocales();
		$this->locale = in_array($locale, $locales) ? $locale : $this->getDefaultLocale();
		return $this;
	}
	public function setCurrentLocale($locale) { return $this->setLocale($locale); }

	/**
	 * Get default locale
	 * @return string
	 */
	public function getDefaultLocale() {
		return $this->container->get(serviceLanguage::class)->getPreferedLocale();
	}

	/**
	 * Get current Language
	 * @return Language | array
	 */
	public function getLanguage() {
		$locale = $this->getLocale();
		if(serviceLanguage::isValidLocale($locale)) {
			foreach ($this->getLanguages() as $language) {
				if($locale === serviceLanguage::language_fullname($language)) return $language;
			}
		}
		return null;
	}
	public function getCurrentLanguage() { return $this->getLanguage(); }

	public function getLanguageName() {
		$language = $this->getLanguage();
		return serviceLanguage::isLanguage($language) ? serviceLanguage::language_fullname($language) : null;
	}

	/**
	 * Get default Language
	 * @return Language | array
	 */
	public function getDefaultLanguage() {
		return $this->container->get(serviceLanguage::class)->getPreferedLanguage();
	}


	/*************************************************************************************/
	/*** SEO
	/*************************************************************************************/

	/**
	 * Is SEO
	 * @return boolean
	 */
	public function isSeo() {
		return $this->bundle instanceOf extendedBundleInterface ? $this->bundle->isSeo() : false;
	}






}