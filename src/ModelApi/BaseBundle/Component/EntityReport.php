<?php
namespace ModelApi\BaseBundle\Component;

// use Doctrine\ORM\Mapping as ORM;
// use ModelApi\AnnotBundle\Annotation as Annot;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;

use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\NestedInterface;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\managedEntity;
use ModelApi\InternationalBundle\Service\serviceTranslation;

use \ReflectionClass;
use \Exception;

class EntityReport {

	// protected $entity;
	// protected $container;
	// protected $repair;
	// protected $serviceEntities;
	// protected $reports;
	// protected $errors;
	// protected $valid;
	// protected $analysed;

	// public function __construct($entity, ContainerInterface $container, $repair = false) {
	// 	$this->entity = $entity;
	// 	$this->container = $container;
	// 	$this->repair = $repair;
	// 	$this->serviceEntities = $this->container->get(serviceEntities::class);
	// 	$this->serviceTranslation = $this->container->get(serviceTranslation::class);
	// 	$this->reports = new ArrayCollection();
	// 	$this->errors = 0;
	// 	$this->analysed = false;
	// 	// $this->isValid();
	// 	if($this->isValid()) $this->analyse();
	// 	return $this;
	// }

	// /**
	//  * Get shortname of service
	//  * @return string
	//  */
	// public function __toString() {
	// 	$class = new ReflectionClass(static::class);
	// 	return $class->getShortName();
	// }

	// public function isValid() {
	// 	return $this->valid = $this->serviceEntities->entityExists($this->entity);
	// }

	// public function getEntity() {
	// 	return $this->entity;
	// }



	// /*************************************************************************************/
	// /*** GROUPS
	// /*************************************************************************************/

	// public function getGroupReports($groupName, $refresh = false) {
	// 	if(!$this->analysed || $refresh) return $this->analyse();
	// 	return $this->reports->filter(
	// 		function($report) use ($groupName) {
	// 			return $report['group'] === $groupName;
	// 		}
	// 	);
	// }

	// public function getGroups($refresh = false) {
	// 	if(!$this->analysed || $refresh) $this->analyse();
	// 	$groups = [];
	// 	foreach ($this->getReports() as $report) {
	// 		$groups[$report['group']] = $report['group'];
	// 	}
	// 	return $groups;
	// }

	// public function getGroupErrors($groupName, $refresh = false) {
	// 	if(!$this->analysed || $refresh) $this->analyse();
	// 	$errors = 0;
	// 	foreach ($this->getGroupReports($groupName) as $report) {
	// 		if($report['error'] && $report['_count_error']) $errors++;
	// 	}
	// 	return $errors;
	// }

	// public function getGroupErrorMessages($groupName, $refresh = false) {
	// 	if(!$this->analysed || $refresh) $this->analyse();
	// 	$messages = [];
	// 	foreach ($this->getGroupReports($groupName) as $report) {
	// 		if($report['error']) $messages[] = $report['message'];
	// 	}
	// 	return $messages;
	// }

	// public function hasGroupErrors($groupName, $refresh = false) {
	// 	if(!$this->analysed || $refresh) $this->analyse();
	// 	return $this->getGroupErrors($groupName) > 0;
	// }

	// public function isGroupClean($groupName, $refresh = false) {
	// 	if(!$this->analysed || $refresh) $this->analyse();
	// 	return $this->getGroupErrors($groupName) === 0;
	// }



	// /*************************************************************************************/
	// /*** GLOBAL
	// /*************************************************************************************/

	// public function getReports($refresh = false) {
	// 	if(!$this->analysed || $refresh) return $this->analyse();
	// 	return $this->reports;
	// }

	// public function getErrors($refresh = false) {
	// 	if(!$this->analysed || $refresh) $this->analyse();
	// 	$this->errors = 0;
	// 	foreach ($this->getReports() as $report) {
	// 		if($report['error'] && $report['_count_error']) $this->errors++;
	// 	}
	// 	return $this->errors;
	// }

	// public function getErrorMessages($refresh = false) {
	// 	if(!$this->analysed || $refresh) $this->analyse();
	// 	$messages = [];
	// 	foreach ($this->getReports() as $report) {
	// 		if($report['error']) $messages[] = $report['message'];
	// 	}
	// 	return $messages;
	// }

	// public function hasErrors($refresh = false) {
	// 	if(!$this->analysed || $refresh) $this->analyse();
	// 	return $this->getErrors() > 0;
	// }

	// public function isClean($refresh = false) {
	// 	if(!$this->analysed || $refresh) $this->analyse();
	// 	return $this->getErrors() === 0;
	// }




	// public function analyse() {
	// 	if($this->isValid()) {
	// 		$this->reports = new ArrayCollection();

	// 		//*********************************************************************************************************
	// 		// GROUP "Nested Interface" -if NestedInterface
	// 		//*********************************************************************************************************
	// 		if(serviceClasses::isInterface($this->entity, 'NestedInterface')) {

	// 			//*********************************************************************************************************
	// 			// 1 SEUL HARDLINK PARENT
	// 			//*********************************************************************************************************
	// 			// $spl = $this->entity->getSolidParents(false, false);
	// 			// $error = $spl->count() > 1;
	// 			// $data = new ArrayCollection();
	// 			// foreach ($spl as $nestlink) {
	// 			// 	$data->add($nestlink->getDirectory());
	// 			// }
	// 			// $test = [
	// 			// 	'name' => 'Parent hardlinks <i class="text-muted">(doit être 0 ou 1)</i>',
	// 			// 	'group' => 'Nested Interface',
	// 			// 	'message' => ($error ? '<strong class="text-danger">'.$spl->count().'</strong>' : '<span class="text-success">'.$spl->count().'</span>'),
	// 			// 	'data' => $data,
	// 			// 	'error' => $error,
	// 			// 	'_count_error' => true,
	// 			// 	'prints' => ['full','medium'],
	// 			// ];
	// 			// $this->reports->set($test['group'].'@'.$test['name'], $test);
	// 			// if($error) $this->errors++;

	// 			// //*********************************************************************************************************
	// 			// // PARENTS IDENTIQUES
	// 			// //*********************************************************************************************************
	// 			// $smp = $this->entity->getSameParents();
	// 			// $error = count($smp) > 0;
	// 			// $data = new ArrayCollection();
	// 			// foreach ($smp as $dir) {
	// 			// 	$data->add($dir['parent']);
	// 			// }
	// 			// $test = [
	// 			// 	'name' => 'Parents identiques',
	// 			// 	'group' => 'Nested Interface',
	// 			// 	'message' => null,
	// 			// 	'data' => $data,
	// 			// 	'error' => $error,
	// 			// 	'_count_error' => true,
	// 			// 	'prints' => ['full','medium'],
	// 			// ];
	// 			// $this->reports->set($test['group'].'@'.$test['name'], $test);
	// 			// if($error) $this->errors++;

	// 			//*********************************************************************************************************
	// 			// ENFANTS IDENTIQUES
	// 			//*********************************************************************************************************
	// 			if(serviceClasses::isInterface($this->entity, 'DirectoryInterface')) {
	// 				$smc = $this->entity->getSameChilds();
	// 				$error = count($smc) > 1;
	// 				$data = new ArrayCollection();
	// 				foreach ($smc as $child) {
	// 					$data->add($child['child']);
	// 				}
	// 				$test = [
	// 					'name' => 'Enfants identiques',
	// 					'group' => 'Nested Interface',
	// 					'message' => null,
	// 					'data' => $data,
	// 					'error' => $error,
	// 					'_count_error' => true,
	// 					'prints' => ['full','medium'],
	// 				];
	// 				$this->reports->set($test['group'].'@'.$test['name'], $test);
	// 				if($error) $this->errors++;
	// 			}
	// 		}

	// 		//*********************************************************************************************************
	// 		// GROUP "Translatable" - if Translatator && is translatable Entity
	// 		//*********************************************************************************************************
	// 		if($this->serviceTranslation->isTranslatator() && $this->serviceTranslation->isTranslatable($this->entity)) {

	// 			//*********************************************************************************************************
	// 			// CONTIENT managedEntity
	// 			//*********************************************************************************************************
	// 			try {
	// 				$managedEntity = $this->entity->translationManager;
	// 				$error = !$managedEntity instanceOf managedEntity;
	// 			} catch (Exception $e) {
	// 				$error = true;
	// 			}
	// 			// $error = property_exists($this->entity, 'translationManager');
	// 			$test = [
	// 				'name' => 'Contient managedEntity',
	// 				'group' => 'Translatable',
	// 				'message' => ($error ? '<strong class="text-danger">ManagedEntity manquant</strong>' : '<span class="text-success">ManagedEntity présent</span>'),
	// 				'data' => [],
	// 				'error' => $error,
	// 				'_count_error' => true,
	// 				'prints' => ['full','medium'],
	// 			];
	// 			$this->reports->set($test['group'].'@'.$test['name'], $test);
	// 			if($error) $this->errors++;

	// 			if(!$error) {
	// 				$translationManager = $this->entity->translationManager;

	// 				//*********************************************************************************************************
	// 				// VALID translationManager ?
	// 				//*********************************************************************************************************
	// 				$errors = $translationManager->getErrors(true); // POUR FULL : test global seulement (sans les propriétés)
	// 				$error = count($errors) > 0;
	// 				$test = [
	// 					'name' => 'Global ManagedEntity validité',
	// 					'group' => 'Translatable',
	// 					'message' => ($error ? '<strong class="text-danger">ManagedEntity invalide <small><i class="text-muted">(GLOBAL)</i></small></strong>' : '<span class="text-success">ManagedEntity valide <small><i class="text-muted">(GLOBAL)</i></small></span>'),
	// 					'data' => $errors,
	// 					'error' => $error,
	// 					'_count_error' => false,
	// 					'prints' => ['full'],
	// 				];
	// 				$this->reports->set($test['group'].'@'.$test['name'], $test);
	// 				// if($error) $this->errors = $this->errors + count($errors);

	// 				$errors = $translationManager->getErrors(false); // POUR MEDIUM : test complet (avec les propriétés)
	// 				$error = count($errors) > 0;
	// 				$test = [
	// 					'name' => 'Complet ManagedEntity validité',
	// 					'group' => 'Translatable',
	// 					'message' => ($error ? '<strong class="text-danger">ManagedEntity invalide <small><i class="text-muted">(COMPLET)</i></small></strong></strong>' : '<span class="text-success">ManagedEntity valide <small><i class="text-muted">(COMPLET)</i></small></span>'),
	// 					'data' => $errors,
	// 					'error' => $error,
	// 					'_count_error' => true,
	// 					'prints' => ['medium'],
	// 				];
	// 				$this->reports->set($test['group'].'@'.$test['name'], $test);
	// 				if($error) $this->errors = $this->errors + count($errors);
	// 				// if($error) $this->errors++;

	// 				//*********************************************************************************************************
	// 				// CONTRÔLE DES CHAMPS translatable
	// 				//*********************************************************************************************************
	// 				$properties = $translationManager->getPropertiesNames();
	// 				foreach ($properties as $property) {
	// 					$propertyErrors = $translationManager->getPropertyErrors($property);
	// 					$error = count($propertyErrors) > 0;
	// 					$test = [
	// 						'name' => 'Propriété <strong>'.$property.'</strong>',
	// 						'group' => 'Translatable',
	// 						'message' => ($error ? '<strong class="text-danger">Propriété invalide</strong>' : '<span class="text-success">Propriété valide</span>'),
	// 						'data' => $propertyErrors,
	// 						'error' => $error,
	// 						'_count_error' => false,
	// 						'prints' => ['full'],
	// 					];
	// 					$this->reports->set($test['group'].'@'.$test['name'], $test);
	// 					// if($error) $this->errors++;
	// 				}

	// 			}


	// 		}

	// 		$this->analysed = true;
	// 	} else {
	// 		$this->analysed = false;
	// 	}
	// 	if($this->reports->count()) {
	// 		$this->reports = $this->reports->toArray();
	// 		ksort($this->reports);
	// 		$this->reports = new ArrayCollection($this->reports);
	// 	}
	// 	if($this->repair && $this->hasErrors(false)) return $this->repair();
	// 	return $this->reports;
	// }


	// public function repair() {
	// 	$this->repair = false;
	// 	// Same childs
	// 	$list = new ArrayCollection();
	// 	foreach ($this->entity->getChilds() as $child) {
	// 		if(!$list->contains($child)) $list->add($child);
	// 			else $this->entity->removeChild($child);
	// 	}
	// 	// FLUSH
	// 	$this->container->get(serviceEntities::class)->getEntityManager()->flush();
	// 	// Retest
	// 	if($this->hasErrors(true)) {
	// 		$this->container->get(serviceFlashbag::class)->addFlashSweet('error', 'L\'entité '.json_encode($this->entity->getName()).' a tentée d\'être réparée.<br>Des erreurs persistent.<br>Recommencez si nécessaire.');
	// 	} else {
	// 		$this->container->get(serviceFlashbag::class)->addFlashSweet('success', 'L\'entité '.json_encode($this->entity->getName()).' a été réparée.<br>Le nouveau rapport ne contient plus d\'erreur.');
	// 	}
	// 	return $this->reports;
	// }

}