<?php
namespace ModelApi\BaseBundle\Component;

// use Doctrine\ORM\Mapping as ORM;
// use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;

use ModelApi\BaseBundle\Service\servicePreference;
use ModelApi\BaseBundle\Service\serviceTools;

use \Exception;

class Preference {

	protected $name;
	protected $value;
	protected $type;
	protected $parent;
	protected $path;

	public function __construct($name, $value, $type = null, Preference $parent = null) {
		$this->type = null;
		$this->setParent($parent);
		$this->setName($name);
		$this->setValue($value, false);
		$this->computePath();
		if(is_string($type)) $this->convertType($type);
		return $this;
	}


	public function __toString() {
		return $this->getName();
	}



	public static function getBaseName() {
		return servicePreference::BASE_NAME;
	}

	public static function getNewName() {
		return servicePreference::NEW_NAME;
	}

	public static function getOriginName() {
		return servicePreference::ORIGIN_NAME;
	}

	public static function getCreateName() {
		return servicePreference::CREATE_NAME;
	}


	/*************************************************************************************/
	/*** PATH
	/*************************************************************************************/

	protected function computePath() {
		$parent = $this;
		$this->path = [$parent->getName()];
		while (null !== $parent = $parent->getParent()) array_unshift($this->path, $parent->getName());
		return $this->path;
	}

	public function getPath($removeRoot = false) {
		$path = $this->computePath();
		if($removeRoot) array_shift($path);
		return implode(servicePreference::PREFS_PATH_SEPARATOR, $path);
	}

	public function getPathSlached($removeRoot = false) {
		$path = $this->getPath($removeRoot);
		$prefix = $removeRoot ? servicePreference::PREFS_PATH_SEPARATOR : '';
		return static::getSlashedPath($prefix.$path);
	}

	public static function getSlashedPath($path) {
		return preg_replace('/'.serviceTools::escapeCharsForStringPregTest(servicePreference::PREFS_PATH_SEPARATOR).'/', DIRECTORY_SEPARATOR, $path);
	}

	public function getPreferenceByPath($path = null, $separator = null) {
		if(empty($path)) return $this;
		if(!is_array($path)) {
			if(!is_string($separator)) $separator = servicePreference::PREFS_PATH_SEPARATOR;
			$path = explode($separator, $path);
		}
		if($this->isRoot() && reset($path) === $this->getName()) {
			$path = array_slice($path, 1);
			if(count($path) < 1) return $this;
		}
		// if(count($path) < 1) return $this;
		if($this->hasChilds()) {
			foreach ($this->getChilds() as $child) {
				if(reset($path) === $child->getName()) return $child->getPreferenceByPath(array_slice($path, 1), $separator);
			}
		}
		return null;
	}


	/*************************************************************************************/
	/*** TYPE
	/*************************************************************************************/

	protected function isForChildable($var) {
		if($this->getGenericType($var) !== 'array') return false;
		if($this->isTypeValueData($var)) return false;
		foreach ($var as $key => $value) {
			if(is_string($key)) return true;
		}
		return false;
	}

	protected function isTypeValueData($var) {
		if($this->getGenericType($var) !== 'array') return false;
		$keys = array_keys($var);
		sort($keys, SORT_STRING);
		if(count($var) === 4 && implode('|', $keys) === 'path|type|value|values') return true;
		if(count($var) === 3 && implode('|', $keys) === 'path|type|value') return true;
		if(count($var) === 2 && implode('|', $keys) === 'type|value') return true;
		return false;
	}

	public function getGenericType($var = null) {
		if($var instanceOf Preference) return "preference";
		if($var instanceOf ArrayCollection) return "childable";
		if (is_array($var)) return "array";
		if (is_string($var)) return "string";
		if (is_bool($var)) return "boolean";
		if (is_int($var)) return "integer";
		if (is_float($var)) return "float";
		if (is_numeric($var)) return "numeric";
		if (is_object($var)) return "object";
		if (is_resource($var)) return "resource";
		if (is_callable($var)) return "callable";
		if (is_null($var)) return "NULL";
		return "unknown type";
	}

	public function isChildableType($var = null) {
		return $this->getGenericType($var) === 'childable';
	}

	public function getValidTypes() {
		return ['array','string','integer','float','numeric','boolean','childable','NULL','preference'];
	}

	public function isValidType($var = null) {
		$validTypes = $this->getValidTypes();
		return in_array($this->getGenericType($var), $validTypes);
	}

	public function getNeutralTypes() {
		return ['NULL'];
	}

	public function isNeutralType($var = null) {
		$neutralTypes = $this->getNeutralTypes();
		return in_array($this->getGenericType($var), $neutralTypes);
	}

	public function getInvalidTypes() {
		return ['object','resource','callable','unknown type'];
	}

	public function isInvalidType($var = null) {
		$invalidTypes = $this->getInvalidTypes();
		return in_array($this->getGenericType($var), $invalidTypes);
	}

	public function getCompatibleTypes() {
		return $this->isNeutralType($this->getValue()) ? $this->getValidTypes() : array_unique(array_merge([$this->getType()], $this->getNeutralTypes()));
	}

	public function isCompatibleType($var = null) {
		if($this->isBoolean() && servicePreference::isEquivToBoolean($var)) return true;
		if($this->isArray() && is_string($var)) return true;
		$compatibleTypes = $this->getCompatibleTypes();
		return in_array($this->getGenericType($var), $compatibleTypes);
	}

	public function getType() {
		$this->type = $this->getGenericType($this->value);
		return $this->type;
	}

	protected function setType($type) {
		$compatibleTypes = $this->getCompatibleTypes();
		if(!in_array($type, $compatibleTypes)) throw new Exception("Error ".__METHOD__.": type ".json_encode($type)." is not compatible (compatible types are: ".json_encode($compatibleTypes).")!", 1);
		$this->type = $type;
		return $this;
	}

	public function isChildable() {
		return $this->getType() === 'childable';
	}

	public function isArray() {
		return $this->getType() === 'array';
	}

	public function isString() {
		return $this->getType() === 'string';
	}

	public function isInteger() {
		return $this->getType() === 'integer';
	}

	public function isNumeric() {
		return $this->getType() === 'numeric';
	}

	public function isFloat() {
		return $this->getType() === 'float';
	}

	public function isBoolean() {
		return $this->getType() === 'boolean';
	}


	/*************************************************************************************/
	/*** NAME
	/*************************************************************************************/

	public function setName($name) {
		if(is_integer($name)) $name = (string)$name;
		if(!is_string($name)) throw new Exception("Error ".__METHOD__.": name ".json_encode($name)." is not a string!", 1);
		$this->name = $name;
		return $this;
	}

	public function getName() {
		return $this->name;
	}


	/*************************************************************************************/
	/*** VALUE
	/*************************************************************************************/

	// public function getValue() {
	// 	return $this->value;
	// }

	public function getValue($path = null, $defaultIfNotFound = null) {
		if(empty($path)) return $this->value;
		$preference = $this->getPreferenceByPath($path);
		return $preference instanceOf Preference ? $preference->getValue() : $defaultIfNotFound;
	}

	public function setValue($value, $control = true) {
		$type = $this->getGenericType($value);
		if($control) {
			if(!$this->isCompatibleType($value)) throw new Exception("Error ".__METHOD__.": value of type ".json_encode($type)." has not a valid type! Valid types for this Preference are ".json_encode($this->getCompatibleTypes()), 1);
			// if($type !== $this->getType()) throw new Exception("Error ".__METHOD__.": value of type ".json_encode($type)." must be of type ".json_encode($this->getType())."!", 1);
		}
		switch ($type) {
			case 'array':
				if($this->isForChildable($value)) {
					$childable = new ArrayCollection();
					foreach ($value as $name => $val) {
						$type = $this->getType($val);
						$childable->set($name, new Preference($name, $val, $type, $this));
					}
					$value = $childable;
				} else if($this->isTypeValueData($value)) {
					// $type = $value['type'];
					$value = $value['value'];
					// compatibility with old version
					if(in_array($this->getName(), ['newsletter','help','cookies'])) $value = (boolean)$value;
					if($this->getGenericType($value) === 'array') return $this->setValue($value, false);
				}
				break;
			case 'integer':
				if($this->isBoolean() && in_array($value, [0,1])) $value = (boolean)$value;
				break;
			case 'string':
				if($this->isBoolean()) {
					$value = servicePreference::equivToBoolean($value);
				}
				if($this->isArray()) {
					servicePreference::stringToArray($value);
				}
				break;
			case 'NULL':
				if($this->isBoolean()) $value = false;
				if($this->isArray()) $value = [];
				break;
		}
		$this->value = $value;
		return $this->getType();
	}


	/*************************************************************************************/
	/*** TO ARRAY
	/*************************************************************************************/

	public function toArray($simple = false) {
		if($simple) {
			if($this->isChildable()) {
				$array = [];
				foreach ($this->getChilds() as $name => $child) $array[(string)$name] = $child->toArray($simple);
			} else {
				$array = $this->getValue();
			}
		} else {
			if($this->isChildable()) {
				$array = [
					'type' => $this->getTypeForArray(),
					'value' => [],
					'path' => $this->getPath(),
				];
				foreach ($this->getChilds() as $name => $child) $array['value'][(string)$name] = $child->toArray($simple);
			} else {
				$array = [
					'type' => $this->getType(),
					'value' => $this->getValue(),
					'path' => $this->getPath(),
				];
			}
		}
		return $array;
	}

	public function getTypeForArray() {
		switch ($this->type) {
			case 'childable': return 'array'; break;
			default: return $this->type; break;
		}
	}


	/*************************************************************************************/
	/*** PARENT
	/*************************************************************************************/

	public function setParent(Preference $parent = null, $inverse = true) {
		if($this->parent === $parent) return $this;
		if($this->parent instanceOf Preference) $this->parent->removeChild($this, false);
		$this->parent = $parent;
		if($inverse && $this->parent instanceOf Preference) $this->parent->addChild($this, null, false);
		return $this;
	}

	public function getParent() {
		return $this->parent;
	}

	public function getParents() {
		$parents = [];
		$parent = $this;
		while (null !== $parent = $parent->getParent()) {
			$parents[] = $parent;
			if(count($parents) > 1000) throw new Exception("Error ".__METHOD__."(): infinite loop!", 1);
		}
		return $parents;
	}

	public function isRoot() {
		return empty($this->parent);
	}

	public function getLevel() {
		$parents = $this->getParents();
		return count($parents);
	}

	public function isLevel($level) {
		$parents = $this->getParents();
		return count($parents) === $level;
	}


	/*************************************************************************************/
	/*** CHILDREN
	/*************************************************************************************/

	public function getChilds() {
		if(!$this->isChildable()) return [];
		return $this->value;
	}

	public function childCount() {
		return $this->isChildable() ? count($this->value) : 0;
	}

	public function childEmpty() {
		return $this->childCount() <= 0;
	}

	public function hasChilds() {
		return $this->childCount() > 0;
	}

	public function addChild($value, $name = null, $inverse = true) {
		if(!$this->isChildable()) $this->convertType('array');
		if(!($value instanceOf Preference)) {
			if(empty($name)) $name = 0;
			$value = new Preference($name, $value, null, $this);
		}
		$array = $this->getValue();
		$array[$value->getName()] = $value;
		return $this;
	}

	public function removeChild(Preference $child, $inverse = true) {
		if(!$this->isChildable()) return $this;
		$this->value = array_filter($this->value, function($item) use ($child, $inverse) {
			if($item === $child) {
				if($inverse) $item->setParent(null, false);
				return false;
			}
			return true;
		});
		return $this;
	}


	/*************************************************************************************/
	/*** CONVERT
	/*************************************************************************************/

	public function convertType($type) {
		if(!$this->isValidType($type)) throw new Exception("Error ".__METHOD__.": type ".json_encode($type)." for convert is not valid.", 1);
		$current = $this->getType();
		if($current === $type) return $this;
		switch ($current) {
			case 'childable':
				switch ($type) {
					// case 'array':
					// 	break;
					case 'string':
						$this->setValue(json_encode($this->getValue()), false);
						break;
					case 'integer':
						$count = count($this->getValue());
						$this->setValue((integer)$count, false);
						break;
					case 'float':
						$count = count($this->getValue());
						$this->setValue((float)$count, false);
						break;
					case 'boolean':
						$count = count($this->getValue()) > 0;
						$this->setValue((boolean)$count, false);
						break;
				}
			case 'array':
				switch ($type) {
					// case 'array':
					// 	break;
					case 'string':
						$this->setValue(json_encode($this->getValue()), false);
						break;
					case 'integer':
						$count = count($this->getValue());
						$this->setValue((integer)$count, false);
						break;
					case 'float':
						$count = count($this->getValue());
						$this->setValue((float)$count, false);
						break;
					case 'boolean':
						$count = count($this->getValue()) > 0;
						$this->setValue((boolean)$count, false);
						break;
				}
				break;
			case 'string':
				switch ($type) {
					case 'array':
						$this->setValue([$this->getName() => new Preference($this->getName(), $this->getValue(), $type, $this)], false);
						break;
					// case 'string':
					// 	break;
					case 'integer':
						$this->setValue((integer)$this->getValue(), false);
						break;
					case 'float':
						$this->setValue((float)$this->getValue(), false);
						break;
					case 'boolean':
						$this->setValue((boolean)$this->getValue(), false);
						break;
				}
				break;
			case 'integer':
				switch ($type) {
					case 'array':
						$this->setValue([$this->getName() => new Preference($this->getName(), $this->getValue(), $type, $this)], false);
						break;
					case 'string':
						$this->setValue((string)$this->getValue(), false);
						break;
					// case 'integer':
					// 	break;
					case 'float':
						$this->setValue((float)$this->getValue(), false);
						break;
					case 'boolean':
						$this->setValue((boolean)$this->getValue(), false);
						break;
				}
				break;
			case 'float':
				switch ($type) {
					case 'array':
						$this->setValue([$this->getName() => new Preference($this->getName(), $this->getValue(), $type, $this)], false);
						break;
					case 'string':
						$this->setValue((string)$this->getValue(), false);
						break;
					case 'integer':
						$this->setValue((integer)$this->getValue(), false);
						break;
					// case 'float':
					// 	break;
					case 'boolean':
						$this->setValue((boolean)$this->getValue(), false);
						break;
				}
				break;
			case 'boolean':
				switch ($type) {
					case 'array':
						$this->setValue([$this->getName() => new Preference($this->getName(), $this->getValue(), $type, $this)], false);
						break;
					case 'string':
						$this->setValue((string)$this->getValue(), false);
						break;
					case 'integer':
						$this->setValue((integer)$this->getValue(), false);
						break;
					case 'float':
						$this->setValue((float)$this->getValue(), false);
						break;
					// case 'boolean':
					// 	break;
				}
				break;
		}
		// $new_type = $this->getType();
		// if($type !== $new_type) throw new Exception("Error creating Preference: type ".json_encode($type)." is not valid (".json_encode($new_type)." expected)!", 1);
		return $this;
	}


}