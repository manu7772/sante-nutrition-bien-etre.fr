<?php
namespace ModelApi\BaseBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceClasses;
use Doctrine\Common\Inflector\Inflector;

use \DateTime;

trait DataFiller {

	/*************************************************************************************/
	/*** FILL WITH DATA
	/*************************************************************************************/

	/**
	 * Get corresponding attribute name
	 * @return string
	 */
	public static function getFieldname($attribute) {
		$attribute = Inflector::camelize($attribute);
		switch ($attribute) {
			case 'text': return 'name'; break;
		}
		return $attribute;
	}

	/**
	 * Get forbidden fields for fill operation
	 * @return array
	 */
	public static function getFillForbiddens() {
		$basics = ['id','classname','shortname','children','children_d','parent','parents','original','state','type','data','a_attr','li_attr'];
		foreach ($basics as $basic) {
			$basic = Inflector::camelize($basic);
			if(!in_array($basic, $basics)) $basics[] = $basic;
		}
		return $basics;
	}

	/**
	 * Is field forbidden for fill operation
	 * @return boolean
	 */
	public static function isFillForbidden($attribute) {
		$forbiddens = static::getFillForbiddens();
		$attribute = static::getFieldname($attribute);
		return in_array($attribute, $forbiddens);
	}

	/**
	 * Is field authorized for fill operation
	 * @return boolean
	 */
	public static function isFillAuthorized($attribute) {
		$forbiddens = static::getFillForbiddens();
		$attribute = static::getFieldname($attribute);
		return !in_array($attribute, $forbiddens);
	}

	/**
	 * Set error message while filled
	 * @param string $message = null
	 * @return self
	 */
	public function setFilledError($message = null) {
		$this->filledError = $message;
		return $this;
	}

	/**
	 * Get message if error while filled. If no error, returns null
	 * @return string | null
	 */
	public function getFilledError() {
		return isset($this->filledError) ? $this->filledError : null;
	}

	/**
	 * Fill entity with data
	 * @param array $data
	 * @param string $preferedSetterPrefix = 'add'
	 * @param boolean $authorizeDirectAttribution = true
	 * @return self
	 */
	public function fillWithData($data, $preferedSetterPrefix = 'add', $authorizeDirectAttribution = true) {
		$this->filledError = null;
		$forbiddens = $this->getFillForbiddens();
		foreach ($data as $attribute => $value) {
			$fieldName = $this->getFieldname($attribute);
			if(static::isFillAuthorized($fieldName)) {
				$method = serviceClasses::getPropertyMethod($fieldName, $this, $preferedSetterPrefix);
				// if(is_string($method)) { echo('<pre><h3>'.$attribute.' = '.$fieldName.' = '.$method.'()</h3>');var_dump($value);echo('</pre>'); }
				switch ($fieldName) {
					case 'parentLinks':
						# code...
						break;
					default:
						if(is_string($method)) {
							if(preg_match('/^add/i', $method)) {
								if(is_array($value)) foreach ($value as $key => $subvalue) $this->$method($subvalue);
									else $this->$method($value);
							} else if(preg_match('/^set/i', $method)) {
								$this->$method($value);
							}
						} else if($authorizeDirectAttribution && isset($this->$fieldName)) {
							$this->$fieldName = $value;
						} else {
							// Setter not found
						}
						break;
				}
			}
		}
		return $this;
	}


}