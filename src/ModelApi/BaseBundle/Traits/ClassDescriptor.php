<?php
namespace ModelApi\BaseBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolation;
// BaseBundle
use ModelApi\BaseBundle\Entity\LaboClassMetadata;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
// FileBundle
use ModelApi\FileBundle\Entity\Fileformat;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\YamlLog;
use ModelApi\DevprintsBundle\Service\DevTerminal;

use \ReflectionClass;
use \ReflectionProperty;
use \Exception;

trait ClassDescriptor {

	/**
	 * @var string
	 * @ORM\Column(name="microtimeid", type="string", nullable=false, unique=false)
	 */
	private $microtimeid;

	/**
	 * Class short name
	 * @ORM\Column(name="shortname", type="string", nullable=true, unique=false)
	 * @var string
	 */
	protected $shortname;

	/**
	 * Classname
	 * @var string
	 */
	protected $classname;

	/**
	 * Instances
	 * @var array
	 */
	protected $instances;

	/**
	 * Child subclasses
	 * @var array
	 */
	protected $subclasses;

	/**
	 * Interfaces
	 * @var array
	 */
	protected $interfaces;

	/**
	 * Is instance of interface
	 * @var array
	 */
	protected $instanceOrInterface;

	/**
	 * Tomcat for Captcha
	 * @var mixed
	 * @Annot\Tomcat()
	 */
	protected $tomcat = false;

	/**
	 * Traits
	 * @var array
	 */
	protected $traits;

	// /**
	//  * LifecycleEventArgs
	//  * @var LifecycleEventArgs
	//  */
	// protected $_eventArgs;

	protected $postLoadEventsLoaded = false;
	protected $PostNewEventsLoaded = false;
	protected $deepControl = 'auto';
	// protected $publicFiltered = false;
	protected $serviceKernel = null;
	protected $modeContext = 'default';


	/**
	 * @Annot\PostCreate()
	 */
	public function init_ClassDescriptor() {
		$this->getMicrotimeid();
		$this->updateShortname();
		// $this->forceValidateTomcat();
		// $this->setDeepControl(false);
		// $this->setPublicFiltered(false);
		return $this;
	}

	public function isFacticee() {
		return false;
	}

	public function getTomcat() {
		return $this->tomcat;
	}

	public function isTomcat() {
		return $this->tomcat !== false;
	}

	public function setTomcat($tomcat) {
		if(is_string($tomcat) && preg_match('/^#JSON#___/', $tomcat)) {
			$test = json_decode(preg_replace('/^#JSON#___/', '', $tomcat), true);
			$this->tomcat = json_last_error() === JSON_ERROR_NONE ? $test : $tomcat;
		} else {
			$this->tomcat = $tomcat;
		}
		return $this;
	}

	public function forceValidateTomcat() {
		$this->tomcat = ['verified' => true];
	}

	public function isTomcatValid() {
		return true;
		// if(!is_array($this->tomcat)) throw new Exception("Error Tomcat is ".gettype($this->tomcat)." > ".json_encode($this->tomcat), 1);
		if(is_bool($this->tomcat)) $this->tomcat;
		if(!is_array($this->tomcat)) return true;
		return isset($this->tomcat['verified']) && $this->tomcat['verified'];
	}

	// public function isAdminvalidated() {
	// 	return method_exists($this, 'getAdminvalidated') ? $this->getAdminvalidated() : true;
	// }

	/**
	 * Get Id or microtimeid (get an Id anyway)
	 * @return string (if microtime id) | integer (if id)
	 */
	public function getIdAnyway() {
		return null == $this->getId() ? $this->getMicrotimeid() : $this->getId();
	}

	/**
	 * Get microtime Id
	 * @return integer 
	 */
	public function getMicrotimeid() {
		// if(!is_string($this->microtimeid)) $this->microtimeid = serviceTools::getMicrotimeid();
		if(!is_string($this->microtimeid)) $this->microtimeid = serviceTools::geUniquid();
		return $this->microtimeid;
	}

	public function getConstantValue($name) {
		if(!is_string($name)) throw new Exception("Error ".__METHOD__."(): constant has to be string, ".gettype($name)." given!", 1);
		if(defined('static::'.$name)) return constant('static::'.$name);
		if(defined('static::'.strtoupper($name))) return constant('static::'.strtoupper($name));
		throw new Exception("Error ".__METHOD__."(): constant ".json_encode($name)." is not defined in ".$this->__toString()." of class ".$this->getClassname()."!", 1);
	}

	/*************************/
	/*** SERVICE NAME      ***/
	/*************************/

	public static function getService() {
		if(defined('static::ENTITY_SERVICE')) return static::ENTITY_SERVICE;
		return null;
	}


	/*************************/
	/*** BDD IDENTIFYERS   ***/
	/*************************/

	public function getBddIdentifyer() {
		if(!isset($this->id)) return null;
		return null === $this->id ? null : serviceEntities::getEntityBddid($this);
	}

	public function getBddid() {
		return $this->getBddIdentifyer();
	}

	public function getBddMcid() {
		return serviceEntities::getEntityBddmcid($this);
	}

	/*************************/
	/*** CLASS NAMES       ***/
	/*************************/

	/**
	 * Get class short name
	 * @return string
	 */
	public function getShortname($lowercase = false) {
		if(empty($this->shortname)) $this->updateShortname();
		return $lowercase ? strtolower($this->shortname) : $this->shortname;
	}

	/**
	 * Update shortname
	 * @return self
	 */
	public function updateShortname() {
		$this->shortname ??= serviceClasses::getShortname(static::class);
		return $this;
	}

	/**
	 * Get classname
	 * @return string
	 */
	public function getClassname() {
		// $class = new ReflectionClass(get_called_class());
		if(isset($this->classname) && !empty($this->classname)) return $this->classname;
		$class = new ReflectionClass(static::class);
		return $this->classname = serviceEntities::removeProxyPrefix($class->getName());
	}

	public function getSubclasses($asShrortname = true) {
		return $this->subclasses[json_encode($asShrortname)] ??= serviceClasses::getSubclasses($this, $asShrortname);
	}

	/**
	 * Get instances of Entity
	 * @return array
	 */
	public function getInstances($asShrortname = true) {
		return $this->instances[json_encode($asShrortname)] ??= serviceClasses::getInstances($this, $asShrortname);
	}

	/**
	 * Get traits of Entity
	 * @return array
	 */
	public function getTraitNames($asShrortname = true) {
		return $this->traits[json_encode($asShrortname)] ??= serviceClasses::getTraitNames($this, $asShrortname);
	}

	/**
	 * Get interfaces of Entity
	 * @return array
	 */
	public function getInterfaces($asShrortname = true) {
		return $this->interfaces[json_encode($asShrortname)] ??= serviceClasses::getInterfaces($this, $asShrortname);
	}

	public function isInstance($instanceNames) {
		if(!is_array($instanceNames)) $instanceNames = [$instanceNames];
		$instances = array_merge($this->getInstances(true), $this->getInstances(false));
		return count(array_intersect($instanceNames, $instances)) > 0;
	}

	public function isInterface($interfaceNames) {
		if(!is_array($interfaceNames)) $interfaceNames = [$interfaceNames];
		$interfaces = array_merge($this->getInterfaces(true), $this->getInterfaces(false));
		return count(array_intersect($interfaceNames, $interfaces)) > 0;
	}

	public function isInstanceOrInterface($instanceNames) {
		return $this->instanceOrInterface[json_encode($instanceNames)] ??= $this->isInstance($instanceNames) || $this->isInterface($instanceNames);
	}

	public function isInstanceOf($classes) {
		return $this->isInstanceOrInterface($classes);
		// if(is_string($classes)) $classes = [$classes];
		// foreach ($classes as $class) {
		// 	if($this->isInstanceOrInterface($class)) return true;
		// }
		// return false;
	}


	/*************************************************************************************/
	/*** POST LOAD/NEW EVENTS LOADED
	/*************************************************************************************/

	public function setPostLoadEventsLoaded($postLoadEventsLoaded = true) {
		$this->postLoadEventsLoaded = $postLoadEventsLoaded;
		return $this;
	}

	public function getPostLoadEventsLoaded() {
		return $this->postLoadEventsLoaded ?? false;
	}

	public function isPostLoadEventsLoaded() {
		return $this->getPostLoadEventsLoaded();
	}



	public function setPostNewEventsLoaded($postNewEventsLoaded = true) {
		$this->postNewEventsLoaded = $postNewEventsLoaded;
		return $this;
	}

	public function getPostNewEventsLoaded() {
		return $this->postNewEventsLoaded ?? false;
	}

	public function isPostNewEventsLoaded() {
		return $this->getPostNewEventsLoaded();
	}


	/**
	 * USE THIS IN ADDMODELTRANSFORMER WITH NEW ENTITIES
	 * @return boolean
	 */
	public function needPostNewEventsLoad() {
		if(!empty($this->id ?? null)) return false;
		return !$this->isPostNewEventsLoaded() && !$this->isPostLoadEventsLoaded();
	}


	/*************************************************************************************/
	/*** SERVICE KERNEL
	/*************************************************************************************/

	public function setServiceKernel(serviceKernel $serviceKernel) {
		$this->serviceKernel = $serviceKernel;
		return $this;
	}

	/*************************************************************************************/
	/*** PROCESS CONTROL
	/*************************************************************************************/

	public function setDeepControl($deepControl) {
		if(static::SHORTCUT_CONTROLS) {
			$this->deepControl = false;
			return $this;
		}
		$this->deepControl = is_bool($deepControl) ? $deepControl : 'auto';
		return $this;
	}

	public function isDeepControl() {
		if(static::SHORTCUT_CONTROLS) return false;
		if(is_bool($this->deepControl)) return $this->deepControl;
		return $this->serviceKernel instanceOf serviceKernel ? !$this->isPublicFiltered() && $this->serviceKernel->isDev() : true;
	}

	/*************************************************************************************/
	/*** BUNDLE/ENV CONTEXT
	/*************************************************************************************/

	public function isPublicFiltered() {
		return $this->serviceKernel instanceOf serviceKernel ? $this->serviceKernel->isPublicBundle() : false;
	}

	/*************************************************************************************/
	/*** MODE CONTEXT
	/*************************************************************************************/

	public function setModeContext($modeContext) {
		$this->modeContext = strtolower($modeContext);
		return $this;
	}

	public function getModeContext() {
		return $this->modeContext ?? 'default';
	}

	public function isDefaultModeContext() {
		return strtolower($this->getModeContext()) === 'default';
	}

	public function isCheckModeContext() {
		return strtolower($this->getModeContext()) === 'check';
	}

	public function isDeepControlOrCheckModeContext() {
		return $this->isDeepControl() || $this->isCheckModeContext();
	}

	// public function isDeepControlOrNotDefaultModeContext() {
	// 	return $this->isDeepControl() || !$this->isDefaultModeContext();
	// }


	/*************************/
	/*** MENU              ***/
	/*************************/

	public function isMenuable() {
		return false;
	}


	/*************************/
	/*** POST              ***/
	/*************************/

	public function postNew(LifecycleEventArgs $args) {
		$args->getEntityManager()->getEventManager()->dispatchEvent(BaseAnnotation::postNew, new LifecycleEventArgs($this, $args->getEntityManager()));
		return $this;
	}

	// public function postLoad_getArgs(LifecycleEventArgs $args) {
	// 	$this->_eventArgs = $args->getEntityManager();
	// 	return $this;
	// }


	/*************************************************************************************/
	/*** CLONE
	/*************************************************************************************/

	public function __clone() {
		$this->doClone();
	}

	protected function doClone() {
		$isTranslatable = serviceClasses::getClassAnnotation($this, HasTranslatable::class);
		if(!empty($hasTranslatable)) {
			$this->_tm = clone $this->_tm; // clone managedEntity!!!
			// $this->_tm->translate();
			// $propertys = ['menutitle'];
			// foreach ($propertys as $property) {
			// 	if(is_string($this->$property)) $this->$property = $this->$property.'_copy';
			// 	if(is_array($this->$property)) foreach ($this->$property as $language => $text) {
			// 		$this->$property[$language] = $text.'_copy';
			// 	}
			// }
		}
		if(isset($this->inputname)) {
			$this->setInputname($this->getInputname()."_copy");
		} else if(isset($this->name)) {
			$this->setName($this->getName()."_copy");
		}
	}



	/*************************/
	/*** PRINT VALIDITY    ***/
	/*************************/

	public function getConstraintViolationList($excludes = []) {
		if(!($this->serviceKernel instanceOf serviceKernel)) return new ArrayCollection();
		return $this->serviceKernel->getErrorsReport($this, $excludes);
	}

	public function validityEntityReport($message = null, $line = null, $method = null, $lauchException = true, $completeCheck = true) {
		$line ??= __LINE__;
		$method ??= __METHOD__;
		// Get errors report
		// Complete ou partial check
		$constraintViolationList = $this->getConstraintViolationList($completeCheck ? [] : ['name','inputname']);
		// Lauch Exception if violations
		if($constraintViolationList->count() > 0) {
			if(!$this->isDefaultModeContext()) {
				$this->printItem();
				// DevTerminal::validationException($this, $message, $line, $method, $lauchException);
			}
			$message ??= 'Errors list ('.$constraintViolationList->count().'):';
			$errors = '';
			$key = 1;
			foreach ($constraintViolationList->checklist as $property_name => $data) {
				if($data['constraintViolation'] instanceOf ConstraintViolation) {
					$new_line = PHP_EOL.$key++.'. ***** '.$data['label'].' ***** => message : '.$data['constraintViolation']->getMessage().' ('.json_encode($data['constraintViolation']->getPropertyPath()).')';
				} else {
					$new_line = PHP_EOL.$key++.'. - '.$data['label'].' => VALID';
				}
				if(is_object($data['value'])) {
					if(is_iterable($data['value'])) {
						$value = '['.get_class($data['value']).'] (items: '.count($data['value']).')';
					} else {
						$value = '['.get_class($data['value']).']'.(method_exists($data['value'], 'getName') ? ' '.json_encode($data['value']->getName()) : '');
					}
				} else {
					if(is_iterable($data['value'])) {
						$value = '['.gettype($data['value']).'] (items: '.count($data['value']).')';
					} else {
						$value = '['.gettype($data['value']).'] '.json_encode($data['value']);
					}					
				}
				$new_line .= PHP_EOL.'- Property '.json_encode($property_name).' value ('.(true === $data['getter'] ? 'P' : 'G').') = '.$value.'.';
				$errors .= $new_line;
			}
			if($lauchException) throw new Exception("Error line ".$line." on ".$method."(): ".PHP_EOL."On entity ".$this->getShortname()." ".json_encode($this->getName())." ".(empty($this->getId()) ? '(created)' : '(updated)')."".PHP_EOL.$message.$errors, 1);
		}
		return $constraintViolationList;
	}

	public function getRawValue($name) {
		$RC = new ReflectionClass($this->getClassname());
		foreach ($RC->getProperties() as $reflectionProperty) {
			if($reflectionProperty->name === $name) return $this->$name;
		}
		return 'FAILED TO GET PROPERTY VALUE';
 	}


	/*************************/
	/*** LOG REPORT        ***/
	/*************************/

	// public function EntityReportLog() {
	// 	// YAML LOG REPORT
	// 	if($this->isInstance('Fileformat')) {
	// 		$report = [
	// 			'CLASSNAME' => $this->getClassname(),
	// 			'SHORTNAME' => $this->getShortname(),
	// 			'Name' => $this->getName(),
	// 			'Extension' => $this->getExtension(),
	// 			'MediaType' => $this->getMediaType(),
	// 			'Subtype' => $this->getSubtype(),
	// 			'ContentType' => $this->getContentType(),
	// 			'Enabled' => $this->getenabled(),
	// 		];
	// 		$result = true;
	// 	}
	// 	if($this->isInstance('UploadInterface')) {
	// 		$report = [
	// 			'CLASSNAME' => $this->getClassname(),
	// 			'SHORTNAME' => $this->getShortname(),
	// 			'DETECTED' => $this->stream_type_while_upload,
	// 			'FileName' => $this->getFileName(),
	// 			'OriginalFileName' => $this->getOriginalFileName(),
	// 			'FileExtension' => $this->getFileExtension(),
	// 			'FileSize' => $this->getFileSize(),
	// 			'FileUrls' => count($this->getFileUrls()) && is_string($this->getFileUrl()),
	// 			'Fileformat' => $this->getFileformat() instanceOf Fileformat,
	// 		];
	// 		$result = $this->isValid();
	// 	}
	// 	if($this->isInstance('File')) {
	// 		$report = [
	// 			'CLASSNAME' => $this->getClassname(),
	// 			'SHORTNAME' => $this->getShortname(),
	// 			'FileExtension' => $this->getFileExtension(),
	// 			'FileSize' => $this->getFileSize(),
	// 			'Mediatype' => $this->getMediaType(),
	// 			'Subtype' => $this->getSubtype(),
	// 			'Nbversions' => $this->getNbversions(),
	// 			'FileUrls' => count($this->getFileUrls()) && is_string($this->getFileUrl()),
	// 			'Name' => $this->getName(),
	// 			'Fileformat' => $this->getFileformat() instanceOf Fileformat,
	// 		];
	// 		$result = [
	// 			'contentTypeSupported' => $this->isContentTypeSupported(),
	// 			'contentTypeValid' => $this->isContentTypeValid(),
	// 		];
	// 	}
	// 	if(isset($result) && isset($report)) YamlLog::registerYamlLogfile(__METHOD__, $result, $report);
	// 	// YAML LOG REPORT
	// 	return $this;
	// }


}