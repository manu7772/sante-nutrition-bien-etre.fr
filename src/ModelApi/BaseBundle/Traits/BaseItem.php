<?php
namespace ModelApi\BaseBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
// Gedmo
use Gedmo\Mapping\Annotation as Gedmo;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;

trait BaseItem {


	/**
	 * @var string
	 * Nom de l'élément - Informatif, n'apparaît pas dans public
	 * @ORM\Column(name="name", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(show=false, update=false, type="TextType", order=1, options={"by_reference"=false, "required"=true}, contexts={"simple"})
	 * @CRUDS\Update(show=false, update=false, type="TextType", order=1, options={"by_reference"=false, "required"=true}, contexts={"simple"})
	 * @CRUDS\Show(role="ROLE_TRANSLATOR")
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(name="slug", type="string", length=128, unique=true, nullable=false)
	 * @CRUDS\Create(show=false, update=false, type="TextType")
	 * @CRUDS\Update(show=false, update=false, type="TextType")
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN")
	 * @Gedmo\Slug(fields={"name"})
	 */
	protected $slug;

	/**
	 * @var string
	 * Nom de l'élément - utilisé également pour la balise <title>
	 * @ORM\Column(name="title", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="TextType", order=2, options={"by_reference"=false, "required"=true, "description"="<strong>Title de la page</strong> apparaissant dans le titre <i>(Balise &lt;H1&gt;)</i> et l'onglet du navigateur. Important pour le référencement."}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="TextType", order=2, options={"by_reference"=false, "required"=true, "description"="<strong>Title de la page</strong> apparaissant dans le titre <i>(Balise &lt;H1&gt;)</i> et l'onglet du navigateur. Important pour le référencement."}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $title;

	/**
	 * @var string
	 * Sous-titre de l'élément - utilisé en supplément de title
	 * Texte de 1 à 3 lignes
	 * @ORM\Column(name="subtitle", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=4, options={"by_reference"=false, "required"=false, "description"="Sous-titre, utilisé sous le titre de la page <i>(Balise &lt;H2&gt;)</i>"}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=4, options={"by_reference"=false, "required"=false, "description"="Sous-titre, utilisé sous le titre de la page <i>(Balise &lt;H2&gt;)</i>"}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $subtitle;

	/**
	 * @var string
	 * Nom court de l'élément - utilisé pour l'affichage notamment dans les menus
	 * @ORM\Column(name="menutitle", type="string", nullable=false, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="TextType", order=5, options={"by_reference"=false, "required"=false, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus qui ne disposent que de peu de place."}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="TextType", order=5, options={"by_reference"=false, "required"=false, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus qui ne disposent que de peu de place."}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $menutitle;

	/**
	 * @var string
	 * Accroche, slogan de l'élément
	 * Texte de 1 ligne
	 * @ORM\Column(name="accroche", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=6, options={"by_reference"=false, "required"=false, "description"="Texte d'accroche, affiché avant le texte principal de la page. Mais paut aussi être utilisé dans un lien URL externe à cette page pour attirer l'attention et inciter à cliquer pour consulter le contenu de la page."}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=6, options={"by_reference"=false, "required"=false, "description"="Texte d'accroche, affiché avant le texte principal de la page. Mais paut aussi être utilisé dans un lien URL externe à cette page pour attirer l'attention et inciter à cliquer pour consulter le contenu de la page."}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $accroche;

	/**
	 * @var string
	 * Description courte de l'élément
	 * Texte de 1 à 3 lignes
	 * @ORM\Column(name="description", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=14, options={"by_reference"=false, "required"=false, "description"="Description de cet élément. Cette description est uniquement utilisée dans l'interface d'administration, afin de mieux repérer ce que vous recherchez."}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=14, options={"by_reference"=false, "required"=false, "description"="Description de cet élément. Cette description est uniquement utilisée dans l'interface d'administration, afin de mieux repérer ce que vous recherchez."}, contexts={"medium", "expert"})
	 * @CRUDS\Show(role=true)
	 */
	protected $description;

	/**
	 * @var string
	 * Texte de l'élément, version courte de fulltext
	 * Texte de 5 ou 6 lignes max
	 * @ORM\Column(name="resume", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=8, options={"by_reference"=false, "required"=false, "description"="Texte résumé (optionnel). Ce texte est utilisé dans les liens disposant d'assez de place pour donner un résumé à lire et inciter à cliquer pour lire le texte entier. Il est également utilisé dans la page même si le grand texte n'a pas été renseigné."}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=8, options={"by_reference"=false, "required"=false, "description"="Texte résumé (optionnel). Ce texte est utilisé dans les liens disposant d'assez de place pour donner un résumé à lire et inciter à cliquer pour lire le texte entier. Il est également utilisé dans la page même si le grand texte n'a pas été renseigné."}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $resume;

	/**
	 * @var string
	 * Texte complet de l'élément - illimité
	 * @ORM\Column(name="full_text", type="text", nullable=true, unique=false)
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", type="SummernoteType", order=9, options={"by_reference"=false, "required"=false, "description"="Grand texte principal de la page. S'il n'est pas décrit ici, alors le texte résumé sera utilisé à la place."}, contexts={"simple"})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", type="SummernoteType", order=9, options={"by_reference"=false, "required"=false, "description"="Grand texte principal de la page. S'il n'est pas décrit ici, alors le texte résumé sera utilisé à la place."}, contexts={"simple"})
	 * @CRUDS\Show(role=true)
	 */
	protected $fulltext;

	/**
	 * @var array
	 * @ORM\Column(name="urls", type="json_array", nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type="TextareaType",
	 * 		options={"required"=false, "by_reference"=false, "property_path"="urlsAsString", "description"="Liste de liens URL."},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type="TextareaType",
	 * 		options={"required"=false, "by_reference"=false, "property_path"="urlsAsString", "description"="Liste de liens URL."},
	 * 		contexts={"medium", "expert"}
	 * )
	 * @CRUDS\Show(role=true)
	 */
	protected $urls;


	/**
	 * @Annot\PostCreate()
	 */
	public function init_BaseItem() {
		$this->name ??= null;
		$this->slug ??= null;
		$this->title ??= null;
		$this->subtitle ??= null;
		$this->menutitle ??= null;
		$this->description ??= null;
		$this->accroche ??= null;
		$this->resume ??= null;
		$this->fulltext ??= null;
		$this->urls ??= [];
		return $this;
	}


	/**
	 * Get entity as string
	 * @return string
	 */
	public function __toString() {
		return $this->getSlug();
	}

	/**
	 * Set menutitle if not defined
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return self
	 */
	public function computeBaseTexts() {
		if(!serviceTools::isStringOverTags($this->getTitle())) $this->setTitle($this->getName());
		if(!serviceTools::isStringOverTags($this->getMenutitle())) $this->setMenutitle($this->getTitle());
		if(!serviceTools::isStringOverTags($this->getSubtitle())) $this->setSubtitle($this->getTitle());
		if(!serviceTools::isStringOverTags($this->getDescription())) $this->setDescription($this->getTitle());
		if(!serviceTools::isStringOverTags($this->getAccroche())) $this->setAccroche($this->getTitle());
		if(!serviceTools::isStringOverTags($this->getFulltext()) && !serviceTools::isStringOverTags($this->getResume())) $this->setFulltext($this->getResume());
		return $this;
	}

	/**
	 * Get name (if requested username)
	 * @return string
	 */
	public function getUsername() {
		return $this->name;
	}

	/**
	 * Get name
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Get name as text (for JStree)
	 * @return string
	 */
	public function getNameAsText() {
		$names = explode('@', $this->getName());
		return end($names);
	}

	/**
	 * Set name
	 * @param string $name
	 * @param boolean $updateNested = true
	 * @return self
	 */
	public function setName($name, $updateNested = true) {
		$name = serviceTools::getTextOrNullStripped($name);
		$name = serviceTools::removeSeparator($name);
		$this->name = $name;
		return $this;
	}

	/*************************************************************************************/
	/*** SLUG
	/*************************************************************************************/

	/**
	 * Get slug
	 * @return string 
	 */
	public function getSlug() {
		return $this->slug;
	}

	// /**
	//  * Set slug
	//  * @param string $slug
	//  * @return Item
	//  */
	// public function setSlug($slug) {
	// 	$this->slug = $slug;
	// 	if($this instanceOf Item) {
	// 		$this->updatePaths(true, true);
	// 		die('-> DONE: set slug '.json_encode($this->slug).' new pathslug: '.json_encode($this->pathslug).'!');
	// 	}
	// 	return $this;
	// }

	/**
	 * Get title
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Set title
	 * @param string $title = null
	 * @return self
	 */
	public function setTitle($title = null) {
		$this->title = serviceTools::getTextOrNullStripped($title);
		return $this;
	}

	/**
	 * Get menutitle
	 * @return string
	 */
	public function getMenutitle() {
		return $this->menutitle;
	}

	/**
	 * Set menutitle
	 * @param string $menutitle = null
	 * @return self
	 */
	public function setMenutitle($menutitle = null) {
		$this->menutitle = serviceTools::getTextOrNullStripped($menutitle);
		return $this;
	}

	/**
	 * Get subtitle
	 * @return string
	 */
	public function getSubtitle() {
		return $this->subtitle;
	}

	/**
	 * Set subtitle
	 * @param string $subtitle = null
	 * @return self
	 */
	public function setSubtitle($subtitle = null) {
		$this->subtitle = serviceTools::getTextOrNull($subtitle);
		return $this;
	}

	/**
	 * Get description
	 * @return string | null
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set description
	 * @param string $description = null
	 * @return self
	 */
	public function setDescription($description = null) {
		$this->description = serviceTools::getTextOrNull($description);
		return $this;
	}

	/**
	 * Get accroche
	 * @return string | null
	 */
	public function getAccroche() {
		return $this->accroche;
	}

	/**
	 * Set accroche
	 * @param string $accroche = null
	 * @return self
	 */
	public function setAccroche($accroche = null) {
		$this->accroche = serviceTools::getTextOrNull($accroche);
		return $this;
	}

	/**
	 * Get resume
	 * @return string | null
	 */
	public function getResume() {
		return $this->resume;
	}

	/**
	 * Set resume
	 * @param string $resume = null
	 * @return self
	 */
	public function setResume($resume = null) {
		$this->resume = serviceTools::getTextOrNull($resume);
		return $this;
	}

	/**
	 * Get fulltext
	 * @return string | null
	 */
	public function getFulltext() {
		return $this->fulltext;
	}

	/**
	 * Set fulltext
	 * @param string $fulltext = null
	 * @return self
	 */
	public function setFulltext($fulltext = null) {
		$this->fulltext = serviceTools::getTextOrNull($fulltext);
		return $this;
	}

	/*************************************************************************************/
	/*** URLS
	/*************************************************************************************/

	/**
	 * Get main (first) url
	 * @return string | null 
	 */
	public function getUrl() {
		return reset($this->urls);
	}

	/**
	 * Get urls
	 * @return array <string> 
	 */
	public function getUrls() {
		return $this->urls;
	}

	/**
	 * Set urls
	 * @param array | string $urls = []
	 * @return self
	 */
	public function setUrls($urls = []) {
		$this->urls = [];
		// $urls = (array) $urls;
		foreach ($urls as $url) if(serviceTools::computeTextOrNull($url)) $this->addUrl($url);
		return $this;
	}

	/**
	 * Get urls as string
	 * @return string
	 */
	public function getUrlsAsString() {
		return is_array($this->urls) ? implode(PHP_EOL, $this->urls) : $this->urls;
	}

	/**
	 * Set urls with string
	 * @see https://www.php.net/manual/fr/function.preg-quote.php
	 * @param string $urls = null
	 * @return self
	 */
	public function setUrlsAsString($urls = null) {
		if(serviceTools::computeTextOrNull($url)) return $this->setUrls([]);
		$urls = preg_split('/('.preg_quote(PHP_EOL).'+)/', $urls);
		return $this->setUrls($urls);
	}

	/**
	 * Get urls as Json
	 * @return string
	 */
	public function getUrlsAsJson() {
		return json_encode($this->urls);
	}

	/**
	 * Set urls with Json
	 * @param string $urls = null
	 * @return self
	 */
	public function setUrlsAsJson($urls = null) {
		if(null === $urls) $this->setUrls([]);
		return $this->setUrls(json_decode($this->urls));
	}

	/**
	 * Add url
	 * @param string $url
	 * @return self
	 */
	public function addUrl($url) {
		if(serviceTools::computeTextOrNull($url)) $this->urls = array_unique(array_merge($this->urls, [$url]));
		return $this;
	}

	/**
	 * Remove url
	 * @param string $url
	 * @return self
	 */
	public function removeUrl($urlToRemove) {
		$this->urls = array_filter($this->urls, function($url) use ($urlToRemove) {
			return $url !== $urlToRemove;
		});
		return $this;
	}

}