<?php
namespace ModelApi\BaseBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;

use \DateTime;

trait DateUpdater {

	/**
	 * Creation date
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false, unique=false)
	 */
	protected $created;

	/**
	 * Update date
	 * @var DateTime
	 * @ORM\Column(name="updated", type="datetime", nullable=true, unique=false)
	 */
	protected $updated;

	protected $touched;

	/**
	 * @ORM\PostLoad()
	 */
	public function updateTouched() {
		$this->touched = $this->updated instanceOf DateTime ? $this->updated : $this->created;
	}

	/**
	 * @Annot\PostCreate()
	 * Init created
	 * @return Entity
	 */
	public function initCreated() {
		if(!isset($this->created) || !($this->created instanceOf DateTime)) $this->setCreated(null);
		$this->updateTouched();
		return $this;
	}

	/**
	 * Set created
	 * @param DateTime $created = null
	 * @return Entity
	 */
	public function setCreated(DateTime $created = null) {
		$this->created = $created instanceOf DateTime ? $created : new DateTime();
		return $this;
	}

	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	public function isRecentlyCreated($modify = '-48 hours') {
		$test = new Datetime();
		$test->modify($modify);
		return $this->created > $test;
	}

	public function isRecentlyTouched($modify = '-48 hours') {
		$test = new Datetime();
		$test->modify($modify);
		return $this->touched > $test;
	}

	/**
	 * Created date on persist (not on create)
	 * @ORM\PrePersist()
	 */
	public function updateCreated() {
		$this->setCreated(null);
	}

	/**
	 * ///@ORM\PreUpdate()
	 * DOES NOT always update --> see serviceAnnotation : does not update un "check" context - used by bin/console Command)
	 * @Annot\UpdateDate()
	 */
	public function updateDate() {
		$this->setUpdated();
	}

	/**
	 * Set updated
	 * @param DateTime $updated = null
	 * @return Entity
	 */
	public function setUpdated(DateTime $updated = null) {
		$this->updated = $updated instanceOf DateTime ? $updated : new DateTime();
		return $this;
	}

	/**
	 * Get updated
	 * @return DateTime|null
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Get updated or created. Gives a Datetime anyway
	 * @return DateTime 
	 */
	public function getLastUpdated() {
		return $this->getUpdated() instanceOf DateTime ? $this->getUpdated() : $this->getCreated();
	}

}