<?php
namespace ModelApi\BaseBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use ModelApi\AnnotBundle\Annotation as Annot;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Entity\Basedirectory;
// UserBundle
use ModelApi\UserBundle\Entity\User;

use \DateTime;

trait BaseEntity {


	protected $id;

	/**
	 * Is softdeleted (if true, only visible by ROLE_SUPER_ADMIN)
	 * @var datetime
	 * @ORM\Column(name="softdeleted", type="datetime", nullable=true, unique=false)
	 */
	protected $softdeleted;

	/**
	 * Is enabled
	 * @var boolean
	 * @ORM\Column(name="enabled", type="boolean", nullable=false, unique=false)
	 */
	protected $enabled;

	/**
	 * Is validated by admin
	 * @var boolean
	 * @ORM\Column(name="adminvalidated", type="boolean", nullable=false, unique=false)
	 * @Annot\Adminvalidated()
	 */
	protected $adminvalidated;

	/**
	 * Is visible even if inactive
	 * @var boolean
	 * @ORM\Column(name="visible", type="boolean", nullable=false, unique=false)
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=100,
	 * 		options={"required"=false},
	 * 		contexts={"translation", "simple", "medium", "expert"}
	 * )
	 * @CRUDS\Show(role="ROLE_EDITOR", order=100)
	 */
	protected $visibleEvenIfInactive;

	/**
	 * Is active (enabled and adminvalidated and not softdeleted)
	 * @var boolean
	 */
	protected $active;

	/**
	 * @var string
	 * @ORM\Column(name="fa_icon", type="string", length=64, nullable=false, unique=false)
	 * @CRUDS\Show(role=true, order=100, type="fa-icon", order=30)
	 */
	protected $icon;

	/**
	 * @var string
	 * @ORM\Column(name="color", type="string", length=32, nullable=true, unique=false)
	 * @CRUDS\Create(
	 * 		type="ColorpickerType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "by_reference"=false},
	 * )
	 * @CRUDS\Update(
	 * 		type="ColorpickerType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=100,
	 * 		options={"required"=true, "by_reference"=false},
	 * )
	 * @CRUDS\Show(role=true, order=100, type="colordot", order=30)
	 */
	protected $color;

	/**
	 * @var string
	 * @ORM\Column(name="oldname", type="string", nullable=true, unique=false)
	 */
	protected $oldname;

	/**
	 * @Annot\InsertContextdate(setter="setContextdate", type="string")
	 */
	protected $contextdate;


	/**
	 * @Annot\PostCreate()
	 */
	public function init_BaseEntity() {
		$this->id = null;
		if(!isset($this->softdeleted)) $this->softdeleted = null;
		if(!isset($this->enabled)) $this->enabled = true;
		if(!isset($this->adminvalidated)) $this->adminvalidated = true;
		if(!isset($this->visibleEvenIfInactive)) $this->visibleEvenIfInactive = false;
		if(!isset($this->icon)) $this->setIcon(static::DEFAULT_ICON);
		if(!isset($this->color)) $this->color = '#BBBBBB';
		if(!isset($this->oldname)) $this->oldname = null;
		if(!isset($this->contextdate)) $this->contextdate = new DateTime(serviceContext::DEFAULT_DATE);
		return $this;
	}


	/**
	 * Set context date
	 * @param string | DateTime | null $contextdate = null
	 * @return self
	 */
	public function setContextdate($contextdate = null) {
		$this->contextdate = $contextdate;
		return $this;
	}

	/**
	 * Get context date
	 * @return DateTime | string
	 */
	public function getContextdate($contextdate = null, $trueValue = false) {
		if($trueValue) return $this->contextdate;
		if($contextdate instanceOf DateTime) return $contextdate;
		if(empty($contextdate)) {
			return new DateTime(serviceContext::DEFAULT_DATE);
		} else if(is_string($contextdate)) {
			return serviceContext::modifyDate(new DateTime(serviceContext::DEFAULT_DATE), $contextdate);
		} else if(is_string($this->contextdate)) {
			return serviceContext::modifyDate(new DateTime(serviceContext::DEFAULT_DATE), $this->contextdate);
		}
		return $this->contextdate instanceOf DateTime ? $this->contextdate : new DateTime(serviceContext::DEFAULT_DATE);
	}


	// /**
	//  * Get entity as string
	//  * @return string
	//  */
	// public function __toString() {
	// 	return (string)$this->getId();
	// }

	/**
	 * Get Id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get softdeleted
	 * @return DateTime 
	 */
	public function getSoftdeleted() {
		return $this->softdeleted;
	}

	/**
	 * Is softdeleted
	 * @return boolean 
	 */
	public function isSoftdeleted($contextdate = null) {
		return serviceEntities::isSoftdeleted($this->softdeleted, $contextdate);
		// if(!($this->softdeleted instanceOf DateTime)) return false;
		// return $this->softdeleted <= $this->getContextdate($contextdate);
	}

	/**
	 * Set softdeleted
	 * @param DateTime $softdeleted = null
	 * @return self
	 */
	public function setSoftdeleted(DateTime $softdeleted = null) {
		if(!($softdeleted instanceOf DateTime)) $softdeleted = new DateTime();
		$this->softdeleted = $softdeleted;
		// $this->setDisabled();
		// replace name
		// if(method_exists($this, 'setName')) {
		// 	if(null === $this->getOldname()) $this->setOldname($this->getName());
		// 	$this->setName(serviceTools::geUniquid('del_'));
		// }
		return $this;
	}

	/**
	 * Set unsoftdeleted
	 * @return self
	 */
	public function setUnsoftdeleted() {
		$this->softdeleted = null;
		// if(method_exists($this, 'setName')) {
		// 	$this->setName($this->getOldname());
		// 	// $this->setOldname(null);
		// }
		return $this;
	}

	/**
	 * Is visible, even if inactive
	 * @return boolean 
	 */
	public function isVisible($contextdate = null) {
		$active = $this->isActive() && !$this->isSoftdeleted($contextdate);
		return $this->getVisibleEvenIfInactive() || $active;
	}

	/**
	 * Is visible, even if inactive
	 * @return boolean 
	 */
	public function isInactiveButVisible($contextdate = null) {
		$inactive = $this->isDisabled() || $this->isSoftdeleted($contextdate);
		return $this->getVisibleEvenIfInactive() && $inactive;
	}

	/**
	 * Get visible even if inactive
	 * @return boolean 
	 */
	public function getVisibleEvenIfInactive() {
		return $this->visibleEvenIfInactive;
	}

	/**
	 * Set visible even if inactive
	 * @param boolean $visibleEvenIfInactive = true
	 * @return self
	 */
	public function setVisibleEvenIfInactive($visibleEvenIfInactive = true) {
		$this->visibleEvenIfInactive = $visibleEvenIfInactive;
		return $this;
	}

	/**
	 * Get enabled
	 * @return boolean 
	 */
	public function getEnabled() {
		return $this->enabled && $this->adminvalidated;
	}

	/**
	 * Is enabled
	 * @return boolean 
	 */
	public function isEnabled() {
		return $this->getEnabled();
	}

	/**
	 * Is disabled
	 * @return boolean 
	 */
	public function isDisabled() {
		return !$this->getEnabled();
	}

	/**
	 * Get disabled
	 * @return boolean 
	 */
	public function getDisabled() {
		return !$this->getEnabled();
	}

	/**
	 * Set enabled
	 * @param boolean $enabled = true
	 * @return self
	 */
	public function setEnabled($enabled = true) {
		$this->enabled = $enabled;
		if(!$this->adminvalidated) $this->enabled = false;
		return $this;
	}

	/**
	 * Set disabled
	 * @return self
	 */
	public function setDisabled() {
		$this->enabled = false;
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return self
	 */
	public function checkEnabled() {
		if(!$this->isAdminvalidated()) $this->enabled = false;
		return $this;
	}

	public function getAdminvalidated() {
		return $this->adminvalidated;
	}

	public function isAdminvalidated() {
		return $this->getAdminvalidated();
	}

	public function setAdminvalidated($adminvalidated = true) {
		$this->adminvalidated = $adminvalidated;
		if($this->adminvalidated) {
			$this->setEnabled(true);
		} else {
			$this->setDisabled();
		}
		return $this;
	}

	public function checkAdminvalidation() {
		if($this instanceOf Basedirectory) $this->setEnabled();
		if(!empty($this->id) && $this->enabled && !$this->adminvalidated) $this->adminvalidated = true;
		if($this instanceOf User && $this->isSuperadmin()) {
			$this->adminvalidated = true;
			$this->enabled = true;
			$this->softdeleted = null;
		}
		return $this;
	}

	/** 
	 * Is entity available for use on site (test all global validity)
	 * @return boolean
	 */
	public function getActive($contextdate = null) {
		return $this->isEnabled() && !$this->isSoftdeleted($contextdate);
	}

	/** 
	 * Is entity available for use on site (test all global validity)
	 * @return boolean
	 */
	public function isActive($contextdate = null) {
		return $this->isEnabled() && !$this->isSoftdeleted($contextdate);
	}

	public function isInactive($contextdate = null) {
		return !$this->isActive($contextdate);
	}

	/**
	 * Set default icon if not defined
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * @return self
	 */
	public function defaultIconIfNone() {
		if(!is_string($this->getIcon())) $this->setIcon(static::DEFAULT_ICON);
		return $this;
	}

	/**
	 * Set icon
	 * @param string $icon = null
	 * @return self
	 */
	public function setIcon($icon = null) {
		$this->icon = is_string($icon) ? $icon : static::DEFAULT_ICON;
		return $this;
	}

	/**
	 * Get icon
	 * @return string
	 */
	public function getIcon() {
		return $this->icon;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDEFAULT_ICON() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDefaultIcon() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public function getIconClass() {
		return 'fa '.$this->getIcon();
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public static function getDefaultIconClass() {
		return 'fa '.static::DEFAULT_ICON;
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public function getIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.$this->getIconClass().$classes.'"></i>';
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public static function getDefaultIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.static::getDefaultIconClass().$classes.'"></i>';
	}

	/**
	 * Get color
	 * @return string 
	 */
	public function getColor() {
		return $this->color;
	}

	/**
	 * Set color
	 * @param string $color
	 * @return self
	 */
	public function setColor($color) {
		$this->color = $color;
		return $this;
	}


	/**
	 * Get oldname
	 * @return string
	 */
	public function getOldname() {
		return $this->oldname;
	}

	/**
	 * Set oldname
	 * @param string $oldname
	 * @return self
	 */
	public function setOldname($oldname = null) {
		$this->oldname = $oldname;
		return $this;
	}

}