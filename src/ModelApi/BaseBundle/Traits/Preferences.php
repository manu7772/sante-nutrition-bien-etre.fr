<?php
namespace ModelApi\BaseBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\common\EventArgs;
use Doctrine\ORM\Events;
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;

// BaseBundle
use ModelApi\BaseBundle\Component\Preference;
use ModelApi\BaseBundle\Service\servicePreference;

use \Exception;

trait Preferences {

	/**
	 * Preferences
	 * @var array
	 * @ORM\Column(name="preferences", type="json_array")
	 * @Annot\Preferences()
	 */
	protected $preferences;

	protected $_save_preferences;


	/*************************************************************************************/
	/*** EVENTS
	/*************************************************************************************/

	public function eventAction($eventType, EventArgs $args, servicePreference $servicePreference) {
		switch ($eventType) {
			case BaseAnnotation::postNew:
				$servicePreference->checkPreferences($this, true);
				$this->preferencesToObject();
				break;
			case Events::postLoad:
				$this->preferencesToObject();
				if(!($this->preferences instanceOf Preference) || empty($this->preferences)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Preferences after ".$eventType." are not instance of Preference! Got ".json_encode($this->preferences).".", 1);
				break;
			// case Events::prePersist:
			// case Events::preUpdate:
			case Events::onFlush:
				if(!($this->preferences instanceOf Preference)) {
					if(!is_array($this->preferences) || empty($this->preferences)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Preferences before ".$eventType." are not instance of Preference or array! Got ".json_encode($this->preferences).".", 1);
					$this->_save_preferences = new Preference(Preference::getBaseName(), $this->preferences);
				} else {
					$this->_save_preferences = $this->preferences;
					$this->preferences = $this->preferences->toArray(false);
					$em = $args->getEntityManager();
					$uow = $em->getUnitOfWork();
					// $uow->computeChangeSet($em->getClassMetadata(get_class($this)), $this);
					$uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($this)), $this);
				}
				if(!is_array($this->preferences) || empty($this->preferences)) {
					throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Preferences on ".$eventType."! Got ".(is_object($this->preferences) ? get_class($this->preferences) : gettype($this->preferences))." instead of array!", 1);
				}
				break;
			// case Events::postPersist:
			// case Events::postUpdate:
			case Events::postFlush:
				if(!($this->_save_preferences instanceOf Preference)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): saved Preferences after ".$eventType." are not instance of Preference! Got ".json_encode($this->preferences).".", 1);
				$this->preferences = $this->_save_preferences;
				break;
		}
		return $this;
	}


	/*************************************************************************************/
	/*** PREFERENCES
	/*************************************************************************************/

	public function getPreferences() {
		$this->preferencesToObject();
		return $this->preferences;
	}

	protected function preferencesToObject() {
		if(!($this->preferences instanceOf Preference)) $this->preferences = new Preference(Preference::getBaseName(), $this->preferences);
		return $this->preferences instanceOf Preference;
	}

	public function getAllChildablePaths($path = null) {
		$paths = [];
		$preference = $this->getPreferenceByPath($path);
		if($preference instanceOf Preference && $preference->isChildable()) {
			$paths[$preference->getPath()] = $preference->getPathSlached(true);
			foreach ($preference->getChilds() as $child) {
				$paths = array_merge($paths, $this->getAllChildablePaths($child->getPath()));
			}
		}
		return $paths;
	}

	public function getPreferenceByPath($path = null, $separator = null) {
		$this->preferencesToObject();
		if(empty($path)) return $this->preferences;
		return $this->preferences->getPreferenceByPath($path, $separator);
	}

	public static function pathToArray($path = null) {
		if(empty($path)) return null;
		if(is_array($path)) return $path;
		$path = preg_split('/[\\.\\/\\|]+/', $path);
		return array_filter($path, function($p) { return !empty($p); });
	}

	public static function pathToString($path = null) {
		if(empty($path)) return "";
		if(is_string($path)) return $path;
		return implode(servicePreference::PREFS_PATH_SEPARATOR, $path);
	}

	public static function getParentPath($path = null, $to = 'same') {
		if(empty($path)) return null;
		$to = strtolower($to);
		if(!in_array($to, ['array','string'])) {
			$format = is_string($path) ? 'string' : 'array';
		}
		$parent = static::pathToArray($path);
		array_pop($parent);
		return $to === 'array' ? static::pathToArray($parent) : static::pathToString($parent);
	}

	public static function extractPrefNameInPath($path) {
		$path = static::pathToArray($path);
		return end($path);
	}

	public function getPreferenceValue($path = null, $defaultValueIfnotFound = null, $exceptionIfNotFound = false) {
		$this->preferencesToObject();
		return $this->getPreference($path, $defaultValueIfnotFound, $exceptionIfNotFound, true);
	}

	public function getPreference($path = null, $defaultValueIfnotFound = null, $exceptionIfNotFound = true, $getValue = false) {
		$this->preferencesToObject();
		if(!is_string($path) && !is_array($path) && !empty($path)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): path must be null, a string or array of string!", 1);
		$path = static::pathToArray($path);
		$preference = $this->getPreferenceByPath($path);
		if(!($preference instanceOf Preference)) return $defaultValueIfnotFound;
		return $getValue ? $preference->getValue() : $preference;
	}

	public function getPreferencesAsSimpleArray() {
		$this->preferencesToObject();
		return $this->preferences->toArray(true);
	}

	public function addPreference($path, $value) {
		return $this->setPreference($path, $value);
	}

	public function setPreference($path, $value, $addIfNotFound = true) {
		$this->preferencesToObject();
		$preference = $this->getPreference($path);
		// if(!($preference instanceOf Preference)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Preference ".json_encode($path)." not found!", 1);
		if($preference instanceOf Preference) {
			$preference->setValue($value);
		} else if($addIfNotFound) {
			$parentPref = $this->getPreference(static::getParentPath($path));
			$newPref = new Preference(static::extractPrefNameInPath($path), $value, null, $parentPref);
		} else {
			throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Preference ".json_encode($path)." not found!", 1);
		}
		return $this;
	}

	public function setPreferences($values, $add = false) {
		if(!$add) $this->preferences = array();
		if(is_array($values)) {
			foreach ($values as $path => $value) {
				$this->setPreference(preg_replace('/[\\.\\/\\|-]+/', '_', $path), $value);
			}
		} else if($values instanceOf Preference) {
			$values->setName(servicePreference::BASE_NAME);
			$this->preferences = $values;
		}
		return $this;
	}

	public function removePreference($path) {
		$this->preferences = array_filter($this->preferences, function($key) use ($path) {
			return $key !== $path;
		}, ARRAY_FILTER_USE_KEY);
		return $this;
	}




}