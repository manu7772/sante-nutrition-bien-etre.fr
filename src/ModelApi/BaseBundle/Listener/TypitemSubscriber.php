<?php
namespace ModelApi\BaseBundle\Listener;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

use Doctrine\ORM\Events;
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;

use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceTypitem;

use \Exception;
use \DateTime;

class TypitemSubscriber implements EventSubscriber {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		return $this;
	}

	/**
	 * Master running Events
	 */
	public function getSubscribedEvents() {
		return array(
			// BaseAnnotation::postNew,
			BaseAnnotation::preCreateForm,
			BaseAnnotation::preUpdateForm,
			// Events::postLoad,
			// Events::prePersist,
			// Events::postPersist,
			// Events::preUpdate,
			// Events::postUpdate,
			// Events::preRemove,
			// Events::postRemove,
			// Events::loadClassMetadata,
			// Events::onClassMetadataNotFound,
			// Events::preFlush,
			// Events::onFlush,
			// Events::postFlush,
			// Events::onClear,
		);
	}

	public function preCreateForm(LifecycleEventArgs $args) {
		return $this->preUpdateForm($args);
	}

	public function preUpdateForm(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($entity instanceOf Typitem) {
			$this->container->get(serviceTypitem::class)->insertGroupsChoices($entity);
		}
		return $this;
	}



}