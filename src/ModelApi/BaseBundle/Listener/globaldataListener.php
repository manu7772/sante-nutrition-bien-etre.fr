<?php
namespace ModelApi\BaseBundle\Listener;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\AcceptHeader;
// AnnotBundle
use ModelApi\AnnotBundle\Service\serviceAnnotation;
// BaseBundle
use ModelApi\BaseBundle\Entity\Logg;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceLogg;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// BinaryBundle
use ModelApi\BinaryBundle\Service\serviceBinaryFile;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceTranslation;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// UserBundle
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Service\serviceEntreprise;
use ModelApi\UserBundle\Service\serviceTier;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \Exception;
use \ReflectionClass;

class globaldataListener {

	use \ModelApi\BaseBundle\Service\baseService;

	const LOGG_REGISTER = true;
	const FLUSH_LOGG = true;

	private $container;
	private $serviceEntities;
	private $serviceContext;
	private $_em;
	private $serviceKernel;
	private $serviceLogg;
	private $locale;
	private $language;
	private $loggRegister;
	private $loggFlush;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->serviceContext = $this->container->get(serviceContext::class);
		$this->_em = $this->serviceEntities->getEntityManager();
		$this->serviceKernel = $this->container->get(serviceKernel::class);
		$this->serviceLogg = $this->container->get(serviceLogg::class);
		$this->language = null;
		$this->setLoggRegister(static::LOGG_REGISTER);
		$this->setLoggFlush(static::FLUSH_LOGG);
		$this->locale = null;
		// DevTerminal::Warning('### GlobaldataListener loaded...');
		return $this;
	}


	public function isLoggRegister() {
		return $this->loggRegister;
	}

	public function setLoggRegister($loggRegister) {
		$this->loggRegister = $loggRegister;
		return $this;
	}

	public function isLoggFlush() {
		$logg = $this->getLogg();
		return $this->loggFlush && $logg instanceOf Logg && $logg->isInitialized();
	}

	public function setLoggFlush($flush) {
		$this->loggFlush = $flush;
		return $this;
	}

	public function getLogg() {
		return $this->serviceLogg->getCurrentLogg($this->isLoggRegister());
	}

	public function loggListener(GetResponseEvent $event) {
		if($this->isRequiredAction($event) && $this->isLoggRegister()) $this->getLogg()->addStatus('GetResponseEvent');
	}

	protected function isRequiredAction($event) {
		$request = $event->getRequest();
		// if(!$this->container->get(serviceAnnotation::class)->isDefaultContext()) return false;
		if(!($request instanceOf Request)) return false;
		if(HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()) return false;
		//if($request->isXmlHttpRequest()) return false;
		return true;
	}

	public function afterAnyEntityLoaded(GetResponseEvent $event) {
		if($this->isRequiredAction($event)) {
			// DevTerminal::Warning('### After any entity loaded...');
			$this->serviceContext->completeContext();
			$twig = $this->container->get('twig'); // TWIG service
			// Define translation domains
			$twig->addGlobal('domain_admin', serviceTwgTranslation::ADMIN_DOMAIN);
			$twig->addGlobal('domain_site', serviceTwgTranslation::DEFAULT_DOMAIN);
			$twig->addGlobal('domain_default', $this->serviceKernel->getTranslationDomainByBundle());
			// $twig->addGlobal('site_context', $this->serviceContext);
			// Define TIMEZONE
			$twig->getExtension('\Twig\Extension\CoreExtension')->setTimezone($this->serviceContext->getTimeZone());
			// LOGG
			if($this->isLoggRegister()) $this->getLogg()->addStatus('GetResponseEvent');
			// From in Request
			if($event->getRequest()->request->count()) serviceBinaryFile::transformRequestBagsUploadedFiles($event->getRequest());
		}
	}

	public function FilterControllerEvent(FilterControllerEvent $event) {
		if($this->isLoggRegister() && $this->isRequiredAction($event)) {
			if($event->getRequest()->getSession() instanceOf Session) {
				// Logg
				$this->getLogg()->addStatus('FilterControllerEvent');
			}
		}
	}

	public function GetResponseForControllerResultEvent(GetResponseForControllerResultEvent $event) {
		// Logg
		if($this->isLoggRegister()) {
			$this->getLogg()->addStatus('GetResponseForControllerResultEvent')->addError($event->getResponse());
		}
	}

	public function InteractiveLoginEvent(InteractiveLoginEvent $event) {
		// Logg
		if($this->isLoggRegister()) {
			$this->getLogg()->addStatus('InteractiveLoginEvent');
		}
	}

	public function AuthenticationEvent(AuthenticationEvent $event) {
		// Logg
		if($this->isLoggRegister()) {
			$this->getLogg()->addStatus('AuthenticationEvent');
		}
	}

	public function AuthenticationFailureEvent(AuthenticationFailureEvent $event) {
		// Logg
		if($this->isLoggRegister()) {
			$this->getLogg()->addStatus('AuthenticationFailureEvent')->addError($event->getAuthenticationException());
		}
	}

	public function GetResponseForExceptionEvent(GetResponseForExceptionEvent $event) {
		// Logg
		if($this->isLoggRegister()) {
			$this->getLogg()->addStatus('GetResponseForExceptionEvent')->addError($event->getException());
		}
	}

	public function FilterResponseEvent(FilterResponseEvent $event) {
		// if($this->isLoggRegister()) {
		if($this->isLoggFlush()) {
			if($this->isRequiredAction($event) && $event->getRequest()->getSession() instanceOf Session) {
				// Logg
				$this->getLogg()->addStatus('FilterResponseEvent');
				$this->getLogg()->setClosed();
				$this->saveLogg();
			}
		}
	}

	private function saveLogg($control = false) {
		$logg = $this->getLogg();
		if($this->isLoggFlush()) {
			$str = $this->container->get(serviceTranslation::class);
			$isStr = $str->isTranslatator();
			if($isStr) $str->disableTranslatator();
			$this->_em->clear();
			$this->_em->persist($logg);
			$this->_em->flush();
			if($control || $this->serviceKernel->isDev()) {
				$identity = $this->_em->getUnitOfWork()->getIdentityMap();
				if(count($identity) > 1 || !array_key_exists("ModelApi\BaseBundle\Entity\Logg", $identity)) throw new Exception("Error ".__METHOD__."(): only Logg must be flushed, but other or no entity was flushed! Got ".json_encode(array_keys($identity))."!", 1);
				if(!$logg->isFullyCorrect()) $this->container->get(serviceFlashbag::class)->addFlashToastr('danger', 'Logg is not fully correct. Got errors!', 'ROLE_SUPER_ADMIN');
				if(!$logg->isEnded()) $this->container->get(serviceFlashbag::class)->addFlashToastr('danger', 'Logg saved as "'.$logg->getLastStatusName().'"!', 'ROLE_SUPER_ADMIN');
			}
			if($isStr) $str->enableTranslatator();
			// YamlLog::directYamlLogfile([
			// 	__METHOD__ => 'Logg flushed!',
			// ]);
		} else {
			if($this->serviceKernel->isDev()) $this->container->get(serviceFlashbag::class)->addFlashToastr('danger', 'Logg could not be saved, while not initialized! Please see Yaml log file for more details.', 'ROLE_SUPER_ADMIN');
			$loggmsg = 'NO LOGG FOUND!';
			// if($logg instanceOf closure) $loggmsg = get_class($logg);
				// else if($logg instanceOf Logg) $loggmsg = serialize($logg);
			if($logg instanceOf Logg) {
				try {
					$loggmsg = serialize($logg);
				} catch (Exception $e) {
					$loggmsg = get_class($logg).' / '.$e->getMessage();
				}
			}
			YamlLog::directYamlLogfile([
				__METHOD__ => 'Logg could not be saved, while not initialized!',
				'Logg' => $loggmsg
			]);
		}
		return $this;
	}


	// 	$identity = $this->_em->getUnitOfWork()->getIdentityMap();
	// 	$data = [];
	// 	foreach (array_keys($identity) as $classname) {
	// 		$data[$classname] = count($identity[$classname]);
	// 	}
	// 	echo('<pre>');var_dump($data);echo('</pre>');
	// }


}