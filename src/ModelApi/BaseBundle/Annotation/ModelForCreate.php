<?php
namespace ModelApi\BaseBundle\Annotation;

use \ReflectionClass;
use \BadMethodCallException;

/**
 * @Annotation
 * @Target("PROPERTY")
 * 
 * @see https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * @see https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class ModelForCreate {

	/**
	 * @var string
	 */
	public $getter = null;

	/**
	 * @var string
	 */
	public $setter = null;

	/**
	 * @var array
	 */
	public $groups = ['default'];

	/**
	 * @var array
	 */
	public $environments = ['all'];

	/**
	 * Constructor.
	 *
	 * @param array $options Key-value for properties to be defined in this class.
	 */
	public function __construct(array $options = []) {
		foreach ($options as $key => $value) $this->$key = $value;
	}

	/**
	 * Error handler for unknown property accessor in Annotation class.
	 *
	 * @param string $name Unknown property name.
	 *
	 * @throws \BadMethodCallException
	 */
	public function __get($name) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, get_class($this)));
	}

	/**
	 * Error handler for unknown property mutator in Annotation class.
	 *
	 * @param string $name  Unknown property name.
	 * @param mixed  $value Property value.
	 *
	 * @throws \BadMethodCallException
	 */
	public function __set($name, $value) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, get_class($this)));
	}

	/**
	 * Get shortname
	 * @return string
	 */
	public function __toString() {
		$class = new ReflectionClass(static::class);
		return $class->getShortName();
	}


}
