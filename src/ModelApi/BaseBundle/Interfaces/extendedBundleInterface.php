<?php
namespace ModelApi\BaseBundle\Interfaces;


interface extendedBundleInterface {

	public function isPublicBundle();

	public function getRoleTemplatable();

	public function getTranslationDomain();

	public function isSeo();

	public function getTemplateStructure();

}
