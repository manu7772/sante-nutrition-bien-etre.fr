<?php
namespace ModelApi\BaseBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\DoctrineType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\CallbackTransformer;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\Persistence\ObjectManager as LegacyObjectManager;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\ChoiceList\ORMQueryBuilderLoader;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
// use Symfony\Bridge\Doctrine\Form\Type\EntityType as ParentEntityType;

class EntityType extends DoctrineType {

	/**
	 * @param ManagerRegistry|LegacyManagerRegistry $registry
	 */
	public function __construct(ManagerRegistry $registry) {
	    $this->registry = $registry;
	}


    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefault('attr', array('class' => 'select-choice'));
        parent::configureOptions($resolver);
        // Invoke the query builder closure so that we can cache choice lists
        // for equal query builders
        $queryBuilderNormalizer = function (Options $options, QueryBuilder $queryBuilder = null) {
            if (\is_callable($queryBuilder)) {
                $queryBuilder = \call_user_func($queryBuilder, $options['em']->getRepository($options['class']));
            }
            return $queryBuilder;
        };
        $resolver->setNormalizer('query_builder', $queryBuilderNormalizer);
        $resolver->setAllowedTypes('query_builder', ['null', 'callable', 'Doctrine\ORM\QueryBuilder']);
    }

    /**
     * Return the default loader object.
     *
     * @param QueryBuilder $queryBuilder
     * @param string       $class
     *
     * @return ORMQueryBuilderLoader
     */
    public function getLoader(LegacyObjectManager $manager, $queryBuilder, $class) {
        return new ORMQueryBuilderLoader($queryBuilder);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'entity';
    }

    /**
     * We consider two query builders with an equal SQL string and
     * equal parameters to be equal.
     *
     * @param QueryBuilder $queryBuilder
     *
     * @return array
     *
     * @internal This method is public to be usable as callback. It should not
     *           be used in user code.
     */
    public function getQueryBuilderPartsForCachingHash($queryBuilder) {
        return [
            $queryBuilder->getQuery()->getSQL(),
            array_map([$this, 'parameterToArray'], $queryBuilder->getParameters()->toArray()),
        ];
    }

    /**
     * Converts a query parameter to an array.
     *
     * @return array The array representation of the parameter
     */
    private function parameterToArray(Parameter $parameter) {
        return [$parameter->getName(), $parameter->getType(), $parameter->getValue()];
    }

	// public function getParent() {
	// 	return ParentEntityType::class;
	// }

}
