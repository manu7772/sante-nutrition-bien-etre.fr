<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class KnobType extends AbstractType {


	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'attr' => array(
				'class' => 'dial',
				// 'data-width' => '100',
				// 'data-displayInput' => false,
				// 'data-linecap' => 'round',
				// 'data-displayPrevious' => true,
				// 'data-angleArc' => 180,
				// 'data-angleOffset' => -90,
				// 'data-angleArc' => 250,
				// 'data-angleOffset' => -125,
				// 'data-min' => 1,
				// 'data-max' => 5,
				// 'data-step' => 1,
				// 'data-bgColor' => '#c2c2d1',
				// 'data-fgColor' => '#337ab7',
				// 'data-cursor' => true,
			),
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'insKnob';
	}

	public function getParent() {
		return IntegerType::class;
	}
}
