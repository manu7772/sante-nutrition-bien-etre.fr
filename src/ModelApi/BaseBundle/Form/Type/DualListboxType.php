<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType as ParentChoiceType;

class DualListboxType extends AbstractType {


	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'attr' => array(
				// 'style' => 'display: none;',
				'class' => 'dual-listbox',
			),
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		// return 'choice';
		return 'insDualListbox';
	}

	public function getParent() {
		return ParentChoiceType::class;
	}
}
