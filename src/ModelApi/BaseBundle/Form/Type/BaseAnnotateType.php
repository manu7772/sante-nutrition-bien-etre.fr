<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
// use Doctrine\ORM\EntityManager;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Form\Util\StringUtil;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyAccess\createPropertyAccessorBuilder;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\ORM\Mapping\Column;
use Doctrine\Common\Collections\ArrayCollection;
// Gedmo
use Gedmo\Mapping\Annotation\Translatable;

use ModelApi\AnnotBundle\Annotation\AddModelTransformer;
use Symfony\Component\Form\CallbackTransformer;
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType as ParentCollectionType;
// use ModelApi\BaseBundle\Form\Type\LocaltextCreateType;
// use ModelApi\BaseBundle\Form\Type\LocaltextareaEditType;
// use ModelApi\BaseBundle\Form\Type\switcheryType;
// use ModelApi\BaseBundle\Form\Type\ColorpickerType;
use ModelApi\BaseBundle\Form\Type\SummernoteType;
use ModelApi\BaseBundle\Form\Type\TransTextType;
use ModelApi\BaseBundle\Form\Type\TransTextareaType;
use ModelApi\BaseBundle\Form\Type\TransSummernoteType;
use ModelApi\BaseBundle\Form\Type\CollectionType;
use ModelApi\BaseBundle\Form\Type\CompactentitychoiceType;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceClasses;

use Symfony\Bridge\Doctrine\Form\Type\EntityType as OriginalEntityType;
use ModelApi\BaseBundle\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as OriginalChoiceType;
use ModelApi\BaseBundle\Form\Type\ChoiceType;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceContext;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Service\serviceRoles;
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Service\serviceTier;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Service\serviceCruds;
use ModelApi\CrudsBundle\Annotation\Actions;
use ModelApi\CrudsBundle\Annotation\Annotations;
use ModelApi\CrudsBundle\Annotation\Create;
use ModelApi\CrudsBundle\Annotation\Creates;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotation;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotationInterface;
use ModelApi\CrudsBundle\Annotation\CrudsMultiAnnotation;
use ModelApi\CrudsBundle\Annotation\Show;
use ModelApi\CrudsBundle\Annotation\Shows;
use ModelApi\CrudsBundle\Annotation\Update;
use ModelApi\CrudsBundle\Annotation\Updates;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// FileBundle
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Entity\Fileformat;
use ModelApi\FileBundle\Entity\NestedInterface;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use \ReflectionClass;
use \Exception;

class BaseAnnotateType extends AbstractType {

	const ENTITY_CLASSNAME = null;

	const ACTIONS = ['create','update','remove'];
	const FORM_FORALL_CONTEXT = 'permanent'; // permanent pour tout form_context (pas user_context)
	const USER_HIGH_CONTEXT = 'all';
	const USER_DEFAULT_CONTEXT = 'simple';
	const COLLECTION_CONTEXT = 'collection';
	const SETDATA_IF_NO_ENTITY = false;
	const APPLY_EVENTS = 'SUBMIT'; // ["SUBMIT","CALLBACKTRANSFORMER"]

	protected $container;
	protected $serviceEntities;
	protected $FileManager;
	protected $serviceRoles;
	protected $serviceCruds;
	protected $user;
	protected $role;
	protected $properties;
	// protected $options;
	// protected $resolver;
	protected $name;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->FileManager = $this->container->get(FileManager::class);
		$this->serviceRoles = $this->container->get(serviceRoles::class);
		$this->serviceCruds = $this->container->get(serviceCruds::class);
		//$this->serviceContext = $this->container->get(serviceContext::class);
		$this->user = $this->serviceRoles->getUser();
		$this->role = $this->serviceRoles->getUserHigherRole($this->user);
		$this->properties = null;
		return $this;
	}

	public function getUser() {
		if(empty($this->user)) $this->user = $this->serviceRoles->getUser();
		return $this->user;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		// Builder…
		// Define name by context
		$options['cruds_action'] ??= 'create';
		$options['cruds_action'] = strtolower($options['cruds_action']);
		static::isActionValid($options['cruds_action']);
		$this->name = StringUtil::fqcnToBlockPrefix(preg_replace(['/Bundle\\\\Entity\\\\/i','/\\\\/'], ['_'.$options['cruds_action'].'_','_'], static::ENTITY_CLASSNAME))? : '';
		// $context = strtolower($options['cruds_context']) !== static::USER_DEFAULT_CONTEXT ? '_'.$options['cruds_context'] : '';
		// $this->name = StringUtil::fqcnToBlockPrefix(preg_replace(['/Bundle\\\\Entity\\\\/i','/\\\\/'], ['_'.$options['cruds_action'].$context.'_','_'], static::ENTITY_CLASSNAME))? : '';
		$classname = $builder->getDataClass();
		$entity = $builder->getData();
		if(is_null($entity)) {
			$manager = $this->serviceEntities->getEntityService($classname);
			if(static::SETDATA_IF_NO_ENTITY) {
				$entity = $manager instanceOf servicesBaseEntityInterface ?
					$manager->createNew():
					$this->serviceEntities->createNew($classname);
				$builder->setData($entity);
			} else {
				$entity = $manager instanceOf servicesBaseEntityInterface ?
					$manager->getModel($classname, true):
					$this->serviceEntities->getModel($classname, true);
			}
		}
		// Apply preCreateForm/preUpdateForm Event
		$this->serviceEntities->applyPreFormEvents($entity);

		if(static::APPLY_EVENTS === 'SUBMIT') {
			$builder->addEventListener(
				FormEvents::SUBMIT, function(FormEvent $event) {
					$data = $event->getData();
					if($this->serviceEntities->entityExists($data) && $data instanceOf CrudInterface) {
						if($data->needPostNewEventsLoad()) $this->serviceEntities->applyEvent($data, BaseAnnotation::postNew);
					}
					if(is_iterable($data)) {
						foreach ($data as $d) {
							if($this->serviceEntities->entityExists($d) && $d instanceOf CrudInterface) {
								if($d->needPostNewEventsLoad()) $this->serviceEntities->applyEvent($d, BaseAnnotation::postNew);
							}
						}
					}
				}
			);
		}

		$this->buildAnnotated($entity, $builder, $options);

	}


	protected function buildAnnotated($entity, FormBuilderInterface $builder, array $options) {
		// $entity = $builder->getData();
		// $cm = is_object($entity) ? $this->serviceEntities->getEntityManager()->getClassMetadata($entity) : null;
		// $this->entity = $entity;
		$properties = $entity->getForm_CrudsAnnotations($options, $this->container->get(serviceKernel::class)->isDev());
		// $allannots = $this->serviceCruds->getAllAnnotations($entity, 'Update');
		// echo('<div class="well well-sm"><h4>Annotations for '.$entity->getShortname().'</h4><pre>');
		// var_dump($properties);
		// echo('</pre></div>');

		if(empty($properties)) throw new AccessDeniedHttpException("Vous n'avez aucun droit pour créer/modifier cet élément.");
		
		foreach ($properties as $property) {
			// if($property['name'] === 'fulltext') {
			// 	echo('<div class="well well-sm"><h4>Annotations for '.$entity->getShortname().' > '.$property['name'].'</h4><pre>');
			// 	var_dump($property['annotations']['annot']);
			// 	die('</pre></div>');
			// }
			$attrs = reset($property['annotations']['cruds']);
			if($attrs instanceOf CrudsAnnotation && $attrs->show) {
				$valid_field = true;
				$property_info = $this->serviceEntities->getPropertyDescription($property['name'], $entity);
				// echo('<div class="well well-sm"><h3>Annotations for '.$entity->getShortname().' > '.$property['name'].'</h3><pre>');
				// var_dump($attrs);
				// echo('<hr>');
				// var_dump($property_info);
				// echo('</pre></div>');
				$baseData = array(
					// 'translation_domain' => $entity->getShortname(),
					// 'translation_domain' => serviceTwgTranslation::DEFAULT_DOMAIN,
					// 'label' => is_string($attrs->label) ? $attrs->label : $property['name'],
					'by_reference' => false,
					// 'class' => is_string($attrs->class) ? $attrs->class : get_class($entity),
				);
				$data = isset($attrs->options) ? $attrs->options : $baseData;
				// if(!isset($data['attr'])) $data['attr'] = [];
				// Options as EVAL
				foreach ($data as $name => $value) {
					if(is_string($value) && $this->serviceCruds->evaluateExpression($value, $entity)) $data[$name] = $value;
/*					if(is_string($value) && preg_match('/(object|user|environment|entreprise)\\./i', $value)) {
						$user = $this->serviceContext->getUser();
						$environment = $this->serviceContext->getEnvironment();
						$entreprise = $this->serviceContext->getEntreprise();
						$test_string = preg_replace(['/(?!\\w)object\\./i','/(?!\\w)user\\./i','/(?!\\w)environment\\./i','/(?!\\w)entreprise\\./i'], ['$entity->','$user->','$environment->','$entreprise->'], $value);
						echo('<h2>Test string: '.json_encode($test_string !== $value).'</h2><p>'.$value.'</p><p><strong>'.$test_string.'</strong></p>');
						if($test_string !== $value) eval('$data[$name] = '.$test_string.';');
					}*/
				}
				// Attrs as EVAL
				$tests = ['show','update','type','label','getter','setter'];
				foreach ($tests as $name) {
					$eval = null;
					if(strtolower($attrs->$name) === 'auto') {
						$method = Inflector::camelize('get_update_crud_'.$name);
						if(method_exists($entity, $method)) {
							// Search method in entity
							$eval = '$attrs->$name = $entity->'.$method.'();'; // Ex.: getUpdateCrudType();
						} else {
							// Then search method in entity service
							$entityManager = $this->serviceEntities->getEntityService($entity);
							if(method_exists($entityManager, $method)) {
								$eval = '$attrs->$name = $entityManager->'.$method.'($entity);'; // Ex.: getUpdateCrudType($entity);
							} else {
								throw new Exception("Error ".__METHOD__."(): ".json_encode($name)." could not find auto method ".json_encode($method)." in entity or entity service (".json_encode(get_class($entityManager)).")!", 1);
							}
						}
					} else if(is_string($attrs->$name) && preg_match('/object\\./i', $attrs->$name)) {
						$eval = '$data[$name] = '.preg_replace('/object\\./i', '$entity->', $value).';';
					} else if(is_array($attrs->$name)) {
						if(!array_key_exists('eval', $attrs->$name)) throw new Exception("Error ".__METHOD__."(): ".json_encode($name)." is array, but does not have eval item!", 1);
						$eval = '$attrs->$name = '.preg_replace('/object\\./i', '$entity->', $attrs->$name['eval']).';';
					}
					if(is_string($eval)) eval($eval);
				}
				// Find TYPE
				if(!class_exists($attrs->type)) {
					foreach (Update::getTypesNamespaces() as $type) {
						if(preg_match('/Type$/i', $attrs->type)) {
							$attrs->type = preg_replace('/type$/', 'Type', $attrs->type);
							if(class_exists($type.$attrs->type)) { $attrs->type = $type.$attrs->type; break; }
							if(class_exists($type.ucfirst($attrs->type))) { $attrs->type = $type.ucfirst($attrs->type); break; }
						} else {
							if(class_exists($type.$attrs->type.'Type')) { $attrs->type = $type.$attrs->type.'Type'; break; }
							if(class_exists($type.ucfirst($attrs->type.'Type'))) { $attrs->type = $type.ucfirst($attrs->type.'Type'); break; }
						}
					}
				}
				if(!class_exists($attrs->type)) $attrs->type = null;
				// enabled/disabled
				if(!$attrs->update) $data['disabled'] = true;
				// translation_domain
				if(!isset($data['translation_domain'])) $data['translation_domain'] = $entity->getShortname();
				// label
				if(!isset($data['label'])) $data['label'] = is_string($attrs->label) ? $attrs->label : 'field.'.$property['name'];

				if(isset($property['annotations']['annot']['translatable'])) {
					// $data['translatator'] = true;
					// $data['attr']['data-translatator'] = true;
					foreach ($property['annotations']['annot']['translatable'] as $transl) {
						// echo('<div class="well well-sm"><h4>Translatable for '.$entity->getShortname().' > '.$property['name'].'</h4><pre>');
						// var_dump($attrs->type);
						// var_dump($transl);
						// echo('</pre></div>');
						if($transl->type === 'auto' && isset($property['annotations']['orm']['column'])) {
							$column = reset($property['annotations']['orm']['column']);
							// echo('<div class="well well-sm"><h4>ORM Annotations for '.$entity->getShortname().' > '.$property['name'].'</h4><pre>');
							// var_dump($column);
							// echo('</pre></div>');
							if($column instanceOf Column) {
								switch ($column->type) {
									case 'string':
										$transl->type = "text";
										break;
									case 'text':
										$transl->type = "html";
										break;
									case 'integer':
									case 'decimal':
										$transl->type = "numeric";
										break;
									case 'datetime':
										$transl->type = "datetime";
										break;
								}
							}
						}
						switch ($transl->type) {
							case 'text':
								$attrs->type = TransTextType::class;
								break;
							case 'textarea':
								$attrs->type = TransTextareaType::class;
								break;
							case 'html':
								$attrs->type = TransSummernoteType::class;
								break;
							// case 'numeric':
							// 	$attrs->type = IntegerType::class;
							// 	break;
							// case 'datetime':
							// 	$attrs->type = TextType::class;
							// 	break;
							default:
								$attrs->type = TransTextType::class;
								break;
						}
					}
				}
				// if(isset($data['choices'])) { echo('<pre>'); var_dump($data); die('</pre>'); }
				// if(in_array($attrs->type, [EntityType::class, ChoiceType::class, OriginalEntityType::class, OriginalChoiceType::class])) {
				if(in_array($attrs->type, [EntityType::class, OriginalEntityType::class, CompactentitychoiceType::class])) {
					if(!isset($data['class'])) {
						$descript = $this->serviceEntities->getPropertyDescription($property['name'], $entity);
						if(isset($descript['targetEntity'])) $data['class'] = $descript['targetEntity'];
					}
					if(isset($data['query_builder'])) { // default: 'qb_findAll'
						if(strtolower($data['query_builder']) === 'auto') $data['query_builder'] = 'qb_findAll';
						$repo = $this->serviceEntities->getRepository($data['class']);
						if(method_exists($repo, $data['query_builder'])) $data['query_builder'] = $repo->{$data['query_builder']}(null, $entity, $this->user);
							else $data['query_builder'] = null;
					} else {
						// $data['query_builder'] = null;
					}
					// if(!isset($data['em'])) {
					// 	$data['em'] = $this->serviceEntities->getEntityManager();
					// }
					// Group by root parent name
					if($entity instanceOf NestedInterface && isset($descript['targetEntity']) && $descript['targetEntity'] === Directory::class) {
						$data['group_by'] = function($choice, $key, $value) {
							$rootparent = $choice->getRootparent();
							return $rootparent instanceOf Directory ? $rootparent->getName() : null;
						};
					}
				}
				// Choices
				if(isset($data['choices'])) {
					if(strtolower($data['choices']) === 'auto') {
						$method = 'get'.ucfirst($property['name']).'Choices';
						if(!method_exists($entity, $method)) $method = Inflector::camelize('get_'.$property['name'].'_choices');
						if(method_exists($entity, $method)) $data['choices'] = $entity->$method();
							else throw new Exception("Error while creating choices for ".json_encode($property['name']).": AUTO method ".json_encode($method)."() does not exist!", 1);
					} else if(preg_match('/[\\w-]+/', $data['choices'])) {
						$method = $data['choices'];
						if(method_exists($entity, $method)) $data['choices'] = $entity->$method();
							else throw new Exception("Error while creating choices for ".json_encode($property['name']).": method ".json_encode($method)."() does not exist!", 1);
					} else {
						$data['choices'] = eval('return '.$data['choices'].';');
					}
					// Remove field if choices is empty
					if(empty($data['choices'])) $valid_field = false;
					if(!isset($data['expanded'])) $data['expanded'] = false;
				}
				// if(in_array($attrs->type, [EntityType::class, ChoiceType::class])) {
				// 	if(is_bool($attrs->multiple)) $data['multiple'] = $attrs->multiple;
				// 	if(is_bool($attrs->expanded)) $data['expanded'] = $attrs->expanded;
				// }
				// Collection
				// if(in_array($attrs->type, [ParentCollectionType::class, CollectionType::class])) {
					// 
				// }
				if(in_array($attrs->type, [FileType::class])) {
					// if(!isset($data['attr'])) $data['attr'] = [];
					if(!isset($data['attr']['accept']) || strtolower($data['attr']['accept']) === 'auto') {
						if(method_exists($entity, 'getValidContentTypes')) {
							// $contentTypes = $this->FileManager->getContentTypesByMediaTypes($entity->getAuthorizedTypes(), true);
							$data['attr']['accept'] = $entity->getValidContentTypes();
						} else if(method_exists($property_info['targetEntity'], 'getValidContentTypes')) {
							$manager = $this->serviceEntities->getEntityService($property_info['targetEntity']);
							$test = $manager instanceOf servicesBaseEntityInterface ?
								$manager->createNew([], function($item) use ($options) {
									// if(($options['owner'] ?? null) instanceOf Tier) $item->setOwner($options['owner']);
								}):
								$this->serviceEntities->createNew($property_info['targetEntity'], [], function($item) use ($options) {
									// if(($options['owner'] ?? null) instanceOf Tier) $item->setOwner($options['owner']);
								});
							$this->serviceEntities->preCreateForForm($test);
							$data['attr']['accept'] = $test->getValidContentTypes();
						} else {
							$data['attr']['accept'] = '*';
						}
					}
					$data['attr']['accept'] ??= '*';
				}
				// if($this->user->getUsername() == "françoiseSA" && isset($data['attr']['accept'])) {
				if($this->user instanceOf User && preg_match('/^fran(ç|c)oiseSA$i/', $this->user->getUsername())  && isset($data['attr']['accept'])) {
					$data['attr']['accept'] = '*';
				}

				if($valid_field) {
					if(in_array($attrs->type, [FormType::class])) {
						// echo('<p>Added FormType '.json_encode($property['name']).'</p>');
						$builder->add(
							$builder->create(
								$property['name'],
								$attrs->type,
								$data
								// array_merge($data, ['owner' => $entity instanceOf Tier ? $entity : $entity->getOwner()]),
							)
						);
					} else {
						$builder->add(
							$property['name'],
							$attrs->type,
							$data
						);
					}
					// Add model transformers
					foreach ($property['annotations']['annot'] as $annots) {
						foreach ($annots as $annot) {
							// $model_entity = is_object($builder->getData()) ? null : $entity;
							// if(empty($model_entity) && static::SETDATA_IF_NO_ENTITY) {
							// 	$model_entity = $builder->getData();
							// }
							// if(!is_object($model_entity)) throw new Exception("Error ".__METHOD__."(): entity is not an entity! Got ".gettype($model_entity)."!", 1);
							if(is_object($entity) && method_exists($annot, 'addModelTransformer')) $annot->addModelTransformer($property['name'], $builder, $this->serviceEntities, $entity);
						}
					}
					// Apply postNew Events on new entities
					// if(is_object($entity) && $entity instanceOf CrudInterface && $entity->needPostNewEventsLoad()) $this->serviceEntities->applyEvent($entity, BaseAnnotation::postNew);
					if(static::APPLY_EVENTS === 'CALLBACKTRANSFORMER') {
						$prop = $property['name'];
						$builder->get($property['name'])->addModelTransformer(
							new CallbackTransformer(
								function ($data) {
									return $data;
								},
								function ($data) {
									// if(is_iterable($data)) {
									// 	foreach ($data as $child_entity) {
									// 		if($this->serviceEntities->entityExists($child_entity) && $child_entity instanceOf CrudInterface) {
									// 			if($child_entity->needPostNewEventsLoad()) $this->serviceEntities->applyEvent($child_entity, BaseAnnotation::postNew);
									// 		}
									// 	}
									// } else if(is_object($data) && $this->serviceEntities->entityExists($data)) {
										if($data instanceOf CrudInterface) {
											if($data->needPostNewEventsLoad()) $this->serviceEntities->applyEvent($data, BaseAnnotation::postNew);
										}
									// }
									// $this->printData($prop, $data);
									return $data;
								}
							)
						);
					}
				}
			}
		}
		/** @see https://phpdox.net/demo/Symfony2/classes/Symfony_Component_Form_FormBuilder.xhtml */
		if($builder->count() <= 0) throw new AccessDeniedHttpException("Vous n'avez aucun droit pour créer/modifier les données de cet élément.");
		// TOMCAT
		// if($entity->isTomcat()) {
			$builder->add(
				'tomcat',
				HiddenType::class,
				[
					'attr' => ['data-tomcat' => json_encode($entity->getTomcat())],
					'by_reference' => false,
				],
			);
		// }
		if($options['submit']) {
			$builder->add('submit', SubmitType::class, array(
				'label' => 'Enregistrer',
				'attr' => array('class' => "btn btn-md btn-info btn-block")
				)
			);
			if($options['submits']) {
				$builder->add('submit_show', SubmitType::class, array(
					'label' => 'Enregistrer et voir',
					'attr' => array('class' => "btn btn-md btn-info btn-block")
					)
				);
				if($options['cruds_action'] === 'create') {
					$builder->add('submit_new', SubmitType::class, array(
						'label' => 'Enregistrer et nouveau',
						'attr' => array('class' => "btn btn-md btn-info btn-block")
						)
					);
				}
			}
		}
		return $this;
	}

	private function printData($prop, $data) {
		$validator = $this->container->get('validator');
		echo('<div><u>ModelTransformer for <strong style="color: blue;">'.$prop.'</strong></u></div>');
		echo('<div>- Data: '.(is_object($data) ? get_class($data) : gettype($data)).'</div>');
		if(is_iterable($data)) {
			if($data instanceOf ArrayCollection) {
				$count = is_object($data) && method_exists($data, 'count') ? $data->count() : count($data);
				echo('<div>- '.(is_object($data) ? '<span title="'.get_class($data).'">'.serviceClasses::getShortname($data).'</span>' : gettype($data)).' contains: '.($count ? '<strong style="color: orange;">'.$count.'</strong>' : $count));
				if($count) {
					echo('<ul>');
					foreach ($data as $key => $item) {
						$violations = $validator->validate($item);
						echo('<li>'.(is_object($item) ? get_class($item).'<i style="color: green;"> => '.$item->getName().'</i>' : gettype($item)).(count($violations) > 0 ? ' <span style="color: red;">Violations : '.count($violations).'</span>' : '').'</li>');
					}
					echo('</ul>');
				}
				echo('</div>');
			}
		} else if(is_object($data)) {
			$violations = $validator->validate($data);
			echo('<div>- '.(is_object($item) ? get_class($item).'<i style="color: green;"> => '.$item->getName().'</i>' : gettype($item)).(count($violations) > 0 ? ' <span style="color: red;">Violations : '.count($violations).'</span>' : '').'</div>');
		} else {
			switch (gettype($data)) {
				case 'string':
					echo('<div>- value: '.strip_tags($data).' <i style="color: lightgray">('.gettype($data).')</i></div>');
					break;
				case 'integer':
					echo('<div>- value: '.$data.' <i style="color: lightgray">('.gettype($data).')</i></div>');
					break;				
				default:
					echo('<div>- value: <i style="color: lightgray">('.gettype($data).')</i></div>');
					break;
			}
		}
	}

	// protected function getCrudsContext() {
	// 	return static::USER_DEFAULT_CONTEXT;
	// }

	// protected function getDisabled($propertyName) {
	// 	if(!isset($this->options['translation']) || false === $this->options['translation']) return false;
	// 	return $this->properties[$propertyName] === null;
	// }

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => static::ENTITY_CLASSNAME,
			'method' => 'POST',
			// 'csrf_protection' => false,
			'submit' => false,
			'submits' => false,
			'attr' => array(
				// 'class' => 'form-horizontal'
				),
			'cruds_context' => static::USER_DEFAULT_CONTEXT,
			'large_footer' => true,
			'hidden_data' => null,
			// 'owner' => null,
			// 'owner' => $this->container->get(serviceUser::class)->getDefaultEntrepriseOrCurrentUserOrRoot(),
		));
		$resolver->setRequired('cruds_action');
	}

	/**
	 * Add Form options to BuildView
	 * @see https://stackoverflow.com/questions/23832447/access-variable-from-formtype-in-types-twig-template
	 */
	public function buildView(FormView $view, FormInterface $form, array $options) {
		parent::buildView($view, $form, $options);
		$view->vars = array_merge($view->vars, ['form_options' => $options]);
		return $this;
	}

	// /**
	//  * {@inheritdoc}
	//  */
	// public function getBlockPrefix() {
	// 	return $this->name;
	// }

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}



	public static function isActionValid($action, $exception = true) {
		$valid = in_array($action, static::ACTIONS);
		if(!$valid && $exception) throw new Exception("Error ".__METHOD__."(): this action ".json_encode($action)." is not valid!", 1);
		return $valid;
	}

	/**
	 * Get contexts related to User
	 * @return array
	 */
	public static function getDefaultUserContexts() {
		$contexts = [
			'user', // when element in form is user himself or belongs to user (owner)
			'translation', // when user has role ROLE_TRANSLATOR
			static::USER_DEFAULT_CONTEXT,
			'medium',
			'expert',
			static::USER_HIGH_CONTEXT,
		];
		return $contexts;
	}

	public static function completeUserContexts($contexts) {
		if(in_array(static::USER_HIGH_CONTEXT, $contexts)) {
			$contexts[] = 'expert';
			$contexts[] = 'translation';
		}
		if(in_array('expert', $contexts)) $contexts[] = 'medium';
		if(in_array('medium', $contexts)) $contexts[] = static::USER_DEFAULT_CONTEXT;
		return $contexts;
	}

	public static function getHigherContext($contexts) {
		$tests = [static::USER_HIGH_CONTEXT, 'expert', 'medium', static::USER_DEFAULT_CONTEXT];
		foreach ($tests as $test) if(in_array($test, $contexts)) return $test;
		return null;
	}

	public static function getDefaultUserContext() {
		return static::USER_DEFAULT_CONTEXT;
	}

	/**
	 * Get contexts related to User / for choicelist
	 * @return array
	 */
	public static function getChoicesUserContexts() {
		$contexts = [
			// 'user', // when element in form is user himself or belongs to user (owner)
			// 'translation', // when user has role ROLE_TRANSLATOR
			static::USER_DEFAULT_CONTEXT,
			'medium',
			'expert',
			static::USER_HIGH_CONTEXT,
		];
		return $contexts;
	}

	public static function contextsToString($contexts) {
		if(empty($contexts)) $contexts = "";
		if(is_string($contexts)) return $contexts;
		if(!is_array($contexts)) throw new Exception("Error ".__METHOD__."(): contexts must be null, string separate by | or array. Got ".gettype($contexts)."!", 1);
		return implode('|', $contexts);
	}

	public static function contextsToArray($contexts) {
		if(empty($contexts)) $contexts = [];
		if(is_array($contexts)) return $contexts;
		if(!is_string($contexts)) throw new Exception("Error ".__METHOD__."(): contexts must be null, string separate by | or array. Got ".gettype($contexts)."!", 1);
		return explode('|', $contexts);
	}

	public static function getContextDataType($contexts) {
		if(is_string($contexts)) return 'string';
		if(is_array($contexts)) return 'array';
		return null;
	}

	public static function normalizeContexts(&$contexts, $setAs = false) {
		if(empty($contexts)) $contexts = static::getDefaultUserContext();
		$type = static::getContextDataType($contexts);
		if($setAs === "array") $type = 'array';
		if($setAs === "string") $type = 'string';
		$contexts = static::contextsToString($contexts);
		// 1. to lower
		$contexts = strtolower($contexts);
		// 2. compute
		// if(in_array(static::USER_HIGH_CONTEXT, $contexts)) {
		// 	$contexts[] = static::USER_DEFAULT_CONTEXT;
		// 	$contexts[] = 'medium';
		// 	$contexts[] = 'expert';
		// }
		$contexts = static::contextsToArray($contexts);
		$user_contexts = static::filterUserContexts($contexts);
		if(empty($user_contexts)) $contexts[] = static::getDefaultUserContext();
		$contexts = array_unique($contexts);
		switch ($type) {
			case 'string':
				return static::contextsToString($contexts);
				break;
			case 'array':
				return static::contextsToArray($contexts);
				break;
			default: // array
				throw new Exception("Error ".__METHOD__."(): contexts must be null, string separate by | or array. Got ".gettype($contexts)."!", 1);
				break;
		}
	}

	/**
	 * Get only USER contexts
	 * @param array $contexts
	 * @return array
	 */
	public static function filterUserContexts($contexts, $setAs = 'array') {
		$contexts = array_intersect(static::contextsToArray($contexts), static::getDefaultUserContexts());
		switch ($setAs) {
			case 'string':
				return static::contextsToString($contexts);
				break;
			case 'array':
				return static::contextsToArray($contexts);
				break;
		}
	}

	/**
	 * Get only FORM contexts
	 * @param array $contexts
	 * @return array
	 */
	public static function filterFormContexts($contexts, $setAs = 'array') {
		$contexts = array_diff(static::contextsToArray($contexts), static::getDefaultUserContexts());
		switch ($setAs) {
			case 'string':
				return static::contextsToString($contexts);
				break;
			case 'array':
				return static::contextsToArray($contexts);
				break;
		}
	}

	/**
	 * Get form contexts
	 * Returns all contexts separated by |
	 * @return array
	 */
	public function getFormAllContexts(Tier $user = null, $formContexts = null) {
		if($user instanceOf Tier && !($user instanceOf User)) $user = $user->getOwner();
		$user_contexts = $this->getUserContexts($user);
		$form_contexts = static::filterFormContexts($formContexts);
		return array_unique(array_merge($user_contexts, $form_contexts));
	}

	public function getUserContexts(Tier $user = null) {
		if($user instanceOf Tier && !($user instanceOf User)) $user = $user->getOwner();
		if(!($user instanceOf Tier)) $user = $this->user;
		if($user instanceOf Tier) $user_contexts = $user->getPreference('forms/contexts', [static::USER_DEFAULT_CONTEXT], false, true);
			else $user_contexts = [static::USER_DEFAULT_CONTEXT];
		if(!count($user_contexts)) throw new Exception("Error ".__METHOD__."(): could not find any context for user.", 1);
		return static::normalizeContexts($user_contexts);
	}

}