<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\TextareaType as ParentTextareaType;
// use ModelApi\InternationalBundle\Entity\ModelLanguage;

class TextareaType extends AbstractType {


	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'attr' => array('style' => 'resize: vertical;height: 120px;'),
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'Textarea';
	}

	public function getParent() {
		return ParentTextareaType::class;
	}
}
