<?php
namespace ModelApi\BaseBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\DoctrineType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\ChoiceList\Factory\CachingFactoryDecorator;
use Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface;
use Symfony\Component\Form\ChoiceList\Factory\DefaultChoiceListFactory;
use Symfony\Component\Form\ChoiceList\Factory\PropertyAccessDecorator;
use Symfony\Component\Form\ChoiceList\ChoiceListInterface;
use Symfony\Component\Form\ChoiceList\View\ChoiceGroupView;
use Symfony\Component\Form\ChoiceList\View\ChoiceListView;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\Persistence\ObjectManager as LegacyObjectManager;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\ChoiceList\ORMQueryBuilderLoader;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Exception\RuntimeException;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Doctrine\ORM\PersistentCollection;
use Doctrine\Common\Collections\ArrayCollection;

class CompactentitychoiceType extends AbstractType {

	private $registry;
	private $choiceListFactory;

	/**
	 * @param ManagerRegistry|LegacyManagerRegistry $registry
	 * @param ChoiceListFactoryInterface $choiceListFactory = null
	 */
	public function __construct(ManagerRegistry $registry, ChoiceListFactoryInterface $choiceListFactory = null) {
		$this->registry = $registry;
		$this->choiceListFactory = $choiceListFactory ?: new CachingFactoryDecorator(
			new PropertyAccessDecorator(
				new DefaultChoiceListFactory()
			)
		);
		return $this;
	}

	public function buildForm(FormBuilderInterface $builder, array $options) {
		// $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
		// 	$data = $event->getData();
		// 	var_dump($data);die();
		// 	if (!\is_array($data)) {
		// 		return;
		// 	}

		// 	foreach ($data as $v) {
		// 		if (null !== $v && !\is_string($v) && !\is_int($v)) {
		// 			throw new TransformationFailedException('All choices submitted must be NULL, strings or ints.');
		// 		}
		// 	}
		// }, 256);
		$builder->addModelTransformer(new CallbackTransformer(
			function ($values) use ($options) {
				if(is_array($values) || $values instanceof PersistentCollection) {
					echo('<h3>Multiple '.gettype($values).' of values:</h3><ul>');
					foreach ($values as $key => $value) {
						echo('<li>- '.(is_object($value) ? $value->getName() : $value).'</li>');
					}
					echo('</ul>');
				} else {
					echo('<h3>Single '.gettype($values).' value:</h3><ul><li>'.(is_object($values) ? $values->getName() : $values).'</li></ul>');
				}
				die();
				if($options['multiple']) {
					if($values instanceof PersistentCollection) $values = $values->toArray();
					if(!is_array($values)) $values = [$values];
					$values = array_map(function($value) {
						return is_object($value) ? $value->getName() : $value;
					}, $values);
				} else {
					if($values instanceof PersistentCollection) $values = $values->first();
					if(is_array($values)) $values = reset($values);
					if(is_object($values)) $values = $values->getName();
				}
				// die(is_array($values) ? 'Array: '.implode(' / ', $values) : 'Value: '.json_encode($values));
				return $values;
			},
			function ($values) use ($options) {
				// if(is_string($values));
			}
		));

		parent::buildForm($builder, $options);
	}

	public function configureOptions(OptionsResolver $resolver) {
		parent::configureOptions($resolver);
		$resolver->setDefault('attr', array('class' => 'select-choice'));
		$resolver->setDefault('query_builder', null);
		$resolver->setDefault('class', null);
		$resolver->setDefault('em', null);
		// EntityManager
		$emNormalizer = function (Options $options, $em) {
			if (null !== $em) return $em instanceof ObjectManager || $em instanceof LegacyObjectManager ? $em : $this->registry->getManager($em);
			$em = $this->registry->getManagerForClass($options['class']);
			if (null === $em) throw new RuntimeException(sprintf('Class "%s" seems not to be a managed Doctrine entity. Did you forget to map it?', $options['class']));
			return $em;
		};
		$resolver->setNormalizer('em', $emNormalizer);
		$resolver->setAllowedTypes('em', ['null', 'string', ObjectManager::class, LegacyObjectManager::class]);
		// QueryBuilder
		// Invoke the query builder closure so that we can cache choice lists
		// for equal query builders
		$queryBuilderNormalizer = function (Options $options, QueryBuilder $queryBuilder = null) {
			if (\is_callable($queryBuilder)) {
				$queryBuilder = \call_user_func($queryBuilder, $options['em']->getRepository($options['class']));
			}
			return $queryBuilder;
		};
		$resolver->setNormalizer('query_builder', $queryBuilderNormalizer);
		$resolver->setAllowedTypes('query_builder', ['null', 'callable', 'Doctrine\ORM\QueryBuilder']);
	}

	/**
	 * Return the default loader object.
	 * @param QueryBuilder $queryBuilder
	 * @param string       $class
	 * @return ORMQueryBuilderLoader
	 */
	public function getLoader(LegacyObjectManager $manager, $queryBuilder, $class) {
		return new ORMQueryBuilderLoader($queryBuilder);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'compactentitychoice';
	}

	public function getParent() {
		return ChoiceType::class;
	}

	// /**
	//  * We consider two query builders with an equal SQL string and
	//  * equal parameters to be equal.
	//  * @param QueryBuilder $queryBuilder
	//  * @return array
	//  * @internal This method is public to be usable as callback. It should not
	//  *           be used in user code.
	//  */
	// public function getQueryBuilderPartsForCachingHash($queryBuilder) {
	//     return [
	//         $queryBuilder->getQuery()->getSQL(),
	//         array_map([$this, 'parameterToArray'], $queryBuilder->getParameters()->toArray()),
	//     ];
	// }

	// /**
	//  * Converts a query parameter to an array.
	//  * @param Parameter $parameter
	//  * @return array The array representation of the parameter
	//  */
	// private function parameterToArray(Parameter $parameter) {
	//     return [$parameter->getName(), $parameter->getType(), $parameter->getValue()];
	// }

	/**
	 * Adds the sub fields for an expanded choice field.
	 */
	private function addSubForms(FormBuilderInterface $builder, array $choiceViews, array $options)
	{
		foreach ($choiceViews as $name => $choiceView) {
			// Flatten groups
			if (\is_array($choiceView)) {
				$this->addSubForms($builder, $choiceView, $options);
				continue;
			}

			if ($choiceView instanceof ChoiceGroupView) {
				$this->addSubForms($builder, $choiceView->choices, $options);
				continue;
			}

			$this->addSubForm($builder, $name, $choiceView, $options);
		}
	}

	private function addSubForm(FormBuilderInterface $builder, $name, ChoiceView $choiceView, array $options)
	{
		$choiceOpts = [
			'value' => $choiceView->value,
			'label' => $choiceView->label,
			'attr' => $choiceView->attr,
			'translation_domain' => $options['choice_translation_domain'],
			'block_name' => 'entry',
		];

		if ($options['multiple']) {
			$choiceType = CheckboxType::class;
			// The user can check 0 or more checkboxes. If required
			// is true, they are required to check all of them.
			$choiceOpts['required'] = false;
		} else {
			$choiceType = RadioType::class;
		}

		$builder->add($name, $choiceType, $choiceOpts);
	}

	private function createChoiceList(array $options)
	{
		if (null !== $options['choice_loader']) {
			return $this->choiceListFactory->createListFromLoader(
				$options['choice_loader'],
				$options['choice_value']
			);
		}

		// Harden against NULL values (like in EntityType and ModelType)
		$choices = null !== $options['choices'] ? $options['choices'] : [];

		return $this->choiceListFactory->createListFromChoices($choices, $options['choice_value']);
	}

	private function createChoiceListView(ChoiceListInterface $choiceList, array $options)
	{
		return $this->choiceListFactory->createView(
			$choiceList,
			$options['preferred_choices'],
			$options['choice_label'],
			$options['choice_name'],
			$options['group_by'],
			$options['choice_attr']
		);
	}

}
