<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\TextType;
// use ModelApi\InternationalBundle\Entity\ModelLanguage;

class LocalarrayType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		// When empty_data is explicitly set to an empty string,
		// a string should always be returned when NULL is submitted
		// This gives more control and thus helps preventing some issues
		// with PHP 7 which allows type hinting strings in functions
		// See https://github.com/symfony/symfony/issues/5906#issuecomment-203189375
		if ('' === $options['empty_data']) {
			$builder->addViewTransformer($this);
		}

		// $builder->addViewTransformer(
		// 	new CallbackTransformer(
		// 		function ($data) use ($builder) {
		// 			// front : transform data
		// 			return $data;
		// 		},
		// 		function ($data) {
		// 			// back : untransform data
		// 			return $data;
		// 		}
		// 	)
		// );
	}

	// public function configureOptions(OptionsResolver $resolver) {
	// 	$resolver->setDefaults(array(
	// 		'compound' => false,
	// 	));
	// }

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'localarray';
	}

	// /**
	//  * {@inheritdoc}
	//  */
	// public function transform($data) {
	// 	// Model data should not be transformed
	// 	return $data;
	// }

	public function getParent() {
		return TextType::class;
	}
}
