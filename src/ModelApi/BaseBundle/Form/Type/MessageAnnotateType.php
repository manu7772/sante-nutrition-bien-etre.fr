<?php
namespace ModelApi\BaseBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// BaseBundle
use ModelApi\BaseBundle\Entity\Message;

// use \ReflectionClass;

class MessageAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Message::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}