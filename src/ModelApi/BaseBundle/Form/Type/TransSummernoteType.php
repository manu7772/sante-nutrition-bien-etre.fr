<?php
namespace ModelApi\BaseBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
// use ModelApi\InternationalBundle\Entity\ModelLanguage;

class TransSummernoteType extends TextareaType {


	public function configureOptions(OptionsResolver $resolver) {
		$resolver
			->setRequired(['translatator'])
			// ->setDefined(['translatator'])
			->addAllowedTypes('translatator', ['boolean'])
			->addAllowedValues('translatator', [true, false])
			->setDefaults(array(
				'translatator' => true,
				'attr' => array(
					// 'style' => 'resize: vertical;height: 120px;',
					'data-summernote-component' => '{}',
					// 'class' => 'summernote',
					// 'on-init' => "init()",
					// 'on-enter' => "enter()",
					// 'on-focus' => "focus(evt)",
					// 'on-blur' => "blur(evt)",
					// 'on-paste' => "paste()",
					// 'on-keyup' => "keyup(evt)",
					// 'on-keydown' => "keydown(evt)",
					// 'on-change' => "change(contents)",
					// 'on-image-upload' => "imageUpload(files)",
					// 'editable' => "editable",
					// 'editor' => "editor",
					'data-translatator' => true,
				),
			));
	}

	public function getName() {
		return 'transsummernote';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'transsummernote';
	}

	public function getParent() {
		return TextareaType::class;
	}
}
