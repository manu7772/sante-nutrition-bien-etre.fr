<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
// Paramétrage de formulaire
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use ModelApi\BaseBundle\Form\Type\LocalarrayType;
use ModelApi\InternationalBundle\Entity\Localtext;

class LocaltextUpdateType extends AbstractType {

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		// Builder…

		// Default
		$builder
			->add('texts', LocalarrayType::class, array(
				'by_reference' => false,
				'description' => "Modifiez les données de chaque langue en cliquant sur les onglets",
				))
			;

		if($options['submit']) {
			$builder->add('submit', SubmitType::class, array(
				'label' => 'Enregistrer',
				'attr' => array(
					'class' => "btn btn-sm btn-primary",
					),
				))
			;
		}

		$builder->addEventListener(
			FormEvents::PRE_SET_DATA, function(FormEvent $event) {
				$data = $event->getData();
				if(!($data instanceOf Localtext)) return;

				$form = $event->getForm();
				if(null != $data->getFormType()) {
					if($form->has('texts')) { $form->remove('texts'); }
					$form->add('texts', $data->getFormType(), array(
						'by_reference' => false,
						'description' => "Modifiez les données de chaque langue en cliquant sur les onglets",
						))
					;
				}
			}
		);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => Localtext::class,
			// 'csrf_protection' => false,
			'submit' => false,
			'attr' => array(
				'class' => 'form-horizontal'
			),
		));
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'modelapi_apibundle_update_localtext';
	}

}