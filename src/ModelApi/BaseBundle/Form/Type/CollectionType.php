<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
// BaseBundle
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// FileBundle
use ModelApi\FileBundle\Service\FileManager;

use Symfony\Component\Form\Extension\Core\Type\CollectionType as ParentCollectionType;
// use ModelApi\InternationalBundle\Entity\ModelLanguage;

class CollectionType extends AbstractType {


	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'attr' => array('data-collection-type' => true), // can be caugth by Angular 
			// 'cruds_action' => BaseAnnotateType::USER_DEFAULT_CONTEXT,
			'cruds_action' => 'create',
			'cruds_context' => BaseAnnotateType::COLLECTION_CONTEXT,
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'Collection';
	}

	public function getParent() {
		return ParentCollectionType::class;
	}
}
