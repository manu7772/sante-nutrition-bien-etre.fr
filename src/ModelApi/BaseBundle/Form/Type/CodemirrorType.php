<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
// use ModelApi\InternationalBundle\Entity\ModelLanguage;

class CodemirrorType extends AbstractType {


	public function configureOptions(OptionsResolver $resolver) {
		$options = [
			'mode' => 'javascript', // --> MIMES : https://www.iana.org/assignments/media-types/media-types.xhtml
			'lineNumbers' => true,
			'lineWrapping' => false,
			'theme' => 'blackboard',
			// 'theme' => 'ambiance',
			// 'CodeMirror-lines' => 20,
			// 'mode' => ['name' => 'javascript', 'json' => true],
			// 'value' => '<script>var test = "bonjour";</script>',
			'matchBrackets' => true,
			'styleActiveLine' => true,
			'mode' => 'application/x-httpd-php',
		];
		// echo('<pre>'); var_dump($this->getData()); echo('</pre>');
		$resolver->setDefaults(array(
			'attr' => array(
				'data-codemirror-component' => json_encode($options),
				'style' => 'resize: vertical;',
			),
		));
	}

	public function getName() {
		return 'codemirror';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'codemirror';
	}

	public function getParent() {
		return TextareaType::class;
	}
}
