<?php
namespace ModelApi\BaseBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
// use ModelApi\InternationalBundle\Entity\ModelLanguage;

class TransTextareaType extends TextareaType {


	public function configureOptions(OptionsResolver $resolver) {
		$resolver
			->setRequired(['translatator'])
			// ->setDefined(['translatator'])
			->addAllowedTypes('translatator', ['boolean'])
			->addAllowedValues('translatator', [true, false])
			->setDefaults(array(
				'translatator' => true,
				'attr' => array(
					'style' => 'resize: vertical;height: 120px;',
					'data-translatator' => true,
				),
			));
	}

	public function getName() {
		return 'transtextarea';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'transtextarea';
	}

	public function getParent() {
		return TextareaType::class;
	}
}
