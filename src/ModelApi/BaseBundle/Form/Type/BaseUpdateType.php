<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;

abstract class BaseUpdateType extends AbstractType {

	const ENTITY_CLASSNAME = null;

	protected $container;
	protected $serviceEntities;
	protected $user;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->user = $this->serviceEntities->getCurrentUserOrNull();
		return $this;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		// Builder…
		// Apply preUpdateForm Event
		$this->serviceEntities->preUpdateForForm($builder->getData());
	}


	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => static::ENTITY_CLASSNAME,
			// 'csrf_protection' => false,
			'submit' => false,
			'attr' => array(
				'class' => 'form-horizontal'
				),
		));
	}


}