<?php
namespace ModelApi\BaseBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\TextType;
// use ModelApi\InternationalBundle\Entity\ModelLanguage;

class TransTextType extends TextType {


	public function configureOptions(OptionsResolver $resolver) {
		$resolver
			->setRequired(['translatator'])
			// ->setDefined(['translatator'])
			->addAllowedTypes('translatator', ['boolean'])
			->addAllowedValues('translatator', [true, false])
			->setDefaults(array(
				'translatator' => true,
				// 'attr' => array('style' => 'resize: vertical;height: 120px;'),
				'attr' => ['data-translatator' => true],
			))
			;
	}

	public function getName() {
		return 'transtext';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'transtext';
	}

	public function getParent() {
		return TextType::class;
	}
}
