<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\RangeType;

class RangesliderType extends AbstractType {


	// public function configureOptions(OptionsResolver $resolver) {
	// 	$resolver->setDefaults(array(
	// 		'attr' => array(
	// 			'class' => 'range-slider',
	// 		),
	// 	));
	// }

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'insRangeSlider';
	}

	public function getParent() {
		return RangeType::class;
	}
}
