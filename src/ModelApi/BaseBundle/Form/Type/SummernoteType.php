<?php
namespace ModelApi\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
// use ModelApi\InternationalBundle\Entity\ModelLanguage;

class SummernoteType extends AbstractType {


	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'attr' => array(
				// 'style' => 'resize: vertical;height: 120px;',
				'data-summernote-component' => '{}',
				// 'class' => 'summernote',
				// 'on-init' => "init()",
				// 'on-enter' => "enter()",
				// 'on-focus' => "focus(evt)",
				// 'on-blur' => "blur(evt)",
				// 'on-paste' => "paste()",
				// 'on-keyup' => "keyup(evt)",
				// 'on-keydown' => "keydown(evt)",
				// 'on-change' => "change(contents)",
				// 'on-image-upload' => "imageUpload(files)",
				// 'editable' => "editable",
				// 'editor' => "editor",
			),
		));
	}

	public function getName() {
		return 'summernote';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'summernote';
	}

	public function getParent() {
		return TextareaType::class;
	}
}
