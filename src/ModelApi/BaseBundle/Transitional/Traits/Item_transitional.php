<?php
namespace ModelApi\BaseBundle\Transitional\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;

// FileBundle
use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Entity\DirectoryInterface;

use \DateTime;

trait Item_transitional {

	/*************************************************************************************/
	/*** REMOVE WHEN NEW_DIRECTORYS IS OK
	/*************************************************************************************/

	/**
	 * Parent links
	 * @var integer
	 * @ORM\OneToMany(targetEntity="ModelApi\FileBundle\Entity\Nestlink", mappedBy="child", fetch="EXTRA_LAZY", cascade={"persist","remove"}, orphanRemoval=true)
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 */
	protected $parentLinks;

	protected $currentParentLink;

	// protected $stillNotTransit;


	/**
	 * @Annot\PostCreate()
	 */
	public function init_Item_transitional() {
		$this->parentLinks = new ArrayCollection();
		$this->currentParentLink = null;
		// $this->stillNotTransit = false;
		return $this;
	}

	// /**
	//  * @ORM\PostLoad()
	//  */
	// public function load_Item_transitional() {
	// 	// if(!($this->parentLinks instanceOf ArrayCollection)) $this->parentLinks = new ArrayCollection();
	// 	// $this->stillNotTransit = !$this->parentLinks->isEmpty();
	// 	return $this;
	// }

	protected function removedTransit() {
		return false;
	}

	/**
	 * Get link type (undefined, solid or symbolic)
	 * @return string {undefined: 0, solid: 1, symbolic: 2}
	 */
	public function getTypeLink($asText = false) {
		if(null === $this->getCurrentParent()) return $asText ? Nestlink::NAME_NONE : 0;
		if($this->getCurrentParent()->isSolid()) return $asText ? Nestlink::NAME_SOLID : 1;
		if($this->getCurrentParent()->isSymbolic()) return $asText ? Nestlink::NAME_SYMBOLIC : 2;
		throw new Exception("Error line ".__LINE__." ".__METHOD__."(): no link type found!", 1);
	}

	// protected function isStillNotTransit() {
	// 	return $this->stillNotTransit;
	// }

	/**
	 * Compute Pathname
	 * @return string
	 */
	public function computePathname($computeChilds = true, $parentPathname = null) {

		return $this;
	}


	public function setCurrentPathslug($slugs) {
		// $this->currentPathslug = $slugs;
		// array_push($this->currentPathslug, $this->slug);
		return $this;
	}

	// /**
	//  * Set current ParentLink
	//  * @param Nestlink $parentLink
	//  * @return Item
	//  */
	// public function setCurrentParentLink(Nestlink $parentLink) {
	// 	if(!$parentLink->isValid()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this parentLink is not valid!", 1);
	// 	if(!$this->hasParentLink($parentLink)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this entity does not have this parentLink!", 1);
	// 	// if($this->previousParentLink !== $this->getCurrentParentLink()) {
	// 	// 	$this->previousParentLink = $this->getCurrentParentLink();
	// 	// }
	// 	$this->currentParentLink = $parentLink;
	// 	$this->position = $this->getCurrentParentLink()->getPosition();
	// 	return $this;
	// }

	// /**
	//  * Remove current ParentLink
	//  * @param Nestlink $parentLink = null
	//  * @return Item
	//  */
	// public function removeCurrentParentLink(Nestlink $parentLink = null) {
	// 	if(null === $parentLink || $parentLink === $this->getCurrentParentLink()) {
	// 		$this->currentParentLink = null;
	// 		$this->position = null;
	// 	}
	// 	return $this;
	// }

	// /**
	//  * Retrieve previous ParentLink
	//  * @param Nestlink $parentLink = null
	//  * @return Item
	//  */
	// public function retrievePreviousParentLink(Nestlink $parentLink = null) {
	// 	if(null === $parentLink || $parentLink === $this->getCurrentParentLink()) {
	// 		if($this->previousParentLink instanceOf Nestlink) {
	// 			$this->setCurrentParentLink($this->previousParentLink);
	// 		} else {
	// 			$this->removeCurrentParentLink();
	// 		}
	// 	}
	// 	return $this;
	// }

	// /**
	//  * Get current ParentLink
	//  * @return Nestlink | null
	//  */
	// public function getCurrentParentLink() {
	// 	return $this->currentParentLink;
	// }

	// /**
	//  * Get position
	//  * @return integer
	//  */
	// public function getPosition() {
	// 	return $this->position;
	// }

	// /**
	//  * Get current parent
	//  * @return Directory | null
	//  */
	// public function getCurrentParent() {
	// 	return $this->getCurrentParentLink() instanceOf Nestlink ? $this->getCurrentParentLink()->getDirectory() : null;
	// }

	// /**
	//  * Get position
	//  * @param integer $position
	//  * @param Nestlink $parentLink = null
	//  * @param boolean $inverse = true
	//  * @return Item
	//  */
	// public function setPosition($position, Nestlink $parentLink = null, $inverse = true) {
	// 	if(!($parentLink instanceOf Nestlink)) $parentLink = $this->getCurrentParentLink();
	// 	if($parentLink instanceOf Nestlink) {
	// 		if($parentLink === $this->getCurrentParentLink()) $this->position = $position;
	// 		if($inverse) $parentLink->setPosition($position, false);
	// 	}
	// 	return $this;
	// }


	// /**
	//  * Get link id
	//  * @return integer | null
	//  */
	// public function getIdLink() {
	// 	return $this->getCurrentParentLink() instanceOf Nestlink ? $this->getCurrentParentLink()->getId() : null;
	// }

	// /**
	//  * Get link type (undefined, solid or symbolic)
	//  * @return string {undefined: 0, solid: 1, symbolic: 2}
	//  */
	// public function getTypeLink($asText = false) {
	// 	if(null === $this->getCurrentParentLink()) return $asText ? 'undefined' : 0;
	// 	if($this->getCurrentParentLink()->isSolid()) return $asText ? 'solid' : 1;
	// 	if($this->getCurrentParentLink()->isSymbolic()) return $asText ? 'symbolic' : 2;
	// 	throw new Exception("Error line ".__LINE__." ".__METHOD__."(): no link type found!", 1);
	// }

	// /**
	//  * Is undefined
	//  * @return boolean
	//  */
	// public function isUndefined() {
	// 	return $this->getTypeLink() === 0;
	// }

	// /**
	//  * Is solid (for current parentLink)
	//  * @return boolean
	//  */
	// public function isSolid() {
	// 	return $this->getTypeLink() === 1;
	// }

	// /**
	//  * Set solid (for current parentLink)
	//  * @param boolean $compute = true
	//  * @return boolean
	//  */
	// public function setSolid($compute = true) {
	// 	if(!$this->isSolid() && $this->getCurrentParentLink() instanceOf Nestlink) {
	// 		$this->getCurrentParentLink()->setSolid($compute);
	// 	}
	// 	return $this;
	// }

	// /**
	//  * Is symbolic (for current parentLink)
	//  * @return boolean
	//  */
	// public function isSymbolic() {
	// 	return $this->getTypeLink() === 2;
	// }

	// /**
	//  * Set symbolic (for current parentLink)
	//  * @param boolean $compute = true
	//  * @return boolean
	//  */
	// public function setSymbolic($compute = true) {
	// 	if(!$this->isSymbolic() && $this->getCurrentParentLink() instanceOf Nestlink) {
	// 		$this->getCurrentParentLink()->setSymbolic($compute);
	// 	}
	// 	return $this;
	// }


	// /**
	//  * Add parentLink
	//  * @param Nestlink $parentLink
	//  * @param boolean $asSymbolic = false
	//  * @return Item
	//  */
	// public function addParentLink(Nestlink $parentLink, $asSymbolic = false) {
	// 	if(!$parentLink->isValid()) return $this;
	// 	if($this->hasParent()) $parentLink->setSymbolic(false);
	// 	if(!$this->hasParentLink($parentLink)) $this->parentLinks->add($parentLink);
	// 	return $this;
	// }

	/**
	 * Remove parentLink
	 * @param Nestlink $parentLink
	 * @return Item
	 */
	public function removeParentLink(Nestlink $parentLink) {
		// if($this->hasParentLink($parentLink)) {
			if(!empty($this->parentLinks)) $this->parentLinks->removeElement($parentLink);
			// $this->removeCurrentParentLink($parentLink);
			// $this->retrievePreviousParentLink($parentLink); // If $parentLink is current parent link, remove it from current
			// $this->computeRootparent();
		// }
		return $this;
	}

	// /**
	//  * Get parentLinks
	//  * @return ArrayCollection <Nestlink>
	//  */
	// public function getParentLinks() {
	// 	// $this->parentLinks = $this->parentLinks->filter(function($parentLink) {
	// 	// 	return $parentLink->isValid();
	// 	// });
	// 	return $this->parentLinks;
	// }

	// /**
	//  * Has parentLink
	//  * @return boolean
	//  */
	// public function hasParentLink(Nestlink $parentLink = null) {
	// 	if(empty($parentLink)) return !$this->parentLinks->isEmpty();
	// 	return $this->parentLinks->contains($parentLink);
	// }

	// // /**
	// //  * Is orphan
	// //  * @return boolean
	// //  */
	// // public function isOrphan() {
	// // 	$this->orphan = !$this->hasParentLink(null);
	// // 	return $this->orphan;
	// // }


	// // /**
	// //  * Get all parent Menus
	// //  * @return ArrayCollection <Menu>
	// //  */
	// // public function getParentsMenu() {
	// // 	$menus = new ArrayCollection();
	// // 	foreach ($this->parentLinks as $parentLink) {
	// // 		$menu = $parentLink->getDirectory();
	// // 		if($menu instanceOf Menu) $menus->add($menu);
	// // 	}
	// // 	return $menus;
	// // }

	// /**
	//  * Get solid parent links
	//  * @param boolean $repair = true
	//  * @return ArrayCollection <Nestlink>
	//  */
	// public function getSolidParentLinks($repair = false, $exceptionIfMoreThanOne = true) {
	// 	$solids = $this->parentLinks->filter(function($parentLink) {
	// 		return $parentLink->isSolid();
	// 	});
	// 	if($solids->count() > 1) {
	// 		if($repair) {
	// 			$this->repairSolidParentLink();
	// 			return $this->getSolidParentLinks(false, $exceptionIfMoreThanOne);
	// 		}
	// 		if($exceptionIfMoreThanOne) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this item ".$this->getName()." has more than one solid parent!", 1);
	// 	}
	// 	return $solids;
	// }

	// public function repairSolidParentLink() {
	// 	$repaired = 0;
	// 	$solidParents = $this->getSolidParentLinks(false, false);
	// 	if($solidParents->count() > 1) {
	// 		foreach ($solidParents as $key => $solidParent) {
	// 			if($key > 0 && $solidParent->isSolid()) {
	// 				$solidParent->setSymbolic(true);
	// 				$repaired++;
	// 			}
	// 		}
	// 	}
	// 	return $repaired > 0;
	// }

	// /**
	//  * Get solid parent link
	//  * @return Nestlink | null
	//  */
	// public function getSolidParentLink() {
	// 	// $this->repairSolidParentLink();
	// 	foreach ($this->getParentLinks(true) as $parentLink) {
	// 		if($parentLink->isSolid()) return $parentLink;
	// 	}
	// 	return null;
	// }

	// /**
	//  * Has one or null solid parent - Needs to be true if valid
	//  * @return boolean
	//  */
	// public function hasOneOrNullSolidParent() {
	// 	// $this->repairSolidParentLink();
	// 	return true;
	// }

}