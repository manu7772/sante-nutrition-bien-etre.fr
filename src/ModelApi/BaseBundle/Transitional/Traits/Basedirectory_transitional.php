<?php
namespace ModelApi\BaseBundle\Transitional\Traits;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use ModelApi\AnnotBundle\Annotation as Annot;
use Doctrine\Common\Collections\ArrayCollection;

// FileBundle
// use ModelApi\FileBundle\Component\Linkdatas;
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Entity\DirectoryInterface;
use ModelApi\FileBundle\Service\serviceBasedirectory;

use \Exception;
use \DateTime;

trait Basedirectory_transitional {

	/*************************************************************************************/
	/*** REMOVE WHEN NEW_DIRECTORYS IS OK
	/*************************************************************************************/

	// protected $Tchilds;

	/**
	 * Child links
	 * @var integer
	 * @ORM\OneToMany(targetEntity="ModelApi\FileBundle\Entity\Nestlink", mappedBy="directory", fetch="EXTRA_LAZY", cascade={"persist","remove"}, orphanRemoval=true)
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 * @ORM\OrderBy({"position" = "ASC"})
	 */
	protected $childLinks;

	public static function echoo($trace = false, $text) {
		if($trace) echo($text);
	}

	/**
	 * @Annot\PostCreate()
	 */
	public function init_Basedirectory_transitional() {
		// $this->Tchilds = new ArrayCollection();
		// $this->childLinks = new ArrayCollection();
		return $this;
	}

	// public function isTransited() {
	// 	return !$this->childLinks->count();
	// 	// if(!$this->childLinks->count()) return true;
	// 	// if(!$this->dirchilds->count()) return false;
	// 	// return true;
	// }

	/**
	 * Datatransform old nestlink relations to linkdatas
	 * TRANSITION : transit childs to dirchilds
	 * REMOVE WHEN NEW_DIRECTORYS IS OK
	 * @return self
	 */
	public function init_old_nestlink($trace = false) {
		if($this->isCompleteOrIncompleteRoot()) $this->refreshName();
		static::echoo($trace, '<div style="border: 1px solid black; padding: 4px; margin: 4px;"><ul>');
		static::echoo($trace, '<h3 style="color: green;">'.$this->getShortname().' : '.$this->getName().' #'.$this->getId().'</h3>');
		static::echoo($trace, '<li><strong><u>Dirchilds</u></strong> '.$this->dirchilds->count().'</li>');
		foreach ($this->dirchilds as $dirchild) {
			static::echoo($trace, '<li>Child: <span style="color: blue;">'.$dirchild->getShortname().'</span>#'.$dirchild->getId().' / '.$dirchild->getName().'</li>');
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '<ul>');
		static::echoo($trace, '<li><strong><u>Linkdatas</u></strong>  '.count($this->linkdatas).'</li>');
		foreach ($this->linkdatas as $position => $itemdata) {
			static::echoo($trace, '<li>'.$position.' : '.json_encode($itemdata).'</li>');
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '<ul>');
		static::echoo($trace, '<li><strong><u>Childlinks</u></strong> '.$this->childLinks->count().' ( <span style="color: blue;">'.$this->getShortname().'</span>#'.$this->getId().' / '.$this->getName().' ) / Owner : '.$this->owner->getName().'</li>');
		if(!$this->isTransited()) {
			$childlinks = [];
			$pos = 0;
			foreach ($this->childLinks as $childlink) if(!empty($childlink) && !empty($childlink->getChild())) {
				// $pos = $childlink->getPosition();
				// while (array_key_exists($pos, $childlinks)) { $pos++; }
				static::echoo($trace, '<li>Find child: <span style="color: blue;">'.$childlink->getChild(false)->getShortname().'</span>#'.$childlink->getChild(false)->getId().' / '.$childlink->getChild(false)->getName().' -  <small style="color: #999;">Position '.$pos.'</small></li>');
				$childlinks[$pos++] = ['item' => $childlink->getChild(false), 'typelink' => static::convertTypelink($childlink->getTypeLink())];
			}
			static::echoo($trace, '<li>----------------------------------------------</li>');
			static::echoo($trace, '<li>Transit operation: <span style="color: orange;">Operating...</span>');
			krsort($childlinks);
			$position = -1;
			foreach ($childlinks as $position => $itemdata) {
				$result = $this->addChild($itemdata['item'], $itemdata['typelink'], $position);
				static::echoo($trace, '<li>Add child: <span style="color: blue;">'.$itemdata['item']->getShortname().'</span>#'.$itemdata['item']->getId().' / '.$itemdata['item']->getName().' -  <small style="color: #999;">Position '.$position.' / Result '.(false === $result ? '<strong style="color: red;">FALSE</strong>' : json_encode($result)).'</small></li>');
			}
			foreach ($this->childLinks as $childlink) if(!empty($childlink)) $childlink->removeAll();
			$this->childLinks = new ArrayCollection();
			static::echoo($trace, '<li>Transit operation: <span style="color: orange;">Done!</span>');
		} else {
			static::echoo($trace, '<li>Transit operation: <span style="color: green;">NOT NECESSARY</span>');
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '<ul>');
		static::echoo($trace, '<li><strong><u>Dirchilds</u></strong> '.$this->dirchilds->count().'</li>');
		foreach ($this->dirchilds as $dirchild) {
			static::echoo($trace, '<li>Child: <span style="color: blue;">'.$dirchild->getShortname().'</span>#'.$dirchild->getId().' / '.$dirchild->getName().' ('.$this->getItemOid($dirchild).')</li>');
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '<ul>');
		static::echoo($trace, '<li><strong><u>Linkdatas</u></strong>  '.count($this->linkdatas).'</li>');
		foreach ($this->linkdatas as $position => $itemdata) {
			static::echoo($trace, '<li>'.$position.' : '.json_encode($itemdata).'</li>');
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '<ul>');
		static::echoo($trace, '<li><strong><u>Missing Linkdatas (from Dirchilds)</u></strong></li>');
		foreach ($this->dirchilds as $dirchild) {
			$found = false;
			foreach ($this->linkdatas as $position => $itemdata) {
				if($this->getItemOid($itemdata) === $this->getItemOid($dirchild)) $found = true;
			}
			// if(!$found) static::echoo($trace, '<li>Child: <span style="color: red;">'.$dirchild->getShortname().'</span>#'.$dirchild->getId().' / '.$dirchild->getName().'</li>');
			if(!$found) {
				$search = $this->serviceBasedirectory->getRepository()->findOneBy(['microtimeid' => $this->getItemOid($dirchild)]);
				static::echoo($trace, '<li>Child: <span style="color: red;">'.$search->getShortname().'</span>#'.$search->getId().' / '.$search->getName().'</li>');
			}
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '<ul>');
		static::echoo($trace, '<li><strong><u>Missing Dirchilds (from Linkdatas)</u></strong></li>');
		foreach ($this->linkdatas as $position => $itemdata) {
			$found = false;
			foreach ($this->dirchilds as $dirchild) {
				if($this->getItemOid($itemdata) === $this->getItemOid($dirchild)) $found = true;
			}
			if(!$found) {
				$search = $this->serviceBasedirectory->getRepository()->findOneBy(['microtimeid' => $itemdata['microtimeid']]);
				static::echoo($trace, '<li>Linkdatas: <span style="color: red;">'.$position.' : '.json_encode($itemdata).'</span><br>');
				static::echoo($trace, '- search and found :'.(empty($search) ? '<span style="color: red;">NOT FOUND</span>' : '<span style="color: orange;">'.$dirchild->getShortname().'#'.$dirchild->getId().' / '.$dirchild->getName().'</span>'));
				static::echoo($trace, '</li>');
			}
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '<ul>');
		$childs = $this->getChilds();
		static::echoo($trace, '<li><strong><u>Childs by query</u></strong> '.$childs->count().'</li>');
		foreach ($childs as $dhild) {
			static::echoo($trace, '<li>Child: <span style="color: blue;">'.$dhild->getShortname().'</span>#'.$dhild->getId().' / '.$dhild->getName().'</li>');
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '<ul>');
		static::echoo($trace, '<li><strong><u>Dirchilds</u></strong> '.$this->dirchilds->count().'</li>');
		foreach ($this->dirchilds as $dirchild) {
			static::echoo($trace, '<li>Child: <span style="color: blue;">'.$dirchild->getShortname().'</span>#'.$dirchild->getId().' / '.$dirchild->getName().' ('.$this->getItemOid($dirchild).')</li>');
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '<ul>');
		static::echoo($trace, '<li><strong><u>Linkdatas</u></strong>  '.count($this->linkdatas).'</li>');
		foreach ($this->linkdatas as $position => $itemdata) {
			static::echoo($trace, '<li>'.$position.' : '.json_encode($itemdata).'</li>');
		}
		static::echoo($trace, '</ul>');

		static::echoo($trace, '</div>');
	}

	public static function convertTypelink($typelink) {
		return $typelink === Nestlink::NAME_SOLID ? serviceBasedirectory::TYPELINK_HARDLINK : serviceBasedirectory::TYPELINK_SYMLINK;
	}


	// /**
	//  * Can have parent Directory
	//  * @return boolean
	//  */
	// public function isChildeable__old() {
	// 	return !$this->isRootDriveOrSystem();
	// }

	// /**
	//  * Get Childs
	//  * @return ArrayCollection <Item>
	//  */
	// public function getTChilds() {
	// 	$this->Tchilds = new ArrayCollection();
	// 	foreach ($this->getChildLinks() as $childLink) {
	// 		if($childLink->isValid()) $this->Tchilds->add($childLink->getChild(true));
	// 			// else $childLink->removeAll();
	// 	}
	// 	return $this->Tchilds;
	// }

	// /**
	//  * Get Child items grouped by duplicates
	//  * @return array
	//  */
	// public function getSameTChilds__old() {
	// 	$Tchilds = [];
	// 	foreach ($this->getTChilds() as $Tchild) {
	// 		$Tchilds[$Tchild->getMicrotimeid()] ??= ['nb' => 0, 'child' => null];
	// 		$Tchilds[$Tchild->getMicrotimeid()]['nb']++;
	// 		$Tchilds[$Tchild->getMicrotimeid()]['child'] = $Tchild;
	// 	}
	// 	return array_filter($Tchilds, function($Tchild) { return $Tchild['nb'] > 1; });
	// }

	// public function isChildPositionsValid() {
	// 	return !$this->checkChildsPositions(false);
	// }

	// public function checkChildsPositions($organize = true) {
	// 	$changed = false;
	// 	$idx = 0;
	// 	foreach ($this->getChildLinks() as $childLink) {
	// 		$child = $childLink->getTChild(true);
	// 		if($childLink->getPosition() !== $idx || $child->getPosition() !== $idx) {
	// 			$changed = true;
	// 			if($organize) {
	// 				$childLink->setPosition($idx);
	// 				$child->setCurrentParentLink($childLink);
	// 			}
	// 		}
	// 		$idx++;
	// 	}
	// 	return $changed;
	// }

	// /**
	//  * Get solid Childs
	//  * @return ArrayCollection <Item>
	//  */
	// public function getSolidChilds() {
	// 	$solidChilds = new ArrayCollection();
	// 	foreach ($this->getChildLinks() as $childLink) {
	// 		if($childLink->isSolid()) $solidChilds->add($childLink->getChild(true));
	// 	}
	// 	return $solidChilds;
	// }

	// /**
	//  * Add Child
	//  * @param Item $child
	//  * @param boolean $asSymbolic = false
	//  * @return Directory
	//  */
	// public function addChild(Item $child, $asSymbolic = false) {
	// 	if(!$child->isChildeable()) { return $this->addLastResult(static::FILE_ERROR_ROOT_DIRECTORY); }
	// 	if($this->hasChild($child)) {
	// 		// has symbolic, but now, setted as hard
	// 		if($child->isSymbolic() && $child->isRoot() && false === $asSymbolic) {
	// 			$child->setSolid();
	// 			return $this->addLastResult($child->isSolid() ? static::FILE_SUCCESS : static::FILE_ERROR_HAS_CHILD);
	// 		}
	// 		return $this->addLastResult(static::FILE_ERROR_HAS_CHILD);
	// 	}
	// 	$nestlink = new Nestlink($this, $child, $asSymbolic);
	// 	$this->getChilds();
	// 	return $this->addLastResult(static::FILE_SUCCESS);
	// }

	// public function setChilds($childs) {
	// 	$lastResult = static::FILE_SUCCESS;
	// 	foreach ($childs as $child) {
	// 		$this->addChild($child);
	// 		if($this->isLastResultError()) $lastResult = $this->getLastResult();
	// 	}
	// 	return $this->addLastResult($lastResult);
	// }

	// public function removeChild(Item $child) {
	// 	foreach ($this->childLinks as $childLink) {
	// 		if($childLink->getChild(false) === $child) {
	// 			$childLink->removeAll();					
	// 			// if($childLink->isSolid()) {
	// 			// 	$child->removeParents();
	// 			// 	$child->setSoftdeleted();
	// 			// } else if($childLink->isSymbolic()) {
	// 			// 	$childLink->removeAll();					
	// 			// }
	// 		}
	// 	}
	// 	$this->getChilds();
	// 	return $this;
	// }

	// public function removeChilds() {
	// 	foreach ($this->childLinks as $childLink) {
	// 		if($childLink->getChild() instanceOf Item) $this->removeChild($childLink->getChild());
	// 	}
	// 	$this->getChilds();
	// 	return $this;
	// }

	// /**
	//  * Has Child
	//  * @param Item $child = null
	//  * @return boolean
	//  */
	// public function hasChild(Item $child = null) {
	// 	if(null === $child) return !$this->getChilds()->isEmpty();
	// 	return $this->getChilds()->filter(function ($onechild) use ($child) {
	// 		return $onechild === $child;
	// 	})->count() > 0;
	// }

	// /**
	//  * Get number of childs
	//  * @return integer
	//  */
	// public function getCountChilds() {
	// 	return $this->getChilds()->count();
	// }

	// /**
	//  * Is deletable (has no children)
	//  * @return boolean
	//  */
	// public function isDeletable() {
	// 	// return ! (boolean) $this->getCountChilds();
	// 	return parent::isDeletable() && !$this->isDirstatic() && !$this->hasChild();
	// }

	/**
	 * Get ChildLinks
	 * @return ArrayCollection <childLinks>
	 */
	public function getChildLinks() {
		return $this->childLinks;
	}

	// public function getChildLinkByChild(Item $child) {
	// 	foreach ($this->childLinks as $childLink) {
	// 		if($childLink->getChild() === $child) return $childLink;
	// 	}
	// 	return null;
	// }


	/**
	 * Remove ChildLink
	 * @param Nestlink $childLink
	 * @return Directory
	 */
	public function removeChildLink(Nestlink $childLink) {
		$this->childLinks->removeElement($childLink);
		return $this;
	}

	// *
	//  * Has ChildLink
	//  * @param Nestlink $childLink
	//  * @return boolean
	 
	// public function hasChildLink(Nestlink $childLink = null) {
	// 	return $childLink instanceOf Nestlink ? $this->getChildLinks()->contains($childLink) : !$this->getChildLinks()->isEmpty();
	// }

	// /**
	//  * Is allready a parent upper in the tree
	//  * @param Nestlink $parentLink
	//  * @return boolean
	//  */
	// public function isAllreadyParent(Nestlink $parentLink) {
	// 	$parent = $parentLink->getDirectory();
	// 	do {
	// 		if($parent === $this) return true;
	// 		$parent = $parent->getParent();
	// 	} while ($parent instanceOf DirectoryInterface);
	// 	return false;
	// }

	// /**
	//  * Add parentLink
	//  * @param Nestlink $parentLink
	//  * @param boolean $asSymbolic = false
	//  * @return Directory
	//  */
	// public function addParentLink(Nestlink $parentLink, $asSymbolic = false) {
	// 	if(!$this->hasParentLink($parentLink)) {
	// 		if(!$this->hasParent() && !$this->isAllreadyParent($parentLink) && false === $asSymbolic) $parentLink->setSolid(false);
	// 			else $parentLink->setSymbolic(false);
	// 		$this->parentLinks->add($parentLink);
	// 	}
	// 	// if($parentLink->isSolid()) $this->computeRootparent();
	// 	return $this;
	// }



}