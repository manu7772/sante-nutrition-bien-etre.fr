<?php
// namespace ModelApi\BaseBundle\Representation;

// use Pagerfanta\Pagerfanta;
// // use \LogicException;

// /**
//  * Pagination for entities
//  * 
//  */
// class Pagination {

//	const SHORTCUT_CONTROLS = true;

// 	use \ModelApi\BaseBundle\Traits\ClassDescriptor;

// 	/**
// 	 * meta data for collection of entities
// 	 * @var array
// 	 */
// 	protected $meta;

// 	/**
// 	 * Array of entities
// 	 * @var ArrayCollection
// 	 */
// 	protected $entities;

// 	/**
// 	 * Number of entities found
// 	 * @var interger
// 	 */
// 	protected $results;

// 	public function __construct(Pagerfanta $Pagerfanta, $params = []) {
// 		$this->meta = array();
// 		$this->results = 0;
// 		$this->setEntities((array)$Pagerfanta->getCurrentPageResults());
// 		$this
// 			// ->setMeta('data', $Pagerfanta->getCurrentPageResults())
// 			->setMeta('limit', $Pagerfanta->getMaxPerPage())
// 			->setMeta('current_items', count($Pagerfanta->getCurrentPageResults()))
// 			->setMeta('total_items', $Pagerfanta->getNbResults())
// 			->setMeta('offset_start', $Pagerfanta->getCurrentPageOffsetStart())
// 			->setMeta('offset_end', $Pagerfanta->getCurrentPageOffsetEnd())
// 			->setMeta('nb_pages', $Pagerfanta->getNbPages())
// 			->setMeta('current_page', $Pagerfanta->getCurrentPage())
// 			->setMeta('has_next_page', $Pagerfanta->hasNextPage())
// 			->setMeta('next_page', $Pagerfanta->hasNextPage() ? $Pagerfanta->getNextPage() : null)
// 			->setMeta('has_previous_page', $Pagerfanta->hasPreviousPage())
// 			->setMeta('previous_page', $Pagerfanta->hasPreviousPage() ? $Pagerfanta->getPreviousPage() : null)
// 			->setMeta('have_to_paginate', $Pagerfanta->haveToPaginate())
// 			->setMeta('params', $params)
// 			;
// 		return $this;
// 		$this->init_ClassDescriptor();
// 	}

// 	public function setMeta($name, $value) {
// 		$this->meta[$name] = $value;
// 		return $this;
// 	}

// 	public function getMeta() {
// 		return $this->meta;
// 	}

// 	public function setEntities($entities) {
// 		$this->entities = $entities;
// 		$this->results = count($this->entities);
// 		return $this;
// 	}

// 	public function getEntities() {
// 		return $this->entities;
// 	}

// 	public function getResults() {
// 		return $this->results;
// 	}

// }

