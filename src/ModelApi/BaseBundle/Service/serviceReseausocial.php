<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceContext;
// use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Entity\Reseausocial;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// FileBundle
use ModelApi\FileBundle\Service\serviceItem;

// use \DateTime;
use \ReflectionClass;

class serviceReseausocial extends serviceItem {

	const ENTITY_CLASS = Reseausocial::class;

	// protected $container;
	// protected $serviceEntities;

	// public function __construct(ContainerInterface $container) {
	// 	$this->container = $container;
	// 	$this->serviceEntities = $this->container->get(serviceEntities::class);
	// 	return $this;
	// }

	public function findByTier(Tier $tier = null, $asArray = false) {
		return $this->getRepository()->findByTier($tier);
	}


	// public function findByEnvironment($asArray = false) {
	// 	$tier = $this->container->get(serviceContext::class)->getEnvironment();
	// 	if(!($tier instanceOf Tier)) return parent::findAll();
	// 	return $this->findByTier($tier);
	// }


}