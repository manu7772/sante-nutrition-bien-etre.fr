<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
use ModelApi\BaseBundle\Entity\Logg;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Service\serviceContext;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Service\serviceUser;

// use \DateTime;
use \ReflectionClass;
use \Closure;

class serviceLogg implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Logg::class;
	const LOGG_SORT = [
		'day' => ['format' => 'Yz', 'increment' => '1 day'],
		'hour' => ['format' => 'YzH', 'increment' => '1 hour'],
		'minute' => ['format' => 'YzHi', 'increment' => '1 minute'],
	];

	protected $container;
	protected $serviceEntities;
	protected $serviceKernel;
	protected $serviceRoute;
	protected $logg;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->serviceKernel = $this->container->get(serviceKernel::class);
		$this->serviceRoute = $this->container->get(serviceRoute::class);
		$this->logg = null;
		return $this;
	}


	public static function getSortFormat($interval = 'hour') {
		return isset(static::LOGG_SORT[$interval]) ? static::LOGG_SORT[$interval]['format'] : static::LOGG_SORT['hour']['format'];
	}

	public static function getIncInterval($interval = 'hour', $way = "+") {
		if(isset(static::LOGG_SORT[$interval])) {
			switch ($way) {
				case 'decrement':
				case 'dec':
				case '<':
				case '-':
					return "-".static::LOGG_SORT[$interval]['increment'];
					break;
				case 'increment':
				case 'inc':
				case '>':
				case '+':
				default:
					return "+".static::LOGG_SORT[$interval]['increment'];
					break;
			}
		}
		return "+".static::LOGG_SORT['hour']['increment'];
	}

	/**
	 * To do when service is initialized (or execute immediatly if still initialized)
	 * @param string|object $name
	 * @param Closure $action
	 * @return $name (usefull if $name is object)
	 */
	public function whenContextInitialized($name, Closure $action) {
		return $this->container->get(serviceContext::class)->whenInitialized($name, $action);
	}

	public function getUser() {
		return $this->container->get(serviceContext::class)->getDefaultUser(); 
	}

	// public function getContext($as_array = true) {
	// 	return $this->container->get(serviceContext::class)->getAllContextNamedElements($as_array, true);
	// }

	// /**
	//  * Get new entity
	//  * @param array $options = []
	//  * @param Closure $closure = null
	//  * @return entity | null
	//  */
	// public function createNew($options = [], $closure = null) {
	// 	$logg = $this->serviceEntities->createNew(static::ENTITY_CLASS, $options, $closure);
	// 	$this->setCurrentLogg($logg);
	// 	return $logg;
	// }

	// public function delete($entity, $flush = true, $forceDelete = false) {
	// 	$entityManager = $this->serviceEntities->getEntityManager();
	// 	if($entity->isInstance(['BaseEntity']) && !$forceDelete) $entity->setSoftdeleted();
	// 		else $entityManager->remove($entity);
	// 	if($flush) $entityManager->flush();
	// 	return $this;
	// }

	public function setCurrentLogg(Logg $logg) {
		if($this->logg instanceOf Logg) throw new Exception("Error ".__METHOD__."(): can not change current Logg in same session!", 1);
		$this->logg = $logg;
		return $this;
	}

	public function getCurrentLogg($createIfNotFound = false) {
		if($this->logg instanceOf Logg) return $this->logg;
		$this->logg = $createIfNotFound ? $this->createNew() : null;
		return $this->logg;
	}


	public function getRequest() {
		return $this->serviceKernel->getMasterRequest();
	}

	public function isXmlHttpRequest() {
		return $this->serviceKernel->isXmlHttpRequest();
	}

	public function getRoute() {
		return $this->serviceRoute->getRoute();
	}

	public function getRoute_params() {
		return $this->serviceRoute->getRouteParams();
	}

	public function getBundleName($getTypeIfBundleNotFound = false) {
		$bundlename = $this->serviceKernel->getCurrentBundleName();
		if(is_string($bundlename)) return $bundlename;
		if($getTypeIfBundleNotFound) {
			$details = $this->serviceKernel->getTypeIfBundleNotFound();
			if(is_array($details)) return '*** '.$details['type'].' ***';
		}
		return null;
	}

	public function getFlashbag($as_array = true) {
		$flashbag = $this->serviceKernel->getFlashbag();
		if(!$as_array) return $$flashbag;
		return $flashbag->toArray();
	}

	public function getLocale() {
		return $this->serviceKernel->getLocale();
	}

	public function getEnv() {
		return $this->serviceKernel->getEnv();
	}

	public function getIps() {
		$ips = ['REMOTE_ADDR' => array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : null];
		$ips['HTTP_X_FORWARDED_FOR'] = array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : null;
		return $ips;
	}

	// public function getUser($getId = false) {
	// 	$user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
	// 	if($getId) return $user instanceOf User ? $user->getId() : null;
	// 	return $user;
	// }


}