<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Bundle\Bundle;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Service\serviceGrants;

use \ReflectionClass;
use \Exception;

class serviceFlashbag implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const MESSAGES_SUPPORTS = ['sweet','toastr'];
	const MESSAGES_TYPES = ['info','success','warning','error'];

	protected $container;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		return $this;
	}



	// Example : $this->container->get(serviceFlashbag::class)->addFlashSweet('warning', 'Cleared language cache.');
	// Example : $this->container->get(serviceFlashbag::class)->addFlashToastr('warning', 'Cleared language cache.');
	public function __call($method, $parameters = []) {
		$serviceKernel = $this->container->get(serviceKernel::class);
		if($serviceKernel->isNorequest()) return ['result' => false, 'message' => 'flashbag.hasNoRequest'];
		if(preg_match('/^addFlash/', $method) && is_array($parameters)) {
			$supportmsg = 'default';
			foreach (static::MESSAGES_SUPPORTS as $support) { if($method === 'addFlash'.ucfirst($support)) $supportmsg = $support; }
			if(!count($parameters)) return ['result' => false, 'message' => 'flashbag.not_enought_parameters'];
			if(count($parameters) === 1) {
				$type = static::MESSAGES_TYPES[0];
				$message = $parameters[0];
			} else {
				$type = in_array(strtolower($parameters[0]), static::MESSAGES_TYPES) ? strtolower($parameters[0]) : static::MESSAGES_TYPES[0];
				$message = $parameters[1];
				if(isset($parameters[2]) && preg_match('/^ROLE_/', $parameters[2])) {
					$role = $parameters[2];
					// ROLE
					// if(!$this->container->get(serviceGrants::class)->isGranted($parameters[2])) return ['result' => false, 'message' => 'flashbag.not_granted'];
						// else $message .= ' / '.$parameters[2];
				}
			}
			if(!is_string($message)) return ['result' => false, 'message' => 'flashbag.invalid_message_type'];
			// parameters are OK
			$serviceKernel->getFlashbag()->add($type, ['support' => $supportmsg, 'message' => $message, 'role' => $role ?? null]);
		} else {
			throw new Exception("Error ".json_decode($method)."(): called method non reconnue!", 1);
		}
		return true;
	}


}