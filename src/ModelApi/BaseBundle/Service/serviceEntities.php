<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityRepository;
// BaseBundle
use ModelApi\BaseBundle\Entity\LaboClassMetadata;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Traits\ClassDescriptor;
use ModelApi\BaseBundle\Component\EntityReport;
use ModelApi\BaseBundle\Annotation\ModelForCreate;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Fileformat;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Service\serviceCruds;
// AnnotBundle
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;
// use ModelApi\AnnotBundle\Annotation\StoreDirContent;

use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \ReflectionProperty;
use \ReflectionClass;
use \ReflectionMethod;
use \Exception;
use \DateTime;

class serviceEntities implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const NOT_FOUND = 'dontexist';
	const PROXY_PREFIX = '#^Proxies\\\\?__CG__\\\\?#';
	const SERVICE_NAME_PREFIX = 'service';

	const DOCTRINE_MAPPING = 'Doctrine\\ORM\\Mapping\\';
	const RELATION_MANYTOMANY = 'ManyToMany';
	const RELATION_ONETOMANY = 'OneToMany';
	const RELATION_MANYTOONE = 'ManyToOne';
	const RELATION_ONETOONE = 'OneToOne';
	const DOCTRINE_COLUMN = 'Column';

	const BDDMCID_PREFIX = 'bddmcid@';
	const BDDMCID_SEPARATOR = '---';
	const BDDID_PREFIX = 'bddid@';
	const BDDID_SEPARATOR = '---';

	const FIND_ONE_METHOD = 'findOneWithOptimizedJoins';

	const CACHE_NAME = 'classmetadata';

	const PLURAL_MARKER = '_X';

	protected $container;
	protected $serviceCache;
	protected $_em;
	protected $modelForCreate;
	protected $laboClassMetadatas;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceCache = $this->container->get(serviceCache::class);
		$this->_em = $this->container->get('doctrine.orm.entity_manager');
		$this->modelForCreate = [];
		$this->laboClassMetadatas = [];
		return $this;
	}


	// protected function hasEventSubscriber(EventSubscriber $EventSubscriber) {
	// 	foreach ($this->getEntityManager()->getEventManager()->getListeners() as $listener => $objects) {
	// 		foreach ($objects as $object) {
	// 			// if($object === $EventSubscriber) return true;
	// 			if(get_class($object) === get_class($EventSubscriber)) return true;
	// 		}
	// 	}
	// 	return false;
	// }


	/***********************************/
	/*** SERVICES ENTITIES
	/***********************************/

	public function getEntityService($classname) {
		// FIRST: in entity
		if(is_object($classname) && method_exists($classname, 'getService')) {
			$entityService = $classname->getService();
			if(class_exists($entityService) && $this->container->has($entityService)) return $this->container->get($entityService);
		}
		$classname = $this->getClassnameByAnything($classname);
		if(!is_string($classname)) return null;
		// SECOND: in entity class
		$entityService = $classname::getService();
		if(class_exists($entityService) && $this->container->has($entityService)) return $this->container->get($entityService);
		// THIRD: in config_model_api.yml file
		$class = new ReflectionClass($classname);
		$shortname = $class->getShortName();
		$entities_parameters = $this->container->getParameter('entities_parameters');
		$entity_parameters = isset($entities_parameters[$shortname]) ? $entities_parameters[$shortname] : [];
		if(isset($entity_parameters['service']) && class_exists($entity_parameters['service']) && $this->container->has($entity_parameters['service'])) { 
			return $this->container->get($entity_parameters['service']);
		}
		// FOURTH: calculate...
		$entityService = $this;
		foreach (serviceClasses::getParentClasses($classname, true, false) as $class) {
			$serviceClass = preg_replace(['#\\\\Entity\\\\#','#\\\\'.ucfirst($shortname).'$#'], ['\\Service\\','\\'.static::SERVICE_NAME_PREFIX.ucfirst($shortname)], $class);
			// $serviceClass = preg_replace('#(\\\\Entity\\\\)(\\w)#', '\\Service\\'.static::SERVICE_NAME_PREFIX.ucfirst('$2'), $class);
			if($this->container->has($serviceClass)) return $this->container->get($serviceClass);
		}
		// FIFTH: By default : return $this... that is the basic service ;-)
		return $this;
	}


	/***********************************/
	/*** ENTITY MANAGER
	/***********************************/

	public function getEntityManager() {
		// https://stackoverflow.com/questions/14258591/the-entitymanager-is-closed
		if (!$this->_em->isOpen()) {
			$this->container->set('doctrine.orm.entity_manager', null);
			$this->container->set('doctrine.orm.default_entity_manager', null);
			$this->_em = $this->container->get('doctrine')->getManager();
		}
		return $this->_em;
	}


	public static function removeProxyPrefix($classname) {
		return preg_replace(static::PROXY_PREFIX, '', $classname);
	}

	public function isLoadedEntity($entity) {
		if(!$this->entityExists($entity)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this entity ".(is_object($entity) ? get_class($entity) : gettype($entity))." is not valid!", 1);
		return $this->getEntityManager()->getUnitOfWork()->isInIdentityMap($entity);
	}

	public function isManagedEntity($entity) {
		if(!$this->entityExists($entity)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): this entity ".(is_object($entity) ? get_class($entity) : gettype($entity))." is not valid!", 1);
		return $this->getEntityManager()->contains($entity);
	}

	/***********************************/
	/*** SOFTDELETED MANAGEMENT
	/***********************************/

	public static function isSoftdeleted($softdeleted = null, $relativeDate = 'NOW') {
		if(empty($softdeleted)) return false;
		if(is_string($softdeleted)) $softdeleted = new DateTime($softdeleted);
		if(empty($relativeDate)) $relativeDate = new DateTime();
		if(is_string($relativeDate)) {
			try {
				$relativeDate = new DateTime($relativeDate);
			} catch (Exception $e) {
				$date = $relativeDate;
				$relativeDate = new DateTime();
				$relativeDate->modify($date);
			}
		}
		return $softdeleted <= $relativeDate;
	}

	/***********************************/
	/*** REQUESTS ON ENTITIES CLASSES
	/***********************************/

	/**
	 * Get all entities names as [classname => shortname]
	 * @see https://abendstille.at/blog/?p=163
	 * @param boolean $onlyInstantiable = false
	 * @param string $instanceOf = null
	 * @param boolean $refresh = false
	 * @return array [classname => shortname]
	 */
	public function getEntitiesNames($onlyInstantiable = false, $instanceOf = null, $refresh = false) {
		$cacheId = !is_string($instanceOf) ? 'names'.($onlyInstantiable ? '_instantiables' : '_all') : 'names_'.$instanceOf.($onlyInstantiable ? '_instantiables' : '_all');
		$entityManager = $this->getEntityManager();
		$entities = $this->serviceCache->getCacheData(
			$cacheId,
			function() use ($entityManager, $onlyInstantiable, $instanceOf) {
				$results = array();
				foreach($entityManager->getConfiguration()->getMetadataDriverImpl()->getAllClassNames() as $classname) {
					$RC = $entityManager->getClassMetadata($classname)->getReflectionClass();
					if(preg_match('/^ModelApi/', $RC->getName())) {
						if(null === $instanceOf || ($RC->isSubclassOf($instanceOf) || $instanceOf === $RC->getName())) {
							if(false === $onlyInstantiable || $RC->isInstantiable()) $results[$RC->getName()] = $RC->getShortName();
						}
					}
				}
				return $results;
			},
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
		return $entities;
	}

	public function getEntitiesOfInterface($interface, $onlyInstantiable = false, $refresh = false) {
		$cacheId = 'interfaces_'.$interface.($onlyInstantiable ? '_instantiables' : '_all');
		$entityManager = $this->getEntityManager();
		$entities = $this->serviceCache->getCacheData(
			$cacheId,
			function() use ($entityManager, $interface, $onlyInstantiable) {
				$results = array();
				foreach($entityManager->getConfiguration()->getMetadataDriverImpl()->getAllClassNames() as $classname) {
					$RC = $entityManager->getClassMetadata($classname)->getReflectionClass();
					if(preg_match('/^ModelApi/', $RC->getName())) {
						if($RC->implementsInterface($interface) && (false === $onlyInstantiable || $RC->isInstantiable())) $results[$RC->getName()] = $RC->getShortName();
					}
				}
				return $results;
			},
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
		return $entities;
	}

	public function getEntitiesOfTrait($trait, $onlyInstantiable = false, $refresh = false) {
		$cacheId = 'traits_'.$trait.($onlyInstantiable ? '_instantiables' : '_all');
		$entityManager = $this->getEntityManager();
		$entities = $this->serviceCache->getCacheData(
			$cacheId,
			function() use ($entityManager, $trait, $onlyInstantiable) {
				$results = array();
				foreach($entityManager->getConfiguration()->getMetadataDriverImpl()->getAllClassNames() as $classname) {
					$RC = $entityManager->getClassMetadata($classname)->getReflectionClass();
					if(preg_match('/^ModelApi/', $RC->getName()) && ($RC->isInstantiable() || false === $onlyInstantiable)) {
						$parent_classes = serviceClasses::getParentClasses($classname, true, true);
						foreach ($parent_classes as $search_class) {
							$RCSUB = new ReflectionClass($search_class);
							foreach ($RCSUB->getTraits() as $RCT) {
								if(in_array($trait, [$RCT->getName(), $RCT->getShortName()])) {
									$results[$RC->getName()] = $RC->getShortName();
									break 2;
								}
							}
						}
					}
				}
				return $results;
			},
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
		return $entities;
	}

	/**
	 * Get instantiable entities of $classname
	 * @param string $classname
	 * @param boolean $onlyInstantiable = false
	 * @return array <string>
	 */
	public function getEntitiesOfInstance($classname, $onlyInstantiable = false, $refresh = false) {
		return $this->getEntitiesNames($onlyInstantiable, $classname, $refresh);
	}

	/**
	 * Get instantiable classname of Entity shortname
	 * @param string $search_shortname
	 * @return string | null
	 */
	public function getUniqueInstantiableClassnameByShortname($search_shortname) {
		$classname =  $this->getClassnameByAnything($search_shortname);
		if(null === $classname) return null;
		$RC = $this->getEntityManager()->getClassMetadata($classname)->getReflectionClass();
		return $RC->isInstantiable() ? $classname : null;
	}

	/**
	 * Entity exists
	 * @param mixed $entity (object, classname or shortname)
	 * @return boolean
	 */
	public function entityExists($entity, $onlyInstantiable = true) {
		$classname =  $this->getClassnameByAnything($entity);
		if(null === $classname) return false;
		$RC = $this->getEntityManager()->getClassMetadata($classname)->getReflectionClass();
		return $RC->isInstantiable() || false === $onlyInstantiable;
	}

	/**
	 * Get classname of Entity shortname
	 * @param string $search_shortname
	 * @return string | null
	 */
	public function getClassnameByShortname($search_shortname) {
		return $this->getClassnameByAnything($search_shortname);
	}

	/**
	 * Get classname of Entity (object, classname or shortname)
	 * @param mixed $mixed
	 * @return string | null
	 */
	public function getClassnameByAnything($mixed, $refresh = false) {
		if($mixed instanceOf ClassMetadata) $mixed = $mixed->getReflectionClass()->getShortName();
		if($mixed instanceOf LaboClassMetadata) $mixed = $mixed->getShortName();
		if(is_object($mixed)) $mixed = get_class($mixed);
		$mixed = strtolower(preg_replace('/\\\*/', '', static::removeProxyPrefix($mixed)));
		$names = $this->serviceCache->getCacheData(
			'classnames',
			function() {
				$results = [];
				foreach ($this->getEntitiesNames() as $classname => $shortname) {
					$results[strtolower(preg_replace('/\\\*/', '', $classname))] = $classname;
					// $results[$shortname] = $classname;
					$results[strtolower($shortname)] = $classname;
					$results[strtolower(preg_replace('/\\\*/', '', 'Proxies__CG__'.$classname))] = $classname;
				}
				return $results;
			},
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
		return $names[$mixed] ??= null;
	}

	/**
	 * Get shortname of Entity (object, classname or shortname)
	 * @param mixed $mixed
	 * @return string | null
	 */
	public function getShortnameByAnything($mixed, $refresh = false) {
		if($mixed instanceOf ClassMetadata) $mixed = $mixed->getReflectionClass()->getShortName();
		if($mixed instanceOf LaboClassMetadata) $mixed = $mixed->getShortName();
		if(is_object($mixed)) $mixed = get_class($mixed);
		$mixed = strtolower(preg_replace('/\\\*/', '', static::removeProxyPrefix($mixed)));
		$names = $this->serviceCache->getCacheData(
			'shortnames',
			function() {
				$results = [];
				foreach ($this->getEntitiesNames(true) as $classname => $shortname) {
					$results[strtolower(preg_replace('/\\\*/', '', $classname))] = $shortname;
					// $results[$shortname] = $shortname;
					$results[strtolower($shortname)] = $shortname;
					$results[strtolower(preg_replace('/\\\*/', '', 'Proxies__CG__'.$classname))] = $shortname;
				}
				return $results;
			},
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
		return $names[$mixed] ??= null;
	}

	public function getSubentities($mixed) {
		$classes = [];
		foreach (serviceClasses::getSubclasses($this->getClassnameByAnything($mixed)) as $class) {
			$class = static::removeProxyPrefix($class);
			if($this->entityExists($class)) $classes[] = $class;
		}
		return array_unique($classes);
	}


	public function getIcons($refresh = false) {
		return $this->serviceCache->getCacheData(
			'icons',
			function() {
				$results = [];
				$cls = ' @@@CLASS@@@';
				foreach ($this->getEntitiesNames() as $classname => $shortname) {
					$results[$classname] = [
						'_classname' => $classname,
						'_shortname' => $shortname,
						'html' => '<i class="fa fa-question text-danger'.$cls.'"></i>',
						'class' => 'fa fa-question text-danger'.$cls,
					];
					// echo('<pre>');var_dump($results[$classname]);echo('</pre>');
					$RC = new ReflectionClass($classname);
					if($RC->isInstantiable() && method_exists($classname, '__construct')) {
						$constructor = $RC->getMethod('__construct');
						if($constructor->getNumberOfParameters() < 1) {
							$entity = $this->getModel($classname, true);
							if(method_exists($entity, 'init_BaseEntity')) $entity->init_BaseEntity();
							// HTML
							$html = method_exists($entity, 'getDefaultIconHtml') ? $entity->getDefaultIconHtml(false, $cls) : '';
							if(!preg_match('/^<i class="fa\\s+fa-\\w+/', $html) && defined($classname.'::DEFAULT_ICON')) $html = '<i class="fa '.$classname::DEFAULT_ICON.$cls.'"></i>';
							if(preg_match('/^<i class="fa\\s+fa-\\w+/', $html)) $results[$classname]['html'] = $html;
							// CLASS
							$class = method_exists($entity, 'getDefaultIcon') ? 'fa '.$entity->getDefaultIcon().$cls : '';
							if(!preg_match('/^fa\\s+fa-\\w+/', $class) && method_exists($entity, 'getDefaultIconClass')) $class = $entity->getDefaultIconClass().$cls;
							// if(!preg_match('/^fa\\s+fa-\\w+/', $class) && method_exists($entity, 'getDefaultIcon')) $class = 'fa '.$entity->getDefaultIcon().$cls;
							if(!preg_match('/^fa\\s+fa-\\w+/', $class) && defined($classname.'::DEFAULT_ICON')) $class = 'fa '.$classname::DEFAULT_ICON.$cls;
							if(preg_match('/^fa\\s+fa-\\w+/', $class)) {
								$results[$classname]['class'] = $class;
								if(!preg_match('/^<i class="fa\\s+fa-\\w+/', $html)) $results[$classname]['html'] = '<i class="'.$class.'"></i>';
							}
						}
					} else if(defined($classname.'::DEFAULT_ICON')) {
						$results[$classname]['html'] = '<i class="fa '.$classname::DEFAULT_ICON.$cls.'"></i>';
						$results[$classname]['class'] = 'fa '.$classname::DEFAULT_ICON.$cls;
					}
					if(defined($classname.'::DEFAULT_ICON')) {
						if(preg_match('/fa-question/', $results[$classname]['html'])) $results[$classname]['html'] = '<i class="fa '.constant($classname.'::DEFAULT_ICON').$cls.'"></i>';
						if(preg_match('/fa-question/', $results[$classname]['class'])) $results[$classname]['class'] = 'fa '.constant($classname.'::DEFAULT_ICON').$cls;
					}
					// clean classes
					$results[$classname]['html'] = preg_replace('/\\s+/', ' ', $results[$classname]['html']);
					$results[$classname]['class'] = preg_replace('/\\s+/', ' ', $results[$classname]['class']);
				}
				return $results;
			},
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
	}

	public function getIcon($entity, $asHtml = true, $asDefault = false, $classes = null, $refresh = false) {
		// return $asHtml ? '<i class="fa fa-question"></i>' : 'fa fa-question';
		if($entity instanceOf ClassMetadata) $entity = $classname = $entity->name;
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) $classes = count($classes) > 0 ? implode(' ', array_unique($classes)) : '';
			else $classes = '';
		if(is_object($entity) && !$asDefault) {
			// from specific entity
			if($asHtml) {
				if(method_exists($entity, 'getIconHtml')) return $entity->getIconHtml(false, $classes);
			} else {
				if(method_exists($entity, 'getIcon')) return 'fa '.$entity->getIcon().' '.$classes;
			}
		}
		$entity = $this->getClassnameByAnything($entity);
		if(is_string($entity)) {
			// from class
			$icons = $this->getIcons($refresh);
			if(isset($icons[$entity])) {
				if($asHtml) return preg_replace('/@@@CLASS@@@/', $classes, $icons[$entity]['html']);
				return preg_replace('/@@@CLASS@@@/', $classes, $icons[$entity]['class']);
			}
		} else {
			// throw new Exception("Error line ".__LINE__." ".__METHOD__."(): can not get icon from ".gettype($entity)."!", 1);
		}
		return $asHtml ? '<i class="fa fa-question '.$classes.'"></i>' : 'fa fa-question '.$classes;
	}


	/***********************************/
	/*** VALIDATE ENTITY
	/***********************************/

	/**
	 * @see https://www.doctrine-project.org/projects/doctrine-orm/en/2.8/reference/php-mapping.html
	 */
	public function getErrorsReport($object, $excludes = []) {
		return $this->container->get(serviceClasses::class)->getErrorsReport($object, $this->entityExists($object) ? $this->getEntityManager()->getClassMetadata($object->getClassname()) : null, $excludes);
	}

	/***********************************/
	/*** BASIC REQUESTS
	/***********************************/

	// /**
	//  * Get prefered BaseMains
	//  * @return array <BaseMain>
	//  */
	// public function getPrefereds() {
	// 	return $this->getRepository()->findByPrefered(true);
	// }

	// /**
	//  * Get prefered (and active) BaseMain
	//  * @param boolean $onlyActive = true
	//  * @return BaseMain | null
	//  */
	// public function getPrefered($onlyActive = true) {
	// 	if(false === $onlyActive) return $this->getRepository()->findOneByPrefered(true);
	// 	foreach ($this->getPrefereds() as $entity) {
	// 		if($entity->isActive()) return $entity;
	// 	}
	// 	return null;
	// }


	/***********************************/
	/*** CLONE ENTITY
	/***********************************/

	public function cloneEntity($entity, $new = null) {
		if(is_object($new)) {
			// Do nothing
		} else if(is_string($new)) {
			$new = $this->createNew($new);
		} else {
			$new = clone $entity;
		}
		// copy properties...

		return $new;
	}



	/***********************************/
	/*** SINGULIER/PLURIEL ENTITIES NAMES
	/***********************************/

	public static function getXableNames() {
		return ['jeu'];
	}

	public static function singlar($name) {
		$name = preg_replace('#(s|x|'.static::PLURAL_MARKER.')$#', '', $name);
		return $name;
	}

	public static function plural($name) {
		if(preg_match('#(s|x|'.static::PLURAL_MARKER.')$#', $name)) return $name;
		// is classname
		if(preg_match('#\\\\#', $name)) return $name.static::PLURAL_MARKER;
		return preg_match('#('.implode('|', static::getXableNames()).')$#', $name) ? $name.'x' : $name.'s';
	}

	public static function humanize($name) {
		$name = preg_replace('/([\\w\\\]*)\\\(\\w+)$/', '$2', $name);
		$plur = '';
		if(preg_match('#'.static::PLURAL_MARKER.'$#', $name)) {
			$plur = preg_match('#('.implode('|', static::getXableNames()).')('.static::PLURAL_MARKER.')?$#', $name) ? 'x' : 's';
		}
		$name = preg_replace('#('.static::PLURAL_MARKER.')$#', $plur, $name);
		return $name;
	}


	/***********************************/
	/*** BDDMCID
	/***********************************/

	public static function getEntityBddmcid($entity) {
		if(!is_object($entity)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Entity is not an object. Got ".gettype($entity).".", 1);
		// if(!($entity instanceOf ClassDescriptor)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Entity ".json_encode($entity->__toString())." does not implements ".json_encode(ClassDescriptor::class).".", 1);
		if(!method_exists($entity, 'getMicrotimeid')) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Entity has no getMicrotimeid() method.", 1);
		$classes = serviceClasses::getParentClasses($entity, true, true, true, true);
		$classes[] = $entity->getMicrotimeid();
		return static::getBddMcid($classes);
	}

	public static function getBddMcid($elements) {
		$bddmcid = static::BDDMCID_PREFIX.static::BDDMCID_SEPARATOR.implode(static::BDDMCID_SEPARATOR, $elements);
		if(!preg_match(static::getValidBddmcidPregTest(), $bddmcid)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): generated BDDMCID is not valid. Obtained ".json_encode($bddmcid).".", 1);
		return $bddmcid;
	}

	public static function getValidBddmcidPregTest() { // 60253731996b71.65078120
		// return '/^bddid@___([\\w\\d]+___)+\\d+$/';
		return '/^'.static::BDDMCID_PREFIX.static::BDDMCID_SEPARATOR.'([\\w\\d]+'.static::BDDMCID_SEPARATOR.')+[\\d\abcdef]{14}\\.[\\d]{8,8}$/';
	}


	/***********************************/
	/*** BDDID
	/***********************************/

	public static function getEntityBddid($entity) {
		if(!is_object($entity)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Entity is not an object. Got ".gettype($entity).".", 1);
		// if(!($entity instanceOf ClassDescriptor)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Entity ".json_encode($entity->__toString())." does not implements ".json_encode(ClassDescriptor::class).".", 1);
		if(!method_exists($entity, 'getId')) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Entity has no getId() method.", 1);
		$classes = serviceClasses::getParentClasses($entity, true, true, true, true);
		$classes[] = $entity->getId();
		return static::getBddid($classes);
	}

	public static function getBddidByClassAndId($classname, $id) {
		$classes = serviceClasses::getParentClasses($classname, true, true, true, true);
		$classes[] = $id;
		return static::getBddid($classes);
	}

	public static function getBddid($elements, $control = true) {
		$bddid = static::BDDID_PREFIX.static::BDDID_SEPARATOR.implode(static::BDDID_SEPARATOR, $elements);
		if($control && !preg_match(static::getValidBddidPregTest(), $bddid)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): generated BDDID is not valid. Obtained ".json_encode($bddid).".", 1);
		return $bddid;
	}

	public static function getValidBddidPregTest() {
		// return '/^bddid@___([\\w\\d]+___)+\\d+$/';
		return '/^'.static::BDDID_PREFIX.static::BDDID_SEPARATOR.'([\\w\\d]+'.static::BDDID_SEPARATOR.')+\\d+$/';
	}

	public static function isValidBddid($bddid, $returnException = true) {
		$valid = preg_match(static::getValidBddidPregTest(), $bddid);
		if(!$valid && $returnException) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): BDDID is not valid. Got ".json_encode($bddid).".", 1);
		return $valid;
	}

	public static function getBddidEntityShortnames($bddid) {
		static::isValidBddid($bddid, true);
		$nbletters = floor(strlen(static::BDDID_SEPARATOR));
		preg_match_all('/'.substr(static::BDDID_SEPARATOR, 0, $nbletters).'([\\w\\d]+)'.substr(static::BDDID_SEPARATOR, -$nbletters).'/', $bddid, $matches);
		return end($matches);
	}

	public static function hasBddidShortname($bddid, $shortname) {
		$shortnames = static::getBddidEntityShortnames($bddid);
		return in_array($shortname, $shortnames);
	}

	public static function getBddidEntityShortname($bddid) {
		$shortnames = static::getBddidEntityShortnames($bddid);
		return end($shortnames);
	}

	public static function getBddidEntityBaseShortname($bddid) {
		$shortnames = static::getBddidEntityShortnames($bddid);
		return reset($shortnames);
	}

	public static function getBddidEntityId($bddid) {
		static::isValidBddid($bddid, true);
		preg_match('/\\d+$/', $bddid, $matches);
		return empty($matches) ? null : reset($matches);
	}

	public function findByBddid($bddid = null, $repositoryMethod = 'find') {
		if(null === $bddid) return null;
		static::isValidBddid($bddid, true);
		$shortname = $this->getBddidEntityShortname($bddid);
		$repo = $this->getRepository($shortname);
		if(!method_exists($repo, $repositoryMethod)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): method ".json_encode($repositoryMethod)."() does not exist for repository ".get_class($repo)." (of ".json_encode($shortname).")!", 1);
		$id = $this->getBddidEntityId($bddid);
		return !empty($id) ? $repo->$repositoryMethod($id) : null;
	}

	public function findByBddids($bddids) {
		if(is_string($bddids)) $bddids = [$bddids];
		if(empty($bddids)) return [];
		$repos = [];
		foreach ($bddids as $bddid) {
			static::isValidBddid($bddid, true);
			$shortname = $this->getBddidEntityBaseShortname($bddid);
			if(!isset($repos[$shortname])) {
				$repos[$shortname] = ['ids' => [], 'repo' => $this->getRepository($shortname)];
			}
			$repos[$shortname]['ids'][] = $this->getBddidEntityId($bddid);
		}
		$results = new ArrayCollection();
		foreach ($repos as $repo) {
			$entities = $repo['repo']->findById($repo['ids']);
			foreach ($entities as $entity) {
				if(!$results->contains($entity)) $results->add($entity);
			}
		}
		return $results->toArray();
	}


	/***********************************/
	/*** REPOSITORY
	/***********************************/

	/** 
	 * Get Repository
	 * @param mixed $classname = null
	 * @return Repository
	 */
	public function getRepository($classname) {
		if(preg_match('/Repository$/i', $classname)) {
			// $repository = $this->getEntityManager()->getRepository($classname);
			// if(method_exists($repository, 'setContainer')) $repository->setContainer($this->container);
			// got repository classname
			$matches = null;
			$returnValue = preg_match('/[\\/\\\\]+(\\w+)Repository$/', $classname, $matches);
			$classname = $matches[1];
		}
		$classname = $this->getClassnameByAnything($classname);
		if(null == $classname) throw new Exception(sprintf("ERROR line ".__LINE__." ".__METHOD__."(): classname ".json_encode($classname)." is not a valid entity!"), 1);
		$repository = $this->getEntityManager()->getRepository($classname);
		if(method_exists($repository, 'setContainer')) $repository->setContainer($this->container);
		return $repository;
	}

	/**
	 * Find entity by id
	 * @param integer|string $id
	 * @param string $method = null
	 * @param mixed $entity = null
	 * @return Object
	 */
	public function getEntityById($id, $method = null, $entity = null, $exceptionIfNotFound = false) {
		$repo = null;
		if($entity instanceOf EntityRepository) {
			$repo = $entity;
		} else {
			$classname = $this->getClassnameByAnything($entity);
			if(!empty($classname) && is_object($entity) && $entity->getId() === (integer)$id) return $entity;
			$repo = $this->getRepository($classname);
		}
		if(!($repo instanceOf EntityRepository)) throw new NotFoundHttpException("Repository not found");
		// var_dump(get_class($repo));
		$method ??= static::FIND_ONE_METHOD;
		$entity = method_exists($repo, $method) ? $repo->$method($id) : $repo->find($id);
		if(is_array($entity) && count($entity)) $entity = reset($entity);
		if($exceptionIfNotFound && empty($entity)) throw new NotFoundHttpException("Entity not found");
		return empty($entity) ? null : $entity;
	}


	/***********************************/
	/*** MODEL FOR CREATE
	/***********************************/

	public function getModelForCreate($classname, $groups = []) {
		// echo('<pre><h3>ModelForCreate models are:</h3><div><ul><li>'.implode('<li></li>', array_keys($this->modelForCreate)).'</li></ul></div></pre>');
		// if(count($this->modelForCreate)) die();
		$entity = $this->createNew($classname);
		if(empty($entity)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): could not create ".json_encode($classname)." from model!", 1);
		$model = null;
		// get in memory
		if(array_key_exists($entity->getClassname(), $this->modelForCreate)) $model = $this->modelForCreate[$entity->getClassname()];
		// get in session if not found
		if(empty($model)) {
			$cn = $classname;
			serviceTools::removeSeparator($cn);
			$bddid = $this->container->get(serviceKernel::class)->getSessionValue('model_entity-'.$cn, null, false);
			if(static::isValidBddid($bddid, false)) $model = $this->findByBddid($bddid);
		}
		if(is_object($model)) {
			$annotations = $this->getCopyFieldsAnnotations($entity, $groups);
			if(empty($annotations)) return $entity;
			foreach ($annotations as $property => $annotation) {
				$getter = $annotation->getter;
				$setter = $annotation->setter;
				$entity->$setter($model->$getter());
			}
		}
		return $entity;
	}

	public function setModelForCreate($entity) {
		$classname = $cn = $entity->getClassname();
		serviceTools::removeSeparator($cn);
		// set in memory
		$this->modelForCreate[$classname] = $entity;
		// set in session
		$this->container->get(serviceKernel::class)->setSessionValue('model_entity-'.$cn, $entity->getBddid());
		// echo('<pre><h3>ModelForCreate models are:</h3><div><ul><li>'.implode('<li></li>', array_keys($this->modelForCreate)).'</li></ul></div></pre>');
		// if(count($this->modelForCreate)) die();
		return $this;
	}

	public function removeModelForCreate($classname) {
		if(is_object($classname)) $classname = $classname->getClassname();
		$this->modelForCreate = array_filter($this->modelForCreate, function($class) use ($classname) {
			return $class !== $classname;
		}, ARRAY_FILTER_USE_KEY);
		return $this;
	}

	public function getCopyFieldsAnnotations($classname, $groups = [], $refresh = false) {
		$cn = $classname;
		serviceTools::removeSeparator($cn);
		$annotations = $this->serviceCache->getCacheData(
			// 'ModelForCreate@'.preg_replace('/(\\/|\\\\)+/', '_', $classname),
			'ModelForCreate@'.$cn,
			function() use ($classname) { return serviceClasses::getPropertysAnnotation($classname, ModelForCreate::class); },
			static::CACHE_NAME,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
		// Select groups
		if(!in_array('all', $groups)) {
			$annotations = array_filter($annotations, function($annotation) use ($groups) {
				if(in_array('all', $annotation->groups)) return true;
				if(empty($groups) && in_array('default', $annotation->groups)) return true;
				return count(array_intersect($annotation->groups, $groups)) > 0;
			});
		}
		// Add getters and setters
		foreach ($annotations as $property => $propertyAnnotation) {
			$propertyAnnotation->getter ??= Inflector::camelize('get_'.$property);
			$propertyAnnotation->setter ??= Inflector::camelize('set_'.$property);
		}
		// echo('<pre><h3>ModelForCreate annotations for '.$classname.' with groups '.json_encode($groups).'</h3><div>'); var_dump($annotations); die('</div></pre>');
		return $annotations;
	}


	/***********************************/
	/*** OPERATIONS ON ENTITIES
	/***********************************/

	public function getLaboClassMetadata(string $entityName) {
		// $entityName = static::removeProxyPrefix($entityName);
		$entityName = $this->getClassnameByAnything($entityName);
		if(array_key_exists($entityName, $this->laboClassMetadatas)) return $this->laboClassMetadatas[$entityName];
		return $this->laboClassMetadatas[$entityName] = new LaboClassMetadata($this->getEntityManager()->getClassMetadata($entityName));
	}

	public function getModel($classname = null, $asEntityIfPossible = false) {
		$classname = $this->getClassnameByAnything($classname);
		$metadata = $this->getLaboClassMetadata($classname);
		if(null === $classname) return null;
		if($asEntityIfPossible) {
			$entity = $metadata;
			$RC = new ReflectionClass($classname);
			// if(!$RC->isInstantiable()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): can not get instance of ".json_encode($classname)." because it is not instantiable!", 1);
			if($RC->isInstantiable()) {
				$optional = true;
				$m = new ReflectionMethod($classname, '__construct');
				foreach ($m->getParameters() as $reflectionParameter) {
					if(!$reflectionParameter->isOptional()) $optional = false;
				}
				if($optional) {
					// try {
						$entity = new $classname();				
					// } catch (Exception $e) {
						// 
					// }
					if($entity instanceOf CrudInterface) $entity->setServiceCruds($this->container->get(serviceCruds::class));
				}
			}
			$entity->_is_model = true;
			return $entity;
		}
		return $metadata;
	}

	/**
	 * Get new entity
	 * @param mixed $classname
	 * @param array $options = []
	 * @param callable $beforePostNewEventClosure = null // --> Closure called just BEFORE postNew Event
	 * @return entity | null
	 */
	public function createNew($classname, $options = [], callable $beforePostNewEventClosure = null) {
		$classname = $this->getClassnameByAnything($classname);
		if(null === $classname) $classname = $this->getClassnameByAnything($classname, true);
		if(null === $classname) return null;
		if(null != $options) {
			if(is_array($options)) {
				$parameters = [];
				foreach ($options as $key => $parameter) {
					$com = is_string($key) ? '"' : '';
					$parameters[] = '$options['.$com.$key.$com.']';
				}
				$eval = "return new \$classname(".implode(', ', $parameters).");";
				$entity = eval($eval);
			} else {
				$entity = new $classname($options);
			}
		} else {
			$entity = new $classname();
		}
		// DevTerminal::Info('<T5>Created new '.$entity->getShortname().'.', 2);
		if($entity instanceOf Item) $entity->initItem($this->container->get(serviceBasedirectory::class));
		if(is_callable($beforePostNewEventClosure)) $beforePostNewEventClosure($entity);
		$this->applyEvent($entity, BaseAnnotation::postNew);
		// if($entity instanceOf Item) $this->applyEvent($entity, ItemAnnotation::postNew);
		return $entity;
	}

	/**
	 * Appley preCreateForm event
	 * @param object $entity
	 * @return $entity
	 */
	public function preCreateForForm($entity) {
		$this->applyEvent($entity, BaseAnnotation::preCreateForm);
		// if($entity instanceOf Item) $this->applyEvent($entity, ItemAnnotation::preCreateForm);
		return $entity;
	}

	/**
	 * Appley preUpdateForm event
	 * @param object $entity
	 * @return $entity
	 */
	public function preUpdateForForm($entity) {
		$this->applyEvent($entity, BaseAnnotation::preUpdateForm);
		// if($entity instanceOf Item) $this->applyEvent($entity, ItemAnnotation::preUpdateForm);
		return $entity;
	}

	/**
	 * Apply preUpdateForm event if $entity is already managed, preCreateForm event if not
	 * @see https://www.strangebuzz.com/en/snippets/test-if-a-doctrine-entity-is-already-persisted-in-the-database
	 * @param object $entity
	 * @return $entity
	 */
	public function applyPreFormEvents($entity) {
		// echo('<h2>Entity '.$entity->getShortname().' '.json_encode($entity->__toString()).' is managed ? '.($this->getEntityManager()->contains($entity) ? '<i class="fa fa-check fa-fw fa-3x text-info"></i>' : '<i class="fa fa-times fa-fw fa-3x text-danger"></i>').'</h2>');
		$this->isLoadedEntity($entity) ? $this->preUpdateForForm($entity) : $this->preCreateForForm($entity);
		return $entity;
	}

	/**
	 * Apply events on $entity
	 * @param object $entity
	 * @param string $typeEvent
	 * @return serviceEntities
	 */
	public function applyEvent($entity, $typeEvent) {
		$this->getEntityManager()->getEventManager()->dispatchEvent($typeEvent, new LifecycleEventArgs($entity, $this->getEntityManager()));
		// $this->getEntityManager()->getEventManager()->dispatchEvent($typeEvent, new LifecycleEventArgs($entity, $this->ObjectManager));
		return $this;
	}

	public function hydrateEntity($entity, $values) {
		foreach ($values as $attribute => $value) {
			$method = 'set'.ucfirst($attribute);
			if(method_exists($entity, $method)) $entity->$method($value);
		}
		return $entity;
	}






	// /***********************************/
	// /*** EXCEPTIONS
	// /***********************************/

	// public function entityNotFound($message = null) {
	// 	if(null === $message) $message = static::NOT_FOUND;
	// 	throw new NotFoundHttpException($this->translator->trans($message, [], $this->getEntityShortname(), $this->getLocale()));
	// }





	/***********************************/
	/*** METADATA INFO
	/***********************************/

	public static function getRelationTypes() {
		return array(
			static::RELATION_MANYTOMANY,
			static::RELATION_ONETOMANY,
			static::RELATION_MANYTOONE,
			static::RELATION_ONETOONE,
		);
	}

	public static function getEnvironments() {
		return array(
			static::ENVIROMENT_DEV,
			static::ENVIROMENT_TEST,
			static::ENVIROMENT_PROD,
		);
	}


	public function getPropertyDescription($property, $entity) {
		if(is_object($entity)) $entity = get_class($entity);
		$CMD = $this->getEntityManager()->getClassMetadata($entity);
		if($CMD->hasField($property)) {
			return $CMD->getFieldMapping($property);
		}
		if($CMD->hasAssociation($property)) {
			return $CMD->getAssociationMapping($property);
		}
		return null;
	}

	/**
	 * Get type of relation of association
	 * @param string|ReflectionProperty $property
	 * @param Entity $entity
	 * @return null | Array {"name": relation "type", relation: } | false if property does not exist
	 */
	public static function getRelationType($property, $entity) {
		if(is_string($property)) {
			$reflClass = new ReflectionClass($entity);
			$property = property_exists(get_class($entity), $property) ? $reflClass->getProperty($property) : null;
		}
		if(!($property instanceOf ReflectionProperty)) return false;
		// if(!($property instanceOf ReflectionProperty)) throw new Exception("serviceAnnotation::getRelationType() error: first parameter ".json_encode($property)." must be a valid property of ".get_class($entity)." or an instance of ReflectionProperty", 1);
		
		$reader = new AnnotationReader();
		foreach (static::getRelationTypes() as $rel) {
			$relation = $reader->getPropertyAnnotation($property, static::DOCTRINE_MAPPING.$rel);
			if(null !== $relation) return ['name' => $rel, 'relation' => $relation];
		}
		return null;
	}

	/**
	 * Get target attribute and side of association
	 * @param Annotation $relation
	 * @param object $entity
	 * @return null|array:3 [
	 *   "attribute" => "place"
	 *   "side" => "mappedBy"|"inversedBy"
	 *   "target" => classname
	 * ]
	 */
	public function getTargetField(Annotation $relation, $entity) {
		$targetField = property_exists($relation, 'mappedBy') ? array('attribute' => $relation->mappedBy, 'side' => 'mappedBy', 'targetClass' => $this->getTargetEntityClassname($relation->targetEntity, $entity)) : null;
		if(null === $targetField) $targetField = property_exists($relation, 'inversedBy') ? array('attribute' => $relation->inversedBy, 'side' => 'inversedBy', 'targetClass' => $this->getTargetEntityClassname($relation->targetEntity, $entity)) : null;
		return $targetField;
	}

	/**
	 * if targetEntity attribute of annotation is shortname, use this to get full classname
	 * @param string $targetName
	 * @param object $entity
	 * @return string
	 */
	public function getTargetEntityClassname($targetName, $entity) {
		if(preg_match('#^.+\\.+$#', $targetName)) return preg_replace('#\\\\#', '\\', $targetName);
		$exp = explode('\\', get_class($entity));
		$exp[count($exp) - 1] = $targetName;
		return implode('\\', $exp);
	}

	public function getMetadataAssociations($entity, $classes, $addSubclasses = false) {
		$classname =  $this->getClassnameByAnything($entity);
		$em = $this->getEntityManager();
		$metadata = $em->getClassMetadata($classname);
		$classes = (array)$classes;
		$fc = [];
		foreach ($classes as $key => $class) {
			if(is_string($class) || is_object($class)) $class = $this->getClassnameByAnything($class);
			if(is_string($class) && !in_array($class, $fc)) {
				$fc[] = $class;
				if($addSubclasses) {
					$meta = $em->getClassMetadata($class);
					$parents = $meta->parentClasses;
					$fc = array_unique(array_merge($fc, $parents));
				}
			}
		}
		$associations = $metadata->getAssociationMappings();
		$associations = array_filter($associations, function ($association) use ($fc, $em, $addSubclasses) {
			if(!$addSubclasses) return in_array($association['targetEntity'], $fc);
			$meta = $em->getClassMetadata($association['targetEntity']);
			$parents = $meta->parentClasses;
			$intersect = array_intersect($parents, $fc);
			return count($intersect) > 0;
		});
		// foreach ($associations as $key => $association) {
		// 	$associations[$key]['methods'] = serviceClasses::getPropertyMethods($association['fieldName'], $entity);
		// }
		return $associations;
	}

	public function getMetadataFields($entity, $fields = true, $types = true) {
		$classname = $this->getClassnameByAnything($entity);
		$metadata = $this->getEntityManager()->getClassMetadata($classname);
		$allFields = $metadata->getFieldNames();
		if(true === $fields || empty($fields)) $fields = $allFields;
		if(is_string($fields)) $fields = [$fields];
		$fields = array_filter($allFields, function($field) use ($fields) { return in_array($field, $fields); });
		$serviceCruds = $this->container->get(serviceCruds::class);
		$result = [];
		foreach ($fields as $fieldName) {
			$mapping = $metadata->getFieldMapping($fieldName);
			if($types === true || in_array($mapping['type'], $types)) {
				$mapping['cruds'] = $serviceCruds->getEntityActionsGrants($entity);
				$result[$fieldName] = $mapping;
			}
		}
		return $result;
	}

	public function getCommonShortname($entities) {
		$metadata = $this->getCommonMetadata($entities);
		if($metadata instanceOf LaboClassMetadata) return $metadata->getShortName();
		if($metadata instanceOf ClassMetadata) return $metadata->getReflectionClass()->getShortName();
		return null;
	}

	public function getCommonMetadata($entities) {
		$class = $this->getCommonClass($entities);
		return is_string($class) ? $this->getLaboClassMetadata($this->getEntityManager()->getClassMetadata($class)) : null;
	}

	public function getCommonClass($entities) {
		$classes = $this->getCommonClasses($entities);
		return empty($classes) ? null : reset($classes);
	}

	public function getCommonClasses($entities) {
		$classes = null;
		$foundClass = null;
		foreach ($entities as $key => $entity) {
			$class = $this->getClassnameByAnything($entity);
			$metadata = $this->getEntityManager()->getClassMetadata($class);
			$allclasses = array_merge([$class], $metadata->parentClasses);
			$classes = !is_array($classes) ? $allclasses : array_intersect($classes, $allclasses);
		}
		if(empty($classes)) return []; // no common class! :-o
		$classes = array_unique($classes);
		// echo('<pre><h3>Classes :</h3>');var_dump($classes);echo('</pre>');
		// get classes, sorted by hierarchy (first: final class)
		$list = [];
		foreach ($classes as $class) {
			$metadata = $this->getEntityManager()->getClassMetadata($class);
			if((count($metadata->parentClasses) + 1) > count($list)) $list = array_merge([$class], $metadata->parentClasses);
		}
		// echo('<pre><h3>List :</h3>');var_dump($list);echo('</pre>');
		return $list;
	}

	public function getIdentifiers($entity, $addAssociations = false) {
		$class = $this->getClassnameByAnything($entity);
		if(!is_string($class)) return [];
		$class = $this->getEntityManager()->getClassMetadata($class);
		$ids = [];
		foreach ($class->identifier as $key => $field) {
			if(isset($class->fieldMappings[$field])) $ids[$field] = $class->fieldMappings[$field];
				else if($addAssociations && isset($class->associationMappings[$field])) $ids[$field] = $class->associationMappings[$field];
		}
		return $ids;
	}

	public function getMainIdentifier($entity, $fieldname = false, $addAssociations = false) {
		$default = 'id';
		$ids = $this->getIdentifiers($entity, $addAssociations);
		if(isset($ids[$default])) return $fieldname ? $default : $ids[$default];
		foreach ($ids as $id => $data) {
			if(!isset($data['targetEntity'])) return $fieldname ? $id : $data;
		}
		return $fieldname ? null : [];
	}

	/***********************************/
	/*** GET SPECIAL FEATURES
	/***********************************/

	public function getBinaryAssociations($entity, $types = []) {
		$binarys = $this->getMetadataAssociations($entity, Binary::class, true);
		$allTypes = Fileformat::getTypes();
		if(empty($types)) $types = $allTypes;
		if(is_string($types)) $types = [$types];
		$types = array_filter($allTypes, function($type) use ($types) { return in_array($type, $types); });
		if(empty($types)) return [];
		$binarys = array_filter($binarys, function($data) use ($types) {
			$testEntity = new $data['targetEntity']();
			$inter = array_intersect($testEntity->getAuthorizedTypes(), $types);
			return count($inter) > 0;
		});
		return $binarys;
	}

	public function compileSerializedEntity(&$data) {
		// echo('<pre>');var_dump($data);
		// $forbiddens = $data['classname']::getFillForbiddens();
		foreach ($data as $attribute => $value) {
			$valid_attribute = $data['classname']::getFieldname($attribute);
			// echo('<h3>'.$data['classname'].' = '.$valid_attribute.' (from '.$attribute.')</h3>');
			$prop_described = $this->getPropertyDescription($valid_attribute, $data['classname']);
			// var_dump($prop_described);
			if($data['classname']::isFillAuthorized($attribute)) {
				if(isset($prop_described['targetEntity'])) {
					// association
				} else {
					// value
					switch ($prop_described['type']) {
						case 'datetime':
							$data[$attribute] = is_string($value) ? new DateTime($value) : null;
							break;
					}
				}
			}
		}
		// die('</pre>');
		return $data;
	}


}