<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use FOS\UserBundle\Util\TokenGenerator;
// BaseBundle
use ModelApi\BaseBundle\Entity\Userrequest;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceKernel;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Service\serviceGrants;
use ModelApi\UserBundle\Service\serviceUser;

use \Exception;

class serviceUserrequest implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Userrequest::class;

	const ROLECOLLAB_NAME = 'rolecollab';
	const VALIDATE_ENTITY_NAME = 'validate_entity';
	const REQUEST_STATUS = [
		0 => 'waiting',
		1 => 'validated',
		2 => 'rejected',
	];

	protected $container;
	protected $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}


	public function getCurrentUserOrNull() {
		return $this->container->get(serviceUser::class)->getCurrentUserOrNull();
	}

	public static function getUserrequestNames() {
		return [
			static::ROLECOLLAB_NAME,
			static::VALIDATE_ENTITY_NAME,
		];
	}

	public static function isValidName($name, $exceptionIfFalse = false) {
		$valid = in_array($name, static::getUserrequestNames());
		if($exceptionIfFalse && !$valid) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Userrequest name is not valid. Use a name in ".json_encode(static::getUserrequestNames()).", please.", 1);
		return $valid;
	}


	/***********************************************************************************/
	/*** NEW USERREQUEST
	/***********************************************************************************/

	public function namedRequest_create($name, User $user, User $requester = null) {
		if(!$this->isPossibleRequestCreate($name, $user)) return null;
		$userrquest = $this->createNew(['user' => $user, 'reqname' => $name, 'requester' => $requester ?? $this->getCurrentUserOrNull()]);
		$errors = $this->container->get('validator')->validate($userrquest);
		if($errors->count()) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): new Userrequest is invalid: ".$errors->getMessages(), 1);
		return $userrquest;
	}


	/***********************************************************************************/
	/*** MAKE USERREQUEST
	/***********************************************************************************/

	public function hasRequest($name, User $user, $reqstatus = []) {
		$this->isValidName($name, true);
		return $this->getRepository()->getCountRequests($name, $user, $reqstatus) > 0;
	}

	public function isPossibleRequestCreate($name, User $user) {
		$this->isValidName($name, true);
		switch ($name) {
			case static::ROLECOLLAB_NAME:
				if(!$user->isOnOrderRequestCollab()) return false;
				if($this->container->get(serviceGrants::class)->isGranted(['ROLE_COLLAB'], $user)) return false;
				if($this->hasRequest($name, $user)) return false;
				return true;
				break;
			case static::VALIDATE_ENTITY_NAME:
				if($this->container->get(serviceGrants::class)->isGranted(['ROLE_COLLAB'], $user)) return false;

				return true;
				break;
			default:
				return true;
				break;
		}
		return false;
	}

	public function compileUserrequestsOnEntity($entity, User $user) {
		foreach ($this->getUserrequestNames() as $name) {
			switch ($name) {
				case static::ROLECOLLAB_NAME:
					# code...
					break;
				case static::VALIDATE_ENTITY_NAME:
					# code...
					break;
				
				default:
					# code...
					break;
			}
		}
	}

	public function sendUserrequest($entity) {
		if(method_exists($entity, 'isAdminvalidated') && !$entity->isAdminvalidated()) {
			if($this->container->get(serviceKernel::class)->isDev()) {
				$this->container->get(serviceFlashbag::class)->addFlashSweet('success', 'La demande de validation de '.$entity->getShortname().' '.json_encode($entity->getName()).' a été envoyée aux administrateurs.');
			} else {
				$this->container->get(serviceFlashbag::class)->addFlashSweet('success', 'La demande de validation a été envoyée aux administrateurs.');
			}
		}
		return $this;
	}



	/***********************************************************************************/
	/*** VALIDATE USERREQUEST
	/***********************************************************************************/

	public function validateUserrequest(Userrequest $userrequest, $status = 1, User $validatedBy = null, $sendToUser = true) {
		$userrequest->setReqstatus($validatedBy, $status);

		$this->getEntityManager()->flush();
		return $this;
	}

	public function sendStatusToUser(Userrequest $userrequest) {
		$sent = true;
		return $sent;
	}


	/***********************************************************************************/
	/*** TOOLS USERREQUEST
	/***********************************************************************************/

	public function getToken($string = null) {
		if(!is_string($string) || empty(trim($string))) $string = random_bytes(32);
		if($this->container->has('security.csrf.token_manager')) {
			// Symfony 4+
			$tokenProvider = $this->container->get('security.csrf.token_manager');
			return $tokenProvider->getToken($string)->getValue();
		} else {
			// any other version
			return TokenGenerator::generateToken();
		}
	}

	public static function numerizeReqstatus(&$reqstatus) {
		if(is_string($reqstatus) && preg_match('/^\\d+$/', $reqstatus)) $reqstatus = intval($reqstatus);
		$origin_reqstatus = $reqstatus;
		$status_list = array_flip(serviceUserrequest::REQUEST_STATUS);
		if(is_string($reqstatus) || is_integer($reqstatus)) {
			$reqstatus = is_integer($reqstatus) ? $reqstatus : $status_list[$reqstatus];
			if(!in_array($reqstatus, $status_list)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): reqstatus ".json_encode($origin_reqstatus)." is not valid! Use status in ".json_encode($status_list).", please.", 1);
		} else if(is_array($reqstatus)) {
			foreach ($reqstatus as $key => $rs) {
				$reqstatus[$key] = static::numerizeReqstatus($rs);
			}
		}
	}

	public static function humanizeReqstatus(&$reqstatus) {
		$status_list = serviceUserrequest::REQUEST_STATUS;
		if(is_string($reqstatus) || is_integer($reqstatus)) {
			$reqstatus = is_string($reqstatus) ? $reqstatus : $status_list[$reqstatus];
			if(!in_array($reqstatus, serviceUserrequest::REQUEST_STATUS)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): reqstatus ".json_encode($origin_reqstatus)." is not valid! Use status in ".json_encode(serviceUserrequest::REQUEST_STATUS).", please.", 1);
		} else if(is_array($reqstatus)) {
			foreach ($reqstatus as $key => $rs) {
				$reqstatus[$key] = static::humanizeReqstatus($rs);
			}
		}
	}


}