<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\SerializationContext;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolation;

use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\AnnotBundle\Annotation\EntityAnnotationInterface;

use ModelApi\DevprintsBundle\Service\DevTerminal;

use \ReflectionClass;
use \ReflectionProperty;
use \ReflectionMethod;
use \Exception;
use \DateTime;

class serviceClasses {

	protected $container;
	protected $translator;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->translator = $this->container->get('translator');
		return $this;
	}


	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString() {
		return static::getShortname(static::class);
	}


	/***********************************/
	/*** GET PROPERTIES OF CLASS
	/***********************************/

	/**
	 * Get shortname of $class
	 * @param string|object $class
	 * @param string $lower = false
	 * @return string
	 */
	public static function getShortname($class, $lower = false) {
		if(!is_object($class) && !(is_string($class) && class_exists($class))) return null;
		return $lower ? strtolower((new ReflectionClass($class))->getShortName()) : (new ReflectionClass($class))->getShortName();
	}

	/**
	 * Get classname of $class
	 * @param string $class
	 * @return string
	 */
	public static function getClassname($class) {
		if(!is_object($class) && !(is_string($class) && class_exists($class))) return null;
		return (new ReflectionClass($class))->getName();
	}

	/**
	 * Get shortname of $name
	 * @param string $name
	 * @return string
	 */
	public static function getShortnameIfClassname($name) {
		if(class_exists($name)) {
			// classname, so get shortname
			$RC = new ReflectionClass($name);
			return $RC->getShortName();
		}
		return $name;
	}

	public static function isInstantiable($classname) {
		if(is_object($classname)) return true;
		$RC = new ReflectionClass($classname);
		return $RC->isInstantiable();
	}

	public static function getReflectionProperty($property, $reflClass) {
		if(!($reflClass instanceOf ReflectionClass)) $reflClass = new ReflectionClass($reflClass);
		try {
			return $reflClass->getProperty($property);			
		} catch (Exception $e) {
			return null;
		}
		return null;
	}

	public static function getSubclasses($parent, $asShortnames = false, $addSelfClass = false) {
		if(is_object($parent)) $parent = get_class($parent);
		$subclasses = $addSelfClass ? [$parent] : [];
		if(!empty($parent) && class_exists($parent)) {
			foreach (get_declared_classes() as $class) {
				if (is_subclass_of($class, $parent)) {
					if($asShortnames) {
						$RC = new ReflectionClass($class);
						$class = $RC->getShortName();
					}
					$subclasses[] = $class;
				}
			}
		}
		return $subclasses;
	}

	/**
	 * Get getter/setter method names of $object by $property
	 * @param string $property
	 * @param $object
	 * @param string $search = 'set'
	 * @param string $priority = 'singularized' (singularized|pluralized)
	 * @param boolean $extended = false (search methods that BEGIN with same names of methods. Ex.: all getItemsXxxx() are available with getItems())
	 * @return array {getters: array <string>, setters: array <string>, removers: array <string>, all: array <string>, method: string | null}
	 */
	public static function getPropertyMethod($property, $object, $search = 'set', $priority = 'singularized', $extended = false, $findAnywayIfPropertyFails = true) {
		$methods = static::getPropertyMethods($property, $object, $search, $priority, $extended, $findAnywayIfPropertyFails);
		return is_string($methods['method']) ? $methods['method'] : null;
	}

	/**
	 * Get getter method name of $object by $property
	 * if $search is defined (string) returns this getter/setter if exists
	 * @param string $property
	 * @param $object
	 * @param string $search = 'set'
	 * @param string $priority = 'singularized' (singularized|pluralized)
	 * @param boolean $extended = false (search methods that BEGIN with same names of methods. Ex.: all getItemsXxxx() are available with getItems())
	 * @return array {getters: array <string>, setters: array <string>, removers: array <string>, all: array <string>, method: string | null}
	 */
	public static function getPropertyMethods($property, $object, $search = 'set', $priority = 'singularized', $extended = false, $findAnywayIfPropertyFails = true) {
		$methods = ['all' => [], 'method' => null];
		// if(!is_object($object)) return $methods;
		// if(is_string($object) && class_exists($object))
		if(!is_object($object) || (is_string($object) && !class_exists($object))) throw new Exception("Error ".__METHOD__."(): second argument must be an object or a string of a class!", 1);
		$reflClass = new ReflectionClass($object);
		$methods['class'] = $reflClass->name;
		$reflProp = null;
		if(is_string($property)) {
			$reflProp = static::getReflectionProperty($property, $reflClass);
			if($reflProp instanceOf ReflectionProperty) $property = $reflProp->name;
		} else {
			$reflProp = $property;
			$property = $property->name;
		}
		if(!($reflProp instanceOf ReflectionProperty) && !$findAnywayIfPropertyFails) throw new Exception("Error ".__METHOD__."(): first argument property must be string or ReflectionProperty and property must exist!", 1);
		$methods['property'] = $property;
		$priorities = ['singularized','pluralized'];
		if(!is_string($priority) || !in_array($priority, $priorities)) $priority = end($priorities);
		$allmethods = $reflClass->getMethods(ReflectionMethod::IS_PUBLIC);
		// $allmethods = array_filter($allmethods, function(ReflectionMethod $reflMethod) { return $reflMethod->isPublic(); });
		$prefixes = ['set','add','get','is','has','remove'];
		if(!is_string($search)) $search = reset($prefixes);
		if(!in_array(strtolower($search), $prefixes) && method_exists($object, $search)) $methods['method'] = $search;
		$baseSearch = preg_replace('/^('.implode('|', $prefixes).')?(.*)$/i', '$1', $search);
		// if(empty($baseSearch)) $baseSearch = reset($prefixes);
		// echo('<p>');var_dump($baseSearch);echo('</p>');
		$logisSearch1 = $priority === 'singularized' ? Inflector::singularize(Inflector::camelize($baseSearch.'_'.$property)) : Inflector::pluralize(Inflector::camelize($baseSearch.'_'.$property));
		$logisSearch2 = $priority === 'pluralized' ? Inflector::singularize(Inflector::camelize($baseSearch.'_'.$property)) : Inflector::pluralize(Inflector::camelize($baseSearch.'_'.$property));
		$searchs = [];
		$methodFound = ['singularized_regular' => null,'pluralized_regular' => null, 'singularized_extended' => null,'pluralized_extended' => null];
		foreach ($prefixes as $prefix) {
			$searchs[$prefix] = [Inflector::singularize(Inflector::camelize($prefix.'_'.$property)), Inflector::pluralize(Inflector::camelize($prefix.'_'.$property))];
		}
		foreach ($prefixes as $prefix) {
			if(!isset($methods[$prefix])) $methods[$prefix] = [];
			foreach ($allmethods as $method) {
				// if(preg_match('/^('.implode('|', $searchs[$prefix]).')'.($extended ? '\\w+':'').'$/', $method->name)) {
				if(preg_match('/^'.$searchs[$prefix][0].'$/i', $method->name)) {
					$methods[$prefix][] = $method->name;
					if(null === $methodFound['singularized_regular']) $methodFound['singularized_regular'] = $method;
					if(preg_match('/^'.$logisSearch2.'$/i', $method->name)) $methodFound['singularized_regular'] = $method;
					if(preg_match('/^'.$logisSearch1.'$/i', $method->name)) $methodFound['singularized_regular'] = $method;
				}
				if(preg_match('/^'.$searchs[$prefix][1].'$/i', $method->name)) {
					$methods[$prefix][] = $method->name;
					if(null === $methodFound['pluralized_regular']) $methodFound['pluralized_regular'] = $method;
					if(preg_match('/^'.$logisSearch2.'$/i', $method->name)) $methodFound['pluralized_regular'] = $method;
					if(preg_match('/^'.$logisSearch1.'$/i', $method->name)) $methodFound['pluralized_regular'] = $method;
				}
				if($extended && preg_match('/^('.implode("|", $searchs[$prefix]).')\\w+$/i', $method->name)) $methods[$prefix][] = $method->name;
			}
			$methods[$prefix] = array_unique($methods[$prefix]);
		}
		foreach ($prefixes as $prefix) $methods['all'] = array_unique(array_merge($methods['all'], $methods[$prefix]));
		// echo('<pre>');var_dump($methodFound);echo('</pre>');
		if(null === $methods['method'] && !empty($baseSearch)) {
			$substitute = null;
			foreach ($methodFound as $key => $method) if($method instanceOf ReflectionMethod) {
				if(null === $methods['method'] && preg_match('/^'.$priority.'_/', $key) && preg_match('/^'.$baseSearch.'/i', $method->name)) $methods['method'] = $method->name;
				if(null === $substitute && preg_match('/^'.$baseSearch.'/i', $method->name)) $substitute = $method->name;
			}
			if(null === $methods['method']) $methods['method'] = $substitute;
		}
		// echo('<pre>');var_dump(['Methods' => $methods]);echo('</pre>');
		return $methods;
	}


	/***********************************/
	/*** CLONE OBJECT
	/***********************************/

	public function cloneObject($object, $new = null) {
		if(is_object($new)) {
			// Do nothing
		} else if(is_string($new)) {
			$new = new $new();
		} else {
			$new = clone $object;
		}
		// copy properties...

		return $new;
	}


	/***********************************/
	/*** VALIDATE OBJECT
	/***********************************/

	protected function getRawValue($object, $property_name) {
		$rawValue = 'FAILED TO GET PROPERTY VALUE';
		if(method_exists($object, 'getRawValue')) {
			$rawValue = $object->getRawValue($property_name);
			if($rawValue !== 'FAILED TO GET PROPERTY VALUE') return [
				'getter' => true,
				'value' => $rawValue,
			];
		}
		$getter = serviceClasses::findPublicGetter($object, $property_name);
		return [
			'getter' => $getter,
			'value' => is_string($getter) ? $object->$getter() : 'FAILED TO GET PROPERTY VALUE',
		];
	}

	public function getErrorsReport($object, ClassMetadata $classmetadata = null, $excludes = []) {
		$constraintViolationList = $this->container->get('validator')->validate($object);
		// Add propertys
		// $constraintViolationList->object = $object;
		// $constraintViolationList->is_entity = $classmetadata instanceOf ClassMetadata;
		// $constraintViolationList->classmetadata = $classmetadata;
		if(count($excludes)) {
			foreach ($constraintViolationList as $key => $constraintViolation) {
				$propertyPath = $constraintViolation->getPropertyPath();
				$property_name = lcfirst(preg_replace('/valid/i', '', $propertyPath));
				if(in_array($property_name, $excludes) || empty($property_name)) $constraintViolationList->remove($key);
			}
		}

		$constraintViolationList->checklist = method_exists($object, 'getValidationChecklist') ? $object->getValidationChecklist() : [];
		foreach ($constraintViolationList->checklist as $property_name => $value) {
			$rawValue = $this->getRawValue($object, $property_name);
			$constraintViolationList->checklist[$property_name] = [
				'label' => $value,
				'constraintViolation' => null,
				'getter' => $rawValue['getter'],
				'value' => $rawValue['value'],
			];
		}
		foreach ($constraintViolationList as $key => $constraintViolation) {
			$propertyPath = $constraintViolation->getPropertyPath();
			$property_name = lcfirst(preg_replace('/valid/i', '', $propertyPath));
			if(array_key_exists($property_name, $constraintViolationList->checklist)) {
				$constraintViolationList->checklist[$property_name]['constraintViolation'] = $constraintViolation;
			} else {
				$rawValue = $this->getRawValue($object, $property_name);
				$constraintViolationList->checklist[$property_name] = [
					'label' => $property_name,
					'constraintViolation' => $constraintViolation,
					'getter' => $rawValue['getter'],
					'value' => $rawValue['value'],
				];
			}
		}
		return $constraintViolationList;
	}

	public static function findPublicGetter($classname, $property, $tests = ['get','is','has']) {
		foreach ($tests as $test) {
			$method = Inflector::camelize($test.'_'.$property);
			if(static::isPublicMethod($classname, $method)) return $method;
		}
		return null;
	}

	public static function isPublicMethod($classname, $method) {
		if(is_object($classname)) $classname = get_class($classname);
		if(!method_exists($classname, $method)) return false;
		$RM = new ReflectionMethod($classname, $method);
		return $RM->isPublic();
	}

	/***********************************/
	/*** EXCEPTIONS
	/***********************************/

	protected function requestNotLoaded($message = null) {
		if(null === $message) $message = "Locale is not valid.";
		throw new ServiceUnavailableHttpException(null, $this->translator->trans($message, [], 'messages', $this->getDefaultLocale()));
	}

	protected function localeNotValid($message = null) {
		if(null === $message) $message = "Service ".$this->__toString()." has not been loaded with your Request.";
		throw new ServiceUnavailableHttpException(null, $this->translator->trans($message, [], 'messages', $this->getLocale()));
	}


	/***********************************/
	/*** SERIALIZE
	/***********************************/

	// public function getSerialized_old($data, $groups = ["base","detail"], $as_array = false, $levels = 25) {
	// 	// if($levels <= 0) return null;
	// 	if($data instanceOf Collection) $data = $data->toArray();
	// 	if(is_array($data) && $levels > 0) {
	// 		foreach ($data as $key => $dat) {
	// 			$data[$key] = $this->getSerialized($dat, $groups, false, $levels - 1);
	// 		}
	// 		return $as_array ? $data : json_encode($data);
	// 	} else if(is_object($data)) {
	// 		$data = $this->container->get('jms_serializer')->serialize($data, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups((array)$groups));
	// 		return $as_array ? json_decode($data, true) : $data;
	// 	}
	// 	return $as_array ? $data : json_encode($data);
	// }

	public function getSerialized($data, $groups = ["base","detail"], $as_array = false, $levels = 25) {
		// if($levels <= 0) return null;
		if($data instanceOf Collection) $data = $data->toArray();
		if(is_array($data) && $levels > 0) {
			foreach ($data as $key => $dat) {
				$data[$key] = $this->getSerialized($dat, $groups, true, $levels - 1);
			}
		} else if(is_object($data)) {
			$data = json_decode($this->container->get('jms_serializer')->serialize($data, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups((array)$groups)), true);
		}
		return $as_array ? $data : json_encode($data);
	}


	/*******************************************/
	/*** CLASSES DESCRIPTIONS & HIERARCHY
	/*******************************************/

	/**
	 * Get parent classes of class $classname as array
	 * Array : ShortName => ClassName
	 * @param mixed $classname
	 * @param boolean $addHimSelf = true
	 * @param boolean $includeNonInstantiable = true
	 * @param boolean $asShortnames = false
	 * @return Array <string> | null
	 */
	public static function getParentClasses($classname, $addHimSelf = true, $includeNonInstantiable = true, $asShortnames = false, $reverse = false) {
		$classes = array();
		if(is_object($classname)) $classname = get_class($classname);
		if(class_exists($classname)) {
			$RC = new ReflectionClass($classname);
			// add himself
			if(true === $addHimSelf) $classes[$RC->getName()] = $RC->getShortName();
			while (false !== $RC = $RC->getParentClass()) {
				if($RC->isInstantiable() || $includeNonInstantiable) $classes[$RC->getName()] = $RC->getShortName();
			}
			// print("     • Classes for ".$classname.": ".PHP_EOL);
			// var_dump($classes);
			$result = $asShortnames ? $classes : array_keys($classes);
			$result = array_unique($result);
			return $reverse ? array_reverse($result) : $result;
		}
		return [];
	}

	public static function getTraitNames($classname, $asShortnames = false) {
		$classes = array();
		if(is_object($classname)) $classname = get_class($classname);
		if(class_exists($classname)) {
			$RC = new ReflectionClass($classname);
			foreach ($RC->getTraits() as $RCT) $classes[$RCT->getName()] = $RCT->getShortName();
			while (false !== $RC = $RC->getParentClass()) {
				foreach ($RC->getTraits() as $RCT) $classes[$RCT->getName()] = $RCT->getShortName();
			}
			return $asShortnames ? $classes : array_keys($classes);
		}
		return [];
	}

	public static function hasTraits($classname, $traits = null) {
		// if(is_object($classname)) $classname = get_class($classname);
		// if(class_exists($classname)) {
			if(!is_array($traits)) $traits = [$traits];
			$shorts_traits = static::getTraitNames($classname, true);
			$class_traits = array_keys($shorts_traits);
			return array_intersect($traits, $shorts_traits) || array_intersect($traits, $class_traits);
		// }
		// return false;
	}

	public static function getInstances($classname, $asShortnames = false) {
		if(is_object($classname)) $classname = get_class($classname);
		if(class_exists($classname)) {
			$RC = new ReflectionClass($classname);
			$classes[$RC->getName()] = $RC->getShortName();
			foreach ($RC->getTraits() as $RCT) $classes[$RCT->getName()] = $RCT->getShortName();
			while (false !== $RC = $RC->getParentClass()) {
				$classes[$RC->getName()] = $RC->getShortName();
				foreach ($RC->getTraits() as $RCT) $classes[$RCT->getName()] = $RCT->getShortName();
			}
			return $asShortnames ? $classes : array_keys($classes);
		}
		return [];
	}

	public static function isInstance($classname, $instances) {
		if(is_string($instances)) $instances = [$instances];
		$list = static::getInstances($classname, true);
		return count(array_intersect($instances, $list)) > 0;
	}


	public static function getInterfaces($classname, $asShortnames = false) {
		$classes = array();
		if(is_object($classname)) $classname = get_class($classname);
		if(class_exists($classname)) {
			$interfaces = class_implements($classname);
			if(is_array($interfaces)) foreach ($interfaces as $interface) {
				$RC = new ReflectionClass($interface);
				$classes[$RC->getName()] = $RC->getShortName();
			}
			return $asShortnames ? array_values($classes) : array_keys($classes);
		}
		return [];
	}

	public static function isInterface($classname, $interfaces) {
		if(is_string($interfaces)) $interfaces = [$interfaces];
		$list = static::getInterfaces($classname, true);
		return count(array_intersect($interfaces, $list));
	}

	public function isInstanceOrInterface($instanceNames, $asShrortname = null) {
		return static::isInstance($instanceNames, $asShrortname) || static::isInterface($instanceNames, $asShrortname);
	}


	/***********************************/
	/*** COMPILE METHOD PARAMETERS
	/***********************************/

	public static function compileMethodParameters($class, $method, $parameters, $returnFalseIfNotAccessible = false) {
		if(!is_array($parameters)) $parameters = [$parameters];
		$m = new ReflectionMethod($class, $method);
		if($returnFalseIfNotAccessible && !($m->isPublic() ||$m->isPublic())) return false;
		$ordered_parameters = [];
		$order = 0;
		$funcparams = $m->getParameters();
		foreach ($funcparams as $key => $param) {
			if($param->name === 'criteria' && !array_key_exists('criteria', $parameters)) {
				$ordered_parameters = [$parameters];
				break;
			}
			if(array_key_exists($param->name, $parameters)) {
				$ordered_parameters[$param->name] = $parameters[$param->name];
			} else {
				$ordered_parameters[$param->name] = array_key_exists($order, $parameters) ? $parameters[$order] : null;
				$order++;
			}
		}
		return $ordered_parameters;
	}


	/***********************************/
	/*** ANNOTATION TOOLS
	/***********************************/

	public static function getPropertyAnnotation($entity, $property, $annotation, $asInfo = false) {
		$reader = new AnnotationReader();
		// if (is_string($annotation)) $annotation = new $annotation();
		if(is_object($annotation)) $annotation = get_class($annotation);
		if(is_object($property)) $property = $property->name;
		// if($property instanceOf ReflectionProperty) {
			// $propertyAnnotation = $reader->getPropertyAnnotation($property, $annotation);
			// if(!empty($propertyAnnotation)) return $asInfo ? ['Annotation' => $propertyAnnotation, 'ReflectionProperty' => $property, 'ReflectionClass' => $reflClass] : $propertyAnnotation;
		// } else if(is_string($property)) {
			$properties = [];
			foreach (static::getInstances($entity) as $class) {
				$RC = new ReflectionClass($class);
				$propertyAnnotation = $reader->getPropertyAnnotation($RC->getProperty($property), $annotation);
				if(!empty($propertyAnnotation)) {
					if($propertyAnnotation instanceOf EntityAnnotationInterface && $entity instanceOf CrudInterface) $propertyAnnotation->setEntity($entity, ['property' => $property]);
					return $asInfo ? ['Annotation' => $propertyAnnotation, 'ReflectionProperty' => $property, 'ReflectionClass' => $reflClass] : $propertyAnnotation;
				}
			}
		// }
		return null;
	}

	/**
	 * Get entity's properties names with annotation
	 * @return array [property_name => Annotation]
	 */
	public static function getPropertysAnnotation($entity, $annotation, $asInfo = false) {
		$reader = new AnnotationReader();
		// if (is_string($annotation)) $annotation = new $annotation();
		if(is_object($annotation)) $annotation = get_class($annotation);
		$propertyAnnotations = [];
		foreach (static::getInstances($entity) as $class) {
			$reflClass = new ReflectionClass($class);
			foreach ($reflClass->getProperties() as $property) if(!isset($propertyAnnotations[$property->name])) {
				$propertyAnnotation = $reader->getPropertyAnnotation($property, $annotation);
				if (is_object($propertyAnnotation)) {
					if($propertyAnnotation instanceOf EntityAnnotationInterface && $entity instanceOf CrudInterface) $propertyAnnotation->setEntity($entity, ['property' => $property->name]);
					$propertyAnnotations[$property->name] = $asInfo ?
						['Annotation' => $propertyAnnotation, 'ReflectionProperty' => $property, 'ReflectionClass' => $reflClass]:
						$propertyAnnotation;
				}
			}
		}
		return $propertyAnnotations;
	}

	public static function getMethodAnnotation($entity, $method, $annotation, $asInfo = false) {
		$reader = new AnnotationReader();
		// if (is_string($annotation)) $annotation = new $annotation();
		if(is_object($annotation)) $annotation = get_class($annotation);
		if(is_object($method)) $method = $method->name;
		// if($method instanceOf ReflectionMethod) {
			// $methodAnnotation = $reader->getMethodAnnotation($method, $annotation);
			// if(!empty($methodAnnotation)) return $asInfo ? ['Annotation' => $methodAnnotation, 'ReflectionMethod' => $method, 'ReflectionClass' => $reflClass] : $methodAnnotation;
		// } else if(is_string($method)) {
			$properties = [];
			foreach (static::getInstances($entity) as $class) {
				$RC = new ReflectionClass($class);
				$methodAnnotation = $reader->getMethodAnnotation($RC->getMethod($method), $annotation);
				if(!empty($methodAnnotation)) {
					if($methodAnnotation instanceOf EntityAnnotationInterface && $entity instanceOf CrudInterface) $methodAnnotation->setEntity($entity, ['method' => $method]);
					return $asInfo ? ['Annotation' => $methodAnnotation, 'ReflectionMethod' => $method, 'ReflectionClass' => $reflClass] : $methodAnnotation;
				}
			}
		// }
		return null;
	}

	/**
	 * Get entity's methods names with annotation
	 * @return array [method_name => Annotation]
	 */
	public static function getMethodsAnnotation($entity, $annotation, $asInfo = false) {
		$reader = new AnnotationReader();
		// if (is_string($annotation)) $annotation = new $annotation();
		if(is_object($annotation)) $annotation = get_class($annotation);
		$methodAnnotations = [];
		foreach (static::getInstances($entity) as $class) {
			$reflClass = new ReflectionClass($class);
			foreach ($reflClass->getMethods() as $method) if(!isset($methodAnnotations[$method->name])) {
				$methodAnnotation = $reader->getMethodAnnotation($method, $annotation);
				if (is_object($methodAnnotation)) {
					if($methodAnnotation instanceOf EntityAnnotationInterface && $entity instanceOf CrudInterface) $methodAnnotation->setEntity($entity, ['method' => $method->name]);
					$methodAnnotations[$method->name] = $asInfo ?
						['Annotation' => $methodAnnotation, 'ReflectionMethod' => $method, 'ReflectionClass' => $reflClass]:
						$methodAnnotation;
				}
			}
		}
		return $methodAnnotations;
	}

	public static function getClassAnnotation($entity, $annotation, $asInfo = false) {
		$reader = new AnnotationReader();
		// if (is_string($annotation)) $annotation = new $annotation();
		if(is_object($annotation)) $annotation = get_class($annotation);
		foreach (static::getInstances($entity) as $class) {
			$reflClass = new ReflectionClass($class);
			$classAnnotation = $reader->getClassAnnotation($reflClass, $annotation);
			if(!empty($classAnnotation)) {
				if($classAnnotation instanceOf EntityAnnotationInterface && $entity instanceOf CrudInterface) $classAnnotation->setEntity($entity);
				return $asInfo ? ['Annotation' => $classAnnotation, 'ReflectionClass' => $reflClass] : $classAnnotation;
			}
		}
		return null;
	}

	public static function getClassAnnotations($entity, $asInfo = false) {
		$reader = new AnnotationReader();
		$classAnnotations = [];
		foreach (static::getInstances($entity) as $class) {
			$reflClass = new ReflectionClass($class);
			$classAnnotations = $reader->getClassAnnotations($reflClass);
			foreach ($classAnnotations as $classAnnotation) if(!isset($classAnnotations[$classAnnotation->name])) {
				if($classAnnotation instanceOf EntityAnnotationInterface && $entity instanceOf CrudInterface) $classAnnotation->setEntity($entity);
				$classAnnotations[$classAnnotation->name] = $asInfo ?
					['Annotation' => $classAnnotation, 'ReflectionClass' => $reflClass]:
					$classAnnotation;
			}
		}
		return $classAnnotations;
	}

	public static function hasClassAnnotation($entity, $annotation) {
		return !empty(static::getClassAnnotation($entity, $annotation));
	}

	public static function getAllAnnotations($entity, $annotations = [], $types = []) {
		$reader = new AnnotationReader();
		if(!is_array($annotations)) $annotations = [$annotations];
		// $annotations = array_map(
		// 	function ($item) { return is_string($item) ? new $item() : $item; },
		// 	$annotations
		// );
		// foreach ($annotations as $key => $annotation) { if(is_string($annotation)) $annotations[$key] = new $annotation(); }
		$types = (array)$types;
		$global_annotations = [];
		// foreach (static::getParentClasses($entity) as $class) {
		foreach (static::getInstances($entity) as $class) {
		// foreach ([$entity] as $class) {
			$reflClass = new ReflectionClass($class);
			if(empty($types) || array_intersect(['property','properties'], $types)) {
				// property
				if(!isset($global_annotations['properties'])) $global_annotations['properties'] = [];
				foreach ($reflClass->getProperties() as $property) {
					$propertyAnnotations = $reader->getPropertyAnnotations($property);
					$propertyAnnotations = array_filter($propertyAnnotations, function($annotation) use ($annotations) {
						if(empty($annotations)) return true;
						foreach ($annotations as $annot) { if($annotation instanceOf $annot) return true; }
						return false;
					});
					foreach ($propertyAnnotations as $annotation) {
						$annot_class = new ReflectionClass($annotation);
						$annot_class = $annot_class->getShortname();
						if(!isset($global_annotations['properties'][$property->name]) || !array_key_exists($annot_class, $global_annotations['properties'][$property->name])) {
							// if(!isset($global_annotations['properties'][$property->name])) $global_annotations['properties'][$property->name] = [];
							if($annotation instanceOf EntityAnnotationInterface && $entity instanceOf CrudInterface) $annotation->setEntity($entity, ['property' => $property->name]);
							$global_annotations['properties'][$property->name][$annot_class] = $annotation;
						}
					}
				}
			}
			if(empty($types) || array_intersect(['method','methods'], $types)) {
				// method
				if(!isset($global_annotations['methods'])) $global_annotations['methods'] = [];
				foreach ($reflClass->getMethods() as $method) {
					$methodAnnotations = $reader->getMethodAnnotations($method);
					$methodAnnotations = array_filter($methodAnnotations, function($annotation) use ($annotations) {
						if(empty($annotations)) return true;
						foreach ($annotations as $annot) { if($annotation instanceOf $annot) return true; }
						return false;
					});
					foreach ($methodAnnotations as $annotation) {
						$annot_class = new ReflectionClass($annotation);
						$annot_class = $annot_class->getShortname();
						if(!isset($global_annotations['methods'][$method->name]) || !array_key_exists($annot_class, $global_annotations['methods'][$method->name])) {
							// if(!isset($global_annotations['methods'][$method->name])) $global_annotations['methods'][$method->name] = [];
							if($annotation instanceOf EntityAnnotationInterface && $entity instanceOf CrudInterface) $annotation->setEntity($entity, ['method' => $method->name]);
							$global_annotations['methods'][$method->name][$annot_class] = $annotation;
						}
					}
				}
			}
			if(empty($types) || in_array('class', $types)) {
				// class
				$classAnnotations = $reader->getClassAnnotations($reflClass);
				$classAnnotations = array_filter($classAnnotations, function($annotation) use ($annotations) {
					if(empty($annotations)) return true;
					foreach ($annotations as $annot) { if($annotation instanceOf $annot) return true; }
					return false;
				});
				foreach ($classAnnotations as $annotation) {
					$annot_class = new ReflectionClass($annotation);
					$annot_class = $annot_class->getShortname();
					if(!isset($global_annotations['class']) || !array_key_exists($annot_class, $global_annotations['class'])) {
						if($annotation instanceOf EntityAnnotationInterface && $entity instanceOf CrudInterface) $annotation->setEntity($entity);
						$global_annotations['class'][$annot_class] = $annotation;
					}
				}
			}
		}
		// if(preg_match('#Eventdate$#', $entity)) {
		// 	echo('<h3>'.$entity.'</h3>');
		// 	echo('<pre>');var_dump($global_annotations['properties']['color']);echo('<pre>');
		// }
		return $global_annotations;
	}

}