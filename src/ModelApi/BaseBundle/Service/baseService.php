<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceModules;

use \ReflectionClass;
use \Exception;

trait baseService {

	protected $serviceModules;

	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString() {
		return serviceClasses::getShortname(static::class);
	}


	public function getServiceModules() {
		if(isset($this->serviceModules) && $this->serviceModules instanceOf serviceModules) return $this->serviceModules;
		$this->serviceModules = isset($this->container) && $this->container instanceOf ContainerInterface ? $this->container->get(serviceModules::class) : null;
		return $this->serviceModules;
	}

	/**
	 * Get service parameter from ParameterBag
	 * @param string $name = null (separate path with . or /)
	 * @param mixed $defaultIfNotFound = null
	 * @param boolean $exceptionIfNoSM = true
	 * @param boolean $exceptionIfErrors = false
	 * @return mixed
	 */
	public function getServiceParameter($name = null, $defaultIfNotFound = null, $exceptionIfNoSM = true, $exceptionIfErrors = false) {
		$serviceModules = $this->getServiceModules();
		if($serviceModules instanceOf serviceModules) {
			return $serviceModules->getServiceParameter($this, $name, $defaultIfNotFound, $exceptionIfErrors);
		} else if($exceptionIfNoSM) {
			throw new Exception("Error ".__METHOD__."(): service Modules not found.", 1);
		}
		return $defaultIfNotFound;
	}

	public function getServiceParameters() {
		return $this->getServiceParameter(null, null);
	}



}