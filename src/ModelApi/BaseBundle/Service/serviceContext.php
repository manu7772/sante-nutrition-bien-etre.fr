<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\Common\Inflector\Inflector;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
// use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceModules;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceAnnonce;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
use ModelApi\BaseBundle\Component\Context;
// InternationalBundle
// use ModelApi\InternationalBundle\Service\serviceLanguage;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
// UserBundle
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Service\serviceEntreprise;
use ModelApi\UserBundle\Service\serviceTier;
use ModelApi\UserBundle\Service\serviceUser;
// use ModelApi\UserBundle\Service\serviceGrants;

use ModelApi\DevprintsBundle\Service\YamlLog;

// use \ReflectionClass;
use \Exception;
use \DateTime;
use \Closure;

class serviceContext implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const CURRENT_CONTEXT_NAME = 'CURRENT';
	const DEFAULT_CONTEXT_NAME = 'DEFAULT';
	const CONTEXT_ENVIRONMENT_TOKEN_NAME = 'X-context-environment';
	const CONTEXT_ENTREPRISE_TOKEN_NAME = 'X-context-entreprise';
	const CONTEXT_LOCALE_TOKEN_NAME = 'locale';
	const CONTEXT_DEFAULTLOCALE_TOKEN_NAME = 'default_locale';
	const CONTEXT_DATE_TOKEN_NAME = 'X-context-date';
	const CONTEXT_RESET_NAME = 'X-context-reset';
	const CONTEXT_FORMAT_DATE = DATE_ATOM;
	const DEFAULT_DATE = "NOW";

	const CONTEXT_IDS = [
		'bundle',
		'environment',
		'entreprise',
		'user',
		'date',
		'language',
		'request',
		// 'session',
	];
	const CONTEXT_ID_MADEBY = [
		'bundle',
		// 'environment',
		// 'entreprise',
		// 'user', // can not use USER because not already loaded!!!
		// 'date',
		// 'language',
		// 'request',
		// 'session',
	];

	protected $container;
	protected $serviceEntities;
	protected $parameterBag;
	protected $current_context;
	protected $default_context;
	protected $contexts;
	protected $actionsWhenInitialized;
	// protected $_initialized;

	public function __construct(ContainerInterface $container) {
		// $this->_initialized = false;
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->parameterBag = $this->container->getParameterBag();
		// Actions when initialized
		$this->actionsWhenInitialized = [];
		// Contexts
		$this->contexts = new ArrayCollection();
		$this->default_context = new Context($this->container, false);
		$this->default_context->setName(static::DEFAULT_CONTEXT_NAME);
		$this->defineCurrentContext($this->default_context);
		return $this;
	}


	/*************************************************************************************/
	/*** PARAMETERS
	/*************************************************************************************/

	// /**
	//  * Get service parameter from ParameterBag
	//  * @param string $name (separate path with . or /)
	//  * @param boolean $defaultIfNotFound = null
	//  * @return mixed
	//  */
	// public function getServiceParameter($name, $defaultIfNotFound = null) {
	// 	$names = preg_split('#[\\.\\/]#', $name);
	// 	$serviceName = $this->__toString();
	// 	if(!$this->parameterBag->has('services')) return $defaultIfNotFound;
	// 	$param = $this->parameterBag->get('services');
	// 	if(isset($param[$serviceName])) $param = $param[$serviceName];
	// 		else return $defaultIfNotFound;
	// 	$idx = 0;
	// 	foreach ($names as $name) {
	// 		if(!empty($name)) {
	// 			if(preg_match('#'.$serviceName.'#i', $name) && $idx < 1) continue;
	// 			if(!isset($param[$name])) return $defaultIfNotFound;
	// 			$param = $param[$name];
	// 			$idx++;
	// 		}
	// 	}
	// 	return $param;
	// }

	public function getDefaultEnvironment() {
		$default = $this->getServiceParameter('defaults/Environment', 'Entreprise');
		$default = $this->serviceEntities->getClassnameByAnything($default);
		switch ($default) {
			case User::class:
				$environment = $this->getDefaultUser();
				// if(empty($environment)) $environment = $this->getDefaultEntreprise();
				break;
			case Entreprise::class:
				$environment = $this->getDefaultEntreprise();
				break;
			default:
				$environment = $this->getDefaultEntreprise();
				break;
		}
		// echo('<div>Default Environment is '.$default.'. Found '.json_encode(empty($environment) ? null : get_class($environment).' '.$environment->__toString()).'</div>');
		return $environment;
	}

	public function getDefaultUser() {
		return $this->container->get(serviceUser::class)->getCurrentUserOrNull();
	}

	public function getDefaultEntreprise() {
		return $this->container->get(serviceEntreprise::class)->getDefaultEntreprise();
	}


	/*************************************************************************************/
	/*** PREINITIALIZE
	/*************************************************************************************/

	/**
	 * Is initialized
	 * @return boolean
	 */
	public function isInitialized() {
		return $this->getCurrentContext()->isComplete();
		// $current_context = $this->getCurrentContext();
		// if(empty($current_context) || !$this->getCurrentContext()->isComplete()) return false;
		// return true;
		// return $this->_initialized;
	}

	/**
	 * To do when service is initialized (or execute immediatly if still initialized)
	 * @param string|object $name
	 * @param Closure $action
	 * @return string $name (usefull if $name is object)
	 */
	public function whenInitialized($name, Closure $action) {
		$name = !is_string($name) ? serviceTools::geUniquid($name) : $name;
		$this->actionsWhenInitialized[$name] = [
			'action' => $action,
			'done' => false,
		];
		$this->doActionsWhenInitialized();
		return $name;
	}

	/**
	 * Do actions when initialized
	 * @param boolean $remove = false
	 * @param boolean $force = false
	 * @return serviceContext
	 */
	public function doActionsWhenInitialized($remove = false, $force = false) {
		if($this->isInitialized()) {
			foreach ($this->actionsWhenInitialized as $name => $action) {
				if(!$action['done'] || $force) $action['action']($this);
				$this->actionsWhenInitialized[$name]['done'] = true;
			}
			if($remove) $this->removeDoneActionsWhenInitialized();
		}
		return $this;
	}

	/**
	 * Remove Action
	 * @param string $name
	 * @return serviceContext
	 */
	public function removeActionWhenInitialized($name) {
		$this->actionsWhenInitialized = array_filter($this->actionsWhenInitialized, function($action_name) use ($name) {
			return $action_name !== $name;
		}, ARRAY_FILTER_USE_KEY);
		return $this;
	}

	/**
	 * Remove Action that are already done
	 * @return serviceContext
	 */
	protected function removeDoneActionsWhenInitialized() {
		$this->actionsWhenInitialized = array_filter($this->actionsWhenInitialized, function($action) {
			return !$action['done'];
		});
		return $this;
	}


	/*************************************************************************************/
	/*** OTHER SERVICES INFORMATIONS
	/*************************************************************************************/

	public function isOwnerEntreprises(User $user = null) {
		return $this->container->get(serviceEntreprise::class)->isOwnerEntreprises($user);
	}

	public function isAnnonce() {
		return $this->container->get(serviceAnnonce::class)->isAnnonce();
	}



	/*************************************************************************************/
	/*** MODULES
	/*************************************************************************************/

	public function getServiceModules() {
		return $this->container->get(serviceModules::class);
	}
	public function getModules() {
		return $this->getServiceModules();
	}



	/*************************************************************************************/
	/*** CONTEXTS
	/*************************************************************************************/

	/**
	 * Is default Context
	 * IMPORTANT: because this is necessary in normal conditions!
	 * @return boolean
	 */
	public function isDefaultContext() {
		return $this->getCurrentContext() === $this->getDefaultContext();
	}

	/**
	 * Restore default Context
	 * @return boolean (true if success)
	 */
	public function restoreDefaultContext() {
		$this->setCurrentContext($this->getDefaultContext());
		return $this->isDefaultContext();
	}

	/**
	 * Get default Context
	 * @return Context
	 */
	public function getDefaultContext() {
		return $this->default_context;
	}

	/**
	 * Get current Context
	 * @return Context
	 */
	public function getCurrentContext() {
		return $this->current_context;
	}

	/**
	 * Define current Context
	 * @param mixed $context_or_name (name or Context)
	 * @return Context (new current Context)
	 */
	public function defineCurrentContext($context_or_name) {
		if(is_string($context_or_name)) {
			if($this->contextExists($context_or_name)) $this->current_context = $this->contexts->get($context_or_name);
				else throw new Exception("Error ".__METHOD__."(): context named ".json_encode($context_or_name)." does not exist!", 1);
		} else if($context_or_name instanceOf Context) {
			$this->addContext($context_or_name);
			$this->current_context = $context_or_name;
		} else {
			$typ = is_object($context_or_name) ? get_class($context_or_name) : gettype($context_or_name);
			throw new Exception("Error ".__METHOD__."(): parameter must be (string) the name of Context or object Context. Got ".json_encode($typ)."!", 1);
		}
		return $this->current_context;
	}

	/**
	 * Add Context
	 * @param Context $context
	 * @return serviceContext
	 */
	public function addContext(Context $context) {
		$this->contexts->set($context->getName(), $context);
		$context->lockName();
		$this->checkContexts();
		return $this;
	}

	/**
	 * Remove Context
	 * @param mixed $context_or_name (name or Context)
	 * @return serviceContext
	 */
	public function removeContext($context_or_name) {
		if(is_string($context_or_name)) $context_or_name = $this->contexts->get($context_or_name);
		if($this->contexts->contains($context)) {
			$this->contexts->removeElement($context);
			$context->unlockName();
		}
		return $this;
	}

	/**
	 * Context exists
	 * @param mixed $context_or_name (name or Context)
	 * @return boolean
	 */
	public function contextExists($context_or_name) {
		return is_string($context_or_name) ? $this->contexts->containsKey($context_or_name) : $this->contexts->contains($context_or_name);
	}

	/**
	 * Get Contexts names
	 * @return array <string>
	 */
	public function getContextNames() {
		return $this->contexts->getKeys();
	}

	/**
	 * Get Contexts (optional: filtered)
	 * @param Closure $filter = null
	 * @return ArrayCollection <ModelApi\BaseBundle\Component\Context>
	 */
	public function getContexts(Closure $filter = null) {
		return $filter instanceOf Closure ? $this->contexts->filter($filter($context)) : $this->contexts;
	}

	/**
	 * Check all Contexts
	 * 	- remove same Contexts
	 * 	- lock name for all Contexts
	 * @param Closure $filter = null
	 * @return ArrayCollection <ModelApi\BaseBundle\Component\Context>
	 */
	protected function checkContexts() {
		$contexts = new ArrayCollection();
		foreach ($this->contexts as $name => $context) {
			$context->lockName();
			if(!$contexts->contains($context)) $contexts->set($name, $context);
		}
		$this->contexts = $contexts;
		return $this;
	}

	/**
	 * Call current Context method
	 * @param string $method
	 * @param array of arguments $arguments
	 * @return mixed
	 */
	public function __call($method, $arguments) {
		$original = $method;
		$this->getMethod($method);
		// try methods
		if(!empty($method)) {
			// if(!$this->getCurrentContext()->isComplete()) throw new Exception("Error ".__METHOD__."(): current context is not complete!", 1);
			return call_user_func_array([$this->getCurrentContext(), $method], $arguments);
		}
		// try constants
		if(defined('static::'.$original)) return constant('static::'.$original);
		throw new Exception("Error ".__METHOD__."(): this method or constant ".json_encode($original)." does not exist!", 1);
	}

	public function getMethod(&$method) {
		if(method_exists($this->getCurrentContext(), $method)) return $method;
		foreach (['get','is','has'] as $base) {
			$test = Inflector::camelize($base.'_'.$method);
			if(method_exists($this->getCurrentContext(), $test)) return $method = $test;
			// $test = Inflector::tableize($base.'_'.$method);
			// if(method_exists($this->getCurrentContext(), $test)) return $method = $test;
		}
		// if($this->current_context->version_ge("2.0.0")) return $method = null;
		// for old versions... < 2.0.0
		$alt = preg_replace('/(get|has|is)Context/i', '$1', $method);
		if(method_exists($this->getCurrentContext(), $alt)) return $method = $alt;
		// $method = preg_replace('#^context#i', '', $method);
		// if(method_exists($this->getCurrentContext(), $method)) return $method;
		foreach (['get','is','has'] as $base) {
			$test = Inflector::camelize($base.'_'.$method);
			if(method_exists($this->getCurrentContext(), $test)) return $method = $test;
			$test = Inflector::camelize($base.'_'.$alt);
			if(method_exists($this->getCurrentContext(), $test)) return $method = $test;
			// $test = Inflector::tableize($base.'_'.$method);
			// if(method_exists($this->getCurrentContext(), $test)) return $method = $test;
		}
		return $method = null;
	}








	public function init_by_request(Request $request) {
		if(!$this->isInitialized()) {
			$session = $request->getSession();
			$checkRequest = true;

			$reset = $this->getDataInRequest($request, static::CONTEXT_RESET_NAME, false, ['post','get']);

			if($reset && $session instanceOf Session) {
				if($reset = 'all') {
					$session->clear();
					$checkRequest = false;
				} else if($reset='context') {
					foreach ($session->all() as $id => $data) {
						if(preg_match('#^ctx_#', $id)) $sesion->remove($id);
					}
					$checkRequest = false;
				}
			}

			// Complete context
			$this->current_context->completeContext();

			if($this->current_context->isRegister()) {

				if($checkRequest) {
					$contextIdentifier = $this->current_context->getId();
					// Check Context in session
					if($session instanceOf Session) {
						// Retrieve session context if exists
						if($session->has($contextIdentifier)) {
							$contextData = $session->get($contextIdentifier);
							$this->current_context->putSessionData($contextData);
						}
					}

					// CHECK REQUEST
					$from_request_data = $this->current_context->getDataForSession(false);
					$test = json_encode($from_request_data);

					// Locale in request
					$request_locale = $this->getDataInRequest($request, static::CONTEXT_LOCALE_TOKEN_NAME);
					if(!empty($request_locale)) $this->current_context->setlocale($request_locale);

					// Environment in request
					$request_environment_bddid = $this->getDataInRequest($request, static::CONTEXT_ENVIRONMENT_TOKEN_NAME);
					if(!empty($request_environment_bddid)) $from_request_data[static::CONTEXT_ENVIRONMENT_TOKEN_NAME] = $request_environment_bddid;

					// Environment in request
					$request_entreprise_bddid = $this->getDataInRequest($request, static::CONTEXT_ENTREPRISE_TOKEN_NAME);
					if(!empty($request_entreprise_bddid)) $from_request_data[static::CONTEXT_ENTREPRISE_TOKEN_NAME] = $request_entreprise_bddid;

					// Date in request
					$request_date = $this->getDataInRequest($request, static::CONTEXT_DATE_TOKEN_NAME);
					if(!empty($request_date)) $from_request_data[static::CONTEXT_DATE_TOKEN_NAME] = $request_date;

					if($test != json_encode($from_request_data)) {
						$this->current_context->putSessionData($from_request_data);
					}
				}

				// Complete context
				// $this->current_context->completeContext();
				// if(!$this->current_context->isComplete() && $this->container->get(serviceKernel::class)->isDev()) {
				// 	echo('<pre><h3>DEV interruption while Context is not complete!</h3>');
				// 	var_dump($this->current_context->getAllContextNamedElements(true));
				// 	var_dump($this->current_context->getErrors(false));
				// 	die('</pre>');
				// }

				if($session instanceOf Session) {
					// // VERSION < 2.0.0
					// // if($this->current_context->version_lt("2.0.0")) {
					// $session->set(static::CONTEXT_LOCALE_TOKEN_NAME, $this->current_context->getLocale());
					// $session->set(static::CONTEXT_DEFAULTLOCALE_TOKEN_NAME, $this->current_context->getDefaultLocale());
					// // }

					// // SAVE IN SESSION
					// $session->set($this->current_context->getId(), $this->current_context->getDataForSession(false));

					$this->whenInitialized('put_in_session', function($serviceContext) use ($session) {
						$session->set(static::CONTEXT_LOCALE_TOKEN_NAME, $this->current_context->getLocale());
						$session->set(static::CONTEXT_DEFAULTLOCALE_TOKEN_NAME, $this->current_context->getDefaultLocale());
						$session->set($this->current_context->getId(), $this->current_context->getDataForSession(false));
					});

					// if($this->current_context->version_lt("2.0.0")) {
					// 	// CONTEXTUAL ENVIRONMENT
					// 	$context_environment = $this->current_context->getEnvironment();
					// 	$session->set(static::CONTEXT_ENVIRONMENT_TOKEN_NAME, null === $context_environment ? null : $context_environment->getBddid());
					// 	// CONTEXTUAL ENTREPRISE
					// 	$context_entreprise = $this->current_context->getEntreprise();
					// 	$session->set(static::CONTEXT_ENTREPRISE_TOKEN_NAME, null === $context_entreprise ? null : $context_entreprise->getBddid());
					// 	// CONTEXTUAL DATE
					// 	$session->set(static::CONTEXT_DATE_TOKEN_NAME, $this->current_context->getDate(false));
					// }
				}
			} else {
				// Complete anyway...
				// $this->current_context->completeContext();
			}
			// $this->_initialized = $this->current_context->isComplete();
			// $this->_initialized = true;
		} else {
			throw new Exception("Service CONTEXT still initialized!", 1);
		}

		// if($this->current_context->hasErrors()) {
		// 	echo('<pre>');
		// 	var_dump($this->current_context->getErrors(false));
		// 	echo('</pre>');
		// }

		$this->current_context->completeContext();
		// $this->doActionsWhenInitialized();
		return $this->getCurrentContext();
	}

	public function getDataInRequest($request, $name, $default = null, $from = []) {
		if(empty($from)) $from = ['header','post','get'];
		// 1. Header
		$data = $request->headers->get($name);
		if(!empty($data) && in_array('header', $from)) return $data;
		// 2. Request (POST)
		$data = $request->request->get($name);
		if(!empty($data) && in_array('post', $from)) return $data;
		// 3. Query (GET)
		$data = $request->query->get($name);
		if(!empty($data) && in_array('get', $from)) return $data;
		// 4. Session
		// $session = $request->getSession();
		// if($session instanceOf Session) {
		// 	$contextIdentifier = $this->getCurrentContext()->getId();
		// 	if($session->has($contextIdentifier)) {
		// 		//
		// 	}
		// }
		// Default
		return $default;
	}

	public static function modifyDate(DateTime $date, $modify) {
		$modifys = explode('|', $modify);
		foreach ($modifys as $modif) $date->modify($modif);
		return $date;
	}

	/**
	 * Get current date
	 * @return string
	 */
	public static function getCurrentDate() {
		return new DateTime(static::DEFAULT_DATE);
	}

	/**
	 * Get current year
	 * @return string
	 */
	public static function getCurrentYear() {
		$date = static::getCurrentDate();
		return $date->format("Y");
	}

}