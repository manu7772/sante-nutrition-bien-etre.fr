<?php
namespace ModelApi\BaseBundle\Service;

// use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
// https://symfony.com/doc/3.4/components/filesystem.html
use Doctrine\Common\Inflector\Inflector;
use Behat\Transliterator\Transliterator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// DevprintsBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

// use \SplFileInfo;
use \ReflectionClass;
use \ReflectionProperty;
use \ReflectionMethod;
use \Exception;
use \DateTime;

class serviceTools implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const REGEX_INDICATOR = 'REGEX::';

//	const PRIVATE_RIGHTS = ['chmod' => 0777, 'group' => 'www-data', 'owner' => 'nginx'];
//	const PUBLIC_RIGHTS = ['chmod' => 0777, 'group' => 'www-data', 'owner' => 'nginx'];


	public static function getMicrotimeid() {
		return preg_replace('/^\d\.(\d+)\s(\d+)$/', "$2$1", microtime());
	}

	public static function geUniquid($prefix = "") {
		if(is_object($prefix)) $prefix = spl_object_hash($prefix).'_'.static::getMicrotimeid().'@';
		if(!is_string($prefix)) $prefix = md5(json_encode($prefix)).'_'.static::getMicrotimeid().'@';
		return uniqid($prefix, true);
	}


	public static function generateSlug($prefix = "") {
		return preg_replace('/[_\\W]+/', '-', static::geUniquid($prefix));
	}

	/**
	 * Get data from Json
	 * @param mixed &$string
	 * @param boolean $asArray = true
	 * @param boolean $returnOriginalIfnot = true
	 * @param boolean $recursive = true
	 */
	public static function fromJson(&$string, $asArray = true, $recursive = true) {
		if(!$recursive && !is_string($string)) return;
		switch (gettype($string)) {
			case 'array':
				foreach ($string as $key => $value) {
					static::fromJson($value, $asArray, $recursive);
					$string[$key] = $value;
				}
				break;
			case 'string':
				$data = json_decode($string, $asArray);
				if(json_last_error() === JSON_ERROR_NONE) {
					$string = $data;
					if(!is_string($string)) static::fromJson($string, $asArray, $recursive);
				}
				break;
			default:
				// do nothing
				break;
		}
		return;
	}

	/**
	 * Is Json string?
	 * @param mixed $string
	 * @param boolean $asArray = true
	 * @return mixed
	 */
	public static function isJson($string, $asArray = true) : mixed {
		if(!is_string($string)) return false;
		$data = json_decode($string, $asArray);
		return json_last_error() === JSON_ERROR_NONE;
	}

	/**
	 * @see http://preg_replace.onlinephpfunctions.com/
	 */
	public static function magnifyText($text, $locale) {
		if (!is_string($text)) return null;
		$text = trim($text);
		switch ($locale) {
			case 'fr-FR':
				$text = preg_replace(['/\\s{2,}/', '/([\\w\\d])([:;\\!\\?])/', '/(\\s+)([\\.,])/'], [' ', '$1&nbsp;$2', '$2'], $text);
				$text = preg_replace('/(\\s+)([\\w\\d-]{1,2})\\s+/', ' $2&nbsp;', $text);
				break;
		}
		return $text;
	}

	public static function magnifyTextPlus(&$text, $includeShortWords = 2, $includeParenthesis = true) {
		// $text = str_replace("\n", "<br />", $text);
		// $text = '(  bonjour aa    }   ] ! ?';
		$istest = false;
		$test1 = $istest ? '' : '<span style="color:red;">|</span>';
		$test2 = $istest ? '' : '<span style="color:orange;">|</span>';
		$test3 = $istest ? '' : '<span style="color:blue;">|</span>';
		$test4 = $istest ? '' : '<span style="color:violet;">|</span>';
		// $test = '';
		$text = nl2br($text);
		// espaces multiples (bleu)
		$text = preg_replace('#\\s{2,}#', ' '.$test3, $text);
		// mots courts (orange)
		if(is_integer($includeShortWords)) {
			$text = preg_replace('#(\\s+)([\\wàâäéèêëîïôöûüù]{1,'.$includeShortWords.'})(\\s+)#i', ' $2'.$test2.'&nbsp;', $text);
			$text = preg_replace('#(&nbsp;)([\\wàâäéèêëîïôöûüù]{1,'.$includeShortWords.'})(\\s+)#i', '&nbsp;$2'.$test2.'&nbsp;', $text);
		}
		// parenthèses (violet)
		if($includeParenthesis) {
			$text = preg_replace('#([\\(|\\[|\\{])(\\s+|&nbsp;)#', '$1'.$test4, $text);
			$text = preg_replace('#(\\s+|&nbsp;)([\\)|\\]|\\}])#', $test4.'$2', $text);
		}
		// ponctuations (rouge)
		$text = preg_replace('#(\\s)+([!¡?¿:;])#', $test1.'&nbsp;$2', $text);
		return $text;
	}


	/**
	 * Compute text and return transform as null if empty
	 * @param string &$text
	 * @param boolean $striptags = true
	 * @return boolean
	 */
	public static function computeTextOrNull(&$text, $striptags = true) {
		if (!is_string($text)) {
			$text = null;
		} else {
			if ($striptags) static::striptags($text);
				else $text = trim($text);
			if (empty($text)) $text = null;
		}
		return is_string($text);
	}

	public static function getTextOrNullStripped($text) {
		return static::isStringOverTags($text) ? $text : null;
	}

	public static function getTextOrNull($text, $striptags_ifnullwithout = true) {
		if (!is_string($text)) return null;
		$text = trim($text);
		if($striptags_ifnullwithout && !static::isStringOverTags($text)) $text = null;
		return $text;
	}

	public static function getTextOrDefault($text, $default = null, $striptags_ifnullwithout = true) {
		if (!is_string($text)) return $default;
		$text = trim($text);
		if($striptags_ifnullwithout && !static::isStringOverTags($text)) $text = $default;
		return $text;
	}

	public static function isStringOverTags($text) {
		if (!is_string($text)) return false;
		return !empty(static::striptags($text));
	}

	public static function striptags(&$text) {
		if (!is_string($text)) return $text = '';
		$text = preg_replace('/(&nbsp;|<br>)/', ' ', nl2br($text));
		# $text = preg_replace('/(&nbsp;)/', '', $text);
		$text = strip_tags($text);
		$text = trim($text);
		return $text;
	}

	public static function br_striptags(&$text) {
		if (!is_string($text)) return $text = '';
		$text = preg_replace('/(&nbsp;)/', ' ', nl2br($text));
		# $text = preg_replace('/(&nbsp;)/', '', $text);
		$text = strip_tags($text, '<br>');
		$text = trim($text);
		return $text;
	}

	/**
	 * Count number of words in $text
	 * @param string $text
	 * @param integer $nbletters = 2
	 * @return integer
	 */
	public function wordscount($text, $nbletters = 2) {
		if(is_object($text)) $text = $text->__toString();
		$text = (string)$text;
		if(!is_string($text) || empty($text)) return 0;
		static::striptags($text);
		$count = preg_split('/([\\W])+/', $text, -1);
		// $count = preg_match('/[\\w]{2,}/', $text, -1);
		$count = array_filter($count, function ($item) use ($nbletters) { return is_string($item) && strlen($item) >= $nbletters; });
		// var_dump($count);
		return count($count);
	}

	/********************************************************************************************************************/
	/*** Array
	/********************************************************************************************************************/

	public static function arrayHasKeys(Array $array, $keys, $strict = false) {
		if(is_string($keys)) $keys = [$keys];
		$count = count($keys);
		$founds = array_intersect(array_keys($array), $keys);
		return $strict ? count($founds) === $count : count($founds) >= $count;
	}


	/********************************************************************************************************************/
	/*** Colors
	/********************************************************************************************************************/

	public static function getContrastColor($hexColor, $resultLight = '#FFFFFF', $resultDark = '#000000') {
		// https://stackoverflow.com/questions/1331591/given-a-background-color-black-or-white-text
		// hexColor RGB
		$R1 = hexdec(substr($hexColor, 1, 2));
		$G1 = hexdec(substr($hexColor, 3, 2));
		$B1 = hexdec(substr($hexColor, 5, 2));
		// Black RGB
		$blackColor = "#000000";
		$R2BlackColor = hexdec(substr($blackColor, 1, 2));
		$G2BlackColor = hexdec(substr($blackColor, 3, 2));
		$B2BlackColor = hexdec(substr($blackColor, 5, 2));
		 // Calc contrast ratio
		 $L1 = 0.2126 * pow($R1 / 255, 2.2) +
			   0.7152 * pow($G1 / 255, 2.2) +
			   0.0722 * pow($B1 / 255, 2.2);
		$L2 = 0.2126 * pow($R2BlackColor / 255, 2.2) +
			  0.7152 * pow($G2BlackColor / 255, 2.2) +
			  0.0722 * pow($B2BlackColor / 255, 2.2);
		$contrastRatio = 0;
		if ($L1 > $L2) {
			$contrastRatio = (int)(($L1 + 0.05) / ($L2 + 0.05));
		} else {
			$contrastRatio = (int)(($L2 + 0.05) / ($L1 + 0.05));
		}
		// If contrast is more than 5, return black color
		if ($contrastRatio > 5) {
			return $resultDark;
		} else { 
			// if not, return white color.
			return $resultLight;
		}
	}

	public static function getColorInverse($hexColor) {
		// https://www.jonasjohn.de/snippets/php/color-inverse.htm
		// 
		// Voir aussi : Lighten or darken a given colour
		// https://gist.github.com/stephenharris/5532899
		$hexColor = str_replace('#', '', $hexColor);
		if (strlen($hexColor) != 6){ return '000000'; }
		$rgb = '';
		for ($x=0;$x<3;$x++){
			$c = 255 - hexdec(substr($hexColor,(2*$x),2));
			$c = ($c < 0) ? 0 : dechex($c);
			$rgb .= (strlen($c) < 2) ? '0'.$c : $c;
		}
		return '#'.$rgb;
	}



	/********************************************************************************************************************/
	/*** Dates
	/********************************************************************************************************************/

	// /**
	//  * Get current year
	//  * @return string
	//  */
	// public static function getCurrentYear() {
	// 	$date = new DateTime(serviceContext::DEFAULT_DATE);
	// 	return $date->format("Y");
	// }

	public static function getPreviousDayOfMonth($date = null, $day = "monday") {
		// if($date instanceOf DateTime) $date = clone $date; // prevent modifiying original DateTime object
		if(is_string($date)) $date = new DateTime($date);
		if(!($date instanceOf DateTime)) $date = new DateTime($date);
		// set first day of month
		$date = new DateTime($date->format('Y-m').'-01 00:00:00');
		// $date->modify('first day of month');
		$date->modify($day.' this week');
		return $date;
	}

	public static function getNamesOfDay($size = null) {
		// $names = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
		$names = [];
		$date = new DateTime('monday this week');
		foreach ([1,2,3,4,5,6,7] as $key => $value) {
			$names[] = utf8_encode(strftime("%A", $date->getTimestamp()));
			$date->modify('+1 day');
		}
		foreach ($names as $key => $name) {
			if(is_integer($size)) $names[$key] = $name = substr($name, 0, $size);
			$names[$key] = strtolower($name);
		}
		return $names;
	}

	public static function getNameOfDay($number = 1) {
		$numbers = static::getNamesOfDay();
		return $days[$number - 1];
	}

	public static function pickDate(DateTime $toModify, DateTime $takeIn) {
		$toModify->setDate(intval($takeIn->format('Y')), intval($takeIn->format('m')), intval($takeIn->format('d')));
		// return $toModify;
	}

	public static function pickTime(DateTime $toModify, DateTime $takeIn) {
		$toModify->setTime(intval($takeIn->format('H')), intval($takeIn->format('i')), intval($takeIn->format('s')));
		// return $toModify;
	}




	/********************************************************************************************************************/
	/*** ENCODE UTF-8 / prevents "Malformed UTF-8 characters, possibly incorrectly encoded"
	/********************************************************************************************************************/

	public static function utf8ize(&$meta_data) {
		if (is_array($meta_data)) {
			foreach ($meta_data as $key => $meta) $meta_data[$key] = static::utf8ize($meta);
		} else if (is_string($meta_data)) {
			// return mb_convert_encoding($meta_data, 'UTF-8', 'UTF-8');
			return trim(utf8_encode($meta_data));
		}
		return $meta_data;
	}


	/********************************************************************************************************************/
	/*** BASEDIRECTORY TOOLS
	/********************************************************************************************************************/

	public static function splitdirspath(&$path) {
		return $path = preg_split('#[\\/\\|\\.\\\\]+#', $path);
	}

	/********************************************************************************************************************/
	/*** FILE TOOLS
	/********************************************************************************************************************/

	public static function removeSeparator(&$string, $replacement = '_') {
		// return $string = preg_replace('/\\'.DIRECTORY_SEPARATOR.'/', $replacement, $string);
		return $string = preg_replace('/(\\/|\\\\)+/', $replacement, $string);
	}

	public static function magnifyPath($path, $endSlash = false, $verify = false) {
		$path = preg_replace('#[\\/\\\]+#', DIRECTORY_SEPARATOR, $path);
		if($verify && !@file_exists($path)) {
			// throw new Exception("Error ".__METHOD__."(): path ".json_encode($path)." does not exist!", 1);
			return false;
		}
		// if(!$endSlash) $path = preg_replace('/[\\/\\\]+$/', '', $path);
		return preg_replace('#[\\/\\\]+#', DIRECTORY_SEPARATOR, $path.(false === $endSlash ? '' : DIRECTORY_SEPARATOR));
	}

	public static function getPathFiles($path, $search = '*') {
		$path = static::magnifyPath($path, true);
		// var_dump($path.$search);
		$files = array_filter(glob($path.$search), function ($item) {
			$info = pathinfo($item);
			return !preg_match('#^\\.#', $info['basename']);
		});
		// var_dump(glob($path.$search));
		return $files;
	}

	public static function splitPath($path, $splitExtension = true) {
		$preg = $splitExtension ? '/[\\.\\/\\\]+/' : '/[\\/\\\]+/';
		return preg_split($preg, $path, '-1', PREG_SPLIT_NO_EMPTY);
	}

	public static function preSeparator($path, $endSlash = false) {
		if(is_string($path) && strlen($path) > 0) $path = preg_match('/^\\'.DIRECTORY_SEPARATOR.'/', $path) ? $path : DIRECTORY_SEPARATOR.$path;
		return empty($path) ? null : static::magnifyPath($path, $endSlash);
	}

	public static function removeFileEverywhere($path, $filename) {
		$fileSystem = new Filesystem();
		$removed = false;
		foreach (static::getPathFiles($path) as $item) if (!@is_link($item)) {
			if (@is_dir($item)) {
				$removed = static::removeFileEverywhere(static::concatWithSeparators([$path, $item]), $filename);
			} else if (@is_file($item)) {
				if ($item === $filename) $fileSystem->remove($item);
			}
		}
		unset($fileSystem);
		return $removed;
	}

	// public static function Utf8_ansi(&$path) {
	// 	// return html_entity_decode(preg_replace('/\\d\\\\u[\\dA-Fa-f]{4}/i', "&#x\\1;", $path), ENT_NOQUOTES, 'UTF-8');

	// 	# $path = preg_replace('/\\d\\\\u([\da-fA-F]{4})/', '&#x\1;', $path);
	// 	// $utf8_ansi2 = array("\u00c0" => "À", "\u00c1" => "Á", "\u00c2" => "Â", "\u00c3" => "Ã", "\u00c4" => "Ä", "\u00c5" => "Å", "\u00c6" => "Æ", "\u00c7" => "Ç", "\u00c8" => "È", "\u00c9" => "É", "\u00ca" => "Ê", "\u00cb" => "Ë", "\u00cc" => "Ì", "\u00cd" => "Í", "\u00ce" => "Î", "\u00cf" => "Ï", "\u00d1" => "Ñ", "\u00d2" => "Ò", "\u00d3" => "Ó", "\u00d4" => "Ô", "\u00d5" => "Õ", "\u00d6" => "Ö", "\u00d8" => "Ø", "\u00d9" => "Ù", "\u00da" => "Ú", "\u00db" => "Û", "\u00dc" => "Ü", "\u00dd" => "Ý", "\u00df" => "ß", "\u00e0" => "à", "\u00e1" => "á", "\u00e2" => "â", "\u00e3" => "ã", "\u00e4" => "ä", "\u00e5" => "å", "\u00e6" => "æ", "\u00e7" => "ç", "\u00e8" => "è", "\u00e9" => "é", "\u00ea" => "ê", "\u00eb" => "ë", "\u00ec" => "ì", "\u00ed" => "í", "\u00ee" => "î", "\u00ef" => "ï", "\u00f0" => "ð", "\u00f1" => "ñ", "\u00f2" => "ò", "\u00f3" => "ó", "\u00f4" => "ô", "\u00f5" => "õ", "\u00f6" => "ö", "\u00f8" => "ø", "\u00f9" => "ù", "\u00fa" => "ú", "\u00fb" => "û", "\u00fc" => "ü", "\u00fd" => "ý", "\u00ff" => "ÿ");
	// 	// return strtr($path, $utf8_ansi2);
	// 	// $path = strtr(json_encode($path), $utf8_ansi2);
	// 	// return json_decode($path);
	// 	# $path = json_decode(preg_replace(['/\\w(\\\\u03[\\dA-Fa-f]{2})/i'], ['e&#x0301;'], json_encode($path)));
	// 	// return $path = json_decode(preg_replace(['/\\w(\\\\u03[\\dA-Fa-f]{2})/i'], ['é'], json_encode($path)));
	// 	// $path = json_decode(preg_replace(['/\\d\\\\u[\\dA-Fa-f]{4}/i'], ['e&#x0301;'], json_encode($path)));
	// 	// $path = htmlentities($path, ENT_NOQUOTES, 'UTF-8');
	// 	$path = json_decode(html_entity_decode(json_encode($path), ENT_NOQUOTES, 'UTF-8'));
	// }

	public static function getUploadFileInstance($uploadFile = null, $filename = null) {
		if (is_string($uploadFile)) {
			$file = new File($uploadFile);
			$filename ??= $file->getFilename();
			$uploadFile = new UploadedFile($uploadFile, $filename);
		}
		return $uploadFile instanceOf UploadedFile ? $uploadFile : null;
	}

	public static function getUploadFileContentType($uploadFile) {
		$uploadFile = static::getUploadFileInstance($uploadFile);
		return $uploadFile instanceOf UploadedFile ? $uploadFile->getMimeType() : null;
	}

	public static function getSafeFilename($filename) {
		return transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $filename);
	}

	public static function concatWithSeparators($names, $endSlash = false) {
		$names = implode(DIRECTORY_SEPARATOR, $names);
		return static::magnifyPath($names, $endSlash);
	}

	public static function createPath($path) {
		if (!@file_exists($path)) {
			$fileSystem = new Filesystem();
			// $old = umask(0);
			// try {
			$fileSystem->mkdir($path);
			// $fileSystem->chmod($path, static::PRIVATE_RIGHTS['chmod'], 0000);
			// } catch (IOExceptionInterface $exception) {
			// $result = false;
			// $message = "     -- An error occurred while creating your directory path: ".json_encode($path);
			// DevTerminal::Error($message);
			// }
			// $fileSystem->chgrp($path, static::PRIVATE_RIGHTS['group'], true);
			unset($fileSystem);
			// umask($old);
		}
	}

	public static function createFile($path, $file) {
		if (!@file_exists($path)) static::createPath($path);
		if (!@file_exists($path.$file)) {
			$fileSystem = new Filesystem();
			$fileSystem->dumpFile($path.$file, null);
			// $fileSystem->chmod($path.$file, static::PRIVATE_RIGHTS['chmod'], 0000);
			// $fileSystem->chgrp($path.$file, static::PRIVATE_RIGHTS['group'], true);
			unset($fileSystem);
		}
	}

	public static function removeFile($path, $file) {
		// if (@file_exists($path.$file)) {
			$fileSystem = new Filesystem();
			$fileSystem->remove($path.$file);
			unset($fileSystem);
		// }
	}

	public static function appendToFile($path, $file, $data) {
		// static::createFile($path, $file);
		$fileSystem = new Filesystem();
		$fileSystem->appendToFile($path.$file, $data);
		// $fileSystem->chmod($path.$file, static::PRIVATE_RIGHTS['chmod'], 0000);
		// $fileSystem->chgrp($path.$file, static::PRIVATE_RIGHTS['group'], true);
		unset($fileSystem);
	}

	public static function getTreeAsArray($path, $id_prefix = null) {
		$id = 0;
		$tree_id = 0;
		$id_prefix .= null === $id_prefix ? '' : '_';
		$tree = [];
		$items = static::getPathFiles($path);
		foreach ($items as $item) {
			$new_id = $id_prefix.$id++;
			$exp = explode('_', $new_id);
			$opended = count($exp) < 3;
			$pathinfo = pathinfo($item);
			$name = $pathinfo['basename'];
			$tree[$tree_id]['id'] = $new_id;
			$tree[$tree_id]['text'] = $name;
			$tree[$tree_id]['type'] = is_dir($item) ? 'Directory' : 'File';
			$tree[$tree_id]['icon'] = is_dir($item) ? 'fa fa-folder' : 'fa fa-file-o';
			// $tree[$tree_id]['state'] = ['opended' => $opended, 'disabled' => false, 'selected' => false];
			// $tree[$tree_id]['data'] = ['pathinfo' => $pathinfo,'full_path' => $item,'realpath' => realpath($item),'link' => is_link($item),'dir' => is_dir($item),'file' => is_file($item),'readable' => is_readable($item),'writable' => is_writable($item),'executable' => is_executable($item)];
			// var_dump($tree[$item]['data']['pathinfo']);
			if (@is_dir($item) && !@is_link($item)) {
				$tree[$tree_id]['children'] = static::getTreeAsArray($item, $tree[$tree_id]['id']);
			}
			$tree_id++;
		}
		return $tree;
	}

	/********************************************************************************************************************/
	/*** REGEX TOOLS
	/********************************************************************************************************************/

	public static function escapeCharsForStringPregTest($string) {
		return str_replace(['|',DIRECTORY_SEPARATOR], ['\\|','\\'.DIRECTORY_SEPARATOR], $string);
	}

	public static function escapeString($string) {
		return preg_quote($string, DIRECTORY_SEPARATOR);
	}

	public static function extractRegexIndicator(&$string) {
		$result = static::isRegexp($string);
		$string = preg_replace('/^'.static::REGEX_INDICATOR.'/', '', $string);
		return $result;
	}

	public static function isRegexp($string) {
		return preg_match('/^'.static::REGEX_INDICATOR.'/', $string);
	}

	public static function cleanFilename(&$string) {
		if(null === static::computeTextOrNull($string)) return $string;
		$string = Transliterator::unaccent($string); // replace accent characters with base character
		$string = preg_replace('/[\\W_]+$/', '', $string); // remove all end wrong characters (.,; etc.)
		$string = preg_replace('/[\\.]+/', '.', $string); // reduce multiple "." to one
		$string = preg_replace('/[^\\w\\.]+/', '_', $string); // replace wrong characters with _
		// return $string;
		return static::computeTextOrNull($string);
	}

	/**
	 * Remove file extension (.jpeg, etc.)
	 * @param string &$string
	 * @return string
	 */
	public static function removeFileExtension(&$string, $extensionMaxSize = 4) {
		$ext = static::getSplitedFileExtension($string, $extensionMaxSize);
		$string = $ext['name'];
		return $string;
	}

	/**
	 * Get array of 'name' and 'ext' of a filename (.jpeg, etc.)
	 * @param string $string
	 * @return array ['name' => string, 'ext' => string|null] / if no extension found, array['ext'] = null
	 */
	public static function getSplitedFileExtension($string, $extensionMaxSize = 4) {
		$result = ['name' => null, 'ext' => null];
		if(null === static::cleanFilename($string)) return $result;
		$split = preg_split('/\\.+/', $string);
		$split = array_filter($split, function($item) { return !empty($item); });
		switch (count($split)) {
			case 0:
				// return $result;
				break;
			case 1:
				$result['name'] = reset($split);
				break;
			default:
				if(strlen(end($split)) <= $extensionMaxSize) $result['ext'] = array_pop($split);
				$result['name'] = implode('.', $split);
				break;
		}
		return $result;


		$string = preg_replace('/[\\.\\s]+$/', '', trim($string));
		if (!preg_match('/\\./', $string)) return ['name' => $string, 'ext' => null];
		$result = preg_match('/^(.*)\\.([^.]*)?$/', $string);
		return ['name' => $result[1], 'ext' => $result[2]];
	}


	public static function cleanString($string) {
		return preg_replace('/(^[\\.\\s]+)([\\.\\s]+$)/', '', $string);
	}




}

