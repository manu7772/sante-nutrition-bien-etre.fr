<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\Filesystem\Filesystem;
// BaseBundle
use ModelApi\BaseBundle\Entity\Popup;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Interfaces\extendedBundleInterface;
// use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Service\serviceGrants;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Service\serviceUser;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Service\serviceImage;
use ModelApi\FileBundle\Service\FileManager;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// EventBundle
use ModelApi\EventBundle\Entity\Parc;
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Service\serviceEvent;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;

// DevprintBundle
use ModelApi\DevprintsBundle\Service\YamlLog;
use ModelApi\DevprintsBundle\Service\DevTerminal;

use \ReflectionClass;
use \Exception;
use \DateTime;

class serviceKernel implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	// Environment
	const ENVIROMENT_DEV = "dev";
	const ENVIROMENT_TEST = "test";
	const ENVIROMENT_PROD = "prod";

	const CONTEXT_TOKEN_NAME = 'contextual_environment';
	const DEFAULT_ROUTE = 'website_site_homepage';

	protected $container;
	protected $currentBundle;
	protected $request_stack;
	protected $request;
	protected $router;
	protected $currentLocale;
	protected $defaultLocale;
	protected $translationDomain;
	protected $isProfiler;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->currentBundle = null;
		$this->request_stack = $this->container->get('request_stack');
		$this->request = $this->request_stack->getCurrentRequest();
		$this->router = $this->container->get('router');
		// if(!$this->isNorequest()) YamlLog::directYamlLogfile(['serviceKernel locales' => ['current' => $this->getLocale(), 'default' => $this->getDefaultLocale()]]);
		$this->currentLocale = $this->getLocale();
		$this->defaultLocale = $this->getDefaultLocale();
		$this->translationDomain = null;
		$this->isProfiler = 'isProfiler';
		return $this;
	}


	/**
	 * Get Project directory (--> app/)
	 * @return string (app folder)
	 */
	public function getProjectDir($path = null, $falseIfNotFound = false) {
		// return $this->container->getParameter('project_dir');
		$project_dir = $this->container->get('kernel')->getRootDir().serviceTools::preSeparator($path);
		if($falseIfNotFound) {
			$filesystem = new Filesystem();
			if(!$filesystem->exists($project_dir)) return false;
		}
		return $project_dir;
	}

	public function getBaseDir($path = null, $falseIfNotFound = false) {
		$baseDir = dirname($this->getProjectDir()).serviceTools::preSeparator($path);
		if($falseIfNotFound) {
			$filesystem = new Filesystem();
			if(!$filesystem->exists($baseDir)) return false;
		}
		return $baseDir;
	}


	/***********************************/
	/*** SESSION
	/***********************************/

	// public function setRequest(Request $request) {
	// 	$this->request = $request;
	// 	// YamlLog::directYamlLogfile(['serviceKernel locales' => ['current' => $this->getLocale(), 'default' => $this->getDefaultLocale()]]);
	// 	return $this;
	// }

	public function getRequest() {
		return $this->request;
		// return $this->request_stack->getCurrentRequest();
	}

	public function getRequestAttribute($attribute) {
		if($this->isNorequest()) return null;
		return $this->getRequest()->attributes->get($attribute);
	}

	public function getSession() {
		if($this->isNorequest()) return null;
		return $this->getRequest()->getSession();
	}

	public function getFlashbag() {
		if($this->isNorequest()) return null;
		return $this->getRequest()->getSession()->getFlashbag();
	}



	/***********************************/
	/*** SESSION VALUES
	/***********************************/

	public static function mergeArraySessionData($values, $defaults) {
		if(is_array($values) && is_array($defaults)) {
			foreach ($values as $key => $value) {
				if(is_array($value) && is_array($defaults[$key])) $values[$key] = static::mergeArraySessionData($value, $defaults[$key]);
			}
		}
		if(empty($values) && !empty($defaults)) $values = $defaults;
		return $values;
	}

	public function getSessionValue($name, $defaults = null, $refresh = true, callable $callable = null, Request $request = null) {
		if(!($request instanceOf Request)) {
			$session = null === $this->getMasterRequest() || null === $this->getMasterRequest()->getSession() ? null : $this->getMasterRequest()->getSession();
		} else {
			$session = !empty($request->getSession()) ? $request->getSession() : null;
		}
		if(empty($session)) return $defaults;
		$values = $session->get($name);
		$values = static::mergeArraySessionData($values, $defaults);
		if(is_callable($callable)) $values = $callable($values);
		if($refresh) $this->setSessionValue($name, $values);
		return $values;
	}

	public function setSessionValue($name, $value, Request $request = null) {
		if(!($request instanceOf Request)) {
			$session = null === $this->getMasterRequest() || null === $this->getMasterRequest()->getSession() ? null : $this->getMasterRequest()->getSession();
		} else {
			$session = !empty($request->getSession()) ? $request->getSession() : null;
		}
		if(empty($session)) return false;
		$session->set($name, $value);
		return true;
	}

	public function getDataBySession($name, Request $request, $defaults, callable $callable = null) {
		if(empty($defaults)) throw new Exception("Error ".__METHOD__."(): defaults parameter is missing!", 1);
		$request_values = $request_origins = array_merge($request->query->all(), $request->request->all());
		if(isset($request_values['reset'])) {
			if(in_array($request_values['reset'], [1, true, 'true', '1'])) $this->setSessionValue($name, $defaults);
			unset($request_values['reset']);
		}
		// filter data keys
		if(is_array($defaults)) {
			$keys = array_keys($defaults);
			$request_values = array_filter($request_values, function($key) use ($keys) {
				return in_array($key, $keys);
			}, ARRAY_FILTER_USE_KEY);
		}
		$origins = $this->getSessionValue($name, $defaults, false, null, $request);
		$values = array_merge($origins, $request_values);
		if(is_callable($callable)) $values = $callable($values, $origins, $request_origins);
		$this->setSessionValue($name, $values);
		return $values;
	}


	/***********************************/
	/*** SESSION DATA
	/***********************************/

	/**
	 * Set entity in session
	 * @param object $entity
	 * @return boolean (true if $entity setted in session / false if not)
	 */
	public function setInSession($entity) {
		if(method_exists($entity, 'isLifesession') && !$entity->isLifesession()) return false;
		if($this->hasInSession($entity)) return true;
		$session_entities = $this->getSession()->get('session_entities');
		if(!is_array($session_entities)) $session_entities = [];
		$session_entities[$entity->getBddid()] = new DateTime($entity->getLifesession(true));
		$this->getSession()->set('session_entities', $session_entities);
		return $this->hasInSession($entity, false);
	}

	/**
	 * Clear entities in session
	 * @return boolean (true if emptied)
	 */
	public function clearInSession() {
		$this->getSession()->set('session_entities', []);
		return empty($this->getSession()->get('session_entities'));
	}

	/**
	 * Remove obsolete entities in session
	 * @return array
	 */
	public function cleanInSession() {
		$session_entities = $this->getSession()->get('session_entities');
		$session_entities = array_filter($session_entities, function($data) {
			return $data > new DateTime();
		});
		$this->getSession()->set('session_entities', $session_entities);
		return $session_entities;
	}

	/**
	 * Has entity in session
	 * @param object $entity
	 * @param boolean $asContextDate = true
	 * @return boolean
	 */
	public function hasInSession($entity, $asContextDate = true) {
		$session_entities = $this->getSession()->get('session_entities');
		if(!is_array($session_entities)) $session_entities = [];
		$contextDate = $asContextDate ? $this->container->get(serviceContext::class)->getContextDate(true) : new DateTime(serviceContext::DEFAULT_DATE);
		return array_key_exists($entity->getBddid(), $session_entities) && $session_entities[$entity->getBddid()] > $contextDate;
	}

	/**
	 * Set entity's end of session
	 * @param object $entity
	 * @return boolean (true if $entity setted in session / false if not)
	 */
	public function getSessionEnds($entity) {
		$session_entities = $this->getSession()->get('session_entities');
		if(!is_array($session_entities)) $session_entities = [];
		return array_key_exists($entity->getBddid(), $session_entities) ? $session_entities[$entity->getBddid()] : null;
	}

	/**
	 * Get all entities in session
	 * @param object $asEntites = true
	 * @param object $clean = true
	 * @return array
	 */
	public function getAllInSession($asEntites = true, $clean = true) {
		if($clean) $this->cleanInSession();
		$session_entities = $this->getSession()->get('session_entities');
		if(!is_array($session_entities)) $session_entities = [];
		if(empty($session_entities)) return [];
		$bddids = array_keys($session_entities);
		return $asEntites ? $this->container->get(serviceEntities::class)->findByBddids($bddids) : $bddids;
	}

	/**
	 * Remove entity from session
	 * @param object $entity
	 * @return serviceKernel
	 */
	public function removeFromSession($entity) {
		$session_entities = $this->getSession()->get('session_entities');
		if(!is_array($session_entities)) $session_entities = [];
		$session_entities = array_filter($session_entities, function($data, $key) use ($entity) {
			return $key != $entity->getBddid();
		}, ARRAY_FILTER_USE_BOTH);
		$this->getSession()->set('session_entities', $session_entities);
		return $this;
	}

	/**
	 * Is Pageweb SEO (referencement) by $media
	 * @param string $media
	 * @param Pageweb $pageweb = null
	 * @return boolean
	 */
	public function referencementActive($media, Pageweb $pageweb = null) {
		if(!($pageweb instanceOf Pageweb)) return false;
		if(!$pageweb->isSeo()) return false;
		if(!$this->isProd()) return false;
		$serviceContext = $this->container->get(serviceContext::class);
		if(!$serviceContext->isNormalContext()) return false;
		// if(!$serviceContext->isDefaultEntreprise()) return false;
		$entreprise = $serviceContext->getContextEntreprise();
		if(!($entreprise instanceOf Entreprise)) return false;
		if(!$entreprise->ceocodesnameExists($media)) return false;
		if(!is_string($entreprise->getCeocode($media))) return false;
		return true;
	}
	public function isSeo($media, Pageweb $pageweb = null) {
		return $this->referencementActive($media, $pageweb);
	}


	/***********************************/
	/*** REQUESTS
	/***********************************/

	public function getMasterRequest() {
		return $this->request_stack->getMasterRequest();
	}

	public function isMasterRequest() {
		return $this->getRequest() === $this->getMasterRequest();
	}

	public function isXmlHttpRequest() {
		return $this->getMasterRequest()->isXmlHttpRequest();
	}

	// public function getParentRequest() {
	// 	return $this->request_stack->getParentRequest();
	// }

	public function hasRequest() {
		return null !== $this->getRequest();
		// return null === $this->getRequest() && null === $this->getMasterRequest();
	}

	public function isNorequest() {
		return null === $this->getRequest();
		// return null === $this->getRequest() && null === $this->getMasterRequest();
	}

	public function isFixturesContext() {
		return $this->isNorequest();
	}


	/***********************************/
	/*** LOCALES
	/***********************************/

	public function setLocale($currentLocale) {
		$this->currentLocale = $currentLocale;
		return $this;
	}

	public function getLocale($asUnderscore = false) {
		if(!empty($this->currentLocale)) return $asUnderscore ? preg_replace('/-/', '_', $this->currentLocale) : $this->currentLocale;
		// YamlLog::directYamlLogfile(['serviceKernel locales' => ['current' => $this->getSessionValue('locale', $this->container->getParameter('locale')), 'default' => $this->getSessionValue('default_locale', $this->container->getParameter('locale'))]]);
		// if($this->isNorequest()) return $this->container->getParameter('locale');
		$loc = $this->getSessionValue('locale', $this->container->getParameter('locale'));
		return $asUnderscore ? preg_replace('/-/', '_', $loc) : $loc;
		// return $this->getRequest()->getLocale();
		// return $this->getMasterRequest() instanceOf Request ? $this->getMasterRequest()->getLocale() : null;
	}

	public function setDefaultLocale($defaultLocale) {
		$this->defaultLocale = $defaultLocale;
		return $this;
	}

	public function getDefaultLocale() {
		if(isset($this->defaultLocale) && null !== $this->defaultLocale) return $this->defaultLocale;
		// YamlLog::directYamlLogfile(['serviceKernel locales' => ['current' => $this->getSessionValue('locale', $this->container->getParameter('locale')), 'default' => $this->getSessionValue('default_locale', $this->container->getParameter('locale'))]]);
		// if($this->isNorequest()) return $this->container->getParameter('locale');
		return $this->getSessionValue('default_locale', $this->container->getParameter('locale'));
		// return $this->getRequest()->getDefaultLocale();
		// return $this->getMasterRequest() instanceOf Request ? $this->getMasterRequest()->getDefaultLocale() : null;
	}


	/***********************************/
	/*** ENVIRONMENT
	/***********************************/

	/**
	 * Get environment (as string)
	 * @return string
	 */
	public function getEnv() {
		return $this->container->get('kernel')->getEnvironment();
	}

	/**
	 * Is DEV environment
	 * @return boolean
	 */
	public function isDev() {
		return strtolower($this->getEnv()) === static::ENVIROMENT_DEV;
	}

	/**
	 * Is TEST environment
	 * @return boolean
	 */
	public function isTest() {
		return strtolower($this->getEnv()) === static::ENVIROMENT_TEST;
	}

	/**
	 * Is PROD environment
	 * @return boolean
	 */
	public function isProd() {
		return strtolower($this->getEnv()) === static::ENVIROMENT_PROD;
	}

	/**
	 * Has Controller
	 * @return boolean
	 */
	public function hasController() {
		$request = $this->getRequest();
		if(!$request instanceOf Request) return false;
		return $request->attributes->has('_controller') ? !empty($request->attributes->get('_controller')) : false;
	}

	/**
	 * Has Bundle
	 * @return boolean
	 */
	public function hasBundle() {
		return $this->getCurrentBundle() instanceOf Bundle;
	}

	/**
	 * Is Profiler (DEV)
	 * @return boolean
	 */
	public function isProfiler() {
		if(is_bool($this->isProfiler)) return $this->isProfiler;
		$bundle = $this->getCurrentBundle(true);
		return $this->isProfiler = is_array($bundle) ? $bundle['type'] === 'profiler' : false;
		// return $bundle instanceOf Bundle ? false : preg_match('#\\.profiler:#', $bundle);
	}

	/**
	 * Get type of controller if exists and bundle not found
	 * @return boolean
	 */
	public function getTypeIfBundleNotFound() {
		$bundle = $this->getCurrentBundle(true);
		return is_array($bundle) ? $bundle : null;
	}



	/***********************************/
	/*** BUNDLES
	/***********************************/

	/**
	 * Get current bundle name
	 * @return string
	 */
	public function getCurrentBundleName() {
		$bundle = $this->getCurrentBundle();
		if(!($bundle instanceOf Bundle)) return null;
		return serviceClasses::getShortname($bundle);
	}

	public function getBundleNameAnyway($shortname = true) {
		$bundle = $this->getCurrentBundle(true);
		if($bundle instanceOf Bundle) return $shortname ? serviceClasses::getShortname($bundle) : get_class($bundle);
		if(is_array($bundle)) return $shortname ? $bundle['type'].'_unBundled' : $bundle['controller'];
		return 'unknown_bundle';
	}

	/**
	 * Get current bundle
	 * @param boolean $getDetailsIfNotFound = false
	 * @return Bundle | null
	 */
	public function getCurrentBundle($getDetailsIfNotFound = false) {
		if($this->currentBundle instanceOf Bundle) return $this->currentBundle;
		$request = $this->getRequest();
		$bundles = $this->getBundles();
		$bundle = null;
		if($request instanceOf Request) {
			$_controller = $request->attributes->get('_controller');
			// echo('<pre><h3>Controller</h3>'); var_dump($_controller); echo('</pre>');
			if(preg_match('#\\\\Controller\\\\#', $_controller)) {
				$exp = preg_split('#\\\\Controller\\\\#', $_controller);
				$bundle = preg_replace('#\\\\#', '', reset($exp));
				if(array_key_exists($bundle, $bundles)) return $this->currentBundle = $bundles[$bundle];
			} else if(preg_match('#\\.controller:#', $_controller)) {
				$exp = explode('.', $_controller);
				array_pop($exp);
				$exp = array_reverse($exp);
				$bundle = '';
				foreach ($exp as $item) {
					$bundle = ucfirst($item).$bundle;
					if(array_key_exists($bundle.'Bundle', $bundles)) return $this->currentBundle = $bundles[$bundle.'Bundle'];
				}
			}
			if(!empty($_controller) && preg_match('#\\.([\\w]+):#', $_controller, $matches)) {
				if($getDetailsIfNotFound) {
					return ['controller' => $_controller, 'type' => $matches[1]];
				}
				// if($this->isDev() && !empty($_controller)) throw new Exception("Error ".__METHOD__."(): could not find bundle by controller ".json_encode($_controller)."!", 1);
			}
		}
		return null;
	}

	public function getBundles() {
		return array_filter($this->container->get('kernel')->getBundles(), function($bundle) { return $bundle instanceOf Bundle; });
	}

	public function bundleExists($name) {
		return !empty($this->getBundleByName($name));
	}

	public function getBundleByName($name) {
		if($name instanceOf Bundle) return $name;
		foreach ($this->getBundles() as $bundlename => $bundle) if($bundlename === $name) return $bundle;
		return null;
	}

	/**
	 * @see https://www.w3schools.com/php/func_filesystem_dirname.asp
	 */
	public function getBundlePath($name, $path = null) {
		$bundle = $this->getBundleByName($name);
		if(empty($bundle)) return null;
		$bundlephpfile = serviceTools::magnifyPath($this->getBaseDir('src/').get_class($bundle)); // ".php" not required !!!"
		$bundlepath = dirname($bundlephpfile).serviceTools::preSeparator($path, true);
		return @file_exists($bundlepath) && is_dir($bundlepath) ? serviceTools::magnifyPath($bundlepath, true) : false;
	}

	public function getBundleNames() {
		$bundleNames = [];
		foreach ($this->getBundles() as $name => $bundle) $bundleNames[$name] = get_class($bundle);
		return $bundleNames;
	}

	public function getPublicBundles() {
		$bundles = $this->getBundles();
		return array_filter($bundles, function($bundle) {
			return method_exists($bundle, 'isPublicBundle') ? $bundle->isPublicBundle() : false;
		});
	}

	public function getPublicBundleNames() {
		$bundleNames = [];
		foreach ($this->getPublicBundles() as $name => $bundle) $bundleNames[$name] = get_class($bundle);
		return $bundleNames;
	}

	public function isPublicBundle() {
		$currentBundle = $this->getCurrentBundle();
		// return $currentBundle instanceOf extendedBundleInterface ? $currentBundle->isPublicBundle() : false;
		return method_exists($currentBundle, 'isPublicBundle') ? $currentBundle->isPublicBundle() : false;
	}

	public function getTemplateStructures(User $user = null) {
		$structures = [];
		foreach ($this->getTemlatableBundles($user) as $name => $bundle) {
			if(method_exists($bundle, 'getTemplateStructure')) $structures[$name] = $bundle->getTemplateStructure();
		}
		return $structures;
	}

	public function getTemlatableBundles(User $user = null) {
		$bundles = $this->getBundles();
		$bundles = array_filter($bundles, function($bundle) use ($user) {
			if(!method_exists($bundle, 'getRoleTemplatable')) return false;
			$templatable = $bundle->getRoleTemplatable();
			return $this->isNorequest() || $this->container->get(serviceGrants::class)->isGranted($templatable, $user);
		});
		return $bundles;
	}

	public function getTemlatableBundleNames(User $user = null) {
		$bundleNames = [];
		foreach ($this->getTemlatableBundles($user) as $name => $bundle) $bundleNames[$name] = get_class($bundle);
		return $bundleNames;
	}

	public function getSeoBundles(User $user = null) {
		$bundles = $this->getTemlatableBundles($user);
		return array_filter($bundles, function($bundle) {
			return method_exists($bundle, 'isSeo') ? $bundle->isSeo() : false;
		});
	}

	public function getSeoBundleNames(User $user = null) {
		$bundleNames = [];
		foreach ($this->getSeoBundles($user) as $name => $bundle) $bundleNames[$name] = get_class($bundle);
		return $bundleNames;
	}

	public function isSeoBundle($bundleOrName, User $user = null) {
		if($bundleOrName instanceOf Bundle) $bundleOrName = get_class($bundleOrName);
		$bundles = $this->getSeoBundles($user);
		return in_array($bundleOrName, $bundles);
	}

	public function getTranslationDomainByBundle($bundleName = null, $refresh = false, $findDefaultAnyway = false) {
		if(null === $this->translationDomain || $refresh) {
			$bundle = is_string($bundleName) ? $this->getBundleByName($bundleName) : $this->getCurrentBundle();
			if(!empty($bundle) && method_exists($bundle, 'getTranslationDomain')) $this->translationDomain = $bundle->getTranslationDomain();
			if(!is_string($this->translationDomain) && $findDefaultAnyway) $this->translationDomain = serviceTwgTranslation::DEFAULT_DOMAIN;
		}
		return $this->translationDomain;
	}



	/***********************************/
	/*** VALIDATE ENTITY
	/***********************************/

	public function getErrorsReport($object, $excludes = []) {
		return $this->container->get(serviceEntities::class)->getErrorsReport($object, $excludes);
	}


}