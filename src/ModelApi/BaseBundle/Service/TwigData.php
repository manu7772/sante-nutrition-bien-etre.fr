<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use \Twig_Extension;
use \Twig_SimpleFilter;
use \Twig_SimpleFunction;
use JMS\Serializer\SerializationContext;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Form\FormView;

// BaseBundle
use ModelApi\BaseBundle\Entity\Popup;
use ModelApi\BaseBundle\Entity\Annonce;
use ModelApi\BaseBundle\Traits\BaseEntity;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceForms;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Service\serviceGrants;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\DirectoryInterface;
use ModelApi\FileBundle\Service\serviceImage;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// EventBundle
use ModelApi\EventBundle\Entity\Parc;
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Service\serviceEvent;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\Actions;
use ModelApi\CrudsBundle\Service\serviceCruds;

use \DateTime;
use \ReflectionClass;
use \ReflectionMethod;
use \ReflectionParameter;
// use \ArrayAccess;
use \Exception;

class TwigData extends Twig_Extension implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	private $container;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	public function getFunctions() {
		return array(
			// functions
			new Twig_SimpleFunction('currentYear', array($this, 'getCurrentYear')),
			new Twig_SimpleFunction('getDatetime', array($this, 'getDatetime')),
			new Twig_SimpleFunction('isBefore', array($this, 'isBefore')),
			new Twig_SimpleFunction('ifAfter', array($this, 'ifAfter')),
			new Twig_SimpleFunction('contextYear', array($this, 'getContextYear')),
			new Twig_SimpleFunction('getLanguage', array($this, 'getLanguage')),
			new Twig_SimpleFunction('login_data', array($this, 'getLoginDataForForm')),
			new Twig_SimpleFunction('register_data', array($this, 'getRegistrationDataForForm')),
			new Twig_SimpleFunction('icon', array($this, 'getIcon')),
			new Twig_SimpleFunction('previousMonday', array($this, 'getPreviousDayOfMonth')),
			new Twig_SimpleFunction('nameOfDay', array($this, 'getNameOfDay')),
			new Twig_SimpleFunction('namesOfDay', array($this, 'getNamesOfDay')),
			new Twig_SimpleFunction('getArray', array($this, 'getArray')),
			new Twig_SimpleFunction('shuffle_array', array($this, 'shuffle_array')),
			// Labo Form
			new Twig_SimpleFunction('laboFormData', array($this, 'getLaboFormData')),
			// Translation
			new Twig_SimpleFunction('domainByKeytext', array($this, 'getDomainByKeytext')),
			// color
			new Twig_SimpleFunction('role_color', array($this, 'getRoleColor')),
			new Twig_SimpleFunction('role_icon', array($this, 'getRoleIcon')),
			new Twig_SimpleFunction('status_color', array($this, 'getStatusColor')),
			new Twig_SimpleFunction('color_contrast', array($this, 'getContrastColor')),
			new Twig_SimpleFunction('color_inverse', array($this, 'getColorInverse')),
			// serialization
			new Twig_SimpleFunction('serialized', array($this, 'getSerialized')),
			// tables
			new Twig_SimpleFunction('table_compile_parameters', array($this, 'tableCompileParameters')),
			new Twig_SimpleFunction('tableClass', array($this, 'tableClass')),
			// agenda
			new Twig_SimpleFunction('parcsList', array($this, 'parcsList')),
			new Twig_SimpleFunction('parcsCalendars', array($this, 'getCachedCalendar')),
			// entities
			new Twig_SimpleFunction('canManage', array($this, 'canManage')),
			new Twig_SimpleFunction('image_by_slug', array($this, 'getImageBySlug')),
			new Twig_SimpleFunction('request_data', array($this, 'getRequestData')),
			new Twig_SimpleFunction('sortedSlugRequests', array($this, 'sortedSlugRequests')),
			new Twig_SimpleFunction('sort_entities', array($this, 'sortEntities')),
			new Twig_SimpleFunction('attr', array($this, 'getAttr')),
			new Twig_SimpleFunction('attrs', array($this, 'getAttrs')),
			new Twig_SimpleFunction('metadata_relations', array($this, 'getMetadataRelation')),
			new Twig_SimpleFunction('metadata_singles', array($this, 'getMetadataSingle')),
			new Twig_SimpleFunction('getPreference', array($this, 'getPreference')),
			new Twig_SimpleFunction('getForm', array($this, 'getForm')),
			new Twig_SimpleFunction('getAnnotUpdateForm', array($this, 'getAnnotUpdateForm')),
			new Twig_SimpleFunction('getDirectorys', array($this, 'getDirectorys')),
			new Twig_SimpleFunction('getBinarys', array($this, 'getBinaryAssociations')),
			new Twig_SimpleFunction('random_media', array($this, 'getRandomMedia')),
			new Twig_SimpleFunction('pagewebDefaultsValues', array($this, 'getPagewebDefaultsValues')),
			new Twig_SimpleFunction('getDoublons', array($this, 'getDoublons')),
			// routes
			new Twig_SimpleFunction('route_exists', array($this, 'route_exists')),
			new Twig_SimpleFunction('hasRoute', array($this, 'route_exists')),
			new Twig_SimpleFunction('entity_url', array($this, 'getEntityUrl')),
			new Twig_SimpleFunction('entity_actions', array($this, 'getEntityActions')),
			// new Twig_SimpleFunction('cat_route_data', array($this, 'cat_route_data')),
			new Twig_SimpleFunction('cat_path', array($this, 'cat_path')),
			new Twig_SimpleFunction('cat_url', array($this, 'cat_url')),
			// session
			new Twig_SimpleFunction('setInSession', array($this, 'setInSession')),
			new Twig_SimpleFunction('clearInSession', array($this, 'clearInSession')),
			new Twig_SimpleFunction('cleanInSession', array($this, 'cleanInSession')),
			new Twig_SimpleFunction('hasInSession', array($this, 'hasInSession')),
			new Twig_SimpleFunction('getSessionEnds', array($this, 'getSessionEnds')),
			new Twig_SimpleFunction('getAllInSession', array($this, 'getAllInSession')),
			new Twig_SimpleFunction('removeFromSession', array($this, 'removeFromSession')),
			new Twig_SimpleFunction('referencementActive', array($this, 'referencementActive')),
			// timer
			new Twig_SimpleFunction('starttimer', array($this, 'starttimer')),
			new Twig_SimpleFunction('printtimer', array($this, 'printtimer')),
		);
	}

	public function getFilters() {
		return array(
			// filters
			new Twig_SimpleFilter('field', array($this, 'getField')),
			new Twig_SimpleFilter('ucfirst', array($this, 'getUcfirst')),
			new Twig_SimpleFilter('textarea_escape', array($this, 'getTextareaEscape')),
			new Twig_SimpleFilter('shortname', array($this, 'getShortname')),
			new Twig_SimpleFilter('addOneDay', array($this, 'addOneDay')),
			new Twig_SimpleFilter('arrayunique', array($this, 'arrayunique')),
			new Twig_SimpleFilter('json_decode', array($this, 'getJsonDecode')),
			// url
			new Twig_SimpleFilter('url_params_combine', array($this, 'urlParamsCombine')),
			new Twig_SimpleFilter('url_normalized', array($this, 'getUrlnormalized')),
			new Twig_SimpleFilter('url_decode', array($this, 'getUrldecode')),
			// text
			new Twig_SimpleFilter('empty', array($this, 'isEmpty')),
			new Twig_SimpleFilter('hasText', array($this, 'hasText')),
			new Twig_SimpleFilter('magnifytext', array($this, 'getMagnifiedText')),
			new Twig_SimpleFilter('striptags', array($this, 'getStriptags')),
			new Twig_SimpleFilter('br_striptags', array($this, 'getBrStriptags')),
			new Twig_SimpleFilter('wordscount', array($this, 'wordscount')),
			// serialization
			new Twig_SimpleFilter('serialized', array($this, 'getSerialized')),
			// tests on entities
			new Twig_SimpleFilter('attr', array($this, 'getAttr')),
			new Twig_SimpleFilter('isEntity', array($this, 'isEntity')),
			new Twig_SimpleFilter('isInstance', array($this, 'isInstance')),
			new Twig_SimpleFilter('isInterface', array($this, 'isInterface')),
			new Twig_SimpleFilter('isInstanceOrInterface', array($this, 'isInstanceOrInterface')),
			new Twig_SimpleFilter('filter_valids', array($this, 'getFilteredValids')),
			new Twig_SimpleFilter('role_color', array($this, 'getRoleColor')),
			new Twig_SimpleFilter('role_icon', array($this, 'getRoleIcon')),
			new Twig_SimpleFilter('status_color', array($this, 'getStatusColor')),
			// tests
			new Twig_SimpleFilter('isArray', array($this, 'isArray')),
			new Twig_SimpleFilter('isString', array($this, 'isString')),
			new Twig_SimpleFilter('isObject', array($this, 'isObject')),
			new Twig_SimpleFilter('isInteger', array($this, 'isInteger')),
			new Twig_SimpleFilter('isBoolean', array($this, 'isBoolean')),
			new Twig_SimpleFilter('isDatetime', array($this, 'isDatetime')),
			// Default and force type
			new Twig_SimpleFilter('removeKeys', array($this, 'removeKeys')),
			new Twig_SimpleFilter('default_string', array($this, 'default_string')),
			new Twig_SimpleFilter('default_integer', array($this, 'default_integer')),
			new Twig_SimpleFilter('default_array', array($this, 'default_array')),
			// google map
			new Twig_SimpleFilter('googlemappize', array($this, 'googlemappize')),
		);
	}

	////////////////////////////// FUNCTIONS /////////////////

	/**
	 * Get current year
	 * @return string
	 */
	public function getCurrentYear() {
		return $this->container->get(serviceContext::class)->getCurrentYear();
	}

	/**
	 * Get DateTime
	 * @return DateTime
	 */
	public function getDatetime($date = 'NOW') {
		return new DateTime($date);
	}

	/**
	 * Now is before date
	 * @return boolean
	 */
	public function isBefore($date) {
		return new DateTime($date) > new DateTime();
	}

	/**
	 * Now is after date
	 * @return boolean
	 */
	public function isAfter($date) {
		return new DateTime($date) < new DateTime();
	}

	/**
	 * Get context year
	 * @return string
	 */
	public function getContextYear() {
		return $this->container->get(serviceContext::class)->getContextYear();
	}

	public function getLanguage($locale = null) {
		return $this->container->get(serviceLanguage::class)->getLanguageByLocale($locale);
	}

	public function getRequestData($classname, $method, $parameters = []) {
		$serviceEntities = $this->container->get(serviceEntities::class);
		$classname_entity =  $serviceEntities->getClassnameByAnything($classname);
		$data = [];
		$isdev = $this->container->get(serviceKernel::class)->isDev();
		if(!empty($classname_entity)) {
			$repository = $serviceEntities->getRepository($classname_entity);
			if(method_exists($repository, $method)) {
				// $parameters = [null, 2, 'test'];
				// $parameters = ['sens' => 'ASC', 'nb' => 2, 'date', 'test'];
				// echo('<pre><h3>Parameters ORIGIN</h3>');var_dump($parameters);echo('</pre>');
				$parameters = serviceClasses::compileMethodParameters($repository, $method, $parameters, true);
				if(false === $parameters) throw new Exception("Error ".__METHOD__."(): ".get_class($repository)."::".$method."() is not public or static!", 1);
				// echo('<pre><h3>Parameters COMPILED for '.$method.'</h3>');var_dump($parameters);echo('</pre>');
				//try {
					$data = call_user_func_array(array($repository, $method), $parameters);
				//} catch (Exception $e) {
				//	if($isdev) throw new Exception("Error ".__METHOD__."():\r-> ERROR while querying (by call_user_func_array) ".json_encode($method)." on ".json_encode(get_class($repository))."!\r-> MESSAGE: ".$e->getMessage()."\r-> PARAMETERS: ".json_encode($parameters)."\r", 1);
				//}
			} else {
				// if($isdev) throw new Exception("Error ".__METHOD__."(): method ".json_encode($method)." does not exist in repository ".json_encode(get_class($repository))."!", 1);
				// MAGIC METHODS
				try {
					$data = $repository->$method($parameters);
				} catch (Exception $e) {
					if($isdev) throw new Exception("Error ".__METHOD__."():\r-> ERROR while querying (by named method) ".json_encode($method)." on ".json_encode(get_class($repository))."!\r-> MESSAGE: ".$e->getMessage()."\r-> PARAMETERS: ".json_encode($parameters)."\r", 1);
				}
			}
		} else {
			// if($isdev) throw new Exception("Error ".__METHOD__."(): entity ".json_encode($classname)." does not exist!", 1);
		}
		return $data;
	}

	/**
	 * Get sorted items by array of entities or slugs (or other field name)
	 * Finally, removes items that are not object
	 * @param array $items
	 * @param string $shortname = 'Item'
	 * @param string $fieldName = 'id'
	 * @return array <object>
	 */
	public function sortedSlugRequests($items, $shortname = 'Item', $fieldName = 'id') {
		$searchs = array_filter($items, function ($item) { return is_string($item) || is_integer($item); });
		// echo('<p>Searchs: '.implode(' / ', array_values($searchs)).'</p>');
		if(count($searchs)) $searchs = $this->getRequestData($shortname, Inflector::singularize(Inflector::camelize('findBy_'.$fieldName)), array_values($searchs));
		// echo('<p>Found: '.count($searchs).'</p>');
		if(count($searchs)) {
			$method = Inflector::singularize(Inflector::camelize('get_'.$fieldName));
			foreach ($items as $key => $item) {
				foreach ($searchs as $search) {
					if(!is_object($item) && is_object($search) && $search->$method() === $item) $items[$key] = $search;
				}
			}
		}
		return array_filter($items, function ($item) { return is_object($item); });
	}

	// public function getRequestData($classname, $method, $parameters = [], $parameter2 = null, $parameter3 = null, $parameter4 = null) {
	// 	$serviceEntities = $this->container->get(serviceEntities::class);
	// 	$classname =  $serviceEntities->getClassnameByAnything($classname);
	// 	if(null !== $classname) {
	// 		$repository = $serviceEntities->getRepository($classname);
	// 		// if(method_exists($repository, $method))
	// 			$data = $repository->$method($parameters, $parameter2, $parameter3, $parameter4);
	// 			if(is_array($parameters)) {
	// 				switch ($method) {
	// 					case 'findBySlug':
	// 						$data2 = [];
	// 						foreach ($parameters as $param) {
	// 							foreach ($data as $entity) {
	// 								if($entity->getSlug() === $param) $data2[] = $entity;
	// 							}
	// 						}
	// 						$data = $data2;
	// 						break;
	// 					case 'findById':
	// 						$data2 = [];
	// 						foreach ($parameters as $param) {
	// 							foreach ($data as $entity) {
	// 								if($entity->getId() === $param) $data2[] = $entity;
	// 							}
	// 						}
	// 						$data = $data2;
	// 						break;
	// 				}
	// 			}
	// 			return $data;
	// 	}
	// 	return [];
	// }

	public function sortEntities($entities, $sorts) {
		$collection = $entities instanceOf Collection;
		if($collection) $entities = $entities->toArray();
		usort($entities, function($a, $b) use ($sorts) {
			foreach ($sorts as $field => $sens) {
				// $a value
				$a_value = null;
				if(is_array($a)) {
					$a_value = isset($a[$field]) ? $a[$field] : null;
				} else if(is_object($a)) {
					$method = 'get'.ucfirst($field);
					$a_value = method_exists($a, $method) ? $a->$method() : null;
				}
				// $b value
				$b_value = null;
				if(is_array($b)) {
					$b_value = isset($b[$field]) ? $b[$field] : null;
				} else if(is_object($b)) {
					$method = 'get'.ucfirst($field);
					$b_value = method_exists($b, $method) ? $b->$method() : null;
				}
				if($a_value > $b_value) return 1;
				if($a_value < $b_value) return -1;
			}
			return 0;
		});
		return $collection ? new ArrayCollection($entities) : $entities;
	}

	public function canManage($item, User $user, $action) {
		if($item->getOwner() === $user) return true;
		if($this->container->get(serviceGrants::class)->isGranted('ROLE_ADMIN', $user)) return true;
		switch ($action) {
			case 'update':
				$tiers = $item instanceOf Tier ? [$item] : $item->getOwnerdirs();
				foreach ($tiers as $tier) {
					if($tier->hasAssociateUser($user)) return true;
				}
				if($user->getMicrotimeid() === $item->getMicrotimeid()) return true;
				$owner = $item instanceOf User ? $owner : $item->getOwner();
				return $user->getMicrotimeid() === $owner->getMicrotimeid();
				break;
			case 'enable':
			case 'softdelete':
			case 'delete':
				if($user->getMicrotimeid() === $item->getMicrotimeid()) return true;
				$owner = $item instanceOf User ? $owner : $item->getOwner();
				return $user->getMicrotimeid() === $owner->getMicrotimeid();
				break;
		}
		return false;
	}

	public function getImageBySlug($slug) {
		return $this->container->get(serviceImage::class)->getRepository()->findOneBySlug($slug);
	}

	public function getPreviousDayOfMonth($date = null, $day = 'monday') {
		return serviceTools::getPreviousDayOfMonth($date, $day);
	}

	public function getNameOfDay($number = 1) {
		return serviceTools::getNameOfDay($number);
	}

	public function getNamesOfDay($size = null) {
		return serviceTools::getNamesOfDay($size);
	}

	public function shuffle_array(&$array) {
		$isAc = false;
		if($array instanceOf ArrayCollection) {
			$isAc = true;
			$array = $array->toArray();
		}
		if(!is_array($array)) return false;
		shuffle($array);
		if($isAc) $array = new ArrayCollection($array);
		return $array;
	}

	public function getLaboFormData(FormView $formview, $json_decode = false, $groups = ['base','laboformdata']) {
		// $string = $this->container->get('jms_serializer')->serialize($formview, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups((array)$groups));
		$children = [];
		if(count($formview->children ?? []) > 0) {
			foreach ($formview->children as $key => $child) {
				$children[$key] = $this->getLaboFormData($child, true, $groups);
			}
		}
		$string = json_encode([
			'data' => isset($formview->vars['data']) ? json_decode($this->container->get('jms_serializer')->serialize($formview->vars['data'], 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups(['base'])), true) : null,
			'parent' => !empty($formview->parent ?? null) ? get_class($formview->parent) : null,
			'children' => $children,
			'id' => $formview->vars['id'],
			'name' => $formview->vars['name'],
			'full_name' => $formview->vars['full_name'] ?? null,
			'disabled' => $formview->vars['disabled'] ?? false,
			'required' => $formview->vars['required'] ?? true,
			'block_prefixes' => $formview->vars['block_prefixes'] ?? null,
			'method' => $formview->vars['method'] ?? null,
			'action' => $formview->vars['action'] ?? null,
		]);
		// echo('<div>FormView class: '.get_class($formview).' / Data: '.$string.'</div>');
		return $json_decode ? json_decode($string, true) : $string;
	}

	public function getDomainByKeytext($keytext) {
		$keytext = explode('_', $keytext);
		return count($keytext) === 3 ? ucfirst($keytext[1]) : 'messages';
	}

	public function getStatusColor($entity, $default = 'info', $prefix = null, $suffix = null, $context_date = null) {
		// $color = 'success';
		$color = $default;
		switch (get_class($entity)) {
			case Annonce::class:
				if($entity->isPeremption($context_date)) $color = 'danger';
				if($entity->isSoonPeremption(72, $context_date)) $color = 'warning';
				if($entity->isInactive()) $color = 'warning';
				if($entity->isSoftdeleted($context_date)) $color = 'danger';
				break;
			default:
				// $RC = new ReflectionClass($entity);
				// if($RC->isInterface(BaseEntity::class)) {
				if(serviceClasses::hasTraits(BaseEntity::class)) {
					if($entity->isInactive()) $color = 'warning';
					if($entity->isSoftdeleted($context_date)) $color = 'danger';
				}
				break;
		}
		return empty($color) ? null : $prefix.$color.$suffix;
	}

	public function getRoleColor($userOrRole) {
		if($userOrRole instanceOf User) {
			if(!$userOrRole->isAdminvalidated()) return 'danger';
			if(!$userOrRole->isActive()) return 'muted';
		}
		$role = $userOrRole instanceOf User ? $userOrRole->getHigherRole() : $userOrRole;
		switch ($role) {
			case 'ROLE_SUPER_ADMIN': return 'danger'; break;
			case 'ROLE_ADMIN': return 'warning'; break;
			case 'ROLE_COLLAB': return 'info'; break;
			case 'ROLE_EDITOR': return 'success'; break;
			case 'ROLE_TRANSLATOR': return 'success'; break;
			case 'ROLE_TESTER': return 'muted'; break;
			default: return 'muted'; break;
		}
	}

	public function getRoleIcon($userOrRole) {
		if($userOrRole instanceOf User) {
			if(!$userOrRole->isAdminvalidated()) return 'fa-user';
			if(!$userOrRole->isActive()) return 'fa-circle-o';
		}
		$role = $userOrRole instanceOf User ? $userOrRole->getHigherRole() : $userOrRole;
		switch ($role) {
			case 'ROLE_SUPER_ADMIN': return 'fa-circle'; break;
			case 'ROLE_ADMIN': return 'fa-circle'; break;
			case 'ROLE_COLLAB': return 'fa-circle'; break;
			case 'ROLE_EDITOR': return 'fa-circle'; break;
			case 'ROLE_TRANSLATOR': return $this->getIcon('TwgTranslation', false, true); break;
			case 'ROLE_TESTER': return 'fa-user-o'; break;
			default: return 'fa-circle'; break;
		}
	}

	public function getContrastColor($hexColor, $resultLight = '#FFFFFF', $resultDark = '#000000') {
		return serviceTools::getContrastColor($hexColor, $resultLight, $resultDark);
	}

	public function getColorInverse($hexColor) {
		return serviceTools::getColorInverse($hexColor);
	}

	public function getArray($items) {
		if(is_object($items) && method_exists($items, 'toArray')) $items = $items->toArray();
		if(empty($items)) $items = [];
		if(!is_array($items)) $items = [$items];
		return $items;
	}

	public function getSerialized($data, $groups = ["base","detail"], $as_array = false, $levels = 25) {
		return $this->container->get(serviceClasses::class)->getSerialized($data, $groups, $as_array, $levels);
	}



	// ************************************************
	// TABLES
	// ************************************************

	public function tableCompileParameters($model, $parameters) {
		$serviceEntities = $this->container->get(serviceEntities::class);
		// $parameters['metadata'] = $model instanceOf ClassMetadata ? $model : $serviceEntities->getEntityManager()->getClassMetadata($model);
		// $classname =  $this->container->get(serviceEntities::class)->getClassnameByAnything($entity);
		if(!is_array($parameters)) throw new Exception("Error ".__METHOD__."(): parameters must be defined as array!", 1);
		if(!isset($parameters['data']) || !is_array($parameters['data'])) throw new Exception("Error ".__METHOD__."(): parameter's data must be defined and be array!", 1);

		$parameters['metadata'] = $serviceEntities->getCommonMetadata($parameters['data']);
		
		$parameters['actions'] ??= true;
		if(is_string($parameters['actions']) && !in_array(is_string($parameters['actions']), ['first','last'])) $parameters['actions'] = false;
		if(true === $parameters['actions']) $parameters['actions'] = 2;

		if(!isset($parameters['fields']) || !is_array($parameters['fields']) || empty($parameters['fields'])) {
			// $parameters['fields'] = array_merge($parameters['metadata']->fieldMappings, $parameters['metadata']->associationMappings);
			$parameters['fields'] = true;
		}

		if(!isset($parameters['orderby']) || !is_string($parameters['orderby'])) {
			$parameters['orderby'] = ['id' => "ASC"];
		} else {
			$parameters['orderby'] = [$parameters['orderby'] => "ASC"];
		}

		if(!isset($parameters['contexts'])) {
			$parameters['contexts'] = ["all"];
		}
		if(is_string($parameters['contexts'])) $parameters['contexts'] = [$parameters['contexts']];

		if(!isset($parameters['table_class'])) $parameters['table_class'] = 'table table-hover table-condensed';
		if(is_array($parameters['table_class'])) $parameters['table_class'] = implode(' ', $parameters['table_class']);

		if(!isset($parameters['length']) || !is_integer($parameters['length']) || $parameters['length'] < 1) {
			$parameters['length'] = count($parameters['data']);
		}

		$parameters['cruds'] = $this->container->get(serviceCruds::class)->getAction_CrudsAnnotations($parameters['metadata']->name, $parameters['contexts'], 'Show', $parameters['fields']);

		return $parameters;
	}

	public function tableClass($entity) {
		$class = '';
		if(is_object($entity)) {
			if(method_exists($entity, 'isPrefered')) {
				if($entity->isPrefered()) $class = "info";
			}
			if(method_exists($entity, 'isDisabled')) {
				if($entity->isDisabled()) $class = "warning";
			}
			if(method_exists($entity, 'isSoftdeleted')) {
				if($entity->isSoftdeleted()) $class = "danger";
			}
		} else if(is_array($entity)) {
			if(array_key_exists('prefered', $entity) && true === $entity['prefered']) $class = 'info';
			if(array_key_exists('enabled', $entity) && false === $entity['enabled']) $class = 'warning';
			if(array_key_exists('active', $entity) && false === $entity['active']) $class = 'warning';
			if(array_key_exists('softdeleted', $entity)) {
				if($entity['softdeleted'] instanceOf DateTime && new DateTime() > $entity['softdeleted']) $class = 'danger';
			}
		}
		return $class;
	}



	public function getLoginDataForForm() {
		return $this->container->get(serviceUser::class)->getLoginDataForForm();
	}

	public function getRegistrationDataForForm($options = []) {
		$form = $this->container->get(serviceUser::class)->getRegistrationDataForForm(true, $options);
		return array(
			'form' => $form,
		);
	}

	public function getIcon($entity, $asHtml = true, $asDefault = false, $classes = null, $refresh = false) {
		return $this->container->get(serviceEntities::class)->getIcon($entity, $asHtml, $asDefault, $classes, $refresh);
	}

	public function parcsList($data) {
		$parcs = new ArrayCollection();
		foreach ($data as $key => $value1) {
			if($value1 instanceOf Eventdate) $value1 = $value1->getEvent();
			if($value1 instanceOf Parc && !$parcs->contains($value1)) $parcs->add($value1);
				else if(is_array($value1)) {
					foreach ($this->parcsList($value1) as $value2) {
						if($value2 instanceOf Parc && !$parcs->contains($value2)) $parcs->add($value2);
					}
				}
		}
		return $parcs;
	}

	public function getCachedCalendar($year = null, $month = null, $refresh = false, $notObsoleteMonths = false) {
		return $this->container->get(serviceEvent::class)->getCachedCalendar($year, $month, $refresh, $notObsoleteMonths);
	}

	// public function getContextDate($asObjectOrFormat = true) {
	// 	return $this->container->get(serviceContext::class)->getContextDate($asObjectOrFormat);
	// }

	public function getAttr($entity, $attr_name, $test = false, $param = null, $defaultValueIfMethodDoesNotExist = null) {
		if(is_array($attr_name)) {
			if(!array_key_exists('fieldName', $attr_name)) throw new Exception("Error ".__METHOD__."(): can not find fieldName in array of keys ".json_encode(array_keys($attr_name))."!", 1);
			$attr_name = $attr_name['fieldName'];
		}
		if($attr_name === 'linkdatas') { echo('LINKDATAS HERE!!!'); return null; }
		$getter = Inflector::camelize('get_'.$attr_name);
		if($test) return method_exists($entity, $getter);
		return method_exists($entity, $getter) ? $entity->$getter($param) : $defaultValueIfMethodDoesNotExist;
	}

	public function getAttrs($entity, $attr_names, $removeEmptyFields = false, $getOnlyFirstOrNull = false) {
		$data = [];
		foreach ($attr_names as $key => $value) {
			if(is_string($key)) {
				$data[$key] = $this->getAttr($entity, $key, false, $value, null);
			} else {
				$data[$value] = $this->getAttr($entity, $value, false, null, null);
			}
		}
		$results = $removeEmptyFields ?
			array_filter($data, function($value) { return !empty($value); }) :
			$data ;
		if($getOnlyFirstOrNull) $results = count($results) ? reset($results) : null;
		return $results;
	}

	public function getMetadataRelation($entity, $classes, $fields = true, $addSubclasses = false) {
		return $this->container->get(serviceEntities::class)->getMetadataAssociations($entity, $classes, $fields, $addSubclasses);
	}

	public function getMetadataSingle($entity, $fields = true, $types = true) {
		return $this->container->get(serviceEntities::class)->getMetadataFields($entity, $fields, $types);
	}

	public function getPreference($string, $bddid = null, $getValues = false) {
		$preference = $this->container->get(servicePreference::class)->getPreference($string, $bddid, $getValues);
		return $preference;
	}

	public function getForm($typeClassname, $data = []) {
		// return false;
		if(!class_exists($typeClassname)) return false;
		// $form = $this->serviceEntities->getForm();
		$data['context'] ??= null;
		$form = $this->container->get('form.factory')->create(
			$typeClassname,
			$data['event'],
			array(
				// 'submit' => true,
				// 'submits' => true,
				'method' => 'POST',
				// 'action' => $this->generateUrl($params['ACTION_ROUTE'], ['context' => $context])
			)
		);
		return $form->createView();
	}

	public function getAnnotUpdateForm($entity, User $user = null, $formContexts = null, $method = 'put', $params = []) {
		$form = $this->container->get(serviceForms::class)->getUpdateForm($entity, $user, $formContexts, $method, $params);
		return !empty($form) ? $form->createView() : null;
	}

	public function getDirectorys($data) {
		if($this->isArray($data)) {
			// nothing
		}
		if($this->isEntity($data)) {
			$isDI = method_exists($data, 'isInterface') ? $data->isInterface(DirectoryInterface::class) : serviceClasses::isInterface($data, DirectoryInterface::class);
			if(!$isDI) {
				if(method_exists($data, 'hasDirs')) {
					if(!$data->hasDirs()) return [];
					$data = [$data->getDriveDir(), $data->getSystemDir()];
				}
			}
		}
		return array_filter($data, function($item) {
			return method_exists($item, 'isInterface') ? $item->isInterface(DirectoryInterface::class) : serviceClasses::isInterface($item, DirectoryInterface::class);
		});
	}

	// public function getAnnotations($entity) {
	// 	// return $entity->getAnnotations();
	// 	return $this->container->get(serviceCruds::class)->getAllAnnotations($entity, null, null, null, true);
	// }

	public function getBinaryAssociations($entity, $types = []) {
		return $this->container->get(serviceEntities::class)->getBinaryAssociations($entity, $types);
	}

	public function getRandomMedia($nb, $shortname = 'Image') {
		$image = $this->container->get(serviceImage::class)->getRepository()->findOneBySlug('vue-depuis-le-belvedere');
		$images = [];
		for ($i=0; $i < $nb; $i++) $images[] = $image;
		return $images;
	}

	public function getPagewebDefaultsValues($elements, $values) {
		if(is_string($values)) $values = [$values => $values];
		$items = !is_array($elements) ? [$elements] : $elements;
		foreach ($elements as $element) {
			if($element instanceOf Pageweb && is_object($element->getTempitem())) array_unshift($items, $element->getTempitem());
		}
		$defaults = [];
		foreach ($values as $name => $fieldnames) {
			$defaults[$name] = null;
			foreach ($items as $item) {
				if(in_array(gettype($defaults[$name]), ['NULL','unknown type'])) {
					foreach ((array)$fieldnames as $fieldname) {
						$found = false;
						$value = null;
						$getter = Inflector::camelize('get_'.$fieldname);
						if(method_exists($item, $getter)) {
							$value = $item->$getter();
							/** @see https://www.php.net/manual/fr/function.gettype.php */
							switch (gettype($value)) {
								case 'NULL':
								case 'unknown type':
									$found = false;
									break;
								case 'string':
									if(serviceTools::isStringOverTags($value)) { $defaults[$name] = $value; $found = true; }
									break;
								default:
									$defaults[$name] = $value;
									$found = true;
									break;
							}
						}
						if($found) break 2;
					}
				}
			}
		}
		return $defaults;
	}

	public function getDoublons($moreInformations = false, $id = null) {
		return $this->container->get(serviceBasedirectory::class)->findDoublons($moreInformations, $id);
	}

	public function route_exists($route) {
		return $this->container->get(serviceRoute::class)->routeExists($route);
	}

	public function getEntityUrl($entity, $action = 'show', $params = [], $referenceType = 0) {
		$action = strtolower($action);
		$actionNames = $this->getActionNames();
		if(in_array($action, $actionNames) && (is_object($entity) || (is_array($entity) && isset($entity['id'])))) {
			// $action = in_array($action, ['show','list','create','update','prefered','enable','softdelete','delete']) ? $action : 'show';
			$referenceType = in_array($referenceType, [UrlGenerator::ABSOLUTE_URL, UrlGenerator::ABSOLUTE_PATH, UrlGenerator::RELATIVE_PATH]) ? $referenceType : UrlGenerator::ABSOLUTE_URL;
			// if($entity instanceOf Basedirectory && $action === 'show') {
			// 	return $this->container->get(serviceRoute::class)->getRouter()->generate('wsa_directory_owner', ['id' => $entity->getId()], $referenceType);
			// }
			if(is_object($entity)) {
				$route = 'wsa_'.$entity->getShortname(true).'_'.$action;
				if($entity->isInstanceOf(Basedirectory::class) && $action === 'show') $route = 'wsa_directory_owner';
			} else {
				$route = 'wsa_'.strtolower($entity['shortname']).'_'.$action;
				if(in_array($entity['shortname'], ['Directory','Menu'])) $route = 'wsa_directory_owner';
			}
			if($this->container->get(serviceRoute::class)->routeExists($route)) {
				$valid = true;
				switch ($action) {
					// case 'list':
					// 	break;
					// case 'create':
					// 	break;
					case 'show':
						$params['id'] ??= is_object($entity) ? $entity->getId() : $entity['id'];
						break;
					case 'update':
						$params['id'] ??= is_object($entity) ? $entity->getId() : $entity['id'];
						break;
					case 'prefered':
						$params['id'] ??= is_object($entity) ? $entity->getId() : $entity['id'];
						$params['status'] ??= (integer)!(is_object($entity) ? $entity->getPrefered() : $entity['prefered']);
						break;
					case 'enable':
						$params['id'] ??= is_object($entity) ? $entity->getId() : $entity['id'];
						$params['status'] ??= (integer)!(is_object($entity) ? $entity->isEnabled() : $entity['enabled']);
						break;
					case 'softdelete':
						$params['id'] ??= is_object($entity) ? $entity->getId() : $entity['id'];
						$params['status'] ??= (integer)!(is_object($entity) ? $entity->isSoftdeleted() : serviceEntities::isSoftdeleted($entity['softdeleted']));
						break;
					case 'delete':
						$user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
						if(!($user instanceOf User) || !$user->isSuperadmin()) $valid = false;
							else $params['id'] ??= is_object($entity) ? $entity->getId() : $entity['id'];
						break;
				}
				if(empty($params['id'])) $valid = false;
				if($valid) return $this->container->get(serviceRoute::class)->getRouter()->generate($route, $params, $referenceType);
			}
		}
		return '#';
	}

	public function getActionNames() {
		// $data = $this->getActionsData();
		// return array_keys($data);
		return ['show' => 'show', 'list' => 'list', 'create' => 'create', 'update' => 'update', 'prefered' => 'prefered', 'enable' => 'enable', 'softdelete' => 'softdelete', 'delete' => 'delete'];
	}
	// public function getActionsData() {
	// 	$actions = new Actions();
	// }

	public function getEntityActions($entity, $actions = []) {
		return new Actions($entity, $actions, $this->container);
	}


	// public function cat_route_data() {
	// 	$serviceRoute = $this->container->get(serviceRoute::class);
	// 	$route = $serviceRoute->getRoute();
	// 	if($route !== $serviceRoute->getCatRoute()) return [];
	// 	return [
	// 		'_route' => $route,
	// 		'_route_params' => $serviceRoute->getCatRouteItems()
	// 	];
	// }

	public function cat_url($item) {
		if(is_string($item) && intval($item).'' === $item) $item = intval($item);
		if(is_string($item) && serviceEntities::isValidBddid($item, false)) $item = $this->serviceEntities->findByBddid($item);
			else if(is_string($item)) $item = $this->getRequestData(Item::class, 'findOneBy', [['slug' => $item]]);
			else if(is_integer($item)) $item = $this->getRequestData(Item::class, 'findOneBy', [['id' => $item]]);
		if(!($item instanceOf Item)) $item = null;
		if(empty($item)) return '#';
		return $this->container->get(serviceRoute::class)->generateCatUrl($item);
	}

	public function cat_path($item) {
		if(is_string($item) && intval($item).'' === $item) $item = intval($item);
		if(is_string($item) && serviceEntities::isValidBddid($item, false)) $item = $this->serviceEntities->findByBddid($item);
			else if(is_string($item)) $item = $this->getRequestData(Item::class, 'findOneBy', [['slug' => $item]]);
			else if(is_integer($item)) $item = $this->getRequestData(Item::class, 'findOneBy', [['id' => $item]]);
		if(!($item instanceOf Item)) $item = null;
		if(empty($item)) return '#';
		return $this->container->get(serviceRoute::class)->generateCatPath($item);
	}

	// public function getUrl($route, $params) {
	// 	return $this->container->get(serviceRoute::class)->routeExists($route);
	// }

	// public function getPath($route, $params) {
	// 	return $this->container->get(serviceRoute::class)->routeExists($route);
	// }





	public function setInSession($entity) {
		return $this->container->get(serviceKernel::class)->setInSession($entity);
	}

	public function clearInSession() {
		return $this->container->get(serviceKernel::class)->clearInSession();
	}

	public function cleanInSession() {
		return $this->container->get(serviceKernel::class)->cleanInSession();
	}

	public function hasInSession($entity, $asContextDate = true) {
		return $this->container->get(serviceKernel::class)->hasInSession($entity, $asContextDate);
	}

	public function getSessionEnds($entity) {
		return $this->container->get(serviceKernel::class)->getSessionEnds($entity);
	}

	public function getAllInSession($asEntites = true, $clean = true) {
		return $this->container->get(serviceKernel::class)->getAllInSession($asEntites, $clean);
	}

	public function removeFromSession($entity) {
		return $this->container->get(serviceKernel::class)->removeFromSession($entity);
	}

	public function referencementActive($media, Pageweb $pageweb = null) {
		return $this->container->get(serviceKernel::class)->referencementActive($media, $pageweb);
	}

	public function starttimer($time = 'NOW') {
		return new DateTime($time);
	}

	public function printtimer(DateTime $start) {
		$now = new DateTime('NOW');
		$diff = $start->diff($now);
		// return $this->getTotalInterval($diff, 'milliseconds');
		return $diff->format('%s,%F s'); // https://www.php.net/manual/fr/dateinterval.format.php
		// return $diff->format('%H : %I : %S.%F'); // https://www.php.net/manual/fr/dateinterval.format.php
	}

	public function getTotalInterval($interval, $type) {
		switch($type){
			case 'years':
				return $interval->format('%Y');
				break;
			case 'months':
				$years = $interval->format('%Y');
				$months = 0;
				if($years){
					$months += $years*12;
				}
				$months += $interval->format('%m');
				return $months;
				break;
			case 'days':
				return $interval->format('%a');
				break;
			case 'hours':
				$days = $interval->format('%a');
				$hours = 0;
				if($days){
					$hours += 24 * $days;
				}
				$hours += $interval->format('%H');
				return $hours;
				break;
			case 'minutes':
				$days = $interval->format('%a');
				$minutes = 0;
				if($days){
					$minutes += 24 * 60 * $days;
				}
				$hours = $interval->format('%H');
				if($hours){
					$minutes += 60 * $hours;
				}
				$minutes += $interval->format('%i');
				return $minutes;
				break;
			case 'milliseconds':
				$days = $interval->format('%a');
				$seconds = 0;
				if($days){
					$seconds += 24 * 60 * 60 * $days;
				}
				$hours = $interval->format('%H');
				if($hours){
					$seconds += 60 * 60 * $hours;
				}
				$minutes = $interval->format('%i');
				if($minutes){
					$seconds += 60 * $minutes;
				}
				$seconds += $interval->format('%s');
				$milliseconds = $seconds * 1000;
				return $milliseconds;
				break;
			default: // seconds
				$days = $interval->format('%a');
				$seconds = 0;
				if($days){
					$seconds += 24 * 60 * 60 * $days;
				}
				$hours = $interval->format('%H');
				if($hours){
					$seconds += 60 * 60 * $hours;
				}
				$minutes = $interval->format('%i');
				if($minutes){
					$seconds += 60 * $minutes;
				}
				$seconds += $interval->format('%s');
				return $seconds;
				break;
		}
	}


	////////////////////////////// FILTERS /////////////////

	public function getField($entity, $field) {
		if(isset($entity->$field)) return $entity->$field;
		$getter = 'get'.ucfirst($field);
		return method_exists($entity, $getter) ? $entity->$getter() : null;
	}

	/**
	 * Get first letter uppercase
	 * @return string
	 */
	public function getUcfirst($string) {
		return ucfirst($string);
	}

	/**
	 * Get formated text for textarea
	 * @return string
	 */
	public function getTextareaEscape($string) {
		return htmlentities($string, ENT_QUOTES | ENT_IGNORE, "UTF-8");
	}

	public function getShortname($entity, $lowercase = false) {
		$shortname = null;
		if($entity instanceOf ClassMetadata) {
			$class = new ReflectionClass($entity->getName());
			$shortname = $class->getShortName();
		} else if(is_object($entity)) {
			if(method_exists($entity, 'getShortName')) {
				$shortname = $entity->getShortName();
			} else {
				$class = new ReflectionClass(get_class($entity));
				$shortname = $class->getShortName();
			}
		} else if(is_string($entity)) {
			if(class_exists($entity)) {
				$class = new ReflectionClass($entity);
				$shortname = $class->getShortName();				
			} else {
				$shortname = $this->container->get(serviceEntities::class)->getShortnameByAnything($entity);
			}
		}
		return $lowercase ? strtolower($shortname) : $shortname;
	}

	/**
	 * Returns $default if $element is empty
	 * @param mixed $element
	 * @param mixed $default
	 * @return mixed
	 */
	public function isEmpty($element, $default = true, $nullIfStringEmptyOverTags = true) {
		if(empty($element)) return $default;
		$test = $element;
		if(is_string($element) && $nullIfStringEmptyOverTags) $test = serviceTools::getTextOrNullStripped($element);
		return empty($test) ? $default : $element;
	}

	/**
	 * Returns $default if $element is empty
	 * @param mixed $element
	 * @param mixed $default
	 * @return mixed
	 */
	public function hasText($element) {
		return strlen(serviceTools::getTextOrNullStripped($element)) > 0;
	}

	public function getMagnifiedText($text, $locale = null) {
		if($locale instanceOf Language) $locale = (string)$locale;
		if(!is_string($locale)) {
			$twigGlobals = $this->container->get('twig')->getGlobals();
			$sessionLocale = $this->container->get(serviceKernel::class)->getSession()->get('locale');
			$locale = isset($twigGlobals['currentLanguage']) ? (string)$twigGlobals['currentLanguage'] : $sessionLocale;
		}
		return serviceTools::magnifyText($text, $locale);
	}

	public function getStriptags($text) {
		return serviceTools::striptags($text);
	}

	public function getBrStriptags($text) {
		return serviceTools::br_striptags($text);
	}

	public function addOneDay(DateTime $date) {
		return $date->modify('+1 day');
	}

	public function arrayunique($array) {
		if(!is_array($array)) return $array;
		return array_unique($array);
	}

	public function getJsonDecode(string $string, $arrayize = true) {
		return json_decode($string, $arrayize);
	}

	/**
	 * Count number of words in $text
	 * @param string $text
	 * @param integer $nbletters = 2
	 * @return integer
	 */
	public function wordscount($text, $nbletters = 2) {
		return serviceTools::wordscount($text, $nbletters);
	}

	public function urlParamsCombine($params, $news, $removeIfSame = false) {
		if(empty($news)) return $params;
		foreach ($news as $key => $data) {
			if(!array_key_exists($key, $params) || (string)$data !== (string)$params[$key]) {
				$params[$key] = (string)$data;
			} else if($removeIfSame) {
				$params = array_filter($params, function ($v, $k) use ($key, $data) {
					return !($key === $k && (string)$data === (string)$v);
				}, ARRAY_FILTER_USE_BOTH);
			}
		}
		if(array_key_exists('nb', $params) && count($params) === 1) $params['reset'] = true;
		return $params;
	}

	public function getUrlnormalized($string) {
		FileManager::urlize($string,'-');
		return $string;
	}

	public function getUrldecode($string) {
		return urldecode($string);
	}

	public function isEntity($entity = null, $onlyInstantiable = false) {
		if(!is_object($entity)) return false;
		return $this->container->get(serviceEntities::class)->entityExists($entity, $onlyInstantiable);
	}

	public function isInstance($entity = null, $instances) {
		if(!is_object($entity)) return false;
		return method_exists($entity, 'isInstance') ? $entity->isInstance($instances) : serviceClasses::isInstance($entity, $instances);
	}

	public function isInterface($entity = null, $interfaces) {
		if(!is_object($entity)) return false;
		return method_exists($entity, 'isInterface') ? $entity->isInterface($interfaces) : serviceClasses::isInterface($entity, $interfaces);
	}

	public function isInstanceOrInterface($entity = null, $insterf) {
		if(!is_object($entity)) return false;
		return $this->isInstance($entity, $insterf) || $this->isInterface($entity, $insterf);
	}

	public function getFilteredValids($entities, $tests = []) {
		$array = false;
		if(!($entities instanceOf ArrayCollection) && is_array($entities)) {
			$array = true;
			$entities = new ArrayCollection($entities);
		}
		// if($entities instanceOf ArrayCollection) {
			$entities = $entities->filter(function($entity) use ($tests) {
				if(!$entity->isActive()) return false;
				foreach ($tests as $test) {
					if(preg_match('/^!/', $test)) {
						// negative test = false if NOT empty
						$test = preg_replace('/^!/', '', $test);
						$getter = 'get'.ucfirst($test);
						if(!empty($entity->$getter())) return false;
					} else {
						// positive test = false if empty
						$getter = 'get'.ucfirst($test);
						if(empty($entity->$getter())) return false;
					}
				}
				return true;
			});
		// }
		return $array ? $entities->toArray() : $entities;
	}



	public function isArray($data, $strict = false) {
		return $strict ? is_array($data) : is_iterable($data);
	}

	public function isString($data, $notStringIfLengthZero = false) {
		if(!is_string($data)) return false;
		return $notStringIfLengthZero ? strlen($data) > 0 : true;
	}

	public function isObject($data) {
		return is_object($data);
	}

	public function isInteger($data) {
		return is_integer($data);
	}

	public function isBoolean($data, $testValue = null) {
		if(is_bool($testValue)) return (boolean)$data === $testValue;
		return is_bool($data);
	}

	public function isDatetime($data) {
		return $data instanceOf DateTime;
	}

	public function removeKeys($array, $keys = []) {
		if(is_string($keys)) $keys = [$keys];
		if(is_array($array) && count($keys)) {
			$array = array_filter($array, function($key) use ($keys) { return !in_array($key, $keys); }, ARRAY_FILTER_USE_KEY);
		}
		return $array;
	}

	public function default_string($item, $default, $testString = false) {
		if($testString) $item = (string)$item;
		return is_string($item) && strlen($item) > 0 ? $item : $default;
	}

	public function default_integer($item, $default, $zeroCausesDefault = true) {
		if($zeroCausesDefault) {
			// $item = (integer)$item;
			// if($item === 0) $item = $default;
			if($item == null) $item = $default;
		}
		return is_integer($item) ? $item : $default;
	}

	public function default_array($item, $default = [], $emptyCausesDefault = true) {
		// $item = (array)$item;
		if($emptyCausesDefault && empty($item)) $item = $default;
		return is_array($item) || $item instanceOf Collection ? $item : $default;
	}

	/**
	 * Get a readable stripped sub string of html code
	 * @param string $string
	 * @return string
	 */
	public function googlemappize($string) {
		return "https://www.google.fr/maps/search/".preg_replace('/\\s+/', '+', $string);
	}




}