<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Service\serviceItem;
// UserBundle
use ModelApi\UserBundle\Entity\User;
// use ModelApi\UserBundle\Service\serviceGrants;
// DevprintBundle
// use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \ReflectionClass;
use \Exception;

class serviceRoute implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const CAT_ROUTE_NAME = 'website_pageweb_by_cat';

	protected $container;
	protected $router;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->router = $this->container->get('router');
		return $this;
	}


	/***********************************/
	/*** ROUTES
	/***********************************/
	// https://api.symfony.com/3.1/Symfony/Component/Routing/RouteCollection.html

	public function getRoute() {
		return $this->container->get(serviceKernel::class)->getRequestAttribute('_route');
	}

	public function getCatRoute() {
		return static::CAT_ROUTE_NAME;
	}

	public function isCatRoute() {
		return $this->getRoute() === $this->getCatRoute();
	}

	public function getRouteParams($magnify = false) {
		$params = $this->container->get(serviceKernel::class)->getRequestAttribute('_route_params');
		if(!$magnify) return $params;
		$params = array_filter($params, function ($value) { return null !== $value; });
		ksort($params);
		return $params;
	}

	// public function getCatRouteItems(Item $addItem = null) {
	// 	if($this->getRoute() !== static::CAT_ROUTE_NAME) return [];
	// 	$routeParams = $this->getRouteParams(true);
	// 	if(count($routeParams) <= 0) return [];
	// 	$dones = new ArrayCollection();
	// 	$items = $this->container->get(serviceItem::class)->getRepository()->findBySlug($routeParams);
	// 	foreach ($routeParams as $cat => $slug) {
	// 		$found = false;
	// 		foreach ($items as $item) {
	// 			if(!$dones->contains($item) && $slug === $item->getSlug()) {
	// 				if(!$item->isMenuable()) break;
	// 				$found = $routeParams[$cat] = $item;
	// 				$dones->add($item);
	// 				break;
	// 			}
	// 		}
	// 		if(empty($found) || !$found->isMenuable()) break;
	// 	}
	// 	if($addItem instanceOf Item) {
	// 		// Add or replace last item if add Item
	// 		$last = end($routeParams);
	// 		if($last !== $addItem) {
	// 			$keys = array_keys($routeParams, $last, true);
	// 			$key = end($keys);
	// 			$num = (integer)$key + ($last->isMenuable() ? 1 : 0);
	// 			$routeParams['cat'.$num] = $addItem;
	// 		}
	// 	}
	// 	return $routeParams;
	// }

	public function generateCatUrl($params) {
		return $this->generateCatPath($params, true);
	}

	public function generateCatPath($params, $absoluteUrl = false) {
		if($params instanceOf Item) $params = $params->getCats();
		return $this->getRouter()->generate(static::CAT_ROUTE_NAME, $params, $absoluteUrl);
	}

	public function getRouter() {
		return $this->router;
	}

	public function getRouteCollection() {
		return $this->getRouter()->getRouteCollection();
	}

	public function getAllRoutes() {
		return $this->getRouter()->getRouteCollection()->all();
	}

	public function routeExists($route) {
		$routes = $this->getAllRoutenames();
		return in_array($route, $routes);
	}

	public function getAllRoutenames() {
		$routes = $this->getAllRoutes();
		return array_keys($routes);
	}

	public function getTemplatableBundleRoutes(User $user = null, $aschoices = false) {
		$routes = [];
		$bundles = $this->container->get(serviceKernel::class)->getTemlatableBundleNames($user);
		foreach (array_keys($bundles) as $bundlename) $routes[$bundlename] = [];
		foreach ($this->getAllRoutes() as $route => $data) {
			if(isset($routes['WebsiteSiteBundle']) && preg_match('/^website_(?!admin|postfixtures)/', $route)) {
				// WebsiteSiteBundle
				$routes['WebsiteSiteBundle'][$route] = true === $aschoices ? $route : $data;
			} else if(isset($routes['WebsiteAdminBundle']) && preg_match('/^(website_admin|website_postfixtures|wsa_)/', $route)) {
				// WebsiteAdminBundle
				$routes['WebsiteAdminBundle'][$route] = true === $aschoices ? $route : $data;
			}
		}
		return $routes;
	}

	public function getTemplatableBundleRoutesForChoice(User $user = null) {
		return $this->getTemplatableBundleRoutes($user, true);
	}

	public function filterValidRoutes($routes) {
		$router = $this->getRouter();
		return array_filter($routes, function($route) use ($router) {
			return null !== $router->getRouteCollection()->get($route['route']);
		});
	}

	public function getLastOrDefaultUrl(Request $request = null, $defaultRoute = null, $params = []) {
		if(!($request instanceOf Request)) $request = $this->getRequest();
		if(!is_string($defaultRoute)) $defaultRoute = static::DEFAULT_ROUTE;
		$referer = $request->headers->get('referer');
		$last = null;
		foreach ($this->getRouteCollection() as $route) if(preg_match('/^\\/(?!_)/', $route->getPath())) {
			$pattern = preg_replace('#\\/#', '\\/', $route->getPath());
			$pattern = preg_replace('#\\\\/\\{\\w+\\}#', '(\\/\\w+)?', $pattern);
			$methods = array_map(function ($method) { return strtoupper($method); }, $route->getMethods());
			if(preg_match('#'.$pattern.'#', $referer) && in_array('GET', $methods)) {
				$last = $referer;
				break;
			}
		}
		return null !== $last ? $last : $this->getRouter()->generate($defaultRoute, $params);
	}



}