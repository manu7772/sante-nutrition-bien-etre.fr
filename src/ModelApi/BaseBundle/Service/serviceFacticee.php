<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;

// BaseBundle
use ModelApi\BaseBundle\Entity\Facticee;
use ModelApi\BaseBundle\Service\serviceEntities;
// AnnotBundle
use ModelApi\AnnotBundle\Annotation\BaseAnnotation;

// use \SplFileInfo;
use \ReflectionClass;
use \Exception;

class serviceFacticee {

	protected $container;
	protected $facticees;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->facticees = new ArrayCollection();
		return $this;
	}

	// public function createNew($data = []) {
	// 	if(!empty($facticee = $this->getFacticee($data))) {
	// 		$facticee->_resetData($data);
	// 	} else {
	// 		$facticee = new Facticee($data, $this);
	// 		$this->addFacticee($facticee);
	// 	}
	// 	return $facticee;
	// }

	public function getFacticee($data) {
		if($data instanceOf Facticee) {
			$this->addFacticee($data);
			return $data;
		}
		foreach ($this->facticees as $facticee) {
			if($data['bddid'] === $facticee->getBddid()) {
				$facticee->_updateData($data);
				return $facticee;
			}
		}
		$facticee = new Facticee($data, $this);
		$this->container->get(serviceEntities::class)->applyEvent($facticee, BaseAnnotation::postNew);
		return $facticee;
	}

	public function getRealEntity(Facticee $facticee) {
		return $this->container->get(serviceEntities::class)->findByBddid($facticee->getBddid());
	}


	public function getFacticees() {
		return $this->facticees;
	}

	public function addFacticee(Facticee $facticee) {
		if(!$this->hasFacticee($facticee)) $this->facticees->add($facticee);
		return $this;
	}

	public function removeFacticees() {
		foreach ($this->facticees as $facticee) {
			$this->removeFacticee($facticee);
		}
		return $this;
	}

	public function removeFacticee(Facticee $facticee) {
		$this->facticees->removeElement($facticee);
		unset($facticee);
	}

	public function hasFacticee(Facticee $facticee) {
		return $this->facticees->contains($facticee);
	}

}