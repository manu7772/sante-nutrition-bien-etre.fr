<?php
namespace ModelApi\BaseBundle\Service;

// use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
// https://symfony.com/doc/3.4/components/filesystem.html
use Doctrine\Common\Inflector\Inflector;
use Behat\Transliterator\Transliterator;
use Symfony\Component\Filesystem\Filesystem;
// use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\servicesBaseInterface;

// use \SplFileInfo;
use \ReflectionClass;
use \ReflectionProperty;
use \ReflectionMethod;
use \Exception;
use \DateTime;

class serviceSysfiles implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const GLOBAL_ACCESS = 0775;
	const GLOBAL_OWNER = 'apache';
	const GLOBAL_GROUP = 'WEB';
	const TMP_PATH = [__DIR__, '..', '..', '..', '..', 'web', 'tmp'];




	/********************************************************************************************************************/
	/*** FILE TOOLS
	/********************************************************************************************************************/

	public static function magnifyPath($path, $endSlash = false, $verify = false) {
		$path = preg_replace('#[\\/\\\]+#', DIRECTORY_SEPARATOR, $path);
		if($verify && !@file_exists($path)) {
			// throw new Exception("Error ".__METHOD__."(): path ".json_encode($path)." does not exist!", 1);
			return false;
		}
		// if(!$endSlash) $path = preg_replace('/[\\/\\\]+$/', '', $path);
		return preg_replace('#[\\/\\\]+#', DIRECTORY_SEPARATOR, $path.(false === $endSlash ? '' : DIRECTORY_SEPARATOR));
	}

	public static function getPathFiles($path, $search = '*') {
		$path = static::magnifyPath($path, true);
		// var_dump($path.$search);
		$files = array_filter(glob($path.$search), function ($item) {
			$info = pathinfo($item);
			return !preg_match('#^\\.#', $info['basename']);
		});
		// var_dump(glob($path.$search));
		return $files;
	}

	public static function splitPath($path, $splitExtension = true) {
		$preg = $splitExtension ? '/[\\.\\/\\\]+/' : '/[\\/\\\]+/';
		return preg_split($preg, $path, '-1', PREG_SPLIT_NO_EMPTY);
	}

	public static function preSeparator($path, $endSlash = false) {
		if(is_string($path) && strlen($path) > 0) $path = preg_match('/^\\'.DIRECTORY_SEPARATOR.'/', $path) ? $path : DIRECTORY_SEPARATOR.$path;
		return empty($path) ? null : static::magnifyPath($path, $endSlash);
	}

	public static function removeFileEverywhere($path, $filename) {
		$fileSystem = new Filesystem();
		$removed = false;
		foreach (static::getPathFiles($path) as $item) if (!@is_link($item)) {
			if (@is_dir($item)) {
				$removed = static::removeFileEverywhere(static::concatWithSeparators([$path, $item]), $filename);
			} else if (@is_file($item)) {
				if ($item === $filename) $fileSystem->remove($item);
			}
		}
		unset($fileSystem);
		return $removed;
	}

	// public static function Utf8_ansi(&$path) {
	// 	// return html_entity_decode(preg_replace('/\\d\\\\u[\\dA-Fa-f]{4}/i', "&#x\\1;", $path), ENT_NOQUOTES, 'UTF-8');

	// 	# $path = preg_replace('/\\d\\\\u([\da-fA-F]{4})/', '&#x\1;', $path);
	// 	// $utf8_ansi2 = array("\u00c0" => "À", "\u00c1" => "Á", "\u00c2" => "Â", "\u00c3" => "Ã", "\u00c4" => "Ä", "\u00c5" => "Å", "\u00c6" => "Æ", "\u00c7" => "Ç", "\u00c8" => "È", "\u00c9" => "É", "\u00ca" => "Ê", "\u00cb" => "Ë", "\u00cc" => "Ì", "\u00cd" => "Í", "\u00ce" => "Î", "\u00cf" => "Ï", "\u00d1" => "Ñ", "\u00d2" => "Ò", "\u00d3" => "Ó", "\u00d4" => "Ô", "\u00d5" => "Õ", "\u00d6" => "Ö", "\u00d8" => "Ø", "\u00d9" => "Ù", "\u00da" => "Ú", "\u00db" => "Û", "\u00dc" => "Ü", "\u00dd" => "Ý", "\u00df" => "ß", "\u00e0" => "à", "\u00e1" => "á", "\u00e2" => "â", "\u00e3" => "ã", "\u00e4" => "ä", "\u00e5" => "å", "\u00e6" => "æ", "\u00e7" => "ç", "\u00e8" => "è", "\u00e9" => "é", "\u00ea" => "ê", "\u00eb" => "ë", "\u00ec" => "ì", "\u00ed" => "í", "\u00ee" => "î", "\u00ef" => "ï", "\u00f0" => "ð", "\u00f1" => "ñ", "\u00f2" => "ò", "\u00f3" => "ó", "\u00f4" => "ô", "\u00f5" => "õ", "\u00f6" => "ö", "\u00f8" => "ø", "\u00f9" => "ù", "\u00fa" => "ú", "\u00fb" => "û", "\u00fc" => "ü", "\u00fd" => "ý", "\u00ff" => "ÿ");
	// 	// return strtr($path, $utf8_ansi2);
	// 	// $path = strtr(json_encode($path), $utf8_ansi2);
	// 	// return json_decode($path);
	// 	# $path = json_decode(preg_replace(['/\\w(\\\\u03[\\dA-Fa-f]{2})/i'], ['e&#x0301;'], json_encode($path)));
	// 	// return $path = json_decode(preg_replace(['/\\w(\\\\u03[\\dA-Fa-f]{2})/i'], ['é'], json_encode($path)));
	// 	// $path = json_decode(preg_replace(['/\\d\\\\u[\\dA-Fa-f]{4}/i'], ['e&#x0301;'], json_encode($path)));
	// 	// $path = htmlentities($path, ENT_NOQUOTES, 'UTF-8');
	// 	$path = json_decode(html_entity_decode(json_encode($path), ENT_NOQUOTES, 'UTF-8'));
	// }

	public static function cleanFileExtension(&$fileExtension) {
		if(!is_string($fileExtension) || empty($fileExtension)) throw new Exception("Error ".__METHOD__."(): file extension ".json_encode($fileExtension)." is not valid!", 1);
		return $fileExtension = preg_replace(['/^(\\.+)/','/(\\.+)/'], ['','.'], $fileExtension);
	}

	public static function getNewTempFilename($fileExtension) {
		return serviceTools::geUniquid('temp_file_'.serviceTools::getMicrotimeid().'@').'.'.static::cleanFileExtension($fileExtension);
	}

	public static function getUploadFileInstance($uploadFile = null, $fileExtension = null) {
		if (is_string($uploadFile) && file_exists($uploadFile)) {
			// URL of a file
			$file = new File($uploadFile);
			$uploadFile = new UploadedFile($uploadFile, $file->getFilename());
		} else if (is_string($uploadFile)) {
			// content of a file --> create tmp file
			return static::getUploadFileInstanceByTmpFile($uploadFile, static::cleanFileExtension($fileExtension));
		}
		return $uploadFile instanceOf UploadedFile ? $uploadFile : null;
	}

	public static function getUploadFileInstanceByTmpFile($content, $fileExtension, $filename = null) {
		static::cleanFileExtension($fileExtension);
		$filename = is_string($filename) && !empty($filename) ? $filename.'.'.$fileExtension : static::getNewTempFilename($fileExtension);
		$filepath = static::createFile(static::concatWithSeparators(static::TMP_PATH), $filename, $content);
		$file = new File($filepath);
		$uploadFile = new UploadedFile($filepath, $file->getFilename());
		return $uploadFile instanceOf UploadedFile ? $uploadFile : null;
	}

	public static function getUploadFileContentType($uploadFile) {
		$uploadFile = static::getUploadFileInstance($uploadFile);
		return $uploadFile instanceOf UploadedFile ? $uploadFile->getMimeType() : null;
	}

	public static function getSafeFilename($filename) {
		return transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $filename);
	}

	public static function concatWithSeparators($names, $endSlash = false) {
		$names = implode(DIRECTORY_SEPARATOR, $names);
		return static::magnifyPath($names, $endSlash);
	}

	public static function createPath($path) {
		if (!@file_exists($path)) {
			$fileSystem = new Filesystem();
			$fileSystem->mkdir($path);
			unset($fileSystem);
		}
		static::setFileGlobalAccess($path);
		return @file_exists($path) ? $path : false;
	}

	public static function createFile($path, $file, $content = null, $replace = false) {
		if (!@file_exists($path)) static::createPath($path);
		if(@file_exists($path)) {
			if(!preg_match('/\\/$/', $path)) $path = $path.DIRECTORY_SEPARATOR;
			$pathfile = $path.$file;
			if($replace) static::removeFile($pathfile);
			if (!@file_exists($pathfile) || $replace) {
				$fileSystem = new Filesystem();
				static::setFileGlobalAccess($path);
				$fileSystem->dumpFile($pathfile, $content);
				unset($fileSystem);
			}
			return @file_exists($pathfile) ? $pathfile : false;
		}
		return false;
	}

	public static function createDir($newdir) {
		if(!@file_exists($newdir)) {
			$fileSystem = new Filesystem();
			$Filesystem->mkdir($newdir, serviceTools::GLOBAL_ACCESS);
			unset($fileSystem);
			return $Filesystem->exists($newdir);
		}
		static::setFileGlobalAccess($newdir);
		return true;
	}

	public static function removeFile($path, $file = null) {
		if(!preg_match('/\\/$/', $path)) $path = $path.DIRECTORY_SEPARATOR;
		$pathfile = $path.$file;
		$fileSystem = new Filesystem();
		if($fileSystem->exists($pathfile)) {
			$fileSystem->remove($pathfile);
		}
		unset($fileSystem);
		return !@file_exists($pathfile);
	}

	public static function appendToFile($path, $file, $data) {
		// static::createFile($path, $file);
		$fileSystem = new Filesystem();
		$pathfile = $path.$file;
		$fileSystem->appendToFile($pathfile, $data);
		static::setFileGlobalAccess($path);
		unset($fileSystem);
		return $path.$file;
	}

	public static function setFileGlobalAccess($pathfile, $recursive = true) {
		$fileSystem = new Filesystem();
		if($fileSystem->exists($pathfile)) {
			$fileSystem->chmod($pathfile, static::GLOBAL_ACCESS, 0000);
			$fileSystem->chown($pathfile, static::GLOBAL_OWNER, $recursive);
			$fileSystem->chgrp($pathfile, static::GLOBAL_GROUP, $recursive);
			unset($fileSystem);
			return true;
		}
		unset($fileSystem);
		return false;
	}

	public static function getTreeAsArray($path, $id_prefix = null) {
		$id = 0;
		$tree_id = 0;
		$id_prefix .= null === $id_prefix ? '' : '_';
		$tree = [];
		$items = static::getPathFiles($path);
		foreach ($items as $item) {
			$new_id = $id_prefix.$id++;
			$exp = explode('_', $new_id);
			$opended = count($exp) < 3;
			$pathinfo = pathinfo($item);
			$name = $pathinfo['basename'];
			$tree[$tree_id]['id'] = $new_id;
			$tree[$tree_id]['text'] = $name;
			$tree[$tree_id]['type'] = is_dir($item) ? 'Directory' : 'File';
			$tree[$tree_id]['icon'] = is_dir($item) ? 'fa fa-folder' : 'fa fa-file-o';
			// $tree[$tree_id]['state'] = ['opended' => $opended, 'disabled' => false, 'selected' => false];
			// $tree[$tree_id]['data'] = ['pathinfo' => $pathinfo,'full_path' => $item,'realpath' => realpath($item),'link' => is_link($item),'dir' => is_dir($item),'file' => is_file($item),'readable' => is_readable($item),'writable' => is_writable($item),'executable' => is_executable($item)];
			// var_dump($tree[$item]['data']['pathinfo']);
			if (@is_dir($item) && !@is_link($item)) {
				$tree[$tree_id]['children'] = static::getTreeAsArray($item, $tree[$tree_id]['id']);
			}
			$tree_id++;
		}
		return $tree;
	}



}

