<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\User;


use \ReflectionProperty;
use \ReflectionClass;
use \Exception;
use \DateTime;

class serviceForms implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	protected $serviceEntities;
	protected $BaseAnnotateType;
	protected $formFactory;
	protected $router;


	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->BaseAnnotateType = $this->container->get(BaseAnnotateType::class);
		$this->formFactory = $this->container->get('form.factory');
		$this->router = $this->container->get('router');
		return $this;
	}

	
	// /***********************************/
	// /*** FORMS
	// /***********************************/

	public static function isAnnotateType($typeClass) {
		$RC = new ReflectionClass($typeClass);
		return $RC->isSubclassOf(BaseAnnotateType::class);
	}

	/**
	 * Get Type classname for entity (priority to AnnotateType)
	 * @param mixed $entity
	 * @param string $action = null
	 * @param boolean $priorityToAnnotate = true
	 * @return string | null
	 */
	public function getFormClass($entity, $action = null, $priorityToAnnotate = true) {
		$classname = $this->serviceEntities->getClassnameByAnything($entity);
		if(null == $classname) {
			if(defined(get_class($entity).'::ENTITY_FORM')) return $entity::ENTITY_FORM;
			return null;
		}
		if(true === $priorityToAnnotate) {
			$formClass = preg_replace('/\\\\Entity\\\\/', '\\Form\\Type\\', $classname).'AnnotateType';
			if(class_exists($formClass) && $this->isAnnotateType($formClass)) return $formClass;
		}
		if(!is_string($action)) return null;
		$formClass = preg_replace('/\\\\Entity\\\\/', '\\Form\\Type\\', $classname).ucfirst($action).'Type';
		if(class_exists($formClass)) return $formClass;
		return null;
	}

	/**
	 * Get new create form
	 * @param object $entity
	 * @param Tier $user = null
	 * @param string $formContexts = null // only form_contexts
	 * @param array $params = []
	 * @return Form
	 * PARAMS:
	 * 		ENTITY_CREATE_TYPE = classname of Type
	 * 		ACTION_ROUTE = route for action
	 * 		SUBMITS = submit buttons
	 */
	public function getCreateForm($classname, Tier $user = null, $formContexts = null, $params = []) {
		$entity = is_object($classname) ? $classname : null;
		$classname = is_object($classname) ? get_class($classname) : $this->serviceEntities->getClassnameByAnything($classname);
		$contexts = $this->BaseAnnotateType->getFormAllContexts($user, $formContexts);
		// echo('<pre>'); var_dump($contexts); echo('</pre>');
		$manager = $this->serviceEntities->getEntityService($classname);
		if(empty($entity)) $entity = $manager instanceOf servicesBaseEntityInterface ? $manager->createNew() : $this->serviceEntities->createNew($classname);

		$params['ACTION_ROUTE'] ??= ['route' => 'wsa_'.$entity->getShortname(true).'_post', 'params' => ['context' => BaseAnnotateType::filterFormContexts($contexts, 'string')]];
		$action_route = is_array($params['ACTION_ROUTE']) ? $this->router->generate($params['ACTION_ROUTE']['route'], $params['ACTION_ROUTE']['params'] ?? []) : $params['ACTION_ROUTE'];

		if(!($params['SUBMIT'] ?? true)) $params['SUBMITS'] = false;
		$action = 'create';
		$typeClassname = $this->getFormClass($entity, $action, true);
		if($this->isAnnotateType($typeClassname)) {
			return $this->formFactory->create(
				$typeClassname,
				$entity,
				array(
					'submit' => $params['SUBMIT'] ?? true,
					'submits' => $params['SUBMITS'] ?? true,
					'method' => $params['METHOD'] ?? 'POST',
					// 'action' => $this->router->generate($params['ACTION_ROUTE'] ?? 'wsa_'.$entity->getShortname(true).'_post', ['context' => BaseAnnotateType::filterFormContexts($contexts, 'string')]),
					'action' => $action_route,
					'cruds_action' => $action,
					'cruds_context' => $contexts,
					'large_footer' => is_bool($params['LARGE_FOOTER'] ?? null) ? $params['LARGE_FOOTER'] : true,
					'hidden_data' => isset($params['HIDDEN_DATA']) ? $params['HIDDEN_DATA'] : null,
				)
			);
		} else {
			method_exists($manager, 'preCreateForForm') ? $manager->preCreateForForm($entity) : $this->serviceEntities->preCreateForForm($entity);
			return $this->formFactory->create(
				$typeClassname,
				$entity,
				array(
					// 'submit' => true,
					// 'submits' => $params['SUBMITS'],
					'method' => 'POST',
					// 'action' => $this->router->generate($params['ACTION_ROUTE'] ?? 'wsa_'.$entity->getShortname(true).'_post', ['context' => BaseAnnotateType::filterFormContexts($contexts, 'string')]),
					'action' => $action_route,
				)
			);
		}
		return null;
	}


	/**
	 * Get update form
	 * @param object $entity
	 * @param Tier $user = null
	 * @param string $formContexts = null // only form_contexts
	 * @param strnig $method = 'put'
	 * @param array $params = []
	 * @return Form
	 */
	public function getUpdateForm($entity, Tier $user = null, $formContexts = null, $method = 'put', $params = []) {
		if(empty($entity->getId())) throw new Exception("Error ".__METHOD__."(): entity is new (has no ID), not for update!", 1);
		$contexts = $this->BaseAnnotateType->getFormAllContexts($user, $formContexts);
		// echo('<pre>'); var_dump($contexts); echo('</pre>');
		$manager = $this->serviceEntities->getEntityService($entity->getClassname());

		$params['ACTION_ROUTE'] ??= ['route' => 'wsa_'.$entity->getShortname(true).'_'.strtolower($method), 'params' => ['id' => $entity->getId(), 'context' => BaseAnnotateType::filterFormContexts($contexts, 'string')]];
		$action_route = is_array($params['ACTION_ROUTE']) ? $this->router->generate($params['ACTION_ROUTE']['route'], $params['ACTION_ROUTE']['params'] ?? []) : $params['ACTION_ROUTE'];

		if(!($params['SUBMIT'] ?? true)) $params['SUBMITS'] = false;
		$action = 'update';
		$typeClassname = $this->getFormClass($entity, $action, true);
		if($this->isAnnotateType($typeClassname)) {
			return $this->formFactory->create(
				$typeClassname,
				$entity,
				array(
					'submit' => $params['SUBMIT'] ?? true,
					'submits' => $params['SUBMITS'] ?? true,
					'method' => strtoupper($method),
					// 'action' => $this->router->generate($params['ACTION_ROUTE'] ?? 'wsa_'.$entity->getShortname(true).'_'.strtolower($method), array('id' => $entity->getId(), 'context' => BaseAnnotateType::filterFormContexts($contexts, 'string'))),
					'action' => $action_route,
					'cruds_action' => $action,
					'cruds_context' => $contexts,
					'large_footer' => is_bool($params['LARGE_FOOTER'] ?? null) ? $params['LARGE_FOOTER'] : true,
					'hidden_data' => isset($params['HIDDEN_DATA']) ? $params['HIDDEN_DATA'] : null,
				)
			);
		} else {
			method_exists($manager, 'preUpdateForForm') ? $manager->preUpdateForForm($entity) : $this->serviceEntities->preUpdateForForm($entity);
			return $this->formFactory->create(
				$typeClassname,
				$entity,
				array(
					'submit' => true,
					'submits' => true,
					'method' => strtoupper($method),
					// 'action' => $this->router->generate($params['ACTION_ROUTE'] ?? 'wsa_'.$entity->getShortname(true).'_'.strtolower($method), array('id' => $entity->getId(), 'context' => BaseAnnotateType::filterFormContexts($contexts, 'string'))),
					'action' => $action_route,
					'large_footer' => is_bool($params['LARGE_FOOTER'] ?? null) ? $params['LARGE_FOOTER'] : true,
					'hidden_data' => isset($params['HIDDEN_DATA']) ? $params['HIDDEN_DATA'] : null,
				)
			);
		}
	}



}