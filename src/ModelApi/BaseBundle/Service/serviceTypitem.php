<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
use ModelApi\BaseBundle\Entity\Blog;
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceKernel;
// CrudsBundle
use ModelApi\CrudsBundle\Service\serviceCruds;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// AnnotBundle
use ModelApi\AnnotBundle\Annotation\Typitem as TypitemAnnot;

// use \DateTime;
use \ReflectionClass;

class serviceTypitem implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Typitem::class;

	protected $container;
	protected $serviceEntities;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		return $this;
	}


	// public function findByTier(Tier $tier = null, $asArray = false) {
	// 	return $this->getRepository()->findAll();
	// }

	public function getTypitemsDescriptions($classnames = null, $refresh = false) {
		if($this->container->get(serviceKernel::class)->isDev()) $refresh = true;
		if(empty($classnames)) {
			$classnames = [];
			foreach ($this->serviceEntities->getEntitiesNames(true) as $classname => $shortname) {
				$classnames[] = $classname;
			}
		} else {
			if(!is_array($classnames)) $classnames = [$classnames];
			foreach ($classnames as $key => $item) {
				if(is_object($item)) $classnames[$key] = get_class($item);
			}
		}
		$classnames = array_unique($classnames);
		$groupNames = $this->container->get(serviceCache::class)->getCacheData(
			'typitem_groupnames_'.md5(implode('|', $classnames)), // Cache ID
			function() use ($classnames) {
				$groupNames = [];
				foreach ($classnames as $classname) {
					$RC = new ReflectionClass($classname);
					$annots = serviceClasses::getAllAnnotations($classname, [TypitemAnnot::class], ['property']);
					foreach ($annots['properties'] as $field => $typitems) {
						foreach ($typitems as $typitem) if($typitem instanceOf TypitemAnnot) {
							$groupNames[$RC->getShortname().'.'.$field] = [
								'groups' => $typitem->groups,
								'description' => $typitem->description,
							];
						}
					}
				}
				return $groupNames;
			},
			serviceCruds::CACHE_FOLDER,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
		// echo('<pre>');var_dump($groupNames);die('</pre>');
		return $groupNames;
	}

	public function getGroupNames($classnames = null, $refresh = false) {
		$groupNames = [];
		foreach ($this->getTypitemsDescriptions($classnames, $refresh) as $key => $groupName) $groupNames[$groupName['description']] = reset($groupName['groups']);
		return $groupNames;
	}

	public function insertGroupsChoices(Typitem $typitem) {
		$groupNames = $this->getGroupNames(null, false);
		$typitem->setGroupitemChoices($groupNames);
		return $this;
	}

	// public function getByGroup($groupNames) {
	// 	return $this->getRepository()->findByGroupitem($groupNames);
	// }


	public function getGroupedTypitems($repo_method_or_collection = 'findAll', $refresh = false) {
		if(!($repo_method_or_collection instanceOf ArrayCollection)) {
			$repo = $this->getRepository();
			$repo_method_or_collection = method_exists($repo, $repo_method_or_collection) ? $repo_method_or_collection : 'findAll';
			$repo_method_or_collection = $repo->$repo_method_or_collection();
		}
		$descriptions = $this->getTypitemsDescriptions(null, $refresh);
		foreach ($descriptions as $name => $description) {
			$descriptions[$name]['entities'] = new ArrayCollection();
			foreach ($repo_method_or_collection as $typitem) {
				if(in_array($typitem->getGroupitem(), $description['groups']) && !$descriptions[$name]['entities']->contains($typitem)) $descriptions[$name]['entities']->add($typitem);
			}
		}
		return $descriptions;
	}

}