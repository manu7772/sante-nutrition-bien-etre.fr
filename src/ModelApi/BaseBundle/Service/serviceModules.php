<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

// use \SplFileInfo;
use \ReflectionClass;
use \Exception;

class serviceModules {

	protected $container;
	protected $parameterBag;
	protected $modules;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->parameterBag = $this->container->getParameterBag();
		$this->loadModules(true);
		return $this;
	}


	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString() {
		return serviceClasses::getShortname(static::class);
	}

	public function getModules($onlyEnabled = true) {
		if(!$onlyEnabled) return $this->modules;
		return array_filter($this->modules, function($module) {
			return $module['enabled'];
		});
	}

	public function getModule($name, $onlyEnabled = true) {
		$this->exceptionIfNotExists($name);
		$modules = $this->getModules($onlyEnabled);
		if(isset($modules[$name])) return $modules[$name];
		return null;
	}

	public function getModulesNames($onlyEnabled = true) {
		$modules = $this->getModules($onlyEnabled);
		return array_keys($modules);
	}

	public function getModulesStatus() {
		$modules = [];
		foreach ($this->modules as $name => $data) $modules[$name] = $data['enabled'];
		return $modules;
	}

	public function isModuleEnabled($name) {
		$this->exceptionIfNotExists($name);
		$module = $this->getModule($name);
		return $module['enabled'];
	}

	public function enableModule($name) {
		$this->exceptionIfNotExists($name);
		if(isset($this->modules[$name])) return $this->modules[$name]['enabled'] = true;
	}

	public function disableModule($name) {
		$this->exceptionIfNotExists($name);
		if(isset($this->modules[$name])) return $this->modules[$name]['enabled'] = false;
	}

	protected function exceptionIfNotExists($name) {
		if(isset($this->modules[$name])) return;
		throw new Exception("Error ".__METHOD__."(): module ".json_encode($name)." does not exist!", 1);
	}

	protected function loadModules($exceptionIfErrors = true, $reset = false) {
		if(!$this->parameterBag->has('modules')) {
			if($exceptionIfErrors) throw new Exception("Error ".__METHOD__."(): parameter ".json_encode('modules')." does not exist!", 1);
			return false;
		}
		if(!isset($this->modules) || !is_array($this->modules) || $reset) $this->modules = $this->parameterBag->get('modules');
		foreach ($this->modules as $name => $data) {
			if(!isset($data['enabled']) || !is_bool($data['enabled'])) {
				if($exceptionIfErrors) throw new Exception("Error ".__METHOD__."(): module ".json_encode($name)." needs to have ".json_encode("enabled")." parameter with boolean value!", 1);
				return false;
			}
		}
		return true;
	}

	/**
	 * Get service parameter from ParameterBag
	 * @param mixed $service (service or service name)
	 * @param string $name = null (separate path with . or /)
	 * @param mixed $defaultIfNotFound = null 
	 * @param boolean $exceptionIfErrors = false
	 * @return mixed
	 */
	public function getServiceParameter($service, $name = null, $defaultIfNotFound = null, $exceptionIfErrors = false) {
		$serviceName = is_object($service) ? $service->__toString() : $service;
		if(!$this->parameterBag->has('services')) {
			if($exceptionIfErrors) throw new Exception("Error ".__METHOD__."(): no services parameters!", 1);
			return $defaultIfNotFound;
		}
		$param = $this->parameterBag->get('services');
		if(isset($param[$serviceName])) {
			$param = $param[$serviceName];
		} else {
			if($exceptionIfErrors) throw new Exception("Error ".__METHOD__."(): service ".json_encode($service->__toString())." parameters does not exist!", 1);
			return $defaultIfNotFound;
		}
		if(empty($name) || !is_string($name)) return $param;
		$names = preg_split('#[\\.\\/]#', $name);
		$idx = 0;
		foreach ($names as $name) {
			if(!empty($name)) {
				if(preg_match('#'.$serviceName.'#i', $name) && $idx < 1) continue;
				if(!isset($param[$name])) {
					if($exceptionIfErrors) throw new Exception("Error ".__METHOD__."(): service ".json_encode($service->__toString())." parameter name ".json_encode($name)." not found!", 1);
					return $defaultIfNotFound;
				}
				$param = $param[$name];
				$idx++;
			}
		}
		// echo('<pre><h3>Params '.json_encode($name).' for '.json_encode($serviceName).':</h3><p>');var_dump($param);echo('</p></pre>');
		return $param;
	}

	public function getServiceParameters($service) {
		return $this->getServiceParameter($service);
	}

}