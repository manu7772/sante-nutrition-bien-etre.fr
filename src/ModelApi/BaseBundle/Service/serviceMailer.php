<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Service\serviceItem;
// UserBundle
use ModelApi\UserBundle\Entity\User;
// use ModelApi\UserBundle\Service\serviceGrants;
// DevprintBundle
// use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \Swift_Mailer;
use \Swift_Message;
use \ReflectionClass;
use \Exception;

/**
 * @see https://symfony.com/doc/3.4/email.html
 */
class serviceMailer implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const HTML_FORMAT = 'text/html';
	const TEXT_FORMAT = 'text/plain';

	protected $container;
	protected $mailer;
	protected $templating;
	protected $mail_pool;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->mailer = $this->container->get('mailer');
		// http://man.hubwiz.com/docset/Symfony.docset/Contents/Resources/Documents/packagist_docset/Symfony/Bundle/TwigBundle/TwigEngine.html
		$this->templating = $this->container->get('templating');
		$this->mail_pool = $this->getServiceParameter('mail_pool');
		return $this;
	}

	public static function getValidFormats() {
		return [
			static::HTML_FORMAT,
			static::TEXT_FORMAT,
		];
	}

	public static function regularizeFormat(&$format = null) {
		switch (strtolower($format)) {
			case static::HTML_FORMAT:
				$format = static::HTML_FORMAT;
				break;
			case static::TEXT_FORMAT:
				$format = static::TEXT_FORMAT;
				break;
			default:
				$format = static::HTML_FORMAT;
				break;
		}
	}

	public function sendMessage(User $to, $globals, $from = null, $format = null, $template = null) {
		static::regularizeFormat($format);
		if($template instanceOf Twig) {
			// Get pathname of entity
		}
		if(empty($template)) {
			$template = $format === static::HTML_FORMAT ? 'WebsiteSiteBundle:Email:basic.html.twig' : 'WebsiteSiteBundle:Email:basic.txt.twig';
		}
		if(!$this->templating->exists($template)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): template ".json_encode($template)." does not exist!", 1);
		$globals['subject'] = strip_tags(nl2br($globals['subject']));
		if(empty($globals['subject'])) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): subject is empty!", 1);
		if(empty($globals['text'])) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): text is empty!", 1);
		$globals['user'] ??= $to;
		$html_view = $this->templating->render($template, $globals);
		// FROM
		// $from ??= ['emmanuel@aequation.fr' => 'Emmanuel Aequation'];
		$entreprise = $this->container->get(serviceContext::class)->getEntreprise();
		$from ??= [$entreprise->getEmail() => $entreprise->getName()];
		// MESSAGE
		$message = new Swift_Message($globals['subject']);
		$message->setFrom($from)
			->setTo($to->getEmail())
			->setBody($html_view, $format)
			;
		$txt_view = '';
		$message->addPart($txt_view);
		// SEND
		$this->mailer->send($message);
		return $this;
	}


}