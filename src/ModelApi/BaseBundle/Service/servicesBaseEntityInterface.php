<?php
namespace ModelApi\BaseBundle\Service;

use ModelApi\BaseBundle\Service\servicesBaseInterface;

interface servicesBaseEntityInterface extends servicesBaseInterface {

	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString();

	public function getRepository();

	public function getModel($classname = null);

	public function createNew($options = [], callable $beforePostNewEventClosure = null);

	public function delete($entity, $flush = true, $forceDelete = false);

	public function undelete($entity, $flush = true);

	public function getServiceModules();

	public function getModelForCreate($classname, $groups = []);

	public function setModelForCreate($entity);

	public function removeModelForCreate($classname);

	/**
	 * Get service parameter from ParameterBag
	 * @param string $name = null (separate path with . or /)
	 * @param mixed $defaultIfNotFound = null
	 * @param boolean $exceptionIfNoSM = true
	 * @param boolean $exceptionIfErrors = false
	 * @return mixed
	 */
	public function getServiceParameter($name = null, $defaultIfNotFound = null, $exceptionIfNoSM = true, $exceptionIfErrors = false);

	public function getServiceParameters();

}

