<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\HttpKernel\Bundle\Bundle;

// BaseBundle
use ModelApi\BaseBundle\Component\Preference;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Traits\Preferences;

use \ReflectionClass;
use \Exception;

class servicePreference implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const PREFS_PATH_SEPARATOR = '|';
	const BASE_NAME = 'preference';
	const NEW_NAME = 'new__';
	const CREATE_NAME = 'create__';
	const ORIGIN_NAME = 'origin__';
	const TRAIT_NAME = Preferences::class;

	protected $container;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		return $this;
	}


	public static function equivToBoolean($value = null) {
		return in_array($value, ['1','on','ON',1,true,'O','o','true']);
	}

	public static function isEquivToBoolean($value = null) {
		return in_array($value, ['1','0',1,0,'on','off','ON','OFF','true','false','TRUE','FALSE',true,false]);
	}

	public static function stringToArray(&$value = null) {
		$value = preg_split('/[\\n\\r]+/', $value);
		$value = array_filter($value, function($val) { return !empty($val); });
		return $value;
	}

	/***********************************/
	/*** CHECK PREFERENCE(S)
	/***********************************/

	public function checkPreferences($entity, $reset = false, $data = null) {
		if(!static::isPreferences($entity)) throw new Exception("Error ".__METHOD__."(): Item ".json_encode($bddid)." has no Preference data!", 1);
		$old_prefs = is_array($data) && count($data) ? $data : $entity->getPreferences(true)->toArray(true);
		$preferences = (boolean)$reset ? null : $old_prefs;
		$entities_parameters = $this->container->getParameter('entities_parameters');
		$shornames = $this->container->get(serviceClasses::class)->getParentClasses($entity, true, true, true, false);
		foreach ($shornames as $shortname) {
			if(isset($entities_parameters[$shortname]) && isset($entities_parameters[$shortname]['preferences'])) {
				if(empty($preferences)) {
					$preferences = $entities_parameters[$shortname]['preferences'];
				} else {
					$preferences = array_replace_recursive($entities_parameters[$shortname]['preferences'], $preferences);
				}
			}
		}
		$setter = 'setPreferences';
		$entity->$setter(new Preference(static::BASE_NAME, $preferences));
		$new_prefs = $entity->getPreferences(true)->toArray(true);
		return json_encode($old_prefs) !== json_encode($new_prefs);
	}

	/***********************************/
	/*** GET PREFERENCE(S)
	/***********************************/

	public static function isPreferences($entity) {
		return serviceClasses::hasTraits($entity, [static::TRAIT_NAME]);
	}

	public function getPreference($string, $bddid = null, $getValues = false) {
		if(empty($bddid)) {
			if(!preg_match('/^bddid@[\\w-]+\\|[\\w\\/-]+$/', $string)) throw new Exception("Error ".__METHOD__."(): bddid parameter not found, neither in ".json_encode($string)."!", 1);
			$data = explode('|', $string);
			$bddid = reset($data);
			$string = next($data);
		}
		// if(!($bddid)) throw new Exception("Error ".__METHOD__."(): bddid is not valid!", 1);
		$item = $this->container->get(serviceEntities::class)->findByBddid($bddid);
		if(!is_object($item)) throw new Exception("Error ".__METHOD__."(): Item not found with bddid ".json_encode($bddid)."!", 1);
		if(!static::isPreferences($item)) throw new Exception("Error ".__METHOD__."(): Item ".json_encode($bddid)." has no Preference data!", 1);
		$preference = $item->getPreference($string);
		return $getValues ? $preference->toArray() : $preference;
	}


	/***********************************/
	/*** FROM REQUEST
	/***********************************/

	public function fromRequest(Request $request, Tier $item) {
		$data = $request->request->all();
		$compiled = [];
		foreach ($data as $name => $new) {
			if(preg_match('/^'.static::NEW_NAME.DIRECTORY_SEPARATOR, $name)) {
				$path = preg_replace('/^'.static::NEW_NAME.DIRECTORY_SEPARATOR, '', $name);
				$compiled[$path] = [];
				$compiled[$path]['new'] = $new;
				$compiled[$path]['origin'] = $data[static::ORIGIN_NAME.$path];
				$compiled[$path]['modified'] = $compiled[$path]['new'] != $compiled[$path]['origin'];
				$compiled[$path]['preference'] = $item->getPreferenceByPath($path);
				if($compiled[$path]['preference'] instanceOf Preference) {
					$compiled[$path]['error'] = $compiled[$path]['preference']->getPath() != $path;
				} else {
					$compiled[$path]['error'] = true;
				}
			}
			if(preg_match('/^'.static::ORIGIN_NAME.DIRECTORY_SEPARATOR, $name)) {
				$path = preg_replace('/^'.static::ORIGIN_NAME.DIRECTORY_SEPARATOR, '', $name);
				if(!isset($compiled[$path])) {
					$compiled[$path] = [];
					$compiled[$path]['new'] = $new;
					// $compiled[$path]['new'] = isset($data[static::NEW_NAME.$path]) ? $data[static::NEW_NAME.$path] : false;
					$compiled[$path]['modified'] = true;
					$compiled[$path]['preference'] = $item->getPreferenceByPath($path);
					if($compiled[$path]['preference'] instanceOf Preference) {
						$compiled[$path]['error'] = $compiled[$path]['preference']->getPath() != $path;
					} else {
						$compiled[$path]['error'] = true;
					}
				}
			}
		}
		foreach ($compiled as $path => $value) {
			if($compiled[$path]['preference']->isBoolean()) {
				if(isset($compiled[$path]['origin'])) $compiled[$path]['origin'] = static::equivToBoolean($compiled[$path]['origin']);
				if(isset($compiled[$path]['new'])) $compiled[$path]['new'] = static::equivToBoolean($compiled[$path]['new']);
			}
			if(!isset($compiled[$path]['origin']) && $compiled[$path]['preference'] instanceOf Preference) {
				$compiled[$path]['origin'] = $compiled[$path]['new'];
				$compiled[$path]['new'] = $compiled[$path]['preference']->isBoolean() ? !static::equivToBoolean($compiled[$path]['origin']) : $compiled[$path]['origin'];
			}
		}
		$success = false;
		foreach ($compiled as $path => $data) if($data['modified']) {
			if($data['error']) {
				if($data['preference'] instanceOf Preference) {
					throw new Exception("Error while registering Preferences. One Preference not corresponding: path was ".json_encode(Preference::getSlashedPath($path))." and found ".json_encode($data['preference']->getPathSlached())."!", 1);
				} else {
					throw new Exception("Error while registering Preferences. One Preference not foud: ".json_encode(Preference::getSlashedPath($path))."!", 1);
				}
			}
			$data['preference']->setValue($data['new']);
			$success = true;
		}
		return $success;
	}




}