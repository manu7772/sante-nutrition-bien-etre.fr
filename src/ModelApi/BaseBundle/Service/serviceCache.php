<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\DeserializationContext;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceCacheInterface;
use ModelApi\BaseBundle\Service\servicesBaseInterface;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Service\serviceBasedirectory;

// use ModelApi\AnnotBundle\Annotation\CacheData;
// use ModelApi\AnnotBundle\Annotation\HasCache;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \Symfony\Component\Finder\Finder;

use \ReflectionClass;
use \DateTime;

/**
 * @see https://symfony.com/doc/3.4/components/cache.html
 * @see https://github.com/symfony/symfony/blob/3.4/src/Symfony/Component/Cache/Adapter/FilesystemAdapter.php
 * @see https://symfony.com/doc/3.4/components/cache/cache_pools.html
 */
class serviceCache implements servicesBaseInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const ENTITY_CLASS = Item::class;
	const FSCACHE_PREFIX = "cached";
	const CACHE_LIFETIME = 900; // 10 min.
	const CACHE_MEDLIFE = 3600; // 1 hour
	const CACHE_LONGLIFE = 3600 * 24; // 1 day
	const CACHE_VERYLONGLIFE = 3600 * 24 * 7; // 1 week
	const CACHE_HIGH_LIFE = 3600 * 24 * 30; // 1 month
	const CACHE_INFINITE = 0; // Infinite life!
	const CACHE_REFRESH_ON_DEV = true;

	const NO_CONTEXT = 0;
	const CONTEXT_LANG = 1;

	protected $container;
	protected $serializer;
	protected $serviceKernel;
	protected $devRefresh;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serializer = $this->container->get('jms_serializer');
		$this->serviceKernel = $this->container->get(serviceKernel::class);
		$this->devRefresh = static::CACHE_REFRESH_ON_DEV && $this->serviceKernel->isDev();
		return $this;
	}

	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString() {
		return serviceClasses::getShortname(static::class);
	}


	public function getSerializer() {
		return $this->serializer;
	}

	public function getNormalizedNameForCacheItem($cacheId, $context = null) {
		if(null === $context) $context = static::CONTEXT_LANG;
		switch ($context) {
			case static::NO_CONTEXT:
				return preg_replace('/[\\{\\}\\(\\)\\/\\\\@:]+/', '_', $cacheId);
				break;
			default:
				// default: CONTEXT_LANG
				$currentLocale = $this->serviceKernel->getLocale();
				return preg_replace('/[\\{\\}\\(\\)\\/\\\\@:]+/', '_', $cacheId.'_'.$currentLocale);
				break;
		}
	}

	public function getCacheDir($env = null) {
		if(empty($folder)) $folder = 'cache_data';
		$folder = preg_replace('/[\\W]+/', '_', $folder);
		$env_folder = 'all';
		switch (strtolower($env)) {
			case 'auto': $env_folder = $this->serviceKernel->getEnv(); break;
			default: if(!empty($env)) $env_folder = $env; break;
		}
		return $this->serviceKernel->getBaseDir('var/cache/'.$env_folder.DIRECTORY_SEPARATOR);
		// return preg_replace('/@@@ENV@@@/', $env_folder, $this->serviceKernel->getBaseDir('var/cache/@@@ENV@@@/'.$folder.DIRECTORY_SEPARATOR));
	}

	public function getCachePath($folder = null, $env = null) {
		if(empty($folder)) $folder = 'cache_data';
		$folder = preg_replace('/[\\W]+/', '_', $folder);
		$env_folder = 'all';
		switch (strtolower($env)) {
			case 'auto': $env_folder = $this->serviceKernel->getEnv(); break;
			default: if(!empty($env)) $env_folder = $env; break;
		}
		return  $this->serviceKernel->getBaseDir('var/cache/'.$env_folder.DIRECTORY_SEPARATOR).$folder.DIRECTORY_SEPARATOR;
	}

	public function refreshStrategy(&$refresh) {
		if(!is_bool($refresh)) $refresh = $this->devRefresh;
		return $refresh;
	}


	/*************************************************************************************/
	/*** CACHE FOR SINGLE DATA
	/*************************************************************************************/

	public function getCacheData($cacheId, callable $callable, $folder = null, $context = null, $env = null, $lifetime = null, $refresh = 'auto') {
		$this->refreshStrategy($refresh);
		$cacheId = $this->getNormalizedNameForCacheItem($cacheId, $context);
		if(empty($lifetime)) $lifetime = static::CACHE_LIFETIME;
		$FilesystemAdapter = new FilesystemAdapter($cacheId, $lifetime, $this->getCachePath($folder, $env));
		//********************** TEST ************************
		// $refresh = true;
		//****************************************************
		if($refresh) $FilesystemAdapter->deleteItem($cacheId);
		$fsCache = $FilesystemAdapter->getItem($cacheId);
		if(!$fsCache->isHit()) {
			$data = $callable();
			$fsCache->set($data);
			$FilesystemAdapter->save($fsCache);
			return $data;
		}
		return $fsCache->get();
	}

	public function getCacheSerialized($entity, callable $callable, $serialization_infos = ['groups' => ['cached_item'], 'format' => 'json'], $folder = null, $context = null, $env = null, $lifetime = null, $refresh = 'auto') {
		$this->refreshStrategy($refresh);
		if(null === $context) $context = static::CONTEXT_LANG;
		if(empty($lifetime)) $lifetime = static::CACHE_LIFETIME;
		$serviceCache = $this;
		$callableOverride = function() use ($callable, $serviceCache, $serialization_infos) {
			$serialization_infos['groups'] ??= 'cached_item';
			$serialization_infos['format'] ??= 'json';
			$result = $serviceCache->getSerializer()->serialize($callable(), $serialization_infos['format'], SerializationContext::create()->enableMaxDepthChecks()->setGroups($serialization_infos['groups']));
			return $result;
		};
		$cacheId = is_string($entity) ? $entity : $entity->getBddid();
		//********************** TEST ************************
		// $refresh = true;
		//****************************************************
		return json_decode($this->getCacheData($cacheId, $callableOverride, $folder, $context, $env, $lifetime, $refresh), true);
	}

	public function clearCacheData($cacheId, $folder = null, $context = null, $env = null, $hard = false) {
		if($hard) {
			$finder = new Finder();
			$dirs = array($this->getCachePath($folder, $env));
			//TODO quick hack...
			if(count($dirs) > 0) { 
				$finder->in($dirs)->files();
				foreach($finder as $file) unlink($file->getRealpath());
				$this->container->get(serviceFlashbag::class)->addFlashToastr('warning', 'Cleared all cache '.json_encode($cacheId).' (system file method).', 'ROLE_ADMIN');
			}
		} else {
			$cacheId = $this->getNormalizedNameForCacheItem($cacheId, $context);
			$FilesystemAdapter = new FilesystemAdapter($cacheId, null, $this->getCachePath($folder, $env));
			$FilesystemAdapter->deleteItem($cacheId);
			$this->container->get(serviceFlashbag::class)->addFlashToastr('warning', 'Cleared all cache '.json_encode($cacheId).' (cache method).', 'ROLE_ADMIN');
		}
		return $this;
	}




}