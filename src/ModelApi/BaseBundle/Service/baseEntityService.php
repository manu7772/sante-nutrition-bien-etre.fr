<?php
namespace ModelApi\BaseBundle\Service;

use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Service\serviceGrants;

use \ReflectionClass;
use \Exception;

trait baseEntityService {

	use \ModelApi\BaseBundle\Service\baseService;



	public function getEntityManager() {
		return $this->serviceEntities->getEntityManager();
	}

	public function getRepository() {
		return $this->serviceEntities->getRepository(static::ENTITY_CLASS);
	}

	public function getModel($classname = null, $asEntityIfPossible = false) {
		if(null === $classname) $classname = static::ENTITY_CLASS;
		return $this->serviceEntities->getModel($classname, $asEntityIfPossible);
	}

	public function createNew($options = [], callable $beforePostNewEventClosure = null) {
		$RC = new ReflectionClass(static::ENTITY_CLASS);
		if($RC->isInstantiable()) return $this->serviceEntities->createNew(static::ENTITY_CLASS, $options, $beforePostNewEventClosure);
		throw new Exception("Error ".__METHOD__."(): Entity ".static::ENTITY_CLASS." is not instantiable!", 1);
	}

	public function delete($entity, $flush = true, $forceDelete = false) {
		$entityManager = $this->serviceEntities->getEntityManager();
		if($entity->isInstance(['BaseEntity']) && !$forceDelete) $entity->setSoftdeleted();
			else $entityManager->remove($entity);
		if($flush) $entityManager->flush();
		return $this;
	}

	public function undelete($entity, $flush = true) {
		if($entity->isInstance(['BaseEntity'])) $entity->setUnsoftdeleted();
		if($flush) $this->serviceEntities->getEntityManager()->flush();
		return $this;
	}

	public function getCopy($entity, $options = [], $closure = null) {
		// throw new Exception("Error ".__METHOD__."(): this service ".static::ENTITY_CLASS." does not manage copy yet!", 1);
		$new_entity = clone $entity;
		if(is_executable($entity)) $closure($new_entity);
		return $new_entity;
	}


	public function getModelForCreate($classname, $groups = []) {
		return $this->serviceEntities->getModelForCreate($classname, $groups);
	}

	public function setModelForCreate($entity) {
		return $this->serviceEntities->setModelForCreate($entity);
	}

	public function removeModelForCreate($classname) {
		return $this->serviceEntities->removeModelForCreate($classname);
	}



	public function findAll() {
		return $this->getRepository()->findAll();
	}

	/**
	 * 
	 * @param 
	 * @param boolean $adminShowAll = true / If is ROLE_ADMIN and in prefered Entreprise environment, show all
	 * @return array <Entity>
	 */
	public function findByEnvironment($asArray = false, $adminShowAll = true) {
		$tier = $this->container->get(serviceContext::class)->getEnvironment();
		if($adminShowAll && $this->container->get(serviceGrants::class)->isGranted('ROLE_ADMIN') && $tier instanceOf Entreprise && $tier->isPrefered()) return $this->findAll(); 
		if(!($tier instanceOf Tier)) return $this->findAll();
		$repo = $this->getRepository();
		return method_exists($repo, 'findByTier') ? $repo->findByTier($tier, $asArray) : $this->findAll();
	}


}