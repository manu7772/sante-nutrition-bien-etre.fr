<?php
namespace ModelApi\BaseBundle\Service;

interface servicesBaseInterface {

	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString();

	public function getServiceModules();

	/**
	 * Get service parameter from ParameterBag
	 * @param string $name = null (separate path with . or /)
	 * @param mixed $defaultIfNotFound = null
	 * @param boolean $exceptionIfNoSM = true
	 * @param boolean $exceptionIfErrors = false
	 * @return mixed
	 */
	public function getServiceParameter($name = null, $defaultIfNotFound = null, $exceptionIfNoSM = true, $exceptionIfErrors = false);

	public function getServiceParameters();


}

