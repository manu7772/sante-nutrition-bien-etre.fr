<?php
namespace ModelApi\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
// BaseBundle
use ModelApi\BaseBundle\Entity\Annonce;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceBloger;

// use \DateTime;
use \ReflectionClass;

class serviceAnnonce extends serviceBloger {

	const ENTITY_CLASS = Annonce::class;


	public function isAnnonce() {
		return $this->getServiceParameter('enabled', true);
	}


}