<?php

namespace ModelApi\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Response;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceLogg;
use ModelApi\BaseBundle\Entity\Logg;
// use ModelApi\BaseBundle\Service\serviceEntities;
// use ModelApi\BaseBundle\Service\servicesBaseInterface;

use \NotFoundException;
use \DateTime;

class LoggController extends Controller {

	/**
	 * @Route("/logg/")
	 */
	public function indexAction() {
		return new Response('This web page is the logg page of BaseBundle. Never use it please.');
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Loggs",
	 *    description="Get loggs",
	 *    views = { "default", "loggs" }
	 * )
	 * @Rest\View(serializerGroups={"base","detail"})
	 * @Rest\Post("/logg/get-loggs", name="modelapibase-get-loggs")
	 */
	public function getLoggsAction(Request $request) {
		$start = 24; // nombre d'heures
		$interval = 'hour';
		$sort_interval = serviceLogg::getSortFormat($interval);
		$inc_interval = serviceLogg::getIncInterval($interval, "+");
		$now = new DateTime();
		$now = $now->format($sort_interval);
		// get Loggs
		$data['data'] = [];
		$data['data'][0] = [
			'label' => 'pages admin',
			'data' => [],
		];
		$data['data'][1] = [
			'label' => 'pages publiques',
			'data' => [],
		];
		// sort all loggs
		$loggs = $this->get(serviceLogg::class)->getRepository()->findLastHoursLoggs($start);
		$earlier = $later = null;
		foreach ($loggs as $logg) if(!$logg->isXmlHttp()) {
			$date = $logg->getCreated();
			$index = $date->format($sort_interval);
			if($index !== $now) {
				if(empty($earlier) || $earlier > $date) $earlier = clone $date;
				if(empty($later) || $later < $date) $later = clone $date;
				$table = $logg->getBundle() === 'WebsiteSiteBundle' ? 1 : 0;
				$data['data'][$table]['data'][$index] ??= [$index, 0];
				$data['data'][$table]['data'][$index][1]++;
			}
		}
		foreach ($data['data'] as $table => $vals) {
			// fill empty columns
			$date = new DateTime('-'.$start.' hours');
			$now = new DateTime();
			// $date = clone $earlier;
			while ($date < $now) {
				$index = $date->format($sort_interval);
				$data['data'][$table]['data'][$index] ??= [$index, 0];
				$date->modify($inc_interval);
			}
			ksort($data['data'][$table]['data']);
			array_unshift($data['data'][$table]['data'], "to_remove");
			$data['data'][$table]['data'] = array_values($data['data'][$table]['data']);
			array_shift($data['data'][$table]['data']);
			// final sort
			$inc = 1;
			foreach ($data['data'][$table]['data'] as $key => $values) {
				$data['data'][$table]['data'][$key][0] = $inc++;
			}
		}

		// https://www.flotcharts.org/
		// https://github.com/flot/flot/blob/master/API.md
		$data['options'] = [
			'series' => [
				'lines' => [
					'show' => true,
					'lineWidth' => 1,
					'fill' => true,
					'fillColor' => [
						'colors' => [
							['opacity' => 0.1],
							['opacity' => 0.8],
						]
					],
				],
			],
			'xaxis' => [
				'tickDecimals' => 0,
				'tickColor' => '#EEEEEE',
			],
			'yaxis' => [
				'tickDecimals' => 0,
				'tickColor' => '#EEEEEE',
			],
			'colors' => [
				// "#23c6c8",
				"#b6cdde",
				"#337ab7",
			],
			'grid' => [
				'color' => "#999999",
				'hoverable' => true,
				'clickable' => true,
				'tickColor' => "#D4D4D4",
				'borderWidth' => 0,
			],
			'legend' => [
				'show' => true,
				'sorted' => false,
			],
			'tooltip' => true,
			'tooltipOpts' => [
				'content' => "%y pages vues",
			],
		];
		return $data;
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Loggs",
	 *    description="Get perfs",
	 *    views = { "default", "perfs" }
	 * )
	 * @Rest\View(serializerGroups={"base","detail"})
	 * @Rest\Post("/perf/get-perfs", name="modelapibase-get-perfs")
	 */
	public function getPerfsAction(Request $request) {
		$start = 24; // nombre d'heures
		$interval = 'hour';
		$sort_interval = servicelogg::getSortFormat($interval);
		$inc_interval = servicelogg::getIncInterval($interval, "+");
		$now = new DateTime();
		$now = $now->format($sort_interval);
		// get Perfs
		$data['data'] = [];
		$data['data'][0] = [
			'label' => 'pages admin',
			'data' => [],
		];
		$data['data'][1] = [
			'label' => 'pages publiques',
			'data' => [],
		];
		// sort all perfs
		$loggs = $this->get(servicelogg::class)->getRepository()->findLastHoursLoggs($start);
		$earlier = $later = null;
		foreach ($loggs as $logg) if(!$logg->isXmlHttp()) {
			$during = $logg->getDuring();
			if($during >= 0) {
				$date = $logg->getCreated();
				$index = $date->format($sort_interval);
				if($index !== $now) {
					if(empty($earlier) || $earlier > $date) $earlier = clone $date;
					if(empty($later) || $later < $date) $later = clone $date;
					$table = $logg->getBundle() === 'WebsiteSiteBundle' ? 1 : 0;
					$data['data'][$table]['data'][$index] ??= [$index, []];
					$data['data'][$table]['data'][$index][1][] = $during;
				}
			}
		}
		foreach ($data['data'] as $table => $vals) {
			// fill empty columns
			$date = new DateTime('-'.$start.' hours');
			$now = new DateTime();
			// $date = clone $earlier;
			while ($date < $now) {
				$index = $date->format($sort_interval);
				$data['data'][$table]['data'][$index] ??= [$index, []];
				$date->modify($inc_interval);
			}
			ksort($data['data'][$table]['data']);
			array_unshift($data['data'][$table]['data'], "to_remove");
			$data['data'][$table]['data'] = array_values($data['data'][$table]['data']);
			array_shift($data['data'][$table]['data']);
			// final sort
			$inc = 1;
			foreach ($data['data'][$table]['data'] as $index => $values) {
				$data['data'][$table]['data'][$index][0] = $inc++;
				$moyenne = count($data['data'][$table]['data'][$index][1]) > 0 ?
					array_sum($data['data'][$table]['data'][$index][1]) / count($data['data'][$table]['data'][$index][1]):
					0;
				$data['data'][$table]['data'][$index][1] = round($moyenne * 1000) / 1000;
			}
		}

		// https://www.flotcharts.org/
		// https://github.com/flot/flot/blob/master/API.md
		$data['options'] = [
			'series' => [
				'lines' => [
					'show' => true,
					'lineWidth' => 1,
					'fill' => true,
					'fillColor' => [
						'colors' => [
							['opacity' => 0.1],
							['opacity' => 0.8],
						]
					],
				],
			],
			'xaxis' => [
				'tickDecimals' => 0,
				'tickColor' => '#EEEEEE',
			],
			'yaxis' => [
				'tickDecimals' => 0,
				'tickColor' => '#EEEEEE',
			],
			'colors' => [
				// "#23c6c8",
				"red",
				"orange",
			],
			'grid' => [
				'color' => "#999999",
				'hoverable' => true,
				'clickable' => true,
				'tickColor' => "#D4D4D4",
				'borderWidth' => 0,
			],
			'legend' => [
				'show' => true,
				'sorted' => false,
			],
			'tooltip' => true,
			'tooltipOpts' => [
				'content' => "%y secondes",
			],
		];
		return $data;
	}



}