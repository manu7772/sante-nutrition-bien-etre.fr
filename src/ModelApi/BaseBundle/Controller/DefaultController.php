<?php

namespace ModelApi\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Response;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\servicesBaseInterface;

use \NotFoundException;

class DefaultController extends Controller {

	/**
	 * @Route("/")
	 */
	public function indexAction() {
		return new Response('This web page is the homepage of BaseBundle. Never use it please.');
	}

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Entities",
	 *    description="Get entity",
	 *    views = { "default", "entity" }
	 * )
	 * @Rest\View(serializerGroups={"base","detail"})
	 * @Rest\Get("/entity-bddid/get/{bddid}", name="modelapibase-entity-bddid-get")
	 */
	public function getByBddidAction($bddid, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$entity = $serviceEntities->findByBddid($bddid);
		if(empty($entity)) throw new NotFoundException("Entity not found", 1);
		return $entity;
	}

	// /**
	//  * @ApiDoc(
	//  *    resource=true,
	//  *    section="Entities",
	//  *    description="Create child of entity",
	//  *    views = { "default", "entity" }
	//  * )
	//  * @Rest\View(serializerGroups={"base","detail"})
	//  * @Rest\Post("/entity-bddid/createchidl", name="modelapibase-entity-bddid-createchidl")
	//  */
	// public function createchidlByBddidAction(Request $request) {
	// 	$serviceEntities = $this->get(serviceEntities::class);
	// 	$entity = $serviceEntities->findByBddid($bddid);
	// 	if(empty($entity)) throw new NotFoundException("Entity not found", 1);
	// 	if($entity->isInstance(['GroupInterface'])) {
	// 		// Group
	// 		$service = $serviceEntities->getEntityService($entity->getClassname());
	// 		$new_group = $service->createNew()
	// 	} else if($entity->isInstance(['NestedInterface'])) {
	// 		// --> get POST data to create it!
	// 	}
	// 	return $entity;
	// }

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Entities",
	 *    description="Delete entity",
	 *    views = { "default", "entity" }
	 * )
	 * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
	 * @Rest\Delete("/entity-bddid/delete/{bddid}", name="modelapibase-entity-bddid-delete")
	 */
	public function deleteByBddidAction($bddid, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$entityManager = $serviceEntities->getEntityManager();
		$entity = $serviceEntities->findByBddid($bddid);
		if(!empty($entity)) {
			$service = $serviceEntities->getEntityService($entity->getClassname());
			if($service instanceOf servicesBaseInterface) {
				$service->delete($entity, false);
			} else {
				if($entity->isInstance(['BaseEntity'])) $entity->setSoftdeleted();
					else $entityManager->remove($entity);
			}
			$entityManager->flush();
		}
	}


}