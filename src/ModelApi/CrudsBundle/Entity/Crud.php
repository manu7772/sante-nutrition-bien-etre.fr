<?php
namespace ModelApi\CrudsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Doctrine\Common\Collections\ArrayCollection;
use Hateoas\Configuration\Annotation as Hateoas;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceClasses;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Service\serviceCruds;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotationInterface;
use ModelApi\CrudsBundle\Annotation\CrudsMultiAnnotation;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\TwgTranslation;

use \Exception;

/**
 * Crud
 * 
 * @ORM\Entity(repositoryClass="ModelApi\CrudsBundle\Repository\CrudRepository")
 * @ORM\Table(name="`crud`", options={"comment":"Cruds"})
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_crud",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show="ROLE_SUPER_ADMIN", create="ROLE_SUPER_ADMIN", update="ROLE_SUPER_ADMIN", delete="ROLE_SUPER_ADMIN", label="Crud")
 * @Annot\HasTranslatable()
 */
class Crud {

	const DEFAULT_ICON = 'fa-key';
	const ENTITY_SERVICE = serviceCruds::class;
	const TYPES = ['class','property','method'];
	const SHORTCUT_CONTROLS = true;

	use \ModelApi\BaseBundle\Traits\ClassDescriptor;
	use \ModelApi\BaseBundle\Traits\DateUpdater;

	/**
	 * Id of Item
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="`id`", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="`classentity`", type="string", nullable=false, unique=false)
	 */
	protected $classentity;

	/**
	 * @var string
	 * Name of attribute or method
	 * @ORM\Column(name="`target`", type="string", nullable=true, unique=false)
	 */
	protected $target;

	/**
	 * @var string
	 * Name of attribute or method
	 * @ORM\Column(name="`context`", type="string", nullable=true, unique=false)
	 */
	protected $context;

	/**
	 * @var string
	 * Values = ["attribute","method","class"]
	 * @ORM\Column(name="`type`", type="string", length=16, nullable=false, unique=false)
	 */
	protected $type;

	/**
	 * @var string
	 * @ORM\Column(name="`entitybddmcid`", type="text", nullable=true, unique=false)
	 */
	protected $entitybddmcid;

	/**
	 * @var string
	 * @ORM\Column(name="`crudname`", type="string", nullable=false, unique=false)
	 */
	protected $crudname;

	/**
	 * @var string
	 * @ORM\Column(name="`data`", type="json_array", nullable=false, unique=false)
	 */
	protected $data;

	/**
	 * @var string
	 * @ORM\Column(name="`description`", type="text", nullable=true, unique=false)
	 * @Annot\Translatable(type=TwgTranslation::TYPE_HTML)
	 */
	protected $description;

	/**
	 * @var string
	 * @ORM\Column(name="`label`", type="string", nullable=true, unique=false)
	 * @Annot\Translatable(type=TwgTranslation::TYPE_TEXT)
	 */
	protected $label;

	/**
	 * @var string
	 * @ORM\Column(name="`placeholder`", type="string", nullable=true, unique=false)
	 * @Annot\Translatable(type=TwgTranslation::TYPE_TEXT)
	 */
	protected $placeholder;

	protected $entity;
	protected $crudsAnnotation;


	public function __construct($entity, CrudsAnnotationInterface $crudsAnnotation, $type, $target = null) {
		if($crudsAnnotation instanceOf CrudsMultiAnnotation) throw new Exception("Error ".__METHOD__."(): can not create Crud with CrudsMultiAnnotation. Please divide it before!", 1);
		
		$this->init_ClassDescriptor();
		$this->id = null;
		// $this->entity = null;
		// $this->crudsAnnotation = null;
		// $this->classentity = null;
		$this->entitybddmcid = null;
		$this->type = $type;
		$this->context = null;
		$this->target = $target;
		$this->data = [];
		$this->description = null;
		$this->label = null;
		$this->placeholder = null;
		$this->compileCruds($entity, $crudsAnnotation);
		return $this;
	}

	/**
	 * Get shortname of service
	 * @return string
	 */
	public function __toString() {
		return (string)$this->getName();
	}

	public function getName() {
		return $this->getShortname().'_'.serviceClasses::getShortname($this->getCrudname()).'_'.$this->getId();
	}

	protected function compileCruds($entity, CrudsAnnotationInterface $crudsAnnotation) {
		$this->crudsAnnotation = $crudsAnnotation;
		if(isset($this->crudsAnnotation->options)) {
			if(!empty($this->crudsAnnotation->options['description'] ?? null)) $this->description = $this->crudsAnnotation->options['description'];
			if(!empty($this->crudsAnnotation->options['label'] ?? null)) $this->label = $this->crudsAnnotation->options['label'];
			if(!empty($this->crudsAnnotation->options['placeholder'] ?? null)) $this->placeholder = $this->crudsAnnotation->options['placeholder'];
		}
		$this->crudname = get_class($crudsAnnotation);
		switch ($this->type) {
			case 'class':
				// Crud for Class
				$this->entity = null;
				$this->classentity = is_object($entity) ? get_class($entity) : $entity;
				break;
			default:
				// Crud for single entity
				if($entity instanceOf CrudInterface) {
					$this->entity = $entity;
					$this->classentity = $this->entity->getClassname();
					// $this->classentity = serviceEntities::removeProxyPrefix(get_class($this->entity));
					$this->entitybddmcid = $entity->getBddMcid();
					$entity->addCrud($this);
				} else if(is_string($entity) && class_exists($entity)) {
					$this->entity = null;
					$this->classentity = $entity;
					$this->entitybddmcid = null;
				} else {
					$type = is_object($entity) ? get_class($entity) : gettype($entity);
					throw new Exception("Error ".__METHOD__."(): entity is not a class nore a valid entity! Got ".$type."!", 1);
				}
				break;
		}
		$this->AnnotationToData();
		return $this;
	}

	public function getAnnotation(CrudInterface $entity = null) {
		$annotation = $this->dataToAnnotation();
		if($this->isDeepControl() && !($annotation instanceOf CrudsAnnotationInterface) || $annotation instanceOf CrudsMultiAnnotation) throw new Exception("Error ".__METHOD__."(): did not succeeded to generate annotation!", 1);
		if(isset($this->annotation->options)) {
			if(!empty($this->getDescription())) $this->annotation->options['description'] = $this->getDescription();
			if(!empty($this->getLabel())) $this->annotation->options['label'] = $this->getLabel();
			if(!empty($this->getPlaceholder())) $this->annotation->options['placeholder'] = $this->getPlaceholder();
		}
		$annotation->setEntity($entity);
		return $annotation;
	}

	protected function dataToAnnotation() {
		// if(!($this->crudsAnnotation instanceOf CrudsAnnotationInterface)) {
		// 	$annot = $this->crudname;
		// 	$this->crudsAnnotation = new $annot();
		// }
		if(empty($this->data)) throw new Exception("Error ".__METHOD__."(): data is missing (or is not serialized string) to generate Annotation! Got ".json_encode($this->data)."!", 1);
		return $this->crudsAnnotation = unserialize($this->data);
	}

	/** 
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	protected function AnnotationToData() {
		if(!($this->crudsAnnotation instanceOf CrudsAnnotationInterface)) throw new Exception("Error ".__METHOD__."(): annotation is missing to generate serialized data!", 1);
		return $this->data = serialize($this->crudsAnnotation);
	}

	// /** 
	//  * @ORM\PostLoad()
	//  */
	// public function postLoad() {
	// 	$this->dataToAnnotation();
	// }

	public function getId() {
		return $this->id;
	}

	public function isValid() {
		return
			$this->crudsAnnotation instanceOf CrudsAnnotationInterface
			// && !empty($data)
			&& class_exists($this->crudname)
			&& class_exists($this->classentity)
			&& in_array($this->type, static::TYPES)
			&& (($this->type === 'class' && empty($this->target)) || ($this->type !== 'class' && is_string($this->target)))
			;
	}


	public function setClassentity($classentity) {
		$this->classentity = $classentity;
		return $this;
	}

	public function getClassentity() {
		return $this->classentity;
	}

	public function setEntity(CrudInterface $entity) {
		if($this->classentity !== $entity->getClassname() || $this->entitybddmcid !== $entity->getBddMcid()) throw new Exception("Error ".__METHOD__."(): this Entity does not belong to this Crud!", 1);
		$this->entity = $entity;
		return $this;
	}

	public function setCrudname($crudname) {
		$this->crudname = $crudname;
		return $this;
	}

	public function getCrudname() {
		return $this->crudname;
	}

	public function setType($type) {
		$this->type = $type;
		return $this;
	}

	public function getType() {
		return $this->type;
	}

	public function setTarget($target) {
		$this->target = $target;
		return $this;
	}

	public function getTarget() {
		return $this->target;
	}

	public function setDescription($description = null) {
		$this->description = serviceTools::getTextOrNull($description);
		return $this;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setLabel($label = null) {
		$this->label = serviceTools::getTextOrNullStripped($label);
		return $this;
	}

	public function getLabel() {
		return $this->label;
	}

	public function setPlaceholder($placeholder = null) {
		$this->placeholder = serviceTools::getTextOrNullStripped($placeholder);
		return $this;
	}

	public function getPlaceholder() {
		return $this->placeholder;
	}

	public function setContext($context = null) {
		$this->context = $context;
		return $this;
	}

	public function getContext() {
		return $this->context;
	}

	public function setEntitybddmcid($entitybddmcid = null) {
		$this->entitybddmcid = $entitybddmcid;
		return $this;
	}

	public function getEntitybddmcid() {
		return $this->entitybddmcid;
	}



	/**
	 * Get icon
	 * @return string
	 */
	public function getIcon() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDEFAULT_ICON() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get DEFAULT_ICON
	 * @return string
	 */
	public static function getDefaultIcon() {
		return static::DEFAULT_ICON;
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public function getIconClass() {
		return 'fa '.static::DEFAULT_ICON;
	}

	/**
	 * Get icon for class in element
	 * @return string
	 */
	public static function getDefaultIconClass() {
		return 'fa '.static::DEFAULT_ICON;
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public function getIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.$this->getIconClass().$classes.'"></i>';
	}

	/**
	 * Get icon as html element
	 * @return string
	 */
	public static function getDefaultIconHtml($fixed_witdh = true, $classes = []) {
		// $fixed_witdh = true === $fixed_witdh ? ' fa-fw' : '';
		if(is_string($classes)) $classes = preg_split('/\\s+/', $classes);
		if(is_array($classes)) {
			if($fixed_witdh && !in_array('fa-fw', $classes)) $classes[] = 'fa-fw';
			$classes = count($classes) > 0 ? ' '.implode(' ', $classes) : '';
		} else {
			$classes = '';
		}
		return '<i class="'.static::getDefaultIconClass().$classes.'"></i>';
	}


}
