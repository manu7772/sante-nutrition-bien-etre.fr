<?php
namespace ModelApi\CrudsBundle\Entity;

use ModelApi\CrudsBundle\Entity\Crud;


interface CrudInterface {
	
	public function getCruds();

	public function addCrud(Crud $crud);

	public function removeCrud(Crud $crud);

	public function removeCruds();

	public function setCruds($cruds);

	/**
	 * Is deletable
	 * @return boolean
	 */
	public function isDeletable();

}