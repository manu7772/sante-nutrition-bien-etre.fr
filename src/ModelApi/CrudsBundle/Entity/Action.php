<?php
namespace ModelApi\CrudsBundle\Entity;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
// BaseBundle
use ModelApi\BaseBundle\Entity\LaboClassMetadata;
use ModelApi\BaseBundle\Entity\Facticee;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Service\serviceContext;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Service\serviceCruds;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\NestedInterface;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Service\serviceUser;
// use ModelApi\UserBundle\Service\serviceRoles;
use ModelApi\UserBundle\Service\serviceGrants;

use Symfony\Component\Routing\Generator\UrlGenerator;

use \Exception;

/**
 * Action
 * 
 */
class Action {

	const HASHTAG = '#';

	protected $entity; // CrudInterface
	protected $status; // integer
	protected $actionName; // string
	protected $container; // ContainerInterface
	protected $serviceCruds; // serviceCruds
	protected $serviceGrants; // serviceGrants

	protected $user;
	protected $url;
	protected $title;
	protected $btn;
	protected $icon;
	protected $confirm;
	protected $disabled;


	public function __construct($actionName, $entity, ContainerInterface $container, $data = []) {
		$this->container = $container;
		$this->actionName = $actionName;
		$this->serviceCruds = $this->container->get(serviceCruds::class);
		// if(!$this->serviceCruds->isValidActionName($this->actionName)) throw new Exception("Error: Action of type ".json_encode($this->actionName)." does not exist!", 1);
		$this->serviceGrants = $this->container->get(serviceGrants::class);
		/*if(is_string($entity)) {
			$entity = $this->container->get(serviceEntities::class)->getModel($entity);
		}*/
		if(!is_object($entity) || (!method_exists($entity, 'isInstanceOf') && !$entity->isInstanceOf(CrudInterface::class))) throw new Exception("Error: Entity must be ".CrudInterface::class." or ".LaboClassMetadata::class.". Got ".(is_object($entity) ? get_class($entity) : gettype($entity).' ('.json_encode($entity)).")!", 1);
		$this->entity = $entity;
		// if($entity->isInstanceOf(Item::class)) echo('<div style="color:orange;">Entity '.json_encode($this->entity->getId()).'</div>');
		$this->url = null;
		$this->user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
		$this->data = $data ?? [];
		return $this;
	}

	public function isValid() {
		// if(!$this->hasEntityAction()) return false;
		$valid = true;
		switch ($this->actionName) {
			case 'show':
				if(!$this->entity->isInstanceOf([CrudInterface::class, Directory::class])) $valid = false;
				break;
			case 'dirs':
				if(!$this->entity->isInstanceOf(NestedInterface::class)) $valid = false;
				if(!$this->serviceGrants->isGrantedEntity($this->entity, $this->user)) $valid = false;
				break;
			case 'list':
				break;
			case 'create':
				if($this->entity instanceOf CrudInterface) $valid = false;
				// if($this->entity->isInstanceOf(User::class) && !$this->serviceGrants->isGranted('ROLE_ADMIN')) $valid = false;
				break;
			case 'update':
				if(!$this->entity->isInstanceOf(CrudInterface::class)) $valid = false;
				if(!$this->serviceGrants->isGrantedEntity($this->entity, $this->user)) $valid = false;
				break;
			case 'prefered':
				if(!$this->entity->isInstanceOf(CrudInterface::class) || !method_exists($this->entity, 'getPrefered')) $valid = false;
				if(!$this->serviceGrants->isGrantedEntity($this->entity, $this->user)) $valid = false;
				if($this->serviceGrants->isGranted('ROLE_ADMIN', $this->user)) $valid = true;
				break;
			case 'enable':
				if(!$this->entity->isInstanceOf(CrudInterface::class) || !method_exists($this->entity, 'getEnabled')) $valid = false;
				if($this->isSelfUserEntity()) $valid = false;
				if(!$this->serviceGrants->isGrantedEntity($this->entity, $this->user)) $valid = false;
				break;
			case 'softdelete':
				if(!$this->entity->isInstanceOf(CrudInterface::class) || !method_exists($this->entity, 'isSoftdeleted')) $valid = false;
				if($this->isSelfUserEntity()) $valid = false;
				if(!$this->serviceGrants->isGrantedEntity($this->entity, $this->user)) $valid = false;
				break;
			case 'delete':
				$valid = $this->isUserSuperadmin() && !(method_exists($this->entity, 'getPrefered') && $this->entity->getPrefered());
				if($this->isSelfUserEntity()) $valid = false;
				break;
			case 'switchctx':
				$valid = $this->isUserSuperadmin() || $this->serviceGrants->isGrantedEntity($this->entity, $this->user);
				if($this->isSelfUserEntity()) $valid = true;
				break;
		}
		// die('<div>Valid: '.json_encode($valid).'</div>');
		return $valid;
	}

	protected function isSelfUserEntity() {
		return $this->user === $this->entity;
	}

	protected function isUserEntity() {
		if($this->serviceGrants->isGranted('ROLE_ADMIN', $this->user)) return true;
		if($this->user->getMicrotimeid() === $this->entity->getMicrotimeid()) return true;
		$owner = $this->entity instanceOf User ? $this->entity : $this->entity->getOwner();
		return $this->user->getMicrotimeid() === $owner->getMicrotimeid();
	}

	protected function canManage() {
		if($this->serviceGrants->isGranted('ROLE_ADMIN', $this->user)) return true;
		$tiers = $this->entity instanceOf Tier ? [$this->entity] : $this->entity->getOwnerdirs();
		foreach ($tiers as $tier) {
			if($tier->hasAssociateUser($this->user)) return true;
		}
		if($this->user->getMicrotimeid() === $this->entity->getMicrotimeid()) return true;
		$owner = $this->entity instanceOf User ? $this->entity : $this->entity->getOwner();
		return $this->user->getMicrotimeid() === $owner->getMicrotimeid();
	}

	public function getStatus($asInteger = true) {
		$this->status = true;
		/*if(!$this->hasEntityAction()) {
			$this->status = false;
		} else {*/
			switch ($this->actionName) {
				case 'show':
					$this->status = !$this->entity->isInstanceOf(Directory::class);
					break;
				case 'dirs':
					$this->status = $this->entity->isInstanceOf(NestedInterface::class);
					if($this->entity->isInstanceOf(NestedInterface::class)) {
						if($this->entity->isInstanceOf(Basedirectory::class)) {
							// THIS directory
							$this->status = 0;
						} else if($this->entity->hasDirs()) {
							// HAS directorys
							$this->status = 1;
						} else {
							// PARENT directory
							$this->status = 2;
						}
					} else {
						// INVALID
						$this->status = -1;
					}
					break;
				case 'list':
					$this->status = true;
					break;
				case 'create':
					$this->status = true;
					// if($this->entity->isInstanceOf(User::class) && !$this->serviceGrants->isGranted('ROLE_ADMIN')) $this->status = false;
					break;
				case 'update':
					$this->status = true;
					break;
				case 'prefered':
					$this->status = method_exists($this->entity, 'getPrefered') ? $this->entity->getPrefered() && $this->entity->isActive() : false;
					break;
				case 'enable':
					$this->status = $this->entity->isEnabled();
					break;
				case 'softdelete':
					$this->status = !$this->entity->isSoftdeleted();
					break;
				case 'delete':
					$this->status = true;
					break;
				case 'switchctx':
					$this->status = true;
					break;
			}
		// }
		return $asInteger ? (integer)$this->status : (boolean)$this->status;
	}

	public function getName() {
		return $this->actionName;
	}

	/*public function getEntityShortnameLower() {
		return $this->entity->getShortname(true);
	}*/

	protected function isUserSuperadmin() {
		$user = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
		return $user instanceOf User ? $user->isSuperadmin() : false;
	}

	public function getUrl($referenceType = 0, $params = [], $defaultUrlIfNull = true) {
		$this->url = null;
		if($this->isValid()) {
			$route = 'wsa_'.$this->entity->getShortname(true).'_'.$this->actionName;
			if($this->entity->isInstanceOf(Directory::class) && $this->actionName === 'show') $route = 'wsa_directory_owner';
			if($this->entity->isInstanceOf(NestedInterface::class) && $this->actionName === 'dirs') $route = 'wsa_directory_owner';
			if($this->container->get(serviceRoute::class)->routeExists($route)) {
				$valid = true;
				if(!$this->isValid()) {
					$valid = false;
				} else {
					switch ($this->actionName) {
						case 'show':
							if($this->entity->isInstanceOf(CrudInterface::class)) {
								$params['id'] ??= $this->entity->getId();
							} else {
								$valid = false;
							}
							break;
						case 'dirs':
							if($this->entity->isInstanceOf(NestedInterface::class) && $this->canManage()) {
								if($this->entity->isInstanceOf(Basedirectory::class) || $this->entity->hasDirs()) {
									$params['id'] ??= $this->entity->getId();
								} else {
									if(!empty($this->entity->getParent()) && $this->entity->getParent()->isInstanceOf(Basedirectory::class)) $params['id'] ??= $this->entity->getId();
										else $valid = false;
								}
							} else {
								$valid = false;
							}
							break;
						case 'list':
							// no parameters
							break;
						case 'create':
							// no parameters
							break;
						case 'update':
							if($this->entity->isInstanceOf(CrudInterface::class) && $this->canManage()) {
								$params['id'] ??= $this->entity->getId();
							} else {
								$valid = false;
							}
							break;
						case 'prefered':
							if(!$this->serviceGrants->isGranted('ROLE_ADMIN', $this->user)) return false;
							if($this->entity->isInstanceOf(CrudInterface::class) && method_exists($this->entity, 'getPrefered') && $this->isUserEntity()) {
								$params['id'] ??= $this->entity->getId();
								$params['status'] ??= (integer)!$this->entity->getPrefered();
							} else {
								$valid = false;
							}
							break;
						case 'enable':
							if($this->entity->isInstanceOf(CrudInterface::class) && $this->isUserEntity()) {
								$params['id'] ??= $this->entity->getId();
								$params['status'] ??= (integer)!$this->entity->isEnabled();
							} else {
								$valid = false;
							}
							break;
						case 'softdelete':
							if($this->entity->isInstanceOf(CrudInterface::class) && $this->isUserEntity()) {
								$params['id'] ??= $this->entity->getId();
								$params['status'] ??= (integer)$this->entity->isSoftdeleted();
							} else {
								$valid = false;
							}
							break;
						case 'delete':
							if(!$this->isUserSuperadmin() || (method_exists($this->entity, 'getPrefered') && $this->entity->getPrefered()) || !$this->entity->isInstanceOf(CrudInterface::class)) $valid = false;
								else $params['id'] ??= $this->entity->getId();
							break;
						case 'switchctx':
							$route = 'website_admin_homepage';
							$params[serviceContext::CONTEXT_ENVIRONMENT_TOKEN_NAME] ??= $this->entity->getBddid();
							break;
					}
				}
				if($valid) {
					$referenceType = in_array($referenceType, [UrlGenerator::ABSOLUTE_URL, UrlGenerator::ABSOLUTE_PATH, UrlGenerator::RELATIVE_PATH]) ? $referenceType : UrlGenerator::ABSOLUTE_URL;
					$this->url = $this->container->get(serviceRoute::class)->getRouter()->generate($route, $params, $referenceType);
				}
			}
		}
		if(true === $defaultUrlIfNull) $defaultUrlIfNull = static::HASHTAG;
		return is_string($defaultUrlIfNull) && empty($this->url) ? $defaultUrlIfNull : $this->url;
	}

	public function hasUrl() {
		$url = $this->getUrl();
		return !empty($url) && $url !== static::HASHTAG;
	}

	public function getTitle($defaultFirst = true) {
		if(!is_array($this->data['title'])) return $this->data['title'];
		$status = $this->getStatus();
		if(array_key_exists($status, $this->data['title'])) return $this->data['title'][$status];
		if($defaultFirst) return reset($this->data['title']);
		return null;
	}

	public function getBtn($defaultFirst = true) {
		if(!is_array($this->data['btn'])) return $this->data['btn'];
		$status = $this->getStatus();
		if(array_key_exists($status, $this->data['btn'])) return $this->data['btn'][$status];
		if($defaultFirst) return reset($this->data['btn']);
		return 'btn-white';
	}

	public function getIcon($defaultFirst = true) {
		if(!is_array($this->data['icon'])) return $this->data['icon'];
		$status = $this->getStatus();
		if(array_key_exists($status, $this->data['icon'])) return $this->data['icon'][$status];
		if($defaultFirst) return reset($this->data['icon']);
		return 'fa-cog';
	}

	public function getConfirm($defaultFirst = true) {
		if(!is_array($this->data['confirm'])) return $this->data['confirm'];
		$status = $this->getStatus();
		if(array_key_exists($status, $this->data['confirm'])) return $this->data['confirm'][$status];
		if($defaultFirst) return reset($this->data['confirm']);
		return false;
	}

	public function getDisabled($defaultFirst = true) {
		if(!is_array($this->data['disabled'])) return $this->data['disabled'];
		$status = $this->getStatus();
		if(array_key_exists($status, $this->data['disabled'])) return $this->data['disabled'][$status];
		if($defaultFirst) return reset($this->data['disabled']);
		return false;
	}


/*	public function hasEntityAction($action = null) {
		$actions_data = $this->serviceCruds->getActionsData();
		echo('<pre><h3>'.($this->entity->isInstanceOf(CrudInterface::class) ? 'Entity' : 'Model').' has action '.json_encode($action).'</h3>'); var_dump($actions_data); die('</pre>');

	}
*/


}
