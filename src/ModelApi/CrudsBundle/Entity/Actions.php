<?php
namespace ModelApi\CrudsBundle\Entity;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Doctrine\ORM\Mapping\ClassMetadata;
// BaseBundle
use ModelApi\BaseBundle\Entity\LaboClassMetadata;
use ModelApi\BaseBundle\Service\serviceEntities;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\Action;
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Service\serviceCruds;

use ArrayAccess;
use ArrayIterator;
// use Closure;
use Countable;
use IteratorAggregate;
use \Exception;

/**
 * Crud
 * 
 */
class Actions implements Countable, IteratorAggregate, ArrayAccess {

	protected $container; // ContainerInterface

	protected $entity; // CrudInterface
	protected $actions;

	/**
	 * Constructor
	 * @param CrudInterface $entity
	 * @param array | string $actions = [] --> list (array) of actions OR context (string) for actions in {'@list','@view','@create','@update'}
	 * @param ContainerInterface $container
	 * @return self
	 */
	public function __construct($entity, $actions = [], ContainerInterface $container) {
		$this->container = $container;
		$this->serviceCruds = $this->container->get(serviceCruds::class);
		// if(empty($this->actions)) $this->actions = Action::getValidActionsNames();
		// $this->actions = [];
		$this->setEntity($entity, $actions);
		return $this;
	}

	public function setEntity($entity, $actions = []) {
		if(is_string($entity)) {
			$entity = $this->container->get(serviceEntities::class)->getModel($entity, true);
		}
		if(!method_exists($entity, 'isInstanceOf') && !$entity->isInstanceOf([CrudInterface::class, LaboClassMetadata::class])) throw new Exception("Error: Entity must be ".CrudInterface::class." or ".LaboClassMetadata::class.". Got ".(is_object($entity) ? get_class($entity) : gettype($entity).' ('.json_encode($entity)).")!", 1);
		// if(empty($actions)) {
		// 	if($entity->isInstanceOf(CrudInterface::class)) $actions = '@entity';
		// 	if($entity->isInstanceOf(LaboClassMetadata::class)) $actions = '@model';
		// }
		$this->entity = $entity;
		$this->setActions($actions);
		// $this->getDescription(true);
		return $this;
	}

	/*protected function getDescription($print = false) {
		$description = [];
		foreach ($this->actions as $name => $action) {
			$description[$name] = [
				'url' => $action->getUrl(),
				'icon' => $action->getIcon(),
			];
		}
		if($print) echo('<pre><h3>'.($this->entity->isInstanceOf(CrudInterface::class) ? 'Entity' : 'Model').' valid URLS: '.json_encode($this->hasValidUrls()).'</h3>'); var_dump($description); echo('</pre>');
		return $description;
	}*/

	public function getEntity() {
		return $this->entity;
	}

	public function setActions($contextGroups = []) {
		$context_actions = $this->serviceCruds->getActionsDataByEntityAndContextGroups($this->entity, $contextGroups);
		// echo('<pre><h3>'.($this->entity->isInstanceOf(CrudInterface::class) ? 'Entity' : 'Model').' with context '.json_encode((array)$contextGroups).'</h3>'); var_dump($context_actions); die('</pre>');
		$this->clearActions();
		foreach ($context_actions as $actionName => $data) {
			$newAction = $this->addAction($actionName, $data);
			// die('<div class="text-white">Action '.$actionName.': '.json_encode($newAction->isValid()));
		}
		// echo('<pre><h3>'.($this->entity->isInstanceOf(CrudInterface::class) ? 'Entity' : 'Model').' with context '.json_encode((array)$contextGroups).'</h3>'); var_dump(array_keys($this->actions)); die('</pre>');
		return $this;
	}

	public function clearActions() {
		$this->actions = array();
		return $this;
	}

	public function addAction($actionName, $data = []) {
		$action = new Action($actionName, $this->entity, $this->container, $data);
		if($action->isValid()) $this->actions[$action->getName()] = $action;
		return $this->actions[$action->getName()] ?? null;
	}

	public function removeAction($actionName) {
		$this->actions = array_filter($this->actions, function($key) use ($actionName) { return strtolower($key) !== strtolower($actionName); }, ARRAY_FILTER_USE_KEY);
		return $this;
	}

	public function hasValidUrls() {
		foreach ($this->actions as $action) if($action->hasUrl()) return true;
		return false;
	}

	public function hasActions() {
		return $this->count() > 0;
	}



	/**
	 * {@inheritDoc}
	 */
	public function toArray() {
		return $this->actions;
	}

	public function count() {
		return count($this->actions);
	}

	public function isEmpty() {
		return count($this->actions) <= 0;
	}

    /**
     * Required by interface IteratorAggregate.
     *
     * {@inheritDoc}
     */
    public function getIterator() {
        return new ArrayIterator($this->toArray());
    }

    /**
     * Required by interface ArrayAccess.
     *
     * {@inheritDoc}
     */
    public function offsetExists($offset) {
        return $this->containsKey($offset);
    }

    /**
     * {@inheritDoc}
     */
    public function containsKey($key) {
        return isset($this->actions[$key]) || array_key_exists($key, $this->actions);
    }

	/**
	 * {@inheritDoc}
	 */
	public function get($action) {
		return $this->actions[$action] ?? null;
	}

    /**
     * Required by interface ArrayAccess.
     * {@inheritDoc}
     */
    public function offsetGet($offset) {
        return $this->get($offset);
    }

    /**
     * Required by interface ArrayAccess.
     * {@inheritDoc}
     */
    public function offsetSet($offset, $value) {
        $this->addAction($offset);
        return $this;
    }

    /**
     * Required by interface ArrayAccess.
     *
     * {@inheritDoc}
     */
    public function offsetUnset($offset) {
        $this->removeAction($offset);
        return $this;
    }





	public function getActionsData() {
		return $this->serviceCruds->getActionsData($this->actionName);
	}





	public function actionExist($action) {
		$actionNames = Action::getValidActionsNames();
		return in_array($action, $actionNames);
	}

	public function getValidActionsNames() {
		return Action::getValidActionsNames();
	}



	public function getContextActionNames() {
		$contexts = static::getActionContexts();
		return array_keys($contexts);
	}

	public function hasContextAction($context) {
		$contexts = static::getActionContexts();
		return array_key_exists($context, $contexts);
	}

	public function getActionContexts() {
		return $this->serviceCruds->getActionContexts();
	}

	/*public function getActionsByContext($context) {
		if(!is_string($context)) return [];
		$contexts = static::getActionContexts();
		return array_key_exists($context, $contexts) ? $contexts[$context] : [];
	}*/



}
