<?php
namespace ModelApi\CrudsBundle\Annotation;

// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotation;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotationInterface;

use \ReflectionClass;
use \ReflectionProperty;
use \BadMethodCallException;
use \InvalidArgumentException;
// use \RuntimeException;

abstract class CrudsMultiAnnotation implements CrudsAnnotationInterface {

	protected function extractAnnotations($array) {
		if($array instanceOf CrudsAnnotation) return [$array];
		$annotations = [];
		if(is_array($array)) {
			foreach ($array as $key => $data) {
				$annotations = array_merge($annotations, $this->extractAnnotations($data));
			}
		}
		return array_values($annotations);
	}

	/**
	 * Constructor.
	 * @param array $options Key-value for properties to be defined in this class.
	 */
	public function __construct(array $options = []) {
		$this->annots = $this->extractAnnotations($options);
	}

	public function setEntity(CrudInterface $entity = null) {
		$this->entity = $entity;
		return $this;
	}

	public function toArray(): array {
		$data = [];
		foreach ($this->annots as $key => $annotation) {
			$data[$key] = $annotation;
		}
		return $data;
	}

	public function __serialize(): array {
		return $this->toArray();
	}

	public function __unserialize(array $data): void {
		$this->__construct($data);
	}

	/**
	 * Error handler for unknown property accessor in Annotation class.
	 * @param string $name Unknown property name.
	 * @throws \BadMethodCallException
	 */
	public function __get($name) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, static::class));
	}

	/**
	 * Error handler for unknown property mutator in Annotation class.
	 * @param string $name  Unknown property name.
	 * @param mixed  $value Property value.
	 * @throws \BadMethodCallException
	 */
	public function __set($name, $value) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, static::class));
	}

	public function __toString() { return 'CrudsMultiAnnotation'; }


}
