<?php
namespace ModelApi\CrudsBundle\Annotation;

use ModelApi\CrudsBundle\Annotation\Create;

/**
 * @Annotation
 * @Target({"PROPERTY","ANNOTATION"})
 * 
 * https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class Update extends Create {

	public function __toString() { return 'Update'; }

}
