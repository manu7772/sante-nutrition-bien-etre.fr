<?php
namespace ModelApi\CrudsBundle\Annotation;

// BaseBundle
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// CrudsBundle
use ModelApi\CrudsBundle\Annotation\CrudsAnnotation;

/**
 * @Annotation
 * @Target({"PROPERTY","ANNOTATION"})
 * 
 * https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class Show extends CrudsAnnotation {

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $role = true;

	/**
	 * @var string
	 */
	public $group_show = null;

	/**
	 * @var integer
	 */
	public $order = 0;

	/**
	 * @var string
	 */
	public $label = null;

	/**
	 * @var string
	 */
	public $type = null;

	/**
	 * @var array
	 */
	public $contexts = [];
	// public $contexts = [BaseAnnotateType::FORM_FORALL_CONTEXT];

	public $orm = null;

	/**
	 * Constructor.
	 * @param array $options Key-value for properties to be defined in this class.
	 */
	public function __construct(array $options = []) {
		foreach ($options as $key => $value) {
			switch ($key) {
				case 'type':
					$options[$key] = $this->getType($value);
					break;
			}
		}
		return parent::__construct($options);
	}


	public function getType($typeShortname = null) {
		if(is_string($typeShortname)) return $typeShortname;
		return null; // let doctrine find the goot Type!
		// throw new InvalidArgumentException(sprintf("Unknown type '%s' on annotation '%s'.", $typeShortname, static::class));
	}


	public function __toString() { return 'Show'; }

}
