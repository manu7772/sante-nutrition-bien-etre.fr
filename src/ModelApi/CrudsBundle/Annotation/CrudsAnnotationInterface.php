<?php
namespace ModelApi\CrudsBundle\Annotation;

use ModelApi\AnnotBundle\Annotation\AnnotAnnotationInterface;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;

interface CrudsAnnotationInterface extends AnnotAnnotationInterface {


	public function __serialize(): array;

	public function __unserialize(array $data): void;

	public function __toString();

	public function setEntity(CrudInterface $entity = null);

}
