<?php
namespace ModelApi\CrudsBundle\Annotation;

// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotationInterface;

use \ReflectionClass;
use \ReflectionProperty;
use \BadMethodCallException;
// use \InvalidArgumentException;
// use \RuntimeException;

/**
 * @Annotation
 * @Target("CLASS")
 * 
 * https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class Actions implements CrudsAnnotationInterface {


	const create = "ROLE_EDITOR";
	const copy = false;
	const show = true;
	const update = "ROLE_TRANSLATOR";
	const prefered = 'ROLE_EDITOR';
	const enable = 'ROLE_EDITOR';
	const disable = 'ROLE_EDITOR';
	const softdelete = 'ROLE_ADMIN';
	const unsoftdelete = 'ROLE_SUPER_ADMIN';
	const delete = 'ROLE_SUPER_ADMIN';

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $create;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $copy;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $show;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $update;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $prefered;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $enable;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $disable;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $softdelete;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $unsoftdelete;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $delete;

	/**
	 * @var string
	 */
	public $label = null;

	/**
	 * Constructor.
	 * @param array $options Key-value for properties to be defined in this class.
	 */
	public function __construct(array $options = []) {
		$RC = new ReflectionClass(static::class);
		foreach ($RC->getConstants() as $name => $value) $options[$name] ??= $value;
		foreach ($options as $key => $value) $this->$key = $value;
		return $this;
	}

	/**
	 * Error handler for unknown property accessor in Annotation class.
	 *
	 * @param string $name Unknown property name.
	 *
	 * @throws \BadMethodCallException
	 */
	public function __get($name) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, get_class($this)));
	}

	/**
	 * Error handler for unknown property mutator in Annotation class.
	 *
	 * @param string $name  Unknown property name.
	 * @param mixed  $value Property value.
	 *
	 * @throws \BadMethodCallException
	 */
	public function __set($name, $value) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, get_class($this)));
	}

	public function setEntity(CrudInterface $entity = null) {
		$this->entity = $entity;
		return $this;
	}

	public function toArray(): array {
		$data = [];
		$RC = new ReflectionClass($this);
		foreach ($RC->getProperties(ReflectionProperty::IS_PUBLIC) as $prop) {
			$name = $prop->getName();
			$data[$name] = $this->$name;
		}
		return $data;
	}

	public function __serialize(): array {
		return $this->toArray();
	}

	public function __unserialize(array $data): void {
		$this->__construct($data);
	}

	public function getActions() {
		$actions = [];
		$RC = new ReflectionClass($this);
		$props = $RC->getProperties(ReflectionProperty::IS_PUBLIC);
		foreach ($props as $prop) {
			$name = $prop->getName();
			$actions[$name] = $this->$name;
		}
		return $this->compileActions($actions);
	}

	public static function getDefaults() {
		$RC = new ReflectionClass(static::class);
		$constants = $RC->getConstants();
		$actions = [];
		foreach ($constants as $name => $value) {
			$actions[$name] = $value;
		}
		return static::compileActions($actions);
	}

	protected static function compileActions($actions) {
		foreach ($actions as $name => $value) {
			switch ($name) {
				case 'create':
					// $actions['copy'] ??= $value;
					$actions['symbolic'] ??= $value;
					break;
				case 'show':
					$actions['list'] ??= $value;
					break;
				case 'update':
					$actions['prefered'] ??= $value;
					$actions['enable'] ??= $value;
					$actions['disable'] ??= $value;
					$actions['move'] ??= $value;
					break;
				case 'delete':
					$actions['softdelete'] ??= $value;
					$actions['unsoftdelete'] ??= $value;
					break;
			}
		}
		return $actions;
	}

	public function __toString() { return 'Actions'; }

}
