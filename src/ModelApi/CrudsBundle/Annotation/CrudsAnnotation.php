<?php
namespace ModelApi\CrudsBundle\Annotation;

// CrudsBundle
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotationInterface;

use \ReflectionClass;
use \ReflectionProperty;
use \BadMethodCallException;
use \InvalidArgumentException;
// use \RuntimeException;

abstract class CrudsAnnotation implements CrudsAnnotationInterface {

	/**
	 * Constructor.
	 * @param array $options Key-value for properties to be defined in this class.
	 */
	public function __construct(array $options = []) {
		foreach ($options as $key => $value) $this->$key = $value;
	}

	public function setEntity(CrudInterface $entity = null) {
		$this->entity = $entity;
		return $this;
	}


	public function toArray(): array {
		$data = [];
		$RC = new ReflectionClass($this);
		foreach ($RC->getProperties(ReflectionProperty::IS_PUBLIC) as $prop) {
			$name = $prop->getName();
			$data[$name] = $this->$name;
		}
		return $data;
	}

	public function __serialize(): array {
		return $this->toArray();
	}

	/**
	 * @see https://www.php.net/manual/fr/language.oop5.magic.php#object.unserialize
	 */
	public function __unserialize(array $data): void {
		$this->__construct($data);
	}

	/**
	 * Error handler for unknown property accessor in Annotation class.
	 * @param string $name Unknown property name.
	 * @throws \BadMethodCallException
	 */
	public function __get($name) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, static::class));
	}

	/**
	 * Error handler for unknown property mutator in Annotation class.
	 * @param string $name  Unknown property name.
	 * @param mixed  $value Property value.
	 * @throws \BadMethodCallException
	 */
	public function __set($name, $value) {
		throw new BadMethodCallException(sprintf("Unknown property '%s' on annotation '%s'.", $name, static::class));
	}

	public function __toString() { return 'CrudsAnnotation'; }


}
