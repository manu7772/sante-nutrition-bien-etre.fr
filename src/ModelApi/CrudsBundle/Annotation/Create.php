<?php
namespace ModelApi\CrudsBundle\Annotation;

// BaseBundle
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// CrudsBundle
use ModelApi\CrudsBundle\Annotation\CrudsAnnotation;

/**
 * @Annotation
 * @Target({"PROPERTY","ANNOTATION"})
 * 
 * https://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/
 * https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html
 */
class Create extends CrudsAnnotation {

	const TYPESNAMESPACES = [
		'ModelApi\\BaseBundle\\Form\\Type\\',
		'Symfony\\Component\\Form\\Extension\\Core\\Type\\',
		'Symfony\\Bridge\\Doctrine\\Form\\Type\\',
	];


	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $show = false;

	/**
	 * @var string
	 */
	public $group_show = null;

	/**
	 * @var string
	 * @Enum({true,false,"ROLE_USER","ROLE_TESTER","ROLE_TRANSLATOR","ROLE_EDITOR","ROLE_COLLAB","ROLE_ADMIN","ROLE_SUPER_ADMIN"})
	 */
	public $update = false;

	// /**
	//  * @var mixed
	//  */
	// public $nullable = 'ORM';

	/**
	 * @var string
	 */
	public $type = 'default';

	// /**
	//  * @var string
	//  */
	// public $class = null;

	// /**
	//  * @var mixed
	//  */
	// public $multiple = null;

	// /**
	//  * @var mixed
	//  */
	// public $expanded = null;

	// /**
	//  * @var string
	//  */
	// public $choice_label = 'name';

	/**
	 * @var array
	 */
	public $contexts = [];
	// public $contexts = [BaseAnnotateType::FORM_FORALL_CONTEXT];

	/**
	 * @var string
	 */
	public $property_path = null;

	// /**
	//  * @var boolean
	//  */
	// public $by_reference = false;

	/**
	 * @var string
	 */
	public $getter = null;

	/**
	 * @var string
	 */
	public $label = null;

	/**
	 * @var string
	 */
	public $setter = null;

	/**
	 * @var array
	 */
	public $options = [];

	/**
	 * @var integer
	 */
	public $order = 0;

	// public $typesNamespaces = static::TYPESNAMESPACES;


	/**
	 * Constructor.
	 * @param array $options Key-value for properties to be defined in this class.
	 */
	public function __construct(array $options = []) {
		foreach ($options as $key => $value) {
			switch ($key) {
				// case 'type':
				// 	$options[$key] = $this->getType($value);
				// 	break;
				case 'nullable':
					if(!is_bool($value)) $options[$key] = 'ORM';
					break;
				// case 'choices':
				// 	$value = explode('::', $value);
				// 	// $options['type'] = $this->getType('ChoiceType');
				// 	$options['type'] = EntityType::class;
				// 	$options["class"] = $value[0];
				// 	break;
			}
		}
		return parent::__construct($options);
	}

	public static function getTypesNamespaces() {
		return static::TYPESNAMESPACES;
	}

	// public function getType($typeShortname) {
	// 	if(preg_match('/object\\./i', $typeShortname)) return array('eval' => $typeShortname);
	// 	if(class_exists($typeShortname)) return $typeShortname;
	// 	foreach ($this->typesNamespaces as $type) {
	// 		if(class_exists($type.$typeShortname)) return $type.$typeShortname;
	// 	}
	// 	return null; // let doctrine find the goot Type!
	// 	// throw new InvalidArgumentException(sprintf("Unknown type '%s' on annotation '%s'.", $typeShortname, static::class));
	// }

	public function __toString() { return 'Create'; }

}
