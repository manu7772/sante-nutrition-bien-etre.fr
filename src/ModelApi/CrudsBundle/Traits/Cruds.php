<?php
namespace ModelApi\CrudsBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as GEDMO;
use ModelApi\AnnotBundle\Annotation as ANNOT;
// use Doctrine\Common\Annotations\AnnotationReader;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;

// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\UserInterface;
// use ModelApi\UserBundle\Service\serviceTier;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\Crud;
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Service\serviceCruds;
use ModelApi\CrudsBundle\Annotation\Actions;
use ModelApi\CrudsBundle\Annotation\Annotations;
use ModelApi\CrudsBundle\Annotation\Create;
use ModelApi\CrudsBundle\Annotation\Creates;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotation;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotationInterface;
use ModelApi\CrudsBundle\Annotation\CrudsMultiAnnotation;
use ModelApi\CrudsBundle\Annotation\Show;
use ModelApi\CrudsBundle\Annotation\Shows;
use ModelApi\CrudsBundle\Annotation\Update;
use ModelApi\CrudsBundle\Annotation\Updates;

use \ReflectionClass;
use \Exception;

Trait Cruds {


	/**
	 * CRUDS for entity
	 * @var integer
	 * @ORM\ManyToMany(targetEntity="ModelApi\CrudsBundle\Entity\Crud", cascade={"persist"}, orphanRemoval=true, fetch="EXTRA_LAZY")
	 * @ORM\JoinTable(
	 * 		joinColumns={ @ORM\JoinColumn(nullable=true, unique=false, onDelete="CASCADE") }
	 * )
	 */
	protected $cruds;
	protected $serviceCruds;

	protected $user;


	public function setServiceCruds(serviceCruds $serviceCruds) {
		if(!($this instanceOf CrudInterface)) throw new Exception("Error ".__METHOD__."(): can not load serviceCruds as this entity is not instance of CrudInterface!", 1);
		$this->serviceCruds = $serviceCruds;
		// $this->setUser($this->serviceCruds->getCurrentUserOrNull());
		// if(!$this->hasCruds()) $this->checkCruds();
		return $this;
	}

	public function checkCruds() {
		if($this->serviceCruds instanceOf serviceCruds) {
			// foreach ($this->cruds as $crud) {
				# code...
			// }
		} else {
			throw new Exception("Error ".__METHOD__."(): serviceCruds is missing!", 1);
		}
		return $this;
	}

	public function getCrudContexts() {
		return $this->serviceCruds->getContexts();
	}

	public function getCrudMasterContexts() {
		return $this->serviceCruds->getMasterContexts();
	}

	public function getCrudContextNames() {
		return $this->serviceCruds->getContextNames();
	}

	public function getCrudMasterContext($name) {
		$contexts = $this->getCrudContexts();
		return isset($contexts[$name]) ? $contexts[$name] : null;
	}

	public function getCruds($asAnnotations = false) {
		if($asAnnotations) {
			$cruds = [];
			foreach ($this->cruds as $crud) $cruds[] = $crud->getAnnotation($this);
			return $cruds;
		} else {
			foreach ($this->cruds as $crud) $crud->setEntity($this);
			return $this->cruds;
		}
	}

	public function addCrud(Crud $crud) {
		if(!$this->cruds->contains($crud)) $this->cruds->add($crud);
		return $this;
	}

	public function removeCrud(Crud $crud) {
		$this->cruds->removeElement($crud);
		return $this;
	}

	public function removeCruds() {
		foreach ($this->getCruds() as $crud) $this->removeCrud($crud);
		return $this;
	}

	public function setCruds($cruds) {
		$this->removeCruds();
		foreach ($cruds as $crud) $this->addCrud($crud);
		return $this;
	}

	public function hasCruds() {
		return $this->cruds->count() > 0;
	}


	// public function setUser(UserInterface $user = null) {
	// 	$this->user = $user;
	// 	return $this;
	// }

	public function getUser() {
		return $this->serviceCruds->getCurrentUserOrNull();
	}

	public function isGranted($action = 'read', $contexts = [], User $user = null) {
		$user ??= $this->getUser();
		switch ($action) {
			case 'create':
				return $user instanceOf User;
				break;
			case 'read':
				return $this->isReadable($user, $contexts);
				break;
			case 'update':
				return $this->isUpdatable($user, $contexts);
				break;
			case 'delete':
				return $this->isDeletable($user, $contexts);
				break;
		}
		throw new Exception("Error ".__METHOD__."(): action is invalid. Expected ".json_encode(['create','read','update','delete']).". Got ".json_encode($action)."!", 1);
	}

	// public function roleShow() {
	// 	if($this instanceOf Directory && $this->isRootSystem()) return UserInterface::ROLE_SUPER_ADMIN; 
	// 	return UserInterface::IS_AUTHENTICATED_ANONYMOUSLY;
	// }



	/**
	 * Is readable
	 * @return boolean
	 */
	public function isReadable(User $user = null, $contexts = []) {
		return true;
	}

	/**
	 * Is updatable
	 * @return boolean
	 */
	public function isUpdatable(User $user = null, $contexts = []) {
		return true;
	}

	/**
	 * Is deletable
	 * @return boolean
	 */
	public function isDeletable(User $user = null, $contexts = []) {
		$user ??= $this->getUser();
		if(!($user instanceOf User)) return false;
		// 1. ROLE_SUPER_ADMIN
		if($user->isSuperadmin()) return true;
		// 2. ANNOTATION
		// $deleteAnnotation = $this->getFilteredAnnotations($this, [Actions::class], $contexts, )
		// 3. ROLE_ADMIN
		if($user->hasRole('ROLE_ADMIN')) {
			if(!in_array($this->getShortname(), ['Message'])) return true;
		}
		// 4. EXCEPTIONS
		if($this instanceOf User) return $this->getUser() === $this;
		if($this instanceOf Basedirectory) {
			if($this->isDirstatic() || $this->hasDirchild()) return false;
		}
		// 5. USER
		return $this->getOwner() === $user;
	}

	// public function getAnnotations($annots = [], $types = [], $refresh = false) {
	// 	return serviceClasses::getAllAnnotations($this, $annots, $types, $refresh);
	// 	// $annotations = $this->serviceCruds->getAnnotations($this);
	// }

	public function getFilteredAnnotations($annotationsList = [], $types = [], callable $filterCallback = null, $refresh = false) {
		return $this->serviceCruds->getFilteredAnnotations($this, $annotationsList, $types, $filterCallback, $refresh);
	}

	public function getForm_CrudsAnnotations(array $options, $refresh = false) {
		BaseAnnotateType::normalizeContexts($options['cruds_context']);
		// echo('<div class="well well-sm"><h4>Annotations for '.$this->getShortname().'</h4><pre>');
		// var_dump($options['cruds_context']);
		// echo('</pre></div>');
		// $annotations = $this->getAnnotations([], 'property');
		$found_annotations = $this->getFilteredAnnotations([], 'property', null, $refresh);
		$properties = [];
		foreach ($found_annotations['properties'] as $property => $annotations) {
			$annots = [
				'name' => $property,
				'action' => strtolower($options['cruds_action']),
				'context' => $options['cruds_context'],
				// 'property' => $property,
				'annotations' => [
					'cruds' => [],
					'orm' => [],
					'annot' => [],
					'gedmo' => [],
					'others' => [],
				],
			];
			$crudAuthorized[$property.'_'.$annots['action']] = true;
			// if($property === "roles" && $this->getShortName() === "User") {
			// 	echo('<div class="well well-sm"><h4>Annotations for '.$this->getShortname().'::'.$property.'</h4><pre>');
			// 	var_dump($annotations);
			// 	echo('</pre></div>');
			// }
			foreach ($annotations as $annotation) {
				$RC = new ReflectionClass($annotation);
				$annotName = strtolower($RC->getShortName());
				$annotNamespace = $RC->getNamespaceName();

				if($annotation instanceOf CrudsAnnotationInterface) {
					// $serviceCruds = $this->serviceCruds;
					// $entity = $this;
					// $action = $annots['action'];
					$found_cruds = $this->serviceCruds->getUniqueCruds(
						$annotation,
						$annots['action'],
						// function($annot) use ($action) { return strtolower($annot->__toString()) === strtolower($action); }
						// function($annot) use ($annots, $serviceCruds, $entity) {
						// 	if(!preg_match('#'.$annots['action'].'#i', $annot->__toString()) || !$this->serviceCruds->compareContexts($annots['context'], $annot->contexts)) return false;
						// 	$serviceCruds->computeAttributeGrants($annot, $annots, $entity);
						// 	return $annot->show;
						// }
					);
					// if($property === "roles" && $this->getShortName() === "User") {
					// 	echo('<div class="panel panel-info"><div class="panel-heading">FOUND Cruds for '.$this->getShortname().'::<strong>'.$property.'</strong></div><div class="panel-body">');
					// 	echo('<p>Action '.$annots['action'].' ---> <strong>Form contexts: '.json_encode($annots['context']).'</strong></p>');
					// 	echo('<pre>');
					// 	var_dump($found_cruds);
					// 	echo('</pre>');
					// 	echo('</div></div>');
					// }
					$cruds = [];
					$removeds = [];
					foreach ($found_cruds as $crud) {
						// if(!preg_match('#'.$annots['action'].'#i', $annot->__toString()) || !$this->serviceCruds->compareContexts($annots['context'], $annot->contexts)) continue;
						if($this->serviceCruds->compareContexts($annots['context'], $crud->contexts, $property)) {
							$this->serviceCruds->computeAttributeGrants($crud, $annots, $this);
							$name = $crud->__toString();
							if($crud->show && !isset($cruds[$name]) && !in_array($name, $removeds)) {
								$cruds[$name] = $crud;
							} else {
								if(isset($cruds[$name])) unset($cruds[$name]);
								$removeds[$name] = $name;
								unset($annots['annotations']['cruds'][strtolower($crud->__toString())]);
								$crudAuthorized[$property.'_'.$annots['action']] = false; // STOP Crud of action
							}
						}
						// if($property === "roles" && $this->getShortName() === "User") {
						// 	echo('<div class="well well-sm"><h4>Annotations for '.$this->getShortname().'::'.$property.' -> SHOW: '.json_encode($crud->show).'</h4><pre>');
						// 	var_dump($crud);
						// 	echo('</pre></div>');
						// }
					}
					if($crudAuthorized[$property.'_'.$annots['action']] && count($cruds) > 0) {
						$crud = reset($cruds);
						// if($property === "roles" && $this->getShortName() === "User") {
						// 	echo('<div class="panel panel-info"><div class="panel-heading">Added Crud for '.$this->getShortname().'::<strong>'.$property.'</strong></div><div class="panel-body">');
						// 	echo('<p>Action '.$annots['action'].' ---> <strong>Form contexts: '.json_encode($annots['context']).'</strong> ---> Annot contexts: '.json_encode($crud->contexts).'</p>');
						// 	echo('</div></div>');
						// }
						$annots['annotations']['cruds'][strtolower($crud->__toString())] = $crud;
					}
				} else if($annotNamespace === 'Doctrine\\ORM\\Mapping') {
					$annots['annotations']['orm'][$annotName] ??= [];
					$annots['annotations']['orm'][$annotName][] = $annotation;
				} else if($annotNamespace === 'Gedmo\\Mapping\\Annotation') {
					$annots['annotations']['gedmo'][$annotName] ??= [];
					$annots['annotations']['gedmo'][$annotName][] = $annotation;
				} else if($annotNamespace === 'ModelApi\\AnnotBundle\\Annotation') {
					$annots['annotations']['annot'][$annotName] ??= [];
					$annots['annotations']['annot'][$annotName][] = $annotation;
				} else {
					$annots['annotations']['others'][] = get_class($annotation);
				}
			}
			// echo('<div class="well well-sm"><h4>Annotations for '.$this->getShortname().'</h4><pre>');
			// var_dump($annots['annotations']['cruds']);
			// echo('</pre></div>');
			if(count($annots['annotations']['cruds']) > 0) $properties[$property] = $annots;
			// if(count($annots['annotations']['cruds']) > 0) {
				// has CRUDS, but now, verify context
				// $translation_context = array_intersect(['translation'], $options['cruds_context']) && (isset($annots['annotations']['annot']["translatable"]) || isset($annots['annotations']['gedmo']["translatable"]));
				// foreach ($annots['annotations']['cruds'] as $crud) {
					// $this->serviceCruds->computeAttributeGrants($crud, $annots, $this);
					// if(array_intersect($options['cruds_context'], $crud->contexts) || in_array(BaseAnnotateType::FORM_FORALL_CONTEXT, $crud->contexts) || in_array(BaseAnnotateType::USER_HIGH_CONTEXT, $options['cruds_context']) || $translation_context && $crud->show) {
						// $properties[$property] = $annots;
					// }
				// }
			// }
		}
		// Reorder attributes
		uasort($properties, function($a, $b) {
			if($a['annotations']['cruds'][$a['action']]->order === $b['annotations']['cruds'][$b['action']]->order) return 0;
			if($a['annotations']['cruds'][$a['action']]->order <= 0) return 1;
			if($b['annotations']['cruds'][$b['action']]->order <= 0) return -1;
			return $a['annotations']['cruds'][$a['action']]->order < $b['annotations']['cruds'][$b['action']]->order ? -1 : 1;
		});
		return $properties;
	}



}