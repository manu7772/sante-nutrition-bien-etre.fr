<?php
namespace ModelApi\CrudsBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use JMS\Serializer\SerializationContext;
// use Doctrine\Common\Inflector\Inflector;
use Doctrine\ORM\Mapping;
/*use Doctrine\ORM\Mapping\ClassMetadata;*/
use Doctrine\Common\Annotations\AnnotationReader;

// CrudsBundle
use ModelApi\CrudsBundle\Entity\Action;
// use ModelApi\CrudsBundle\Entity\Actions;
use ModelApi\CrudsBundle\Entity\Crud;
use ModelApi\CrudsBundle\Entity\CrudInterface;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotation;
use ModelApi\CrudsBundle\Annotation\CrudsAnnotationInterface;
use ModelApi\CrudsBundle\Annotation\CrudsMultiAnnotation;
use ModelApi\CrudsBundle\Annotation\Actions;
use ModelApi\CrudsBundle\Annotation\Show;
use ModelApi\CrudsBundle\Annotation\Create;
use ModelApi\CrudsBundle\Annotation\Update;
// BaseBundle
use ModelApi\BaseBundle\Entity\LaboClassMetadata;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\servicesBaseEntityInterface;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// UserBundle
// use ModelApi\UserBundle\Service\serviceRoles;
use ModelApi\UserBundle\Service\serviceGrants;
// use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Entity\User;

// use ModelApi\CrudsBundle\Annotation\Actions as CRUD_namespaceName;			// CRUD_namespaceName
// use ModelApi\AnnotBundle\Annotation\AttachUser as Annot_namespaceName;		// Annot_namespaceName
// use Doctrine\ORM\Mapping\Column as ORM_namespaceName;						// ORM_namespaceName
// use Gedmo\Mapping\Annotation\Translatable as Gedmo_namespaceName;			// Gedmo_namespaceName

use \ReflectionClass;
use \ReflectionProperty;
use \Exception;

class serviceCruds implements servicesBaseEntityInterface {

	use \ModelApi\BaseBundle\Service\baseEntityService;

	const ENTITY_CLASS = Crud::class;
	const CACHE_FOLDER = 'annotations';

	protected $container;
	// protected $serviceUser;
	// protected $serviceRoles;
	protected $serviceGrants;
	//protected $serviceContext;
	protected $serviceEntities;
	protected $reader;
	protected $actions_data;
	protected $actions_contexts;
	protected $entitys_info;
	// protected $data;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceCache = $this->container->get(serviceCache::class);
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->serviceGrants = $this->container->get(serviceGrants::class);
		//$this->serviceContext = $this->container->get(serviceContext::class);
		$this->reader = new AnnotationReader();
		$this->entitys_info = [];
		return $this;
	}

	public function getServiceGrants() {
		return $this->serviceGrants;
	}

	public function getActionsData() {
		// if(isset($this->actions_data) && !empty($this->actions_data)) return $this->actions_data;
		$this->actions_data ??= $this->getServiceParameter('actions.data');
		// foreach ($this->actions_data as $actionName => $data) {
			# code...
		// }
		// echo('<pre><h3>Actions data</h3><div>'); var_dump($this->actions_data); die('</div></pre>');
		return $this->actions_data;
	}

	protected function getEntityInfoForActions($entity) {
		// Entity
		$entity_info['entity'] = is_string($entity) ? $this->container->get(serviceEntities::class)->getModel($entity, true) : $entity;
		// Classes
/*		if($entity_info['entity'] instanceOf ClassMetadata) {
			$RC = $entity_info['entity']->getReflectionClass();
			$entity_info['classes'] = [$RC->getName(), $RC->getShortName(), strtolower($RC->getShortName())];
		} else if(is_object($entity_info['entity']) && method_exists($entity_info['entity'], 'isInstanceOf') && $entity_info['entity']->isInstanceOf(CrudInterface::class)) {*/
		if(is_object($entity_info['entity']) && method_exists($entity_info['entity'], 'isInstanceOf') && $entity_info['entity']->isInstanceOf(CrudInterface::class)) {
			$entity_info['classes'] = [$entity_info['entity']->getClassname(), $entity_info['entity']->getShortname(false), $entity_info['entity']->getShortname(true)];
		} else {
			throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Entity ".(is_object($entity_info['entity']) ? get_class($entity_info['entity']) : json_encode($entity_info['entity']))." is not valid for generate Actions data!", 1);
		}
		// Context groups
		if(empty($entity_info['context_groups'])) {
			if($entity->isInstanceOf(CrudInterface::class)) $entity_info['context_groups'] = ['@entity'];
			if($entity->isInstanceOf(LaboClassMetadata::class)) $entity_info['context_groups'] = ['@model'];
		}
		if(is_string($entity_info['context_groups'])) $entity_info['context_groups'] = [$entity_info['context_groups']];
		$entity_info['context_groups'] = array_map(function($context) {
			return preg_replace('/^@/', '', $context);
			// return !preg_match('/^@/', $context) '@'.$context : $context;
		}, $entity_info['context_groups']);
		// Identifier
		$entity_info['identifier'] = $entity_info['entity'] instanceOf LaboClassMetadata ? $entity_info['entity']->getClassIdentifier() : $entity_info['entity']->getBddid();
		return $entity_info;
	}

	public function getActionData($actionName) {
		$actions_data = $this->getActionsData();
		return $actions_data[$actionName] ?? null;
	}

	public function getActionsNames() {
		return array_keys($this->getActionsData());
	}

	public function getActionsDataByEntityAndContextGroups($entity, $contextGroups = []) {
		$entity_info = $this->getEntityInfoForActions($entity);
		$contextGroups = array_map(function($context) {
			return preg_replace('/^@/', '', $context);
			// return !preg_match('/^@/', $context) '@'.$context : $context;
		}, (array)$contextGroups);
		// if(isset($this->entitys_info[$entity_info['identifier']])) return $this->entitys_info[$entity_info['identifier']];
		$this->entitys_info[$entity_info['identifier']] ??= array_filter($this->getActionsData(), function($data) use ($entity_info, $contextGroups) {
			if(!empty($entity_info['context_groups'])) {
				if(!count(array_intersect((array)$contextGroups, $data['context_groups']))) return false;
			}
			if(count($data['classes'])) {
				return count(array_intersect($data['classes'], $entity_info['classes']));
			}
			return true;
		});
		// echo('<pre><h3>Actions data for contexts '.json_encode($contextGroups).' and classes '.json_encode($entity_classes).'</h3><div>'); var_dump($this->entitys_info[$entity_info['identifier']]); die('</div></pre>');
		return $this->entitys_info[$entity_info['identifier']];
	}

	/*public function getNewAction($actionName, $entity) {
		return new Action($actionName, $entity, $this->container);
	}*/

	public function isValidActionName($actionName) {
		return array_key_exists($actionName, $this->getActionsData());
	}

	public function getContexts() {
		return $this->actions_contexts ??= $this->getServiceParameter('actions.contexts');
	}

	public function getMasterContexts() {
		return array_unique(array_values($this->getContexts()));
	}

	public function getContextNames() {
		return array_keys($this->getContexts());
	}



	public function getCurrentUserOrNull() {
		return $this->container->get(serviceContext::class)->getUser();
	}

	/**
	 * Test expression in CRUDS annotations
	 * @param string &$expression
	 * @return $expression string has changed
	 */
	public function evaluateExpression(&$expression, $entity) {
		$mem = $expression;
		if(is_string($expression) && preg_match('/(?!\\w)(\\s*)(object|user|environment|entreprise)\\./i', $expression)) {
			$serviceContext = $this->container->get(serviceContext::class);
			$user = $serviceContext->getUser();
			$environment = $serviceContext->getEnvironment();
			$entreprise = $serviceContext->getEntreprise();
			$test_string = preg_replace(['/(?!\\w)(\\s*)object\\./i','/(?!\\w)(\\s*)user\\./i','/(?!\\w)(\\s*)environment\\./i','/(?!\\w)(\\s*)entreprise\\./i'], ['$entity->','$user->','$environment->','$entreprise->'], $expression);
			//echo('<h2>Test string: '.json_encode($test_string !== $expression).'</h2><p>'.$expression.'</p><p><strong>'.$test_string.'</strong></p>');
			if($test_string !== $expression) eval('$expression = '.$test_string.';');
		}
		return $mem !== $expression;
	}

	public function computeAttributeGrants(CrudsAnnotation $crud, $annots, $entity, User $user = null) {
		// Special register User
		if(empty($user) && $entity instanceOf User && empty($entity->getId())) $user = $entity;
		
		$attributes = ['show','update','nullable','getter','setter'];
		foreach ($attributes as $attribute) if(isset($crud->$attribute)) {
			$expression = $crud->$attribute;
			if($this->evaluateExpression($expression, $entity)) $crud->$attribute = $expression;
			switch ($attribute) {
				case 'show':
				case 'update':
					if(is_string($crud->$attribute)) $crud->$attribute = $this->serviceGrants->isGranted($crud->$attribute, $user);
					// $crud->$attribute = true;
					break;
				case 'nullable':
					if(!is_bool($crud->$attribute)) {
						// nullable not defined, use ORM's attribute
						if(count($annots['annotations']['orm']) > 0) {
							foreach ($annots['annotations']['orm'] as $annottype => $ormlist) {
								foreach ($ormlist as $orm) {
									if(isset($orm->nullable)) {
										$crud->$attribute = $orm->nullable;
										break 2;
									}
								}
							}
						}
					}
					if(!is_bool($crud->$attribute)) $crud->$attribute = true;
					break;
				case 'getter':
					// if(!is_string($crud->$attribute) || !method_exists($entity, $crud->$attribute)) {
						$getter = $this->container->get(serviceClasses::class)->getPropertyMethods($annots["name"], $entity, $crud->$attribute);
						$crud->$attribute = $getter;
					// }
					break;
				case 'setter':
					// if(!is_string($crud->$attribute) || !method_exists($entity, $crud->$attribute)) {
						$setter = $this->container->get(serviceClasses::class)->getPropertyMethods($annots["name"], $entity, $crud->$attribute);
						$crud->$attribute = $setter;
					// }
					break;
			}
		}
		return $crud;
	}

	public function computeActionGrants($grants = null, User $user = null) {
		if(!($user instanceOf User)) $user = $this->container->get(serviceContext::class)->getUser();
		if(null == $grants) $grants = Actions::getDefaults();
		foreach ($grants as $key => $grant) {
			if(is_string($grant)) $grants[$key] = $this->serviceGrants->isGranted($grant, $user);
		}
		return $grants;
	}

	public function getEntitiesCruds(User $user = null) {
		if(!($user instanceOf User)) $user = $this->container->get(serviceContext::class)->getUser();
		$entities = $this->serviceEntities->getEntitiesNames(true);
		foreach ($entities as $classname => $shortname) {
			$entities[$classname] = [
				'user' => [
					'id' => $user->getId(),
					'username' => $user->getUsername(),
				],
				'classname' => $classname,
				'shortname' => $shortname,
				'cruds' => $this->getEntityActionsGrants($classname, $user),
			];
		}
		return $entities;
	}

	public function getEntityActionsGrants($entity, User $user = null) {
		$classname = $this->serviceEntities->getClassnameByAnything($entity);
		if(!is_string($classname)) return [];
		$grants = null;
		if(is_object($entity) && method_exists($entity, 'getCruds')) {
			$grants = $entity->getCruds();
		}
		if(empty($grants) || $grants->isEmpty()) {
			$classname = $this->serviceEntities->getClassnameByAnything($entity);
			if(null === $classname) {
				if(is_object($entity)) $entity = get_class($entity);
				throw new Exception("Error line ".__LINE__." ".__METHOD__."(): Entity ".json_encode($entity)." does not exist!", 1);
			}
			$RC = new ReflectionClass($classname);
			$annotations = $this->reader->getClassAnnotations($RC);
			$grants = null;
			foreach ($annotations as $annotation) {
				if($annotation instanceOf Actions) {
					$grants = $annotation->getActions();
				}
			}
		}
		// Compute grants
		return $this->computeActionGrants($grants, $user);
	}

	public static function compareContexts($formContexts, $crudsContexts, $property = null) {
		if(is_string($formContexts)) $formContexts = BaseAnnotateType::contextsToArray($formContexts);
		if(is_string($crudsContexts)) $crudsContexts = BaseAnnotateType::contextsToArray($crudsContexts);
		if(!is_array($formContexts)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): formContexts parameter must be array! Got ".gettype($formContexts)."!", 1);
		if(!is_array($crudsContexts)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): crudsContexts parameter must be array! Got ".gettype($crudsContexts)."!", 1);
		if(empty($formContexts)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): formContexts parameter can not be empty!", 1);
		// if(empty($crudsContexts)) return true;

		$test = reset($crudsContexts);
		if(is_array($test)) {
			foreach ($crudsContexts as $subCrudsContexts) {
				if(!is_array($subCrudsContexts)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): crudsContexts that contains array of contexts can not contain anything else than arrays!", 1);
				if(static::compareContexts($formContexts, $subCrudsContexts, $property)) return true;
			}
		} else {
			// Complete user contexts
			$formContexts = BaseAnnotateType::completeUserContexts($formContexts);
			// Test User contexts
			$user_formContexts = BaseAnnotateType::filterUserContexts($formContexts);
			$user_crudsContexts = BaseAnnotateType::filterUserContexts($crudsContexts);
			$test1 = count(array_intersect($user_formContexts, $user_crudsContexts)) > 0;
			if(in_array(BaseAnnotateType::USER_HIGH_CONTEXT, $user_formContexts)) $test1 = true;
			if(empty($user_crudsContexts)) $test1 = true;

			// echo('<div class="panel panel-primary"><strong class="panel-heading">Intersect Form/cruds contexts for '.$property.' with '.json_encode($formContexts).'</strong><div class="panel-body"><pre>');
			// var_dump($user_formContexts);
			// echo('<br>');
			// var_dump($user_crudsContexts);
			// echo('<br>');
			// var_dump($test1);
			// echo('</pre></div></div>');
			// echo('<hr>');

			// Test Form contexts
			$form_formContexts = BaseAnnotateType::filterFormContexts($formContexts);
			$form_crudsContexts = BaseAnnotateType::filterFormContexts($crudsContexts);
			$test2 = count(array_intersect($form_crudsContexts, $form_formContexts)) > 0 || (empty($form_crudsContexts) && empty($form_formContexts));
			if(in_array(BaseAnnotateType::FORM_FORALL_CONTEXT, $form_crudsContexts)) $test2 = true;
			if(in_array('default', $form_formContexts) && empty($form_crudsContexts)) $test2 = true;

			// echo('<div class="panel panel-primary"><strong class="panel-heading">Intersect Form/cruds contexts with '.json_encode($formContexts).'</strong><div class="panel-body"><pre>');
			// var_dump($form_formContexts);
			// echo('<br>');
			// var_dump($form_crudsContexts);
			// echo('<br>');
			// var_dump($test2);
			// echo('</pre></div></div>');

			return $test1 && $test2;
		}
		return false;
	}

	public static function getCrudsFromCrudMultiAnnotation(CrudsAnnotationInterface $annotation, callable $filterCallback = null) {
		$cruds = [];
		if($annotation instanceOf CrudsMultiAnnotation) {
			foreach ($annotation->annots as $crudsAnnotation) $cruds[] = $crudsAnnotation;
		} else {
			$cruds[] = $annotation;
		}
		return is_callable($filterCallback) ? array_filter($cruds, $filterCallback) : $cruds;
	}

	/**
	 * Is action on entity granted to User
	 * @param string $action
	 * @param mixed $entity (object or classname)
	 * @param User $user = null
	 * @return boolean
	 */
	public function isEntityActionGranted($action, $entity, User $user = null) {
		$actions = $this->getEntityActionsGrants($entity, $user);
		return isset($actions[$action]) ? $actions[$action] : true;
	}


	public static function getUniqueCruds(CrudsAnnotationInterface $annotation, $action, callable $filterCallback = null) {
		$cruds = [];
		if($annotation instanceOf CrudsMultiAnnotation) {
			foreach ($annotation->annots as $crudsAnnotation) {
				$cruds = array_merge($cruds, static::getUniqueCruds($crudsAnnotation, $action, $filterCallback));
			}
		} else if($annotation instanceOf CrudsAnnotationInterface) {
			if(preg_match('#^'.$action.'$#i', $annotation->__toString())) $cruds[] = $annotation;
		}
		return is_callable($filterCallback) ? array_filter($cruds, $filterCallback) : $cruds;
	}

	public function getAnnotations($entity, $annotationsList = [], $types = [], $refresh = false) {
		if(is_object($entity)) $entity = get_class($entity);
		if(is_string($annotationsList)) $annotationsList = [$annotationsList];
		$cacheId = 'ent_annots_'.md5($entity.json_encode($annotationsList).json_encode($types));
		return $this->serviceCache->getCacheData(
			$cacheId,
			function() use ($entity, $annotationsList, $types) {
				return serviceClasses::getAllAnnotations($entity, $annotationsList, $types);
			},
			static::CACHE_FOLDER,
			serviceCache::NO_CONTEXT,
			null,
			serviceCache::CACHE_INFINITE,
			$refresh
		);
	}

	public function getFilteredAnnotations($entity, $annotationsList = [], $types = [], callable $filterCallback = null, $refresh = false) {
		$annotations = $this->getAnnotations($entity, $annotationsList, $types, $refresh);
		// filter
		if(array_key_exists('properties', $annotations) && is_callable($filterCallback)) {
			foreach ($annotations['properties'] as $property => $annots) {
				$annotations['properties'][$property] = array_filter($annots, $filterCallback);
			}
			$annotations['properties'] = array_filter($annotations['properties'], function ($annots) { return count($annots); });
		}
		if(array_key_exists('methods', $annotations) && is_callable($filterCallback)) {
			foreach ($annotations['methods'] as $method => $annots) {
				$annotations['methods'][$method] = array_filter($annots, $filterCallback);
			}
			$annotations['methods'] = array_filter($annotations['methods'], function ($annots) { return count($annots); });
		}
		if(array_key_exists('class', $annotations) && is_callable($filterCallback)) {
			$annotations['class'] = array_filter($annotations['class'], $filterCallback);
		}
		return $annotations;
	}

	public function clearAnnotations() {
		$cacheIds = ['ent_annots_'];
		foreach ($cacheIds as $cacheId) {
			$this->serviceCache->clearCacheData($cacheId, static::CACHE_FOLDER, serviceCache::NO_CONTEXT, null, false);
		}
		return $this;
	}

	/**
	 * Get Cruds annotations for form Type
	 * @param object $entity
	 * @param array $options
	 * @return array
	 */
	public function getAction_CrudsAnnotations($entity, array $contexts = [], $crud_shortname = 'Show', $fields = true) {
		$RC = new ReflectionClass($entity);
		$entity_shortname = $RC->getShortName();
		$entity_classname = $RC->getName();
		$props = $RC->getProperties();
		$properties = [];
		foreach ($props as $property) if(true === $fields || in_array($property->getName(), $fields)) {
			$name = $property->getName();
			$annotations = $this->reader->getPropertyAnnotations($property);
			// echo('<pre><h3>Annotations :</h3>');var_dump($annotations);echo('</pre>');
			$foundCrudsAnnotations = array_filter($annotations, function($annotation) use ($crud_shortname) {
				return $annotation instanceOf CrudsAnnotation && $crud_shortname === $annotation->__toString();
			});
			// echo('<pre><h3>Cruds :</h3>');var_dump($foundCrudsAnnotations);echo('</pre>');
			if(!empty($foundCrudsAnnotations)) {
				$annots = [
					'name' => $property->getName(),
					'action' => $crud_shortname,
					'contexts' => $contexts,
					// 'property' => $property,
					'cruds' => [],
				];
				foreach ($foundCrudsAnnotations as $annotation) {
					$RC = new ReflectionClass($annotation);
					$annotName = $RC->getShortName();
					$annot_contexts = array_intersect($contexts, $annotation->contexts);
					if(!empty($annot_contexts) || in_array("all", $annotation->contexts)) {
						if(!is_string($annotation->label)) $annotation->label = 'field.'.$name;
						foreach ($annotations as $annot) if(preg_match('/^Doctrine\\\\ORM\\\\Mapping\\\\/', get_class($annot))) {
							$RC = new ReflectionClass($annot);
							switch ($RC->getShortName()) {
								case 'Column':
									if(!is_string($annotation->type)) $annotation->type = $annot->type;
									break;
								case 'OneToOne':
								case 'OneToMany':
								case 'ManyToOne':
								case 'ManyToMany':
									if(!is_string($annotation->type)) $annotation->type = $RC->getShortName();
									break;
							}
							if(!isset($annotation->orm)) $annotation->orm = [];
							$annotation->orm[$RC->getShortName()] = $annot;
						}
						$annots['cruds'][$annotName] = $annotation;
					}
				}
				$properties[$name] = $annots;
			}
		}
		// REORDER properties
		uasort($properties, function($a, $b) use ($crud_shortname) {
			if($a['cruds'][$crud_shortname]->order === $b['cruds'][$crud_shortname]->order) return 0;
			if($a['cruds'][$crud_shortname]->order <= 0) return 1;
			if($b['cruds'][$crud_shortname]->order <= 0) return -1;
			return $a['cruds'][$crud_shortname]->order < $b['cruds'][$crud_shortname]->order ? -1 : 1;
		});
		return ['shortname' => $entity_shortname, 'classname' => $entity_classname, 'fields' => $properties];
	}


	public function checkCrudsEntities(/*$refresh = false*/) {
		$entityManager = $this->serviceEntities->getEntityManager();
		// if($refresh) {
			// Remove all Cruds
			$cruds = $this->getRepository()->findAll();
			foreach ($cruds as $crud) {
				$entityManager->remove($crud);
			}
			$entityManager->flush();
			unset($cruds);
			// Control
			$cruds = $this->getRepository()->findAll();
			if(count($cruds)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): while refresh, not all Cruds could be deleted!", 1);
			unset($cruds);
		// }
		$createds = 0;
		$entities = $this->serviceEntities->getEntitiesNames(true);
		foreach ($entities as $classname => $shortname) {
			$all_annotations = $this->getAnnotations($classname, [CrudsAnnotationInterface::class], [], true);
			foreach ($all_annotations as $type => $annotations) {
				$type_ok = null;
				if(preg_match('/^propert/', $type)) $type_ok = 'property';
				if(preg_match('/^method/', $type)) $type_ok = 'method';
				if(preg_match('/^class/', $type)) $type_ok = 'class';
				switch ($type) {
					case 'class':
						// Classes
						foreach ($annotations as $name => $annotation) {
							$new_crud = $this->createNew([$classname, $annotation, $type_ok]);
							if($new_crud->isValid()) {
								$entityManager->persist($new_crud);
								$createds++;
							} else {
								throw new Exception("Error line ".__LINE__." ".__METHOD__."(): new Crud is not valid! Params are ".json_encode([$lassname, $annotation->__toString(), $type_ok]).".", 1);
							}
						}
						break;
					default:
						// Properties & Methods
						// deploy multiAnnotations
						foreach ($annotations as $name => $crudAnnotations) {
							foreach ($crudAnnotations as $key => $annotation) {
								if($annotation instanceOf CrudsMultiAnnotation) {
									// unset($annotations[$name][$key]);
									$annotations[$name][$key] = [];
									foreach ($annotation->toArray() as $annot) {
										if(!($annot instanceOf CrudsAnnotationInterface)) throw new Exception("Error line ".__LINE__." ".__METHOD__."(): multiAnnotation contains elements that are not CrudsAnnotationInterface!", 1);
										$annot_class = new ReflectionClass($annotation);
										$annot_class = $annot_class->getShortname();
										// if(isset($annotations[$name][$key]) && !is_array($annotations[$name][$key])) $annotations[$name][$key] = [$annotations[$name][$key]];
										// 	else $annotations[$name][$key] ??= [];
										$annotations[$name][$key][] = $annot;
									}
								}
							}
						}
						$annotations = array_filter($annotations, function($annot) { return !($annot instanceOf CrudsMultiAnnotation); });
						// Create Cruds
						foreach ($annotations as $name => $crudAnnotations) {
							foreach ($crudAnnotations as $key => $annotation) {
								if(!is_array($annotation)) $annotation = [$annotation];
								foreach ($annotation as $annot) {
									$new_crud = $this->createNew([$classname, $annot, $type_ok, $name]);
									if($new_crud->isValid()) {
										$entityManager->persist($new_crud);
										$createds++;
									} else {
										throw new Exception("Error line ".__LINE__." ".__METHOD__."(): new Crud is not valid! Params are ".json_encode([$classname, $annot->__toString(), $type_ok, $name]).".", 1);
									}
								}
							}
						}
						break;
				}
			}
		}
		if($createds > 0) {
			$entityManager->flush();
			$this->container->get(serviceFlashbag::class)->addFlashToastr('success', $createds.' Cruds créés!');
		} else {
			$this->container->get(serviceFlashbag::class)->addFlashToastr('warning', 'Aucun Crud n\'a été créé!');
		}
		return $this;
	}

}


