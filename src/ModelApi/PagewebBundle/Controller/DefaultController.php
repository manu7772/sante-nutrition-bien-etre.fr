<?php

namespace ModelApi\PagewebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
// use FOS\RestBundle\Controller\Annotations\QueryParam;

use ModelApi\PagewebBundle\Entity\Pageweb;


class DefaultController extends Controller {

	/**
	 * @ApiDoc(
	 *    resource=true,
	 *    section="Pageweb",
	 *    description="Get Pagewebs",
	 *    output = { "class" = Pageweb::class, "collection" = true, "groups" = {"Pageweb"} },
	 *    views = { "default", "Pageweb" }
	 * )
	 * @Rest\View(serializerGroups={"base","Pageweb"})
	 * @Rest\Get("/pagewebs", name="modelapifile-getpagewebs")
	 */
	public function getItemsAction(Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$pagewebs = $em->getRepository(Pageweb::class)->findAll();
		return $pagewebs;
	}

}