<?php
namespace ModelApi\PagewebBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceBaseMain;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Service\serviceRoles;
// FileBundle
// use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceTwig;

// use \DateTime;
use \ReflectionClass;
use \NotFoundHttpException;

class servicePageweb extends serviceTwig {

	const ENTITY_CLASS = Pageweb::class;

	protected $basepagewebs;

	public function __construct(ContainerInterface $container) {
		parent::__construct($container);
		$this->basepagewebs = new ArrayCollection();
		return $this;
	}


	// public function getCopy($entity, $options = [], $closure = null) {
	// 	$new_entity = clone $entity;
	// 	if(is_executable($entity)) $closure($new_entity);
	// 	return $new_entity;
	// }

	// /**
	//  * Get new entity
	//  * @param array $options = []
	//  * @param callable $closure = null
	//  * @return entity | null
	//  */
	// public function createNew($options = [], $closure = null) {
	// 	return $this->serviceEntities->createNew(static::ENTITY_CLASS, $options, $closure);
	// }

	public function getTemplateStructures(User $user = null) {
		return $this->container->get(serviceKernel::class)->getTemplateStructures($user);
	}

	public function getTemplateNames(User $user = null) {
		$templates = $this->getTemplateStructures($user);
		return array_keys($templates);
	}

	// public function createNew($options) {
	// 	$Pageweb = $this->serviceEntities->createNew(static::ENTITY_CLASS, $options);
	// 	$this->checkPagewebs($Pageweb);
	// 	return $Pageweb;
	// }

	public function findPagewebByTypepage($typepage) {
		$tier = $this->container->get(serviceUser::class)->getCurrentUserOrNull();
		if(empty($tier)) return [];
		return $this->getRepository()->findPagewebByTypepage($typepage, $tier);
	}

	public function getPreferedPageweb() {
		return $this->getRepository()->findOneBy(['prefered' => true]);
	}



	// REPORTER TOUT CE MERDIER DANS LE REPOSITORY !!!!!!!!!!!!!!

	// public function getIfAllreadyLoaded($values) {
	// 	return $this->basepagewebs->filter(function($pageweb) use ($values) {
	// 		$result = false;
	// 		foreach ($values as $field => $value) {
	// 			foreach (['get','is','has'] as $base) {
	// 				$method = $base.ucfirst($field);
	// 				if(method_exists($pageweb, $method)) {
	// 					$result = $result && $pageweb->$method() === $value;
	// 					break;
	// 				}
	// 			}
	// 			$call = 'get'.ucfirst($field);
	// 			if(!method_exists($pageweb, $call)) $call = 'is'.ucfirst($field);
	// 			if(!method_exists($pageweb, $call)) $call = 'has'.ucfirst($field);
	// 			if(method_exists($pageweb, $call)) $result = $result && $pageweb->$call() === $value;
	// 				// else $result = false;
	// 		}
	// 		return $result;
	// 	})->first();
	// }

	public function findOneByFilename($name) {
		return $this->getRepository()->findOneByFilename($name);
	}

	public function findOneBySlug($slug) {
		return $this->getRepository()->findOneBySlug($slug);
	}

	public function findPagewebAnyway(Item $entity, $flush = true) {
		if($entity instanceOf Pageweb) return $entity;
		$pageweb = $entity->getPageweb();
		if(!($pageweb instanceOf Pageweb) || !$this->container->get('templating')->exists($pageweb->getBundlepathname())) {
			$pageweb = $this->getRepository()->findOnePageForItem($entity);
			if(!($pageweb instanceOf Pageweb) || !$this->container->get('templating')->exists($pageweb->getBundlepathname())) throw new NotFoundHttpException("Page web for ".$entity->getShortname()." ".$entity->__toString()." not found");
			$entity->setPageweb($pageweb);
			if($flush) $this->getEntityManager()->flush();
			$pageweb->setTempitem($entity);
		}
		return $pageweb;
	}



	// public function getIfAlreadyLoaded($values) {
	// 	return $this->basepagewebs->filter(function($pageweb) use ($values) {
	// 		$result = false;
	// 		foreach ($values as $field => $value) {
	// 			$call = 'get'.ucfirst($field);
	// 			if(!method_exists($pageweb, $call)) $call = 'is'.ucfirst($field);
	// 			if(!method_exists($pageweb, $call)) $call = 'has'.ucfirst($field);
	// 			if(method_exists($pageweb, $call)) $result = $result && $pageweb->$call() === $value;
	// 				// else $result = false;
	// 		}
	// 		return $result;
	// 	});
	// }

	// public function findOneByFilename($name) {
	// 	if(!is_string($name)) return null;
	// 	$results = $this->getIfAlreadyLoaded(['filename' => $name]);
	// 	if($results->count() > 0) return $results->first();
	// 	$pageweb = $this->getRepository()->findOneByFilename($name);
	// 	if($pageweb instanceOf Pageweb && !$this->basepagewebs->contains($pageweb)) $this->basepagewebs->add($pageweb);
	// 	return $pageweb;
	// }

	// public function findOneBySlug($slug) {
	// 	if(!is_string($slug)) return null;
	// 	$results = $this->getIfAlreadyLoaded(['slug' => $slug]);
	// 	if($results->count() > 0) return $results->first();
	// 	$pageweb = $this->getRepository()->findOneBySlug($slug);
	// 	if($pageweb instanceOf Pageweb && !$this->basepagewebs->contains($pageweb)) $this->basepagewebs->add($pageweb);
	// 	return $pageweb;
	// }

}