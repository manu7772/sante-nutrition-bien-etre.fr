<?php
namespace ModelApi\PagewebBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;
// use Doctrine\ORM\EntityManagerInterface;

// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
use ModelApi\PagewebBundle\Service\servicePageweb;
// FileBundle
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Service\serviceTwig;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\servicesBaseInterface;

use Twig_Error_Loader;
use Twig_LoaderInterface;
use Twig_Source;
use \DateTime;
use \Symfony\Component\Finder\Finder;

class TwigDatabaseLoader implements Twig_LoaderInterface, servicesBaseInterface {
	// https://stackoverflow.com/questions/8188674/how-to-render-twig-template-from-database-in-symfony2

	use \ModelApi\BaseBundle\Service\baseService;

	protected $container;
	protected $servicePageweb;
	protected $serviceTwig;
	protected $templates;

	// public function __construct(EntityManagerInterface $em) {
	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->servicePageweb = $this->container->get(servicePageweb::class);
		$this->serviceTwig = $this->container->get(serviceTwig::class);
		$this->templates = array();
	}

	public function getSourceContext($name) {
		if(null === $template = $this->getTemplate($name)) throw new Twig_Error_Loader(sprintf('Template "%s" does not exist.', $name));
		// echo('<pre>');var_dump($template);die('</pre>');
		return new Twig_Source($template['binaryFile'], $name);
	}

	public function exists($name) {
		return null !== $this->getTemplate($name);
	}

	public function getCacheKey($name) {
		return $name;
	}

	public function isFresh($name, $time) {
		return true;
		if(null === $template = $this->getTemplate($name)) return false;
		// var_dump($template['fresh_timestamp'] <= $time);
		return $template['fresh_timestamp'] <= $time;
		// return $template->getLastUpdated()->getTimestamp() <= $time;
	}

	public function getPreferedTemplate($bundlename = null) {
		if(!is_string($bundlename)) $bundlename = $this->container->get(serviceKernel::class)->getCurrentBundleName();
		$template = $this->servicePageweb->getRepository()->findForScreen(['prefered' => 1, 'bundle' => $bundlename]);
		return $template instanceOf Pageweb && $template->isActive() ? $template : null;
	}

	/**
	 * @param $name
	 * @return Twig|null
	 */
	protected function getTemplate($name) {
		if(!is_string($name)) return null;
		if(isset($this->templates[$name])) return $this->templates[$name];
		$template = $this->serviceTwig->getRepository()->getTwigCodeWithDates($name);
		if(!is_array($template)) return null;
		$this->templates[$name] = $template;
		return $this->templates[$name];
	}

	public function clearTwigCache($environments = null, $message = null) {
		if(!is_array($environments)) $environments = ['dev','prod','test'];
		// $cacheDir = __DIR__."/../../../../var/cache";
		$cacheDir = $this->container->get(serviceKernel::class)->getBaseDir('var/cache/');
		$finder = new Finder();
		$dirs = array();
		foreach ($environments as $env) {
			$dir = $cacheDir.strtolower($env)."/twig";
			if(file_exists($dir) && is_dir($dir)) $dirs[] = $dir;
		}
		//TODO quick hack...
		if(count($dirs) > 0) { 
			$finder->in($dirs)->files();
			foreach($finder as $file) unlink($file->getRealpath());
			// if($this->serviceKernel->isDev()) 
				$this->container->get(serviceFlashbag::class)->addFlashToastr('warning', 'Cleared templates cache. '.$message, 'ROLE_SUPER_ADMIN');
		}
		return $this;
	}


}
