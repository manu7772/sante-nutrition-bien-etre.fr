<?php
namespace ModelApi\PagewebBundle\Form\Type;

// use Symfony\Component\Form\AbstractType;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
use Symfony\Component\Form\FormBuilderInterface;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;

// use \ReflectionClass;

class PagewebAnnotateType extends BaseAnnotateType {

	const ENTITY_CLASSNAME = Pageweb::class;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder,$options);
	}


}