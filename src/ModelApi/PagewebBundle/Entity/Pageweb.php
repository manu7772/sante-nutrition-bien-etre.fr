<?php
namespace ModelApi\PagewebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModelApi\AnnotBundle\Annotation as Annot;
use ModelApi\CrudsBundle\Annotation as CRUDS;
use Doctrine\Common\Collections\ArrayCollection;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Inflector\Inflector;
// Gedmo
// use Gedmo\Mapping\Annotation as Gedmo;
// use Gedmo\Translatable\Translatable;
use Website\SiteBundle\Classes\PagewebsData;

// AnnotBundle
use ModelApi\AnnotBundle\Annotation\HasTranslatable;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// use ModelApi\BaseBundle\Traits\ClassDescriptor;
// PagewebBundle
use ModelApi\PagewebBundle\Service\servicePageweb;
use ModelApi\PagewebBundle\Repository\PagewebRepository;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Basedirectory;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Photo;
// UserBundle
// use ModelApi\UserBundle\Entity\Group;
// use ModelApi\UserBundle\Entity\GroupableInterface;
// CrudsBundle
// use ModelApi\CrudsBundle\Entity\CrudInterface;
// DevprintBundle
// use ModelApi\DevprintsBundle\Service\DevTerminal;
use ModelApi\DevprintsBundle\Service\YamlLog;

use \Exception;
use \ReflectionClass;

/**
 * Pageweb
 * 
 * @ORM\Entity(repositoryClass=PagewebRepository::class)
 * @ORM\Table(name="`pageweb`", options={"comment":"Pageweb element"})
 * @see https://www.doctrine-project.org/projects/doctrine-orm/en/2.8/reference/annotations-reference.html#namednativequery
 * !!!@ORM\NamedNativeQueries({
 * 		!!!@ORM\NamedNativeQuery(
 * 			name             = "fetchJoinedTwigsections",
 * 			resultSetMapping = "mappingJoinedTwigsections",
 * 			query            = "SELECT p, t FROM ModelApi\PagewebBundle\Entity\Pageweb p INNER JOIN p.twigsections t where p.id = :id"
 * 		)
 * })
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_pageweb",
 *          parameters = { "id" = "expr(object.getId())" }
 *      )
 * )
 * @ORM\HasLifecycleCallbacks
 * @CRUDS\Actions(show=true, copy=true, create="ROLE_EDITOR", update="ROLE_TRANSLATOR", delete="ROLE_EDITOR")
 * @Annot\HasTranslatable()
 */
class Pageweb extends Twig {

	const DEFAULT_ICON = 'fa-globe';
	const DEFAULT_OWNER_DIRECTORY = '/Pagewebs';
	const PROCESS_CONTROL = false; // true if DEV
	const ENTITY_SERVICE = servicePageweb::class;

	// /**
	//  * @var string
	//  * @ORM\Column(name="singlefilename", type="string", nullable=false, unique=false)
	//  */
	// protected $singlefilename;

	/**
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=200)
	 */
	protected $parent;

	/**
	 * Name
	 * @var string
	 * @CRUDS\Create(
	 * 		show=false,
	 * 		update=false,
	 * 		order=1,
	 * 		options={"required"=true, "attr"={"placeholder"="field.name_placeholder"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		show=false,
	 * 		update=false,
	 * 		order=1,
	 * 		options={"required"=true, "attr"={"placeholder"="field.name_placeholder"}}
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=1)
	 */
	protected $name;

	/**
	 * @var string
	 * H1 title - balise H1
	 * @ORM\Column(name="titleh1", type="text", nullable=true, unique=false)
	 * @Annot\Translatable
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		type="SmalltextareaType",
	 * 		order=4,
	 * 		options={"required"=false},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * )
	 * @CRUDS\Update(
	 * 		show="ROLE_TRANSLATOR",
	 * 		update="ROLE_TRANSLATOR",
	 * 		type="SmalltextareaType",
	 * 		order=4,
	 * 		options={"required"=false},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * )
	 * @CRUDS\Show(role=true)
	 */
	protected $titleh1;

	/**
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto", "label"="Photo principale de la page web."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "expert"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPhoto", "label"="Photo principale de la page web."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "expert"},
	 * 	)
	 * @CRUDS\Show(role=true, order=600)
	 */
	protected $photo;

	/**
	 * @ORM\OneToOne(targetEntity="ModelApi\BinaryBundle\Entity\Photo", cascade={"persist"}, orphanRemoval=true)
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsEntete", "label"="Image entête", "description"="Image utilisée pour l'entête de la page web.<br>Cette image est une image par défaut : si cete page web contient un élément (article, blog, etc.), alors c'est l'image d'entête de cet élément qui sera utilisé en priorité."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsEntete", "label"="Image entête", "description"="Image utilisée pour l'entête de la page web.<br>Cette image est une image par défaut : si cete page web contient un élément (article, blog, etc.), alors c'est l'image d'entête de cet élément qui sera utilisé en priorité."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * 	)
	 * @CRUDS\Show(role=true, order=600)
	 */
	protected $entete;

	/**
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPicone", "label"="Image carrée", "description"="Image carrée utilisée pour illustrer un lien vers cette page. Cette image n'est pas utilisée dans la page, mais uniquement dans les liens externes."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=600,
	 * 		options={"required"=false, "by_reference"=false, "attr"={"accept" = "auto"}, "property_path"="resourceAsPicone", "label"="Image carrée", "description"="Image carrée utilisée pour illustrer un lien vers cette page. Cette image n'est pas utilisée dans la page, mais uniquement dans les liens externes."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * 	)
	 * @CRUDS\Show(role=true, order=600)
	 */
	protected $picone;

	/**
	 * @var string
	 * Html text - contenu de la page
	 * @CRUDS\Create(
	 * 		type="CodemirrorType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1000,
	 * 		options={"by_reference"=false, "required"=true},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * )
	 * @CRUDS\Update(
	 * 		type="CodemirrorType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=1000,
	 * 		options={"by_reference"=false, "required"=true},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "simple"},
	 * )
	 * @CRUDS\Show(role=true, order=1000)
	 */
	protected $stringAsVersion;

	/**
	 * @ORM\Column(name="prefered", type="boolean", nullable=true, unique=false)
	 * @Annot\UniqueField(value=true, onError="force", min=1, max=1, altValue=false, groupedByFields={"bundle"})
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role="ROLE_EDITOR")
	 */
	protected $prefered;

	/**
	 * @ORM\Column(name="preloader", type="boolean", nullable=true, unique=false)
	 * @CRUDS\Create(show=false, update=false, type="switcheryType", options={"required"=false, "by_reference"=false})
	 * @CRUDS\Update(show=false, update=false, type="switcheryType", options={"required"=false, "by_reference"=false})
	 * @CRUDS\Show(role="ROLE_EDITOR")
	 */
	protected $preloader;


	/**
	 * @ORM\Column(name="seo", type="boolean", nullable=true)
	 * @CRUDS\Create(
	 * 		type="switcheryType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=150,
	 * 		options={"required"=false, "by_reference"=false},
	* 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "medium"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="switcheryType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=150,
	 * 		options={"required"=false, "by_reference"=false},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "medium"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_EDITOR", order=150)
	 */
	protected $seo;

	/**
	 * Upload file
	 * @CRUDS\Create(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		options={"required"=false, "attr"={"accept" = "auto"}}
	 * 	)
	 * @CRUDS\Update(
	 * 		type="FileType",
	 * 		show=false,
	 * 		update=false,
	 * 		options={"required"=false, "attr"={"accept" = "auto"}}
	 * 	)
	 * @CRUDS\Show(role=false)
	 */
	protected $uploadFile;

	/**
	 * @var string
	 * @Annot\Translatable
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=2, options={"by_reference"=false, "required"=false, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=2, options={"by_reference"=false, "required"=false, "description"="<strong>Titre pour menus</strong> : version TRÈS COURTE du nom (ou titre) de l'élément, pour l'affichage dans les menus."}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Show(role=true)
	 */
	protected $menutitle;

	/**
	 * @var string
	 * @Annot\Translatable(type="text")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=3, options={"by_reference"=false, "required"=false}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=3, options={"by_reference"=false, "required"=false}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Show(role=true)
	 */
	protected $subtitle;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Show(role=true)
	 */
	protected $accroche;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Show(role=true)
	 */
	protected $resume;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(show="ROLE_EDITOR", update="ROLE_EDITOR", order=4, options={"by_reference"=false, "required"=false}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Update(show="ROLE_TRANSLATOR", update="ROLE_TRANSLATOR", order=4, options={"by_reference"=false, "required"=false}, contexts={BaseAnnotateType::FORM_FORALL_CONTEXT})
	 * @CRUDS\Show(role=true)
	 */
	protected $fulltext;

	/**
	 * @var string
	 * @Annot\Translatable(type="html")
	 * @CRUDS\Create(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=4,
	 * 		options={"by_reference"=false, "required"=false},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "medium"},
	 * 	)
	 * @CRUDS\Update(
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=4,
	 * 		options={"by_reference"=false, "required"=false},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "medium"},
	 * 	)
	 * @CRUDS\Show(role=true)
	 */
	protected $description;

	/**
	 * @var array
	 * @CRUDS\Create(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=2000,
	 * 		options={"required"=false, "multiple"=true, "label"="Pour items", "choices"="auto", "description"="Choisir quel(s) élément(s) cette page web est destinée à afficher. Ceci permet d'ajouter cette page web à la liste des mises en pages disponibles pour un élément crée. Distinguez bien les pages pour les LISTES d'éléments (aau pluriel) de celles pour un seul élément (au singulier)."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "medium"},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="ChoiceType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=2000,
	 * 		options={"required"=false, "multiple"=true, "label"="Pour items", "choices"="auto", "description"="Choisir quel(s) élément(s) cette page web est destinée à afficher. Ceci permet d'ajouter cette page web à la liste des mises en pages disponibles pour un élément crée. Distinguez bien les pages pour les LISTES d'éléments (aau pluriel) de celles pour un seul élément (au singulier)."},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT, "medium"},
	 * 	)
	 * @CRUDS\Show(role="ROLE_SUPER_ADMIN", order=2000, label="Pour Items")
	 */
	protected $itemtypes;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role="ROLE_EDITOR")
	 */
	protected $typepage;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role="ROLE_EDITOR")
	 */
	protected $color;

	/**
	 * @var string
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role="ROLE_EDITOR")
	 */
	protected $dirmenus;


	/**
	 * @ORM\ManyToOne(targetEntity="ModelApi\FileBundle\Entity\Basedirectory", cascade={"persist"})
	 * @ORM\JoinColumn(name="sections_directory", nullable=true, unique=false)
	 * @Annot\JoinedOwnDirectory(getter="getSection", setter="setSection", altgetter="getTwigsections", altsetter="setTwigsections", path="system/Twigs")
	 */
	protected $section;

	/**
	 * @CRUDS\Create(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=125,
	 * 		options={"multiple"=true, "required"=false, "group_by"="typepage", "by_reference"="false", "class"="ModelApi\FileBundle\Entity\Twig", "query_builder"="qb_pagewebSections"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @CRUDS\Update(
	 * 		type="EntityType",
	 * 		show="ROLE_EDITOR",
	 * 		update="ROLE_EDITOR",
	 * 		order=125,
	 * 		options={"multiple"=true, "required"=false, "group_by"="typepage", "by_reference"="false", "class"="ModelApi\FileBundle\Entity\Twig", "query_builder"="qb_pagewebSections"},
	 * 		contexts={BaseAnnotateType::FORM_FORALL_CONTEXT},
	 * 	)
	 * @Annot\JoinSections(setter="setTwigsections", getter="getTwigsections")
	 * @CRUDS\Show(role="ROLE_EDITOR", order=125)
	 */
	protected $twigsections;

	/**
	 * @var integer
	 * Pageweb
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role=false, order=500)
	 */
	protected $pageweb;

	/**
	 * @ORM\Column(name="defaultjoin", type="boolean", nullable=true, unique=false)
	 * @CRUDS\Create(show=false, update=false)
	 * @CRUDS\Update(show=false, update=false)
	 * @CRUDS\Show(role="ROLE_EDITOR")
	 */
	protected $defaultjoin;

	protected $tempitem;
	protected $tempitems;

	public function __construct($predefinedTemplatename = null, $predefinedTypepage = null) {
		parent::__construct($predefinedTemplatename, $predefinedTypepage);
		$this->titleh1 = null;
		$this->subtitle = null;
		$this->entete = null;
		$this->accroche = null;
		$this->prefered = false;
		// $this->typepage = 'root';
		$this->preloader = false;
		$this->seo = true;
		$this->section = null;
		$this->twigsections = new ArrayCollection();
		$this->pageweb = null;
		$this->tempitem = null;
		$this->tempitems = new ArrayCollection();
		return $this;
	}

	public static function getDefaultConstantForStringAsVersion() {
		return PagewebsData::MODEL_CODE_PAGEWEB;
	}

	/**
	 * Get default declinations names
	 * @return array
	 */
	public function getDefaultDeclinationsNames() {
		return ['original'];
	}

	// /**
	//  * Set singlefilename
	//  * @param string $singlefilename
	//  * @return Pageweb
	//  */
	// public function setSinglefilename($singlefilename) {
	// 	$this->singlefilename = preg_replace('/(\\.html)(\\.twig)?$/', '', $singlefilename);
	// 	return $this;
	// }

	// /**
	//  * Get singlefilename
	//  * @return string
	//  */
	// public function getSinglefilename() {
	// 	return $this->singlefilename;
	// }


	/**
	 * Get Basedirectory of Sections
	 * @return Basedirectory
	 */
	public function getSection() {
		if(empty($this->section)) $this->section = $this->getOwnerDirectory('system/Twigs');
		return $this->section;
	}

	public function setSection() {
		$this->section = null;
		$this->getSection();
		return $this;
	}

	public function getTwigsections() {
		if($this->getSection() instanceOf Basedirectory) {
			$this->twigsections = $this->section->getChilds()->filter(function($item) { return $item instanceOf Twig; });
			return $this->twigsections;
		}
		if(!($this->twigsections instanceOf ArrayCollection)) $this->twigsections = new ArrayCollection();
		return $this->twigsections;
	}

	public function setTwigsections($twigsections) {
		if(is_array($twigsections)) $twigsections = new ArrayCollection($twigsections);
		foreach ($this->getTwigsections() as $twigsection) {
			if(!$twigsections->contains($twigsection)) $this->removeTwigsection($twigsection);
		}
		foreach ($twigsections as $twigsection) $this->addTwigsection($twigsection);
		return $this;
	}

	public function addTwigsection(Twig $twig) {
		if($this->getSection() instanceOf Basedirectory) {
			$this->section->addChild($twig);
			return $this->getTwigsections()->contains($twig);
		}
		if(!$this->getTwigsections()->contains($twig)) $this->twigsections->add($twig);
		return true;
	}

	public function removeTwigsection(Twig $twig) {
		if($this->getSection() instanceOf Basedirectory) {
			$this->section->removeChild($twig);
			return !$this->getTwigsections()->contains($twig); 
		}
		$this->getTwigsections()->removeElement($twig);
		return true;
	}

	public function hasSection($slug) {
		if(preg_match('/^[\\w-]+$/', $slug)) {
			// string test
			foreach ($this->getTwigsections() as $section) if($section->getSlug() === $slug) return true;
		} else {
			// preg test
			foreach ($this->getTwigsections() as $section) if(preg_match($slug, $section->getSlug())) return true;
		}
		return false;
	}

	public function hasTypeSection($type, $auMoins = 1) {
		return $this->getTwigsections()->filter(function($item) use ($type) { return strtolower($item->getTypepage()) === $type; })->count() >= $auMoins;
	}

	public function hasNoTypeSection($type) {
		return $this->getTwigsections()->filter(function($item) use ($type) { return strtolower($item->getTypepage()) === $type; })->count() < 1;
	}

	public function __call($method, $parameters) {
		switch ($method) {
			case 'getTpls':
				$attribute = reset($parameters);
				$data = new ArrayCollection();
				$attribute = strtolower($attribute);
				$typepages = $this->getTypesections();
				$single = Inflector::singularize($attribute);
				// if(!in_array($single, $typepages)) return new ArrayCollection(); // Not found!
				return $this->getTwigsections()->filter(function($item) use ($single) { return strtolower($item->getTypepage()) === $single; });
				break;
			case 'getTpl':
				$attribute = reset($parameters);
				$data = null;
				$attribute = strtolower($attribute);
				$typepages = $this->getTypesections();
				$single = Inflector::singularize($attribute);
				// if(!in_array($single, $typepages)) return null; // Not found!
				$tpl = $this->getTwigsections()->filter(function($item) use ($single) { return strtolower($item->getTypepage()) === $single; })->first();
				return $tpl instanceOf Twig ? $tpl : null;
				break;
		}
		return null;
	}



	/*************************************************************************************/
	/*** TYPEPAGE
	/*************************************************************************************/

	public function getTypepages() {
		if(!$this->hasTemplatestructure()) return [];
		$names = [];
		$templatestructure = $this->getTemplatestructure();
		$types = array_filter($templatestructure, function ($type) { return preg_match('/^(?!_)/', $type); }, ARRAY_FILTER_USE_KEY);
		$typepages = array_keys($types);
		$names = [];
		foreach ($typepages as $typepage) $names[$typepage] = $typepage;
		return $names;
	}

	public function getTypesections() {
		return parent::getTypepages();
	}

	/*************************************************************************************/
	/*** FOLDER FOR TEMPLATE
	/*************************************************************************************/

	/**
	 * Get Folder
	 * @return string | null
	 */
	public function getFolder() {
		if(!$this->hasTemplatestructure()) return null;
		$templatestructure = $this->getTemplatestructure();
		return $templatestructure[$this->getTypepage()]['folder'];
	}






	/**
	 * Set titleh1
	 * @param string $titleh1 = null
	 * @return Pageweb
	 */
	public function setTitleh1($titleh1 = null) {
		$this->titleh1 = trim($titleh1);
		return $this;
	}

	/**
	 * Get titleh1
	 * @return string
	 */
	public function getTitleh1() {
		return $this->titleh1;
	}

	/**
	 * @ORM\PrePersist()
	 */
	public function PrePersistDefineTitleh1() {
		if(null === $this->titleh1) $this->titleh1 = $this->getTitle();
		return $this;
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function PreUpdateDefineTitleh1() {
		if(null === $this->titleh1) $this->titleh1 = $this->getTitle();
		return $this;
	}

	// /**
	//  * Set subtitle
	//  * @param string $subtitle = null
	//  * @return Pageweb
	//  */
	// public function setSubtitle($subtitle = null) {
	// 	$this->subtitle = trim($subtitle);
	// 	return $this;
	// }

	// /**
	//  * Get subtitle
	//  * @return string
	//  */
	// public function getSubtitle() {
	// 	return $this->subtitle;
	// }

	// /**
	//  * Set accroche
	//  * @param string $accroche = null
	//  * @return Pageweb
	//  */
	// public function setAccroche($accroche = null) {
	// 	$this->accroche = trim($accroche);
	// 	return $this;
	// }

	// /**
	//  * Get accroche
	//  * @return string
	//  */
	// public function getAccroche() {
	// 	return $this->accroche;
	// }







	// public function initTemplatename($templatename) {
	// 	$this->setTemplatename($templatename);
	// 	$templatestructure = $this->getTemplatestructure();
	// 	$this->setSeo($templatestructure['root']['seo']);
	// 	$this->setTypepage();
	// 	return $this;
	// }




	/**
	 * Get prefered
	 * @return boolean
	 */
	public function getPrefered() {
		return $this->prefered;
	}

	/**
	 * Is prefered
	 * @return boolean
	 */
	public function isPrefered() {
		return $this->getPrefered();
	}

	/**
	 * Set prefered
	 * @param boolean $prefered
	 * @return Pageweb
	 */
	public function setPrefered($prefered) {
		// if(!is_bool($prefered)) throw new Exception("Error ".__METHOD__."(): argument has to be a boolean. ".gettype($prefered)." given!", 1);
		$this->prefered = $prefered;
		return $this;
	}

	/**
	 * Get seo
	 * @return boolean
	 */
	public function getSeo() {
		return $this->seo;
	}

	/**
	 * Is seo
	 * @return boolean
	 */
	public function isSeo() {
		return $this->seo;
	}

	/**
	 * Set seo
	 * @param boolean $seo
	 * @return Pageweb
	 */
	public function setSeo($seo) {
		$this->seo = $seo;
		return $this;
	}

	/**
	 * Get preloader
	 * @return boolean
	 */
	public function getPreloader() {
		return $this->preloader;
	}

	/**
	 * Set preloader
	 * @param boolean $preloader
	 * @return Pageweb
	 */
	public function setPreloader($preloader) {
		$this->preloader = $preloader;
		return $this;
	}



	/*************************************************************************************/
	/*** ENTETE
	/*************************************************************************************/

	/**
	 * Set URL as entete
	 * @param string $url
	 * @return Pageweb
	 */
	public function setUrlAsEntete($url) {
		$entete = new Photo();
		$entete->setUploadFile($url);
		$this->setEntete($entete);
	}

	/**
	 * Set resource as entete
	 * @param string $resource
	 * @return Pageweb
	 */
	public function setResourceAsEntete(UploadedFile $resource = null) {
		if($resource instanceOf UploadedFile) {
			$entete = new Photo();
			$entete->setUploadFile($resource);
			$this->setEntete($entete);
		}
		return $this;
	}

	public function getResourceAsEntete() {
		return null;
	}

	/**
	 * Set entete
	 * @param Entete $entete = null
	 * @return Pageweb
	 */
	public function setEntete(Photo $entete = null) {
		$this->entete = $entete;
		return $this;
	}

	/**
	 * Get entete
	 * @return Entete | null
	 */
	public function getEntete() {
		return $this->entete;
	}

	/**
	 * Has entete
	 * @return boolean
	 */
	public function hasEntete() {
		return $this->getEntete() instanceOf Photo;
	}





	/**
	 * @ORM\PostLoad()
	 */
	public function defineTempitems() {
		$this->tempitem = null;
		$this->tempitems = new ArrayCollection();
	}

	/**
	 * Set tempitem
	 * @param mixed $tempitem = null 
	 * @return Pageweb
	 */
	public function setTempitem($tempitem = null) {
		$this->tempitem = $tempitem;
		return $this;
	}

	/**
	 * Get tempitem
	 * @return mixed
	 */
	public function getTempitem($findInCollectionIfNull = true) {
		if(!$findInCollectionIfNull || null !== $this->tempitem) return $this->tempitem;
		$tempitem = $this->tempitems->first();
		return is_object($tempitem) ? $tempitem : null;
	}

	/**
	 * Set tempitems
	 * @param $tempitems = null 
	 * @return Pageweb
	 */
	public function setTempitems($tempitems = null) {
		if(null === $tempitems) $tempitems = new ArrayCollection();
		if(!is_array($tempitems) && !($tempitems instanceOf ArrayCollection)) $tempitems = new ArrayCollection([$tempitems]);
		if(is_array($tempitems)) $tempitems = new ArrayCollection($tempitems);
		$this->tempitems = $tempitems;
		return $this;
	}

	/**
	 * Get tempitems
	 * @return ArrayCollection
	 */
	public function getTempitems() {
		return $this->tempitems;
	}

	/*************************/
	/*** MENU              ***/
	/*************************/

	public function isMenuable() {
		return true;
	}



}