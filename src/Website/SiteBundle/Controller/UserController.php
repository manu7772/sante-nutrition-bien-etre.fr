<?php

namespace Website\SiteBundle\Controller;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security as CoreSecurity;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
// BaseBundle
// use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// UserBundle
use ModelApi\UserBundle\Service\serviceUser;
// SiteBundle
use Website\SiteBundle\Controller\DefaultController;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
use ModelApi\PagewebBundle\Service\servicePageweb;

use \Exception;

class UserController extends Controller {

	const DEFAULT_ROUTE = 'website_site_homepage';

	private $eventDispatcher;
	private $tokenManager;

	public function __construct(EventDispatcherInterface $eventDispatcher = null, CsrfTokenManagerInterface $tokenManager = null) {
		$this->eventDispatcher = $eventDispatcher;
		$this->tokenManager = $tokenManager;
		return $this;
	}

	public function redirectLastOrDefaultUrl($request, $defaultRoute = null, $params = []) {
		if(!is_string($defaultRoute)) $defaultRoute = DefaultController::DEFAULT_ROUTE;
		$url = $this->get(serviceRoute::class)->getLastOrDefaultUrl($request, $defaultRoute, $params);
		return $this->redirect($url);
	}

	/**
	 * @Route("/login", name="website_site_user_login", options={ "method_prefix" = false }, methods={"GET"})
	 */
	public function loginAction(Request $request) {
		// $globals['login_data'] = $this->get(serviceUser::class)->getLoginDataForForm();

		/** @var $session Session */
		$session = $request->getSession();

		$authErrorKey = CoreSecurity::AUTHENTICATION_ERROR;
		$lastUsernameKey = CoreSecurity::LAST_USERNAME;

		// get the error if any (works with forward and redirect -- see below)
		if ($request->attributes->has($authErrorKey)) {
			$error = $request->attributes->get($authErrorKey);
		} elseif (null !== $session && $session->has($authErrorKey)) {
			$error = $session->get($authErrorKey);
			$session->remove($authErrorKey);
		} else {
			$error = null;
		}

		if (!$error instanceof AuthenticationException) {
			$error = null; // The value does not come from the security component.
		}

		// last username entered by the user
		$lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

		$csrfToken = $this->tokenManager
			? $this->tokenManager->getToken('authenticate')->getValue()
			: null;


		$globals['login_data'] = [
			'last_username' => $lastUsername,
			'error' => $error,
			'csrf_token' => $csrfToken,
		];
		//return $this->render('WebsiteSiteBundle:Special:login.html.twig', $globals);
		$globals['Pageweb'] = $this->get(servicePageweb::class)->getRepository()->findOneBy(['bundlepathname' => 'WebsiteSiteBundle:Pageweb:login']);
		$templatename = empty($globals['Pageweb']) ? 'WebsiteSiteBundle:Special:login.html.twig' : $globals['Pageweb']->getBundlepathname();
		return $this->render($templatename, $globals);
	}

	// /**
	//  * @Route("/user/register", name="website_site_user_register", options={ "method_prefix" = false }, methods={"GET"})
	//  */
	// public function registerAction(Request $request) {
	// 	throw new Exception("Vous ne pouvez pas vous inscrire pour le moment.", 1);
		
	// }

	/**
	 * @Route("/profile", name="website_site_profile", options={ "method_prefix" = false }, methods={"GET"})
	 * @Security("has_role('ROLE_USER')")
	 */
	public function profileAction(Request $request) {
		$globals['Pageweb'] = $this->get(servicePageweb::class)->getRepository()->findOneBy(['bundlepathname' => 'WebsiteSiteBundle:Pageweb:profile']);
		$templatename = empty($globals['Pageweb']) ? 'WebsiteSiteBundle:Special:profile.html.twig' : $globals['Pageweb']->getBundlepathname();
		return $this->render($templatename, $globals);
		//return $this->render('WebsiteSiteBundle:Pageweb:profile.html.twig');
	}

	/**
	 * @Route("/user/setting", name="website_site_setting", options={ "method_prefix" = false }, methods={"GET"})
	 * @Security("has_role('ROLE_USER')")
	 */
	public function settingAction(Request $request) {
		$globals['Pageweb'] = $this->get(servicePageweb::class)->getRepository()->findOneBy(['bundlepathname' => 'WebsiteSiteBundle:Pageweb:setting']);
		$templatename = empty($globals['Pageweb']) ? 'WebsiteSiteBundle:Special:setting.html.twig' : $globals['Pageweb']->getBundlepathname();
		return $this->render($templatename, $globals);
		//return $this->render('WebsiteSiteBundle:Pageweb:setting.html.twig');
	}

	/**
	 * @Route("/user/subscribe_newsletter", name="website_site_subscribe_newsletter", options={ "method_prefix" = false }, methods={"POST"})
	 */
	public function subscribeNewsletterAction(Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$serviceUser = $this->get(serviceUser::class);
		$email = $request->request->get('email');
		$user = $serviceUser->getRepository()->findOneByEmail($email);
		if(empty($user)) {
			$user = $serviceUser->createTempUser(['email' => $email]);
			if(empty($user)) {
				// Error on creation
				$this->get(serviceFlashbag::class)->addFlashSweet('error', 'error_create_user');
			} else {
				$user->setEnabled()->setNewsletter(true);
				$em->persist($user);
				$em->flush();
				$this->get(serviceFlashbag::class)->addFlashSweet('success', 'newsletter.success');
			}
		} else {
			// User exists
			if($user->isNewsletter()) {
				$this->get(serviceFlashbag::class)->addFlashSweet('warning', 'newsletter.allready_subscribed');
			} else {
				// $user->setNewsletter(true);
				// $em->flush();
				$this->get(serviceFlashbag::class)->addFlashSweet('warning', 'newsletter.need_connect_to_subscribe');
			}
		}
		// $this->get(serviceFlashbag::class)->addFlashSweet('warning', 'WARNING : Could not set '.$item->getName().' in '.$directory->getName().'!');
		return $this->redirectLastOrDefaultUrl($request);
	}
	// manu7772@gmail.com




	/**
	 * @param Request $request
	 * @Route(
	 *		"/user/register",
	 *		name="user_register",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 */
	public function registerAction(Request $request) {
		$serviceUser = $this->get(serviceUser::class);
		if($serviceUser->isAuthorizedToRegister()) {
			$options = [
				'LARGE_FOOTER' => true
			];
			$globals['form'] = $serviceUser->getRegistrationDataForForm(true, $options, ['register','register_extended']);
		} else {
			$globals['form'] = null;
			$globals['message'] = "Les enregistrements utilisateurs ne sont pas autorisés.";
		}
		// $globals['register_data'] = $globals['form'];
		$globals['Pageweb'] = $this->get(servicePageweb::class)->getRepository()->findOneBy(['bundlepathname' => 'WebsiteSiteBundle:Pageweb:register']);

		$templatename = empty($globals['Pageweb']) ? 'WebsiteSiteBundle:Special:register.html.twig' : $globals['Pageweb']->getBundlepathname();
		return $this->render($templatename, $globals);
	}

	/**
	 * @param Request $request
	 * @Route(
	 *		"/user/register",
	 *		name="user_register_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 */
	public function postRegisterAction(Request $request) {
		$globals = [];
		$serviceUser = $this->get(serviceUser::class);
		if($serviceUser->isAuthorizedToRegister()) {
			$getEntityManager = $serviceUser->getEntityManager();
			$form = $serviceUser->getRegistrationDataForForm(false, [], ['register','register_extended'], false);
			$globals['user'] = $form->getData();
			$form->handleRequest($request);
			if ($form->isSubmitted()) {
				if ($form->isValid()) {
					$getEntityManager->persist($globals['user']);
					$getEntityManager->flush();
					return $this->render('WebsiteSiteBundle:Special:confirmed.html.twig', $globals);
				}

				$event = new FormEvent($form, $request);
				$this->eventDispatcher ??= $this->get('event_dispatcher');
				$this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

				if (null !== $response = $event->getResponse()) {
					return $response;
				}
			}
			$globals['form'] = $form->createView();
			return $this->render('WebsiteSiteBundle:Special:register.html.twig', $globals);
		} else {
			return $this->redirectToRoute('user_register');
		}
	}



}
