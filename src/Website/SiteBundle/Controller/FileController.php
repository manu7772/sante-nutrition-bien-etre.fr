<?php

namespace Website\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;

// FileBundle
use ModelApi\FileBundle\Entity\File;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FileController extends Controller {

	/**
	 * @see https://ourcodeworld.com/articles/read/329/how-to-send-a-file-as-response-from-a-controller-in-symfony-3
	 */

	/**
	 * @Route(
	 * 		"/view/{slug}.{ext}/{version}",
	 * 		name="website_view_media",
	 * 		defaults={ "version" = null },
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function viewMediaAction($slug, $ext, $version = null, Request $request) {
		$file = $this->get(serviceEntities::class)->getRepository(File::class)->findOneBy(['slug' => $slug]);
		if(is_integer($version)) $file = $file->getVersion($version);
		if(empty($file) || !$file->isActive()) throw new NotFoundHttpException("File not found");
		// $publicResourcesFolderPath = $this->get('kernel')->getRootDir() . '/../web/public-resources/';
		// $response->headers->set('Content-type' , 'application/pdf');
		return new BinaryFileResponse($file->getFileUrl());
	}

	/**
	 * @Route(
	 * 		"/download/{slug}.{ext}/{version}",
	 * 		name="website_download_media",
	 * 		defaults={ "version" = null },
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function downloadMediaAction($slug, $ext, $version = null, Request $request) {
		$file = $this->get(serviceEntities::class)->getRepository(File::class)->findOneBy(['slug' => $slug]);
		if(is_integer($version)) $file = $file->getVersion($version);
		if(empty($file) || !$file->isActive()) throw new NotFoundHttpException("File not found");
		// $publicResourcesFolderPath = $this->get('kernel')->getRootDir() . '/../web/public-resources/';
		// $response->headers->set('Content-type' , 'application/pdf');
		$filename = $file->getFileUrl();
		// This should return the file to the browser as response
		$response = new BinaryFileResponse($filename);
		// To generate a file download, you need the mimetype of the file
		$mimeTypeGuesser = new FileinfoMimeTypeGuesser();
		// Set the mimetype with the guesser or manually
		if($mimeTypeGuesser->isSupported()){
			// Guess the mimetype of the file according to the extension of the file
			$response->headers->set('Content-Type', $mimeTypeGuesser->guess($filename));
		} else {
			// Set the mimetype of the file manually, in this case for a text file is text/plain
			$response->headers->set('Content-Type', 'text/plain');
		}
		$originalFileName = $file instanceOf File ? $file->getCurrentVersion()->getOriginalFileName() : $file->getOriginalFileName();
		// Set content disposition inline of the file
		$response->setContentDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			$originalFileName
		);
		return $response;
	}

	// // Return a dinamically created file
	// // When sending a dinamically created file, you must add a Content-Disposition header to your response. In a regular HTTP response, the Content-Disposition response header is a header indicating if the content is expected to be displayed inline in the browser, that is, as a Web page or as part of a Web page, or as an attachment in this case, that is downloaded and saved locally. In this case, our file doesn't exist in the storage of the server but in the memory and variables.
	// // While creating this header for basic file downloads is easy, using non-ASCII filenames is more involving. The makeDisposition() abstracts the hard work behind a simple API:
	// public function dynamicallyMediaAction() {
	// 	// Provide a name for your file with extension
	// 	$filename = 'TextFile.txt';
	// 	// The dinamically created content of the file
	// 	$fileContent = "Hello, this is the content of my File";
	// 	// Return a response with a specific content
	// 	$response = new Response($fileContent);
	// 	// Create the disposition of the file
	// 	$disposition = $response->headers->makeDisposition(
	// 		ResponseHeaderBag::DISPOSITION_ATTACHMENT,
	// 		$filename
	// 	);
	// 	// Set the content disposition
	// 	$response->headers->set('Content-Disposition', $disposition);
	// 	// Dispatch request
	// 	return $response;
	// }


}
