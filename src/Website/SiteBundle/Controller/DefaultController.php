<?php

namespace Website\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

// TEMP
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Filesystem\Filesystem;
// TEMP


// BaseBundle
// use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Entity\Blog;
use ModelApi\BaseBundle\Entity\Annonce;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceBlog;
use ModelApi\BaseBundle\Service\serviceAnnonce;
// FileBundle
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Service\serviceImage;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceItem;
// EventBundle
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Service\serviceEvent;
// MarketBundle
use ModelApi\MarketBundle\Entity\Product;
use ModelApi\MarketBundle\Service\serviceProduct;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceTranslation;
// PagewebBundle
use ModelApi\PagewebBundle\Entity\Pageweb;
use ModelApi\PagewebBundle\Service\servicePageweb;
use ModelApi\PagewebBundle\Service\TwigDatabaseLoader;
// UserBundle
use ModelApi\UserBundle\Service\serviceEntreprise;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use \Exception;

class DefaultController extends Controller {

	const DEFAULT_ROUTE = 'website_site_homepage';
	const CAT_NAME = 'cat';

	public function redirectLastOrDefaultUrl($request, $defaultRoute = null, $params = []) {
		if(!is_string($defaultRoute)) $defaultRoute = DefaultController::DEFAULT_ROUTE;
		$url = $this->get(serviceRoute::class)->getLastOrDefaultUrl($request, $defaultRoute, $params);
		return $this->redirect($url);
	}

	/**
	 * Find Pageweb for entity or list of entities (of same class!!!)
	 * if $entity is Entity, find Pageweb for this $entity
	 * if $entity is string (shortname with 's' or not), find first generic Pageweb for entity or for list of entities
	 * @param mixed $entity
	 * @return Pageweb
	 */
	protected function findPagewebAnyway($entity) {
		if($entity instanceOf Pageweb) return $entity;
		if(is_string($entity)) {
			// Shortname
			$pageweb = $this->get(servicePageweb::class)->getRepository()->findOnePageForItem($entity, null);
			if(!($pageweb instanceOf Pageweb) || !$this->get('templating')->exists($pageweb->getBundlepathname())) {
				throw new NotFoundHttpException("Page web for ".json_encode($entity)." not found");
			}
		} else if(is_iterable($entity)) {
			// Array of entities
			$pageweb = $this->get(servicePageweb::class)->getRepository()->findOnePageForItem($entity, null);
			if(!($pageweb instanceOf Pageweb) || !$this->get('templating')->exists($pageweb->getBundlepathname())) {
				$entity = reset($entity);
				if(is_object($entity)) throw new NotFoundHttpException("Page web for ".$entity->getShortname()." ".$entity->__toString()." not found");
				throw new NotFoundHttpException("Page web for ".gettype($entity)." not found");
			}
		} else {
			// Entity
			$pageweb = $entity->getPageweb();
			if(!($pageweb instanceOf Pageweb) || !$this->get('templating')->exists($pageweb->getBundlepathname())) {
				$pageweb = $this->get(servicePageweb::class)->getRepository()->findOnePageForItem($entity, null);
				if(!($pageweb instanceOf Pageweb) || !$this->get('templating')->exists($pageweb->getBundlepathname())) {
					throw new NotFoundHttpException("Page web for ".$entity->getShortname()." ".json_encode($entity->__toString())." not found");
				}
			}
			// if(!$pageweb->hasItemtype($entity->getShortname())) {
			// 	throw new Exception("This ".$entity->getShortname()." ".json_encode($pageweb->__toString())." does not manage this type of entity: ".$entity->getShortname()." ".json_encode($entity->__toString())."!", 1);
			// }
			$pageweb->setTempitem($entity);
		}
		return $pageweb;
	}



	/**
	 * @Route(
	 * 		"/",
	 * 		name=DefaultController::DEFAULT_ROUTE,
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function indexAction(Request $request) {
		// $this->get(serviceFlashbag::class)->addFlashToastr('success', 'Bonjour des Grottes !');
		// $globals['images'] = $this->get(serviceImage::class)->getRepository()->findImages();
		$globals['Pageweb'] = $this->get(TwigDatabaseLoader::class)->getPreferedTemplate();
		if(!($globals['Pageweb'] instanceOf Pageweb)) throw new NotFoundHttpException("Page not found");
		// $globals['resetUrl'] = $this->generateUrl('fos_user_resetting_reset', ['token' => '_oiThjzZAUo-ZYnRnTH2SaINFPMS9KHggZ01XaDwnAc']);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/cat/{cat1}/{cat2}/{cat3}/{cat4}/{cat5}/{cat6}/{cat7}/{cat8}/{cat9}",
	 * 		name=serviceRoute::CAT_ROUTE_NAME,
	 * 		defaults={"cat1" = null, "cat2" = null, "cat3" = null, "cat4" = null, "cat5" = null, "cat6" = null, "cat7" = null, "cat8" = null, "cat9" = null},
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function catAction($cat1 = null, $cat2 = null, $cat3 = null, $cat4 = null, $cat5 = null, $cat6 = null, $cat7 = null, $cat8 = null, $cat9 = null, Request $request) {
		if(null === $cat1) return $this->redirectToRoute(DefaultController::DEFAULT_ROUTE);
		$route = [
			'_route' => serviceRoute::CAT_ROUTE_NAME,
			'_route_params' => [ 'cat1' => $cat1, 'cat2' => $cat2, 'cat3' => $cat3, 'cat4' => $cat4, 'cat5' => $cat5, 'cat6' => $cat6, 'cat7' => $cat7, 'cat8' => $cat8, 'cat9' => $cat9 ],
		];
		$route['_route_params'] = array_filter($route['_route_params'], function($cat) { return is_string($cat); });
		$slug = end($route['_route_params']);
		$globals['breadcrumb'] = $this->get(serviceItem::class)->completeRoute($route);
		$item = $this->get(serviceItem::class)->getRepository()->findOneBySlugForPublicView(['slug' => $slug]);
		if($item instanceOf Menu) $globals['Menu'] = $item;
		$globals['Pageweb'] = $this->findPagewebAnyway($item);
		if($globals['Pageweb']->isPrefered()) return $this->redirectToRoute(DefaultController::DEFAULT_ROUTE);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/index/{menu}",
	 * 		name="website_pageweb_by_menu",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function menuAction($menu, Request $request) {
		$globals['Menu'] = $this->get(serviceMenu::class)->getRepository()->findOneBy(['slug' => $menu]);
		if(!($globals['Menu'] instanceOf Event)) throw new NotFoundHttpException("Menu ".json_encode($menu)." not found");
		$globals['Pageweb'] = $this->findPagewebAnyway($globals['Menu']);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/animation/{animation}",
	 * 		name="website_pageweb_by_animation",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function animationAction($animation, Request $request) {
		$globals['Item'] = $this->get(serviceEvent::class)->getRepository()->findOneBy(['slug' => $animation]);
		// if(!($globals['Item'] instanceOf Event)) throw new NotFoundHttpException("Animation ".json_encode($animation)." not found");
		$globals['Pageweb'] = $this->findPagewebAnyway($globals['Item']);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/evenement/{evenement}",
	 * 		name="website_pageweb_by_evenement",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function evenementAction($evenement, Request $request) {
		$globals['Item'] = $this->get(serviceEvent::class)->getRepository()->findOneBy(['slug' => $evenement]);
		if(!($globals['Item'] instanceOf Event)) throw new NotFoundHttpException("Event ".json_encode($evenement)." not found");
		$globals['Pageweb'] = $this->findPagewebAnyway($globals['Item']);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/blogs/{nb}",
	 * 		defaults={ "nb" = 8 },
	 * 		name="website_blogs",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function blogsAction($nb = 8, Request $request) {
		if($nb < 1) $nb = 1;
		$globals['Items'] = $this->get(serviceBlog::class)->getRepository()->findCurrents(null, $nb, Blog::DEFAULT_CATEGORY);
		// if(!count($globals['Items'])) throw new NotFoundHttpException("No blog found");
		$globals['Pageweb'] = $this->findPagewebAnyway($globals['Items']);;
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/blog/{blog}",
	 * 		name="website_pageweb_by_blog",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function blogAction($blog, Request $request) {
		$globals['Item'] = $this->get(serviceBlog::class)->getRepository()->findOneBy(['slug' => $blog]);
		// Default current Blog if not found or none
		// if(!($globals['Item'] instanceOf Blog)) $globals['Item'] = $this->get(serviceBlog::class)->getRepository()->findCurrent(null, Blog::DEFAULT_CATEGORY);
		// if(!($globals['Item'] instanceOf Blog)) throw new NotFoundHttpException("Blog ".json_encode($blog)." not found");
		$globals['Pageweb'] = $this->findPagewebAnyway($globals['Item']);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/annonces/{nb}",
	 * 		defaults={ "nb" = 8 },
	 * 		name="website_annonces",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function annoncesAction($nb = 8, Request $request) {
		if($nb < 1) $nb = 1;
		$globals['Items'] = $this->get(serviceAnnonce::class)->getRepository()->findCurrents(null, $nb);
		// if(!count($globals['Items'])) throw new NotFoundHttpException("No annonce found");
		$globals['Pageweb'] = $this->findPagewebAnyway($globals['Items']);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/annonce/{annonce}",
	 * 		name="website_pageweb_by_annonce",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function annonceAction($annonce, Request $request) {
		$globals['Item'] = $this->get(serviceAnnonce::class)->getRepository()->findOneBy(['slug' => $annonce]);
		// Default current Annonce if not found or none
		// if(!($globals['Item'] instanceOf Annonce)) $globals['Item'] = $this->get(serviceAnnonce::class)->getRepository()->findCurrent();
		// if(!($globals['Item'] instanceOf Annonce)) throw new NotFoundHttpException("Annonce ".json_encode($annonce)." not found");
		$globals['Pageweb'] = $this->findPagewebAnyway($globals['Item']);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/clinique/{clinique}",
	 * 		name="website_pageweb_by_clinique",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function cliniqueAction($clinique, Request $request) {
		$globals['Item'] = $this->get(serviceEntreprise::class)->getRepository()->findOneBy(['slug' => $clinique]);
		// Default current Clinique if not found or none
		// if(!($globals['Item'] instanceOf Clinique)) $globals['Item'] = $this->get(serviceEntreprise::class)->getRepository()->findCurrent();
		// if(!($globals['Item'] instanceOf Clinique)) throw new NotFoundHttpException("Clinique ".json_encode($clinique)." not found");
		$globals['Pageweb'] = $this->findPagewebAnyway($globals['Item']);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/product/{product}",
	 * 		name="website_pageweb_by_product",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function productAction($product, Request $request) {
		$globals['Item'] = $this->get(serviceProduct::class)->getRepository()->findOneBy(['slug' => $product]);
		if(!($globals['Item'] instanceOf Product)) throw new NotFoundHttpException("Product ".json_encode($product)." not found");
		$globals['Pageweb'] = $this->findPagewebAnyway($globals['Item']);
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 * 		"/audioguide",
	 * 		name="website_pageweb_audioguide",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"}
	 * )
	 */
	public function audioguideAction($product, Request $request) {
		$globals['Pageweb'] = $this->get(servicePageweb::class)->getRepository()->findOneBy(['slug' => 'telecharger-lapplication-mobile']);
		if(!($globals['Pageweb'] instanceOf Pageweb) || !$this->get('templating')->exists($globals['Pageweb']->getBundlepathname())) {
			throw new NotFoundHttpException("Page audioguide not found");
		}
		return $this->render($globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 *		"/public-switch-translation",
	 *		name="public_language_switchmode",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function switchTranslationModeAction(Request $request) {
		$this->get(serviceTranslation::class)->switchTranslatorMode();
		return $this->redirectLastOrDefaultUrl($request, DefaultController::DEFAULT_ROUTE);
	}



}
