<?php

namespace Website\SiteBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// BaseBundle
use ModelApi\BaseBundle\Interfaces\extendedBundleInterface;
// UserBundle
use ModelApi\UserBundle\Entity\UserInterface;

class WebsiteSiteBundle extends Bundle implements extendedBundleInterface {

	const TEMPLATE_STRUCTURE_NAME = 'star2';

	public function isPublicBundle() { return true; }

	public function getRoleTemplatable() { return UserInterface::ROLE_USER; }

	public function getTranslationDomain() { return serviceTwgTranslation::DEFAULT_DOMAIN; }

	public function isSeo() { return true; }

	public function getTemplateStructure() {
		$template = $this->container->getParameter('entities_parameters')['Pageweb']['structures'][static::TEMPLATE_STRUCTURE_NAME];
		$template['root']['seo'] = $this->isSeo();
		$template['root']['public'] = $this->isPublicBundle();
		return $template;
	}

}
