<?php
namespace Website\SiteBundle\Resources\Fixtures\entities;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
// use Doctrine\Common\DataFixtures\AbstractFixture;
// use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
// use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use FixturesBundle\DataFixtures\ORM\AbstractPreFixturesInterface;

class LoadFileformatData implements AbstractPreFixturesInterface {

	// use \FixturesBundle\Traits\FixturesByYml;

	protected $container;
	protected $_em;

	/**
	 * Sets the container.
	 * @param ContainerInterface|null $container A ContainerInterface instance or null
	 */
	public function setContainer(ContainerInterface $container = null) {
		$this->container = $container;
		return $this;
	}

	public function load(ObjectManager $_em) {
		$this->_em = $_em;
		// create entities...
	}

	/**
	 * {@inheritDoc}
	 * Get the order of this fixture
	 * @return integer
	 */
	public function getOrder() {
		return 1;
	}

	public function isEnabled() {
		return false;
	}

}
