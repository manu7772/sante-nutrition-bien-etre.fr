
$('.title-popover').popover({trigger: 'hover', html: true, container: 'body'});

var $popup_block = $('#popup_block');
if($popup_block !== undefined) $popup_block.modal();

// Trigger checked on first element of class "is-checked"
setTimeout(function() {
	$('.is-checked').first().select().trigger( "click" );
}, 500);
