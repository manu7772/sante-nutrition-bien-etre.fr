
var module_name = $('#body').attr('data-ng-app');
// console.debug('Module:', module_name);

var app = angular
	.module(module_name,
		[
			"ngSanitize",
			"ngResource",
			// "angular-ladda",
			"pascalprecht.translate",
			"summernote",
		]
	)
	.config(['$sceDelegateProvider', '$interpolateProvider', '$compileProvider',
		function config(
			$sceDelegateProvider,
			$interpolateProvider,
			$compileProvider,
			// laddaProvider
			) {
			$sceDelegateProvider.resourceUrlWhitelist([
				// Allow same origin resource loads.
				'self',
				// Allow loading from our assets domain.  Notice the difference between * and **.
				'https://www.youtube.com/**',
				'https://youtu.be/**',
				'https://www.google.com/**',
				'https://connect.facebook.net/**',
				'https://googleads.g.doubleclick.net/**',
				'https://static.doubleclick.net/**',
				'https://gadget.open-system.fr/**',
				// 'https://www.facebook.com/**',
			]);
			$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);
			// laddaProvider.setOption({ /* optional */
			// 	// style: 'expand-right',
			// 	style: 'overlay',
			// 	spinnerSize: 35,
			// 	spinnerColor: '#ffffff',
			// });
			$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
			// --> replacement in SublimText :
			// {{\s*([\s\w\d\.\-\+'"\|:;,\[\]\(\)@#\*\!\=]+)\s*}}
			// {[{ $1 }]}
		}
	])
	.run();






