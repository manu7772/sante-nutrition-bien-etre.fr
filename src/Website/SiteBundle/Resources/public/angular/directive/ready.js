// Set dot to green when angular is ready
// use :
// <span data-angular-ready><i class="fa fa-circle fa-fw text-danger"></i></span>
app.directive("angularReady", [function () {
	return {
		restrict : "A",
		replace: true,
		template : '<i class="fa fa-circle fa-fw text-success"></i>',
		// scope: true,
		// controller: function ($scope, $element) {
			// $element.attr('title', 'Angular version '+angular.version.full);
			// console.debug('Angular:', 'READY');
			// if(!!Tinycon) {
			// 	Tinycon.setBubble("A");
			// 	Tinycon.setOptions({ background: '#337ab7' });
			// }
			// $('>i', $element).addClass('text-success');
		// },
	};
}]);


// Modale for one day of agenda informations
// use :
// <element data-agenda-day="{{ item2|json_encode }}">...</element>
app.directive("agendaDay", ["$templateRequest","$compile", "$timeout","entities", function ($templateRequest, $compile, $timeout, entities) {
	return {
		restrict : "A",
		replace: false,
		scope: {
			agendaDay: '=',
		},
		controller: function ($scope, $element) {
			if(angular.isObject($scope.agendaDay)) {
				$element.on('click', function (item) {
					// for(i in $scope.agendaDay) {
						// $scope.agendaDay[i] = angular.fromJson($scope.agendaDay[i]);
					// }
					console.debug('Agenda day original data:', $scope.agendaDay);
					if(undefined === $scope.agendaDay.eventdates.Animation) $scope.agendaDay.eventdates.Animation = [];
					// $scope.agendaDay.eventdates.Animation.filter(function(value, index){ return a.indexOf(value) == index });
					var eventdates = {};
					angular.forEach($scope.agendaDay.eventdates.Animation, function(eventdate, index) {
						eventdates[eventdate.menutitle] = eventdate;
					});
					$scope.agendaDay.eventdates.Animation = eventdates;

					console.debug('Agenda day info:', $scope.agendaDay);
					// console.debug('Agenda day anims:', $scope.agendaDay.eventdates.Animation);
					var templateUrl = entities.getBaseUrl('bundles/websitesite/angular/templates/modale/info-agenda-byday.html', false);
					// var html = '<hr><p>Horaires : de '+moment($scope.agendaDay.start).format('h:mm')+' à '+moment($scope.agendaDay.end).format('h:mm')+'</p>';
					$templateRequest(templateUrl).then(
						function success(template) {
							var $template = angular.element(template);
							var btnPdf = '';
							if(angular.isObject($scope.agendaDay.parc.pdf) && angular.isString($scope.agendaDay.parc.pdf.file_url)) {
								btnPdf = '&nbsp;<a href="'+entities.getBaseUrl($scope.agendaDay.parc.pdf.file_url, false)+'" target="_blank" role="button" class="pull-right btn btn-sm text-white" style="background-color:'+$scope.agendaDay.parc.color+';"><i class="fa fa-file-pdf-o fa-fw"></i> PDF</a>';
							}
							$compile($template)($scope);
							Swal.fire({
								title: '<span class="pull-left">'+moment($scope.agendaDay.start).format("DD/MM/YYYY")+'</span>'+btnPdf,
								html: $template.get(0),
								showConfirmButton: false,
								showCancelButton: false,
								// cancelButtonText: 'Fermer',
								// width: '96%',
								showCloseButton: true,
								focusCancel: false,
							});
						},
						function error(response) {
							toastr.error('<strong class="text-white">Erreur</strong><br><p class="text-white">Informations indisponibles !</p>');
						}
					);
				});
			}
		},
	};
}]);

// go href on click
// use :
// <element data-go-href="URL" target="_blank">...</element>
app.directive("goHref", [function () {
	return {
		restrict : "A",
		replace: false,
		scope: {
			goHref: '=',
		},
		controller: function ($scope, $element) {
			if(angular.isString($scope.goHref)) {
				$element.on('click', function (event) {
					event.preventDefault();
					var blank = $element.attr('target') === '_blank';
					// alert('Clicked: '+blank);
					if(blank) window.open($scope.goHref);
						else location.replace($scope.goHref);
				});
			}
		},
	};
}]);

// Modale for entity informations
// use :
// <element data-info-entity="#id">...</element>
app.directive("infoEntity", ["$templateRequest","$compile","$timeout","sitedata","entities",'localization', function ($templateRequest, $compile, $timeout, sitedata, entities, localization) {
	return {
		restrict : "A",
		replace: false,
		scope: {
			infoEntity: '=',
		},
		controller: function ($scope, $element) {
			$element.on('click', function (item) {
				$scope.loading = true;
				$scope.entity = {};
				$scope.sitedata = sitedata;
				$scope.operator = sitedata.get('entreprise').preferences.MarketplaceBundle.operator;
				var openModale = function() {
					// $scope.text = $scope.entity.fulltext || $scope.entity.resume || $scope.entity.description || $scope.entity.accroche || '';
					// var $template = angular.element("<h3>{[{ entity.name }]} / <small>{{ entity.longname }}</small></h3>");
					var getShortname = function(data) {
						if(/^bddid@/.test(data)) {
							return entities.getShortnameByBddid(data);
						} else {
							try {
								// infoEntity is json
								entity = angular.fromJson(data);
							} catch (e) {
								// infoEntity is id element reference
								try	{
									entity = angular.fromJson($(data).attr("data-entity-data"));
								} catch (e) {
									return false;
								}
							}
							if(angular.isObject(entity) && Object.keys(entity).length) {
								return entities.getShortnameByBddid(entity.bddid);
							}
						}
						return false;
					};
					var shortname = getShortname($scope.infoEntity);
					// console.debug("Loading "+shortname);
					if(angular.isString(shortname)) {
						var templateUrl = entities.getBaseUrl('bundles/websitesite/angular/templates/modale/info-' + shortname.toLowerCase() + '.html', false);
						$templateRequest(templateUrl).then(
							function success(template) {
								var $template = angular.element(template);
								// $compile($template)($scope);
								// var html = angular.element('<div></div>');
								// html.append($template);
								Swal.fire({
									// icon: 'error',
									// title: $scope.entity.longname,
									html: '<div id="modale-info-entity"></div>',
									showConfirmButton: false,
									width: '96%',
									showCloseButton: true,
									showCancelButton: false,
									// focusCancel: false,
									// cancelButtonText: 'Fermer',
									footer: '<small><strong>Attention</strong> : pensez à vérifier les horaires le jour de votre venue aux Grottes du Cerdon.</small>',
									onOpen: function(element) {
										$compile($template)($scope);
										$('#modale-info-entity').append($template);
										var lauchWidget = function() {
											// if(shortname === "Product") {
												console.debug("Loading widgetProduit for "+shortname+' with locale '+localization.getLocale(true)+' ('+localization.getLocale()+')');
												// if($scope.entity.active) {
													$timeout(function() {
														// console.debug("SWAL lauched!");
														var widgetProduit = AllianceReseaux.Widget.Instance( "Produit", { idPanier:"MyAzMzM", idIntegration:1160, langue:'fr', ui:$scope.entity.opcode } );
														widgetProduit.Initialise();
													}, 100);
												// }
											// };
										};
										if(/^bddid@/.test($scope.infoEntity)) {
											// infoEntity id Bddid
											entities.getEntityByBddid($scope.infoEntity).then(
												function success(response) {
													$scope.entity = response.data;
													console.debug(shortname + ' info (Ajax):', $scope.entity);
													$scope.loading = false;
													if(shortname === "Product") lauchWidget();
												},
												function error(response) {
													console.error(response);
													toastr.error('<strong class="text-white">Erreur</strong><br><p class="text-white">Billet indisponible !</p>');
												}
											);
										} else {
											try {
												// infoEntity is json
												$scope.entity = angular.fromJson($scope.infoEntity);
											} catch (e) {
												// infoEntity is id element reference
												var $bloc = $($scope.infoEntity);
												var entity_data = $bloc.attr("data-entity-data");
												$scope.entity = angular.fromJson(entity_data);
											}
											if(angular.isObject($scope.entity) && Object.keys($scope.entity).length) {
												console.debug(shortname + ' info (Json):', $scope.entity);
												$scope.loading = false;
												if(shortname === "Product") lauchWidget();
											} else {
												toastr.error('<strong class="text-white">Erreur</strong><br><p class="text-white">Billet indisponible !</p>');
											}
										}
									},
								});
							},
							function error(response) {
								if(sitedata.isDev()) toastr.error('<strong class="text-white">Erreur</strong><br><p class="text-white">Template introuvable ('+templateUrl+') !</p>');
									else toastr.error('<strong class="text-white">Erreur</strong><br><p class="text-white">Informations indisponibles !</p>');
							}
						);
					} else {
						if(sitedata.isDev()) toastr.error('<strong class="text-white">Erreur</strong><br><p class="text-white">Entity informations missing ('+angular.toJson($scope.infoEntity)+') !</p>');
							else toastr.error('<strong class="text-white">Erreur</strong><br><p class="text-white">Informations indisponibles !</p>');
					}
				};
				openModale();
			});
		},
	};
}]);



//*************************************************************************************************************
// POST TRAITEMENT
//*************************************************************************************************************

app.directive('img', [function () {
	return {
		restrict: 'E',
		replace: false,
		controller: function ($scope, $element) {
			var classes = $element.attr('class');
			if(undefined === classes || !classes.length) {
				$element.attr('class', 'img-thumbnail');
			}
		}
	};
}]);

// magnify-text
app.directive('magnifyText', [function () {
	return {
		restrict: 'A',
		replace: false,
		controller: function ($scope, $element) {
			$element.magnifyTexts(3, true);
		}
	};
}]);


$.fn.magnifyTexts = function(includeShortWords, includeParenthesis) {
	if(includeShortWords === undefined) includeShortWords = 2;
	if(includeParenthesis !== false) includeParenthesis = true;
		var magnify_text = function(text) {
			var nbsp = String.fromCharCode(160);
			var text = text.replace(/(?:\r\n|\r|\n)/g, ' ');
			text = text.replace(new RegExp('\\s{2,}', 'g'), ' ');
			// Chiffres, tél, etc.
			text = text.replace(/(\d)\s+(\d)/g, '$1'+nbsp+'$2');
			// Mots courts
			if(includeShortWords > 0) {
				text = text.replace(new RegExp('(\\s+)([\\wàâäéèêëîïôöûüù]{1,' + includeShortWords + '})(\\s+)', 'gi'), ' $2' + nbsp);
				text = text.replace(new RegExp('('+nbsp+')([\\wàâäéèêëîïôöûüù]{1,' + includeShortWords + '})(\\s+)', 'gi'), nbsp + '$2' + nbsp);
			}
			// Parenthèses, accolades, etc.
			if(includeParenthesis) {
				text = text.replace(new RegExp('([\\(|\\[|\\{])(\\s+|'+nbsp+')', 'g'), '$1');
				text = text.replace(new RegExp('(\\s+|'+nbsp+')([\\)|\\]|\\}])', 'g'), '$2');
			}
			// Ponctuations
			text = text.replace(new RegExp('(\\s)+([!¡?¿:;\\/])', 'g'), nbsp + '$2');
			text = text.replace(new RegExp('(\\s)+([\'\\,\\.])', 'g'), '$2');
			return text;
		}
		var i = 0;
		var stop = false;
		$(this).contents().each(function() {
			// Prevent actions in Symfony Toolbar!!!
			if($(this).hasClass('sf-toolbar')) stop = true;
			if(!stop) {
				switch (this.nodeType) {
					case Node.ELEMENT_NODE:
						$(this).magnifyTexts(includeShortWords, includeParenthesis);
						break;
					case Node.TEXT_NODE:
						var text = this.textContent;
						if(text.replace(/[\s\r\n]+/g, '').length) this.textContent = magnify_text(text);
							else this.textContent = text.replace(/[\s\r\n]+/g, '');
						break;
					default:
						// do nothing...
						break;
				}
			}
		});
	return;
}

