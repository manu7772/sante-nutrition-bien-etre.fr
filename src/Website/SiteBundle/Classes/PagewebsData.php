<?php

namespace Website\SiteBundle\Classes;


class PagewebsData {

	const MODEL_CODE_TWIG = '<section class="py-8 py-md-10">
	<div class="container">
		<div class="text-center border-bottom pb-6 mb-7">
			{% set longtext = Section.fulltext|default(Section.resume) %}
			{% if longtext is not null %}
			<section class="pb-4"><div class="container"><div class="row"><div class="col-lg-12"><p class="mb-6">{{ include(template_from_string(longtext, Section.name)) }}</p></div></div></div></section>
			{% endif %}
		</div>
	</div>
</section>';

	const MODEL_CODE_TWIG_BANNER = '{% set slides = {
	\'slide3\': {img: \'/assets/bandeau3.jpg\', hommep: null},
	\'slide1\': {img: \'/assets/bandeau1.jpg\', hommep: null},
	\'slide6\': {img: \'/assets/bandeau2.jpg\', hommep: null},
} %}
{% set slider_id = \'slider_03\' %}
{% set rev_slider_auto = \'rev_slider_auto\' %}

<div id="{{ slider_id }}_wrapper" class="{{ rev_slider_auto }} rev_slider_wrapper fullwidthbanner-container" data-alias="grottes-du-cerdon-03" data-source="gallery" style="margin:0px auto;background:#dee2e6;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.4.8.1 fullwidth mode -->
	<div id="{{ slider_id }}" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8.1">
		<ul>
			{% for slideId,Aslide in slides %}

			{% if slideId == \'slide1\' %}
			<!-- SLIDE {{ slideId }}  -->
			<li data-index="rs-{{ slideId }}" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="-190"  data-delay="5900"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->
				<img src="{{ asset(\'bundles/websitesite/Generated_sliders/\' ~ slider_id ~ Aslide.img) }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
				<!-- LAYERS -->
				<!-- LAYER NR. 1 -->
				<div class="tp-caption tp-resizeme" 
					id="slide-{{ slideId }}-layer-1" 
					data-x="center"
					data-hoffset="" 
					data-y="320" 
					data-width="[\'900\']"
					data-height="[\'auto\']"
					data-type="text" 
					data-responsive_offset="on" 
					data-frames=\'[{"delay":840,"speed":1200,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"+3280","speed":540,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]\'
					data-textAlign="[\'right\',\'right\',\'right\',\'right\']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"
					style="z-index: 1; min-width: 550px; max-width: 550px; white-space: normal; font-size: 50px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Ubuntu; font-style:italic;"
				>
					se rassembler pour mieux affronter les défis de l\'avenir
					{# VISITE DES GROTTES &amp;&nbsp;PARC DE&nbsp;LOISIRS PRÉHISTORIQUES #}
				</div>
				<!-- LAYER NR. 2 -->
				{% if Aslide.hommep is not null %}
				<div class="tp-caption tp-resizeme" 
					id="slide-{{ slideId }}-layer-2" 
					data-x="-100" 
					data-y="" 
					data-width="[\'none\',\'none\',\'none\',\'none\']"
					data-height="[\'none\',\'none\',\'none\',\'none\']"
					data-type="image" 
					data-responsive_offset="on" 
					{# data-frames=\'[{"delay":1000,"speed":1000,"frame":"0","to":"o:1;","ease":"Power3.fadeInOut"},{"delay":"1000","speed":1000,"frame":"999","ease":"Power3.fadeInOut"}]\' #}
					data-frames=\'[{"delay":140,"speed":1200,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"+3800","speed":540,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]\'
					data-textAlign="[\'inherit\',\'inherit\',\'inherit\',\'inherit\']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"
					style="z-index: 2;"
				>
					<img src="{{ asset(\'bundles/websitesite/Generated_sliders/\' ~ slider_id ~ Aslide.hommep) }}" alt="" data-ww="447px" data-hh="500px" data-no-retina>
				</div>
				{% endif %}
			</li>
			{% endif %}

			{% if slideId == \'slide2\' %}
			<!-- SLIDE {{ slideId }}  -->
			<li data-index="rs-{{ slideId }}" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="-190"  data-delay="5010"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->
				<img src="{{ asset(\'bundles/websitesite/Generated_sliders/\' ~ slider_id ~ Aslide.img) }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
				<!-- LAYERS -->
				<!-- LAYER NR. 1 -->
				<div class="tp-caption tp-resizeme" 
					id="slide-{{ slideId }}-layer-1" 
					data-x="center"
					data-hoffset="" 
					data-y="center" 
					data-width="[\'900\']"
					data-height="[\'auto\']"
					data-type="text" 
					data-responsive_offset="on" 
					data-frames=\'[{"delay":840,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]\'
					data-textAlign="[\'inherit\',\'inherit\',\'inherit\',\'inherit\']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"
					style="z-index: 1; min-width: 613px; max-width: 613px; max-width: 300px; max-width: 300px; white-space: normal; font-size: 50px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Ubuntu;font-style:italic;"
				>
					se rassembler pour mieux affronter les défis de l\'avenir
				</div>
			</li>
			{% endif %}

			{% if slideId == \'slide3\' %}
			<!-- SLIDE {{ slideId }}  -->
			<li data-index="rs-{{ slideId }}" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="-190"  data-delay="5010"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->
				<img src="{{ asset(\'bundles/websitesite/Generated_sliders/\' ~ slider_id ~ Aslide.img) }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
				<!-- LAYERS -->
				<!-- LAYER NR. 1 -->
				<div class="tp-caption tp-resizeme" 
					id="slide-{{ slideId }}-layer-1" 
					data-x="center"
					data-hoffset="" 
					data-y="280" 
					data-width="[\'900\']"
					data-height="[\'320\']"
					data-type="text" 
					data-responsive_offset="on" 
					data-frames=\'[{"delay":840,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]\'
					data-textAlign="[\'center\',\'center\',\'center\',\'center\']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"
					style="z-index: 1; min-width: 613px; max-width: 613px; max-width: 300px; max-width: 300px; white-space: normal; font-size: 50px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Ubuntu;font-style:italic;"
				>
					un groupement d\'intérêt économique de vétérinaires des régions Normandie et Picardie
				</div>
			</li>
			{% endif %}

			{% if slideId == \'slide4\' %}
			<!-- SLIDE {{ slideId }}  -->
			<li data-index="rs-{{ slideId }}" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="-190"  data-delay="5900"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->
				<img src="{{ asset(\'bundles/websitesite/Generated_sliders/\' ~ slider_id ~ Aslide.img) }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
				<!-- LAYERS -->
				<!-- LAYER NR. 1 -->
				<div class="tp-caption tp-resizeme" 
					id="slide-{{ slideId }}-layer-1" 
					data-x="center"
					data-hoffset="" 
					data-y="120" 
					data-width="[\'550\']"
					data-height="[\'auto\']"
					data-type="text" 
					data-responsive_offset="on" 
					data-frames=\'[{"delay":840,"speed":1200,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"+3280","speed":540,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]\'
					data-textAlign="[\'inherit\',\'inherit\',\'inherit\',\'inherit\']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"
					style="z-index: 1; min-width: 550px; max-width: 550px; white-space: normal; font-size: 40px; line-height: 45px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Ubuntu;"
				>
					se rassembler pour mieux affronter les défis de l\'avenir
				</div>
				<!-- LAYER NR. 2 -->
				{% if Aslide.hommep is not null %}
				<div class="tp-caption tp-resizeme" 
					id="slide-{{ slideId }}-layer-2" 
					data-x="-100" 
					data-y="" 
					data-width="[\'none\',\'none\',\'none\',\'none\']"
					data-height="[\'none\',\'none\',\'none\',\'none\']"
					data-type="image" 
					data-responsive_offset="on" 
					data-frames=\'[{"delay":10,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]\'
					data-textAlign="[\'inherit\',\'inherit\',\'inherit\',\'inherit\']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"
					style="z-index: 2;"
				>
					<img src="{{ asset(\'bundles/websitesite/Generated_sliders/\' ~ slider_id ~ Aslide.hommep) }}" alt="" data-ww="447px" data-hh="500px" data-no-retina>
				</div>
				{% endif %}
			</li>
			{% endif %}

			{% if slideId == \'slide5\' %}
			<!-- SLIDE {{ slideId }}  -->
			<li data-index="rs-{{ slideId }}" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="-190"  data-delay="5010"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->
				<img src="{{ asset(\'bundles/websitesite/Generated_sliders/\' ~ slider_id ~ Aslide.img) }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
				<!-- LAYERS -->
				<!-- LAYER NR. 1 -->
				<div class="tp-caption tp-resizeme" 
					id="slide-{{ slideId }}-layer-1" 
					data-x="center"
					data-hoffset="" 
					data-y="90" 
					data-width="[\'613\']"
					data-height="[\'300\']"
					data-type="text" 
					data-responsive_offset="on" 
					data-frames=\'[{"delay":840,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]\'
					data-textAlign="[\'inherit\',\'inherit\',\'inherit\',\'inherit\']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"
					style="z-index: 1; min-width: 613px; max-width: 613px; max-width: 300px; max-width: 300px; white-space: normal; font-size: 50px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Ubuntu;font-style:italic;"
				>
					un groupement d\'intérêt économique de vétérinaires des régions Normandie et Picardie
				</div>
			</li>
			{% endif %}

			{% if slideId == \'slide6\' %}
			<!-- SLIDE {{ slideId }}  -->
			<li data-index="rs-{{ slideId }}" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="-190"  data-delay="5010"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->
				<img src="{{ asset(\'bundles/websitesite/Generated_sliders/\' ~ slider_id ~ Aslide.img) }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
				<!-- LAYERS -->
				<!-- LAYER NR. 1 -->
				<div class="tp-caption tp-resizeme" 
					id="slide-{{ slideId }}-layer-1" 
					data-x="10"
					data-hoffset="" 
					data-y="90" 
					data-width="[\'613\']"
					data-height="[\'auto\']"
					data-type="text" 
					data-responsive_offset="on" 
					data-frames=\'[{"delay":840,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]\'
					data-textAlign="[\'inherit\',\'inherit\',\'inherit\',\'inherit\']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"
					style="z-index: 1; min-width: 613px; max-width: 613px; max-width: 300px; max-width: 300px; white-space: normal; font-size: 50px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Ubuntu;font-style:italic;"
				>
					Les cliniques et cabinets du groupement VetoAlliance offre un espace agréable riche en services à tous ses adhérents
				</div>
			</li>
			{% endif %}

			{% endfor %}
		</ul>
		<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
	</div>
</div>';

	const MODEL_CODE_TWIG_FOOTER = '<footer class="footer">
	<div class="footer-bg-color py-4">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-4 mb-7 mb-lg-0">
					<a class="d-inline-block" href="{{ path(\'website_site_homepage\') }}" title="{{ site_context.Entreprise.name|default(\'\') }}">
						{% if site_context.Entreprise.avatar|default(null) is not null %}
						<img class="w-100 mb-6" src="{{ asset(fileurl(site_context.Entreprise.avatar, \'original\')) }}" alt="{{ site_context.Entreprise.slug|default(\'\') }}">
						{% endif %}
					</a>
					<p class="mb-0 text-white">{{ site_context.Entreprise.description|default(\'\')|raw }}</p>
				</div>
				<div class="col-md-6 col-lg-4 mt-md-4 mb-7 mb-lg-0">
					<div class="title-tag text-white">
						<h6>{{ \'Informations\'|trans }}</h6>
					</div>
					<ul class="list-unstyled mb-0">
						<li class="media mb-2">
							<div class="mr-3"><i class="fa fa-home text-white" aria-hidden="true"></i></div>
							<div class="media-body text-white"><a class="text-white" href="{{ site_context.Entreprise.mainAdresse.address|default(\'\')|striptags|googlemappize }}" target="_blank">{{ site_context.Entreprise.mainAdresse.address|default(\'\')|raw }}</a></div>
						</li>
						{% set telephon = site_context.Entreprise.telephons|first %}
						{% if telephon|isString %}
						<li class="media mb-2">
							<div class="mr-3"><i class="fa fa-phone text-white" aria-hidden="true"></i></div>
							<div class="media-body text-white"><a class="text-white" href="tel:{{ site_context.Entreprise.telephon.phonenumber }}">{{ site_context.Entreprise.telephon.getPhonenumberHumanized() }}</a></div>
						</li>
						{% endif %}
						<li class="media">
							<div class="mr-3"><i class="fa fa-envelope-o text-white" aria-hidden="true"></i></div>
							<div class="media-body text-white"><a class="text-white" href="mailto:{{ site_context.Entreprise.email|default(\'\') }}">{{ site_context.Entreprise.email|default(\'\') }}</a></div>
						</li>
					</ul>
				</div>
				<div class="col-md-6 col-lg-4 mt-lg-4">
					<div class="title-tag text-white">
						<h6>{{ \'Newsletter\'|trans }}</h6>
					</div>
					<p class="text-white">{{ \'Abonnez-vous à la newsletter\'|trans }} {{ site_context.Entreprise.title|default(\'\') }}</p>
					<form class="mb-6" action="{{ path(\'website_site_subscribe_newsletter\') }}" method="post">
						<div class="input-group input-group-sm">
							<input name="email" type="email" class="form-control form-control-sm form-transparent text-white" required="" placeholder="{{ \'Votre adresse Email\'|trans }}">
							<div class="input-group-append">
								<button class="input-group-text border-0 btn btn-append hover-bg-primary" type="submit"><i class="fa fa-long-arrow-right text-white woc" aria-hidden="true"></i></button>
							</div>
						</div>
					</form>
					<ul class="list-inline d-flex mb-0">
						{% for reseau in site_context.Entreprise.reseausocials|default([]) %}
						<li class="mr-3 mr-lg-2 mr-xl-3 text-white"><a class="icon-default icon-border rounded-circle hover-bg-primary text-white" href="{{ reseau.url }}" target="_blank">{{ reseau.iconHtml(false, \'text-white\')|raw }}</a></li>
						{% endfor %}
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="copyright py-6">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 order-1 order-md-0">
					<p class="mb-0 mb-md-0 text-md-left text-white">
						<span data-angular-ready>
							<i class="fa fa-circle fa-fw text-danger"></i>
						</span> Copyright © {{ currentYear() }} {{ \'Tous droits réservés pour\'|trans }} <a href="{{ url(\'website_site_homepage\') }}" target="_blank">{{ site_context.Entreprise.title|default(\'\') }}</a>
					</p>
				</div>
				<div class="col-md-6">
					<ul class="list-inline text-capitalize d-flex align-items-center justify-content-md-end justify-content-center mb-md-0">
						<li class="mr-3 text-white"><a class="text-white" href="{{ url(\'website_pageweb_by_cat\', {cat1: \'informations-juridiques\'}) }}">{{ \'CGU site web\'|trans }}</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>';

	const MODEL_CODE_TWIG_HEADER = '<header class="header{{ Pageweb.getTpl(\'banner\')|default(null) is not null ? \' pb-9 pb-lg-0\' }}" id="pageTop">
{% import \'WebsiteSiteBundle:Macro:menuzord.html.twig\' as menuzord %}

	{% if not is_granted(\'ROLE_TRANSLATOR\') %}
	{% if not (app.user.preferences.cookies|default(false) or app.session.get(\'accepted_cookies\')|default(false)) %}
	<!-- Top Bar -->
	<div class="top-bar d-md-block">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-12">
					<ul class="list-inline d-flex mb-0">
						<li><div class="media-body" data-cookie-advert="\'{{ url(\'user_accept_cookies\', {id: app.user.id|default(0)}) }}\'">{{ \'cookies_advert\'|trans|raw }}</div></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	{% endif %}
	{% elseif is_granted(\'ROLE_SUPER_ADMIN\') or not site_context.isNormalContext() %}
	<!-- Top Bar -->
	<div class="top-bar d-md-block bg-danger text-white">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-12">
					<ul class="list-inline d-flex mb-0">
						<li><div class="media-body text-white"><strong>{{ \'Informations ADMIN\'|trans }}</strong> <i class="fa fa-arrow-right fa-fw"></i> </div></li>
						<li><div class="media-body text-white">Langue <strong>{{ currentLanguage.description|default(currentLanguage.fullname) }}</strong> <i class="fa fa-square fa-fw"></i> </div></li>
						<li><div class="media-body text-white" title="{{ site_context.getContextDate()|localizeddate(\'none\', \'none\', app.request.locale, null, "cccc d MMMM Y HH:mm") }}">Date <strong>{{ site_context.date_as_string|trans([], domain_admin) }}</strong> <i class="fa fa-square fa-fw"></i> </div></li>
						<li><div class="media-body text-white">Contexte <strong>{{ site_context.Entreprise.name|default(\'<i class="fa fa-times fa-fw"></i>\')|raw }}</strong></div></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	{% endif %}
	{{ menuzord.make_menu(Header.dirmenus|default([]), Pageweb|default(null)) }}
</header>';

	const MODEL_CODE_TWIG_MODALE = '<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-label="signupModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header rounded">
				<h3 class="modal-title text-uppercase font-weight-bold">{{ Section.titleh1|default(Section.title)|raw }}</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				{% set longtext = Section.fulltext|default(Section.resume) %}
				{% if longtext is not null %}
				<section class="pb-4"><div class="container"><div class="row"><div class="col-lg-12"><p class="mb-6">{{ include(template_from_string(longtext, Section.name)) }}</p></div></div></div></section>
				{% else %}
				<section class="pb-4"><div class="container"><div class="row"><div class="col-lg-12"><p class="mb-6">Message... ICI.</p></div></div></div></section>
				{% endif %}
			</div>
		</div>
	</div>
</div>';

	const MODEL_CODE_TWIG_SECTION = '<section class="py-8 py-md-10">
	<div class="container">
		<div class="text-center border-bottom pb-6 mb-7">
			{% set longtext = Section.fulltext|default(Section.resume) %}
			{% if longtext is not null %}
			<section class="pb-4"><div class="container"><div class="row"><div class="col-lg-12"><p class="mb-6">{{ include(template_from_string(longtext, Section.name)) }}</p></div></div></div></section>
			{% endif %}
		</div>
	</div>
</section>';

	const MODEL_CODE_TWIG_TITLE = '<section class="page-title">
	{% set elements = pagewebDefaultsValues([Pageweb, Title], {
		title: [\'titleh1\',\'title\'],
		subtitle: [\'subtitle\',\'accroche\',\'menutitle\'],
		image: [\'entete\',\'photo\'],
	}) %}

	{% if elements.image is not null %}
	<div class="bg-img bg-overlay-darken" style="background-image: url({{ asset(fileurl(elements.image,\'optimized\',1440)) }});">
	{% else %}
	<div class="bg-img bg-overlay-darken">
	{% endif %}
		<div class="container">
			<div class="row align-items-center justify-content-center" style="height: {{ title_height|default(200) }}px;">
				<div class="col-lg-6">
					<div class="page-title-content">
						<div class="title-border">
							<h1 class="text-uppercase text-white font-weight-bold">{{ elements.title|default(\'TITRE MANQUANT\')|br_striptags|raw }}</h1>
						</div>
						{% if elements.subtitle|hasText %}
						<p class="font-weight-normal text-white mt-6 mb-4">{{ elements.subtitle|br_striptags|raw }}</p>
						{% endif %}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>';

	const MODEL_CODE_PAGEWEB = '{% extends \'WebsiteSiteBundle:Root:root_01.html.twig\' %}
{% import \'WebsiteSiteBundle:Macro:blocks.html.twig\' as blocks %}

{% block header %}
	{% set header = Pageweb.getTpl(\'header\') %}
	{% if header is not null %}{{ include(header.bundlepathname, {\'Pageweb\': Pageweb, \'Header\': header}) }}{% endif %}
{% endblock header %}

{% block banner %}
	{% set banner = Pageweb.getTpl(\'banner\') %}
	{% if banner is not null %}{{ include(banner.bundlepathname, {\'Pageweb\': Pageweb, \'Banner\': banner}) }}{% endif %}
{% endblock banner %}

{% block title %}
	{% set title = Pageweb.getTpl(\'title\') %}
	{% if title is not null %}{{ include(title.bundlepathname, {\'Pageweb\': Pageweb, \'Title\': title, \'subtitle\': null}) }}{% endif %}
{% endblock title %}

{% block extra_content %}

	{% set item = Pageweb.tempitem|default(Pageweb) %}

	{% if item.active|default(false) %}

		{% if item.accroche|default(null) is not null %}
		<section class="py-0"><div class="container"><div class="text-center"><h3 class="text-uppercase font-weight-bold position-relative"><span>{{ item.accroche|raw }}</span></h3></div></div></section>
		{% endif %}

		{% set longtext = item.fulltext|default(item.resume) %}
		{% if longtext|hasText %}
		<section class="pb-4"><div class="container"><div class="row"><div class="col-lg-12"><p class="mb-6">{{ include(template_from_string(longtext, item.name)) }}</p></div></div></div></section>
		{% endif %}

		{% set videos = item.getOwnerDirectoryChilds(\'drive/Video\', \'Video\') %}
		{% if videos|length %}
		{% set video = videos|first %}
		<section class="py-4 py-md-6 bg-smoke">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-12">
						<figure>{{ video.getEmbedVideo()|raw }}</figure>
					</div>
				</div>
			</div>
		</section>
		{% endif %}

		{% set images = item.getOwnerDirectoryChilds(\'system/Diaporamas\', \'Image\') %}
		{% if images|length %}
		<section class="py-4 py-md-6 bg-smoke">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-8">
						{{ blocks.slider_carousel(images, {type: \'panoramics\', size:800, suffle: false}) }}
					</div>
				</div>
			</div>
		</section>
		{% endif %}

	{% else %}
	{{ blocks.notfound(\'Entreprise\') }}
	{% endif %}

{% endblock %}

{% block section %}
	{% for section in Pageweb.getTpls(\'section\') %}
		{{ include(section.bundlepathname, {\'Pageweb\': Pageweb, \'Section\': section}) }}
	{% endfor %}
{% endblock section %}

{% block footer %}
	{% set footer = Pageweb.getTpl(\'footer\') %}
	{% if footer is not null %}{{ include(footer.bundlepathname, {\'Pageweb\': Pageweb, \'Footer\': footer}) }}{% endif %}
{% endblock footer %}

{% block modale %}
	{% for modale in Pageweb.getTpls(\'modale\') %}
		{{ include(modale.bundlepathname, {\'Pageweb\': Pageweb, \'Modale\': modale}) }}
	{% endfor %}
{% endblock modale %}';

	const MODEL_CODE_PAGEWEB_ROOT = '{% extends \'WebsiteSiteBundle:Root:root_01.html.twig\' %}
{% import \'WebsiteSiteBundle:Macro:blocks.html.twig\' as blocks %}

{% block header %}
	{% set header = Pageweb.getTpl(\'header\') %}
	{% if header is not null %}{{ include(header.bundlepathname, {\'Pageweb\': Pageweb, \'Header\': header}) }}{% endif %}
{% endblock header %}

{% block banner %}
	{% set banner = Pageweb.getTpl(\'banner\') %}
	{% if banner is not null %}{{ include(banner.bundlepathname, {\'Pageweb\': Pageweb, \'Banner\': banner}) }}{% endif %}
{% endblock banner %}

{% block title %}
	{% set title = Pageweb.getTpl(\'title\') %}
	{% if title is not null %}{{ include(title.bundlepathname, {\'Pageweb\': Pageweb, \'Title\': title, \'subtitle\': null}) }}{% endif %}
{% endblock title %}

{% block extra_content %}

	{% set item = Pageweb.tempitem|default(Pageweb) %}

	{% if item.active|default(false) %}

		{% if item.accroche|default(null) is not null %}
		<section class="py-0"><div class="container"><div class="text-center"><h3 class="text-uppercase font-weight-bold position-relative"><span>{{ item.accroche|raw }}</span></h3></div></div></section>
		{% endif %}

		{% set longtext = item.fulltext|default(item.resume) %}
		{% if longtext|hasText %}
		<section class="pb-4"><div class="container"><div class="row"><div class="col-lg-12"><p class="mb-6">{{ include(template_from_string(longtext, item.name)) }}</p></div></div></div></section>
		{% endif %}

		{% set videos = item.getOwnerDirectoryChilds(\'drive/Video\', \'Video\') %}
		{% if videos|length %}
		{% set video = videos|first %}
		<section class="py-4 py-md-6 bg-smoke">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-12">
						<figure>{{ video.getEmbedVideo()|raw }}</figure>
					</div>
				</div>
			</div>
		</section>
		{% endif %}

		{% set images = item.getOwnerDirectoryChilds(\'system/Diaporamas\', \'Image\') %}
		{% if images|length %}
		<section class="py-4 py-md-6 bg-smoke">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-8">
						{{ blocks.slider_carousel(images, {type: \'panoramics\', size:800, suffle: false}) }}
					</div>
				</div>
			</div>
		</section>
		{% endif %}

	{% else %}
	{{ blocks.notfound(\'Entreprise\') }}
	{% endif %}

{% endblock %}

{% block section %}
	{% for section in Pageweb.getTpls(\'section\') %}
		{{ include(section.bundlepathname, {\'Pageweb\': Pageweb, \'Section\': section}) }}
	{% endfor %}
{% endblock section %}

{% block footer %}
	{% set footer = Pageweb.getTpl(\'footer\') %}
	{% if footer is not null %}{{ include(footer.bundlepathname, {\'Pageweb\': Pageweb, \'Footer\': footer}) }}{% endif %}
{% endblock footer %}

{% block modale %}
	{% for modale in Pageweb.getTpls(\'modale\') %}
		{{ include(modale.bundlepathname, {\'Pageweb\': Pageweb, \'Modale\': modale}) }}
	{% endfor %}
{% endblock modale %}';


}
