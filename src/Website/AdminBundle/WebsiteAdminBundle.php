<?php

namespace Website\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
// BaseBundle
use ModelApi\BaseBundle\Interfaces\extendedBundleInterface;
// UserBundle
use ModelApi\UserBundle\Entity\UserInterface;

class WebsiteAdminBundle extends Bundle implements extendedBundleInterface {

	const TEMPLATE_STRUCTURE_NAME = 'inspinia';

	public function isPublicBundle() { return false; }

	public function getRoleTemplatable() { return UserInterface::ROLE_SUPER_ADMIN; }

	public function getTranslationDomain() { return serviceTwgTranslation::ADMIN_DOMAIN; }

	public function isSeo() { return false; }

	public function getTemplateStructure() {
		$template = $this->container->getParameter('entities_parameters')['Pageweb']['structures'][static::TEMPLATE_STRUCTURE_NAME];
		$template['root']['seo'] ??= $this->isSeo();
		$template['root']['public'] ??= $this->isPublicBundle();
		return $template;
	}

}
