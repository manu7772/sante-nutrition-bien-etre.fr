app
	.filter('default',function () {
		return function (input, defaultValue) {
			var defaultValue = defaultValue || null;
			return input || defaultValue;
		};
	})
	.filter('defaultfield',function () {
		return function (input) {
			return input || '<i class="fa fa-times fa-fw text-muted"></i>';
		};
	})
	.filter('defaulticon',function () {
		return function (input) {
			var icon = !!input.icon ? input.icon : input;
			var icon = angular.isString(icon) ? icon : 'fa fa-question';
			return icon;
		};
	})
	.filter('striphtml',function () {
		return function (string, keepBr) {
			if(undefined === keepBr || false === keepBr) return String(string).replace(/<[^>]+>/gm, '');
			return String(string).replace(/(<br>|<br\/>|<br \/>)/igm, '###_BR_###').replace(/<[^>]+>/gm, '').replace(/###_BR_###/gm, '<br>');
		};
	})
	.filter('magnify',function () {
		return function (string) {
			return String(string)
				.replace(/\s{2,}/img, ' ') // replace all double, triple or more spaces
				.replace(/(\s\w{1,3})(\s+)/igm, '$1&nbsp;') // do not have a short word at end of line
				.replace(/(\s+)([?!:;])/igm, '&nbsp;$2') // for orphan punctuations
				.replace(/(\s+)([\.,])/igm, '$2') // . and , have no space before us
				;
		};
	})
	.filter('keys', function () {
		return function (Obj) {
			return Object.keys(Obj);
		};
	})
	.filter('dateformat', function () {
		// eq. blog.publication|dateformat : 'YYYY'
		return function (item,format) {
			if(!item) return '<i class="fa fa-times fa-fw text-muted"></i>';
			// if(!item) item = [];
			// console.log('Date:', {item: item, format: format});
			var dd = moment(item);
			// console.log('To date:', dd.format(format ? format : 'YYYY-MM-DD'));
			return dd.format(format ? format : 'YYYY-MM-DD');
		};
	})
	.filter('numberformat', function () {
		return function (number,locale,options) {
			return localization.number_format(number, locale, options);
		};
	})
	.filter('trustAsHtml', ['$sce', function ($sce) {
		return function (html) {
			return $sce.trustAsHtml(html);
		};
	}])
	.filter('filesize', ["localization",function (localization) {
		return function (size,max) {
			return localization.printFilesize(size,max);
		};
	}])
	.filter('fileurl', ['entities', function (entities) {
		return function (entity,Xsize,type,forceType) {
			var url = entity.file_urls.original;
			type = type.replace(/s$/i,'').toLowerCase();
			url = entity.file_urls.original;
			if(type === 'realsize' && !!entity.file_urls.realsize) {
				url = entity.file_urls.realsize;
			} else if(type !== "original") {
				var Xsize = Xsize || 0;
				var type = type || 'optimized';
				// console.debug('Filter fileUrl:', {entity: entity, Xsize: Xsize, type: type});
				var sizefound = 0;
				angular.forEach(entity.file_urls, function(fileUrl,format) {
					var spl = format.split(/_W?/);
					var base = spl[0].replace(/s$/i,'');
					// var test = new RegExp('^'+type+'_', 'i');
					if(type === base) {
						switch(base) {
							case 'optimized':
								var width = parseInt(spl[1], 10);
								if(Xsize <= 0) {
									if(width >= sizefound) { sizefound = width; url = fileUrl; }
								} else if(width >= sizefound && sizefound < Xsize) {
									sizefound = width; url = fileUrl;
								} else if(sizefound > width && width >= Xsize) {
									sizefound = width; url = fileUrl;
								} else if(sizefound < 1 && !!forceType) {
									sizefound = width; url = fileUrl;
								}
								break;
							default:
								var width = parseInt(spl[1].split(/x/i)[0], 10);
								if(Xsize <= 0) {
									if(width >= sizefound) { sizefound = width; url = fileUrl; }
								} else if(width >= sizefound && sizefound < Xsize) {
									sizefound = width; url = fileUrl;
								} else if(sizefound > width && width >= Xsize) {
									sizefound = width; url = fileUrl;
								} else if(sizefound < 1 && !!forceType) {
									sizefound = width; url = fileUrl;
								}
								break;
						}
					}
				});
			}
			return entities.getBaseUrl(url, false);
			// var result = !!url  ? entities.getBaseUrl(url, false) : '#';
			// console.debug('View image URL:', result);
			// return result;
		};
	}])
;
