// if(/^\/app_dev.php(\/.+)?$/.test(window.location.pathname)) {
	// OVERRIDING CONSOLE FOR DEV
	var exconsole = {};
	var cons = ['log','info','warn','error','debug','table','assert','group','groupEnd','groupCollapsed','time','timeEnd'];
	angular.forEach(cons, function(value) {
		exconsole[value] = console[value];
		console[value] = function(d1,d2) { if(!!console_switch) exconsole[value].apply(this, arguments); };
	});
	console.on = function() { console_switch = true; };
	console.off = function() { console_switch = false; };
	console.off();
	if(/\/app_dev.php(\/.+)?$/.test(window.location.pathname)) {
		console.on();
		console.debug('CONSOLE:', "ON");
	}

	// Array utils
	if (!Array.prototype.last){
		Array.prototype.last = function() { return this[this.length - 1]; };
	};
	if (!Array.prototype.first){
		Array.prototype.first = function() { return this[0]; };
	};
// }

// https://stackoverflow.com/questions/12747904/js-override-confirm
// window.confirm = function(message, doConfirm) {
// 	var okConfirm = function() {
// 		if(!!doConfirm && (typeof doConfirm === "function")) {
// 			doConfirm();
// 		}
// 	};
// 	var doNotConfirm = function() {
// 	};
// 	var ttr = toastr.warning('Confirmez, svp.<br><button class="btn btn-danger" onClick="doNotConfirm();">NON</button><button class="btn btn-info" onClick="okConfirm();">OUI</button>');
// };


/*
Include SweetAlert before this script
http://t4t5.github.io/sweetalert
https://sweetalert.js.org/guides/
*/

window.alert = function (message, callback) {
	callback = callback || function(){};
	var options = {
		title: "",
		html: message,
		icon: "info",
		showConfirmButton: true,
		confirmButtonText: 'OK',
	};
	if(typeof(message) === "object") {
		options = message;
	}
	Swal.fire(options).then(function(response) { callback(response); });
};

window.confirm = function(message, title, callback) {
	callback = callback || function(){};
	var options = {
		title: title,
		html: message,
		icon: "warning",
		showConfirmButton: true,
		confirmButtonText: 'Confirmer',
		showCancelButton: true,
		cancelButtonText: 'Annuler',
		// dangerMode: true,
	};
	if(typeof(message) === "object") {
		options = message;
	}
	Swal.fire(options).then(function(response) { callback(response); });
};

function prompt(message, value, callback) {
	callback = callback || function(){};
	var options = {
		title: "",
		html: message,
		icon: "question",
		showCancelButton: true,
		inputValue: value,
	};
	if(typeof(message) === "object") {
		options = message;
	}
	Swal.fire(options).then(function(response) { callback(response); });
};
