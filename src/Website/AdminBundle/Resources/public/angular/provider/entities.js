app.provider('entities', [
	function () {
		var parent = this;
		var app_php = 'app_dev.php/';
		var isDevEnv = /^\/app_dev.php(\/.+)?$/.test(window.location.pathname);
		var ENV = isDevEnv ? app_php : '';
		var baseUrl = window.location.origin+'/';

		// console.debug('Host data:', {hostname: window.location.hostname, baseUrl: baseUrl, dev: isDevEnv, env: ENV, location: window.location});
		// alert(window.location.hostname);

		// https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value
		// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Erreurs/Cyclic_object_value
		var decycle = function(obj, stack = []) {
		    if (!obj || typeof obj !== 'object')
		        return obj;
		    
		    if (stack.includes(obj))
		        return null;

		    let s = stack.concat([obj]);

		    return Array.isArray(obj)
		        ? obj.map(x => decycle(x, s))
		        : Object.fromEntries(
		            Object.entries(obj)
		                .map(([k, v]) => [k, decycle(v, s)]));
		};


		this.$get = ['$http','$q','$timeout',
			function ($http, $q, $timeout) {

				var getBaseUrl = function(path, asRoute) {
					var route = !!asRoute ? baseUrl+ENV+path : baseUrl+path;
					// console.debug('Base url:', route);
					return route;
				};

				var getEntityById = function(shortname, id) {
					if(!id || id < 1) {
						var defer = $q.defer();
						defer.reject('Invalid ID for getting entity!');
						return defer.promise;
					}
					if(!shortname) {
						var defer = $q.defer();
						defer.reject('Invalid entity name: '+angular.toJson(shortname)+'!');
						return defer.promise;
					}
					shortname = shortname.toLowerCase();
					var url = getBaseUrl('api/'+shortname+'/'+id, true);
					console.debug('Get entity '+shortname+'/'+id+': ', url);
					var promise = $http({
						url: url,
						method: 'GET',
					});
					promise.then(
						function success(response) {
							console.debug('Entity:', response.data);
						},
						function error(response) {
							console.error(response);
							alert({text: "L'élément n'a pu être trouvé : "+response.statusText, icon: "error" });
						}
					);
					return promise;
				};

				var getItemById = function(id) {
					return getEntityById('Item', id);
				};

				var getShortnameByBddid = function(bddid) {
					if(/^bddid@/.test(bddid)) {
						var bddid = bddid.split('---');
						return bddid[bddid.length - 2];
					}
					return false;
				}

				var getEntityByBddid = function(bddid) {
					// bddid = bddid.split('---');
					// return getEntityById(bddid[1], bddid[bddid.length - 1]);
					var url = getBaseUrl('api/entity-bddid/get/'+bddid, true);
					console.debug('Get entity by BDDID '+bddid+': ', url);
					var promise = $http({
						url: url,
						method: 'GET',
					});
					promise.then(
						function success(response) {
							console.debug('Entity:', response.data);
						},
						function error(response) {
							console.error(response);
							alert({text: "L'élément n'a pu être trouvé : "+response.statusText, icon: "error" });
						}
					);
					return promise;
				};

				var deleteEntityByBddid = function(bddid) {
					var url = getBaseUrl('api/entity-bddid/delete/'+bddid, true);
					console.debug('Delete entity: ', url);
					var promise = $http({
						url: url,
						method: 'DELETE',
					});
					promise.then(
						null,
						function error(response) {
							console.error(response);
							alert({text: "L'élément n'a pu être supprimé : "+response.statusText, icon: "error" });
						}
					);
					return promise;
				};




				var JstreeCreate = function(node) {
					var url = getBaseUrl('api/jstree/item/create', true);
					switch(node.shortname) {
						case 'Group': url = getBaseUrl('api/jstree/group/create', true); break;
					}
					var promise = $http({
						url: url,
						method: 'POST',
						data: node,
					})
					promise.then(
						null,
						function error(response) {
							console.error(response);
							// alert({text: "L'élément n'a pu être supprimé : "+response.statusText, icon: "error" });
						}
					);
					return promise;
				};

				var JstreeMove = function(data) {
					var promise = $http({
						url: getBaseUrl('api/jstree/item/move', true),
						method: 'POST',
						data: decycle(data), // https://stackoverflow.com/questions/9382167/serializing-object-that-contains-cyclic-object-value
					})
					promise.then(
						null,
						function error(response) {
							console.error(response);
							// alert({text: "L'élément n'a pu être supprimé : "+response.statusText, icon: "error" });
						}
					);
					return promise;
				};

				var JstreeDelete = function(shortname, id) {
					var url = getBaseUrl('api/jstree/item/'+shortname+'/'+id, true);
					console.debug('Remove entity: ', url);
					var promise = $http({
						url: url,
						method: 'DELETE',
					});
					promise.then(
						null,
						function error(response) {
							console.error(response);
							alert({text: "L'élément n'a pu être supprimé : "+response.statusText, icon: "error" });
						}
					);
					return promise;
				};

				var JstreePaste = function(parent, child) {
					var promise = $http({
						url: getBaseUrl('api/jstree/item/paste', true),
						method: 'POST',
						data: {parent: parent, child: child},
					})
					promise.then(
						null,
						function error(response) {
							console.error(response);
							alert({text: "L'élément n'a pu être collé : "+response.statusText, icon: "error" });
						}
					);
					return promise;
				};

				var service = {
					current: null,
					// getCurrent: function() { return service.current; },
					getBaseUrl: function(path, asRoute) { return getBaseUrl(path, asRoute); },
					setCurrent: function(bddid) {
						if(angular.isString(bddid)) {
							// id done, so load entity from API
							// service.current = null; // state : loading...
							getEntityByBddid(bddid).then(
								function success(response) {
									service.current = response.data;
									console.debug('--- Current entity:', service.current);
								},
								function error(response) {
									service.current = null;
									console.error('Error while loading entity:', response);
									alert(response.data.message);
								},
							);
						} else {
							// entity or none (null, false, etc.) done
							service.current = bddid;
						}
						return service;
					},
					refreshCurrent: function() { return angular.isObject(service.current) ? setCurrent(data.id) : service; },
					getItemById: function(id) { return getItemById(id); },
					getEntityById: function(shortname, id) { return getEntityById(shortname, id); },
					// Jstree methods
					JstreeMove: function(data) { return JstreeMove(data); },
					JstreeDelete: function(shortname, id) { return JstreeDelete(shortname, id); },
					JstreePaste: function(parent, child) { return JstreePaste(parent, child); },
					// Actions with bddid
					JstreeCreate: function(node) { return JstreeCreate(node); },
					getEntityByBddid: function(bddid) { return getEntityByBddid(bddid); },
					deleteEntityByBddid: function(bddid) { return deleteEntityByBddid(bddid); },
					getShortnameByBddid: function(bddid) { return getShortnameByBddid(bddid); },
				};

				return service;
			}
		];

	}
]);






