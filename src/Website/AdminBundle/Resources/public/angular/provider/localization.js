app.provider('localization', [
	function () {
		var parent = this;

		var locale = $('html').attr('lang');

		var getLocale = function(short) {
			short = undefined === short ? false : short;
			if(!short) return locale;
			short = locale.split('-');
			if(undefined !== short[1]) return short[1].toLowerCase();
			return short[0].toLowerCase();
		};
		console.debug('Page web locale:', getLocale()+' ('+getLocale(true)+')');

		var number_format = function (number, locales, options) {
			// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/NumberFormat
			var number = parseFloat(number);
			if(!isFinite(number)) number = 0;
			var locales = !!locales ? locales : locale;
			var defaults = {
				style: "decimal",
				maximumFractionDigits: 2,
			};
			angular.extend(defaults, options);
			return new Intl.NumberFormat(locales, defaults).format(number);
		};

		var printFilesize = function (size, max, locales, options) {
			var sizes = ["o","ko","mo","go"];
			var max = !!max ? max.toLowerCase() : sizes.last();
			if(!sizes.includes(max)) max = sizes.last();
			var GO = 1000000000; // 1073741824 ???
			var MO = 1000000; // 1048576 ???
			var KO = 1000; // 1024
			var defaults = {
				style: "decimal",
				maximumFractionDigits: 1,
				minimumFractionDigits: 1,
			};
			angular.extend(defaults, options);
			if(size >= GO && !["o","ko","mo"].includes(max)) return number_format(size / GO, locales, defaults) + "Go";
			if(size >= MO && !["o","ko"].includes(max)) return number_format(size / MO, locales, defaults) + "Mo";
			if(size >= KO && !["o"].includes(max)) return number_format(size / KO, locales, defaults) + "Ko";
			return size + "octets";
		};


		this.$get = [
			function () {

				var service = {
					getLocale: function(short) { return getLocale(short); },
					number_format: function (number, locales, options) { return number_format(number, locales, options); },
					printFilesize: function (size, max, locales, options) { return printFilesize(size, max, locales, options); },
				};

				return service;
			}
		];

	}
]);






