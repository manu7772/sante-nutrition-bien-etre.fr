app.provider('translations', ['$translateProvider', 'sitedataProvider', 'notifyerProvider',
	function ($translateProvider, sitedataProvider, notifyerProvider) {
		var parent = this;
		var currentLocale = $('html').attr('lang');

		var translations = {
			'fr': {
				'Fermer': 'Fermer',
				'Textes': 'Textes',
				'Création': 'Création',
				'Modification': 'Dern. modif.',
				'Modifier': 'Modifier',
				'Cancel': 'Annuler',
				'Save': 'Enregistrer',
				'Voir': 'Voir',
				'VoirTout': 'Liste',
				'Search': 'Rechercher...',
				'Copier': 'Copier',
				'Imprimer': 'Imprimer',
				'Confirmer': 'Confirmer',
				'Disable': 'Désactiver',
				'Delete': 'Supprimer',
				'Csv': 'CSV',
				'Excel': 'Excel',
				'Pdf': 'Pdf',
				'Copier': 'Copier',
				'Imprimer': 'Imprimer',
				'Loading': 'Chargement...',
				'Oui': 'Oui',
				'Non': 'Non',
				'Quitter': 'Quitter',
				'Mon compte': 'Mon profil',
				'Messages': 'Mes messages',
				'Tableau de bord': 'Tableau de bord',
				'roles': {
					'names': 'Roles',
					'name': 'Role',
					'roles': {
						'ALL': 'tous utilisateurs',
						'ROLE_USER': 'utilisateur',
						'ROLE_TESTER': 'testeur',
						'ROLE_TRANSLATOR': 'traducteur',
						'ROLE_UPDATOR': 'éditeur',
						'ROLE_COLLAB': 'collaborateur',
						'ROLE_ADMIN': 'administrateur',
						'ROLE_SUPER_ADMIN': 'super administrateur'
					}
				},
				'jstree': {
					'contextmenu': {
						'nouveau': 'Créer',
						'renommer': 'Renommer',
						'editer': 'Editer',
						// 'Créer': 'Créer',
						'copier': 'Copier le lien',
						'coller': 'Coller le lien',
						'supprimer': 'Supprimer',
						'refresh': 'Rafraîchir'
					},
					'actions': {
						'reset': 'Réinitialiser',
						'redraw': 'Rafraîchir',
						'supprimer_name': 'Supprimer l\'élément "{[{ name  }]}" ?'
					},
					'info': {
						'generating': 'Création en cours…'
					},
					'success': {
						'create': 'La création été réalisée'
					},
					'errors': {
						'failedCreate': 'La création a échoué',
						'invalidName': 'Ce nom est incorrect'
					}
				},
				'entities': {
					'generic': {
						'names': 'Entités',
						'name': 'Entité',
						'fields': 'Champs',
						'field': 'Champ',
						'associations': 'Associations',
						'association': 'Association'
					},
					'group': {
						'names': 'Groupes',
						'name': 'Groupe',
						'fields': {
							'id': 'ID'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							'update': 'Modifier {[{ name  }]}',
							'create': 'Nouveau',
							'cancel': 'Annuler',
							'save': 'Enregistrer',
							'new': 'Nouveau groupe'
						}
					},
					'localtext': {
						'names': 'Traductions',
						'name': 'Traduction',
						'fields': {
							'id': 'ID',
							'text': 'Texte',
							'texts': 'Texte',
							'locales': 'Langues'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							'update': 'Modifier {[{ name  }]}',
							'create': 'Nouveau',
							'cancel': 'Annuler',
							'save': 'Enregistrer'
						}
					},
					'user': {
						'names': 'Utilisateurs',
						'name': 'Utilisateur',
						'fields': {
							'id': 'ID',
							'username': 'Nom utilisateur',
							'firstname': 'Nom',
							'lastname': 'Prénom',
							'email': 'Courriel',
							'name': 'Nom',
							'localtextname': 'Nom',
							'menutitle': 'Nom court',
							'localtextmenutitle': 'Nom court',
							'description': 'Description',
							'localtextdescription': 'Description',
							'functionality': 'Fonction',
							'localtextfunctionality': 'Fonction',
							'acceptCookies': 'Accepte cookies',
							'color': 'Couleur',
							'locale': 'Langue',
							'enabled': 'Actif',
							'lastLogin': 'Dernière connexion',
							'owneddirectory': 'Dossier',
							'avatar': 'Photo'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							'update': 'Modifier {[{ name  }]}',
							'create': 'Nouveau',
							'cancel': 'Annuler',
							'save': 'Enregistrer',
							'new': 'Nouvel Utilisateur'
						}
					},
					'atelier': {
						'names': 'Ateliers',
						'name': 'Atelier',
						'fields': {
							'id': 'ID',
							'name': 'Nom',
							'localtextname': 'Nom',
							'menutitle': 'Nom court',
							'localtextmenutitle': 'Nom court',
							'accroche': 'Accroche',
							'localtextaccroche': 'Accroche',
							'description': 'Description',
							'localtextdescription': 'Description',
							'functionality': 'Fonction',
							'localtextfunctionality': 'Fonction',
							'alertmsg': 'Alerte',
							'localtextalertmsg': 'Alerte',
							'weather': 'Note pratique',
							'localtextweather': 'Note pratique',
							'during': 'Durée',
							'localtextduring': 'Durée',
							'age': 'Âge',
							'localtextage': 'Âge',
							'color': 'Couleur',
							'locale': 'Langue',
							'enabled': 'Actif',
							'thumbnail': 'Icône',
							'resourceAsThumbnail': 'Icône'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							'update': 'Modifier {[{ name  }]}',
							'create': 'Nouveau',
							'create': 'Nouveau',
							'cancel': 'Annuler',
							'save': 'Enregistrer',
							'new': 'Nouvel Atelier'
						}
					},
					'entreprise': {
						'names': 'Entreprises',
						'name': 'Entrepriser',
						'fields': {
							'id': 'ID',
							'name': 'Nom',
							'localtextname': 'Nom',
							'locale': 'Langue',
							'enabled': 'Actif',
							'prefered': 'Principale',
							'accroche': 'Accroche',
							'description': 'Description',
							'googlecode': 'Code Google',
							'color': 'Couleur'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							'update': 'Modifier {[{ name  }]}',
							'create': 'Ajouter',
							'cancel': 'Annuler',
							'save': 'Enregistrer',
							'new': 'Ajouter entreprise'
						}
					},
					'tag': {
						'names': 'Tags',
						'name': 'Tag',
						'fields': {
							'id': 'ID',
							'name': 'Nom',
							'localtextname': 'Nom',
							'locale': 'Langue',
							'enabled': 'Actif'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							'update': 'Modifier {[{ name  }]}',
							'create': 'Nouveau',
							'cancel': 'Annuler',
							'save': 'Enregistrer',
							'new': 'Nouveau tag'
						}
					},
					'language': {
						'names': 'Langues',
						'name': 'Langue',
						'fields': {
							'id': 'ID',
							'name': 'Langue',
							'fullname': 'Nom ISO',
							'country': 'Pays ISO',
							'countryName': 'Nom du pays',
							'description': 'Description',
							'prefered': 'Langue par défaut sur le site web',
							'color': 'Couleur',
							'enabled': 'Visible sur le site web',
							'thumbnail': 'drapeau'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							'update': 'Modifier {[{ name  }]}',
							'create': 'Nouveau',
							'cancel': 'Annuler',
							'save': 'Enregistrer'
						}
					},
					'directory': {
						'names': 'Dossiers',
						'name': 'Dossier',
						'fields': {
							'id': 'ID',
							'name': 'Nom'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							// 'update': 'Modifier {[{ name  }]}',
							'create': 'Nouveau',
							// 'cancel': 'Annuler',
							// 'save': 'Enregistrer',
							'new': 'Nouveau dossier'
						}
					},
					'historic': {
						'names': 'Archives',
						'name': 'Archive',
						'fields': {
							'id': 'ID',
							'username': 'Nom utilisateur'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							// 'update': 'Modifier {[{ name  }]}',
							'create': 'Nouveau'
							// 'cancel': 'Annuler',
							// 'save': 'Enregistrer',
						}
					},
					'labolog': {
						'names': 'Logs',
						'name': 'Log',
						'fields': {
							'id': 'ID',
							'username': 'Nom utilisateur'
						},
						'actions': {
							'show': 'Voir {[{ name  }]}',
							// 'update': 'Modifier {[{ name  }]}',
							'create': 'Nouveau'
							// 'cancel': 'Annuler',
							// 'save': 'Enregistrer',
						}
					}
				},
				'languages': {
					'fr': 'Français',
					'fr-FR': 'Français',
					'fr_FR': 'Français',
					'en': 'Anglais',
					'en-GB': 'Anglais',
					'en_GB': 'Anglais',
					'nl': 'Néerlandais',
					'nl-NL': 'Néerlandais',
					'nl_NL': 'Néerlandais',
					'de': 'Allemand',
					'de-DE': 'Allemand',
					'de_DE': 'Allemand',
					'es': 'Espagnol',
					'es-ES': 'Espagnol',
					'es_ES': 'Espagnol',
					'ru-RU': 'Russe',
					'ru_RU': 'Russe'
				},
				'table': {
					'thead': {
						'id': 'ID',
						'actions': 'Actions'
					}
				},
				'errors': {
					'Internal Server Error': 'Erreur interne du serveur',
					'No Content': 'Aucune donnée trouvée',
					'Not Found': 'Aucune donnée trouvée',
					'Forbidden': 'Accès interdit pour cet élément',
					'Method Not Allowed': 'Permission refusée. Vous n\'avez pas les droits pour accéder à cette opération.',
					'Bad Request': 'Requête erronnée',
					'Validation Failed': 'Les données sont invalides (Erreur du programme : contactez le webmaster)',
					'undefined': 'Erreur indéterminée',
					'creator': 'Erreur, création impossible',
					'updator': 'Erreur, édition impossible',
					'disconnected': 'Vous avez été déconnecté. Reconnectez-vous, svp.',
					'documentation': {
						'missing': 'ERREUR CRITIQUE : les données API sont manquantes, ils est impossible de lancer le programme !<br>Veuillez contacter l\'administrateur.'
					},
					'save': 'Le formulaire est invalide. Vérifiez les champs notifiés, svp.',
					'system': 'Erreur système : contactez l\'administrateur',
					'no_data_for_submit': 'Aucune donnée à enregistrer',
					'not_modified': 'Une erreur a eu lieu lors de la modification (erreur {[{ status  }]})',
				},
				'success': {
					'save': 'Enregistrement effectué',
					'enabled': 'L\'élément a été activé (visible)',
					'disabled': 'L\'élément a été désactivé (invisible)',
					'unsoftdeleted': 'L\'élément a été rétabli (n\'est plus supprimé)',
					'softdeleted': 'L\'élément a été supprimé',
					'deleted': 'L\'élément a été supprimé de la base de données'
				},
				'messages': {
					'saving': 'Enregistrement...',
					'votretexte': 'Entrez votre texte ici...',
					'confirm_request': 'Veuillez confirmer, svp.',
					'confirm_request_disable': 'Veuillez confirmer la désactivation (invisible), svp.',
					'confirm_request_softdelete': 'Veuillez confirmer la suppression, svp.',
					'confirm_request_delete': 'Veuillez confirmer la suppression définitive, svp.'
				},
				'form': {
					'errors': {
						'invalid': 'Le formulaire est invalide',
						'The CSRF token is invalid. Please try to resubmit the form.': 'Le code de sécurité est invalide. Tentez de renvoyer le formulaire, svp.',
						'This value should not be null.': 'Cette valeur ne peut être nulle'
					}
				}
			}
		};
		// waiting for translaations…
		notifyerProvider.console.warn('COPIED TRANSLATIONS BY FR, WAITING FOR FINAL TRANSLATION. (see provider/translations.js / line 719)');
		translations.en = translations.fr;
		translations.de = translations.fr;
		translations.es = translations.fr;
		translations.nl = translations.fr;
		translations.ru = translations.fr;

		this.getShortname = function(language) {
			return language.split(sitedataProvider.get('country_separator'))[0];
		};

		this.getLocales = function() {
			return Object.keys(translations);
		};

		this.currentLocale = function(shortLocale) {
			return !!shortLocale ? parent.getShortname(currentLocale) : currentLocale;
		};

		this.getTranslations = function(language) {
			if(!language) language = parent.currentLocale();
			if(!translations[language]) language = parent.getShortname(language);
			return !!translations[language] ? translations[language] : undefined;
		};

		this.initTranslations = function() {
			var languages = sitedataProvider.getLanguages();
			var translations;
			angular.forEach(languages, function (language) {
				translations = parent.getTranslations(language);
				if(!!translations) $translateProvider.translations(language, translations);
			});
			// Available language keys
			var langageKeys = {};
			var locales = [];
			angular.forEach(languages, function (language) {
				locales.push(parent.getShortname(language));
				langageKeys[parent.getShortname(language)+sitedataProvider.get('country_separator')+'*'] = parent.getShortname(language);
			});
			// notifyerProvider.console.debug('- Locales:', locales);
			// notifyerProvider.console.debug('- LangageKeys:', langageKeys);
			$translateProvider.registerAvailableLanguageKeys(locales, langageKeys);
			$translateProvider.determinePreferredLanguage();
			// https://angular-translate.github.io/docs/#/guide/19_security
			$translateProvider.useSanitizeValueStrategy('sanitize');
			$translateProvider.preferredLanguage(parent.currentLocale());
			// https://angular-translate.github.io/docs/#/guide/11_custom-storages
			// $translateProvider.useStorage('storage');
		};
		parent.initTranslations();

		this.$get = ['$http', '$filter', 'sitedata',
			function ($http, $filter, sitedata) {
				$translateProvider.use(parent.currentLocale());
				// Configure Headers
				$http.defaults.headers.common[sitedata.get('locale_header_name')] = parent.currentLocale();
				// Configure momentJS
				moment.locale(parent.currentLocale(true));

				var service = {
					// getTranslations: function(language) { return parent.getTranslations(language); },
					currentLocale: function(shortLocale) { return parent.currentLocale(shortLocale); },
					translate: function(text, vars) { return $filter('translate')(text, vars); },
				};
				return service;
			}
		];

	}
]);






