app.provider('user', ['sitedataProvider',
	function (sitedataProvider) {
		var parent = this;

		var User;

		this.setUser = function (new_user) {
			User = new_user;
			return this;
		};
		parent.setUser(sitedataProvider.getUser());

		this.getUser = function () {
			return User;
		};

		this.getUsername = function () {
			return !!User ? User.username : null;
		};

		this.getUserHelp = function () {
			var help;
			try { help = User.preferences.help.value; } catch(e) { help = false; }
			return help;
		};

		this.isSuperadmin = function () {
			var superadmin;
			try { superadmin = User.roles.includes('ROLE_SUPER_ADMIN'); } catch(e) { superadmin = false; }
			return superadmin;
		};

		this.$get = [
			function () {

				var service = {
					setUser: function(new_user) { parent.setUser(new_user); return service; },
					getUser: function() { return parent.getUser(); },
					getUsername: function() { return parent.getUsername(); },
					isSuperadmin: function() { return parent.isSuperadmin(); },
					getUserHelp: function() { return parent.getUserHelp(); },
				};
				return service;
			}
		];

	}
]);






