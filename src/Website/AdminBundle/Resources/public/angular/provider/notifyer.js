app.provider('notifyer', ['sitedataProvider',
	function (sitedataProvider) {
		var parent = this;

		var Ntoastr = {
			toastr_init_options: angular.copy(toastr.options),
			toastr_default_options: {
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": 300,
				"hideDuration": 1500,
				"timeOut": 3000,
				"extendedTimeOut": 1000,
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			},
			reset: function () {
				Ntoastr.setDefaultOptions(Ntoastr.toastr_init_options);
			},
			init: function () {
				angular.copy(Ntoastr.toastr_default_options, toastr.options);
			},
			setDefaultOptions: function (options) {
				var options = !!options ? options : Ntoastr.toastr_default_options;
				angular.copy(options, Ntoastr.toastr_default_options);
				Ntoastr.init();
			},
			getDefaultOptions: function () {
				return Ntoastr.toastr_default_options;
			},
			addDefaultOption: function (name, value) {
				Ntoastr.toastr_default_options[name] = value;
				Ntoastr.init();
			},
		};
		Ntoastr.init();

		this.console = {
			info: function (title, data) { if(sitedataProvider.isDev()) !!data ? console.info(title, data) : console.info(title); },
			log: function (title, data) { if(sitedataProvider.isDev()) !!data ? console.log(title, data) : console.log(title); },
			debug: function (title, data) { if(sitedataProvider.isDev()) !!data ? console.debug(title, data) : console.debug(title); },
			warn: function (title) { if(sitedataProvider.isDev()) console.warn(title); },
			error: function (title, data) { if(sitedataProvider.isDev()) !!data ? console.error(title, data) : console.error(title); },
		};


		this.$get = ['$rootScope', '$compile', '$q',
			function ($rootScope, $compile, $q) {

				Ntoastr.info = function (text, vars, $isolated_scope, options) {
					var defer = $q.defer();
					if(!!options) angular.extend(toastr.options, options);
					var $message = $('<span data-translate="'+text+'" data-translate-values="'+angular.toJson(vars || {})+'"></span>');
					if(!$isolated_scope) $isolated_scope = $rootScope.$new();
					$compile($message)($isolated_scope);
					toastr.options.onHidden = function () { defer.resolve(true); };
					toastr['info']($message);
					if(!!options) Ntoastr.init();
					return defer.promise;
				};

				Ntoastr.success = function (text, vars, $isolated_scope, options) {
					var defer = $q.defer();
					if(!!options) angular.extend(toastr.options, options);
					var $message = $('<span data-translate="'+text+'" data-translate-values="'+angular.toJson(vars || {})+'"></span>');
					if(!$isolated_scope) $isolated_scope = $rootScope.$new();
					$compile($message)($isolated_scope);
					toastr.options.onHidden = function () { defer.resolve(true); };
					toastr['success']($message);
					if(!!options) Ntoastr.init();
					return defer.promise;
				};

				Ntoastr.warning = function (text, vars, $isolated_scope, options) {
					var defer = $q.defer();
					if(!!options) angular.extend(toastr.options, options);
					var $message = $('<span data-translate="'+text+'" data-translate-values="'+angular.toJson(vars || {})+'"></span>');
					if(!$isolated_scope) $isolated_scope = $rootScope.$new();
					$compile($message)($isolated_scope);
					toastr.options.onHidden = function () { defer.resolve(true); };
					toastr['warning']($message);
					if(!!options) Ntoastr.init();
					return defer.promise;
				};

				Ntoastr.error = function (text, vars, $isolated_scope, options) {
					var defer = $q.defer();
					if(!!options) angular.extend(toastr.options, options);
					var $message = $('<span data-translate="'+text+'" data-translate-values="'+angular.toJson(vars || {})+'"></span>');
					if(!$isolated_scope) $isolated_scope = $rootScope.$new();
					$compile($message)($isolated_scope);
					toastr.options.onHidden = function () { defer.resolve(true); };
					toastr['error']($message);
					if(!!options) Ntoastr.init();
					return defer.promise;
				};


				Ntoastr.confirm = function (type, text, vars, button_options, $isolated_scope, options) {
					var defer = $q.defer();
					if(!['info','success','warning','error'].includes(type)) type = 'info';
					if(!!options) angular.extend(toastr.options, options);
					if(angular.isString(button_options)) {
						button_options = {text: button_options};
					}
					var $message = $('<span><span data-translate="'+text+'" data-translate-values="'+angular.toJson(vars)+'"></span><br><br><button type="button" class="btn clear btn-white text-danger pull-right" data-ng-click="confirm()" data-translate="'+button_options.text+'" data-translate-values="'+angular.toJson(button_options.values || {})+'">OK</button></span>');
					if(!$isolated_scope) $isolated_scope = $rootScope.$new();
					$isolated_scope.confirm = function () {
						defer.resolve(true);
					};
					toastr.options.onHidden = function () { defer.reject(false); };
					toastr.options.timeOut = 8000;
					toastr.options.extendedTimeOut = 3000;
					// compile with $isolated_scope
					$compile($message)($isolated_scope);
					toastr[type]($message);
					if(!!options) Ntoastr.init();
					return defer.promise;
				};

				var service = {
					toastr: {
						// return promise
						info: function(text, vars, $isolated_scope, options) { return Ntoastr.info(text, vars, $isolated_scope, options); },
						success: function(text, vars, $isolated_scope, options) { return Ntoastr.success(text, vars, $isolated_scope, options); },
						warning: function(text, vars, $isolated_scope, options) { return Ntoastr.warning(text, vars, $isolated_scope, options); },
						error: function(text, vars, $isolated_scope, options) { return Ntoastr.error(text, vars, $isolated_scope, options); },
						confirm: function(type, text, vars, button_options, $isolated_scope, options) { return Ntoastr.confirm(type, text, vars, button_options, $isolated_scope, options); },
					},
					console: {
						info: function(title, data) { parent.console.info(title, data); },
						log: function(title, data) { parent.console.log(title, data); },
						debug: function(title, data) { parent.console.debug(title, data); },
						warn: function(title) { parent.console.warn(title); },
						error: function(title, data) { parent.console.error(title, data); },
					},
				};
				return service;
			}
		];

	}
]);






