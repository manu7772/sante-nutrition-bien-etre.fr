app.provider('notifier', [
	function () {
		var parent = this;

		var Ntoastr = {
			toastr_init_options: angular.copy(toastr.options),
			toastr_default_options: {
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": 300,
				"hideDuration": 1500,
				"timeOut": 7000,
				"extendedTimeOut": 1000,
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			},
			reset: function () {
				Ntoastr.setDefaultOptions(Ntoastr.toastr_init_options);
			},
			init: function () {
				angular.copy(Ntoastr.toastr_default_options, toastr.options);
			},
			setDefaultOptions: function (options) {
				var options = !!options ? options : Ntoastr.toastr_default_options;
				angular.copy(options, Ntoastr.toastr_default_options);
				Ntoastr.init();
			},
			getDefaultOptions: function () {
				return Ntoastr.toastr_default_options;
			},
			addDefaultOption: function (name, value) {
				Ntoastr.toastr_default_options[name] = value;
				Ntoastr.init();
			},
		};
		Ntoastr.init();



		this.$get = ['$compile', 'translations',
			function ($compile, translations) {

				Ntoastr.info = function (text, vars, $isolated_scope, options) {
					if(!!options) angular.extend(toastr.options, options);
					var $message = $('<span>'+translations.translate(text, vars)+'</span>');
					if(!!$isolated_scope) {
						// compile with $isolated_scope
						$compile($message)($isolated_scope);
					}
					toastr['info']($message);
					if(!!options) Ntoastr.init();
				};

				Ntoastr.success = function (text, vars, $isolated_scope, options) {
					if(!!options) angular.extend(toastr.options, options);
					var $message = $('<span>'+translations.translate(text, vars)+'</span>');
					if(!!$isolated_scope) {
						// compile with $isolated_scope
						$compile($message)($isolated_scope);
					}
					toastr['success']($message);
					if(!!options) Ntoastr.init();
				};

				Ntoastr.warning = function (text, vars, $isolated_scope, options) {
					if(!!options) angular.extend(toastr.options, options);
					var $message = $('<span>'+translations.translate(text, vars)+'</span>');
					if(!!$isolated_scope) {
						// compile with $isolated_scope
						$compile($message)($isolated_scope);
					}
					toastr['warning']($message);
					if(!!options) Ntoastr.init();
				};

				Ntoastr.error = function (text, vars, $isolated_scope, options) {
					if(!!options) angular.extend(toastr.options, options);
					var $message = $('<span>'+translations.translate(text, vars)+'</span>');
					if(!!$isolated_scope) {
						// compile with $isolated_scope
						$compile($message)($isolated_scope);
					}
					toastr['error']($message);
					if(!!options) Ntoastr.init();
				};

				var service = {
					info: function(text, vars, $isolated_scope, options) { return info(text, vars, $isolated_scope, options); },
					success: function(text, vars, $isolated_scope, options) { return success(text, vars, $isolated_scope, options); },
					warning: function(text, vars, $isolated_scope, options) { return warning(text, vars, $isolated_scope, options); },
					error: function(text, vars, $isolated_scope, options) { return error(text, vars, $isolated_scope, options); },
				};
				return service;
			}
		];

	}
]);






