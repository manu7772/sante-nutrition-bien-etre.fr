app.provider('modals', [
	function () {
		var parent = this;

		var modals = [];

		this.$get = ['$compile', '$timeout', 'translations',
			function ($compile, $timeout, translations) {

				var $loader = null;

				var getNewModal = function ($template) {

				};

				var service = {
					getNewModal: function ($template) {
						return getNewModal($template);
					},
					enableFullLoader: function (timeout) {
						$loader = $('<div id="loading">Loading&#8230;</div>');
						$('body').prepend($loader);
						if(angular.isNumber(timeout)) {
							$timeout(function () {
								service.disableFullLoader(0);
							}, parseInt(timeout));
						}
					},
					disableFullLoader: function (timeout) {
						if(!!$loader) {
							timeout = angular.isNumber(timeout) ? timeout : 0;
							$timeout(function () {
								$loader.remove();
								$loader = null;
							}, parseInt(timeout));
						}
					},
				};
				return service;
			}
		];

	}
]);






