app.provider('sitedata', [
	function () {
		var parent = this;
		var sitedata = {};
		var testJson = /(((^\"?\[).+(\]\"?$))|((^\"?{).+(}\"?$)))/;

		var $sitedata = $('#site-data > div[id^="sd-"]');
		$sitedata.each(function (item) {
			var value = $(this).attr('data-info');
			sitedata[$(this).attr('id').replace(/^sd-/i,'')] = testJson.test(value) ? angular.fromJson(value) : value;
		});
		sitedata.public = sitedata.public === "1";

		this.getKeys = function() {
			return Object.keys(sitedata);
		};

		this.get = function(name) {
			return sitedata[name];
		};

		this.getEnvironment = function() {
			return sitedata.environment.toLowerCase();
		};

		this.isDev = function() {
			return parent.getEnvironment() === 'dev';
		};
		this.isProd = function() {
			return parent.getEnvironment() === 'prod';
		};
		this.isTest = function() {
			return parent.getEnvironment() === 'test';
		};
		this.isPublic = function() {
			return sitedata.public;
		};
		if(parent.isDev()) {
			console.debug('Site data:', sitedata);
			console.debug('Public:', parent.isPublic());
		}

		this.getSitedata = function() {
			return sitedata;
		};

		this.getLanguages = function() {
			return sitedata.languages;
		};

		this.getUser = function () {
			return sitedata.user || null;
		};

		this.getCacheRemover = function() {
			return parent.isDev() ? "?"+Date.now() : '';
		}

		this.$get = function () {

			var service = {
				// generic getters
				get: function(name) { return parent.get(name); },
				getKeys: function() { return parent.getKeys(); },
				getSitedata: function() { return parent.getSitedata(); },
				getCacheRemover: function() { return parent.getCacheRemover(); },
				// specific getters
				isDev: function() { return parent.isDev(); },
				isProd: function() { return parent.isProd(); },
				getEnvironment: function() { return parent.getEnvironment(); },
				getLanguages: function() { return parent.getLanguages(); },
			};
			return service;
		};

	}
]);






