// Clickable icon to edit translated text
// use :
// <i data-live-translator="URL"><i class="fa fa-circle fa-fw text-danger"></i></i>
app.directive("liveTranslator", [function () {
	return {
		restrict : "A",
		replace: false,
		scope: {
			liveTranslator: '=',
		},
		controller: function ($scope, $element) {
			// console.debug('liveTranslator:', $scope.liveTranslator);
			if('#' === $scope.liveTranslator['url']) {
				$element.removeClass('text-warning').addClass('text-danger');
			} else {
				$element.removeClass('text-warning').addClass('text-info');
				$element.on('click', function (item) {
					window.location = $scope.liveTranslator['url'];
				});
			}
		},
	};
}]);

// Hide element if AngularJs works
// use :
// <div data-angular-hide>...</div>
app.directive("angularHide", [function () {
	return {
		restrict : "A",
		replace: false,
		controller: function ($scope, $element) {
			$element.addClass('hidden').css('dispaly', 'none');
		},
	};
}]);

// Need confirm before an action
// use :
// <button class="btn btn-white btn-sm confirm"><i class="fa fa-xxxx fa-fw"></i> Cliquez ici</button>
// https://stackoverflow.com/questions/16695886/using-onbeforeunload-event-url-change-on-selecting-stay-on-this-page
app.directive("actionConfirm", ['$timeout', function ($timeout) {
	return {
		restrict : "C",
		scope: {
			confirmMessage: '=',
		},
		controller: function ($scope, $element) {
			$element.on('click', function(e) {
				e.preventDefault();
				var href = this.href;
				if(angular.isString($scope.confirmMessage)) title = $scope.confirmMessage;
					else title = $(this).attr('title') || null;
				if(undefined !== href && confirm('Confirmer', title, function(response) {
					// console.log(response);
					if(response.value || false) {
						if($element.hasClass('action-confirm-wait')) {
							Swal.fire({
								icon: null,
								title: 'Opération en cours...',
								html: '<div><p><i class="fa fa-spinner fa-spin fa-3x text-muted"></i></p><p>Attente de retour du serveur</p><p><em><i>Si cette opération est trop longue, le serveur peut renvoyer une erreur. Dans ce cas, recommencez cette opération svp.</i></em></p></div>',
							});
						}
						window.location.href = href;
					} else {
						// switch off all WOC spinners if canceled
						$('body i.fa.woclicked').each(function() {
							$(this).attr('class', $(this).data('class'));
						});
					}
				}));
			});
		}
	};
}]);

// Create wait spin icon on click
// use :
// <button class="btn btn-white btn-sm woc woc-delay-0"><i class="fa fa-xxxx fa-fw"></i> Cliquez ici</button>
app.directive("woc", ['$timeout', function ($timeout) {
	return {
		restrict : "C",
		controller: function ($scope, $element) {
			var spinner = 'fa-refresh'; // possibles : fa-circle-o-notch / fa-spinner / fa-refresh / fa-cog
			var turn = 'fa-spin'; // possibles : fa-spin / fa-pulse
			var spin_class = 'fa '+spinner+' '+turn;
			var $icon = $element.hasClass('fa') ? $element : $element.find('i.fa').last(); // supports stacked icons !! :-)
			if(!!$icon) {
				$icon.data('class', $icon.attr('class'));
				var keep_classes = ['fa-fw','fa-2x','fa-3x','fa-4x','fa-5x','fa-spin','pull-right','pull-left','m-xxs','m','m-xs','m-sm','m-md','m-lg','m-xl','m-n','m-l-none','m-l-xs','m-l-sm','m-l','m-l-md','m-l-lg','m-l-xl','m-l-n-xxs','m-l-n-xs','m-l-n-sm','m-l-n','m-l-n-md','m-l-n-lg','m-l-n-xl','m-r-none','m-r-xxs','m-r-xs','m-r-sm','m-r','m-r-md','m-r-lg','m-r-xl','m-r-n-xxs','m-r-n-xs','m-r-n-sm','m-r-n','m-r-n-md','m-r-n-lg','m-r-n-xl','space-15','space-20','space-25','space-30'];
				angular.forEach(keep_classes, function (fa) {
					if($icon.hasClass(fa)) spin_class += ' '+fa;
				});
				// woc-0000 / woc-infinite
				var woc_options = { delay: 60000 };
				var test = /^woc-/;
				var classList = $element.attr('class').split(/\s+/);
				angular.forEach(classList, function (classe) {
					if(test.test(classe)) {
						classe = classe.split('-');
						switch (classe[1]) {
							case 'delay': woc_options[classe[1]] = parseInt(classe[2]); break;
							default: woc_options[classe[1]] = classe[2]; break;
						}
					}
				});
				$element.data('woc_options', woc_options);
				$element.on('click', function (event) {
					// switch off other spinners
					$('body i.fa.woclicked').each(function() {
						$(this).attr('class', $(this).data('class'));
					});
					// switch on this one
					var woc_options = $element.data('woc_options');
					$icon.attr('class', spin_class);
					$icon.addClass('woclicked');
					if(woc_options.delay > 0) {
						$timeout(function() {
							$icon.attr('class', $icon.data('class'));
						}, woc_options.delay);
					}
				});
			}
		}
	};
}]);

// User accept cookies
// use :
// <span data-cookie-advert="URL">...</span>
app.directive("cookieAdvert", ['$http', function ($http) {
	return {
		restrict : "A",
		replace: false,
		scope: {
			cookieAdvert: '=',
		},
		controller: function ($scope, $element) {
			var $element = $element;
			var $scope = $scope;
			$element.on('click', function() {
				var promise = $http({
					url: $scope.cookieAdvert,
					method: 'POST'
				});
				promise.then(
					function success(response) {
						console.debug('Cookies:', response);
						var $container = $element.closest('.top-bar');
						if(!$container.length) $container = $element;
						$container.fadeOut(2000).remove();
						Swal.fire({text: 'Vous avez accepté les cookies. Merci !', position: 'top-end', toast: true, showConfirmButton: false, timerProgressBar: true, timer: 2000});						
					},
					function error(response) {
						console.error('ERROR Cookies:', response);
					}
				);
			});
		},
	};
}]);

// use :
// <div data-flash-message data-type="{{ type }}" data-support="{{ message.support }}" data-title="{{ message.title }}">{{ message.message|trans|raw }}</div>
app.directive('flashMessage', ['$filter', function ($filter) {
	return {
		restrict: "A",
		replace: false,
		scope: {
			flashMessage: '=',
		},
		controller: function ($scope, $element) {
			if(angular.isObject($scope.flashMessage) && $scope.flashMessage.instances.includes('Popup')) {
				var options = {};
				switch($scope.flashMessage.masterType.toLowerCase()) {
					case 'sweet':
						options.icon = $scope.flashMessage.optionType;
						options.title = $scope.flashMessage.title || $scope.flashMessage.menutitle;
						options.html = $scope.flashMessage.fulltext || $scope.flashMessage.resume;
						// options.grow = 'fullscreen';
						// options.timer = 5000;
						// options.timerProgressBar = true;
						var mixSwal = Swal.mixin(options);
						mixSwal.fire();
						break;
					case 'toastr':
						// var memOptions = toastr.options;
						// toastr.options = options;
						// toastr[type](title, html);
						// toastr.options = memOptions;
						// options.grow = 'fullscreen';
						options.toast = true;
						options.position = 'top-end';
						options.showConfirmButton = false;
						options.timerProgressBar = true;
						options.icon = $scope.flashMessage.optionType;
						options.timer = 3000;
						options.onOpen = (toast) => {
							toast.addEventListener('mouseenter', Swal.stopTimer)
							toast.addEventListener('mouseleave', Swal.resumeTimer)
						}
						// options.title = $scope.flashMessage.title || $scope.flashMessage.menutitle;
						// options.html = $filter('striphtml')($scope.flashMessage.resume || $scope.flashMessage.accroche, true);
						options.html = $scope.flashMessage.resume || $scope.flashMessage.accroche;
						var mixSwal = Swal.mixin(options);
						mixSwal.fire();
						break;
				}
			} else {
				var options = $element.attr('data-options') || {};
				if(angular.isString(options)) options = angular.fromJson(options);
				var support = $element.attr('data-support').toLowerCase();
				var title = $element.attr('data-title') || null;
				var type = $element.attr('data-type').toLowerCase();
				var html = $element.html();
				switch(support.toLowerCase()) {
					case 'sweet':
						options.icon = type;
						options.title = $element.attr('data-title');
						options.html =  html;
						options.timerProgressBar = true;
						var mixSwal = Swal.mixin(options);
						mixSwal.fire();
						break;
					case 'toastr':
						options.toast = true;
						options.icon = type;
						// options.html =  $filter('striphtml')(html, true);
						options.html =  html;
						if(undefined === options.position) options.position = 'top-end';
						if(undefined === options.timer) options.timer = 5000;
						if(undefined === options.timerProgressBar) options.timerProgressBar = true;
						if(undefined === options.showConfirmButton) options.showConfirmButton = false;
						options.onOpen = (toast) => {
							toast.addEventListener('mouseenter', Swal.stopTimer)
							toast.addEventListener('mouseleave', Swal.resumeTimer)
						}
						var mixSwal = Swal.mixin(options);
						mixSwal.fire();
						break;
				}
			}
		}
	};
}]);

// use :
// <a role="button" data-show-popup="{{ one|serialized() }}"></a>
app.directive('showPopup', ['$filter', function ($filter) {
	return {
		restrict: "A",
		replace: false,
		scope: {
			showPopup: '=',
		},
		controller: function ($scope, $element) {
			console.debug('Show popup', $scope.showPopup);
			// if($scope.showPopup.shortname == 'Popup') {
			if(Object.values($scope.showPopup.instances).indexOf('Popup') > -1) {
				$element.on('click', function(event) {
					event.preventDefault();
					var options = {};
					switch($scope.showPopup.masterType.toLowerCase()) {
						case 'sweet':
							options.icon = $scope.showPopup.optionType;
							options.title = $scope.showPopup.title || $scope.showPopup.menutitle;
							options.html = $scope.showPopup.fulltext || $scope.showPopup.resume;
							var mixSwal = Swal.mixin(options);
							mixSwal.fire();
							break;
						case 'toastr':
							options.toast = true;
							options.position = 'top-end';
							options.showConfirmButton = false;
							options.timerProgressBar = true;
							options.icon = $scope.showPopup.optionType;
							options.timer = 3000;
							options.onOpen = (toast) => {
								toast.addEventListener('mouseenter', Swal.stopTimer)
								toast.addEventListener('mouseleave', Swal.resumeTimer)
							}
							options.html = $scope.showPopup.resume || $scope.showPopup.accroche;
							var mixSwal = Swal.mixin(options);
							mixSwal.fire();
							break;
					}
				});
			}
		}
	};
}]);

// use :
app.directive('popover', ['$compile',function ($compile) {
	return {
		restrict: "A",
		replace: false,
		scope: {
			content: '@',
			trigger: '<',
			compile: '<',
		},
		controller: function ($scope, $element) {
			var options = {
				container: "body",
				html: true,
				//content: $scope.content,
				content: function() {
					return $scope.content;
				},
				placement: 'bottom',
				trigger: $scope.trigger || 'hover',
				toggle: 'popover',
			};
			$element.popover(options);
			if($scope.compile || false) {
				$element.on('shown.bs.popover', function(pop) {
					//console.debug('Compile popover', $element.data('bs.popover').tip().find('.popover-content'));
					$compile($element.data('bs.popover').tip().find('.popover-content'))($scope);
				});
			}
		}
	};
}]);

// use :
app.directive('footable', [function () {
	return {
		restrict: "C",
		replace: false,
		controller: function ($scope, $element) {
			// alert("New footable");
			$element.footable();
		}
	};
}]);

app.directive("sortableList", [function() {
	return {
		restrict: "A",
		replace: false,
		scope: {
			sortableList: '@',
		},
		controller: function ($scope, $element) {
			// console.debug('Sortable', $element.attr('id'));
			if(angular.isString($scope.sortableList)) {
				$element.sortable({
					connectWith: $scope.sortableList,
					update: function( event, ui ) {
						var oneentity = $( "#oneentity" ).sortable( "toArray" );
						var entitylist = $( "#entitylist" ).sortable( "toArray" );
						$('.output'+$scope.sortableList).html("Entity: "+window.JSON.stringify(oneentity)+"<br/>"+"Entities: "+window.JSON.stringify(entitylist));
					}
				}).disableSelection();
			} else {
				$element.sortable().disableSelection();
			}
		},
	};
}]);

// use : <div class="modal-body" data-self-content="{{ Modale.bddid }}" data-self-content-anchor="#form-register" data-self-content-url="{{ url('apifile-twig-getcontent') }}"></div>
app.directive('selfContent', ['$http','$compile','$timeout','$interval',
	function ($http, $compile, $timeout, $interval) {
		return {
			restrict: "A",
			replace: false,
			scope: {
				selfContent: "=",
				selfContentAnchor: "=",
				selfContentUrl: "=",
				selfContentWaitstate: "=",
				selfContentTimeout: "=",
				selfContentInterval: "=",
			},
			controller: function($scope, $element) {
				var nb_lauched = 0;
				// statusOn
				var status_on = true;
				var timeoutStatus = undefined;
				var refreshStatusOn = function() {
					console.debug('*** Refresh status On...');
					status_on = true;
					if(!angular.isUndefined(timeoutStatus)) $timeout.cancel(timeoutStatus);
					timeoutStatus = $timeout(
						function() { status_on = false; },
						600000 // 10 min
					);
				}
				// Interval data
				var intervaldata = false;
				var lq_interval = undefined;
				if(angular.isArray($scope.selfContentInterval)) {
					intervaldata = $scope.selfContentInterval;
				} else if(angular.isString($scope.selfContentInterval)) {
					intervaldata = $scope.selfContentInterval.split(',');
				} else if(angular.isNumber($scope.selfContentInterval)) {
					intervaldata = [$scope.selfContentInterval, 12, true];
				}
				if(angular.isArray(intervaldata)) {
					if(!angular.isNumber(intervaldata[0])) intervaldata[0] = Math.ceil(parseInt(intervaldata[0]) || 5000);
					if(intervaldata[0] < 1000) intervaldata[0] = 1000;
					if(!angular.isNumber(intervaldata[1])) intervaldata[1] = Math.ceil(parseInt(intervaldata[1] || 12));
					if(!angular.isNumber(intervaldata[2])) intervaldata[2] = new Boolean(intervaldata[2] || true);
				}
				// console.debug('Timeout:', $scope.selfContentTimeout);
				// console.debug('Interval:', intervaldata);
				// launch Query
				var launchQuery = function() {
					if(!status_on) return;
					nb_lauched++;
					if(nb_lauched < 2) {
						var $spinner = $('<div style="width: 100%; height: 200px; text-align: center;"><i class="fa fa-spinner fa-spin fa-3x fa-fw" style="margin-top: 80px; color: #aaa;"></i></div>');
						$element.html($spinner);
					}
					// alert('Lauched self content on '+angular.toJson($scope.selfContentUrl)+'!');
					// console.debug('Self content data '+nb_lauched+':', {interval: intervaldata, bddid: $scope.selfContent, url: $scope.selfContentUrl});
					// var formdata = new FormData();
					// formdata.append('bddid', $scope.selfContent);
					var promise = $http({
						url: $scope.selfContentUrl,
						method: 'GET',
						// data: formdata,
						// headers: { 'Content-Type': undefined }, //this is important
						// transformRequest: angular.identity, //also important
					});
					promise.then(
						function success(response) {
							if(response.status !== 200) {
								console.error(response);
								var $error = '<div style="width: 100%; height: 200px; text-align: center;"><h3 style="text-align: center; color: darkred;">Une erreur est survenue, le formulaire n\'a pu être chargé.<br><small>Rechargez la page et recommencez, svp.</small></h3></div>';
								$element.html($error);
							} else {
								console.debug('Contenu HTML chargé:', response);
								var $html = $(response.data.response.content);
								if(angular.isString($scope.selfContentAnchor)) {
									$html = $('<div></div>').append($html);
									$html = $html.find($scope.selfContentAnchor);
								}
								$element.html($html);
								$compile($html)($scope);
								$html.mouseenter(function() { refreshStatusOn(); });
							}
						},
						function error(response) {
							console.error(response);
							var $error = '<div style="width: 100%; height: 200px; text-align: center;"><h3 style="text-align: center; color: darkred;">Une erreur est survenue, le formulaire n\'a pu être chargé.<br><small>Rechargez la page et recommencez, svp.</small></h3></div>';
							$element.html($error);
						}
					);
				};
				$scope.refresh = function() {
					console.debug('*** Refresh content...');
					// console.debug('Timeout:', $scope.selfContentTimeout);
					// console.debug('Interval:', intervaldata);
					$scope.stop();
					launchQuery_interval();
					// Status On/Off
					refreshStatusOn();
				}
				$scope.stop = function() {
					nb_lauched = 0;
					if(angular.isDefined(lq_interval)) $interval.cancel(lq_interval);
					lq_interval = undefined;
				}
				var launchQuery_interval = function() {
					$scope.stop();
					if(intervaldata !== false) {
						launchQuery();
						lq_interval = $interval(function() { launchQuery(); }, intervaldata[0], intervaldata[1], intervaldata[2]);
					} else {
						launchQuery();
					}
				};
				$timeout(function() {
					if(angular.isString($scope.selfContentWaitstate)) {
						var $depend = $($scope.selfContentWaitstate);
						$timeout(function() {
							if($depend.hasClass('modal')) {
								$depend.on('shown.bs.modal', function() {
									$scope.refresh();
								});
								$depend.on('hidden.bs.modal', function() {
									$scope.stop();
								});
							} else {
								$depend.on('shown', function() {
									$scope.refresh();
								});
								$depend.on('hidden', function() {
									$scope.stop();
								});
							}
						});
					} else {
						$scope.refresh();
					}
				}, $scope.selfContentTimeout || 0);
			},
		};
	}
]);

// use : <input data-tomcat="">
app.directive('tomcat', ['$compile','sitedata',
	function ($compile,sitedata) {
	// https://bootstrap-datepicker.readthedocs.io/en/stable/
	// https://bootstrap-datepicker.readthedocs.io/en/stable/options.html#forceparse
	return {
		restrict: "A",
		replace: false,
		scope: {
			tomcat: '=',
		},
		controller: function ($scope, $element) {
			if(sitedata.isDev()) console.debug('Tomcat', $scope.tomcat);
			if($scope.tomcat === true || $scope.tomcat === 1) {
				$element.val(angular.toJson({verified: false}));
				var $form = $element.closest('form');
				// $form.css('border', '1px solid green');
				var $submits = $form.find('button[type^="submit"]')
				var $submit = $submits.first();
				// $submit.addClass('btn-danger');
				var $captcha = $('<div class="well well-sm text-center" style="margin-bottom: 16px;"><h4 class="text-center"><strong>Renseignez le test avant de valider, svp.</strong><br><small><i>Cliquez sur l\'image pour changer le code à copier si vous n\'arrivez pas à le lire.</i></small></h4><div><canvas id="captcha-canvas"></canvas></div><div><input id="captcha-input" class="text-center font-bold" style="font-size: 1.5em;" autocomplete="off"></div><div id="captcha-result" class="text-danger text-center"></div></div>');
				$submit.before($captcha);
				$compile($captcha)($scope);
				var $canvas = $captcha.find('#captcha-canvas');
				var $input = $captcha.find('#captcha-input');
				var $result = $captcha.find('#captcha-result');
				var captcha = new Captcha($canvas, {length: 7, width: 400, height: 100, font: 'bold 23px Arial'});
				$submits.on('click', function(event) {
					event.preventDefault();
					if(!$(this).hasClass('disabled')) {
						var test = captcha.valid($input.val());
						$element.data('test', angular.toJson({verified: test}));
						$element.val(angular.toJson({verified: test}));
						// alert('Captcha is valid: '+$input.val()+' > '+angular.toJson(test));
						if(test) {
							$result.show().text('CODE VALIDE !');
							if($element.data('test') !== $element.val()) {
								alert('Alerte sécurité');
								$form.remove();
							} else {
								$element.val('#JSON#___'+$element.val());
								$form.submit();
							}
						} else {
							$result.show().text('Le code copié est invalide. Veuillez recommencer, svp.');
						}
					}
				});
			} else {
				// alert('No captcha!');
			}
		}
	};
}]);


// use : <form data-labo-form="<data>"></form>
app.directive('laboForm', ['$timeout','$compile','$http','sitedata',
	function ($timeout, $compile, $http, sitedata) {
	return {
		restrict: "A",
		replace: false,
		scope: {
			laboForm: '=',
		},
		// controller: function ($scope, $form) {
		link: function ($scope, $form) {
			console.debug('laboForm', $scope.laboForm);
			var instances = $scope.laboForm.data.instances;
			var manage_inputname = Object.keys(instances).find(key => ['Binary','Pdf','Image','Video','Audio','Tempfile'].includes(instances[key]));
			$scope.progress = '0%';
			// if(!manage_inputname) return;
			// console.debug(manage_inputname ? '>>> Manage inputname' : '>>> DO NOT Manage inputname');
			var $form = $form;

			// SUBMIT
			var $submits = $form.find(":submit");
			var disableSubmits = function() { $submits.attr('disabled', true).addClass('disabled'); }
			var enableSubmits = function() { $submits.attr('disabled', false).removeClass('disabled'); }
			$submits.on('click', function(event) {
				if($(this).hasClass('disabled')) event.preventDefault();
			});

			// DEv informations
			/*if(sitedata.isDev()) {
				var $dev_info_serialize = $('<div class="m-b-xs, m-t-xs" style="border: 1px solid red;">DEV informations (serialized)</div>');
				//var $dev_info_json = $('<div class="m-b-xs, m-t-xs" style="border: 1px solid red;">DEV informations (json)</div>');
				$form.append($dev_info_serialize);
				//$form.append($dev_info_json);
				$form.find('input, select, checkbox').on('change', function(e) {
					//var form_data = new FormData($form.get(0));
					var form_data = $form.serialize();
					//var form_data = angular.toJson($form);
					console.debug('FORM DATA:', form_data);
					$dev_info_serialize.text(form_data);
					//$dev_info_serialize.text(JSON.stringify($form));
					//$dev_info_json.text(angular.toJson($form));
				});
			}*/

			// INPUT FILE
			$form.find('[data-form-binary]').each(function(item) {
				var $block_file = $(this);
				var post_URL = $block_file.attr('data-post-url');
				if(!angular.isString(post_URL) || post_URL.length < 1) {
					console.error('POST URL '+item+' :', 'NOT FOUND');
					return;
				}
				console.debug('POST URL '+item+' :', post_URL);
				var $input = $block_file.find('input:file').first();
				var name_prefix = $input.attr('name').split('[')[0];
				// var $input_uploadinfo = $form.find('input[name="'+name_prefix+'[uploadinfo]'+'"]').first();
				// if($input_uploadinfo.length) {
					var current_file = null;
					var nouveau_fichier = 'Nouveau fichier';
					// Duplicate input:file as input:text
					var $inputText = $('<input type="text">');
					$inputText.attr("id", $input.attr("id"));
					$input.removeAttr("id");
					$inputText.attr("name", $input.attr("name"));
					$input.removeAttr("name");
					if($input.attr("required") !== undefined) $inputText.attr("required", $input.attr("required"));
					$input.removeAttr('required');
					$input.after($inputText);
					$input.hide();
					$inputText.hide();
					// END: Duplicate input:file as input:text
					// set name in field inpuname
					var $input_inputname;
					var origin_inputname;
					if(manage_inputname) {
						$input_inputname = $form.find('input[name="'+name_prefix+'[inputname]'+'"]').first();
						origin_inputname = $input_inputname.val();
						if(!angular.isString(origin_inputname) || origin_inputname.length <= 0) origin_inputname = nouveau_fichier;
					}

					// if($input_uploadinfo.length) $input_uploadinfo.removeAttr('disabled');

					var origin_src = '';
					var src_attr = '';
					var $model_img = $block_file.find('img').first();
					if($model_img.length) {
						origin_src = $model_img.attr('src');
						console.debug('Origin image src:', origin_src);
						src_attr = origin_src.length > 1 ? ' src="'+origin_src+'"' : '';
						// $model_img.remove();
					}
					var $img = $('<img'+src_attr+' class="img-responsive img-thumbnail img-lg" style="cursor: pointer;">');
					var $block_img = $('<button class="btn btn-lg btn-white text-center"></button>');
					$block_img.append($img);
					var $upload_progress = $('<div class="upload-progress text-center text-muted" data-ng-bind="progress">Chargement...</div>')
					$block_img.append($upload_progress);
					$compile($upload_progress)($scope);
					$upload_progress.hide();
					// $block_add = $('<button class="btn btn-primary text-center m-r-sm"><i class="fa fa-plus fa-fw fa-2x text-white"></i></button>');
					$block_img.on('click', function(event) {
						event.preventDefault();
						$input.trigger("click");
					});

					var getOriginImage = function() {
						return angular.isString(origin_src) && origin_src.length > 1 ? origin_src : false;
					}

					var updatePicture = function(action) {
						if(action === 'loading') {
							disableSubmits();
							$block_img.find('*').hide();
							$block_img.append($('<i class="fa fa-spinner fa-spin fa-3x text-muted m-t-xs m-b-xs"></i>'));
						} else {
							if(action === 'reset') { current_file = null; }
							if(angular.isObject(current_file)) {
								if(angular.isObject(action)) {
									if(!angular.isUndefined(action.file_urls.thumbnail_128x128)) {
										$img.attr('src', '/'+action.file_urls.thumbnail_128x128);
									} else {
										$img.removeAttr('src');
									}
									if(!angular.isUndefined($input_inputname)) $input_inputname.val(action.original_file_name);
									$inputText.val(angular.toJson({type: 'UploadedFile', file: action.file_url, filename: action.original_file_name}));
									console.debug('New file: '+$inputText.val());
								} else {
									$inputText.val(null);
								}
								if(undefined === $img.attr('src') || $img.attr('src').length <= 0) {
									$block_img.find('*').hide();
									$block_img.append($('<i class="fa fa-check fa-3x text-info"></i>'));									
								} else {
									$block_img.find('*').show();
									$upload_progress.hide();
									$block_img.find('>i.fa').remove();
								}
							} else {
								if(getOriginImage()) {
									$img.attr('src', getOriginImage());
									$block_img.find('*').show();
									$upload_progress.hide();
									$block_img.find('>i.fa').remove();
									if(!angular.isUndefined($input_inputname)) $input_inputname.val(origin_inputname);
								} else {
									$block_img.find('*').hide();
									$block_img.append($('<i class="fa fa-upload fa-3x text-muted"></i>'));
									if(!angular.isUndefined($input_inputname)) $input_inputname.val(nouveau_fichier);
								}
								$inputText.val(null);
							}
							enableSubmits();
						}
					};
					updatePicture();

					$input.change(function(item) {
						var file = $input.get(0).files[0];
						$input.val('');
						console.debug('New image', file);

						// https://developer.mozilla.org/fr/docs/Web/API/FileReader
						// https://developer.mozilla.org/fr/docs/Web/API/FileReader/readAsText
						// https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/loadend_event
						if(!angular.isUndefined(file)) {
							updatePicture('loading');
							reader = new FileReader();
							reader.onloadend = function(result) {
								// Send to server
								var sizeKo = Math.round(file.size / 1024);
								var $uploadprogress = $block_img.find('.upload-progress');
								$uploadprogress.attr('title', sizeKo+'Ko');
								var formdata = new FormData();
								formdata.append('file', result.target.result);
								formdata.append('size', file.size);
								formdata.append('type', file.type);
								formdata.append('name', file.name);

								$scope.progress = '0%';
								$upload_progress.show();
								// var unbindWatcher = $scope.$watch('progress', function(oldvalue, newvalue) {
								// 	if(newvalue < 100) {
								// 		$uploadprogress.html(newvalue+'%');
								// 	} else {
								// 		$uploadprogress.html('Traitement...');
								// 	}
								// });
								var promise = $http({
									url: post_URL,
									method: 'POST',
									data: formdata,
									headers: { 'Content-Type': undefined }, //this is important
									transformRequest: angular.identity, //also important
									eventHandlers: {
										readystatechange: function(event) {
											if(event.currentTarget.readyState === 4) {
												// console.log("Server has finished extra work!");
												// $uploadprogress.html('Terminé');
												$scope.progress = 'Terminé';
												// unbindWatcher(); // stop watchin progress
											}
										}
									},
									uploadEventHandlers: {
										progress: function(e) {
											if (e.lengthComputable) {
												// console.log('loaded: '+e.loaded+' / total: '+e.total);
												$scope.progress = Math.round(e.loaded * 100 / e.total)+'%';
												// $scope.progress = e.loaded;
												// if (e.loaded === e.total) {
												// 	console.log("File upload finished!");
												// 	console.log("Server will perform extra work now...");
												// } else {
												// 	console.log("progress: " + $scope.progress + "%/"+sizeKo+"Ko");
												// }
											}
										},
									}
								});
								promise.then(
									function success(response) {
										if(response.status !== 200) {
											console.error(response);
											updatePicture();
											alert('Erreur, ce format de fichier n\'est pas supporté.');
										} else {
											console.debug('Fichier enregistré:', response);
											current_file = file;
											updatePicture(response.data);
										}
									},
									function error(response) {
										console.error(response);
										updatePicture();
										alert('Erreur, ce format de fichier n\'est pas supporté.');
									}
								);
							};
							// reader.readAsBinaryString(file);
							// reader.readAsArrayBuffer(file);
							reader.readAsDataURL(file);
							// reader.readAsText(file);
						} else {
							updatePicture();
							// console.debug('No file selected...');
						}
					});
					// Prepare interface
					$block_file.hide(0, function() {
						$block_file.find('i, img').remove();
						// $block_file.append($block_add);
						$block_file.append($block_img);
						if(getOriginImage()) {
							// button reset
							var $reset_btn = $('<a class="btn btn-sm btn-white m-xs" title="Rétablir le fichier original."><i class="fa fa-refresh fa-fw"></i></a>');
							$reset_btn.on('click', function() {
								updatePicture('reset');
							});
							$block_file.append($reset_btn);
						}
						$timeout(function() { enableSubmits(); }, 1000);
						// END: show
						$block_file.delay(200).show();
					});
					// START FORM
					disableSubmits();
				// }
			});
		}
	};
}]);



