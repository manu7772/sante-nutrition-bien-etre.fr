
// /**
//  * Use: <a href="url" data-create-entity="Shortname">Create new entity</a>
//  */
// app.directive("fieldShow", ['$templateRequest', '$compile', 'sitedata', 'translations', 'notifyer',
// 	function ($templateRequest, $compile, sitedata, translations, notifyer) {
// 		return {
// 			restrict: "AC",
// 			scope: {
// 				fieldShow: '@',
// 				value: '=',
// 				label: '=',
// 				parameters: '=',
// 			},
// 			controller:function($scope, $element){
// 				$scope.fieldShow = $scope.parameters.type || 'field-text';
// 				$scope.templateUrl = sitedata.getPath('websiteadmin', 'angular/templates/directive/fields/'+$scope.fieldShow+'.html');
// 				// notifyer.console.debug("Field show "+$scope.fieldShow+":", $scope);
// 			},
// 			link: function ($scope, $element, attrs) {
// 				$templateRequest($scope.templateUrl).then(
// 					function success(template) {
// 						$element.replaceWith($compile(template)($scope));
// 						// notifyer.console.debug("Field show "+$scope.fieldShow+":", $scope);
// 					},
// 					function error(response) {
// 						toastr["error"](translations.translate('errors.'+response.statusText));
// 					}
// 				);
// 			},
// 		};
// 	}
// ]);

/**
 * Select2
 */
// app.directive("select", ['$timeout',
// 	function ($timeout) {
// 		return {
// 			restrict: "E",
// 			link: function($scope, $element){
// 				$timeout(function() { $element.select2(); });
// 			},
// 		};
// 	}
// ]);

/**
 * Show data about current entity (watched in entities service)
 * Use: <div data-current-entity-show></div>
 */
app.directive("currentEntityShow", ['$interval','entities',
	function ($interval,entities) {
		return {
			restrict: "A",
			replace: true,
			templateUrl: entities.getBaseUrl('bundles/websiteadmin/angular/templates/bases/show-entity-01.html', false),
			controller:function($scope, $element){
				// $interval(function() { $scope.current = entities.current; },200);
				$scope.$watch(function() { return entities.current; }, function(newValue, oldValue) {
					if (newValue !== oldValue) {
						$scope.current = angular.copy(newValue);
						// console.debug('Current entity:', $scope.current)
					}
				});
			},
		};
	}
]);

/**
 * Get (and optionally resolve) doublons of Item
 * Use: <div data-get-doublons="{{ url }}" data-resolve-doublons="{{ url }}" ></div>
 */
app.directive("getDoublons", ['$interval','$http','$templateRequest','$compile','sitedata','entities',
	function ($interval,$http,$templateRequest,$compile,sitedata,entities) {
		return {
			restrict: "A",
			replace: true,
			scope: {
				getDoublons: "=",
				resolveDoublons: "=",
			},
			template: '<button class="btn btn-xs pull-right btn-danger" title="Dossiers différents! Cliquer pour réparer!"><i class="fa fa-exclamation-triangle fa-fw"></i></button>',
			controller: function($scope, $element) {
				$scope.loading = false;
				$scope.item_shortname = 'Item';
				$element.on('click', function(item) {
					$scope.loading = true;
					$scope.error = false;
					var error_options = function(response, message) {
						return {
							icon: 'error',
							title: response.statusText,
							html: '<div>'+(message || 'Une erreur est survenue!')+'</div>',
							showConfirmButton: false,
							showCloseButton: true,
							showCancelButton: true,
							cancelButtonText: '<i class="fa fa-times fa-fw"></i> fermer',
						};
					};
					option_resolve = $scope.resolveDoublons !== undefined;
					$scope.resolveButton = option_resolve ? '<i class="fa fa-refresh fa-fw"></i> résoudre' : false;
					console.debug('Get doublons by url:', $scope.getDoublons);
					$scope.id = $scope.getDoublons.split('/');
					$scope.id = parseInt($scope.id[$scope.id.length - 1]);
					console.debug('Resolve doublons by url ('+angular.toJson(option_resolve)+'):', $scope.resolveDoublons);
					$scope.html_content_wait = '<i class="fa fa-refresh fa-spin fa-3x fa-fw" data-ng-hide="!loading"></i>';
					var templateName = 'show-item-doublons-01.html';
					templateUrl = entities.getBaseUrl('bundles/websiteadmin/angular/templates/bases/'+templateName+sitedata.getCacheRemover(), false);
					$templateRequest(templateUrl).then(
						function success(template) {
							Swal.fire({
								icon: null,
								title: '<span data-ng-bind="item_shortname">Item</span>&nbsp;doublons',
								html: template,
								width: '64rem',
								allowOutsideClick: true,
								allowEscapeKey: true,
								showCloseButton: true,
								showConfirmButton: true,
								confirmButtonText: $scope.resolveButton,
								showCancelButton: true,
								cancelButtonText: '<i class="fa fa-times fa-fw"></i> fermer',
								onBeforeOpen: function(item) {
									$compile(Swal.getContent())($scope);
									$compile(Swal.getTitle())($scope);
									$scope.loading = true;
									$http.post($scope.getDoublons).then(
										function success(response) {
											if(response.status === 200) {
												console.debug('Item doublons:', response);
												$scope.data = response.data;
												$scope.loading = false;
											} else {
												$scope.loading = false;
												$scope.error = true;
												$scope.message = response.data.message;
												Swal.update(error_options(response, response.data.message));
											}
										},
										function error(response) {
											$scope.loading = false;
											$scope.error = true;
											$scope.message = response.data.message;
											Swal.update(error_options(response, response.data.message));
										}
									);
								},
							}).then(function (result) {
								console.debug('Swal result', result);
								if(result.value) {
									$scope.loading = true;
									Swal.fire({
										icon: null,
										title: 'Résolution doublons',
										html: $scope.html_content_wait,
										onBeforeOpen: function(item) {
											$compile(Swal.getContent())($scope);
											$scope.loading = true;
											$http.post($scope.resolveDoublons, {id: $scope.id}).then(
												function success(response) {
													console.debug('Item check result:', response);
													$scope.loading = false;
													if(response.status === 200) {
														if(response.data.id !== undefined) {
															console.debug('*** Return resolve:', response.data);
															$element.removeClass('btn-danger').addClass('btn-info');
															$element.find('>i.fa').removeClass('fa-exclamation-triangle').addClass('fa-check');
															Swal.update({
																icon: 'success',
																title: 'Check '+response.data.shortname+' "'+response.data.name+'"',
																html: '<div>L\'opération a été effectuée avec succès.</div>',
															});
														} else {
															Swal.update({
																icon: 'warning',
																title: 'Check '+response.data.shortname+' "'+response.data.name+'"',
																html: '<div>'+response.data.message+'</div>',
															});
														}
													} else {
														Swal.update(error_options(response, response.data.message));
													}
												},
												function error(response) {
													console.debug('Item check result:', response);
													Swal.update(error_options(response, response.data.message));
												}
											);
										},
									});
								} else {
									// nothing...
								}
							});
						},
						function error(response) {
							Swal.fire({
								icon: 'error',
								title: 'Erreur fonctionnalité',
								html: '<div>'+response.data.message+'</div>',
								timer: 3000,
								timerProgressBar: true,
							});
						}
					);
				});	
			},
		};
	}
]);

/**
 * Show informations about Item
 * Use: <div data-show-info="{{ url }}"></div>
 */
app.directive("showInfo", ['$interval','$http','$templateRequest','$compile','sitedata','entities',
	function ($interval,$http,$templateRequest,$compile,sitedata,entities) {
		return {
			restrict: "A",
			replace: false,
			scope: {
				showInfo: "=",
				checkItemUrl: "=",
			},
			controller:function($scope, $element) {
				$scope.loading = false;
				$element.on('click', function(item) {
					// console.debug('showInfo', $scope.showInfo);
					$scope.loading = true;
					$scope.item_shortname = 'Item';
					// alert('Informations whith url: '+$scope.showInfo);
					var error_options = function(response, message) {
						return {
							icon: 'error',
							title: message || 'Une erreur est survenue!',
							html: '<div>'+response.statusText+'</div>',
							showConfirmButton: false,
							showCloseButton: true,
							showCancelButton: true,
							cancelButtonText: '<i class="fa fa-times fa-fw"></i> fermer',
						};
					};
					var compileResponseData = function(data) {
						var icon_valid = '<i class="fa fa-circle fa-fw text-info"></i>';
						var icon_invalid = '<i class="fa fa-circle fa-fw text-danger"></i>';
						data.valid = data.valid ? icon_valid : icon_invalid;
						data.validName = data.validName ? icon_valid : icon_invalid;
						data.validInputname = data.validInputname ? icon_valid : icon_invalid;
						data.validRootDirs = data.validRootDirs ? icon_valid : icon_invalid;
						data.validRootparent = data.validRootparent ? icon_valid : icon_invalid;
						data.validOwnerdirs = data.validOwnerdirs ? icon_valid : icon_invalid;
						data.validOwnerdir = data.validOwnerdir ? icon_valid : icon_invalid;
						data.validOwner = data.validOwner ? icon_valid : icon_invalid;
						data.validOwners = data.validOwners ? icon_valid : icon_invalid;
						data.validParent = data.validParent ? icon_valid : icon_invalid;
						data.validOwnerAndParent = data.validOwnerAndParent ? icon_valid : icon_invalid;
						data.validRoot = data.validRoot ? icon_valid : icon_invalid;
						data.validOrphan = data.validOrphan ? icon_valid : icon_invalid;
						data.validPathname = data.validPathname ? icon_valid : icon_invalid;
						data.validPathslug = data.validPathslug ? icon_valid : icon_invalid;
						return data;
					};

					$scope.html_content_wait = '<i class="fa fa-refresh fa-spin fa-fw" data-ng-hide="!loading"></i>';
					// $scope.html_content_info = '<div class="table-responsive" data-ng-hide="loading"><table class="table table-hover"><thead><tr><th>Test</th><th>Résultat</th></tr></thead><tbody><tr><td class="text-left"><strong>General Valid</strong></td><td class="text-left" data-ng-bind-html="data.valid"></td></tr><tr><td class="text-left"><i>Valid Name</i></td><td class="text-left" data-ng-bind-html="data.validName"></td></tr><tr><td class="text-left"><i>Valid Inputname</i></td><td class="text-left" data-ng-bind-html="data.validInputname"></td></tr><tr><td class="text-left"><i>Valid RootDirs</i></td><td class="text-left" data-ng-bind-html="data.validRootDirs"></td></tr><tr><td class="text-left"><i>Valid Rootparent</i></td><td class="text-left" data-ng-bind-html="data.validRootparent"></td></tr><tr><td class="text-left"><i>Valid Ownerdirs</i></td><td class="text-left" data-ng-bind-html="data.validOwnerdirs"></td></tr><tr><td class="text-left"><i>Valid Ownerdir</i></td><td class="text-left" data-ng-bind-html="data.validOwnerdir"></td></tr><tr><td class="text-left"><i>Valid Owner</i></td><td class="text-left" data-ng-bind-html="data.validOwner"></td></tr><tr><td class="text-left"><i>Valid Owners</i></td><td class="text-left" data-ng-bind-html="data.validOwners"></td></tr><tr><td class="text-left"><i>Valid Parent</i></td><td class="text-left" data-ng-bind-html="data.validParent"></td></tr><tr><td class="text-left"><i>Valid OwnerAndParent</i></td><td class="text-left" data-ng-bind-html="data.validOwnerAndParent"></td></tr><tr><td class="text-left"><i>Valid Root</i></td><td class="text-left" data-ng-bind-html="data.validRoot"></td></tr><tr><td class="text-left"><i>Valid Orphan</i></td><td class="text-left" data-ng-bind-html="data.validOrphan"></td></tr><tr><td class="text-left"><i>Valid Pathname</i></td><td class="text-left" data-ng-bind-html="data.validPathname"></td></tr><tr><td class="text-left"><i>Valid Pathslug</i></td><td class="text-left" data-ng-bind-html="data.validPathslug"></td></tr></tbody></table></div>';
					// var tmpl = '<div>'+$scope.html_content_wait+$scope.html_content_info+'</div>';

					var templateName = 'show-item-info-01.html';
					templateUrl = entities.getBaseUrl('bundles/websiteadmin/angular/templates/bases/'+templateName+sitedata.getCacheRemover(), false);
					$templateRequest(templateUrl).then(
						function success(template) {
							$scope.loading = true;
							Swal.fire({
								icon: null,
								title: '<span data-ng-bind="item_shortname">Item</span>&nbsp;informations',
								html: template,
								width: '64rem',
								allowOutsideClick: true,
								allowEscapeKey: true,
								showCloseButton: true,
								showConfirmButton: true,
								confirmButtonText: '<i class="fa fa-refresh fa-fw"></i> vérifier',
								showCancelButton: true,
								cancelButtonText: '<i class="fa fa-times fa-fw"></i> fermer',
								onBeforeOpen: function(item) {
									$compile(Swal.getContent())($scope);
									$compile(Swal.getTitle())($scope);
									$http.post($scope.showInfo).then(
										function success(response) {
											$scope.loading = false;
											if(response.status === 200) {
												console.debug('Item informations:', response);
												$scope.item_shortname = response.data.shortname;
												$scope.data = compileResponseData(response.data);
											} else {
												Swal.update(error_options(response, response.data.message));
											}
										},
										function error(response) {
											$scope.loading = false;
											Swal.update(error_options(response, response.data.message));
										}
									);
								},
							}).then(function(result) {
								console.debug('Swal result', result);
								if(result.value) {
									$scope.loading = true;
									Swal.fire({
										icon: null,
										title: 'Verification',
										html: $scope.html_content_wait,
										onBeforeOpen: function(item) {
											$compile(Swal.getContent())($scope);
											$http.post($scope.checkItemUrl).then(
												function success(response) {
													$scope.loading = false;
													console.debug('Item check result:', response);
													if(response.status === 200) {
														if(response.data.valid) {
															Swal.update({
																icon: 'success',
																title: 'Check '+response.data.shortname+' "'+response.data.name+'"',
																html: '<div>L\'opération a été effectuée avec succès.</div>',
															});
														} else {
															Swal.update({
																icon: 'warning',
																title: 'Check '+response.data.shortname+' "'+response.data.name+'"',
																html: '<div>'+response.data.message+'</div>',
															});
														}
													} else {
														Swal.update(error_options(response, response.data.message));
													}
												},
												function error(response) {
													$scope.loading = false;
													console.debug('Item check result:', response);
													Swal.update(error_options(response, response.data.message));
												}
											);
										},
									});
								} else {
									// nothing...
								}
							});
						},
						function error(response) {
							Swal.fire({
								icon: 'error',
								title: 'Erreur fonctionnalité',
								html: '<div>'+response.data.message+'</div>',
								timer: 3000,
								timerProgressBar: true,
							});
						}
					);
				});
			},
		};
	}
]);

app.directive("nestableDirectorys", [
	function () {
		return {
			restrict: "A",
			replace: false,
			controller: function($scope, $element) {
				var id = $element.attr('id');

				var updateOutput = function (e) {
					var list = e.length ? e : $(e.target);
					var output = list.data('output');
					if (window.JSON) {
						output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
					} else {
						output.val('JSON browser support required for this demo.');
					}
				};

				$element.nestable({ group: 1 }).on('change', updateOutput);

				updateOutput($element.data('output', $('#'+id+'-output')));

				$('#'+id+'-menu').on('click', function (e) {
					var target = $(e.target);
					var action = target.data('action');
					if (action === 'expand-all') {
						$element.nestable('expandAll');
					}
					if (action === 'collapse-all') {
						$element.nestable('collapseAll');
					}
				});

			},
		};
	}
]);



/**
 * Print media
 * Use: <div data-print-media="object"></div>
 */
app.directive("printMedia", ['$templateRequest','$compile','$timeout','$interval','$filter','sitedata','entities',
	function ($templateRequest,$compile,$timeout,$interval,$filter,sitedata,entities) {
		return {
			restrict: "A",
			replace: true,
			template: '<div class="text-center" style="margin: 40px auto 70px; width: 100%;"><i data-ng-if="loading" class="fa fa-refresh fa-spin fa-fw fa-5x text-muted"></i></div>',
			scope: {
				printMedia: "<",
				printShortname: "<",
				printWidth: "<",
				printType: "<",
				printPlay: "<",
			},
			controller:function($scope, $element){
				$scope.printWidth = parseInt($scope.printWidth || 800);
				$scope.printShortname = $scope.printShortname || 'Item'; // Item by default
				$scope.printType = $scope.printType || "optimized";
				$scope.printPlay = !!$scope.printPlay ? 1 : 0;
				// console.debug('Play: ', $scope.printPlay);
				// if(isFinite($scope.printPlay)) $scope.printPlay = $scope.printPlay > 0 ? true : false;
				// $scope.printPlay = typeof $scope.printPlay === 'boolean' ? $scope.printPlay : false;
				$scope.playable = false;
				$scope.message = null;
				$scope.error = false;
				$scope.mediaUrl = null;
				var templateUrl = null;
				var loadGif = false;
				var SGif = null;
				$scope.status = {
					playing: false,
					length: 0,
					loading: true,
					current: 0,
					multiple: false,
				};

				var mediaReady = function() {
					$scope.status.loading = true;
					// $scope.mediaUrl = $filter('fileurl')($scope.printMedia, $scope.printWidth, $scope.printType);
					switch ($scope.printMedia.mediatype) {
						case 'image':
							if(/gif/i.test($scope.printMedia.file_extension)) {
								$scope.printType = 'realsize';
								$scope.playable = true;
								templateName = 'print-media-image-animated.html';
								loadGif = true;
							} else {
								templateName = 'print-media-image-flatten.html';
							}
							$scope.mediaUrl = $filter('fileurl')($scope.printMedia, $scope.printWidth, $scope.printType);
							$scope.mediaAnimated = $filter('fileurl')($scope.printMedia, $scope.printWidth, "originals");
							break;
						case 'video':
							$scope.playable = true;
							switch ($scope.printMedia.typevideo) {
								case 'url':
									templateName = 'print-media-video_url.html';
									break;
								case 'file':
									templateName = 'print-media-video_file.html';
									$scope.mediaUrl = $filter('fileurl')($scope.printMedia, $scope.printWidth, $scope.printType);
									break;
							}
							break;
						case 'pdf':
							templateName = 'print-media-pdf.html';
							$scope.mediaUrl = $filter('fileurl')($scope.printMedia, $scope.printWidth, "originals");
							break;
						default:
							switch ($scope.printMedia.shortname) {
								case 'Video':
									$scope.playable = true;
									switch ($scope.printMedia.typevideo) {
										case 'url':
											templateName = 'print-media-video_url.html';
											break;
										case 'file':
											templateName = 'print-media-video_file.html';
											$scope.mediaUrl = $filter('fileurl')($scope.printMedia, $scope.printWidth, $scope.printType);
											break;
									}
									break;
								default:
									console.error('Undefined mediatype', $scope.printMedia)
									templateName = 'print-media-invalid.html';
									$scope.message = "Cet élément " + $scope.printMedia.shortname + " ne peut être affiché.";
									break;
							}
							break;
					}
					templateUrl = entities.getBaseUrl('bundles/websiteadmin/angular/templates/bases/'+templateName+sitedata.getCacheRemover(), false);
					$templateRequest(templateUrl).then(
						function success(template) {
							$scope.status.loading = false;
							var $template = $(template);
							$compile($template)($scope);
							$element.replaceWith($template);
							$timeout(function() {
								switch ($scope.printMedia.mediatype) {
									case 'image':
										if(loadGif) {
											var options = {
												gif: $('img', $template).first().get(0),
												progressbar_height: 5,
												progressbar_foreground_color: '#036',
											};
											SGif = new SuperGif(options);
											SGif.load();
											// $scope.status.playing = true;
											$scope.Rewind = function() { SGif.move_to(0); return false; };
											$scope.Play = function() { SGif.play(); $scope.status.playing = SGif.get_playing(); return false; };
											$scope.Pause = function() { SGif.pause(); $scope.status.playing = SGif.get_playing(); return false; };
											$scope.StepBack = function() { SGif.move_relative(-1); return false; };
											$scope.StepForward = function() { SGif.move_relative(1); return false; };
											// $scope.status.length = SGif.get_length();
											// if(!!$scope.printPlay) $scope.Play(); else $scope.Pause();
											var is_gif_styled = false;
											$interval(function() {
												$scope.status.playing = SGif.get_playing();
												$scope.status.loading = SGif.get_loading();
												$scope.status.length = SGif.get_length();
												$scope.status.current = SGif.get_current_frame() + 1;
												$scope.status.currentPC = Math.ceil(($scope.status.current / $scope.status.length) * 100)+"%";
												$scope.status.multiple = $scope.status.length > 1;
												$scope.status.atBegin = $scope.status.current <= 1;
												$scope.status.atEnd = $scope.status.current >= $scope.status.length;
												if(!$scope.status.loading && !is_gif_styled) {
													$timeout(function() {
														is_gif_styled = true;
														$canvas = $template.find('.jsgif > canvas').first();
														if($canvas.length) {
															$canvas
																.addClass('img-responsive')
																.addClass('img-rounded')
																.css('margin-right', 'auto')
																.css('margin-left', 'auto')
																;
														}
													});
												}
											}, 100);
										}
										break;
									case 'pdf':
										var pdfDoc = null,
											pageNum = 1,
											pageRendering = false,
											pageNumPending = null,
											scale = 1,
											zoomRange = 0.25,
											canvas = $('#pdf_canvas', $template).get(0),
											ctx = canvas.getContext('2d');

										/**
										 * Get page info from document, resize canvas accordingly, and render page.
										 * @param num Page number.
										 */
										var renderPage = function(num, scale) {
										    pageRendering = true;
										    // Using promise to fetch the page
										    pdfDoc.getPage(num).then(function(page) {
										        var viewport = page.getViewport({ scale: scale });
										        canvas.height = viewport.height;
										        canvas.width = viewport.width;
										        // Render PDF page into canvas context
										        var renderContext = {
										            canvasContext: ctx,
										            viewport: viewport
										        };
										        var renderTask = page.render(renderContext);
										        // Wait for rendering to finish
										        renderTask.promise.then(function () {
										            pageRendering = false;
										            if (pageNumPending !== null) {
										                // New page rendering is pending
										                renderPage(pageNumPending);
										                pageNumPending = null;
										            }
										        });
										        return renderTask.promise;
										    });
										    // Update page counters
										    // $('#page_num', $template).attr('value', num);
										    $('#page_num', $template).val(num);
										}

										/**
										 * If another page rendering in progress, waits until the rendering is
										 * finised. Otherwise, executes rendering immediately.
										 */
										var queueRenderPage = function(num) {
										    if (pageRendering) {
										        pageNumPending = num;
										    } else {
										        renderPage(num,scale);
										    }
										}

										/**
										 * Displays previous page.
										 */
										var onPrevPage = function() {
										    if (pageNum <= 1) return;
										    pageNum--;
										    var scale = pdfDoc.scale;
										    queueRenderPage(pageNum, scale);
										}

										/**
										 * Displays next page.
										 */
										var onNextPage = function() {
										    if (pageNum >= pdfDoc.numPages) return;
										    pageNum++;
										    var scale = pdfDoc.scale;
										    queueRenderPage(pageNum, scale);
										}

										/**
										 * Zoom in page.
										 */
										var onZoomIn = function() {
										    if (scale >= pdfDoc.scale) return;
										    scale += zoomRange;
										    var num = pageNum;
										    renderPage(num, scale)
										}

										/**
										 * Zoom out page.
										 */
										var onZoomOut = function() {
										    if (scale >= pdfDoc.scale) return;
										    scale -= zoomRange;
										    var num = pageNum;
										    queueRenderPage(num, scale);
										}

										/**
										 * Zoom fit page.
										 */
										var onZoomFit = function() {
										    if (scale >= pdfDoc.scale) return;
										    scale = 1;
										    var num = pageNum;
										    queueRenderPage(num, scale);
										}

										$('#prev', $template).on('click', function(item) { onPrevPage(); });
										$('#next', $template).on('click', function(item) { onNextPage(); });
										$('#zoomin', $template).on('click', function(item) { onZoomIn(); });
										$('#zoomout', $template).on('click', function(item) { onZoomOut(); });
										$('#zoomfit', $template).on('click', function(item) { onZoomFit(); });

										/**
										 * Asynchronously downloads PDF
										 */
										var loadingTask = pdfjsLib.getDocument($scope.mediaUrl);
										loadingTask.promise.then(function (pdfDoc_) {
											pdfDoc = pdfDoc_;
											var documentPagesNumber = pdfDoc.numPages;
											$('#page_count', $template).text('/ ' + documentPagesNumber);
											$('#page_num').on('change', function() {
												var pageNumber = Number($(this).val());
												if(pageNumber > 0 && pageNumber <= documentPagesNumber) {
													queueRenderPage(pageNumber, scale);
												}
											});
											// Initial/first page rendering
											renderPage(pageNum, scale);
										});
										break;
									case 'video':
										// 
										break;
									default:
										// 
										break;
								}
							});
						},
						function error(response) {
							$scope.status.loading = false;
							toastr.error('<strong>Error:</strong><br><p>could not load template!</p>');
						}
					);
				};

				if(/^bddid@---/i.test($scope.viewMedia)) {
					// by bddid
					$scope.loading = true;
					entities.getEntityByBddid($scope.viewMedia).then(
						function success(response) {
							$scope.viewMedia = response.data;
							$scope.viewShortname = $scope.viewMedia.shortname;
							openModale();
						},
						function error(response) {
							$scope.error = true;
							$scope.loading = false;
							console.error('Error media:', response);
							toastr.error('<strong>Modale Error:</strong><br>'+(response.statusText || response));
						}
					);
				} else if(isFinite($scope.printMedia)) {
					// by id
					$scope.status.loading = true;
					// console.debug('Print loading:', {shortname: $scope.printShortname, id: $scope.printMedia});
					entities.getEntityById($scope.printShortname, $scope.printMedia).then(
						function success(response) {
							$scope.printMedia = response.data;
							$scope.printShortname = $scope.printMedia.shortname;
							mediaReady();
						},
						function error(response) {
							$scope.error = true;
							$scope.status.loading = false;
							console.error('Error media:', response);
							toastr.error('<strong>Print Error:</strong><br>'+(response.statusText || response));
						}
					);
				} else {
					$scope.printMedia = angular.fromJson($scope.printMedia);
					mediaReady();
				}

			},
		};
	}
]);

/**
 * Open modale for media view
 * Use: <img src="https://media_thumbnail.url" class="img-responsive" data-view-media="(here, media id or json representation of media entity)">
 */
app.directive("viewMedia", ['$templateRequest','$compile','$timeout','$filter','entities','sitedata', function ($templateRequest,$compile,$timeout,$filter,entities,sitedata) {
	return {
		restrict : "A",
		scope: {
			viewMedia: "<",
			viewShortname: "<",
			viewWidth: "<",
			viewType: "<",
			viewName: "<",
			viewDetails: "<",
		},
		controller: function ($scope, $element) {
			$element.css('cursor', 'pointer');
			$element.click(function (event) {
				$scope.loading = true;
				$scope.error = false;
				$scope.playable = false;
				$scope.playing = false;
				var openModale = function() {
					// console.debug('Media for modale:', $scope.viewMedia);
					$scope.viewWidth = parseInt($scope.viewWidth || 800);
					$scope.modalSize = $scope.viewWidth > 599 ? 'modal-lg' : 'modal-sm';
					if($scope.viewMedia.mediatype === 'pdf') $scope.modalSize = 'modal-lg';
					$scope.viewType = $scope.viewType || "optimized";
					$scope.viewName = $scope.viewName || $scope.viewMedia.name;
					if(isFinite($scope.viewDetails)) $scope.viewDetails = $scope.viewDetails > 0 ? true : false;
					$scope.viewDetails = typeof $scope.viewDetails === 'boolean' ? $scope.viewDetails : false;
					var templateUrl = entities.getBaseUrl('bundles/websiteadmin/angular/templates/modal/modal-view-media.html'+sitedata.getCacheRemover(), false);
					$templateRequest(templateUrl).then(
						function success(template) {
							var $modale = $(template);
							$compile($modale)($scope);
							$('body > #modals').append($modale);
							$modale.modal();
							$modale.on('hidden.bs.modal', function (e) {
								$timeout(function () { $modale.empty().remove(); }, 1000);
							});
							$timeout(function() { $scope.loading = false; }, 300);
						},
						function error(response) {
							toastr.error('<strong>Error:</strong><br>could not load template!');
						}
					);
				};
				if(/^bddid@---/i.test($scope.viewMedia)) {
					// by bddid
					$scope.loading = true;
					$scope.viewShortname = $scope.viewShortname || entities.getShortnameByBddid($scope.viewMedia); // Item by default
					console.debug('Modale data:', {shortname: $scope.viewShortname, media: $scope.viewMedia});
					entities.getEntityByBddid($scope.viewMedia).then(
						function success(response) {
							$scope.viewMedia = response.data;
							$scope.viewShortname = $scope.viewMedia.shortname;
							openModale();
						},
						function error(response) {
							$scope.error = true;
							$scope.loading = false;
							console.error('Error media:', response);
							toastr.error('<strong>Modale Error:</strong><br>'+(response.statusText || response));
						}
					);
				} else if(isFinite($scope.viewMedia)) {
					// by id
					$scope.viewShortname = $scope.viewShortname || 'Item'; // Item by default
					$scope.loading = true;
					entities.getEntityById($scope.viewShortname, $scope.viewMedia).then(
						function success(response) {
							$scope.viewMedia = response.data;
							$scope.viewShortname = $scope.viewMedia.shortname;
							openModale();
						},
						function error(response) {
							$scope.error = true;
							$scope.loading = false;
							console.error('Error media:', response);
							toastr.error('<strong>Modale Error:</strong><br>'+(response.statusText || response));
						}
					);
				} else {
					$scope.viewMedia = angular.fromJson($scope.viewMedia);
					// console.debug('Modale from json:', {shortname: $scope.viewShortname, media: $scope.viewMedia});
					openModale();
				}
			});
		}
	}
}]);

/**
 * Print bloc for Item
 * Use: <blocitem data-item-data="file"></blocitem>
 */
app.directive("blocitem", ["entities","sitedata", function(entities,sitedata) {
	return {
		restrict: "E",
		replace: true,
		templateUrl: entities.getBaseUrl('bundles/websiteadmin/angular/templates/bases/bloc-item.html'+sitedata.getCacheRemover(), false),
		scope: {
			item: "=",
			media: "<",
			size: "<",
			statusbar: "<",
		},
		link: function ($scope, $element, attrs) {
			console.debug('Bloc Item:', $scope.item);
			$scope.size = $scope.size || 90;
			$scope.statusbar = $scope.statusbar || false;
			$scope.media = angular.isObject($scope.media) ? $scope.media : $scope.item;
			$scope.viewables = ['image','pdf'];
			$scope.isImage = $scope.viewables.includes($scope.media.mediatype || '');
		},
	};
}]);

/**
 * Module for manage files in folder
 * Use: <div class="ibox-content" data-manage-files="{{ {type: 'video', parent: videos_folder.id}|json_encode }}">
 */
app.directive("manageFiles", ['$templateRequest','$compile','$http','$httpParamSerializer','$interval','$filter','entities','sitedata', function ($templateRequest,$compile,$http,$httpParamSerializer,$interval,$filter,entities,sitedata) {
	return {
		restrict: "A",
		replace: false,
		templateUrl: entities.getBaseUrl('bundles/websiteadmin/angular/templates/bases/manage-files-list.html'+sitedata.getCacheRemover(), false),
		scope: {
			manageFiles: "=",
		},
		controller: function ($scope, $element) {
			$scope.manageFiles.text_addfile = $scope.manageFiles.text_addfile || 'Ajouter '+$scope.manageFiles.classes[0];
			$scope.dev = sitedata.isDev();
			$scope.loading = false;
			$scope.error = false;
			$scope.loading_form = false;
			$scope.files = {};
			$scope.is_swal = false;
			$scope.loadStyle = {};
			$scope.itemSize = 100;
			var URL_FILES = entities.getBaseUrl('api/basedirectory/childs/'+$scope.manageFiles.parent, true);
			var URL_FORM = null;
			var FORM_DATA = null;
			var form_template = null;

			if($scope.dev) {
				$interval(function() { $scope.is_swal = Swal.isVisible(); }, 500);
			}

			var base_options = function(html) {
				return {
					icon: null,
					title: $scope.manageFiles.text_addfile,
					html: '<div class="container-fluid"><div class="row text-left"><div class="col-md-12">'+html+'</div></div></div>',
					footer: '',
					target: 'body',
					width: '95rem',
					//padding: '1rem',
					position: 'center',
					allowOutsideClick: true,
					allowEscapeKey: true,
					showCloseButton: true,
					showCancelButton: false,
					//cancelButtonText: '<i class="fa fa-times fa-fw"></i> annuler',
					showConfirmButton: false,
					onRender: function(item) {
						$scope.is_swal = true;
						$scope.loading_form = false;
						$swal_content = $(Swal.getContent());
						$compile($swal_content)($scope);
						$form = $swal_content.find('form');
						//var $inputs = $form.find(":submit");
						//$inputs.addClass('btn-danger');
						$form.on('submit', function(e) {
							e.preventDefault();
							sendForm($form);
						});
					},
					onBeforeOpen: function(item) {
						$scope.is_swal = true;
					},
					onAfterClose: function(item) {
						$scope.is_swal = false;
					},
				};
			};

			var success_options = function(response, message) {
				return {
					icon: 'success',
					title: $scope.manageFiles.text_addfile,
					html: '<div class="container-fluid"><div class="row text-center"><div class="col-md-12">'+(message || 'Cette opération a été réalisée avec succès !')+'</div></div></div>',
					//footer: '',
					width: '65rem',
					showConfirmButton: false,
					//showCloseButton: true,
					showCancelButton: true,
					cancelButtonText: '<i class="fa fa-times fa-fw"></i> Fermer',
					//timer: 3000,
					//timerProgressBar: true,
				};
			};

			var warning_options = function(response, message) {
				return {
					icon: 'warning',
					title: $scope.manageFiles.text_addfile + ' / ' + message,
					html: '<div class="container-fluid"><div class="row text-left"><div class="col-md-12">'+response.data+'</div></div></div>',
					//footer: '',
					showConfirmButton: false,
					//showCloseButton: true,
					//showCancelButton: true,
					//cancelButtonText: '<i class="fa fa-times fa-fw"></i> Fermer',
				};
			};

			var error_options = function(response, message) {
				var retry = $scope.dev ? '<div class="text-center m-t-md m-b-md"><button data-ng-click="retry()" class="btn btn-danger btn-sm woc"><i class="fa fa-refresh fa-fw m-r-xs"></i>Recommencer</buttons></div>' : '';
				return {
					icon: 'error',
					title: response.statusText,
					html: '<div class="container-fluid"><div class="row text-center"><div class="col-md-12">'+(message || 'Une erreur est survenue !')+retry+'</div></div></div>',
					//footer: footer,
					showConfirmButton: false,
					//showCloseButton: true,
					showCancelButton: true,
					cancelButtonText: '<i class="fa fa-times fa-fw"></i> Fermer',
				};
			};

			$scope.refreshFileslist = function(data, mask) {
				if(mask || true) $scope.loading = true;
				$scope.loadStyle = {opacity: '0.4'};
				//console.debug('URL_FILES: ', URL_FILES);
				if(!angular.isArray(data || null)) {
					$http.get(URL_FILES).then(
						function success(response) {
							$scope.loadStyle = {};
							if(response.status === 200) {
								$scope.loading = false;
								console.debug('Fichiers:', response.data);
								$scope.files = response.data;
							} else {
								$scope.loading = false;
								$scope.error = true;
								$scope.message = response.data.message;
								Swal.fire(error_options(response, response.data.message));
							}
						},
						function error(response) {
							$scope.loadStyle = {};
							$scope.loading = false;
							$scope.error = true;
							$scope.message = response.data.message;
							Swal.fire(error_options(response, response.data.message));
						}
					);
				} else {
					$scope.loadStyle = {};
					$scope.loading = false;
					$scope.files = data;
				}
			};

			var sendForm = function($form) {
				if(undefined != $form) {
					FORM_DATA = $form.serialize();
					URL_FORM = $form.attr('action');
					FORM_METHOD = $form.attr('method') || 'post';
				}
				if(angular.isString(URL_FORM)) {
					console.debug('Form data:', FORM_DATA);
					var promise = $http({
						url: URL_FORM,
						method: FORM_METHOD,
						data: FORM_DATA,
						headers: {'Content-Type': 'application/x-www-form-urlencoded'},
						//headers: {'Content-Type': 'multipart/form-data'},
						//transformRequest: angular.identity,
					});
					promise.then(
						function success(response) {
							if(response.status === 200) {
								$scope.loading = false;
								if(angular.isString(response.data)) {
									console.warn('Warning form:', response);
									Swal.update(warning_options(response, 'Données incomplètes'));
								} else if(angular.isArray(response.data) && response.data.length) {
									console.debug('Success form:', response);
									$scope.refreshFileslist(response.data);
									Swal.update(success_options(response, 'Votre fichier a été enregistré !'));
								} else {
									$scope.error = true;
									$scope.message = "Erreur";
									console.error('Error form:', response);
									Swal.update(error_options(response, "Une erreur est survenue !"));
								}
							} else {
								$scope.loading = false;
								$scope.error = true;
								$scope.message = response.data.message;
								console.error('Error form:', response);
								Swal.update(error_options(response, response.data.message));
							}
						},
						function error(response) {
							$scope.loading = false;
							$scope.error = true; $scope.message = response.data.message;
							console.error('Error form:', response);
							Swal.update(error_options(response, response.data.message));
						}
					);
					return promise;
				}
				console.error('Can not send form because form is not defined!');
			};

			$scope.retry = function() {
				sendForm();
			};

			$scope.addFile = function() {
				if(!$scope.is_swal) {
					if(null == form_template || $scope.dev) {
						$scope.loading_form = true;
						var FORM_URL = entities.getBaseUrl('api/basedirectory/create-child/form/'+$scope.manageFiles.parent+'/'+$scope.manageFiles.classes[0], true);
						console.debug('FORM URL: ', FORM_URL);
						$http.get(FORM_URL).then(
							function success(response) {
								form_template = response.data;
								Swal.fire(base_options(form_template));
							},
							function error(response) {
								$scope.loading_form = false;
								$scope.error = true;
								$scope.message = response.data.message;
								console.error('Error on load form template', response);
								Swal.fire(error_options(response, response.data.message));
							}
						);
					} else {
						Swal.fire(base_options(form_template));
					}
				}
			};

			// INIT
			$scope.refreshFileslist();
		},
	}
}]);





