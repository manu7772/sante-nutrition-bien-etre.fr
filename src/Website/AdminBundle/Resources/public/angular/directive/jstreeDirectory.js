
// var temp_data = null;

// var globals_initialized = false;

var copied_node = null;
var copyNode = function(node) {
	copied_node = !!node ? angular.copy(node, {}) : null;
	console.debug('*** COPIED node:', copied_node);
};

// var global_jstree = function() {
// 	if(!globals_initialized) {
// 		$(document).on("dnd_start.vakata", function (event, element) {
// 			console.debug('Drag start:', {event: event, element: element});
// 			// temp_data = angular.copy(data);
// 		});

// 		$(document).on("dnd_stop.vakata", function (event, element) {
// 			console.debug('Drag stop:', {event: event, element: element});
// 			// if(null !== temp_data) data = angular.copy(temp_data);
// 			// temp_data = null;
// 			// element.data.origin.restore_state();
// 		});
// 		globals_initialized = true;
// 	}
// 	return globals_initialized;
// };

// Tree files
// use :
// <div id="jstree" data-jstree-path-files></div>
app.directive("jstreePathFiles", ['$timeout','entities', function ($timeout, entities) {
	// global_jstree();
	return {
		restrict : "A",
		scope: {
			jstreePathFiles: '<',
		},
		controller: function ($scope, $element) {
			// var splitedId = function (id) { return angular.isString(id) ? id.split('_').last() : null; }
			var plugins = [
				// "contextmenu",
			];
			// console.debug('jstreeDirectory:', $scope.jstreeDirectory);
			$element.jstree({
				plugins: plugins,
				core: {
					themes: {
						'stripes': 'true',
					},
					data: {
						url: entities.getBaseUrl('api/jstree/systemfile/'+encodeURI($scope.jstreePathFiles), true),
					},
					check_callback:  true,
				},
				// contextmenu: {
				// 	items: function (node) {
				// 		var Jstree = $element.jstree(true);
				// 		// The default set of all items
				// 		// console.debug('Menu node:', node);
				// 		var items = {};
				// 		// 
				// 		// The "rename" menu item
				// 		if(!["#","Root"].includes(node.type)) {
				// 			items.renameItem = {
				// 				label: "Renommer",
				// 				icon: 'fa fa-pencil-square-o',
				// 				action: function(obj) {
				// 					Jstree.edit(node, node.data.name, function (node, result, canceled) {
				// 						// callback
				// 					});
				// 				},
				// 			};
				// 		}
				// 		if(["File"].includes(node.type)) {
				// 			items.removeItem = {
				// 				label: "Supprimer",
				// 				icon: 'fa fa-trash',
				// 				action: function(obj) {
				// 					// confirm
				// 					confirm("Voulez-vous vraiment supprimer ce fichier ?", "Confirmation", function(response) {
				// 						// entities.setCurrent(null);
				// 						if(!!response) {
				// 							Jstree.deselect_node(node);
				// 							Jstree.delete_node(node);
				// 						}
				// 					});
				// 				},
				// 			};
				// 		}
				// 		return items;
				// 	},
				// },
			});

			var refresh_buttons = $("body").find("[data-jstree-refresh='#"+$element.attr('id')+"']");
			// if(refresh_buttons.length) {
				refresh_buttons.each(function (item) {
					// alert('Button detected for refreshing '+$element.attr('id')+' Jstree!');
					$(this).on('click', function () {
						$element.jstree(true).refresh();
					});
				});
			// }

			$element.on('select_node.jstree', function (node, selected, event) {
				// alert(angular.toJson(selected.node));
				// console.debug('Selected node:', {node: node, selected: selected, event: event});
				console.debug('Selected', selected.instance.element.data());
				console.debug('Selected', selected.node);
			});
		},
	};
}]);

// Tree files
// use :
// <div id="jstree" data-jstree-files></div>
/*app.directive("jstreeFiles", ['$timeout', function ($timeout) {
	// global_jstree();
	return {
		restrict : "A",
		scope: {
			jstreeFiles: '=',
		},
		controller: function ($scope, $element) {
			var splitedId = function (id) { return angular.isString(id) ? id.split('_').last() : null; }
			var plugins = [
				// "checkbox",
				// "unique",
				"sort",
				"changed",
				"contextmenu",
				"state",
				"dnd",
				"types",
			];
			// console.debug('jstreeDirectory:', $scope.jstreeDirectory);
			$element.jstree({
				plugins: plugins,
				dnd: {
					always_copy: false,
					check_while_dragging: true,
					copy: true,
					drag_selection: true,
					inside_pos: 0,
					is_draggable: true,
					large_drag_target: false,
					large_drop_target: false,
					open_timeout: 300,
					touch: true,
					use_html5: false,
				},
				sort: function(node1, node2) {
					node1 = $element.jstree(true).get_node(node1);
					node2 = $element.jstree(true).get_node(node2);
					if(!!node1.data || !!node2.data) return 1;
					if(!!node1.data.link || !!node2.data.link) return 1;
					return node1.data.link.position > node2.data.link.position ? 1 : -1;
				},
				state: [$element.attr('id')],
				types: {
					"#" : {
						use_data: true,
						max_children : -1,
						max_depth : -1,
						valid_children : ["default","File","Directory"],
					},
					"Root" : { // is DirectoryInterface
						use_data: true,
						icon : "fa fa-folder",
						max_children : -1,
						max_depth : -1,
						valid_children : ["default","File","Directory"],
					},
					"Orphan" : { // not DirectoryInterface, but no parent
						use_data: true,
						icon : "fa fa-file",
						max_children : -1,
						max_depth : -1,
						valid_children : "none",
					},
					"Directory" : {
						use_data: true,
						icon : "fa fa-folder",
						max_children : -1,
						max_depth : -1,
						valid_children : ["default","File","Directory"],
					},
					"default" : {
						use_data: true,
						icon : "fa fa-file",
						max_children : -1,
						max_depth : -1,
						valid_children : "none",
					},
					"File" : {
						use_data: true,
						icon : "fa fa-file-o",
						max_children : -1,
						max_depth : -1,
						valid_children : "none",
					}
				},
				core: {
					themes: {
						'stripes': 'true',
					},
					data: {
						url: function (node) {
							// console.debug('Load node:', node);
							return node.id === '#' ?
								entities.getBaseUrl('api/jstree/root-nested-item', true):
								null; // no children!
						},
						data: function (node) {
							return null;
							// return { 'id' : node.id };
						},
					},
					check_callback:  true,
				},
			});

			var refresh_buttons = $("body").find("[data-jstree-refresh='#"+$element.attr('id')+"']");
			// if(refresh_buttons.length) {
				refresh_buttons.each(function (item) {
					// alert('Button detected for refreshing '+$element.attr('id')+' Jstree!');
					$(this).on('click', function () {
						$element.jstree(true).refresh();
					});
				});
			// }

			$element.on("changed.jstree", function (e, data) {
				console.debug('Changed:', {data: data, e: e});
			});

			$element.on('select_node.jstree', function (node, selected, event) {
				// alert(angular.toJson(selected.node));
				// console.debug('Selected node:', {node: node, selected: selected, event: event});
				console.debug('Selected', selected.instance.element.data());
				console.debug('Selected', selected.node);
			});
		},
	};
}]);
*/

var embJstree = {
	refresh_node_or_parent: function(Jstree, node) {
		if(angular.isString(node)) node = Jstree.get_node(node);
		if(!!node.data && node.data.can_have_children) {
			var opened = node.state.opened;
			Jstree.refresh_node(node);
			if(opened) Jstree.open_node(node);
			// Jstree.select_node(node);
		} else if(!!node.parent) {
			embJstree.refresh_node_or_parent(Jstree, Jstree.get_node(node.parent));
		}
	},
};


// Tree directory
// use :
// <div id="jstree" data-jstree-directory="{[{ directory.id  }]}"></div>
app.directive("jstreeDirectory", ['$http','$timeout','entities', function ($http, $timeout, entities) {
	// global_jstree();

	// var waitIcon = function (node, OnOff, timing) {
	// 	// node.original.icon = 'fa fa-spinner fa-fw fa-spin text-success';
	// 	var $nodeIcon = $(node);
	// 	// console.debug('****************** NODE:', node);
	// 	if(!!OnOff) {
	// 		// ON
	// 		if(!node.original.save_icon) node.original.save_icon = node.original.icon;
	// 		node.original.icon = 'fa fa-spinner fa-fw fa-spin text-success';
	// 		if(isFinite(timing) && timing > 0) $timeout(function() { waitIcon(node, false); }, timing);
	// 	} else if(!!node.original.save_icon) {
	// 		// OFF
	// 		node.original.icon = node.original.save_icon;
	// 	}
	// };

	return {
		restrict : "A",
		scope: {
			jstreeDirectory: '<',
		},
		controller: function ($scope, $element) {
			// var splitedId = function (id) { return angular.isString(id) ? id.split('_').last() : null; }
			var regularId = function (node) { return node.original.regular_id; }
			var plugins = [
				// "checkbox",
				// "unique",
				// "sort",
				"changed",
				"contextmenu",
				"state",
				"dnd",
				"types",
			];

			var stop_propagation = {
				rename: false,
			};

			// $scope.currentItem = null;
			var setCurrentItem = function(bddid) {
				// console.debug('--- Define current Item:', bddid);
				$timeout(function() { entities.setCurrent(bddid); }, 200);
			};
			setCurrentItem(null);

			// console.debug('jstreeDirectory:', $scope.jstreeDirectory);
			$element.jstree({
				plugins: plugins,
				dnd: {
					always_copy: false,
					check_while_dragging: true,
					copy: true,
					drag_selection: true,
					inside_pos: 0,
					is_draggable: true,
					large_drag_target: false,
					large_drop_target: false,
					open_timeout: 300,
					touch: true,
					use_html5: false,
				},
				// sort: function(node1, node2) {
				// 	node1 = $element.jstree(true).get_node(node1);
				// 	node2 = $element.jstree(true).get_node(node2);
				// 	if(undefined == node1.data || undefined == node2.data) return 1;
				// 	if(undefined == node1.data.link || undefined == node2.data.link) return 1;
				// 	return node1.data.link.position > node2.data.link.position ? 1 : -1;
				// },
				state: [$element.attr('id')],
				types: {
					"#" : {
						use_data: true,
						max_children : -1,
						max_depth : -1,
						valid_children : ["default","File","Directory"],
					},
					"Root" : { // is DirectoryInterface
						use_data: true,
						icon : "fa fa-folder",
						max_children : -1,
						max_depth : -1,
						valid_children : ["default","File","Directory"],
					},
					"Orphan" : { // not DirectoryInterface, but no parent
						use_data: true,
						icon : "fa fa-file",
						max_children : -1,
						max_depth : -1,
						valid_children : "none",
					},
					"Directory" : {
						use_data: true,
						icon : "fa fa-folder",
						max_children : -1,
						max_depth : -1,
						valid_children : ["default","File","Directory"],
					},
					"default" : {
						use_data: true,
						icon : "fa fa-file",
						max_children : -1,
						max_depth : -1,
						valid_children : "none",
					},
					"File" : {
						use_data: true,
						icon : "fa fa-file-o",
						max_children : -1,
						max_depth : -1,
						valid_children : "none",
					}
				},
				core: {
					themes: {
						'stripes': 'true',
					},
					data: {
						url: function (node) {
							var URL;
							// console.debug('Load node:', node);
							var URL = node.id === '#' ?
								entities.getBaseUrl('api/jstree/directory/'+$scope.jstreeDirectory, true):
							// if(node.data.can_have_children)
								entities.getBaseUrl('api/jstree/directory-childs/'+regularId(node), true);
							// return entities.getBaseUrl('api/jstree/item/'+splitedId(node.id), true);
							console.debug('URL childs', URL);
							return URL;
						},
						data: function (node) { // GET data
							return null;
							// return { 'id' : node.id };
						},
					},
					check_callback:  true,
				},
				contextmenu: {
					items: function (node) {
						var Jstree = $element.jstree(true);
						// The default set of all items
						// console.debug('Menu node:', node);
						var items = {};
						// 
						// The "rename" menu item
						if(!["#","Root"].includes(node.type)) {
							items.renameItem = {
								label: "Renommer",
								icon: 'fa fa-pencil-square-o',
								action: function(obj) {
									Jstree.edit(node, node.data.name, function (node, result, canceled) {
										// callback
										setCurrentItem(node.original.bddid);
										// entities.refreshCurrent();
									});
								},
							};
						}
						// 
						// The "delete" menu item
						if(!["#","Root"].includes(node.type)) {
							items.deleteItem = {
								_disabled: !node.state.deletable || node.original.count_childs > 0,
								label: "Supprimer",
								icon: 'fa fa-trash',
								action: function (obj) {
									// confirm
									confirm("Voulez-vous vraiment supprimer cet élément "+node.text+" ?", "Supprimer", function(response) {
										// entities.setCurrent(null);
										if(!!response) {
											var parent_node = Jstree.get_node(node.parent);
											// if(node.data.link.type === 'symbolic') {
												// remove symbolic link
												var shortname = node.data.link.shortname;
												var id = node.data.link.id;
											// } else {
												// remove entity
												// var shortname = node.original.type;
												// var id = splitedId(node.id);
											// }
											entities.JstreeDelete(shortname, id).then(
												function success(response) {
													// if(!!parent_node) parent_node.original.count_childs--;
													Jstree.deselect_node(node);
													Jstree.delete_node(node);
													setCurrentItem(null);
													// refresh parent
													embJstree.refresh_node_or_parent(Jstree, parent_node);
													// Jstree.refresh_node(parent_node);
													// Jstree.open_node(parent_node);
													// Jstree.select_node(parent_node);
													// alert({text: response.message, icon: "success" });
												}
											);
										}
									});
								},
							};
						}
						// 
						// The "create" sub-menu item
						var create_sub = {};
						var createItem = false;
						// if(["Directory","Root"].includes(node.type)) {
						if(!!node.data.can_have_children && ['all','Directory'].includes(node.data.valid_children)) {
							create_sub.createDirectory = {
								_disabled: !node.state.opened && !(node.original.count_childs < 1),
								label: "Dossier",
								icon: 'fa fa-folder',
								action: function (obj) {
									Jstree.open_node(node);
									var $new_node = Jstree.create_node(node, { text: 'New Folder', type: 'Directory' });
									Jstree.deselect_all();
									Jstree.select_node($new_node);
									stop_propagation.rename = true;
									Jstree.edit($new_node, $new_node.text, function (edit_node, result, canceled) {
										// callback
										stop_propagation.rename = false;
										edit_node.li_attr.title = edit_node.text;
										edit_node.data = {
											can_have_children: true,
										};
										edit_node.shortname = 'Directory';
										edit_node.parent = node.original.bddid;
										console.debug('----- New directory post:', edit_node);
										entities.JstreeCreate(edit_node).then(
											function success(response) {
												// success
												// console.debug('----- New directory return:', response.data);
												if(angular.isString(response.data.filled_error)) {
													// but error anyway
													alert('Opération échouée : \n- '+response.data.filled_error);
													// Jstree.select_node($new_node);
													// $timeout(function() { Jstree.delete_node($new_node); }, 500);
													embJstree.refresh_node_or_parent(Jstree, node);
												} else {
													// refresh
													embJstree.refresh_node_or_parent(Jstree, node);
													// Jstree.select_node($new_node);
													setCurrentItem(response.data.original.bddid);
												}
											},
											function error(response) {
												// error
												// console.debug('Creation directory error:', response);
												alert('Ce dossier n\' a pu être créé\n- '+response.statusText, function(response) {
													// Jstree.select_node($new_node);
													// $timeout(function() { Jstree.delete_node($new_node); }, 500);
													embJstree.refresh_node_or_parent(Jstree, node);
												});
											}
										);
									});
								},
							};
							// if(!["Root"].includes(node.type)) {
							if(!!node.data.can_have_children && ['all','File'].includes(node.data.valid_children) && createItem) {
								create_sub.createFile = {
									_disabled: !node.state.opened && !(node.original.count_childs < 1),
									label: "Fichier",
									icon: 'fa fa-file-o',
									action: function (obj) {
										// $form = $('<form><input type="file"></form>');
										// $('body').append($form);
										// $('>input', $form).trigger();
										$new_node = Jstree.create_node(node, { text: 'New File', type: 'File' });
										Jstree.deselect_all();
										Jstree.select_node($new_node);
										setCurrentItem($new_node.original.bddid);
										// $form.empty().remove();
									},
								};
							}
						}
						if(Object.keys(create_sub).length > 0) {
							items.createItem = {
								separator_before: true,
								label: "Nouveau",
								icon: 'fa fa-plus',
								submenu: create_sub,
							};
						}
						// The "Copy" item
						if(!["Root","#"].includes(node.type)) {
							items.copyItem = {
								separator_before: true,
								label: "Copier",
								icon: 'fa fa-files-o',
								action: function(obj) {
									// embJstree.refresh_node_or_parent(Jstree, node);
									copyNode(node);
								},
							};
						}
						// The "Paste" item
						var cnid = !!copied_node ? copied_node.id : null;
						if(!["Root","#"].includes(node.type)) {
							var txt = !!copied_node ? copied_node.text : 'élément';
							items.pasteItem = {
								_disabled: !copied_node || node.id == cnid,
								label: "Coller "+txt,
								icon: 'fa fa-clipboard',
								action: function(obj) {
									// embJstree.refresh_node_or_parent(Jstree, node);
									console.debug('--- Paste node:', copied_node);
									console.debug('--- Paste into:', node);
									entities.JstreePaste(node, copied_node).then(
										function success(response) {
											embJstree.refresh_node_or_parent(Jstree, node);
										}
									);
								},
							};
						}
						// 
						// The "Refresh" item
						// if(["Directory","Root","#"].includes(node.type)) {
							items.refreshItem = {
								separator_before: true,
								label: "Actualiser...",
								icon: 'fa fa-refresh',
								action: function(obj) {
									embJstree.refresh_node_or_parent(Jstree, node);
								},
							};
						// }
						// 
						return items;
					},
				},
			});
			var Jstree = $element.jstree(true);
			// console.debug('******* JSTREE:', Jstree);
			var refresh_buttons = $("body").find("[data-jstree-refresh='#"+$element.attr('id')+"']");
			refresh_buttons.each(function (item) {
				// alert('Button detected for refreshing '+$element.attr('id')+' Jstree!');
				$(this).on('click', function () {
					Jstree.refresh();
				});
			});

			$element
				.on("rename_node.jstree", function (event, data) {
					if(!stop_propagation.rename && data.text !== data.old) {
						// console.debug('Rename:', {event: event, data: data, node: data.node});
						text = data.text.trim();
						var new_name_ok = !!text && text.length > 0;
						// new_name_ok = false;
						if(new_name_ok) {
							// name is ok
							var sendData = angular.copy(data.node.original, {});
							sendData.text = data.text;
							// console.debug('Rename send data:', sendData);
							$http({
								// url: entities.getBaseUrl('api/jstree/rename/'+splitedId(data.node.id), true),
								url: entities.getBaseUrl('api/jstree/post/items', true),
								method: 'POST',
								data: sendData,
							}).then(
								function success(response) {
									// 
									// console.debug('Rename success:', response.data);
									embJstree.refresh_node_or_parent(Jstree, data.node);
								},
								function error(response) {
									// 
									// console.debug('Rename error:', response);
									alert('Cet élément n\' a pu être renommé\n- '+response.statusText, function(response) {
										$timeout(function() { stop_propagation.rename = true; Jstree.rename_node(data.node, data.old); }, 500);
									});
								}
							);
						} else {
							// bad name
							alert('Ce nom "'+data.text+'" est incorrect.', function(response) {
								$timeout(function() { stop_propagation.rename = true; Jstree.rename_node(data.node, data.old); }, 500);
							});
						}
					}
					stop_propagation.rename = false;
				})
				.on("move_node.jstree", function (event, data) {
					console.debug('--- Moved event:', event);
					console.debug('--- Moved node:', data);
					// if(!node.is_multi) {
					// 	// single
					// } else {
					// 	// multi
					// }
					entities.JstreeMove(data).then(
						function success(response) {
							embJstree.refresh_node_or_parent(Jstree, data.parent);
							embJstree.refresh_node_or_parent(Jstree, data.old_parent);
							Jstree.select_node(Jstree.get_node(data.node.id));
						},
						function error(response) {
							alert('Cet élément n\' a pu être déplacé\n- '+response.statusText, function(response) {
								$timeout(function() {
									embJstree.refresh_node_or_parent(Jstree, data.parent);
									embJstree.refresh_node_or_parent(Jstree, data.old_parent);
									Jstree.select_node(Jstree.get_node(data.node.id));
								}, 500);
							});
						}
					);
				})
				.on("changed.jstree", function (event, data) {
					// console.debug('- '+event.type+' '+data.action+':', data);
					// $scope.currentItem = null;
					var new_select = false;
					switch(data.action) {
						case "deselect_node":
						case "select_node":
							if(!Object.keys(data.changed.selected).length) {
								// $scope.currentItem = null;
								setCurrentItem(null);
							} else {
								new_select = Jstree.get_node(data.changed.selected.first());
								// if(!!new_select) $scope.currentItem = new_select;
								if(!!new_select) {
									setCurrentItem(new_select.original.bddid);
									// console.debug('--- selected ---> '+event.type+' > '+data.action+': new selection', new_select);
								}
							}
							break;
					}
				})
				// .on('select_node.jstree', function (node, selected, event) {
					// alert(angular.toJson(selected.node));
					// console.debug('Selected node:', {node: node, selected: selected, event: event});
					// var nodeId = splitedId(selected.node.id);
					// console.debug('Selected '+nodeId, selected.instance.element);
					// console.debug('Selected '+nodeId, selected.node);
					// entities.infoEntity(nodeId, )
				// })
			;

		},
	};
}]);













// Tree group
// use :
// <div id="jstree" data-jstree-groups="{[{ group.id  }]}"></div>
app.directive("jstreeGroups", ['$http','$timeout','entities', function ($http, $timeout, entities) {
	// global_jstree();

	// var waitIcon = function (node, OnOff, timing) {
	// 	// node.original.icon = 'fa fa-spinner fa-fw fa-spin text-success';
	// 	var $nodeIcon = $(node);
	// 	// console.debug('****************** NODE:', node);
	// 	if(!!OnOff) {
	// 		// ON
	// 		if(!node.original.save_icon) node.original.save_icon = node.original.icon;
	// 		node.original.icon = 'fa fa-spinner fa-fw fa-spin text-success';
	// 		if(isFinite(timing) && timing > 0) $timeout(function() { waitIcon(node, false); }, timing);
	// 	} else if(!!node.original.save_icon) {
	// 		// OFF
	// 		node.original.icon = node.original.save_icon;
	// 	}
	// };

	return {
		restrict : "A",
		scope: {
			jstreeGroups: '<',
		},
		controller: function ($scope, $element) {
			var plugins = [
				// "checkbox",
				// "unique",
				"changed",
				"contextmenu",
				"state",
				"dnd",
				"types",
			];
			// alert('Lauched Jstree for Groups!');

			var stop_propagation = {
				rename: false,
			};

			// $scope.currentGroup = null;
			var setCurrentGroup = function(bddid) {
				// console.debug('--- Define current Group:', bddid);
				$timeout(function() { entities.setCurrent(bddid); }, 200);
			};
			setCurrentGroup(null);

			// console.debug('jstreeGroups:', $scope.jstreeGroups);
			$element.jstree({
				plugins: plugins,
				dnd: {
					always_copy: false,
					check_while_dragging: true,
					copy: true,
					drag_selection: true,
					inside_pos: 0,
					is_draggable: true,
					large_drag_target: false,
					large_drop_target: false,
					open_timeout: 300,
					touch: true,
					use_html5: false,
				},
				state: [$element.attr('id')],
				types: {
					"#" : {
						use_data: true,
						max_children : -1,
						max_depth : -1,
						valid_children : ["Group"],
					},
					"Root" : {
						use_data: true,
						icon : "fa fa-folder",
						max_children : -1,
						max_depth : -1,
						valid_children : ["Group"],
					},
					"Group" : {
						use_data: true,
						icon : "fa fa-users",
						max_children : -1,
						max_depth : -1,
						valid_children : ["Group"],
					},
				},
				core: {
					themes: {
						'stripes': 'true',
					},
					data: {
						url: function (node) {
							// console.debug('Load node:', node);
							if(node.id === '#') return entities.getBaseUrl('api/jstree/group/'+$scope.jstreeGroups, true);
							// if(node.data.can_have_children)
								return entities.getBaseUrl('api/jstree/group-childs/'+node.id, true);
							// return entities.getBaseUrl('api/jstree/item/'+splitedId(node.id), true);
						},
						data: function (node) { // GET data
							return null;
							// return { 'id' : node.id };
						},
					},
					check_callback:  true,
				},
				contextmenu: {
					items: function (node) {
						var Jstree = $element.jstree(true);
						// The default set of all items
						// console.debug('Menu node:', node);
						var items = {};
						// 
						// The "rename" menu item
						if(!["#","Root"].includes(node.type)) {
							items.renameItem = {
								label: "Renommer",
								icon: 'fa fa-pencil-square-o',
								action: function(obj) {
									Jstree.edit(node, node.data.name, function (node, result, canceled) {
										// callback
										setCurrentGroup(node.original.bddid);
										// entities.refreshCurrent();
									});
								},
							};
						}
						// 
						// The "delete" menu item
						if(!["#","Root"].includes(node.type)) {
							items.deleteItem = {
								_disabled: !node.state.deletable || node.original.count_childs > 0,
								label: "Supprimer",
								icon: 'fa fa-trash',
								action: function (obj) {
									// confirm
									confirm("Voulez-vous vraiment supprimer cet élément "+node.text+" ?", "Supprimer", function(response) {
										// entities.setCurrent(null);
										if(!!response) {
											var parent_node = Jstree.get_node(node.parent);
											entities.deleteEntityByBddid(node.original.bddid).then(
												function success(response) {
													// if(!!parent_node) parent_node.original.count_childs--;
													Jstree.deselect_node(node);
													Jstree.delete_node(node);
													setCurrentGroup(null);
													// refresh parent
													embJstree.refresh_node_or_parent(Jstree, parent_node);
													// Jstree.refresh_node(parent_node);
													// Jstree.open_node(parent_node);
													// Jstree.select_node(parent_node);
													// alert({text: response.message, icon: "success" });
												}
											);
										}
									});
								},
							};
						}
						// 
						if(!!node.data.can_have_children) {
							items.createItem = {
								separator_before: true,
								_disabled: !node.data.valid_children.includes('Group'),
								label: "Nouveau groupe",
								icon: 'fa fa-plus',
								action: function (obj) {
									Jstree.open_node(node);
									var $new_node = Jstree.create_node(node, { text: 'NEW GROUP', type: 'Group' });
									Jstree.deselect_all();
									Jstree.select_node($new_node);
									stop_propagation.rename = true;
									Jstree.edit($new_node, $new_node.text, function (edit_node, result, canceled) {
										// callback
										stop_propagation.rename = false;
										edit_node.li_attr.title = edit_node.text;
										edit_node.data = {
											can_have_children: true,
										};
										edit_node.shortname = 'Group';
										edit_node.parent = node.original.bddid;
										console.debug('----- New Group post:', edit_node);
										entities.JstreeCreate(edit_node).then(
											function success(response) {
												// success
												// console.debug('----- New directory return:', response.data);
												if(angular.isString(response.data.filled_error)) {
													// but error anyway
													alert('Opération échouée : \n- '+response.data.filled_error);
													// Jstree.select_node($new_node);
													// $timeout(function() { Jstree.delete_node($new_node); }, 500);
													embJstree.refresh_node_or_parent(Jstree, node);
												} else {
													// refresh
													embJstree.refresh_node_or_parent(Jstree, node);
													// Jstree.select_node($new_node);
													setCurrentGroup(response.data.original.bddid);
												}
											},
											function error(response) {
												// error
												// console.debug('Creation directory error:', response);
												alert('Ce dossier n\' a pu être créé\n- '+response.statusText, function(response) {
													// Jstree.select_node($new_node);
													// $timeout(function() { Jstree.delete_node($new_node); }, 500);
													embJstree.refresh_node_or_parent(Jstree, node);
												});
											}
										);
									});
								},
							};
						}
						// 
						// The "Refresh" item
						// if(["Directory","Root","#"].includes(node.type)) {
							items.refreshItem = {
								separator_before: true,
								label: "Actualiser...",
								icon: 'fa fa-refresh',
								action: function(obj) {
									embJstree.refresh_node_or_parent(Jstree, node);
								},
							};
						// }
						// 
						return items;
					},
				},
			});
			var Jstree = $element.jstree(true);
			// console.debug('******* JSTREE:', Jstree);
			var refresh_buttons = $("body").find("[data-jstree-refresh='#"+$element.attr('id')+"']");
			refresh_buttons.each(function (item) {
				// alert('Button detected for refreshing '+$element.attr('id')+' Jstree!');
				$(this).on('click', function () {
					Jstree.refresh();
				});
			});

			$element
				.on("rename_node.jstree", function (event, data) {
					if(!stop_propagation.rename && data.text !== data.old) {
						// console.debug('Rename:', {event: event, data: data, node: data.node});
						text = data.text.trim();
						var new_name_ok = !!text && text.length > 0;
						// new_name_ok = false;
						if(new_name_ok) {
							// name is ok
							var sendData = angular.copy(data.node.original, {});
							sendData.text = data.text;
							// console.debug('Rename send data:', sendData);
							$http({
								// url: entities.getBaseUrl('api/jstree/rename/'+splitedId(data.node.id), true),
								url: entities.getBaseUrl('api/jstree/post/items', true),
								method: 'POST',
								data: sendData,
							}).then(
								function success(response) {
									// 
									// console.debug('Rename success:', response.data);
									embJstree.refresh_node_or_parent(Jstree, data.node);
								},
								function error(response) {
									// 
									// console.debug('Rename error:', response);
									alert('Cet élément n\' a pu être renommé\n- '+response.statusText, function(response) {
										$timeout(function() { stop_propagation.rename = true; Jstree.rename_node(data.node, data.old); }, 500);
									});
								}
							);
						} else {
							// bad name
							alert('Ce nom "'+data.text+'" est incorrect.', function(response) {
								$timeout(function() { stop_propagation.rename = true; Jstree.rename_node(data.node, data.old); }, 500);
							});
						}
					}
					stop_propagation.rename = false;
				})
				.on("move_node.jstree", function (event, data) {
					console.debug('--- Moved event:', event);
					console.debug('--- Moved node:', data);
					// if(!node.is_multi) {
					// 	// single
					// } else {
					// 	// multi
					// }
					entities.JstreeMove(data).then(
						function success(response) {
							embJstree.refresh_node_or_parent(Jstree, data.parent);
							embJstree.refresh_node_or_parent(Jstree, data.old_parent);
							Jstree.select_node(Jstree.get_node(data.node.id));
						},
						function error(response) {
							alert('Cet élément n\' a pu être déplacé\n- '+response.statusText, function(response) {
								$timeout(function() {
									embJstree.refresh_node_or_parent(Jstree, data.parent);
									embJstree.refresh_node_or_parent(Jstree, data.old_parent);
									Jstree.select_node(Jstree.get_node(data.node.id));
								}, 500);
							});
						}
					);
				})
				.on("changed.jstree", function (event, data) {
					// console.debug('- '+event.type+' '+data.action+':', data);
					// $scope.currentItem = null;
					var new_select = false;
					switch(data.action) {
						case "deselect_node":
						case "select_node":
							if(!Object.keys(data.changed.selected).length) {
								// $scope.currentItem = null;
								setCurrentGroup(null);
							} else {
								new_select = Jstree.get_node(data.changed.selected.first());
								// if(!!new_select) $scope.currentItem = new_select;
								if(!!new_select) {
									setCurrentGroup(new_select.original.bddid);
									console.debug('--- selected ---> '+event.type+' > '+data.action+': new selection', new_select);
								}
							}
							break;
					}
				})
				// .on('select_node.jstree', function (node, selected, event) {
					// alert(angular.toJson(selected.node));
					// console.debug('Selected node:', {node: node, selected: selected, event: event});
					// var nodeId = splitedId(selected.node.id);
					// console.debug('Selected '+nodeId, selected.instance.element);
					// console.debug('Selected '+nodeId, selected.node);
					// entities.infoEntity(nodeId, )
				// })
			;

		},
	};
}]);


// /**
//  * Use: <div data-jstree-lazy-directory="id" data-mode="manage|view"></div>
//  */
// app.directive("jstreeLazyDirectory", ['$http', '$compile', '$timeout', 'sitedata', 'translations', 'notifyer',
// 	function ($http, $compile, $timeout, sitedata, translations, notifyer) {
// 		return {
// 			restrict: "A",
// 			// template: '<div class="jstree"></div>',
// 			scope: {
// 				jstreeLazyDirectory: '=',
// 				mode: "@",
// 			},
// 			controller: function ($scope, $element) {
// 				// notifyer.console.debug('jstreeLazyDirectory:', $scope);
// 				$scope.mode = $scope.mode || 'show';
// 				var plugins = $scope.mode === 'manage' ? ["contextmenu", "dnd", "state", "types", "changed"] : ["state", "types"];
// 				var types = sitedata.get('jstree-directory_types');
// 				notifyer.console.debug('Jstree '+$scope.mode+':', {plugins: plugins, types: types});
// 				var $info = $('#ibox-directory-info .ibox-content').first();
// 				if($info.length && !!$info.data('content')) $info.data('content') = $info.html();
// 				var addInfo = function (info, $target) {
// 					if(!angular.isObject(info)) return;
// 					var $ul = $('<ul></ul>');
// 					$target.append($ul);
// 					angular.forEach(info, function (item,key) {
// 						if(!angular.isObject(item)) {
// 							$ul.append($('<li><span class="text-muted">'+key+':</span> <span style="font-weight: bold;">'+item+'</span></li>'));
// 						} else {
// 							var $li = $('<li><span class="text-muted">'+key+':</span></li>');
// 							$ul.append($li);
// 							addInfo(item, $li);
// 						}
// 					});
// 				};
// 				var getRootUrl = function(id) {
// 					var id = !!id ? '/'+id : '';
// 					return sitedata.getPath('api', 'jstree/root/lazy/directory'+id);
// 				};
// 				var getUrl = function(id) {
// 					return sitedata.getPath('api', 'jstree/lazy/directory/'+id);
// 				};
// 				var contextmenu_items = function(node) {
// 					var Jstree = $element.jstree(true);
// 					notifyer.console.debug('Node: ', node);
// 					notifyer.console.debug('Tree: ', Jstree);
// 					var items = {};
// 					if(node.parent !== '#') {
// 						// create items
// 						var valids = node.valid_children;
// 						if(!valids) valids = node.original.valid_children;
// 						if(!valids) valids = Jstree.settings.types[node.type].valid_children;
// 						notifyer.console.debug('Valid children:', valids);
// 						if(valids.length) {
// 							var getCreateSubmenu = function(type) {
// 								return {
// 									label: translations.translate('entities.'+Jstree.settings.types[type].text.toLowerCase()+'.actions.new'),
// 									icon: Jstree.settings.types[type].icon,
// 									action: function(data) {
// 										var ref = $.jstree.reference(data.reference);
// 										sel = ref.get_selected();
// 										if (!sel.length) {
// 											toastr["error"](translations.translate('jstree.errors.failedCreate'));
// 											return;
// 										}
// 										var new_name = prompt('Nom', Jstree.settings.types[type].text);
// 										if(!!new_name) {
// 											notifyer.console.debug('Creating "'+type+'":', node);
// 											toastr["info"](translations.translate('jstree.info.generating'));
// 											$http.post(
// 												sitedata.getPath('api', 'jstree/create/item'),
// 												{name: new_name, classname: type, parent_id: node.id}
// 											).then(
// 												function success(response) {
// 													if(response.status === 200) {
// 														// Jstree.refresh_node(node);
// 														// sel = ref.create_node(sel.pop(), {
// 														// 	// text: Jstree.settings.types[type].text,
// 														// 	text: new_name,
// 														// 	type: type
// 														// });
// 													} else {
// 														toastr["error"](translations.translate('jstree.errors.failedCreate'));
// 													}
// 													Jstree.refresh_node(node);
// 												},
// 												function error(response) {
// 													toastr["error"](translations.translate('jstree.errors.failedCreate'));
// 												}
// 											);
// 											// if (sel) {
// 											// 	$timeout(function() { ref.edit(sel); }, 0);
// 											// 	// toastr["success"](translations.translate('jstree.success.create'));
// 											// } else {
// 											// 	toastr["error"](translations.translate('jstree.errors.failedCreate'));
// 											// }
// 										} else {
// 											toastr["error"](translations.translate('jstree.errors.invalidName'));
// 										}
// 									},
// 								};
// 							};
// 							items.createItem = {
// 								label: translations.translate('jstree.contextmenu.nouveau'),
// 								icon: 'fa fa-plus-square',
// 								separator_after: true,
// 								submenu: {},
// 							};
// 							for(var i in valids) {
// 								// notifyer.console.debug('Test create on '+valids[i]+':', {
// 									// create_role: sitedata.get('entities_cruds')[valids[i]].jstree_actions.create,
// 									// granted: sitedata.userGranted(sitedata.get('entities_cruds')[valids[i]].jstree_actions.create),
// 								// });
// 								if(sitedata.userGranted(sitedata.get('entities_cruds')[valids[i]].jstree_actions.create)) {
// 									items.createItem.submenu['create_'+valids[i]] = getCreateSubmenu(valids[i]);
// 								}
// 							}
// 							if(Object.keys(items.createItem.submenu).length < 1) delete items.createItem;
// 						}
// 						// Rename
// 						if(sitedata.userGranted(node.original.cruds.jstree_actions.rename) && node.parents.length > 2) {
// 							items.renameItem = { // The "rename" menu item
// 								label: translations.translate('jstree.contextmenu.renommer'),
// 								icon: 'fa fa-pencil-square-o',
// 								action: function(obj) {
// 									Jstree.edit(node, null, function (new_node, result, canceled) {
// 										// callback
// 										notifyer.console.debug('Edited node:', {node: new_node, Result: result, Canceled: canceled});
// 										new_node.original.text = new_node.text;
// 									});
// 								},
// 							};
// 						}
// 						// Edit : copy / paste
// 						if(sitedata.userGranted(node.original.cruds.jstree_actions.copyable)) {
// 							items.copyableItem = { // The "edit" menu/submenu item
// 								label: translations.translate('jstree.contextmenu.copier'),
// 								icon: 'fa fa-clone',
// 								submenu: {
// 									copyItem: { // The "copy" menu item
// 										label: translations.translate('jstree.contextmenu.copier'),
// 										icon: 'fa fa-clone',
// 										action: function (obj) { $(obj).addClass("copy"); return { copyItem: this.copy(obj) }; },
// 									},
// 									pasteItem: { // The "paste" menu item
// 										label: translations.translate('jstree.contextmenu.coller'),
// 										icon: 'fa fa-external-link',
// 										action: function (obj) { $(obj).addClass("paste"); return { pasteItem: this.paste(obj) }; },
// 									},
// 								},
// 							};
// 						}
// 						// Delete
// 						// if is granted and can not delete directory having items / root can not be deleted
// 						if(sitedata.userGranted(node.original.cruds.jstree_actions.removable) && !node.children.length && node.parent !== "#" && node.parents.length > 2) {
// 							items.deleteItem = { // The "delete" menu item
// 								label: translations.translate('jstree.contextmenu.supprimer'),
// 								icon: 'fa fa-ban',
// 								action: function(obj) {
// 									// var response = confirm('Supprimer "' + node.text + '" ?');
// 									var response = confirm(translations.translate('jstree.actions.supprimer_name', {name: node.text}));
// 									if (true === response) Jstree.delete_node(node);
// 									else return false;
// 								},
// 							};
// 						}
// 						items.refreshItem = { // The "delete" menu item
// 							label: translations.translate('jstree.contextmenu.refresh'),
// 							icon: 'fa fa-refresh',
// 							action: function(obj) {
// 								Jstree.refresh_node(node);
// 							},
// 						};
// 					}
// 					return Object.keys(items).length ? items : null;
// 				};
// 				var destroyJstree = function() {
// 					if(!!$element.jstree(true)) {
// 						$element.jstree(true).destroy(false);
// 					}
// 				};
// 				var initJstree = function() {
// 					destroyJstree();
// 					$element.jstree({
// 						core: {
// 							check_callback: true,
// 							themes: {
// 								dir: true,
// 								stripes: true,
// 								ellipsis: true,
// 								dots: true,
// 							},
// 						 	data: function (node, callBack) {
// 								if(node.id === "#") {
// 									$http.get(getRootUrl($scope.jstreeLazyDirectory)).then(
// 										function success(response) {
// 											notifyer.console.debug('Jstree load '+node.id+'root:', response.data);
// 											callBack(response.data);
// 										},
// 										function error(response) {
// 											toastr["error"](translations.translate('errors.'+response.statusText));
// 										}
// 									);
// 								} else {
// 									notifyer.console.debug('Node:', node);
// 									$http.get(getUrl(node.id)).then(
// 										function success(response) {
// 											notifyer.console.debug('Jstree load '+node.id+':', response.data);
// 											callBack(response.data);
// 										},
// 										function error(response) {
// 											toastr["error"](translations.translate('errors.'+response.statusText));
// 										}
// 									);
// 								}
// 							},
// 						},
						// dnd: {
						// 	always_copy: false,
						// 	check_while_dragging: true,
						// 	copy: true,
						// 	drag_selection: true,
						// 	inside_pos: 0,
						// 	is_draggable: true,
						// 	large_drag_target: false,
						// 	large_drop_target: false,
						// 	open_timeout: 300,
						// 	touch: true,
						// 	use_html5: false,
						// },
// 						types: types,
// 						plugins: plugins,
// 						contextmenu: {
// 							items: contextmenu_items,
// 						},
// 					});
// 					var endRedraw = function(nodes) {
// 						$timeout(function() {
// 							$('body').find('.jstree_redraw > i.fa').removeClass('fa-spin');
// 							$('body').find('.jstree_reset > i.fa').removeClass('fa-spin');
// 						}, 300);
// 					}
// 					$element.on("redraw.jstree", function (nodes) {
// 						endRedraw(nodes);
// 					});
// 					$element.on("dnd_start.vakata", function (data, element, helper, event) {
// 						alert('See notifyer.console, please!');
// 						notifyer.console.debug('Drag start:', {data: data, element: element});
// 					});
// 					if($info.length) {
// 						$element.on('select_node.jstree', function (node, selected, event) {
// 							notifyer.console.debug('Selected node:', {node: node, selected: selected, event: event});
// 							$info.empty();
// 							addInfo(selected.node.original, $info);
// 						});
// 					}
// 				};
// 				$('body').on('click', '.jstree_redraw', function() {
// 				    $('> i.fa', $(this)).addClass('fa-spin');
// 				    $timeout(function() {
// 				        $element.jstree(true).redraw(true);
// 				    }, 0);
// 				});
// 				$('body').on('click', '.jstree_reset', function() {
// 					$('> i.fa', $(this)).addClass('fa-spin');
// 					$timeout(function() {
// 						$element.jstree(true).refresh();
// 						// destroyJstree();
// 						// initJstree();
// 					}, 0);
// 				});
// 				// init
// 				initJstree();
// 			}
// 		};
// 	}
// ]);
