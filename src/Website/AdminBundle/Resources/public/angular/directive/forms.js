
// Use switchery
// use :
// <input type="checkbox" data-switchery='{"size": "small", "color": "#3f89d6"}'>
app.directive("switchery", ['$timeout', function ($timeout) {
	return {
		restrict : "A",
		replace: false,
		scope: {
			switchery: "=",
		},
		controller: function ($scope, $element) {
			if(angular.isString($scope.switchery)) {
				$scope.switchery = angular.fromJson($scope.switchery);
			}
			$timeout(function () {
				var swit = new Switchery($element.get(0), $scope.switchery);
				$timeout(function () {
					$element.parent().find('>span.switchery').each(function() { $(this).addClass('m-r-sm') });
				});
			});
		},
	};
}]);

// Use select2
app.directive("selectChoice", ['$timeout',
	function ($timeout) {
		return {
			restrict: "C",
			link: function($scope, $element){
				$timeout(function() { $element.select2(); });
			},
		};
	}
]);

// collection-type / data-prototype
// https://symfony.com/doc/3.4/form/form_collections.html#allowing-new-tags-with-the-prototype
app.directive("collectionType", ['$timeout','$compile',
	function ($timeout, $compile) {
		return {
			restrict: "A",
			scope: {
				season: "@",
				prototype: "@",
			},
			link: function($scope, $element){
				var $addTagButton = $('<i class="fa fa-plus fa-fw"></i>');
				var $newLinkLi = $('<li class="list-group-item active text-center new-btn" style="cursor: pointer;"></li>').append($addTagButton);
				var globalIndex = 0;
				// var now = moment();
				// alert("Hello it's "+now.format("dddd, MMMM Do YYYY, h:mm:ss a"));

				var isSeason = function() {
					return $scope.season !== undefined;
				}

				var getMomentFromInput = function(e) {
					date = e.val().replace(/(^\d{1,2})\/(\d{1,2})\/(\d{2,4})$/, '$2/$1/$3');
					return moment(date);
				}
				var putMomentToInput = function(e, date) {
					e.val(date.format('DD/MM/YYYY'));
				}

				var addNewForm = function() {
					// $collectionHolder.find('li').css({'border': '1px solid gray'});
					var $lastLi = $collectionHolder.find('li.formli').last();
					// $lastLi.css({'border': "3px solid red"});
					// Get the data-prototype explained earlier
					var prototype = $scope.prototype;

					// var $newForm = prototype;
					// You need this only if you didn't set 'label' => false in your tags field in TaskType
					// Replace '__name__label__' in the prototype's HTML to
					// instead be a number based on how many items we have
					// $newForm = $newForm.replace(/__name__label__/g, index);

					// Replace '__name__' in the prototype's HTML to
					// instead be a number based on how many items we have
					var $newForm = $(prototype.replace(/__name__label__/g, globalIndex).replace(/__name__/g, globalIndex));

					// Display the form in the page in an li, before the "Add a tag" link li
					var $newFormLi = $('<li class="list-group-item formli"></li>').append($newForm);
					$newLinkLi.before($newFormLi);
					addNewFormDeleteLink($newFormLi);

					if(isSeason() && $lastLi.length) {
						// increment date
						// lastdate = $lastLi.find('input.date-picker').first().val();
						// if(angular.isString(lastdate)) {
							// lastdate = lastdate.replace(/(^\d{1,2})\/(\d{1,2})\/(\d{2,4})$/, '$2/$1/$3');
							// console.log('New date with', lastdate);
							// lastdate = moment(lastdate);
							lastdate = getMomentFromInput($lastLi.find('input.date-picker').first());
							lastdate.add(1, 'days');
							putMomentToInput($newFormLi.find('input.date-picker').first(), lastdate);
							// $newFormLi.find('input.date-picker').first().val(lastdate.format('DD/MM/YYYY'));
						// }

					}
					reorderIndexAndLabels();
					$compile($newFormLi)($scope);
					if(isSeason()) {
						var $date_input = $newFormLi.find('input.date-picker').first();
						if($date_input.length) {
							$date_input.blur(function(e) {
								var date = getMomentFromInput($date_input);
								$date_input.attr('title', date.format("dddd, MMMM Do YYYY"));
							});
						}
					}
					$newFormLi.data('index', globalIndex);
					globalIndex++;
					// console.debug('New index', globalIndex);
				};

				var addNewFormDeleteLink = function($formLi) {
					if(!$formLi.find('button.remove-btn').length) {
						var $removeFormButton = $('<button type="button" class="btn btn-xs btn-danger remove-btn">&nbsp;&nbsp;<i class="fa fa-ban fa-fw"></i>&nbsp;&nbsp;</button>');
						$formLi.append($removeFormButton);

						$removeFormButton.on('click', function(e) {
							// remove the li for the tag form
							$formLi.remove();
							reorderIndexAndLabels();
						});
					}
				};

				var reorderIndexAndLabels = function() {
					// 1. Reorder by dates
					if(isSeason()) {
						dates = [];
						var index = 1;
						$collectionHolder.find('>li').each(function(e) {
							var $input = $(this).find('input.date-picker');
							if($input.length && angular.isString($input.first().val())) dates[index++] = moment($input.first().val());
						});
						// console.log('Dates', dates);
					}
					// 2. Reorder labels
					index = 1;
					$collectionHolder.find('>li').each(function(e) {
						var $label = $(this).find('label').first();
						var $u = $label.find('u');
						if($u.length) $label = $u;
						$label.text(index);

						index++;
					});
				};

				var init = function() {
					// if(isSeason()) alert('Start new season! '+$scope.season);
					// var prototype = $scope.prototype;
					$collectionHolder = $('<ul class="list-group"></ul>');
					$element.append($collectionHolder);
					$collectionHolder.append($newLinkLi);

					// add existant elements
					$element.find('>div.form-group').each(function(e) {
						var $newFormLi = $('<li class="list-group-item"></li>').append($(this));
						if(!$newFormLi.hasClass('new-btn')) addNewFormDeleteLink($newFormLi);
						$newLinkLi.before($newFormLi);
						$newFormLi.data('index', globalIndex);
						globalIndex++;
					});
					reorderIndexAndLabels();
					$newLinkLi.on('click', function(e) {
						// add a new tag form (see next code block)
						addNewForm();
					});
				};

				init();
			},
		};
	}
]);


// Use summernote
// use :
// <textarea data-summernote-component>…</textarea>
// @see https://codemirror.net/doc/manual.html
app.directive("summernoteComponent", ['$timeout', function ($timeout) {
	return {
		restrict : "A",
		replace: false,
		link: function ($scope, $element, attr) {
			var $element = $element;
			$timeout(function() {
				$element.summernote({
					tabsize: 2,
					height: 140,			// set editor height
					minHeight: 120,			// set minimum height of editor
					maxHeight: 800,			// set maximum height of editor
					focus: false,			// set focus to editable area after initializing summernote
					callbacks: {
						onChange: function(contents, $editable) {
							$element.text(escape(contents));
							// console.debug('New content:', $element.text());
						}
					}
				});
			});
		},
	};
}]);

// Use codemirror
// use :
// <textarea data-codemirror-component>…</textarea>
app.directive("codemirrorComponent", ['$timeout', function ($timeout) {
	return {
		restrict : "A",
		replace: false,
		scope: {
			codemirrorComponent: '=',
		},
		link: function ($scope, $element, attr) {
			var $element = $element;
			// $element.addClass('cm-s-blackboard');
			// console.debug('CodeMirror options:', $scope.codemirrorComponent);
			var editor = CodeMirror.fromTextArea($element.get(0), $scope.codemirrorComponent);
			editor.setSize(null, 260);
			editor.on("change", function(cm, change) {
				$element.text(cm.getValue());
			});
		},
	};
}]);

// Use colorpicker
// use :
// <input colorpicker-component>
app.directive("colorpickerComponent", ['$timeout', function ($timeout) {
	return {
		restrict : "A",
		replace: false,
		controller: function ($scope, $element) {
			$timeout(function() {
				$element.colorpicker({
					colorSelectors: {
						'black': '#000000',
						'white': '#ffffff',
						'red': '#FF0000',
						'default': '#777777',
						'primary': '#337ab7',
						'success': '#5cb85c',
						'info': '#5bc0de',
						'warning': '#f0ad4e',
						'danger': '#d9534f'
					}
				});
			});
		},
	};
}]);

// use :
app.directive('dial', [function () {
	return {
		restrict: "C",
		replace: false,
		scope: {
			dialOptions: '=',
		},
		controller: function ($scope, $element) {
			$element.knob();
		}
	};
}]);

// use :
app.directive('dualListbox', [function () {
	return {
		restrict: "C",
		replace: false,
		scope: {
			dualListboxOptions: '=',
		},
		controller: function ($scope, $element) {
			var options = {
				nonSelectedListLabel: 'Options',
				selectedListLabel: 'Sélection',
				preserveSelectionOnMove: 'moved',
				moveOnSelect: true,
				nonSelectedFilter: '',
				selectedFilter: '',
				filterTextClear: 'Voir tout',
			};
			if(angular.isObject($scope.dualListboxOptions)) angular.extend(options, $scope.dualListboxOptions);
			$element.bootstrapDualListbox(options);
		}
	};
}]);

// use :
app.directive('rangeSlider', [function () {
	return {
		restrict: "C",
		replace: false,
		scope: {
			rangeSliderOptions: '=',
		},
		controller: function ($scope, $element) {
			var $input = $element.find('>input').last();
			var values = angular.fromJson($input.val());
			var options = {
				// skin: 'big',
				min: 4,
				max: 32,
				from: values[0],
				to: values[1],
				// step: 1,
				type: 'double',
				// prefix: "nb.",
				// maxPostfix: "+",
				// prettify_enabled: true,
				// hasGrid: true,
				// grid_num: 16,
				keyboard: true,
				onFinish: function(data) {
					// var value = ;
					// console.debug(typeof value + ' : ' + value);
					$input.val(angular.toJson([data.fromNumber,data.toNumber]));
				},
			};
			if(angular.isObject($scope.rangeSliderOptions)) angular.extend(options, $scope.rangeSliderOptions);
			$element.ionRangeSlider(options);
		}
	};
}]);

// use :
app.directive('datePicker', [function () {
	// https://bootstrap-datepicker.readthedocs.io/en/stable/
	// https://bootstrap-datepicker.readthedocs.io/en/stable/options.html#forceparse
	return {
		restrict: "C",
		replace: false,
		scope: {
			datepickerOptions: '=',
		},
		controller: function ($scope, $element) {
			var options = {
				format: 'dd/mm/yyyy',
				// format: 'DD d MM yyyy',
				showWeekDays: false,
				todayHighlight: true,
				todayBtn: false,
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true,
				zIndexOffset: 100,
			};
			// if(angular.isObject($scope.datepickerOptions)) angular.extend(options, $scope.datepickerOptions);
			$element.datepicker(options);
		}
	};
}]);







