// Set dot to green when angular is ready
// use :
// <span data-angular-ready><i class="fa fa-circle fa-fw text-danger"></i></span>
app.directive("angularReady", [function () {
	return {
		restrict : "A",
		replace: true,
		template : '<i class="fa fa-circle fa-fw text-info"></i>',
		// scope: true,
		controller: function ($scope, $element) {
			if(!!Tinycon) {
				Tinycon.setBubble("A");
				Tinycon.setOptions({ background: '#337ab7' });
			}
			$element.attr('title', 'Angular version '+angular.version.full);
		},
	};
}]);

app.directive("img", [function () {
	return {
		restrict : "E",
		replace: false,
		// scope: true,
		controller: function ($scope, $element) {
			if(!$element.hasClass('img-responsive')) $element.addClass('img-responsive');
		},
	};
}]);


