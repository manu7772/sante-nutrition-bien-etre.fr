
	// // MetisMenu -> https://github.com/onokumus/metismenu
	// var $metismenu = $('#side-menu');
	// if($metismenu.length) {
	//     var active = null;
	//     var actualhref = $metismenu.attr('data-actual-path');
	//     if(!!actualhref) {
	//         active = $metismenu.find('href=["'+actualhref+'"]').first();
	//         var $pli;
	//         if(active.length) {
	//             $pli = active.closest('li', $metismenu);
	//             if($pli.length) $pli.addClass('active');
	//         }
	//     }
	//     // and launch
	//     $('#side-menu').metisMenu();
	// }


// MetisMenu / https://github.com/onokumus/metismenu
// use :
// <ul class="nav metismenu" id="side-menu" data-side-menu data-actual-path="<URL>"><li>...</li>...
app.directive("sideMenu", [function () {
	return {
		restrict : "A",
		replace: false,
		controller: function ($scope, $element) {
			var $active = $element.find('>li:not(:first) a[href|="'+window.location.pathname+'"]').first();
			$active.parents('li').each(function(item) { $(this).addClass('active'); });
			// and launch
			$element.metisMenu();
		}
	};
}]);

// Create wait spin icon on click
// use :
// <button class="btn btn-white btn-sm woc woc-delay-0"><i class="fa fa-xxxx fa-fw"></i> Cliquez ici</button>
app.directive("filesize", ['localization', function (localization) {
	return {
		restrict : "A",
		replace: false,
		scope: {
			filesize: '<'
		},
		controller: function ($scope, $element) {
			$element.empty().text(localization.printFilesize($scope.filesize));
		}
	};
}]);

// Create wait spin icon on click
// use :
// <button class="btn btn-white btn-sm woc woc-delay-0"><i class="fa fa-xxxx fa-fw"></i> Cliquez ici</button>
app.directive("carousel", ['$timeout', function ($timeout) {
	return {
		restrict : "C",
		replace: false,
		controller: function ($scope, $element) {
			$timeout(function() {
				// alert('Carousel "'+$element.attr('id')+'" !!!');
				// Carousel($element);
			});
		}
	};
}]);

// Graph
// use :
// <div data-flot-line-chart="..." data-flotchart-data="..." data-flotchart-options="..."></div>
app.directive("flotLineChart", ['$http','$timeout','$compile', function ($http, $timeout, $compile) {
	return {
		restrict : "A",
		replace: false,
		scope: {
			flotLineChart: '<',
			flotchartData: '<',
			flotchartOptions: '<',
		},
		controller: function ($scope, $element) {
			$timeout(function() {
				// console.debug('Graph', $scope);
				if($.plot !== undefined) {
					var options = {
						series: {
							lines: {
								show: true,
								lineWidth: 1,
								fill: true,
								fillColor: {
									colors: [
										{opacity: 0.1},
										{opacity: 0.8},
									],
								},
							},
						},
						xaxis: {
							tickDecimals: 0,
						},
						yaxis: {
							tickDecimals: 2,
						},
						colors: [
							"red",
							"orange",
						],
						grid: {
							color: "#999999",
							hoverable: true,
							clickable: true,
							tickColor: "#D4D4D4",
							borderWidth: 0,
						},
						legend: {
							show: true,
							sorted: false,
							labelFormatter: function(label, series) {
								// console.debug('***** LABEL ' + label + ' :', series);
								total = 0;
								for (var index in series.data) {
									// console.debug('*** DATA', series.data[index]);
									if(angular.isArray(series.data[index])) total = total + series.data[index][1];
								}
								return label + ' <i>(' + total + ')</i>';
							},
						},
						tooltip: true,
						tooltipOpts: {
							content: "%y pages vues",
						},
					};

					// console.debug('Stats URL', $scope.flotLineChart);
					if(angular.isString($scope.flotLineChart)) {
						// get by api URL
						$http.post($scope.flotLineChart, JSON.stringify({})).then(
							function success(response) {
								// console.debug('***** STATS DATA', response.data.data);
								// console.debug('***** STATS OPTIONS', response.data.options);
								// options = angular.extend(options, response.data.options);
								angular.merge(options, response.data.options);
								$.plot($element, response.data.data, options);
							},
							function error(response) {
								// console.error('Loaded stats', response);
								$scope.message = 'errors.'+response.statusText;
								var $tmpl = $('<p><span translate="{[{ message  }]}"></span><br>Reload, please: <i class="fa fa-refresh fa-fw fa-1x" data-ng-class="{\'fa-spin\': loading}" data-ng-click="load()"></i></p>');
								$compile($tmpl)($scope);
								toastr["error"]($tmpl);
							}
						);
					} else if(angular.isArray($scope.flotchartData) && $scope.flotchartData.length) {
						// get by data in element
						$.plot($element, $scope.flotchartData, $scope.flotchartOptions);
					} else {
						$element.html('<h3 class="text-center text-danger">No data for PlotChart stats!</h3>');
					}
				} else {
					$element.html('<h3 class="text-center text-danger">Plugin PlotChart stats not loaded!</h3>');
				}
			});
		}
	};
}]);

