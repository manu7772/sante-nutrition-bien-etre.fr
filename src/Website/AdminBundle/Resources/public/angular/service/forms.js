app.service("forms", ['$http', '$q', 'sitedata', 'translations', 'notifyer',
	function ($http, $q, sitedata, translations, notifyer) {
		var parent = this;
		// var getFormUrl = undefined;
		// var default_method = 'patch';

		// var validParameters = function(shortname, id) {
		// 	return angular.isString(shortname) && angular.isNumber(id);
		// };

		// var setGetFormUrl = function(new_url) {
		// 	getFormUrl = !!new_url ? new_url : undefined;
		// };

		// var getGetFormUrl = function(shortname, id, method) {
		// 	return getFormUrl || sitedata.getPath('api', 'form/'+shortname+'s/'+id+'/'+(method || default_method));
		// };

		var formFn = function (data, $element) {
			var parent = this;
			var data = data;
			var $element = $element;
			var formdata_stored = angular.copy(data);

			var initialize = function(data, parent) {
				// add touched
				data.parent = parent || null;
				data.vars.touched = false;
				data.setTouched = function (touched) {
					data.vars.touched = !!touched; // default is true
					if(!!data.parent) data.parent.setTouched(data.vars.touched); // default is true
				};
				data.template = sitedata.getPath('websiteadmin', 'angular/templates/form/form-'+data.vars.original_type+'.html');
				// children
				if(Object.keys(data.children).length) {
					angular.forEach(data.children, function (child) {
						initialize(child, data);
					});
				}
			};

			var formdataToEntity = function (formdata, formname) {
				eval('var '+formname+' = {};');
				angular.forEach(formdata, function (idx) {
					var vars = idx.name.replace(/^[-\w]+\[/, '').replace(/]$/, '').split('][');
					// vars.pop(); // .pop() removes last element, unecessary!
					if(vars.length) {
						var i = 0;
						var names = '';
						while (extract = vars.shift()) {
							names += '["'+extract+'"]';
							eval('if(undefined === '+formname+names+') '+formname+names+' = {};');
						}
					}
				});
				// eval('notifyer.console.debug("----- SEND Form '+formname+':", '+formname+');');
				angular.forEach(formdata, function (idx) {
					idx.name = idx.name.replace(/\[([-\w]+)\]/g, '["$1"]');
					try {
						JSON.parse(idx.value);
					} catch(e) {
						// idx.value = '"'+idx.value+'"';
						idx.value = '"'+idx.value.replace(/\\?"/g,'\\"')+'"';
					}
					eval(idx.name+' = '+idx.value);
				});
				return eval(formname);
			};

			// FORM
			this.init = function() {
				parent.formdata = data;
				parent.method = data.vars.method;
				parent.action = data.vars.action;
				parent.shortname = data.vars.data.shortname.toLowerCase();
				parent.id = data.vars.data.id;
				parent.currentlocale = data.vars.data.currentlocale;
				initialize(data);
				return parent;
			};

			this.restore = function() {
				data = angular.copy(formdata_stored);
				parent.init();
			};

			this.setNewFormdata = function(new_data) {
				data = angular.copy(new_data);
				formdata_stored = angular.copy(data);
				parent.init();
			};

			this.init();

			this.inputFileChange = function(item) {
				var item = item;
				console.debug('Input file changed:', item);
				if($element.files.length) {
					var file = $element.files[0];
					if(file.constructor.name === 'File' && file.type !== undefined) {
						var registerFile = function () {
							if(false !== resize) {
								var Freader = new FileReader();
								Freader.onloadend = function (event) {
									// var donnees = event.target.result;
									//Traitez ici vos données binaires. Vous pouvez par exemple les envoyer à un autre niveau du framework avec $http ou $ressource
									var data = event.target.result;
									if(true === resize) {
										// resize file
									}
									// $textarea.text(data);
									item.data.binaryFile = data;
									item.data.fileName = file.name;
									item.data.fileSize = file.size;
									// parent.formdata.setTouched(true);
									// notifyer.console.debug('Input file val:', $textarea.text());
								};
								Freader.readAsDataURL(file);
								// Freader.readAsArrayBuffer(file);
								// Freader.readAsBinaryString(file);
							} else {
								// Abort
								// $textarea.text('');
							}
						};
						var maxlength = item.attr.maxlength || 20000000; // 20Mo
						var resize = null;
						if(file.size > maxlength) {
							notifyer.toastr.confirm('warning', 'Ce fichier est trop volumineux. Annulez ou acceptez de le réduire avant téléchargement.', null, 'Réduire').then(function resize() {
								resize = true;
								registerFile();
							}, function abord() {
								resize = false;
							});
						} else {
							registerFile();
						}
					}
				}
			};
			// set file by Input (not multiple)
			this.setFile = function(item) {
				var item = item;
				var $textarea = $('#'+item.id);
				// notifyer.console.debug('Load image maxlength (octets):', maxlength);
				var $inputImage = $textarea.closest('span.base64container').find('input[type="file"]').first();
				$inputImage.trigger('click');
			};

			// save
			this.submit_form = function () {
				defer = $q.defer();
				var $form = $element.find('form#'+parent.formdata.vars.id);
				var method = ($form.attr('method') || parent.method).toLowerCase(); // --> "patch" method by default
				var action = ($form.attr('action') || parent.action).toLowerCase(); // --> "patch" method by default
				var form_data = $form.serializeArray();
				// notifyer.console.debug('***** Get form data "'+'#'+parent.formdata.vars.id+'":', form_data);
				var send_data = formdataToEntity(form_data, parent.formdata.vars.id);
				notifyer.console.debug('***** Request '+method+' form data "'+'#'+parent.formdata.vars.id+'":', send_data);
				// submit with method
				$http[method](action, send_data).then(
					function success(response) {
						// toastr["success"](translations.translate('success.save'));
						try {
							// Form returned : valid or not
							response.return_type = response.data.vars.valid ? 'return_form_valid' : 'return_form_invalid';
							notifyer.console.error('*** FORM '+method.toUpperCase()+' response '+parent.shortname+'#'+parent.id+':', response);
						} catch {
							// Text returned (system error)
							if(angular.isString(response.data)) {
								response.return_type = 'return_system_error';
								notifyer.console.error('*** FORM '+method.toUpperCase()+' response '+parent.shortname+'#'+parent.id+':', response);
							} else {
								// Entity returned : OK
								response.return_type = 'return_entity';
								notifyer.console.debug('*** FORM '+method.toUpperCase()+' response '+parent.shortname+'#'+parent.id+':', response);
							}
						}
						defer.resolve(response);
					},
					function error(response) {
						switch (response.status) {
							case 405:
								notifyer.console.error('*** FORM ERROR '+method.toUpperCase()+' '+parent.shortname+'/'+parent.id+':', 'YOUR HAVE BEEN DISCONNECTED!!!');
								break;
							default:
								notifyer.console.error('*** FORM ERROR '+method.toUpperCase()+' '+parent.shortname+'/'+parent.id+':', response);
								break;
						}
						// toastr["error"](translations.translate('errors.'+response.statusText));
						defer.reject(response);
					}
				);
				return defer.promise;
			};
		};


		var loadUpdateForm = function (entity, action, $element, config) {
			var entity = entity;
			var action = action;
			var $element = $element;
			notifyer.console.debug('Load form for '+action+':', entity);
			defer = $q.defer();
			var id = entity.id;
			$http[entity.cruds.actions.api[action].ajx_method](entity.cruds.actions.api[action].url, config || {}).then(
				function success(response) {
					notifyer.console.debug('Loaded '+action+' form response:', response);
					if(response.status === 200) {
						defer.resolve(new formFn(response.data, $element));
					} else {
						if(response.status !== -1) notifyer.console.debug('Error while loading '+action+' form response:', response);
						defer.reject(response);
					}
				},
				function error(response) {
					if(response.status !== -1) notifyer.console.debug('Error while loading '+action+' form response:', response);
					defer.reject(response);
				}
			);
			return defer.promise;
		};

		var loadCreateForm = function (shortname, $element, config) {
			var shortname = shortname;
			var action = 'create';
			var $element = $element;
			notifyer.console.debug('Load form for '+action+':', shortname);
			defer = $q.defer();

			var cruds = sitedata.getCruds(shortname);
			var URL = cruds.actions.api.create;
			notifyer.console.debug('Load create form URL:', URL);
			$http[URL.ajx_method](URL.url, config || {}).then(
				function success(response) {
					notifyer.console.debug('Loaded '+action+' form response:', response);
					if(response.status === 200) {
						defer.resolve(new formFn(response.data, $element));
					} else {
						if(response.status !== -1) notifyer.console.debug('Error while loading '+action+' form response:', response);
						defer.reject(response);
					}
				},
				function error(response) {
					if(response.status !== -1) notifyer.console.debug('Error while loading '+action+' form response:', response);
					defer.reject(response);
				}
			);
			return defer.promise;
		};

		// service
		var service = {
			loadUpdateForm: function(entity, action, $element, config) { return loadUpdateForm(entity, action, $element, config); },
			loadCreateForm: function(shortname, $element, config) { return loadCreateForm(shortname, $element ,config); },
		};
		return service;
	}

]);



