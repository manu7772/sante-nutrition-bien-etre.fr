app.service('entitiesDescriptions', ['translations', 'sitedata', 'notifyer',
	function (translations, sitedata, notifyer) {
		var parent = this;

		this.getShowColumns = function(entity) {
			switch (entity.shortname.toLowerCase()) {
				case 'directory':
					var columns = {
						'name': {
							'class': 'text-left',
							// 'type': 'field-text',
						},
						'color': {
							'class': 'text-left',
							'type': 'field-color',
						},
					};
					break;
				case 'atelier':
					var columns = {
						'name': {
							'class': 'text-left',
							// 'type': 'field-text',
						},
						'menutitle': {
							'class': 'text-left',
							// 'type': 'field-text',
						},
						'accroche': {
							'class': 'text-justify',
							// 'type': 'field-text',
						},
						'description': {
							'class': 'text-justify',
							// 'type': 'field-ibox-height-scroll',
						},
						'alertmsg': {
							'class': 'text-justify',
							// 'type': 'field-ibox',
						},
						'weather': {
							'class': 'text-justify',
							// 'type': 'field-ibox',
						},
						'during': {
							'class': 'text-justify',
							// 'type': 'field-ibox',
						},
						'age': {
							'class': 'text-justify',
							// 'type': 'field-ibox',
						},
						'color': {
							'class': 'text-left',
							'type': 'field-color',
						},
					};
					break;
				case 'tag':
					var columns = {
						'name': {
							'class': 'text-left',
						},
					};
					break;
				case 'user':
					var columns = {
						'username': {
							'class': ['text-left','font-bold'],
						},
						'firstname': {
							'class': 'text-left',
						},
						'lastname': {
							'class': 'text-left',
						},
						'email': {
							'class': 'text-left',
						},
						'acceptCookies': {
							'class': 'text-left',
							'type': 'field-boolean',
						},
						'functionality': {
							'class': 'text-justify',
						},
						'locale': {
							'class': 'text-left',
							'type': 'field-locale',
						},
						'color': {
							'class': 'text-left',
							'type': 'field-color',
						},
						'owneddirectory': {
							'class': 'text-left',
							'type': 'field-jst-directory',
						},
					};
					break;
				case 'language':
					var columns = {
						'name': {
							'class': 'text-left',
						},
						'fullname': {
							'class': 'text-left',
						},
						'country': {
							'class': 'text-left',
						},
						'countryName': {
							'class': 'text-left',
						},
						'description': {
							'class': 'text-justify',
						},
						'color': {
							'class': 'text-left',
							'type': 'color',
						},
					};
					break;
				case 'entreprise':
					var columns = {
						'name': {
							'class': 'text-left',
						},
						'prefered': {
							'class': 'text-justify',
							'type': 'field-boolean',
						},
						'accroche': {
							'class': 'text-left',
							// 'type': 'field-text',
						},
						'description': {
							'class': 'text-left',
							// 'type': 'field-ibox-height-scroll',
						},
						'googlecode': {
							'class': 'text-left',
							// 'type': 'field-ibox-height-scroll',
						},
						'color': {
							'class': 'text-left',
							'type': 'color',
						},
					};
					break;
				case 'historic':
					var columns = {
						'username': {
							'class': 'text-left',
						},
					};
					break;
				case 'labolog':
					var columns = {
						'username': {
							'class': 'text-left',
						},
					};
					break;
				case 'localtext':
					var columns = {
						'texts': {
							'class': 'text-left',
						},
					};
					break;
				default:
					var columns = {
						'id': {
							'class': 'text-left',
						},
					};
					break;
			}
			var verifiedCols = {};
			angular.forEach(columns, function (parameters, fieldname) {
				if(undefined !== entity[fieldname]) verifiedCols[fieldname] = parameters || null;
			});
			return verifiedCols;
		};

		this.getDatatablesColumns = function(shortname, options) {
			var options = options || {};
			var shortname = shortname.toLowerCase();
			options.actions = options.actions || true;
			var computeAndReorderActions = function(actions, order) {
				// Order :
				// array => set only elements of order
				// true => all default elements + not listed (defaults) elements of actions at the end
				// false => all defaults elements only
				var default_order = ['show','update','disable','enable','softdelete','unsoftdelete','delete'];
				var order_items = angular.isArray(order) ? order : default_order;
				if(true === order) angular.forEach(Object.keys(actions), function (key) {
					if(!order_items.includes(key)) order_items.push(key);
				});
				var new_order = {};
				angular.forEach(order_items, function (name) {
					if(undefined === new_order[name] && angular.isObject(actions[name])) {
						new_order[name] = angular.copy(actions[name]);
						// Add full URL
						new_order[name].url = sitedata.getPath('root', new_order[name].path);
					}
				});
				return new_order;
			};
			var getActionButtons = function (data, meta, actions) {
				if((!angular.isArray(actions) && true !== actions) || !data.api) return null;
				var $buttons = $('<div class="btn-group need-compile" role="group" aria-label="actions"></div>');
				var icon = undefined;
				var btn_class = undefined;
				data.api = computeAndReorderActions(data.api, actions);
				angular.forEach(data.api, function (url, action) {
					if(angular.isObject(url) && (!!actions[action] || true === actions)) {
						switch (action) {
							case 'show': icon = 'fa-eye'; btn_class = 'btn-white woc woc-delay-2000'; break;
							case 'update': icon = 'fa-pencil'; btn_class = 'btn-white woc woc-delay-0'; break;
							case 'disable': icon = 'fa-unlock'; btn_class = 'btn-white woc woc-delay-0'; break;
							case 'enable': icon = 'fa-lock'; btn_class = 'btn-warning woc woc-delay-0'; break;
							case 'softdelete': icon = 'fa-trash-o'; btn_class = 'btn-white woc woc-delay-0'; break;
							case 'unsoftdelete': icon = 'fa-trash'; btn_class = 'btn-warning woc woc-delay-0'; break;
							case 'delete': icon = 'fa-times'; btn_class = 'text-danger btn-white woc woc-delay-0'; break;
							default: icon = undefined; btn_class = undefined; break;
						}
						if(!!icon) {
							// notifyer.console.debug('- URL '+action+':', angular.toJson(url));
							// url = angular.toJson(url);
							$buttons.append($('<a data-ng-click="dtAction(\''+action+'\', \''+meta.row+'\')" role="button" class="btn btn-xs '+btn_class+'" title="'+action+'"><i class="fa '+icon+' fa-fw"></i></a>'));
						}
					}
				});
				return $('<div></div>').append($buttons).html();
			};
			switch (shortname) {
				case 'user':
					return {
						ajax: sitedata.getPath('api', 'DT/'+shortname+'s', {locale: translations.currentLocale()}),
						columns: [
							{ name: translations.translate('table.thead.id'), data: 'id', className: 'text-center', exportColumn: true },
							{
								name: translations.translate('table.thead.actions'),
								data: 'cruds',
								orderable: false,
								searchable: false,
								render: function(data, type, full, meta){
									if(type === 'display') return getActionButtons(data.actions, meta, options.actions);
									if(type === 'filter') return full.id;
									return null;
								},
							},
							{
								name: translations.translate('entities.user.fields.avatar'),
								data: 'avatar',
								orderable: false,
								searchable: false,
								className: 'text-center',
								render: function(data, type, full, meta){
									if(type === 'display') {
										// notifyer.console.debug('- User '+full.username+':', full);
										if(!!data && angular.isObject(data.fileUrls) && !!data.fileUrls.thumbnails_128x128) return '<img src="'+sitedata.getPath('web', data.fileUrls.thumbnails_128x128)+'" class="img-rounded" style="height:32px;">';
											else return '<i class="fa '+full.icon+' fa-fw" style="color:'+full.color+';"></i>';
									}
									if(type === 'filter') return full.id;
									return data;
								},
							},
							{ name: 'Utilisateur', data: 'username', defaultContent: '?', exportColumn: true },
							{ name: 'Nom', data: 'firstname', defaultContent: '?', exportColumn: true },
							{ name: 'Prénom', data: 'lastname', defaultContent: '?', exportColumn: true },
							{ name: 'Email', data: 'email', exportColumn: true },
							// {
							// 	data: 'roles',
							// 	render: function(data, type, full, meta){
							// 		if(angular.isObject(data) && type === 'display') {
							// 			var result = [];
							// 			angular.forEach(data, function (value, key) {
							// 				result.push(value);
							// 			});
							// 			return result.join(', ');
							// 		}
							// 		return data;
							// 	},
							// },
							{
								name: 'Langue',
								data: 'locale',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										return full.locale !== full.currentlocale ? translations.translate('languages.'+full.locale)+'<br><i>['+translations.translate('languages.'+full.currentlocale)+']</i>' : translations.translate('languages.'+full.locale);
									}
									return data;
								},
							},
							{
								name: 'Dossier',
								data: 'owneddirectory',
								className: 'text-left',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										// if(!!data) notifyer.console.debug('- User '+full.name+' folder:', data);
										return !!data ? '<i class="fa '+data.icon+' fa-fw" style="color:'+data.color+';"></i> '+data.name : '<i class="fa fa-times fa-fw text-muted"></i>';
									}
									return data;
								},
							},
							{
								name: 'Connexion',
								data: 'lastLogin',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column lastLogin:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var lastLog = moment(data);
											data = lastLog.isValid() ? lastLog.format('L') : 'Erreur';
										}
										if(type === 'filter') {
											var lastLog = moment(data);
											data = lastLog.isValid() ? lastLog.format('X') : 'Erreur'; // As timestamp for sorting
										}
									}
									return data;
								},
							},
						],
					};
					break;
				case 'atelier':
					return {
						ajax: sitedata.getPath('api', 'DT/'+shortname+'s', {locale: translations.currentLocale()}),
						columns: [
							{ name: translations.translate('table.thead.id'), data: 'id', className: 'text-center', exportColumn: true },
							{
								name: translations.translate('entities.atelier.fields.thumbnail'),
								data: 'thumbnail',
								orderable: false,
								searchable: false,
								className: 'text-center',
								render: function(data, type, full, meta){
									if(!!data) {
										if(type === 'display' && angular.isString(data.fileUrl)) return '<img src="'+sitedata.getPath('web', data.fileUrl)+'" class="img-circle" style="height:32px;">';
										if(type === 'filter') return full.id;
									}
									return data;
								},
							},
							{
								name: translations.translate('table.thead.actions'),
								data: 'cruds',
								orderable: false,
								searchable: false,
								render: function(data, type, full, meta){
									if(type === 'display') return getActionButtons(data.actions, meta, options.actions);
									if(type === 'filter') return full.id;
									return null;
								},
							},
							{
								name: 'Nom',
								data: 'name',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										// notifyer.console.debug('- Atelier '+full.name+':', full);
										// notifyer.console.debug('Column:', {data: data, type: type, full: full, meta: meta});
										if(undefined !== full.color) {
											color = !!full.color ? full.color : 'transparent';
											return '<i class="fa fa-paw fa-fw" style="color: '+color+';"></i> '+data;
										}
									}
									return data;
								},
							},
							{
								name: 'Langue',
								data: 'locale',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										return full.locale !== full.currentlocale ? translations.translate('languages.'+full.locale)+'<br><i>['+translations.translate('languages.'+full.currentlocale)+']</i>' : translations.translate('languages.'+full.locale);
									}
									return data;
								},
							},
							{
								name: translations.translate('Modification'),
								data: 'updated',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column updated:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var updated = moment(data);
											data = updated.isValid() ? updated.format('L') : 'Erreur';
										}
										if(type === 'filter') {
											var updated = moment(data);
											data = updated.isValid() ? updated.format('X') : 'Erreur'; // As timestamp for sorting
										}
									}
									return data;
								},
							},
							{
								name: translations.translate('Création'),
								data: 'created',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column created:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var created = moment(data);
											data = created.isValid() ? created.format('L') : 'Erreur';
										}
										if(type === 'filter') {
											var created = moment(data);
											data = created.isValid() ? created.format('X') : 'Erreur'; // As timestamp for sorting
										}
									}
									return data;
								},
							},
						],
					};
					break;
				case 'tag':
					return {
						ajax: sitedata.getPath('api', 'DT/'+shortname+'s', {locale: translations.currentLocale()}),
						columns: [
							{ name: translations.translate('table.thead.id'), data: 'id', className: 'text-center', exportColumn: true },
							{
								name: translations.translate('table.thead.actions'),
								data: 'cruds',
								orderable: false,
								searchable: false,
								render: function(data, type, full, meta){
									if(type === 'display') return getActionButtons(data.actions, meta, options.actions);
									if(type === 'filter') return full.id;
									return null;
								},
							},
							{
								name: 'Nom',
								data: 'name',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										// notifyer.console.debug('- Tag '+full.name+':', full);
										// notifyer.console.debug('Column:', {data: data, type: type, full: full, meta: meta});
										if(undefined !== full.color) {
											color = !!full.color ? full.color : 'transparent';
											return '<i class="fa fa-paw fa-fw" style="color: '+color+';"></i> '+data;
										}
									}
									return data;
								},
							},
							{
								name: 'Langue',
								data: 'locale',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										return full.locale !== full.currentlocale ? translations.translate('languages.'+full.locale)+'<br><i>['+translations.translate('languages.'+full.currentlocale)+']</i>' : translations.translate('languages.'+full.locale);
									}
									return data;
								},
							},
							{
								name: translations.translate('Modification'),
								data: 'updated',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column updated:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var updated = moment(data);
											data = updated.isValid() ? updated.format('L') : 'Erreur';
										}
										if(type === 'filter') {
											var updated = moment(data);
											data = updated.isValid() ? updated.format('X') : 'Erreur'; // As timestamp for sorting
										}
									}
									return data;
								},
							},
							{
								name: translations.translate('Création'),
								data: 'created',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column created:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var created = moment(data);
											data = created.isValid() ? created.format('L') : 'Erreur';
										}
										if(type === 'filter') {
											var created = moment(data);
											data = created.isValid() ? created.format('X') : 'Erreur'; // As timestamp for sorting
										}
									}
									return data;
								},
							},
						],
					};
					break;
				case 'entreprise':
					return {
						ajax: sitedata.getPath('api', 'DT/'+shortname+'s', {locale: translations.currentLocale()}),
						columns: [
							{ name: translations.translate('table.thead.id'), data: 'id', className: 'text-center', exportColumn: true },
							{
								name: translations.translate('table.thead.actions'),
								data: 'cruds',
								orderable: false,
								searchable: false,
								render: function(data, type, full, meta){
									if(type === 'display') return getActionButtons(data.actions, meta, options.actions);
									if(type === 'filter') return full.id;
									return null;
								},
							},
							{
								name: 'Nom',
								data: 'name',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										// notifyer.console.debug('- Tag '+full.name+':', full);
										// notifyer.console.debug('Column:', {data: data, type: type, full: full, meta: meta});
										if(undefined !== full.color) {
											color = !!full.color ? full.color : 'transparent';
											return '<i class="fa fa-paw fa-fw" style="color: '+color+';"></i> '+data;
										}
									}
									return data;
								},
							},
							{
								name: 'Langue',
								data: 'locale',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										return full.locale !== full.currentlocale ? translations.translate('languages.'+full.locale)+'<br><i>['+translations.translate('languages.'+full.currentlocale)+']</i>' : translations.translate('languages.'+full.locale);
									}
									return data;
								},
							},
							{
								name: 'Défaut',
								data: 'prefered',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') return !!data ? '<i class="fa fa-circle text-success"></i>' : '<i class="fa fa-circle text-muted"></i>';
									if(type === 'filter') return !!data ? 1 : 0;
									return null;
								},
							},
							{
								name: translations.translate('Modification'),
								data: 'updated',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column updated:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var updated = moment(data);
											data = updated.isValid() ? updated.format('L') : 'Erreur';
										}
										if(type === 'filter') {
											var updated = moment(data);
											data = updated.isValid() ? updated.format('X') : 'Erreur'; // As timestamp for sorting
										}
									}
									return data;
								},
							},
							{
								name: translations.translate('Création'),
								data: 'created',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column created:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var created = moment(data);
											data = created.isValid() ? created.format('L') : 'Erreur';
										}
										if(type === 'filter') {
											var created = moment(data);
											data = created.isValid() ? created.format('X') : 'Erreur'; // As timestamp for sorting
										}
									}
									return data;
								},
							},
						],
					};
					break;
				case 'directory':
					return {
						ajax: sitedata.getPath('api', 'DT/'+shortname+'s', {locale: translations.currentLocale()}),
						columns: [
							{ name: translations.translate('table.thead.id'), data: 'id', className: 'text-center', exportColumn: true },
							{
								name: translations.translate('table.thead.actions'),
								data: 'cruds',
								orderable: false,
								searchable: false,
								render: function(data, type, full, meta){
									if(type === 'display') return getActionButtons(data.actions, meta, options.actions);
									if(type === 'filter') return full.id;
									return null;
								},
							},
							{
								name: 'Nom',
								data: 'name',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										// notifyer.console.debug('Column:', {data: data, type: type, full: full, meta: meta});
										if(undefined !== full.color) {
											color = !!full.color ? full.color : '#CCCCCC';
											return '<i class="fa fa-folder fa-fw" style="color: '+color+';"></i> '+data;
										}
									}
									return data;
								},
							},
							{
								name: 'Type',
								data: 'dirinterface',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										// notifyer.console.debug('Column:', {data: data, type: type, full: full, meta: meta});
										switch(data) {
											case 'proprietary':
												return '<span class="text-success"><i class="fa fa-lock fa-fw"></i> '+data+'</span>';
												break;
											case 'public':
												return '<span class="text-info"><i class="fa fa-unlock-alt fa-fw"></i> '+data+'</span>';
												break;
											case 'trash':
												return '<em class="text-muted"><i class="fa fa-trash fa-fw"></i> '+data+'</em>';
												break;
											case 'system':
												return '<span class="text-danger"><i class="fa fa-minus-circle fa-fw"></i> '+data+'</span>';
												break;
										}
									}
									return data;
								},
							},
							// {
							// 	name: 'Langue',
							// 	data: 'locale',
							// 	className: 'text-center',
							// 	exportColumn: true,
							// 	render: function(data, type, full, meta){
							// 		if(type === 'display') {
							// 			return full.locale !== full.currentlocale ? translations.translate('languages.'+full.locale)+'<br><i>['+translations.translate('languages.'+full.currentlocale)+']</i>' : translations.translate('languages.'+full.locale);
							// 		}
							// 		return data;
							// 	},
							// },
							{
								name: 'Level/Parent/Root',
								data: 'level',
								className: 'text-left',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										return data+' / '+(!!full.directory ? full.directory.name : '<i class="fa fa-times fa-fw text-muted"></i>')+' / '+(!!full.rootDirectory ? full.rootDirectory.name : '<i class="fa fa-times fa-fw text-muted"></i>');
									}
									return data;
								},
							},
							{
								name: 'Propriétaire',
								data: 'owner',
								className: 'text-left',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										// if(type === 'type') notifyer.console.debug('*** Column FULL:', full);
										if(!!data) return '<i class="fa '+data.icon+' fa-fw" style="color:'+data.color+';"></i> '+data.name;
										return '<i class="fa fa-times fa-fw text-muted"></i>';
									}
									return data;
								},
							},
						],
					};
					break;
				case 'localtext':
					return {
						ajax: sitedata.getPath('api', 'DT/'+shortname+'s', {locale: translations.currentLocale()}),
						columns: [
							{ name: translations.translate('table.thead.id'), data: 'id', className: 'text-center', exportColumn: true },
							{
								name: translations.translate('table.thead.actions'),
								data: 'cruds',
								orderable: false,
								searchable: false,
								render: function(data, type, full, meta){
									// if(type === 'display') notifyer.console.debug('Localtext:', {data: data, full: full});
									if(type === 'display') return getActionButtons(data.actions, meta, options.actions);
									if(type === 'filter') return full.id;
									return null;
								},
							},
							{
								name: translations.translate('entities.'+shortname+'.fields.text'),
								data: 'text',
								exportColumn: true,
								render:  function(data, type, full, meta){
									if(!!data) {
										if(type === 'display') {
											data = data.replace(/&nbsp;/ig," ");
											data = data.replace(/(<([^>]+)>)/ig,"");
											data = data.substring(0,50)+(data.length > 50 ? '…' : '');
										}
									}
									return data;
								},
							},
							{
								name: translations.translate('entities.'+shortname+'.fields.locales'),
								data: 'locales',
								orderable: false,
								exportColumn: true,
								render: function(data, type, full, meta){
									if(angular.isObject(data) && type === 'display') {
										var result = '';
										angular.forEach(data, function (value) {
											var isnotnull = !!full.texts[value] ? full.texts[value].toString().replace(/(<([^>]+)>)/ig,"").replace(/\s/ig,"").length > 0 : false;
											var state = isnotnull ? 'default' : 'danger';
											if(isnotnull) angular.forEach(full.texts, function (text, locale) {
												if(locale !== value && text === full.texts[value]+'') { state = 'warning'; }
											});
											result += '<span class="label label-'+state+' m-r-xs" title="'+translations.translate('languages.'+value)+'">'+value.split('-')[0]+'</span>';
										});
										return result;
									}
									return data;
								},
							},
							{
								name: translations.translate('Modification'),
								data: 'updated',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column updated:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var updated = moment(data);
											data = updated.isValid() ? updated.format('L') : 'Erreur';
										}
										if(type === 'filter') {
											var updated = moment(data);
											data = updated.isValid() ? updated.format('X') : 'Erreur'; // As timestamp for sorting
										}
									}
									return data;
								},
							},
							{
								name: translations.translate('Création'),
								data: 'created',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column created:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var created = moment(data);
											data = created.isValid() ? created.format('L') : 'Erreur';
										}
										if(type === 'filter') {
											var created = moment(data);
											data = created.isValid() ? created.format('X') : 'Erreur'; // As timestamp for sorting
										}
									}
									return data;
								},
							},
						],
					};
					break;
				case 'language':
					return {
						ajax: sitedata.getPath('api', 'DT/'+shortname+'s', {locale: translations.currentLocale()}),
						columns: [
							{ name: translations.translate('table.thead.id'), data: 'id', className: 'text-center', exportColumn: true },
							{
								name: translations.translate('table.thead.actions'),
								data: 'cruds',
								orderable: false,
								searchable: false,
								render: function(data, type, full, meta){
									// if(type === 'type') notifyer.console.debug('*** Column FULL:', full);
									if(type === 'display') return getActionButtons(data.actions, meta, options.actions);
									if(type === 'filter') return full.id;
									return null;
								},
							},
							{
								name: 'Langue',
								data: 'name',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') {
										if(!!full.color) {
											return data+' <i class="fa fa-flag fa-fw" style="color: '+full.color+';"></i>';
										}
									}
									return data;
								},
							},
							{ name: 'Nom ISO', data: 'fullname', className: 'text-center', exportColumn: true },
							// { name: 'Pays ISO', data: 'country', className: 'text-center', exportColumn: true },
							{ name: 'Nom pays', data: 'countryName', exportColumn: true },
							{
								name: 'Défaut',
								data: 'prefered',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') return !!data ? '<i class="fa fa-circle text-success"></i>' : '<i class="fa fa-circle text-muted"></i>';
									if(type === 'filter') return !!data ? 1 : 0;
									return null;
								},
							},
							{
								name: translations.translate('entities.language.fields.thumbnail'),
								data: 'thumbnail',
								orderable: false,
								searchable: false,
								className: 'text-center',
								render: function(data, type, full, meta){
									if(!!data) {
										if(type === 'display' && angular.isString(data.fileUrl)) return '<img src="'+sitedata.getPath('web', data.fileUrl)+'" style="height:22px;">';
										if(type === 'filter') return full.id;
									}
									return data;
								},
							},
						],
					};
					break;
				case 'labolog':
					return {
						ajax: sitedata.getPath('api', 'DT/'+shortname+'s', {locale: translations.currentLocale()}),
						pageLength: 100,
						buttons: [],
						order: [[ 0, 'desc' ]],
						createdRow: function(row, data, dataIndex) {
							// notifyer.console.debug('Row:', {data: data, row: row, dataIndex: dataIndex});
							if(!!data.responseInfo) {
								$(row).addClass(data.responseInfo.type != "exception" ? '' : 'danger');
							}
						},
						columns: [
							{ name: translations.translate('table.thead.id'), data: 'id', className: 'text-center', exportColumn: true },
							{
								name: translations.translate('table.thead.actions'),
								data: 'cruds',
								orderable: false,
								searchable: false,
								render: function(data, type, full, meta){
									if(type === 'display') return '<div class="btn-group need-compile" role="group" aria-label="actions"><a href="#" data-ng-click="show(\''+shortname+'\', '+full.id+')" role="button" class="btn btn-xs btn-default"><i class="fa fa-eye fa-fw"></i></a></div>';
									// if(type === 'filter') return full.id;
									return full.id;
								},
							},
							{ name: 'Nom utilisateur', data: 'username', exportColumn: true },
							{
								name: 'IP',
								data: 'requestInfo',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') return $filter('defaultfield')(data.ClientIp);
									// if(type === 'filter') return !!data ? data.ClientIp : 0;
									return data.ClientIp || 0;
								},
							},
							{
								name: 'Method',
								data: 'requestInfo',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') return $filter('defaultfield')(data.Method);
									// if(type === 'filter') return data.Method;
									return data.Method;
								},
							},
							{
								name: 'Response',
								data: 'responseInfo',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									if(type === 'display') return data.type != 'exception' ? '<i class="fa fa-check text-info"></i>' : '<i class="fa fa-times text-danger"></i>';
									// if(type === 'filter') return data.type;
									return $filter('defaultfield')(data.type);
								},
							},
							{
								name: translations.translate('Start log'),
								data: 'created',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column created:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var created = moment(data);
											data = created.isValid() ? created.format('L LTS') : 'Erreur';
										}
										if(type === 'filter') {
											var created = moment(data);
											data = created.isValid() ? created.format('X') : 'Erreur'; // As timestamp for sorting
										}
									} else {
										data = '<i class="fa fa-times text-muted"></i>';
									}
									return data;
								},
							},
							{
								name: translations.translate('End log'),
								data: 'ended',
								className: 'text-center',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column created:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											var created = moment(data);
											data = created.isValid() ? created.format('L LTS') : 'Erreur';
										}
										if(type === 'filter') {
											var created = moment(data);
											data = created.isValid() ? created.format('X') : 'Erreur'; // As timestamp for sorting
										}
									} else {
										data = '<i class="fa fa-times text-muted"></i>';
									}
									return data;
								},
							},
							{
								name: translations.translate('Timing'),
								data: 'created',
								className: 'text-right',
								exportColumn: true,
								render: function(data, type, full, meta){
									// notifyer.console.debug('Column created:', {data: data, type: type, full: full, meta: meta});
									if(!!data) {
										if(type === 'display') {
											if(!full.ended) {
												data = '<i class="fa fa-times text-muted"></i>';
											} else {
												var created = moment(data);
												var ended = moment(full.ended);
												if(created.isValid() && ended.isValid()) {
													var timing = moment.duration(ended.diff(created));
													// data = timing.seconds()+'.'+timing.milliseconds();
													data = timing.asMilliseconds()+' ms';
													// data = timing.asSeconds();
												} else {
													data = 'Erreur';
												}
											}
										}
										else 
										// if(type === 'filter')
										{
											if(!full.ended) {
												data = 0;
											} else {
												var created = moment(data);
												var ended = moment(full.ended);
												if(created.isValid() && ended.isValid()) {
													var timing = moment.duration(ended.diff(created));
													data = timing.asMilliseconds()
												} else {
													data = 0;
												}
											}
										}
									} else {
										data = '<i class="fa fa-times text-muted"></i>';
									}
									return data;
								},
							},
						],
					};
					break;
				default:
					return {
						ajax: sitedata.getPath('api', 'DT/'+shortname+'s', {locale: translations.currentLocale()}),
						columns: [
							{ name: translations.translate('table.thead.id'), data: 'id', className: 'text-center' },
						],
					};
			}
		};


	}

]);






