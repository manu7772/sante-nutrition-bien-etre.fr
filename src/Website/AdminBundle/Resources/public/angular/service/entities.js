// app.service("entities", ['$rootScope','$http', '$q', '$templateRequest', '$compile', '$timeout', '$interval', 'translations', 'sitedata', 'entitiesDescriptions', 'forms', 'dateFilter', 'notifyer', 'modals',
// 	function ($rootScope, $http, $q, $templateRequest, $compile, $timeout, $interval, translations, sitedata, entitiesDescriptions, forms, dateFilter, notifyer, modals) {
// 		var parent = this;

// 		// var $scope = $rootScope.$new();
// 		// $scope.entity_current_language = translations.currentLocale();
// 		// $scope.loading = false;
// 		// $scope.errorload = false;
// 		// this.getScope = function() { return $scope; };

// 		// this.compile = function($element) {
// 		// 	$compile($element)($scope);
// 		// };

// 		this.extractEntityFromForm = function(form) {
// 			var entity = {};
// 			angular.forEach(form.children, function (value, key) {
// 				entity[key] = angular.copy(value.vars.value);
// 			});
// 			return entity;
// 		};

// 		this.getEntityLocales = function(entity) {
// 			$scope.entity_current_language;
// 			var languages = {};
// 			angular.forEach(entity, function (field, fieldname) {
// 				if(field.shortname === 'localtext') {
// 					$scope.entity_current_language = field.currentlocale;
// 					angular.forEach(field.texts, function (text, locale) {
// 						languages[locale] = locale;
// 					});
// 				}
// 			});
// 			if(!!entity.currentlocale) {
// 				$scope.entity_current_language = entity.currentlocale;
// 			}
// 			return languages;
// 		};

// 		var loadEntity = function (shortname, id) {
// 			var shortname = shortname;
// 			var id = id;
// 			var URL = sitedata.getPath('api', shortname+'s/'+id);
// 			// notifyer.console.debug('Load entity url:', URL);
// 			// $scope.errorload = false;
// 			// $scope.loading = true;
// 			var promise = $http({
// 				url: URL,
// 				method: 'get',
// 			});
// 			promise.then(
// 				function (response) {
// 					// $scope.loading = false;
// 					if(response.status === 200) {
// 						// $scope.errorload = false;
// 					} else {
// 						// $scope.errorload = true;
// 						notifyer.toastr.error('errors.'+response.statusText);
// 					}
// 				},
// 				function (response) {
// 					// $scope.errorload = true;
// 					// $scope.loading = false;
// 					notifyer.console.error('ERROR GET '+shortname+':', response);
// 					notifyer.toastr.error('errors.'+response.statusText);
// 				}
// 			);
// 			return promise;
// 		};

// 		this.createEntity = function (shortname) {
// 			// notifyer.toastr.info('Création : '+shortname);
// 			var $create_scope = $rootScope.$new();
// 			$create_scope.shortname = shortname.toLowerCase();
// 			$create_scope.form = null;
// 			$create_scope.entity_current_language = translations.currentLocale();
// 			$create_scope.loading = false;
// 			$create_scope.errorload = false;
// 			var $modal = null;
// 			var cruds = sitedata.getCruds(shortname);

// 			$create_scope.save = function() {
// 				if(!!$create_scope.form) {
// 					// save form data
// 					$create_scope.form.submit_form();
// 				} else {
// 					notifyer.toastr.warning('errors.no_data_for_submit');
// 				}
// 			};
// 			closeAll = function () {
// 				$timeout(function () {
// 					if(!!$modal) $modal.modal('hide');
// 				}, 500);
// 				$timeout(function () {
// 					$create_scope.errorload = false;
// 					$create_scope.loading = false;
// 				}, 1500);
// 			};
// 			justEndLoad = function () {
// 				$create_scope.loading = false;
// 				$create_scope.errorload = false;
// 			};

// 			var lauchModale = function() {
// 				if(!!$modal) {
// 					$modal.on('hidden.bs.modal', function (event) { lauchModale(); });
// 					closeAll();
// 				} else {
// 					var templateUrl = sitedata.getPath('websiteadmin', 'angular/templates/modal/modal-'+$create_scope.shortname+'-create.html');
// 					$create_scope.loading = true;
// 					$templateRequest(templateUrl).then(
// 						function success(template) {
// 							$modal = $(template);
// 							$('#modals').empty().append($modal);
// 							$compile($modal)($create_scope);
// 							$modal.modal({backdrop: 'static'});
// 							$modal.on('hidden.bs.modal', function (event) {
// 								delete $modal;
// 								$modal = null;
// 								justEndLoad();
// 							});
// 							forms.loadCreateForm($create_scope.shortname, 'create', $modal).then(
// 								function success(response) {
// 									$create_scope.form = response;
// 									justEndLoad();
// 								},
// 								function error(response) {
// 									$create_scope.errorload = true;
// 									notifyer.toastr.error('errors.'+response.statusText).then(function onClose() { closeAll(); });
// 								}
// 							);
// 						},
// 						function error(response) {
// 							$create_scope.errorload = true;
// 							notifyer.toastr.error('errors.creator');
// 						}
// 					);
// 				}
// 			};
// 			lauchModale();
// 			// notifyer.toastr.info('Création : '+shortname);
// 		};



// 		//********************************************
// 		// DATATABLE
// 		//********************************************

// 		this.generateNewTable = function (shortname, options, $element) {

// 			var table = {
// 				table: null,
// 				$table_scope: $rootScope.$new(),
// 				$modal: null,
// 				exportColumns: [],
// 				reload_tempo: sitedata.isDev() ? 15000 : 10000,
// 				reload: null,
// 				redrawn: false,
// 				XHRcanceler: {std_group: null},
// 				resetXHR: function (group) {
// 					var group = !!group ? group : 'std_group';
// 					if(!!table.XHRcanceler[group]) {
// 						table.XHRcanceler[group].resolve();
// 						notifyer.console.warn('ABORTING AJAX Request (Group: "'+group+'")…');
// 					}
// 					table.closeXHR(group);
// 					table.XHRcanceler[group] = $q.defer();
// 					return table.XHRcanceler[group].promise;
// 				},
// 				closeXHR: function (group) {
// 					var group = !!group ? group : 'std_group';
// 					if(!!table.XHRcanceler[group]) {
// 						notifyer.console.warn('CLOSING AJAX Request (Group: "'+group+'")…');
// 					}
// 					table.XHRcanceler[group] = null;
// 				},
// 				startReload: function (now) {
// 					if(!!table.table) {
// 						table.stopReload();
// 						// if now
// 						if(!!now) {
// 							table.table.ajax.reload(null, false);
// 							table.redrawn = false;
// 						}
// 						// launch reload
// 						table.reload = $interval(function () {
// 							if(table.redrawn && !table.$modal) {
// 								if(table.table.settings()[0].jqXHR.readyState !== 4) {
// 									table.table.settings()[0].jqXHR.abort();
// 									notifyer.console.debug('- TABLE Reload '+dateFilter(new Date(), 'h:mm:ss')+':', 'ABORTED');
// 								}
// 								// ready for new request…
// 								table.table.ajax.reload(null, false);
// 								table.redrawn = false;
// 								notifyer.console.debug('- TABLE Reload '+dateFilter(new Date(), 'h:mm:ss')+':', angular.copy(table.table.settings()[0].jqXHR));
// 							}
// 						}, table.reload_tempo);
// 					}
// 				},
// 				stopReload: function () {
// 					if(!!table.reload) {
// 						// stop reload
// 						$interval.cancel(table.reload);
// 						table.reload = null;
// 					}
// 				},
// 				closeAll: function () {
// 					$timeout(function () {
// 						table.startReload(true);
// 						table.closeModal();
// 						$timeout(function () {
// 							table.justEndLoad();
// 						}, 1000);
// 					}, 300);
// 				},
// 				openModal: function ($template, options) {
// 					$('#modals').empty().append($template)
// 					$compile($template)(table.$table_scope);
// 					$template.modal(options);
// 					table.$modal.on('hidden.bs.modal', function (e) {
// 						// delete table.$table_scope.entity;
// 						table.$modal = null;
// 						table.justEndLoad();
// 						table.startReload(true);
// 					});
// 				},
// 				closeModal: function () {
// 					modals.disableFullLoader();
// 					if(!!table.$modal) {
// 						table.$modal.modal('hide');
// 						$timeout(function () {
// 							table.$modal.empty();
// 							table.$modal = null;
// 						}, 200);
// 					}
// 				},
// 				justEndLoad: function () {
// 					table.$table_scope.loading = false;
// 					table.$table_scope.errorload = false;
// 					modals.disableFullLoader();
// 				},
// 				getRowData: function(row) {
// 					return !!table.table ? table.table.row(row).data() : null;
// 				},
// 				callbacks: {
// 					show: { success: [], error: [] },
// 					create: { success: [], error: [] },
// 					update: { success: [], error: [] },
// 					enable: { success: [], error: [] },
// 					disable: { success: [], error: [] },
// 					softdelete: { success: [], error: [] },
// 					unsoftdelete: { success: [], error: [] },
// 					delete: { success: [], error: [] },
// 				},
// 				// Table callbacks
// 				addCallback: function(action, status, callback) {
// 					table.callbacks[action][status].push(callback);
// 					return table;
// 				},
// 			};


// 			table.$table_scope.entity_current_language = translations.currentLocale();
// 			table.$table_scope.loading = false;
// 			table.$table_scope.errorload = false;
// 			table.$table_scope.getResultClass = function(testform) { return Object.keys(testform.children || []).length > 0; };
// 			table.$table_scope.hasChildren = function(testform) {
// 				var classes = {};
// 				if(!testform.vars.touched) return classes;
// 				if(Object.keys(testform.vars.errors.errors).length) {
// 					classes['has-error'] = true;
// 				} else {
// 					// classes['has-success'] = true;
// 				}
// 				return classes;
// 			};
// 			table.$table_scope.dtAction = function(action, row, confirmed) {
// 				var confirmed = !!confirmed;
// 				if(!!table[action]) table[action](row, confirmed); else notifyer.toastr.warning('Functionnality "'+action+'" not ready yet!');
// 			};

// 			// Export columns
// 			var cols = '';
// 			var description = entitiesDescriptions.getDatatablesColumns(shortname, options);
// 			// notifyer.console.debug('Table description:', description);
// 			angular.forEach(description.columns, function (value, key) {
// 				cols += '<th>'+(value.name || value.data)+'</th>';
// 				if(!!value.exportColumn) table.exportColumns.push(key);
// 			});
// 			// add columns in header (and footer) table
// 			$element.html(!!options.footer ? '<thead class="need-compile">'+cols+'</thead><tfoot class="need-compile">'+cols+'</tfoot>' : '<thead class="need-compile">'+cols+'</thead>');

// 			table.table = $element.DataTable(angular.extend({
// 				language: {
// 					url: sitedata.getPath('web')+'json/'+translations.currentLocale(true)+'.json',
// 					// url: sitedata.getPath('websiteadmin')+'json/'+translations.currentLocale(true)+'.json',
// 				},
// 				searchDelay: 600,
// 				order: [[ 0, 'asc' ]],
// 				createdRow: function(row, data, dataIndex) {
// 					// notifyer.console.debug('Row:', {data: data, row: row, dataIndex: dataIndex});
// 					if(!!data.softdeleted) $(row).addClass('danger');
// 						else if(!data.enabled && data.enabled !== undefined) $(row).addClass('warning');
// 							else if(!!data.prefered) $(row).addClass('info');
// 				},
// 				pageLength: 25,
// 				responsive: true,
// 				dom: '<"html5buttons"B>lTfgitp',
// 				buttons: [
// 					{
// 						extend: 'copy',
// 						text: translations.translate('Copier'),
// 						exportOptions: { columns: table.exportColumns },
// 					},
// 					{
// 						extend: 'csv',
// 						text: translations.translate('Csv'),
// 						title: shortname+'File_'+moment().format('YYYY-MM-DDTHH-mm-ss'),
// 						exportOptions: { columns: table.exportColumns },
// 					},
// 					{
// 						extend: 'excel',
// 						text: translations.translate('Excel'),
// 						title: shortname+'File_'+moment().format('YYYY-MM-DDTHH-mm-ss'),
// 						exportOptions: { columns: table.exportColumns },
// 					},
// 					{
// 						extend: 'pdf',
// 						text: translations.translate('Pdf'),
// 						title: shortname+'File_'+moment().format('YYYY-MM-DDTHH-mm-ss'),
// 						exportOptions: { columns: table.exportColumns },
// 					},
// 					{
// 						extend: 'print',
// 						text: translations.translate('Imprimer'),
// 						exportOptions: { columns: table.exportColumns },
// 						customize: function (win){
// 							$(win.document.body).addClass('white-bg');
// 							$(win.document.body).css('font-size', '10px');
// 							$(win.document.body).addClass('compact').css('font-size', 'inherit');
// 						},
// 					}
// 				],
// 				preDrawCallback: function(settings) {
// 					return !table.$table_scope.loading || table.$table_scope.errorload;
// 				},
// 				drawCallback: function (settings) {
// 					notifyer.console.debug('Table drawCallback '+dateFilter(new Date(), 'h:mm:ss')+':', angular.copy(settings));
// 					if(!!settings.jqXHR) {
// 						settings.jqXHR.then(
// 							function success(response) {
// 								// actions buttons
// 								settings.oInstance.find('.need-compile').each(function (item) {
// 									$(this).removeClass('need-compile');
// 									$compile(this)(table.$table_scope);
// 								});
// 								table.redrawn = true;
// 							},
// 							function error(response) {
// 								notifyer.toastr.error('Une erreur est survenue!');
// 							}
// 						);
// 					}
// 				}
// 			}, description));


// 			// Actions
// 			table.$table_scope.save = function() {
// 				if(!table.$table_scope.loading && table.$table_scope.form.formdata.vars.touched) {
// 					modals.enableFullLoader();
// 					table.$table_scope.errorload = false;
// 					// table.$table_scope.loading = true;
// 					table.$table_scope.form.submit_form().then(
// 						function success(response) {
// 							switch (response.return_type) {
// 								case 'return_form_valid':
// 								case 'return_entity':
// 									// Success : entity returned
// 									// table.table.ajax.reload(null, false);
// 									notifyer.toastr.success('success.save')
// 										// .then(function onClose() { table.closeAll(); });
// 									table.closeAll();
// 									angular.forEach(table.callbacks.update.success, function (callback, key) { callback(response.data); });
// 									break;
// 								case 'return_form_invalid':
// 									// Errors : form returned
// 									table.$table_scope.form.setNewFormdata(response.data);
// 									notifyer.toastr.error('errors.save')
// 										// .then(function onClose() { table.justEndLoad(); });
// 									table.closeAll();
// 									angular.forEach(table.callbacks.update.error, function (callback, key) { callback(response.data); });
// 									break;
// 								case 'return_system_error':
// 									// Errors : form returned
// 									// table.$table_scope.form.setNewFormdata(response.data);
// 									table.closeModal();
// 									notifyer.toastr.error('errors.system')
// 										// .then(function onClose() { table.closeAll(); });
// 									table.closeAll();
// 									// var $iframe = $('<div class="modal inmodal fade" role="dialog"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal"><span>Fermer</span></button></div></div></div></div>');
// 									// $iframe.find('.modal-content').first().append($(response.data).find('body'));
// 									$iframe.modal();
// 									// angular.forEach(table.callbacks.update.error, function (callback, key) { callback(response.data); });
// 									break;
// 							}
// 						},
// 						function error(response) {
// 							table.$table_scope.errorload = true;
// 							switch (response.status) {
// 								case 405:
// 									notifyer.toastr.error('errors.disconnected').then(function onClose() { table.closeAll(); });
// 									break;
// 								default:
// 									notifyer.toastr.error('errors.'+response.statusText).then(function onClose() { table.closeAll(); });
// 									break;
// 							}
// 							angular.forEach(table.callbacks.update.error, function (callback, key) { callback(response); });
// 						}
// 					);
// 				}
// 			};
// 			table.show = function (row, confirmed) {
// 				table.$table_scope.row = row;
// 				table.$table_scope.entity = table.getRowData(table.$table_scope.row);
// 				table.$table_scope.entity.shortname = table.$table_scope.entity.shortname.toLowerCase();
// 				var URL = table.$table_scope.entity.cruds.actions.api.show;
// 				if(!!table.$modal) {
// 					// modal is still open
// 					table.$modal.on('hidden.bs.modal', function (event) { table.show(table.$table_scope.row);table.closeXHR(); });
// 					table.justEndLoad();
// 				} else if(!!URL) {
// 					notifyer.console.debug('Show row #'+table.$table_scope.row+' data:', table.$table_scope.entity);
// 					var templateUrl = sitedata.getPath('websiteadmin', 'angular/templates/modal/modal-'+table.$table_scope.entity.shortname+'-show.html');
// 					$templateRequest(templateUrl).then(
// 						function success(template) {
// 							table.$table_scope.errorload = false;
// 							table.$table_scope.loading = true;
// 							table.$modal = $(template);
// 							table.openModal(table.$modal);
// 							// URL.url = sitedata.getPath('root', URL.path);
// 							notifyer.console.debug('Entity show URL:', URL.url);
// 							$http[URL.ajx_method](URL.url, {timeout: table.resetXHR()}).then(
// 								function success(response) {
// 									switch (response.status) {
// 										case 200:
// 											table.$table_scope.entity = response.data;
// 											// notifyer.console.debug('Entity:', table.$table_scope.entity);
// 											table.$table_scope.entity.shortname = table.$table_scope.entity.shortname.toLowerCase();
// 											table.$table_scope.columns = entitiesDescriptions.getShowColumns(table.$table_scope.entity);
// 											// notifyer.console.debug('Show entity:', table.$table_scope.entity);
// 											table.justEndLoad();
// 											break;
// 										default: // other errors…
// 											notifyer.toastr.warning('This element encountered errors and can not be correctly loaded for show (code '+response.status+')!').then(function onClose() { table.closeAll(); });
// 									}
// 								},
// 								function error(response) {
// 									if(response.status !== -1) {
// 										table.$table_scope.errorload = true;
// 										notifyer.toastr.error('errors.'+response.statusText).then(function onClose() { table.closeAll(); });
// 									}
// 								}
// 							);
// 						},
// 						function error(response) {
// 							table.$table_scope.errorload = true;
// 							notifyer.toastr.error('errors.'+response.statusText).then(function onClose() { table.closeAll(); });
// 						}
// 					);
// 				} else {
// 					// NO URL
// 					notifyer.toastr.error('errors.noUrl_show');
// 				}
// 			};
// 			table.update = function (row, confirmed) {
// 				table.$table_scope.row = row;
// 				table.$table_scope.form = table.getRowData(table.$table_scope.row);
// 				table.$table_scope.form.shortname = table.$table_scope.form.shortname.toLowerCase();
// 				var URL = table.$table_scope.form.cruds.actions.api.update;
// 				if(!!table.$modal) {
// 					// modal is still open
// 					table.$modal.on('hidden.bs.modal', function (event) { table.update(table.$table_scope.row);table.closeXHR(); });
// 					table.justEndLoad();
// 				} else if(!!URL) {
// 					notifyer.console.debug('Update row #'+table.$table_scope.row+' data:', table.$table_scope.form);
// 					var templateUrl = sitedata.getPath('websiteadmin', 'angular/templates/modal/modal-'+table.$table_scope.form.shortname+'-update.html');
// 					$templateRequest(templateUrl).then(
// 						function success(template) {
// 							table.$table_scope.errorload = false;
// 							table.$table_scope.loading = true;
// 							table.$modal = $(template);
// 							table.openModal(table.$modal, {backdrop: 'static'});
// 							forms.loadUpdateForm(table.$table_scope.form, 'update', table.$modal, {timeout: table.resetXHR()}).then(
// 								function (response) {
// 									table.$table_scope.form = response;
// 									notifyer.console.debug('form:', table.$table_scope.form);
// 									// table.$table_scope.form.shortname = table.$table_scope.form.shortname.toLowerCase();
// 									table.justEndLoad();
// 									// table.table.ajax.reload(null, false);
// 									// angular.forEach(table.callbacks.update.success, function (callback, key) { callback(response); });
// 								},
// 								function (response) {
// 									if(response.status !== -1) {
// 										table.$table_scope.errorload = true;
// 										notifyer.toastr.error('errors.'+response.statusText).then(function onClose() { table.closeAll(); });
// 									}
// 								}
// 							);
// 						},
// 						function error(response) {
// 							table.$table_scope.errorload = true;
// 							notifyer.toastr.error('errors.updator').then(function onClose() { table.closeAll(); });
// 						}
// 					);
// 				} else {
// 					// NO URL
// 					notifyer.toastr.error('errors.noUrl_update');
// 				}
// 			};
// 			table.enable = function (row) {
// 				table.$table_scope.row = row;
// 				table.$table_scope.entity = table.getRowData(table.$table_scope.row);
// 				notifyer.console.debug('Enable row #'+table.$table_scope.row+' data:', table.$table_scope.entity);
// 				var URL = table.$table_scope.entity.cruds.actions.api.enable;
// 				if(!!URL) {
// 					// URL.url = sitedata.getPath('root', URL.path);
// 					notifyer.console.debug('Entity enable URL:', URL.url);
// 					$http[URL.ajx_method](URL.url).then(
// 						function success(response) {
// 							switch (response.status) {
// 								case 200:
// 									table.table.ajax.reload(null, false);
// 									angular.forEach(table.callbacks.enable.success, function (callback, key) { callback(response); });
// 									notifyer.toastr.success('success.enabled');
// 									break;
// 								default: // other errors…
// 									angular.forEach(table.callbacks.enable.error, function (callback, key) { callback(response); });
// 									notifyer.toastr.warning('errors.not_modified', {status: response.status});
// 							}
// 						},
// 						function error(response) {
// 							angular.forEach(table.callbacks.enable.error, function (callback, key) { callback(response); });
// 							notifyer.toastr.error('errors.'+response.statusText);
// 						}
// 					);
// 				} else { notifyer.toastr.error('errors.noUrl_enable'); }
// 			};
// 			table.disable = function (row, confirmed) {
// 				var confirmed = !!confirmed;
// 				if(!confirmed) {
// 					notifyer.toastr.confirm('warning', 'messages.confirm_request_disable', null, 'Disable').then(function confirmed(response) { table.$table_scope.dtAction('disable', row, true); });
// 				} else {
// 					table.$table_scope.row = row;
// 					table.$table_scope.entity = table.getRowData(table.$table_scope.row);
// 					notifyer.console.debug('Disable row #'+table.$table_scope.row+' data:', table.$table_scope.entity);
// 					var URL = table.$table_scope.entity.cruds.actions.api.disable;
// 					if(!!URL) {
// 						// URL.url = sitedata.getPath('root', URL.path);
// 						notifyer.console.debug('Entity disable URL:', URL.url);
// 						$http[URL.ajx_method](URL.url).then(
// 							function success(response) {
// 								switch (response.status) {
// 									case 200:
// 										table.table.ajax.reload(null, false);
// 										angular.forEach(table.callbacks.disable.success, function (callback, key) { callback(response); });
// 										notifyer.toastr.success('success.disabled');
// 										break;
// 									default: // other errors…
// 										angular.forEach(table.callbacks.disable.error, function (callback, key) { callback(response); });
// 										notifyer.toastr.warning('errors.not_modified', {status: response.status});
// 								}
// 							},
// 							function error(response) {
// 								angular.forEach(table.callbacks.disable.error, function (callback, key) { callback(response); });
// 								notifyer.toastr.error('errors.'+response.statusText);
// 							}
// 						);
// 					} else { notifyer.toastr.error('errors.noUrl_disable'); }
// 				}
// 			};
// 			table.softdelete = function (row, confirmed) {
// 				var confirmed = !!confirmed;
// 				if(!confirmed) {
// 					notifyer.toastr.confirm('warning', 'messages.confirm_request_softdelete', null, 'Delete').then(function confirmed(response) { table.$table_scope.dtAction('softdelete', row, true); });
// 				} else {
// 					table.$table_scope.row = row;
// 					table.$table_scope.entity = table.getRowData(table.$table_scope.row);
// 					notifyer.console.debug('Softdelete row #'+table.$table_scope.row+' data:', table.$table_scope.entity);
// 					var URL = table.$table_scope.entity.cruds.actions.api.softdelete;
// 					if(!!URL) {
// 						// URL.url = sitedata.getPath('root', URL.path);
// 						notifyer.console.debug('Entity softdelete URL:', URL.url);
// 						$http[URL.ajx_method](URL.url).then(
// 							function success(response) {
// 								switch (response.status) {
// 									case 200:
// 										table.table.ajax.reload(null, false);
// 										angular.forEach(table.callbacks.softdelete.success, function (callback, key) { callback(response); });
// 										notifyer.toastr.success('success.softdeleted');
// 										break;
// 									default: // other errors…
// 										angular.forEach(table.callbacks.softdelete.error, function (callback, key) { callback(response); });
// 										notifyer.toastr.warning('errors.not_modified', {status: response.status});
// 								}
// 							},
// 							function error(response) {
// 								angular.forEach(table.callbacks.softdelete.error, function (callback, key) { callback(response); });
// 								notifyer.toastr.error('errors.'+response.statusText);
// 							}
// 						);
// 					} else { notifyer.toastr.error('errors.noUrl_softdelete'); }
// 				}
// 			};
// 			table.unsoftdelete = function (row, confirmed) {
// 				table.$table_scope.row = row;
// 				table.$table_scope.entity = table.getRowData(table.$table_scope.row);
// 				notifyer.console.debug('Unsoftdelete row #'+table.$table_scope.row+' data:', table.$table_scope.entity);
// 				var URL = table.$table_scope.entity.cruds.actions.api.unsoftdelete;
// 				if(!!URL) {
// 					// URL.url = sitedata.getPath('root', URL.path);
// 					notifyer.console.debug('Entity unsoftdelete URL:', URL.url);
// 					$http[URL.ajx_method](URL.url).then(
// 						function success(response) {
// 							switch (response.status) {
// 								case 200:
// 									table.table.ajax.reload(null, false);
// 									angular.forEach(table.callbacks.unsoftdelete.success, function (callback, key) { callback(response); });
// 									notifyer.toastr.success('success.unsoftdeleted');
// 									break;
// 								default: // other errors…
// 									angular.forEach(table.callbacks.unsoftdelete.error, function (callback, key) { callback(response); });
// 									notifyer.toastr.warning('errors.not_modified', {status: response.status});
// 							}
// 						},
// 						function error(response) {
// 							angular.forEach(table.callbacks.unsoftdelete.error, function (callback, key) { callback(response); });
// 							notifyer.toastr.error('errors.'+response.statusText);
// 						}
// 					);
// 				} else { notifyer.toastr.error('errors.noUrl_unsoftdelete'); }
// 			};
// 			table.delete = function (row, confirmed) {
// 				var confirmed = !!confirmed;
// 				if(!confirmed) {
// 					notifyer.toastr.confirm('error', 'messages.confirm_request_delete', null, 'Delete').then(function confirmed(response) { table.$table_scope.dtAction('delete', row, true); });
// 				} else {
// 					table.$table_scope.row = row;
// 					table.$table_scope.entity = table.getRowData(table.$table_scope.row);
// 					notifyer.console.debug('Delete row #'+table.$table_scope.row+' data:', table.$table_scope.entity);
// 					var URL = table.$table_scope.entity.cruds.actions.api.delete;
// 					if(!!URL) {
// 						// URL.url = sitedata.getPath('root', URL.path);
// 						notifyer.console.debug('Entity delete URL:', URL.url);
// 						$http[URL.ajx_method](URL.url).then(
// 							function success(response) {
// 								switch (response.status) {
// 									case 204:
// 										table.table.ajax.reload(null, false);
// 										angular.forEach(table.callbacks.delete.success, function (callback, key) { callback(response); });
// 										notifyer.toastr.success('success.deleted');
// 										break;
// 									default: // other errors…
// 										angular.forEach(table.callbacks.delete.error, function (callback, key) { callback(response); });
// 										notifyer.toastr.warning('errors.not_modified', {status: response.status});
// 								}
// 							},
// 							function error(response) {
// 								angular.forEach(table.callbacks.delete.error, function (callback, key) { callback(response); });
// 								notifyer.toastr.error('errors.'+response.statusText);
// 							}
// 						);
// 					} else { notifyer.toastr.error('errors.noUrl_delete'); }
// 				}
// 			};

// 			// start TABLE :
// 			table.startReload();
// 			$element.on('$destroy', function() {
// 				table.stopReload();
// 			});
// 			// table
// 			return table;
// 		};


// 	}

// ]);




