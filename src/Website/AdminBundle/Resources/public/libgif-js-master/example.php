<?php $imagick = new imagick(); ?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>GIF animation control</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
	.hidden { display: none; }
	body {
		padding-top: 50px;
	}
	.starter-template {
		padding: 40px 15px;
		text-align: center;
	}
	</style>
</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="example.php" title="<?php echo $imagick->getVersion()['versionString']; ?>">Imagick <?php echo $imagick->getVersion()['versionNumber']; ?></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="../details.php">Informations</a></li>
					<li><a href="../imagick.php">Démo</a></li>
					<li class="active"><a href="#">Démo LibGif</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container-fluid" style="margin-bottom: 100px;">
		<center>
			<h1>Example with controls and no auto-play</h1>
			<img id="example1" src="./example_gifs/lamp_preview.jpg" rel:animated_src="./example_gifs/lamp.gif" rel:auto_play="0" />
			<br>
			<a href="#" id="b_rw">Rewind</a> | <a href="#" id="b_pl">Play</a> | <a href="#" id="b_pa">Pause</a> | <a href="#" id="b_fo">Step forward</a> | <a href="#" id="b_ba">Step back</a>

			<h1>Example with rubbing and auto-play on</h1>
			<img id="example2" src="./example_gifs/lamp.gif" rel:auto_play="1" rel:rubbable="1" />
			<br>
			<p>Image via <a href="http://mlkshk.com/p/IP08">mlkshk</a>
		</center>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript" src="./libgif.js"></script>
	<script type="text/javascript" src="./rubbable.js"></script>
	<script type="text/javascript">
		// image 1
		var sup1 = new SuperGif({ gif: $('img#example1').get(0), progressbar_height: 3, progressbar_foreground_color: '#336' } );
		sup1.load();
		$('#b_rw').on('click', function() { sup1.move_to(0); return false; });
		$('#b_pl').on('click', function() { sup1.play(); return false; });
		$('#b_pa').on('click', function() { sup1.pause(); return false; });
		$('#b_fo').on('click', function() { sup1.move_relative(1); return false; });
		$('#b_ba').on('click', function() { sup1.move_relative(-1); return false; });

		// image 2
		var sup2 = new RubbableGif({ gif: $('img#example2').get(0), progressbar_height: 3, progressbar_foreground_color: '#336' } );
		sup2.load();

	</script>
</body>
</html>