<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Menu;
// use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceDirectory;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Service\serviceBasedirectory;
use ModelApi\FileBundle\Form\Type\BasedirectoryAnnotateType;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\Action;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class BasedirectoryController extends EntityController {

	const ENTITY_CLASSNAME = Basedirectory::class;
	const ENTITY_SERVICE = serviceBasedirectory::class;
	const ENTITY_CREATE_TYPE = BasedirectoryAnnotateType::class;
	const ENTITY_UPDATE_TYPE = BasedirectoryAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/basedirectory/list",
	 *		name="wsa_basedirectory_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getBasedirectorysAction(Request $request) {
		// return $this->getEntitysAction($request);
		$classname = static::ENTITY_CLASSNAME;
		$globals = [];
		$globals['entity'] = new $classname();
		$globals['entities'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findByParent(null);
		// $globals['entities'] = $this->get(serviceUser::class)->getUserBasedirectorys('drive/');
		$globals['Pageweb']['titleh1'] = 'Dossiers';
		return $this->render('WebsiteAdminBundle:Pageweb:basedirectory_list.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory/transit/{id}",
	 *		defaults={"id" = null},
	 *		name="wsa_basedirectory_transit",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function transitBasedirectorysAction($id = null, Request $request) {
		// return $this->getEntitysAction($request);
		$globals['entity'] = empty($id) ? null : $this->get(serviceItem::class)->getRepository()->find($id);
		$globals['entities'] = $this->get(self::ENTITY_SERVICE)->getRepository()->findAsArray('auto', []);
		// $globals['entities'] = $this->get(serviceUser::class)->getUserBasedirectorys('drive/');
		$globals['Pageweb']['titleh1'] = 'Test Transit Dossiers';
		return $this->render('WebsiteAdminBundle:Pageweb:basedirectory_transit.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory_owner/list/{id}",
	 *		defaults={"id" = null},
	 *		name="wsa_basedirectory_owner",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getBasedirectorysOwnerAction($id = null, Request $request) {
		$globals = [];
		// $id is id or bddid of item
		if(empty($id)) {
			$globals['entity'] = $this->getUser();
		} else {
			$globals['entity'] = serviceEntities::isValidBddid($id, false) ? $this->get(serviceEntities::class)->findByBddid($id) : $this->get(serviceItem::class)->getRepository()->find($id);
		}
		// Test
		$action = new Action('dirs', $globals['entity'], $this->container);
		switch ($action->getStatus()) {
			case 0:
				// THIS basedirectory
				$globals['basedirectorys'] = [$globals['entity']];
				$globals['owner'] = $globals['entity']->getOwnerdir();
				$globals['Pageweb']['titleh1'] = 'Éléments de dossier <strong><u><i class="fa '.$globals['entity']->getIcon().' fa-1x fa-fw" style="color:'.$globals['entity']->getColor().';"></i>'.$globals['entity']->getName().'</u></strong>';
				break;
			case 1:
				// HAS basedirectorys
				$globals['basedirectorys'] = [$globals['entity']->getDriveDir(), $globals['entity']->getSystemDir()];
				$globals['owner'] = $globals['entity'];
				$globals['Pageweb']['titleh1'] = $globals['entity'] === $this->getUser() ? 'Mes dossiers <strong>'.$this->getUser()->getUsername().'</strong>' : 'Dossiers de <strong><u><i class="fa '.$globals['entity']->getIcon().' fa-1x fa-fw" style="color:'.$globals['entity']->getColor().';"></i>'.$globals['entity']->getName().'</u></strong>';
				break;
			case 2:
				// PARENT basedirectory
				$globals['basedirectorys'] = [$globals['entity']->getParent()];
				$globals['owner'] = $globals['entity']->getOwnerdir();
				$globals['Pageweb']['titleh1'] = $globals['entity'] === $this->getUser() ? 'Mes dossiers <strong>'.$this->getUser()->getUsername().'</strong>' : 'Dossier parent de <strong><u><i class="fa '.$globals['entity']->getIcon().' fa-1x fa-fw" style="color:'.$globals['entity']->getColor().';"></i>'.$globals['entity']->getName().'</u></strong>';
				break;
			default:
				$globals['basedirectorys'] = [];
				$globals['owner'] = null;
				$globals['Pageweb']['titleh1'] = 'Cet élément n\'est pas concerné par les dossiers...';
				$this->get(serviceFlashbag::class)->addFlashToastr('error', 'Cet élément n\'a aucun dossier');
				break;
		}
		$needFlush = false;
		$globals['basedirectorys'] = array_filter($globals['basedirectorys'], function($item) use (&$needFlush) {
			// if($item instanceOf Basedirectory) $needFlush = $needFlush || $item->checkChildsPositions();
			return $item instanceOf Basedirectory;
		});
		// if($needFlush) $this->get(serviceEntities::class)->getEntityManager()->flush();
		$globals['actions'] = false;
		return $this->render('WebsiteAdminBundle:Pageweb:basedirectory_owner_list.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory/show/{id}",
	 *		name="wsa_basedirectory_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getBasedirectoryAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory/info/{action}/{id}",
	 *		defaults={"action" = "show-all", "id" = null},
	 *		name="wsa_info_directorys",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function infoBasedirectoryAction($action = "show-all", $id = null, Request $request) {
		if(preg_match('/^resolve/', $action)) {
			ini_set('memory_limit', -1);
			set_time_limit(300);
		}
		switch ($action) {
			case 'resolve-doublons':
				if($this->get(self::ENTITY_SERVICE)->resolveDoublons()) {
					$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Tous les doublons ont été résolus !');
				} else {
					$globals['doublons'] = $this->get(self::ENTITY_SERVICE)->findDoublons();
					if(count($globals['doublons'])) $this->get(serviceFlashbag::class)->addFlashToastr('error', 'Il reste encore '.count($globals['doublons']).' doublon(s) dans la base de doonées !');
						else $this->get(serviceFlashbag::class)->addFlashToastr('warning', 'Il semble ne pas avoir été trouvé de doublons !');
				}
				return $this->redirectLastOrDefaultUrl($request, 'wsa_info_directorys');
				break;
			case 'resolve-joinedowndirectory':
				$this->get(serviceItem::class)->resolveJoinedowndirectory();
				// $this->getDoctrine()->getEntityManager()->flush();
				$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Tous les liens JoinedOwndirectory ont été révisés.');
				return $this->redirectLastOrDefaultUrl($request, 'wsa_info_directorys');
				break;
			case 'resolve-paths':
				$em = $this->getDoctrine()->getEntityManager();
				$globals['entities'] = $this->get(self::ENTITY_SERVICE)->getRepository()->findAll();
				// $globals['entities'] = $this->get(self::ENTITY_SERVICE)->getRepository()->findAllAsFacticee();
				foreach ($globals['entities'] as $item) $item->updatePaths();
				$em->flush();
				return $this->redirectLastOrDefaultUrl($request, 'wsa_info_directorys');
				break;
			case 'resolve-lostchilds':
				$globals['entities'] = $this->get(self::ENTITY_SERVICE)->resolveLostchilds($id);
				return $this->redirectLastOrDefaultUrl($request, 'wsa_info_directorys');
				break;

			case 'show-doublons':
				// $globals['entities'] = [];
				// $globals['doublons'] = $this->get(self::ENTITY_SERVICE)->findDoublons(true);
				$globals = $this->get(self::ENTITY_SERVICE)->getForCheckAsFacticee([]);
				break;
			case 'show-invalids':
				$globals = $this->get(self::ENTITY_SERVICE)->getForCheckAsFacticee([]);
				$globals['entities'] = array_filter($this->get(self::ENTITY_SERVICE)->findAll(), function($dir) {
					return !$dir->isValid();
				});
				// $globals['doublons'] = $this->get(self::ENTITY_SERVICE)->findDoublons();
				break;
			case 'show-lostchilds':
				// $globals['entities'] = $this->get(self::ENTITY_SERVICE)->findLostchilds();
				// $globals['doublons'] = $this->get(self::ENTITY_SERVICE)->findDoublons();
				$globals = $this->get(self::ENTITY_SERVICE)->getForCheckAsFacticee([]);
				break;
			default:
				$globals = $this->get(self::ENTITY_SERVICE)->getForCheckAsFacticee([]);
				break;
		}
		$globals['action'] = $action;
		return $this->getEntitysAction($request, $globals);
	}

	// /**
	//  * @Route(
	//  *		"/file/basedirectory/create",
	//  *		name="wsa_basedirectory_create",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_TRANSLATOR')")
	//  */
	// public function createBasedirectoryAction(Request $request) {
	// 	return $this->createEntityAction($request);
	// }

	// /**
	//  * @Route(
	//  *		"/file/basedirectory/create",
	//  *		name="wsa_basedirectory_post",
	//  *		options={ "method_prefix" = false },
	//  *		methods="POST"
	//  * )
	//  * @Security("has_role('ROLE_TRANSLATOR')")
	//  */
	// public function postBasedirectoryAction(Request $request) {
	// 	return $this->postEntityAction($request);
	// }

	/**
	 * @Route(
	 *		"/file/basedirectory/update/{id}/{method}/{context}",
	 *		defaults={"method" = "put", "context" = null},
	 *		name="wsa_basedirectory_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateBasedirectoryAction($id, Request $request, $method = 'put', $context = null) {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_basedirectory_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putBasedirectoryAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_basedirectory_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchBasedirectoryAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/basedirectory/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_basedirectory_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function enableBasedirectoryAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_basedirectory_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function softdeleteBasedirectoryAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory/check-childpositions/{id}",
	 *		name="wsa_basedirectory_checkchildpositions",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function checkChildpositionsAction($id, Request $request) {
		$basedirectory = $this->getEntityById($id);
		if($basedirectory->checkChildsPositions()) {
			// $this->get(serviceEntities::class)->getEntityManager()->flush();
			if($basedirectory->isChildPositionsValid()) {
				$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Position des éléments du dossier checkés !');
				$this->get(serviceEntities::class)->getEntityManager()->flush();
				if($basedirectory instanceOf Menu) $basedirectory->getCachedchilds(true, true);
			} else {
				$this->get(serviceFlashbag::class)->addFlashToastr('error', 'Positions checkées, toujours désorganisées !');
			}
		} else {
			$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Position des éléments du dossier déjà valides. Aucune opération n\'a été nécessaire !');
		}
		return $this->redirectToRoute('wsa_directory_owner', ['id' => $basedirectory->getId()]);
		// return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory/move-child/{parent}/{child}/{position}",
	 *		name="wsa_basedirectory_movechild",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @param Basedirectory $parent
	 * @param Item $child
	 * @param Request $request
	 * @return Response
	 */
	public function moveChildAction(Basedirectory $parent, Item $child, $position, Request $request) {
		if(empty($parent)) {
			$this->get(serviceFlashbag::class)->addFlashToastr('error', 'Dossier introuvable !');
		} else {
			$position = (integer)$position;
			$new_position = $parent->setChildPosition($child, $position);
			if($new_position !== $position) $this->get(serviceFlashbag::class)->addFlashToastr('error', 'Opération échouée !');
			$this->get(serviceEntities::class)->getEntityManager()->flush();
			// if($parent instanceOf Menu) $parent->getCachedchilds(true, true);
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/file/basedirectory/remove-child/{parent}/{child}",
	 *		name="wsa_basedirectory_removechild",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @param Basedirectory $parent
	 * @param Item $child
	 * @param Request $request
	 * @return Response
	 */
	public function removeChildAction(Basedirectory $parent, Item $child, Request $request) {
		if(empty($parent)) {
			$this->get(serviceFlashbag::class)->addFlashToastr('error', 'Dossier introuvable !');
		} else {
			$parent->removeChild($child);
			$this->get(serviceEntities::class)->getEntityManager()->flush();
			// if($parent instanceOf Menu) $parent->getCachedchilds(true, true);
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}








}
