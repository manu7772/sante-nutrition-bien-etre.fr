<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// MarketBundle
use ModelApi\MarketBundle\Service\serviceActivity;
use ModelApi\MarketBundle\Entity\Baseprod;
use ModelApi\MarketBundle\Entity\Activity;
use ModelApi\MarketBundle\Form\Type\ActivityAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ActivityController extends EntityController {

	const ENTITY_CLASSNAME = Activity::class;
	const ENTITY_SERVICE = serviceActivity::class;
	const ENTITY_CREATE_TYPE = ActivityAnnotateType::class;
	const ENTITY_UPDATE_TYPE = ActivityAnnotateType::class;

	/**
	 * @Route(
	 *		"/activity/list",
	 *		name="wsa_activity_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getActivitysAction(Request $request) {
		$globals['entities'] = $this->getRepository(static::ENTITY_CLASSNAME)->findRoots();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/activity/show/{id}",
	 *		name="wsa_activity_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getActivityAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/activity/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_activity_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createActivityAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/activity/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_activity_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postActivityAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/activity/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_activity_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateActivityAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/activitys/sort",
	 *		name="wsa_activity_sort",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function sortActivitysAction(Request $request) {
		$em = $this->get(serviceEntities::class)->getEntityManager();
		$entities = $this->getRepository(static::ENTITY_CLASSNAME)->findAll();
		$index = ['orphan' => 0];
		foreach ($entities as $entity) {
			$parent = $entity->getParentprod();
			if($parent instanceOf Baseprod) {
				$index[$parent->getId()] ??= 0;
				$entity->setSortprod($index[$parent->getId()]++);
			} else {
				$entity->setSortprod($index['orphan']++);
			}
		}
		$em->flush();
		$this->get(serviceFlashbag::class)->addFlashToastr(null, 'Activitys sort have been updated!');
		return $this->redirectToRoute('wsa_activity_list');
	}

	/**
	 * @Route(
	 *		"/activity/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_activity_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putActivityAction($id, Request $request) {
		return $this->putEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/activity/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_activity_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchActivityAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/activity/prefered/{id}/{status}",
	 *		name="wsa_activity_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function preferedActivityAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/activity/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_activity_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableActivityAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/activity/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_activity_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteActivityAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/activity/delete/{id}",
	 *		name="wsa_activity_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteActivityAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
