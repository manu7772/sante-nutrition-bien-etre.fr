<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\TierManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceLogg;
use ModelApi\BaseBundle\Service\servicePreference;
use ModelApi\BaseBundle\Component\Preference;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Service\serviceTier;
use ModelApi\UserBundle\Repository\TierRepository;
// use ModelApi\UserBundle\Form\Type\TierAnnotateType;
use ModelApi\UserBundle\Service\serviceRoles;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use \Exception;

class TierController extends EntityController {

	const ENTITY_CLASSNAME = Tier::class;
	const ENTITY_SERVICE = serviceTier::class;
	// const ENTITY_CREATE_TYPE = TierAnnotateType::class;
	// const ENTITY_UPDATE_TYPE = TierAnnotateType::class;

    // protected $eventDispatcher;
    // protected $formFactory;
    // protected $userManager;
    // protected $tokenGenerator;
    // protected $mailer;

    // /**
    //  * @param EventDispatcherInterface $eventDispatcher
    //  * @param FactoryInterface         $formFactory
    //  * @param TierManagerInterface     $userManager
    //  * @param TokenGeneratorInterface  $tokenGenerator
    //  * @param MailerInterface          $mailer
    //  */
    // public function __construct(EventDispatcherInterface $eventDispatcher, FactoryInterface $formFactory, TierManagerInterface $userManager, TokenGeneratorInterface $tokenGenerator, MailerInterface $mailer) {
    //     $this->eventDispatcher = $eventDispatcher;
    //     $this->formFactory = $formFactory;
    //     $this->userManager = $userManager;
    //     $this->tokenGenerator = $tokenGenerator;
    //     $this->mailer = $mailer;
    // }

	// /**
	//  * @Route(
	//  *		"/tier/list/{role}",
	//  * 		defaults={"role": null},
	//  *		name="wsa_tier_list",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_TRANSLATOR')")
	//  */
	// public function getTiersAction($role = null, Request $request) {
	// 	$globals['serviceRoles'] = $this->get(serviceRoles::class);
	// 	// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findAllWithAvatar(false);
	// 	return $this->getEntitysAction($request, $globals);
	// }

	// /**
	//  * @Route(
	//  *		"/tier/show/{id}",
	//  *		name="wsa_tier_show",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_TRANSLATOR')")
	//  */
	// public function getTierAction($id, Request $request) {
	// 	$globals['user_loggs'] = $this->get(serviceLogg::class)->getRepository()->findSearch(25, ['userid' => $id]);
	// 	// Stats
	// 	$globals['time_average'] = 0;
	// 	foreach ($globals['user_loggs'] as $logg) $globals['time_average'] += $logg->getDuring();
	// 	$loggCount = count($globals['user_loggs']);
	// 	$globals['time_average'] = $loggCount > 0 ? $globals['time_average'] / $loggCount : 0;
	// 	return $this->getEntityAction($id, $request, $globals);
	// }

	// /**
	//  * @Route(
	//  *		"/tier/create",
	//  *		name="wsa_tier_create",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_ADMIN')")
	//  */
	// public function createTierAction(Request $request) {
	// 	return $this->createEntityAction($request);
	// }

	// /**
	//  * @Route(
	//  *		"/tier/create",
	//  *		name="wsa_tier_post",
	//  *		options={ "method_prefix" = false },
	//  *		methods="POST"
	//  * )
	//  * @Security("has_role('ROLE_ADMIN')")
	//  */
	// public function postTierAction(Request $request) {
	// 	return $this->postEntityAction($request);
	// }

	// /**
	//  * @Route(
	//  *		"/tier/update/{id}/{method}",
	//  *		defaults={"method" = "put"},
	//  *		name="wsa_tier_update",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_ADMIN')")
	//  */
	// public function updateTierAction($id, Request $request, $method = 'put') {
	// 	return $this->updateEntityAction($id, $request, 'default', $method);
	// }

	// /**
	//  * @Route(
	//  *		"/tier/put/{id}",
	//  *		name="wsa_tier_put",
	//  *		options={ "method_prefix" = false },
	//  *		methods="PUT"
	//  * )
	//  * @Security("has_role('ROLE_ADMIN')")
	//  */
	// public function putTierAction($id, Request $request) {
	// 	return $this->putEntityAction($id, $request);
	// }

	// *
	//  * @Route(
	//  *		"/tier/patch/{id}",
	//  *		name="wsa_tier_patch",
	//  *		options={ "method_prefix" = false },
	//  *		methods="PATCH"
	//  * )
	//  * @Security("has_role('ROLE_ADMIN')")
	 
	// public function patchTierAction($id, Request $request) {
	// 	return $this->patchEntityAction($id, $request);
	// }




	// /**
	//  * @Route(
	//  *		"/tier/prefered/{id}/{status}",
	//  *		defaults={"status" = 1},
	//  *		name="wsa_tier_prefered",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_ADMIN')")
	//  */
	// public function preferedTierAction($id, $status = 1, Request $request) {
	// 	return $this->preferedEntityAction($id, $status, $request);
	// }

	// /**
	//  * @Route(
	//  *		"/tier/enable/{id}/{status}",
	//  *		defaults={"status" = 1},
	//  *		name="wsa_tier_enable",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_ADMIN')")
	//  */
	// public function enableTierAction($id, $status = 1, Request $request) {
	// 	return $this->enableEntityAction($id, $status, $request);
	// }

	// /**
	//  * @Route(
	//  *		"/tier/softdelete/{id}/{status}",
	//  *		defaults={"status" = 1},
	//  *		name="wsa_tier_softdelete",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_ADMIN')")
	//  */
	// public function softdeleteTierAction($id, $status = 1, Request $request) {
	// 	return $this->softdeleteEntityAction($id, $status, $request);
	// }



	/**
	 * @Route(
	 *		"/tier/check-preferences/{id}/{reset}",
	 *		defaults={"reset" = 0},
	 *		name="wsa_tier_check_preferences",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function checkpreferencesAction($id, $reset = false, Request $request) {
		// var_dump(static::ENTITY_CLASSNAME);
		$tier = $this->getEntityById($id, null, Tier::class);
		if($this->get(servicePreference::class)->checkPreferences($tier, $reset)) {
			$this->getDoctrine()->getEntityManager()->flush();
			$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Preferences for '.json_encode($tier->getName()).' has been updated!');
		} else {
			$this->get(serviceFlashbag::class)->addFlashToastr('warning', 'Preferences for '.json_encode($tier->getName()).' has been updated, but no changes!');
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/newsletter/{id}/{path}/{value}",
	 *		name="wsa_tier_setpreference",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function setPreferenceAction($id, $path, $value, Request $request) {
		$tier = $this->getEntityById($id);
		$this->get(servicePreference::class)->checkPreferences($tier, false);
		$tier->setPreference($path, json_decode($value, true));
		$this->getDoctrine()->getEntityManager()->flush();
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/cookies/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_tier_cookies",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function cookiesTierAction($id, $status = 1, Request $request) {
		$user = $this->getEntityById($id);
		$user->setPreference('cookies', (boolean)$status);
		$this->getDoctrine()->getEntityManager()->flush();
		if($user === $this->getUser()) {
			$session = $request->getSession();
			$session->set('accepted_cookies', $user->getPreference('cookies'));
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/newsletter/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_tier_newsletter",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function newsletterTierAction($id, $status = 1, Request $request) {
		$user = $this->getEntityById($id);
		$user->setPreference('newsletter', (boolean)$status);
		$this->getDoctrine()->getEntityManager()->flush();
		// if($user === $this->getUser()) {
			// $session = $request->getSession();
			// $session->set('accepted_cookies', $user->getPreference('cookies'));
		// }
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/preference/{id}/{name}/{parameter}",
	 *		name="wsa_tier_parameter",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function parameterTierAction($id, $name, $parameter, Request $request) {
		$user = $this->getEntityById($id);
		$user_parameter = $user->getPreference($name, null, false, true);
		if($user_parameter != $parameter) {
			$user->setPreference($name, $parameter);
			$this->getDoctrine()->getEntityManager()->flush();
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/preference",
	 *		name="wsa_post_tier_parameter",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function postParameterAction(Request $request) {
		$bddid = $request->request->get('entity__bddid');
		$item = $this->get(serviceEntities::class)->findByBddid($bddid);
		// echo('<pre><h2>DATA</h2>');var_dump($data);die('</pre>');
		$success = $this->get(servicePreference::class)->fromRequest($request, $item);
		if($success) $this->getDoctrine()->getEntityManager()->flush();
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/sendinformations/{user}",
	 *		name="wsa_tier_sendinfos",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function sendinfosTierAction($user, Request $request) {
		$this->getManager()->sendTierInformations($user);
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}






}
