<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EventController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// EventBundle
use ModelApi\EventBundle\Service\serviceJeux;
use ModelApi\EventBundle\Entity\Jeux;
use ModelApi\EventBundle\Form\Type\JeuxAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class JeuxController extends EventController {

	const ENTITY_CLASSNAME = Jeux::class;
	const ENTITY_SERVICE = serviceJeux::class;
	const ENTITY_CREATE_TYPE = JeuxAnnotateType::class;
	const ENTITY_UPDATE_TYPE = JeuxAnnotateType::class;

	/**
	 * @Route(
	 *		"/event/jeux/list",
	 *		name="wsa_jeux_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getJeuxsAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/event/jeux/show/{id}",
	 *		name="wsa_jeux_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getJeuxAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/event/jeux/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_jeux_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createJeuxAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/jeux/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_jeux_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postJeuxAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/jeux/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_jeux_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateJeuxAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/event/jeux/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_jeux_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putJeuxAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/event/jeux/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_jeux_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchJeuxAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/event/jeux/prefered/{id}/{status}",
	 *		name="wsa_jeux_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function preferedJeuxAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/jeux/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_jeux_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableJeuxAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/jeux/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_jeux_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteJeuxAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/jeux/delete/{id}",
	 *		name="wsa_jeux_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteJeuxAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
