<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceAnnonce;
use ModelApi\BaseBundle\Entity\Annonce;
use ModelApi\BaseBundle\Form\Type\AnnonceAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AnnonceController extends EntityController {

	const ENTITY_CLASSNAME = Annonce::class;
	const ENTITY_SERVICE = serviceAnnonce::class;
	const ENTITY_CREATE_TYPE = AnnonceAnnotateType::class;
	const ENTITY_UPDATE_TYPE = AnnonceAnnotateType::class;

	/**
	 * @Route(
	 *		"/annonce/list",
	 *		name="wsa_annonce_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getAnnoncesAction(Request $request) {
		$globals['current'] = $this->getManager()->getRepository()->findCurrent();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/annonce/show/{id}",
	 *		name="wsa_annonce_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getAnnonceAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/annonce/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_annonce_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function createAnnonceAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/annonce/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_annonce_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function postAnnonceAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/annonce/update/{id}/{context}/{method}",
	 * 		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_annonce_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function updateAnnonceAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, 'default', $method);
	}

	/**
	 * @Route(
	 *		"/annonce/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_annonce_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function putAnnonceAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/annonce/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_annonce_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function patchAnnonceAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/annonce/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_annonce_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function enableAnnonceAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/annonce/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_annonce_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function softdeleteAnnonceAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/annonce/delete/{id}",
	 *		name="wsa_annonce_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function deleteAnnonceAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
