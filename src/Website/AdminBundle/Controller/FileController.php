<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceForms;
// FileBundle
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Tempfile;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FileController extends EntityController {

	const ENTITY_CLASSNAME = File::class;
	const ENTITY_SERVICE = FileManager::class;

	/**
	 * @Route(
	 *		"/file/list",
	 *		name="wsa_file_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getFilesAction(Request $request) {
		$globals = [];
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->findByTier($this->getUser(), false);
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/file/list/orphan",
	 *		name="wsa_file_orphan_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function getOrphanFilesAction(Request $request) {
		$globals = [];
		$globals['entities'] = $this->get(static::ENTITY_SERVICE)->findOrphansByTier($this->getUser(), false);
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/file/version/set-current/{id}",
	 *		name="wsa_file_set_current_version",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function setCurrentVersionAction($id, Request $request) {
		$em = $this->getDoctrine()->getEntityManager();
		$version = $em->getRepository(Fileversion::class)->find($id);
		if(empty($version)) {
			$this->addFlash('error', '<strong>ERROR</strong> : this Fileversion does not exist!');
		} else {
			$file = $version->getFile();
			$file->setCurrentVersion($version);
			$em->flush();
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}



	/**
	 * @Route(
	 *		"/file/create/{parent}",
	 *		defaults={"parent" = null},
	 *		name="wsa_file_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function createFileAction($parent = null, Request $request) {
		$globals['parent'] = null; // If null, assign logic parent
		if(!empty($parent)) {
			$globals['parent'] = $this->get(serviceBasedirectory::class)->getRepository()->find($parent);
			if(empty($globals['parent'])) throw new NotFoundHttpException($this->getShortname()." not found");
		}
		// $owner = $globals['parent'] instanceOf Basedirectory ? $globals['parent']->getOwnerdir() : $this->getUser();
		// $globals['entity'] = $this->get(static::ENTITY_SERVICE)->createNew([], function($tempfile) use ($owner) {
		// 	$tempfile->setOwner($owner);
		// });
		$globals['entity'] = $this->get(static::ENTITY_SERVICE)->createNew();


		$url = ['route' => 'wsa_file_post', 'params' => $globals['parent'] instanceOf Basedirectory ? ['parent' => $globals['parent']->getId()] : null];
		$form = $this->get(serviceForms::class)->getCreateForm($globals['entity'], $this->getUser(), null, ['ACTION_ROUTE' => $url, 'SUBMITS' => true]);

		$globals['form'] = $form->createView();
		$globals['entity'] = $form->getData();
		// return $this->render('WebsiteAdminBundle:Superadmin:print_entity.html.twig', ['entities' => [$globals['entity']]]);

		$globals['Pageweb'] = $this->getPageweb('create');
		$globals['actions'] = '@create';
		return $this->render(is_string($globals['Pageweb']) ? $globals['Pageweb'] : $globals['Pageweb']->getBundlepathname(), $globals);
	}

	/**
	 * @Route(
	 *		"/file/create/{parent}",
	 *		defaults={"parent" = null},
	 *		name="wsa_file_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function postFileAction($parent = null, Request $request) {
		$globals['parent'] = null; // If null, assign logic parent
		if(!empty($parent)) {
			$globals['parent'] = $this->get(serviceBasedirectory::class)->getRepository()->find($parent);
			if(empty($globals['parent'])) throw new NotFoundHttpException($this->getShortname()." not found");
		}
		$globals['entity'] = $this->get(static::ENTITY_SERVICE)->createNew();
		// $owner = $globals['parent'] instanceOf Basedirectory ? $globals['parent']->getOwnerdir() : $this->getUser();
		$url = ['route' => 'wsa_file_post', 'params' => $globals['parent'] instanceOf Basedirectory ? ['parent' => $globals['parent']->getId()] : null];
		$form = $this->get(serviceForms::class)->getCreateForm($globals['entity'], $this->getUser(), null, ['ACTION_ROUTE' => $url, 'SUBMITS' => true]);
		$form->handleRequest($request);
		// if($globals['parent'] instanceOf Basedirectory) $form->getData()->setTempParent($globals['parent']);
		// Get File
		$file = $this->get(static::ENTITY_SERVICE)->createFileByTempfile($form->getData());
		if($globals['parent'] instanceOf Basedirectory) $globals['parent']->addChild($file);
		// $this->printTest($file, $form);
		$errors = $this->get('validator')->validate($file);
		// FORM IS VALID
		if($errors->count() === 0) {
			$em = $this->get(serviceEntities::class)->getEntityManager();
			$em->persist($file);
			try {
				$em->flush();
			} catch (Exception $e) {
				echo($e->getMessage());
			}
			// return $this->render('WebsiteAdminBundle:Superadmin:print_entity.html.twig', ['entities' => [$entity, $file]]);
			return $this->redirectToRoute('wsa_directory_owner', ['id' => $globals['parent'] instanceOf Basedirectory ? $globals['parent']->getId() : $file->getParent()->getId()]);
		}
		// FORM HAS ERRORS
		// $form = $this->get(serviceForms::class)->getCreateForm($file, $this->getUser(), null, ['ACTION_ROUTE' => $url, 'SUBMITS' => true]);
		$form->isValid();
		$globals['entity'] ??= $form->getData();
		$globals['Pageweb'] ??= $this->getPageweb('create');
		$globals['form'] ??= $form->createView();
		$globals['actions'] ??= '@create';
		return $this->render(is_string($globals['Pageweb']) ? $globals['Pageweb'] : $globals['Pageweb']->getBundlepathname(), $globals);
	}

	// private function printTest($file, $form) {
	// 	$errors = $this->get('validator')->validate($file);
	// 	echo('<pre><h3>'.get_class($errors).' of File '.$file->getShortname().'</h3>');
	// 	var_dump($errors->__toString());
	// 	echo('<ul>');
	// 	echo('<li>Name: '.json_encode($file->getName()).'</li>');
	// 	echo('<li>Content type: '.(is_object($file->getFileformat()) ? $file->getFileformat()->getContentType() : '<span style="color: red;">NULL</span>').'</li>');
	// 	echo('<li>Media type: '.(is_object($file->getFileformat()) ? $file->getFileformat()->getMediaType() : '<span style="color: red;">NULL</span>').'</li>');
	// 	echo('<li>Sub type: '.(is_object($file->getFileformat()) ? $file->getFileformat()->getSubtype() : '<span style="color: red;">NULL</span>').'</li>');
	// 	echo('</ul>');
	// 	echo('</pre>');
	// 	$form->isValid();
	// 	echo('<pre><h3>'.get_class($form->getErrors()).' of Tempitem</h3>'); var_dump($form->getErrors()->__toString()); die('</pre>');
	// }

	/**
	 * @Route(
	 *		"/event/file/delete/{id}",
	 *		name="wsa_file_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function deleteFileAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}



}
