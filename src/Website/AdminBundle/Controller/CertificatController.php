<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// UserBundle
use ModelApi\UserBundle\Service\serviceCertificat;
use ModelApi\UserBundle\Entity\Certificat;
use ModelApi\UserBundle\Form\Type\CertificatAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CertificatController extends EntityController {

	const ENTITY_CLASSNAME = Certificat::class;
	const ENTITY_SERVICE = serviceCertificat::class;
	const ENTITY_CREATE_TYPE = CertificatAnnotateType::class;
	const ENTITY_UPDATE_TYPE = CertificatAnnotateType::class;

	/**
	 * @Route(
	 *		"/tier/certificat/list",
	 *		name="wsa_certificat_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getCertificatsAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/tier/certificat/show/{id}",
	 *		name="wsa_certificat_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getCertificatAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/tier/certificat/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_certificat_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createCertificatAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/certificat/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_certificat_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postCertificatAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/certificat/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_certificat_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateCertificatAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/certificats/sort",
	 *		name="wsa_certificat_sort",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function sortCertificatsAction(Request $request) {
		$em = $this->get(serviceEntities::class)->getEntityManager();
		$entities = $this->getRepository(static::ENTITY_CLASSNAME)->findAll();
		$index = 0;
		foreach ($entities as $entity) {
			$entity->setSortcertificat($index++);
		}
		$em->flush();
		$this->get(serviceFlashbag::class)->addFlashToastr(null, 'Certificats sort have been updated!');
		return $this->redirectToRoute('wsa_certificat_list');
	}

	/**
	 * @Route(
	 *		"/tier/certificat/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_certificat_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putCertificatAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/certificat/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_certificat_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchCertificatAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/tier/certificat/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_certificat_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableCertificatAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/tier/certificat/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_certificat_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteCertificatAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/certificat/delete/{id}",
	 *		name="wsa_certificat_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteCertificatAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
