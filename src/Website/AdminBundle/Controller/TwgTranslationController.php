<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\Common\Inflector\Inflector;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceForms;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\TwgTranslation;
use ModelApi\InternationalBundle\Service\serviceTwgTranslation;
use ModelApi\InternationalBundle\Form\Type\TwgTranslationAnnotateType;
use ModelApi\InternationalBundle\Service\serviceTranslation;
use ModelApi\InternationalBundle\Service\serviceLanguage;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use \Exception;

class TwgTranslationController extends EntityController {

	const ENTITY_CLASSNAME = TwgTranslation::class;
	const ENTITY_SERVICE = serviceTwgTranslation::class;
	const ENTITY_CREATE_TYPE = TwgTranslationAnnotateType::class;
	const ENTITY_UPDATE_TYPE = TwgTranslationAnnotateType::class;

	const SESSION_SEARCH_TWGTR_NAME = 'search_twgtranslations';
	const SESSION_KM_TWGTR_NAME = 'km_twgtranslations';


	/**
	 * @Route(
	 *		"/twgtranslation/km/edit",
	 *		name="wsa_twgtranslation_km_edit",
	 *		options={ "method_prefix" = false },
	 *		methods={"GET","POST"}
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function kmEditAction(Request $request) {
		$manager = $this->getManager();
		$globals = [];
		$this->compileKmDefaults($globals);
		$globals['data'] = $this->get(serviceKernel::class)->getDataBySession($globals['form_name'], $request, $globals['defaults'], function($values, $origins, $request_origins) use (&$globals, $manager) {
			// var_dump($values['domains']);die();
			// Domains
			$globals['request_origins'] = $request_origins;
			if(isset($request_origins['switch_domain']) && in_array($request_origins['switch_domain'], array_keys($globals['domains']))) {
				if(isset($request_origins['submit_only'])) {
					if(in_array($request_origins['switch_domain'], $values['domains']) && count($values['domains']) === 1) $values['domains'] = [array_key_first($globals['domains'])];
						else $values['domains'] = [$request_origins['switch_domain']];
					$values['index'] = 0;
				} else {
					if(in_array($request_origins['switch_domain'], $values['domains'])) $values['domains'] = array_filter($values['domains'], function($domain) use ($request_origins) { return $domain !== $request_origins['switch_domain']; });
						else array_push($values['domains'], $request_origins['switch_domain']);
				}
			}
			if(empty($values['domains'])) $values['domains'] = array_slice($globals['default_domains'], 0, 1);
			// Count & Index
			$count = $manager->getRepository()->countTwgTranslationsByKDL(null, $values['domains']);
			$values['index'] = (integer)$values['index'];
			if(!is_integer($values['index'])) $values['index'] = $origins['index'];
			if($values['index'] >= $count) $values['index'] = 0;
			if($values['index'] < 0) $values['index'] = $count - 1;
			// $globals['entities'] = $manager->getRepository()->getGrouppedKeytextTwgTranslationByKDL($values['index'], null, $values['domains']);
			return $values;
		});
		return $this->redirectToRoute('wsa_twgtranslation_km_list');
		// return $this->render('WebsiteAdminBundle:Pageweb:twgtranslation_km.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/km/list",
	 *		name="wsa_twgtranslation_km_list",
	 *		options={ "method_prefix" = false },
	 *		methods={"GET"}
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function kmListAction(Request $request) {
		$manager = $this->getManager();
		$globals = [];
		$this->compileKmDefaults($globals);
		$globals['data'] = $this->get(serviceKernel::class)->getDataBySession($globals['form_name'], $request, $globals['defaults']);
		$repository = $manager->getRepository();
		$globals['count'] = $repository->countTwgTranslationsByKDL(null, $globals['data']['domains']);
		$globals['entities'] = $repository->getGrouppedKeytextTwgTranslationByKDL($globals['data']['index'], null, $globals['data']['domains']);
		$globals['complete_list'] = $repository->getScalarGrouppedKeytextTwgTranslationByKDL(null, $globals['data']['domains']);
		// Get forms
		foreach ($this->get(serviceLanguage::class)->getAllLanguages(true) as $language) {
			$globals['languages'][$language['fullname']] = $language;
		}
		$globals['model'] = $manager->getModel();
		$globals['forms'] = $this->getEntitiesForms($globals['entities'], $globals['languages']);
		return $this->render('WebsiteAdminBundle:Pageweb:twgtranslation_km.html.twig', $globals);
	}

	protected function getEntitiesForms($entities, $languages) {
		if(!count($entities)) return [];
		$formContexts = 'saisie_km';
		$forms = [];
		// Add Update forms for entites
		$locales = [];
		$keytext = null;
		$domain = null;
		$typecontent = null;
		foreach ($entities as $entity) {
			$params = [
				'ACTION_ROUTE' => ['route' => 'wsa_twgtranslation_km_put', 'params' => ['id' => $entity->getId()]],
				'SUBMITS' => false,
				'LARGE_FOOTER' => false,
			];
			$keytext ??= $entity->getKeytext();
			$domain ??= $entity->getDomain();
			$typecontent ??= $entity->getTypeContent();
			if($entity->getKeytext() !== $keytext || $entity->getDomain() !== $domain) {
				throw new Exception("Error ".__METHOD__."(): all entities must have same keytext and domain. Needed ".json_encode([$keytext, $domain]).", but got ".json_encode([$entity->getKeytext(), $entity->getDomain()])."!", 1);
			}
			$params['HIDDEN_DATA'] = $entity->serialize();
			$forms[$entity->getTranslatableLocale()] = $this->get(serviceForms::class)->getUpdateForm($entity, $this->getUser(), (array)$formContexts, 'put', $params)->createView();
			$locales[$entity->getTranslatableLocale()] = $entity->getTranslatableLocale();
		}
		// Add create forms for missing entities
		$params = [
			'ACTION_ROUTE' => ['route' => 'wsa_twgtranslation_km_post', 'params' => null],
			'SUBMITS' => false,
			'LARGE_FOOTER' => false,
		];
		foreach ($languages as $fullname => $language) {
			if(!array_key_exists($language['fullname'], $forms)) {
				$entity = $this->getManager()->createNew([], function($entity) use ($keytext, $domain, $typecontent, $language) {
					$entity->setKeytext($keytext);
					$entity->setDomain($domain);
					$entity->setTypecontent($typecontent);
					$entity->setTranslatableLocale($language['fullname']);
				});
				$params['HIDDEN_DATA'] = $entity->serialize();
				// $errors = $this->get('validator')->validate($entity);
				// if(count($errors) > 0) throw new Exception("Error ".__METHOD__."(): ".((string)$errors), 1);
				$forms[$language['fullname']] = $this->get(serviceForms::class)->getCreateForm($entity, $this->getUser(), (array)$formContexts, $params)->createView();
			}
		}
		return $forms;
	}

	protected function compileKmDefaults(&$globals) {
		$manager = $this->getManager();
		$globals['model'] = $manager->getModel(static::ENTITY_CLASSNAME);
		$globals['form_name'] = static::SESSION_KM_TWGTR_NAME;
		$domains = $manager->getDomaineNames();
		foreach ($domains as $domain) {
			$globals['domains'][$domain] = $manager->getRepository()->countTwgTranslationsByKDL(null, $domain);
		}
		// echo('<pre>'); var_dump($domains); echo('</pre>');
		// get data
		// $globals['default_domains'] = array_filter($domains, function($domain) {
		// 	return !in_array($domain, ['admin','validators']);
		// });
		$globals['default_domains'] = array_slice($domains, 0, 1);
		$globals['defaults'] = ['index' => 0, 'domains' => $globals['default_domains']];
		return $globals;
	}

	/**
	 * @Route(
	 *		"/twgtranslation/km/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twgtranslation_km_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function kmPutTwgTranslationAction($id, Request $request, $context = null) {
		$formContexts = 'saisie_km';
		$manager = $this->getManager();
		$globals['entity'] = $this->getEntityById($id);
		$params = [
			'ACTION_ROUTE' => ['route' => 'wsa_twgtranslation_km_put', 'params' => null],
			'SUBMITS' => false,
			'LARGE_FOOTER' => false,
		];
		$globals['form'] = $this->get(serviceForms::class)->getUpdateForm($globals['entity'], $this->getUser(), array_unique(array_merge((array)$context, (array)$formContexts)), 'put', $params);
		$globals['form']->handleRequest($request, true);
		if($globals['form']->isSubmitted() && $globals['form']->isValid()) {
			$this->getManager()->getEntityManager()->flush();
			switch ($this->getSubmitName($request)) {
				default: return $this->redirectToRoute('wsa_twgtranslation_km_list'); break;
			}
		}
		$this->compileKmDefaults($globals);
		$globals['data'] = $this->get(serviceKernel::class)->getDataBySession($globals['form_name'], $request, $globals['defaults']);
		$globals['count'] = $manager->getRepository()->countTwgTranslationsByKDL(null, $globals['data']['domains']);
		$globals['entities'] = $manager->getRepository()->getGrouppedKeytextTwgTranslationByKDL($globals['data']['index'], null, $globals['data']['domains']);
		// Get forms
		foreach ($this->get(serviceLanguage::class)->getAllLanguages(true) as $language) {
			$globals['languages'][$language['fullname']] = $language;
		}
		$globals['forms'] = $this->getEntitiesForms($globals['entities'], $globals['languages']);
		foreach ($globals['forms'] as $key => $form) {
			if($form->vars['data'] === $globals['form']->getData()) $globals['forms'][$key] = $globals['form']->createView();
		}
		return $this->render('WebsiteAdminBundle:Pageweb:twgtranslation_km.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/km/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twgtranslation_km_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function kmPostTwgTranslationAction(Request $request, $context = null) {
		$formContexts = 'saisie_km';
		$manager = $this->getManager();
		// $hidden_data = $request->request->get('hidden_data');
		// echo('<pre>');var_dump(unserialize($hidden_data));die('</pre>');
		$hidden_data = unserialize($request->request->get('hidden_data'));
		$globals['entity'] = $this->getManager()->createNew([], function($entity) use ($hidden_data) {
			// echo('<pre>');var_dump($hidden_data);echo('</pre>');
			if(is_array($hidden_data)) {
				foreach ($hidden_data as $attribute => $value) {
					$setter = Inflector::camelize('set_'.$attribute);
					if(method_exists($entity, $setter)) {
						$entity->$setter($value);
						// echo('<h2>'.get_class($entity).'->'.$setter.'('.json_encode($value).')</h2>');
					}
				}
			}
			// echo('<pre>');var_dump(unserialize($entity->serialize()));die('</pre>');
		});
		$params = [
			'ACTION_ROUTE' => ['route' => 'wsa_twgtranslation_km_post', 'params' => null],
			'SUBMITS' => false,
			'LARGE_FOOTER' => false,
		];
		$globals['form'] = $this->get(serviceForms::class)->getCreateForm($globals['entity'], $this->getUser(), array_unique(array_merge((array)$context, (array)$formContexts)), $params);
		$globals['form']->handleRequest($request, true);
		if($globals['form']->isSubmitted() && $globals['form']->isValid()) {
			$this->getManager()->getEntityManager()->persist($globals['form']->getData());
			$this->getManager()->getEntityManager()->flush();
			switch ($this->getSubmitName($request)) {
				default: return $this->redirectToRoute('wsa_twgtranslation_km_list'); break;
			}
		}
		$this->compileKmDefaults($globals);
		$globals['data'] = $this->get(serviceKernel::class)->getDataBySession($globals['form_name'], $request, $globals['defaults']);
		$globals['count'] = $manager->getRepository()->countTwgTranslationsByKDL(null, $globals['data']['domains']);
		$globals['entities'] = $manager->getRepository()->getGrouppedKeytextTwgTranslationByKDL($globals['data']['index'], null, $globals['data']['domains']);
		// Get forms
		foreach ($this->get(serviceLanguage::class)->getAllLanguages(true) as $language) {
			$globals['languages'][$language['fullname']] = $language;
		}
		$globals['forms'] = $this->getEntitiesForms($globals['entities'], $globals['languages']);
		foreach ($globals['forms'] as $key => $form) {
			if($form->vars['data'] === $globals['form']->getData()) $globals['forms'][$key] = $globals['form']->createView();
		}
		return $this->render('WebsiteAdminBundle:Pageweb:twgtranslation_km.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/list",
	 *		name="wsa_twgtranslation_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getTwgTranslationsAction(Request $request) {
		$globals['defaults'] = ['nb' => 50, 'domain' => $this->getManager()->getDefaultDomaineName(), 'keytext' => null, 'mode' => null];
		$globals['search'] = $this->get(serviceKernel::class)->getDataBySession(static::SESSION_SEARCH_TWGTR_NAME, $request, $globals['defaults'], function($values) {
			if(empty($values['keytext'])) $values['mode'] = null;
			return $values;
		});
		$globals['entities'] = $this->getManager()->findByKeytext($globals['search']['keytext'], $globals['search']['mode'], $globals['search']['nb'], $globals['search']['domain']);
		$globals['domains'] = $this->getManager()->getDomaineNames();
		$globals['modes'] = $this->getManager()->getRegexModes();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/show/{id}",
	 *		name="wsa_twgtranslation_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getTwgTranslationAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twgtranslation_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function createTwgTranslationAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twgtranslation_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function postTwgTranslationAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/update-bykeys/{keytext}/{domain}/{method}",
	 *		defaults={"method" = "put"},
	 *		name="wsa_twgtranslation_update_bykeys",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateTwgTranslationBykeysAction($keytext, $domain, Request $request, $method = 'put') {
		$keytext = urldecode($keytext);
		$globals['entity'] = $this->getManager()->getRepository()->findOneBy(['keytext' => $keytext, 'domain' => $domain]);
		// If does not exist
		if(empty($globals['entity'])) {
			$globals['entity'] = $this->getManager()->createNew([], function($entity) use ($keytext, $domain) {
				$entity->setKeytext($keytext);
				$entity->setDomain($domain);
			});
			return $this->createEntityAction($request, 'default', $globals);
		}
		return $this->updateEntityAction(0, $request, 'default', $method, $globals);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_twgtranslation_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateTwgTranslationAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twgtranslation_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putTwgTranslationAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twgtranslation_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchTwgTranslationAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/twgtranslation/reset-cache",
	 *		name="wsa_twgtranslation_reset_cache",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function resetCacheTwgTranslationAction(Request $request) {
		$this->get(serviceLanguage::class)->resetCacheLanguages([]);
		$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Cache has been reseted for all domains!');
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/twgtranslation/delete/{id}",
	 *		name="wsa_twgtranslation_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function deleteTwgTranslationAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}

	// /**
	//  * @Route(
	//  *		"/twgtranslation/softdelete/{id}/{status}",
	//  *		defaults={"status" = 1},
	//  *		name="wsa_twgtranslation_softdelete",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_TRANSLATOR')")
	//  */
	// public function softdeleteTwgTranslationAction($id, $status = 1, Request $request) {
	// 	// return $this->softdeleteEntityAction($id, $status, $request);
	// 	$entity = $this->getEntityById($id);
	// 	$em = $this->get(serviceEntities::class)->getEntityManager();
	// 	$em->remove($entity);
	// 	$em->flush();
	// 	return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	// }


	/**
	 * @Route(
	 *		"/admin-switch-translation",
	 *		name="wsa_language_switchmode",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function switchTranslationModeAction(Request $request) {
		$this->get(serviceTranslation::class)->switchTranslatorMode();
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}









}
