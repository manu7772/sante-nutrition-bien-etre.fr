<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceForms;
// EventBundle
use ModelApi\EventBundle\Service\serviceJourney;
use ModelApi\EventBundle\Service\serviceEvent;
use ModelApi\EventBundle\Entity\Event;
use ModelApi\EventBundle\Entity\Journey;
use ModelApi\EventBundle\Form\Type\JourneyAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use DateTime;

class JourneyController extends EntityController {

	const ENTITY_CLASSNAME = Journey::class;
	const ENTITY_SERVICE = serviceJourney::class;
	const ENTITY_CREATE_TYPE = JourneyAnnotateType::class;
	const ENTITY_UPDATE_TYPE = JourneyAnnotateType::class;

	/**
	 * @Route(
	 *		"/event/journey/list/{year}",
	 *		defaults={"year" = null},
	 *		name="wsa_journey_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getJourneysAction($year = null, Request $request) {
		// echo('<pre>');var_dump(new DateTime(null));die('<pre>');
		if(empty($year)) $year = $this->get(serviceContext::class)->getContextYear();
		$globals['year'] = $year;
		$globals['entities'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findOrderedByDate($year);
		$globals['years'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findYears();
		$globals['nextdate'] = $this->get(static::ENTITY_SERVICE)->getRepository()->getNextEmptyDateOfYear();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/event/journey/show/{id}",
	 *		name="wsa_journey_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getJourneyAction($id, Request $request) {
		$globals['entity'] = $id instanceOf Journey ? $id : $this->getEntityById($id);
		$globals['parcForm'] = $this->get(serviceForms::class)->getUpdateForm($globals['entity'], $this->getUser(), 'journey_changeparc')->createView();
		return $this->getEntityAction($id, $request, $globals);
	}

	/**
	 * @Route(
	 *		"/event/journey/create/{context}",
	 *		defaults={"context" = "journey_changeparc"},
	 *		name="wsa_journey_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createJourneyAction(Request $request, $context = 'journey_changeparc') {
		$globals = array();
		$globals['entity'] = $this->getManager()->createNew();
		$start = $this->get(static::ENTITY_SERVICE)->getRepository()->getNextEmptyDateOfYear();
		$start->setTime(Journey::DEFAULT_H_START['H'], Journey::DEFAULT_H_START['i'], Journey::DEFAULT_H_START['s']);
		$end = clone $start;
		$end->setTime(Journey::DEFAULT_H_END['H'], Journey::DEFAULT_H_END['i'], Journey::DEFAULT_H_END['s']);
		$globals['entity']->setEnd($end);
		$globals['entity']->setStart($start);
		return $this->createEntityAction($request, $context, $globals);
	}

	/**
	 * @Route(
	 *		"/event/journey/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_journey_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postJourneyAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/journey/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_journey_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateJourneyAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/event/journey/put/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_journey_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putJourneyAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/event/journey/patch/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_journey_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchJourneyAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/event/journey/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_journey_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableJourneyAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/journey/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_journey_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteJourneyAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/journey/delete/{id}",
	 *		name="wsa_journey_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteJourneyAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
