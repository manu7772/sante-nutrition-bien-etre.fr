<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// UserBundle
use ModelApi\UserBundle\Service\servicePartenaire;
use ModelApi\UserBundle\Entity\Partenaire;
use ModelApi\UserBundle\Form\Type\PartenaireAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PartenaireController extends EntityController {

	const ENTITY_CLASSNAME = Partenaire::class;
	const ENTITY_SERVICE = servicePartenaire::class;
	const ENTITY_CREATE_TYPE = PartenaireAnnotateType::class;
	const ENTITY_UPDATE_TYPE = PartenaireAnnotateType::class;

	/**
	 * @Route(
	 *		"/tier/partenaire/list",
	 *		name="wsa_partenaire_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getPartenairesAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/tier/partenaire/show/{id}",
	 *		name="wsa_partenaire_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getPartenaireAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/tier/partenaire/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_partenaire_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createPartenaireAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/partenaire/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_partenaire_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postPartenaireAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/partenaire/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_partenaire_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updatePartenaireAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/partenaires/sort",
	 *		name="wsa_partenaire_sort",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function sortPartenairesAction(Request $request) {
		$em = $this->get(serviceEntities::class)->getEntityManager();
		$entities = $this->getRepository(static::ENTITY_CLASSNAME)->findAll();
		$index = 0;
		foreach ($entities as $entity) {
			$entity->setSortpartenaire($index++);
		}
		$em->flush();
		$this->get(serviceFlashbag::class)->addFlashToastr(null, 'Partenaires sort have been updated!');
		return $this->redirectToRoute('wsa_partenaire_list');
	}

	/**
	 * @Route(
	 *		"/tier/partenaire/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_partenaire_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putPartenaireAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/partenaire/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_partenaire_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchPartenaireAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/tier/partenaire/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_partenaire_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enablePartenaireAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/tier/partenaire/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_partenaire_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeletePartenaireAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/partenaire/delete/{id}",
	 *		name="wsa_partenaire_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deletePartenaireAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
