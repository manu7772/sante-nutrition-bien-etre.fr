<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EventController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceContext;
// EventBundle
use ModelApi\EventBundle\Service\serviceParc;
use ModelApi\EventBundle\Service\serviceJourney;
use ModelApi\EventBundle\Entity\Parc;
use ModelApi\EventBundle\Form\Type\ParcAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ParcController extends EventController {

	const ENTITY_CLASSNAME = Parc::class;
	const ENTITY_SERVICE = serviceParc::class;
	const ENTITY_CREATE_TYPE = ParcAnnotateType::class;
	const ENTITY_UPDATE_TYPE = ParcAnnotateType::class;

	/**
	 * @Route(
	 *		"/event/parc/list/{year}",
	 *		defaults={"year" = null},
	 *		name="wsa_parc_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getParcsAction($year = null, Request $request) {
		// if(empty($year)) $year = $this->get(serviceContext::class)->getContextYear();
		$globals['year'] = $year;
		$globals['years'] = $this->get(serviceJourney::class)->getRepository()->findYears();
		if(in_array($globals['year'], $globals['years'])) {
			$globals['year_parcs'] = $this->get(serviceJourney::class)->getRepository()->findOrderedByDate($globals['year']);
		}
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/event/parc/show/{id}",
	 *		name="wsa_parc_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getParcAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/event/parc/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_parc_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createParcAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/parc/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_parc_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postParcAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/parc/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_parc_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateParcAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/event/parc/auto-update/{id}",
	 *		name="wsa_parc_auto_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function autoUpdateParcAction($id, Request $request) {
		return $this->autoUpdateEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/event/parc/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_parc_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putParcAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/event/parc/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_parc_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchParcAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/event/parc/prefered/{id}/{status}",
	 *		name="wsa_parc_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function preferedParcAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/parc/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_parc_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableParcAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/parc/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_parc_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteParcAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/parc/delete/{id}",
	 *		name="wsa_parc_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteParcAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}




	// /**
	//  * @Route(
	//  *		"/event/parc/year/{year}",
	//  *		name="wsa_parc_year",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_EDITOR')")
	//  */
	// public function yearParcsAction($year, Request $request) {
	// 	$globals['year'] = $year;
	// 	$globals['years'] = $this->get(serviceJourney::class)->getRepository()->findYears();
	// 	return $this->getEntitysAction($request, $globals);
	// }





}
