<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// UserBundle
use ModelApi\UserBundle\Entity\Group;
use ModelApi\UserBundle\Service\serviceGroup;
use ModelApi\UserBundle\Service\serviceTier;
use ModelApi\UserBundle\Form\Type\GroupAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class GroupController extends EntityController {

	const ENTITY_CLASSNAME = Group::class;
	const ENTITY_SERVICE = serviceGroup::class;
	const ENTITY_CREATE_TYPE = GroupAnnotateType::class;
	const ENTITY_UPDATE_TYPE = GroupAnnotateType::class;

	/**
	 * @Route(
	 *		"/user/group/list",
	 *		name="wsa_group_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getGroupsAction(Request $request) {
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->findByTier($this->getUser());
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/user/group/show/{id}",
	 *		name="wsa_group_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getGroupAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/user/group/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_group_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createGroupAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/user/group/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_group_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postGroupAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/user/group/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_group_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function updateGroupAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/user/group/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_group_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function putGroupAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/user/group/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_group_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function patchGroupAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/user/group/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_group_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableGroupAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/user/group/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_group_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteGroupAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/group/delete/{id}",
	 *		name="wsa_group_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteGroupAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
