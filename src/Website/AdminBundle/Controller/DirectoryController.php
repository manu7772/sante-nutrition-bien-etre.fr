<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
// use Website\AdminBundle\Controller\BasedirectoryController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// FileBundle
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\Basedirectory;
use ModelApi\FileBundle\Service\serviceItem;
use ModelApi\FileBundle\Service\serviceDirectory;
use ModelApi\FileBundle\Service\serviceBasedirectory;
use ModelApi\FileBundle\Form\Type\DirectoryAnnotateType;
// CrudsBundle
use ModelApi\CrudsBundle\Entity\Action;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class DirectoryController extends EntityController {

	const ENTITY_CLASSNAME = Directory::class;
	const ENTITY_SERVICE = serviceDirectory::class;
	const ENTITY_CREATE_TYPE = DirectoryAnnotateType::class;
	const ENTITY_UPDATE_TYPE = DirectoryAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/directory/list",
	 *		name="wsa_directory_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getDirectorysAction(Request $request) {
		// return $this->getEntitysAction($request);
		$classname = static::ENTITY_CLASSNAME;
		$globals = [];
		$globals['entity'] = new $classname();
		$globals['entities'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findByParent(null);
		// $globals['entities'] = $this->get(serviceUser::class)->getUserDirectorys('drive/');
		$globals['Pageweb']['titleh1'] = 'Dossiers';
		return $this->render('WebsiteAdminBundle:Pageweb:directory_list.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/file/directory_owner/list/{id}",
	 *		defaults={"id" = null},
	 *		name="wsa_directory_owner",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getDirectorysOwnerAction($id = null, Request $request) {
		$globals = [];
		// $id is id or bddid of item
		if(empty($id)) {
			$globals['entity'] = $this->getUser();
		} else {
			$globals['entity'] = serviceEntities::isValidBddid($id, false) ? $this->get(serviceEntities::class)->findByBddid($id) : $this->get(serviceItem::class)->getRepository()->find($id);
		}
		if(empty($globals['entity'])) throw new NotFoundHttpException("Erreur : id ".json_encode($id)." dans ".json_encode(static::ENTITY_CLASSNAME)." n'a pu être trouvé !");
		// Test
		$action = new Action('dirs', $globals['entity'], $this->container);
		switch ($action->getStatus()) {
			case 0:
				// THIS directory
				$globals['directorys'] = [$globals['entity']];
				$globals['owner'] = $globals['entity']->getOwnerdir();
				$globals['Pageweb']['titleh1'] = 'Éléments de dossier <strong><u><i class="fa '.$globals['entity']->getIcon().' fa-1x fa-fw" style="color:'.$globals['entity']->getColor().';"></i>'.$globals['entity']->getName().'</u></strong>';
				break;
			case 1:
				// HAS directorys
				$globals['directorys'] = [$globals['entity']->getDriveDir(), $globals['entity']->getSystemDir()];
				$globals['owner'] = $globals['entity'];
				$globals['Pageweb']['titleh1'] = $globals['entity'] === $this->getUser() ? 'Mes dossiers <strong>'.$this->getUser()->getUsername().'</strong>' : 'Dossiers de <strong><u><i class="fa '.$globals['entity']->getIcon().' fa-1x fa-fw" style="color:'.$globals['entity']->getColor().';"></i>'.$globals['entity']->getName().'</u></strong>';
				break;
			case 2:
				// PARENT directory
				$globals['directorys'] = [$globals['entity']->getParent()];
				$globals['owner'] = $globals['entity']->getOwnerdir();
				$globals['Pageweb']['titleh1'] = $globals['entity'] === $this->getUser() ? 'Mes dossiers <strong>'.$this->getUser()->getUsername().'</strong>' : 'Dossier parent de <strong><u><i class="fa '.$globals['entity']->getIcon().' fa-1x fa-fw" style="color:'.$globals['entity']->getColor().';"></i>'.$globals['entity']->getName().'</u></strong>';
				break;
			default:
				$globals['directorys'] = [];
				$globals['owner'] = null;
				$globals['Pageweb']['titleh1'] = 'Cet élément n\'est pas concerné par les dossiers...';
				$this->get(serviceFlashbag::class)->addFlashToastr('error', 'Cet élément n\'a aucun dossier');
				break;
		}
		$needFlush = false;
		$globals['directorys'] = array_filter($globals['directorys'], function($item) use (&$needFlush) {
			// if($item instanceOf Basedirectory) $needFlush = $needFlush || $item->checkChildsPositions();
			return $item instanceOf Basedirectory;
		});
		// if($needFlush) $this->get(serviceEntities::class)->getEntityManager()->flush();
		$globals['actions'] = false;
		return $this->render('WebsiteAdminBundle:Pageweb:directory_owner_list.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/file/directory/show/{id}",
	 *		name="wsa_directory_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getDirectoryAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/directory/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_directory_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createDirectoryAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/directory/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_directory_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postDirectoryAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/directory/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_directory_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function updateDirectoryAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/directory/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_directory_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function putDirectoryAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/directory/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_directory_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function patchDirectoryAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/directory/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_directory_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableDirectoryAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/directory/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_directory_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteDirectoryAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/directory/delete/{id}",
	 *		name="wsa_directory_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteDirectoryAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/directory/check-childpositions/{id}",
	 *		name="wsa_directory_checkchildpositions",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function checkChildpositionsAction($id, Request $request) {
		$directory = $this->getEntityById($id);
		if($directory->checkChildsPositions()) {
			$this->get(serviceEntities::class)->getEntityManager()->flush();
			if($directory->isChildPositionsValid()) {
				// $this->get(serviceEntities::class)->getEntityManager()->flush();
				$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Position des éléments du dossier checkés !');
			} else {
				$this->get(serviceFlashbag::class)->addFlashToastr('error', 'Positions checkées, toujours désorganisées !');
			}
		} else {
			$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Position des éléments du dossier déjà valides. Aucune opération n\'a été nécessaire !');
		}
		return $this->redirectToRoute('wsa_directory_owner', ['id' => $directory->getId()]);
		// return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	// /**
	//  * @Route(
	//  *		"/file/directory/check-childs/{id}",
	//  *		name="wsa_directory_checkmissingchilds",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_ADMIN')")
	//  */
	// public function checkMissingChildsAction($id, Request $request) {
	// 	$directory = $this->getEntityById($id);
	// 	$childs = $this->get(serviceItem::class)->getRepository()->findItemsByParent($directory);
	// 	foreach ($childs as $child) {
	// 		$directory->addChild($child, serviceBasedirectory::TYPELINK_HARDLINK);
	// 	}
	// 	$this->get(serviceEntities::class)->getEntityManager()->flush();
	// 	$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Les éléments de '.json_encode($directory->getName()).' ont été récupérés !');
	// 	return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	// }





}
