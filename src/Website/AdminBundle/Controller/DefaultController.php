<?php

namespace Website\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
// use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// use ModelApi\BaseBundle\Service\serviceTools;
// use ModelApi\BaseBundle\Service\serviceKernel;
// use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Entity\Annonce;
use ModelApi\BaseBundle\Entity\Message;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceMailer;
// use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceContext;
use ModelApi\BaseBundle\Component\Context;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Service\serviceEntreprise;
// FileBundle
use ModelApi\FileBundle\Entity\Basedirectory;
// use ModelApi\FileBundle\Entity\Item;
// use ModelApi\FileBundle\Entity\File;
// use ModelApi\FileBundle\Entity\Image;
// use ModelApi\FileBundle\Entity\Pdf;
// use ModelApi\FileBundle\Entity\Video;
// use ModelApi\FileBundle\Entity\Directory;
// use ModelApi\FileBundle\Entity\NestedInterface;
// use ModelApi\FileBundle\Entity\DirectoryInterface;
// use ModelApi\FileBundle\Entity\Nestlink;
// use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceItem;
// use ModelApi\FileBundle\Service\serviceBasedirectory;
// // CrudsBundle
use ModelApi\CrudsBundle\Service\serviceCruds;
// EventBundle
use ModelApi\EventBundle\Service\serviceJourney;
// // UserBundle
// use ModelApi\UserBundle\Service\serviceUser;

// use \Traversable;

class DefaultController extends Controller {

	protected function returnMain($globals = []) {
		if($this->get('templating')->exists('WebsiteSiteBundle:Admin:Pageweb/index.html.twig')) return $this->render('WebsiteSiteBundle:Admin:Pageweb/index.html.twig', $globals);
		return $this->render('WebsiteAdminBundle:Pageweb:index.html.twig', $globals);
	}

	/**
	 * @Route("/", name="website_admin_homepage", options={ "method_prefix" = false }, methods={"GET"})
	 * @Security("has_role('ROLE_USER')")
	 */
	public function indexAction(Request $request) {

		// $mail_data = [
		// 	'subject' => 'mail de test',
		// 	'text' => '<div>Bonjour, voici un <strong>mail</strong> de test !!!</div>',
		// 	'user' => $this->getUser(),
		// 	'entreprise' => $this->container->get(serviceEntreprise::class)->getPreferedEntreprise(),
		// ];
		// $this->get(serviceMailer::class)->sendMessage(
		// 	$this->getUser(),
		// 	$mail_data,
		// );

		// $basedirectorys = $this->get(serviceEntities::class)->getRepository(Basedirectory::class)->findAll();
		// foreach($basedirectorys as $bd) $bd->restoreLinkdatas();
		// $this->get(serviceEntities::class)->getEntityManager()->flush();

		// return $this->getDefaultData();
		// $this->get(serviceFlashbag::class)->addFlashToastr('info', 'Bonjour !');
		// $this->get(serviceCache::class)->clearEntityCache();
		// $tier = $this->get(serviceContext::class)->getEnvironment();
		// $shortnames = $tier instanceOf User ? ['Annonce','Blog','Entreprise','Message','Pdf','Popup','Video'] : ['Annonce','Blog','Message','Popup'];
		// $globals['user_info'] = $this->get(serviceItem::class)->countItemsByClass($shortnames, $tier instanceOf User ? $tier : null);
		$globals['years'] = $this->get(serviceJourney::class)->getRepository()->findYears();
		// $globals['doublons'] = $this->get(serviceBasedirectory::class)->findDoublons(false);
		return $this->returnMain($globals);
	}

	/**
	 * @Route(
	 *		"/admin/clear-cache/entity",
	 *		name="wsa_clearcache_entity",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 */
	public function clearEntityCacheAction(Request $request) {
		$globals = [];
		$this->get(serviceCruds::class)->clearAnnotations();
		$this->get(serviceFlashbag::class)->addFlashToastr('info', 'Entity cache cleared!');
		return $this->returnMain($globals);
	}

	/**
	 * @Route(
	 *		"/admin/test/context",
	 *		name="wsa_test_context",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 */
	public function testContextAction(Request $request) {
		$globals['context'] = new Context($this->container);
		return $this->returnMain($globals);
	}





}