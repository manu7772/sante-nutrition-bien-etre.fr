<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Entity\Userrequest;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceUserrequest;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Form\Type\UserrequestAnnotateType;
// UserBundle
use ModelApi\UserBundle\Entity\User;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserrequestController extends EntityController {

	const ENTITY_CLASSNAME = Userrequest::class;
	const ENTITY_SERVICE = serviceUserrequest::class;
	const ENTITY_CREATE_TYPE = UserrequestAnnotateType::class;
	const ENTITY_UPDATE_TYPE = UserrequestAnnotateType::class;

	/**
	 * @Route(
	 *		"/userrequest/list",
	 *		name="wsa_userrequest_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function getUserrequestsAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/userrequest/validate/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_userrequest_validate",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function validateUserrequestAction(Userrequest $id, $status = 1, Request $request) {
		if(!empty($id)) {
			$this->get(serviceUserrequest::class)->validateUserrequest($id, $status, $this->getUser(), true);
			switch ($id->getReqstatus()) {
				case 0:
					$this->get(serviceFlashbag::class)->addFlashSweet('info', 'La requête a été réinitialisée.');
					break;
				case 1:
					$this->get(serviceFlashbag::class)->addFlashSweet('success', 'La requête a bien été validée.');
					break;
				case 2:
					$this->get(serviceFlashbag::class)->addFlashSweet('warning', 'La requête a bien été rejetée.');
					break;
				default:
					$this->get(serviceFlashbag::class)->addFlashSweet('error', 'Le statut de la requête est invalide !');
					break;
			}
		} else {
			$this->get(serviceFlashbag::class)->addFlashSweet('error', 'Requête introuvable, votre demande n\'a pu être traitée !');
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/userrequest/delete/{id}",
	 *		name="wsa_userrequest_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function deleteUserrequestAction(Userrequest $id, Request $request) {
		$globals = [];
		if(!empty($id)) $globals['entity'] = $id;
		return $this->deleteEntityAction($id, $request, $globals);
	}


	/****************************************************************************************************************/
	/*** USER REQUESTS
	/****************************************************************************************************************/


	/**
	 * @Route(
	 *		"/userrequest/role-collab/{id}/{requester}",
	 *		defaults={"requester" = null},
	 *		name="wsa_userrequest_rolecollab",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function requestRolecollabAction(User $id, User $requester = null, Request $request) {
		$userrquest = $this->get(serviceUserrequest::class)->namedRequest_create(serviceUserrequest::ROLECOLLAB_NAME, $id, $requester ?? $this->getUser());
		if(!empty($userrquest)) {
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($userrquest);
			$em->flush();
			$this->get(serviceFlashbag::class)->addFlashSweet('success', 'La demande a été enregistrée.');
		} else {
			$this->get(serviceFlashbag::class)->addFlashSweet('warning', 'La demande n\'a pu être enregistrée.');
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}







}
