<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceReseausocial;
use ModelApi\BaseBundle\Entity\Reseausocial;
use ModelApi\BaseBundle\Form\Type\ReseausocialAnnotateType;
// UserBundle
use ModelApi\UserBundle\Entity\Tier;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ReseausocialController extends EntityController {

	const ENTITY_CLASSNAME = Reseausocial::class;
	const ENTITY_SERVICE = serviceReseausocial::class;
	const ENTITY_CREATE_TYPE = ReseausocialAnnotateType::class;
	const ENTITY_UPDATE_TYPE = ReseausocialAnnotateType::class;

	/**
	 * @Route(
	 *		"/reseausocial/list/{tier}",
	 *		name="wsa_reseausocial_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getReseausocialsAction(Tier $tier = null, Request $request) {
		$globals = [];
		// if(!empty($tier)) $globals['entities'] = $tier->getReseausocials();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/reseausocial/show/{id}",
	 *		name="wsa_reseausocial_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getReseausocialAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/reseausocial/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_reseausocial_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createReseausocialAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/reseausocial/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_reseausocial_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postReseausocialAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/reseausocial/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_reseausocial_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function updateReseausocialAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/reseausocial/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_reseausocial_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function putReseausocialAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/reseausocial/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_reseausocial_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function patchReseausocialAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/reseausocial/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_reseausocial_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function enableReseausocialAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/reseausocial/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_reseausocial_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function softdeleteReseausocialAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/reseausocial/delete/{id}",
	 *		name="wsa_reseausocial_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function deleteReseausocialAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
