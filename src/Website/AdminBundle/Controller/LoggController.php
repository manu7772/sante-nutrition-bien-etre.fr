<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceKernel;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceLogg;
use ModelApi\BaseBundle\Entity\Logg;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LoggController extends EntityController {

	const ENTITY_CLASSNAME = Logg::class;
	const ENTITY_SERVICE = serviceLogg::class;

	const SESSION_SEARCH_LOGGS_NAME = 'search_loggs';

	/**
	 * @Route(
	 *		"/logg/list/{nb}",
	 * 		defaults={ "nb" = null },
	 *		name="wsa_logg_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function getLoggsAction($nb = null, Request $request) {
		$globals['search']['nb'] = (integer)$nb;
		// if(empty($globals['search']['nb'])) $globals['search']['nb'] = 50;
		$globals['search']['params'] = $request->query->all();
		// Session search data
		$serviceKernel = $this->get(serviceKernel::class);
		// $globals['session_loggs_data'] = $serviceKernel->getSessionValue(static::SESSION_SEARCH_LOGGS_NAME, ['nb' => 50, 'params' => []]);
		$globals['session_loggs_data'] = $serviceKernel->getSessionValue(static::SESSION_SEARCH_LOGGS_NAME);
		if(!is_array($globals['session_loggs_data'])) $globals['session_loggs_data'] = ['nb' => 50, 'params' => []];
		if(!array_key_exists('nb', $globals['session_loggs_data']) || !is_integer($globals['session_loggs_data']['nb'])) $globals['session_loggs_data']['nb'] = 50;
		if(!array_key_exists('params', $globals['session_loggs_data']) || !is_array($globals['session_loggs_data']['params'])) $globals['session_loggs_data']['params'] = [];
		// Compute search data
		if(strtolower($nb) == 'reset') {
			$globals['search']['nb'] = 50;
			$globals['search']['params'] = [];
		} else {
			if(array_key_exists('reset', $globals['search']['params'])) $globals['search']['params'] = [];
				else if(empty($globals['search']['params'])) $globals['search']['params'] = $globals['session_loggs_data']['params'];
			if(empty($globals['search']['nb'])) $globals['search']['nb'] = $globals['session_loggs_data']['nb'];
		}
		$globals['session_loggs_data'] = $globals['search'];
		$serviceKernel->setSessionValue(static::SESSION_SEARCH_LOGGS_NAME, $globals['session_loggs_data']);
		$globals['entities'] = $this->getManager()->getRepository()->findSearch($globals['search']['nb'], $globals['search']['params']);
		// Stats
		$globals['time_average'] = 0;
		if(count($globals['entities'])) {
			foreach ($globals['entities'] as $logg) $globals['time_average'] += $logg->getDuring();
			$globals['time_average'] = $globals['time_average'] / count($globals['entities']);
		}
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/logg/show/{id}",
	 *		name="wsa_logg_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function getLoggAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/logg/reset",
	 *		name="wsa_logg_reset",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function resetLoggsAction(Request $request) {
		$loggs = $this->getRepository()->findAll();
		$em = $this->get(serviceEntities::class)->getEntityManager();
		foreach ($loggs as $logg) {
			if($logg->isCurrent()) $em->detach($logg);
				else $em->remove($logg);
		}
		try {
			$em->flush();
		} catch (Exception $e) {
			// echo($e->getMessage());
			$this->get(serviceFlashbag::class)->addFlashToastr('error', $e->getMessage());
			return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
		}
		$this->get(serviceFlashbag::class)->addFlashToastr('success', 'All loggs have been deleted!');
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}








}
