<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTelephon;
use ModelApi\BaseBundle\Entity\Telephon;
use ModelApi\BaseBundle\Form\Type\TelephonAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TelephonController extends EntityController {

	const ENTITY_CLASSNAME = Telephon::class;
	const ENTITY_SERVICE = serviceTelephon::class;
	const ENTITY_CREATE_TYPE = TelephonAnnotateType::class;
	const ENTITY_UPDATE_TYPE = TelephonAnnotateType::class;

	/**
	 * @Route(
	 *		"/telephon/list",
	 *		name="wsa_telephon_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getTelephonsAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/telephon/show/{id}",
	 *		name="wsa_telephon_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getTelephonAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/telephon/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_telephon_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function createTelephonAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/telephon/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_telephon_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function postTelephonAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/telephon/update/{id}/{context}/{method}",
	 * 		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_telephon_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function updateTelephonAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, 'default', $method);
	}

	/**
	 * @Route(
	 *		"/telephon/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_telephon_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function putTelephonAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/telephon/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_telephon_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function patchTelephonAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/telephon/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_telephon_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function enableTelephonAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/telephon/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_telephon_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function softdeleteTelephonAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/telephon/delete/{id}",
	 *		name="wsa_telephon_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function deleteTelephonAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
