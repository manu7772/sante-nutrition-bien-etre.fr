<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceTypitem;
use ModelApi\BaseBundle\Entity\Typitem;
use ModelApi\BaseBundle\Form\Type\TypitemAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TypitemController extends EntityController {

	const ENTITY_CLASSNAME = Typitem::class;
	const ENTITY_SERVICE = serviceTypitem::class;
	const ENTITY_CREATE_TYPE = TypitemAnnotateType::class;
	const ENTITY_UPDATE_TYPE = TypitemAnnotateType::class;

	/**
	 * @Route(
	 *		"/typitem/list",
	 *		name="wsa_typitem_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getTypitemsAction(Request $request) {
		// $globals['groups_descriptions'] = $this->get(static::ENTITY_SERVICE)->getTypitemsDescriptions(null, true);
		$globals['grouped_entities'] = $this->get(static::ENTITY_SERVICE)->getGroupedTypitems();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/typitem/show/{id}",
	 *		name="wsa_typitem_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getTypitemAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/typitem/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_typitem_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createTypitemAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/typitem/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_typitem_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postTypitemAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/typitem/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_typitem_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateTypitemAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/typitem/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_typitem_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putTypitemAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/typitem/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_typitem_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchTypitemAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/typitem/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_typitem_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableTypitemAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/typitem/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_typitem_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteTypitemAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/typitem/delete/{id}",
	 *		name="wsa_typitem_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteTypitemAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
