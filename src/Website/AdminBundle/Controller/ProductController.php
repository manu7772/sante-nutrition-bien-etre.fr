<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// MarketBundle
use ModelApi\MarketBundle\Service\serviceProduct;
use ModelApi\MarketBundle\Entity\Baseprod;
use ModelApi\MarketBundle\Entity\Product;
use ModelApi\MarketBundle\Form\Type\ProductAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProductController extends EntityController {

	const ENTITY_CLASSNAME = Product::class;
	const ENTITY_SERVICE = serviceProduct::class;
	const ENTITY_CREATE_TYPE = ProductAnnotateType::class;
	const ENTITY_UPDATE_TYPE = ProductAnnotateType::class;

	/**
	 * @Route(
	 *		"/product/list",
	 *		name="wsa_product_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getProductsAction(Request $request) {
		$globals['entities'] = $this->getRepository(static::ENTITY_CLASSNAME)->findRoots();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/product/show/{id}",
	 *		name="wsa_product_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getProductAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/product/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_product_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createProductAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/product/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_product_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postProductAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/product/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_product_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateProductAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/products/sort",
	 *		name="wsa_product_sort",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function sortProductsAction(Request $request) {
		$em = $this->get(serviceEntities::class)->getEntityManager();
		$entities = $this->getRepository(static::ENTITY_CLASSNAME)->findAll();
		$index = ['orphan' => 0];
		foreach ($entities as $entity) {
			$parent = $entity->getParentprod();
			if($parent instanceOf Baseprod) {
				$index[$parent->getId()] ??= 0;
				$entity->setSortprod($index[$parent->getId()]++);
			} else {
				$entity->setSortprod($index['orphan']++);
			}
		}
		$em->flush();
		$this->get(serviceFlashbag::class)->addFlashToastr(null, 'Products sort have been updated!');
		return $this->redirectToRoute('wsa_product_list');
	}

	/**
	 * @Route(
	 *		"/product/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_product_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putProductAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/product/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_product_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchProductAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/product/prefered/{id}/{status}",
	 *		name="wsa_product_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function preferedProductAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/product/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_product_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableProductAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/product/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_product_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteProductAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/product/delete/{id}",
	 *		name="wsa_product_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteProductAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
