<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
// use Website\AdminBundle\Controller\BasedirectoryController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// FileBundle
use ModelApi\FileBundle\Entity\Menu;
use ModelApi\FileBundle\Service\serviceMenu;
use ModelApi\FileBundle\Form\Type\MenuAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MenuController extends EntityController {

	const ENTITY_CLASSNAME = Menu::class;
	const ENTITY_SERVICE = serviceMenu::class;
	const ENTITY_CREATE_TYPE = MenuAnnotateType::class;
	const ENTITY_UPDATE_TYPE = MenuAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/menu/list",
	 *		name="wsa_menu_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getMenusAction(Request $request) {
		// return $this->getEntitysAction($request);
		$classname = static::ENTITY_CLASSNAME;
		$globals = [];
		$globals['model'] = new $classname();
		$globals['entities'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findByForadminlist();
		// $globals['entities'] = $this->get(serviceUser::class)->getUserMenus('drive/');
		$globals['Pageweb']['titleh1'] = 'Dossiers';
		return $this->render('WebsiteAdminBundle:Pageweb:menu_list.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/file/menu_owner/list/{bddid}",
	 *		defaults={"bddid" = null},
	 *		name="wsa_menu_owner",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getMenusOwnerAction($bddid = null, Request $request) {
		$globals = [];
		$globals['entity'] = is_string($bddid) ? $this->get(serviceEntities::class)->findByBddid($bddid) : $this->getUser();
		$globals['Pageweb']['titleh1'] = $globals['entity'] === $this->getUser() ? 'Mes dossiers' : 'Dossiers de '.$globals['entity']->__toString();
		return $this->render('WebsiteAdminBundle:Pageweb:menu_owner_list.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/file/menu/show/{id}",
	 *		name="wsa_menu_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getMenuAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/menu/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_menu_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createMenuAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/menu/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_menu_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postMenuAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/menu/update/cachedchilds/{id}",
	 *		name="wsa_menu_updatecachedchilds",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateCachedchildsMenuAction($id, Request $request) {
		$menu = $this->getEntityById($id);
		// $menu->_fsCache->getCacheds(true);
		$menu->getCachedchilds(true);
		$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Menu '.json_encode($menu->getName()).' mis à jour.');
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/file/menu/update/cachedchilds",
	 *		name="wsa_menu_updateallcachedchilds",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateAllCachedchildsMenuAction(Request $request) {
		// $this->get(serviceCache::class)->clearEntityCache(Menu::class);
		$this->get(serviceMenu::class)->refreshMenusCache();
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/file/menu/update/{id}/{method}/{context}",
	 *		defaults={"method" = "put", "context" = null},
	 *		name="wsa_menu_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateMenuAction($id, Request $request, $method = 'put', $context = null) {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/menu/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_menu_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putMenuAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/menu/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_menu_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchMenuAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}



	/**
	 * @Route(
	 *		"/file/menu/refresh/{id}",
	 *		name="wsa_menu_refresh",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function refreshMenuAction($id, Request $request) {
		return $this->updateCachedchildsMenuAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/menu/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_menu_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableMenuAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/menu/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_menu_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteMenuAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/menu/delete/{id}",
	 *		name="wsa_menu_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteMenuAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}


	/**
	 * @Route(
	 *		"/file/menu/check-childpositions/{id}",
	 *		name="wsa_menu_checkchildpositions",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function checkChildpositionsAction($id, Request $request) {
		$menu = $this->getEntityById($id);
		if($menu->checkChildsPositions()) {
			$this->get(serviceEntities::class)->getEntityManager()->flush();
			$menu->getCachedchilds(true);
			if($menu->isChildPositionsValid()) $this->get(serviceFlashbag::class)->addFlashToastr('success', 'Position des éléments du menu checkés !');
				else $this->get(serviceFlashbag::class)->addFlashToastr('error', 'Positions checkées, toujours désorganisées !');
		} else {
			$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Position des éléments du menu déjà valides. Aucune opération n\'a été nécessaire !');
		}
		return $this->redirectToRoute('wsa_directory_owner', ['id' => $menu->getId()]);
		// return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}





}
