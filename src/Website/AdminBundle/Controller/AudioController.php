<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Service\serviceAudio;
use ModelApi\FileBundle\Entity\Audio;
use ModelApi\FileBundle\Form\Type\AudioAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AudioController extends EntityController {

	const ENTITY_CLASSNAME = Audio::class;
	const ENTITY_SERVICE = serviceAudio::class;
	const ENTITY_CREATE_TYPE = AudioAnnotateType::class;
	const ENTITY_UPDATE_TYPE = AudioAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/audio/list",
	 *		name="wsa_audio_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getAudiosAction(Request $request) {
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->findAudiosByTier($this->getUser(), false);
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/file/audio/show/{id}",
	 *		name="wsa_audio_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getAudioAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/audio/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_audio_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createAudioAction(Request $request, $context = null) {
		$globals['source'] = $this->getManager()->getServiceParameter('source');
		return $this->createEntityAction($request, $context, $globals);
	}

	/**
	 * @Route(
	 *		"/file/audio/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_audio_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postAudioAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/audio/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_audio_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateAudioAction($id, Request $request, $method = 'put') {
		return $this->updateEntityAction($id, $request, $method);
	}

	/**
	 * @Route(
	 *		"/file/audio/put/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_audio_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putAudioAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/audio/patch/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_audio_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchAudioAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/audio/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_audio_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableAudioAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/audio/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_audio_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteAudioAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/audio/delete/{id}",
	 *		name="wsa_audio_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteAudioAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
