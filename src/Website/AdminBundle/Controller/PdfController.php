<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Service\servicePdf;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Form\Type\PdfAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PdfController extends EntityController {

	const ENTITY_CLASSNAME = Pdf::class;
	const ENTITY_SERVICE = servicePdf::class;
	const ENTITY_CREATE_TYPE = PdfAnnotateType::class;
	const ENTITY_UPDATE_TYPE = PdfAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/pdf/list",
	 *		name="wsa_pdf_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getPdfsAction(Request $request) {
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->findPdfsByTier($this->getUser(), false);
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/file/pdf/show/{id}",
	 *		name="wsa_pdf_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getPdfAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/pdf/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_pdf_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createPdfAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/pdf/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_pdf_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postPdfAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/pdf/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_pdf_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updatePdfAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/pdf/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_pdf_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putPdfAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/pdf/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_pdf_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchPdfAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/pdf/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_pdf_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enablePdfAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/pdf/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_pdf_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeletePdfAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/pdf/delete/{id}",
	 *		name="wsa_pdf_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deletePdfAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
