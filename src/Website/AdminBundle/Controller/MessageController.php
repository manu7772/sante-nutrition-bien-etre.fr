<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceMessage;
use ModelApi\BaseBundle\Entity\Message;
use ModelApi\BaseBundle\Form\Type\MessageAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MessageController extends EntityController {

	const ENTITY_CLASSNAME = Message::class;
	const ENTITY_SERVICE = serviceMessage::class;
	const ENTITY_CREATE_TYPE = MessageAnnotateType::class;
	const ENTITY_UPDATE_TYPE = MessageAnnotateType::class;

	/**
	 * @Route(
	 *		"/message/list",
	 *		name="wsa_message_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getMessagesAction(Request $request) {
		$globals['current'] = $this->getManager()->getRepository()->findCurrent();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/message/show/{id}",
	 *		name="wsa_message_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getMessageAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/message/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_message_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function createMessageAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/message/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_message_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function postMessageAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/message/update/{id}/{context}/{method}",
	 * 		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_message_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function updateMessageAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, 'default', $method);
	}

	/**
	 * @Route(
	 *		"/message/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_message_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function putMessageAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/message/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_message_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function patchMessageAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/message/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_message_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function enableMessageAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/message/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_message_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function softdeleteMessageAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"message/delete/{id}",
	 *		name="wsa_message_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function deleteMessageAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"message/send",
	 *		name="send_internal_message",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function sendMessageAction(Request $request) {
		// $this->getManager()->createNewByRequest($request);
		$this->get(serviceFlashbag::class)->addFlashSweet('success', 'Votre message a bien été envoyé !');
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}







}
