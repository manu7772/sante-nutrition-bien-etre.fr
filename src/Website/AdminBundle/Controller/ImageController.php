<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Service\serviceImage;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Form\Type\ImageAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ImageController extends EntityController {

	const ENTITY_CLASSNAME = Image::class;
	const ENTITY_SERVICE = serviceImage::class;
	const ENTITY_CREATE_TYPE = ImageAnnotateType::class;
	const ENTITY_UPDATE_TYPE = ImageAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/image/list",
	 *		name="wsa_image_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getImagesAction(Request $request) {
		$globals = [];
		$service = $this->get(static::ENTITY_SERVICE);
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/file/image/show/{id}",
	 *		name="wsa_image_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getImageAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/image/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_image_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createImageAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/image/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_image_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postImageAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/image/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_image_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function updateImageAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/image/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_image_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function putImageAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/image/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_image_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function patchImageAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/image/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_image_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableImageAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/image/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_image_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteImageAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/image/delete/{id}",
	 *		name="wsa_image_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteImageAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
