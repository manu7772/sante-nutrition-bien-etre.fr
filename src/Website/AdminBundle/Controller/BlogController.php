<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceBlog;
use ModelApi\BaseBundle\Entity\Blog;
use ModelApi\BaseBundle\Form\Type\BlogAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class BlogController extends EntityController {

	const ENTITY_CLASSNAME = Blog::class;
	const ENTITY_SERVICE = serviceBlog::class;
	const ENTITY_CREATE_TYPE = BlogAnnotateType::class;
	const ENTITY_UPDATE_TYPE = BlogAnnotateType::class;

	/**
	 * @Route(
	 *		"/blog/list",
	 *		name="wsa_blog_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getBlogsAction(Request $request) {
		$globals['current'] = $this->getManager()->getRepository()->findCurrent();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/blog/show/{id}",
	 *		name="wsa_blog_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getBlogAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/blog/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_blog_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createBlogAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/blog/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_blog_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postBlogAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/blog/update/{id}/{context}/{method}",
	 * 		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_blog_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateBlogAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, 'default', $method);
	}

	/**
	 * @Route(
	 *		"/blog/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_blog_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putBlogAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/blog/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_blog_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchBlogAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/blog/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_blog_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableBlogAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/blog/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_blog_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteBlogAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/blog/delete/{id}",
	 *		name="wsa_blog_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteBlogAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
