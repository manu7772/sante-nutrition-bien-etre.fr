<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\TierController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceLogg;
use ModelApi\BaseBundle\Service\servicePreference;
use ModelApi\BaseBundle\Component\Preference;
// UserBundle
use ModelApi\UserBundle\Entity\User;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Form\Type\UserAnnotateType;
use ModelApi\UserBundle\Service\serviceRoles;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use \Exception;

class UserController extends TierController {

	const ENTITY_CLASSNAME = User::class;
	const ENTITY_SERVICE = serviceUser::class;
	const ENTITY_CREATE_TYPE = UserAnnotateType::class;
	const ENTITY_UPDATE_TYPE = UserAnnotateType::class;

    // protected $eventDispatcher;
    // protected $formFactory;
    // protected $userManager;
    // protected $tokenGenerator;
    // protected $mailer;

    // /**
    //  * @param EventDispatcherInterface $eventDispatcher
    //  * @param FactoryInterface         $formFactory
    //  * @param UserManagerInterface     $userManager
    //  * @param TokenGeneratorInterface  $tokenGenerator
    //  * @param MailerInterface          $mailer
    //  */
    // public function __construct(EventDispatcherInterface $eventDispatcher, FactoryInterface $formFactory, UserManagerInterface $userManager, TokenGeneratorInterface $tokenGenerator, MailerInterface $mailer) {
    //     $this->eventDispatcher = $eventDispatcher;
    //     $this->formFactory = $formFactory;
    //     $this->userManager = $userManager;
    //     $this->tokenGenerator = $tokenGenerator;
    //     $this->mailer = $mailer;
    // }

	/**
	 * @Route(
	 *		"/tier/user/list/{role}",
	 * 		defaults={"role": null},
	 *		name="wsa_user_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TESTER')")
	 */
	public function getUsersAction($role = null, Request $request) {
		$globals['serviceRoles'] = $this->get(serviceRoles::class);
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findAllWithAvatar(false);
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/tier/user/show/{id}",
	 *		name="wsa_user_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getUserAction($id, Request $request) {
		$globals['serviceRoles'] = $this->get(serviceRoles::class);
		if($this->isGranted('ROLE_ADMIN')) {
			$globals['user_loggs'] = $this->get(serviceLogg::class)->getRepository()->findSearch(25, ['userid' => $id]);
			// Stats
			$globals['time_average'] = 0;
			foreach ($globals['user_loggs'] as $logg) $globals['time_average'] += $logg->getDuring();
			$loggCount = count($globals['user_loggs']);
			$globals['time_average'] = $loggCount > 0 ? $globals['time_average'] / $loggCount : 0;
		}
		if($this->getUser()->getId() != $id) {
			// create form for private message
			// $this->get(serviceFlashbag::class)->addFlashSweet('success', 'Create form for message to User #'.$id);
			// $gobals['form_message'] = $this->
		}
		return $this->getEntityAction($id, $request, $globals);
	}

	/**
	 * @Route(
	 *		"/tier/user/entreprise/list/{id}",
	 *		name="wsa_user_entreprise_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function getUserEntreprisesAction($id, Request $request) {
		$globals['model'] ??= $this->get(serviceEntities::class)->getModel(Entreprise::class);
		$globals['user'] = $this->getEntityById($id);
		$globals['entities'] = $globals['user']->getAssociateEntreprises();
		$globals['Pageweb'] = 'WebsiteAdminBundle:Pageweb:user_entrepriseassociates_list.html.twig';
		$globals['actions'] = '@list';
		return $this->render($globals['Pageweb'], $globals);
	}

	/**
	 * @Route(
	 *		"/tier/user/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_user_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function createUserAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/user/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_user_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function postUserAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/user/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_user_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function updateUserAction($id, Request $request, $context = null, $method = 'put') {
		$globals['entity'] ??= $this->getEntityById($id);
		if(!$this->isGranted($globals['entity']->getHigherRole(false))) throw new AccessDeniedHttpException("Vous n'avez pas les droits pour modifier cet utilisateur ".json_encode($globals['entity']->getUsername())."!");
		return $this->updateEntityAction($id, $request, $context, $method, $globals);
	}

	/**
	 * @Route(
	 *		"/tier/user/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_user_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function putUserAction($id, Request $request, $context = null) {
		$globals['entity'] ??= $this->getEntityById($id);
		if($globals['entity'] !== $this->getUser() && !$this->isGranted('ROLE_ADMIN')) throw new AccessDeniedHttpException("Vous ne pouvez pas modifier un autre utilisateur.");
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/user/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_user_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function patchUserAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/tier/user/switch-role/{user}/{role}/{status}",
	 *		defaults={"status" = null},
	 *		name="wsa_user_switch_role",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function switchRoleUserAction(User $user, $role, $status = null, Request $request) {
		$role = strtoupper($role);
		if($role !== 'ROLE_USER') {
			switch ($status) {
				case 1:
					$user->addRole($role);
					break;
				case 0:
					$user->removeRole($role);
					break;			
				default:
					if($user->hasRole($role)) $user->removeRole($role);
						else $user->addRole($role);
					break;
			}
			$this->getDoctrine()->getEntityManager()->flush();
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/user/prefered/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_user_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function preferedUserAction($id, $status = 1, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/tier/user/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_user_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function enableUserAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/tier/user/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_user_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function softdeleteUserAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}



	/**
	 * @Route(
	 *		"/tier/user/help/{id}/{status}",
	 *		defaults={"status" = -1},
	 *		name="wsa_user_help",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function helpUserAction($id, $status = -1, Request $request) {
		$user = $this->getEntityById($id);
		if(!in_array($status, [0,1])) $status = !$user->getPreferenceValue('help', false);
		$user->setPreference('help', (boolean)$status);
		$this->getDoctrine()->getEntityManager()->flush();
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/user/cookies/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_user_cookies",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function cookiesUserAction($id, $status = 1, Request $request) {
		$user = $this->getEntityById($id);
		$user->setPreference('cookies', (boolean)$status);
		$this->getDoctrine()->getEntityManager()->flush();
		if($user === $this->getUser()) {
			$session = $request->getSession();
			$session->set('accepted_cookies', $user->getPreference('cookies'));
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/user/newsletter/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_user_newsletter",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function newsletterUserAction($id, $status = 1, Request $request) {
		$user = $this->getEntityById($id);
		$user->setPreference('newsletter', (boolean)$status);
		$this->getDoctrine()->getEntityManager()->flush();
		// if($user === $this->getUser()) {
			// $session = $request->getSession();
			// $session->set('accepted_cookies', $user->getPreference('cookies'));
		// }
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/tier/user/preference/{id}/{name}/{parameter}",
	 *		name="wsa_user_parameter",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_COLLAB')")
	 */
	public function parameterUserAction($id, $name, $parameter, Request $request) {
		$user = $this->getEntityById($id);
		// $user_parameter = $user->getPreference($name, null, false, true);
		// if($user_parameter != $parameter) {
			$user->setPreference($name, $parameter);
			$this->getDoctrine()->getEntityManager()->flush();
		// }
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	// /**
	//  * @Route(
	//  *		"/tier/preference",
	//  *		name="wsa_post_user_parameter",
	//  *		options={ "method_prefix" = false },
	//  *		methods="POST"
	//  * )
	//  * @Security("has_role('ROLE_COLLAB')")
	//  */
	// public function postParameterAction(Request $request) {
	// 	$bddid = $request->request->get('entity__bddid');
	// 	$item = $this->get(serviceEntities::class)->findByBddid($bddid);
	// 	// echo('<pre><h2>DATA</h2>');var_dump($data);die('</pre>');
	// 	$success = $this->get(servicePreference::class)->fromRequest($request, $item);
	// 	if($success) $this->getDoctrine()->getEntityManager()->flush();
	// 	return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	// }

	/**
	 * @Route(
	 *		"/tier/user/sendinformations/{user}",
	 *		name="wsa_user_sendinfos",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function sendinfosUserAction($user, Request $request) {
		$this->getManager()->sendUserInformations($user);
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}






}
