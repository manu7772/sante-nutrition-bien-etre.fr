<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceCache;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// PagewebBundle
use ModelApi\PagewebBundle\Service\servicePageweb;
use ModelApi\PagewebBundle\Service\TwigDatabaseLoader;
use ModelApi\PagewebBundle\Entity\Pageweb;
use ModelApi\PagewebBundle\Form\Type\PagewebAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PagewebController extends EntityController {

	const ENTITY_CLASSNAME = Pageweb::class;
	const ENTITY_SERVICE = servicePageweb::class;
	const ENTITY_CREATE_TYPE = PagewebAnnotateType::class;
	const ENTITY_UPDATE_TYPE = PagewebAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/pageweb/list",
	 *		name="wsa_pageweb_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getPagewebsAction(Request $request) {
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->findPagewebByTypepage('root');
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/file/pageweb/show/{id}",
	 *		name="wsa_pageweb_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getPagewebAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/pageweb/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_pageweb_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createPagewebAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/pageweb/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_pageweb_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postPagewebAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/pageweb/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_pageweb_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updatePagewebAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/pageweb/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_pageweb_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putPagewebAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/pageweb/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_pageweb_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchPagewebAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/pageweb/prefered/{id}/{status}",
	 *		name="wsa_pageweb_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function preferedPagewebAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/pageweb/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_pageweb_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enablePagewebAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/pageweb/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_pageweb_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeletePagewebAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}



	/**
	 * @Route(
	 *		"/file/pageweb/update-cache",
	 *		name="wsa_pageweb_updatecache",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateCachePagewebsAction(Request $request) {
		$this->get(TwigDatabaseLoader::class)->clearTwigCache(null, 'Action in Pageweb Controller.');
		// $this->get(serviceFlashbag::class)->addFlashToastr('success', 'Templates cache mise à jour.');
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/event/pageweb/delete/{id}",
	 *		name="wsa_pageweb_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deletePagewebAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}





}
