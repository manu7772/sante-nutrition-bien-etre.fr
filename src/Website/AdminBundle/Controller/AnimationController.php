<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EventController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// EventBundle
use ModelApi\EventBundle\Service\serviceAnimation;
use ModelApi\EventBundle\Entity\Animation;
use ModelApi\EventBundle\Form\Type\AnimationAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AnimationController extends EventController {

	const ENTITY_CLASSNAME = Animation::class;
	const ENTITY_SERVICE = serviceAnimation::class;
	const ENTITY_CREATE_TYPE = AnimationAnnotateType::class;
	const ENTITY_UPDATE_TYPE = AnimationAnnotateType::class;

	// /**
	//  * @Route(
	//  *		"/event/animation/visible-agenda/{visibleagenda}/{id}",
	//  *		defaults={"id" = null},
	//  *		name="wsa_animation_visibleagenda",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  * @Security("has_role('ROLE_TRANSLATOR')")
	//  */
	// public function setAnimationVisibleagendaAction($visibleagenda, $id = null, Request $request) {
	// 	return $this->setEventVisibleagendaAction($visibleagenda, $id, $request);
	// }

	/**
	 * @Route(
	 *		"/event/animation/list",
	 *		name="wsa_animation_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getAnimationsAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/event/animation/show/{id}",
	 *		name="wsa_animation_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getAnimationAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/event/animation/create/{context}",
	 *		defaults={"context" = "create_event"},
	 *		name="wsa_animation_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createAnimationAction(Request $request, $context = 'create_event') {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/animation/create/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_animation_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postAnimationAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/animation/update/{id}/{context}/{method}",
	 *		defaults={"context" = "update_event", "method" = "put"},
	 *		name="wsa_animation_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateAnimationAction($id, Request $request, $context = "update_event", $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/event/animation/put/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_animation_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putAnimationAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/event/animation/patch/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_animation_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchAnimationAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/event/animation/prefered/{id}/{status}",
	 *		name="wsa_animation_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function preferedAnimationAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/animation/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_animation_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function enableAnimationAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/animation/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_animation_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function softdeleteAnimationAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/animation/delete/{id}",
	 *		name="wsa_animation_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteAnimationAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
