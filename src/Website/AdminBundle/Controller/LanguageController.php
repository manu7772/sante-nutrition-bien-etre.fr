<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceKernel;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\Language;
use ModelApi\InternationalBundle\Service\serviceLanguage;
use ModelApi\InternationalBundle\Form\Type\LanguageAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LanguageController extends EntityController {

	const ENTITY_CLASSNAME = Language::class;
	const ENTITY_SERVICE = serviceLanguage::class;
	const ENTITY_CREATE_TYPE = LanguageAnnotateType::class;
	const ENTITY_UPDATE_TYPE = LanguageAnnotateType::class;

	/**
	 * @Route(
	 *		"/language/list",
	 *		name="wsa_language_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getLanguagesAction(Request $request) {
		$globals['entities'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findAllWithThumbnail();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/language/show/{id}",
	 *		name="wsa_language_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getLanguageAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/language/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_language_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function createLanguageAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/language/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_language_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function postLanguageAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/language/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_language_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateLanguageAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/language/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_language_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putLanguageAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/language/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_language_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchLanguageAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/language/prefered/{id}/{status}",
	 *		name="wsa_language_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function preferedLanguageAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/language/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_language_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function enableLanguageAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/language/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_language_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function softdeleteLanguageAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/language/delete/{id}",
	 *		name="wsa_language_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteLanguageAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}





}
