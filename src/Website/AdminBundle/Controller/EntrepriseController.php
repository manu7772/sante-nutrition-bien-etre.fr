<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// UserBundle
use ModelApi\UserBundle\Service\serviceEntreprise;
use ModelApi\UserBundle\Entity\Entreprise;
use ModelApi\UserBundle\Form\Type\EntrepriseAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class EntrepriseController extends EntityController {

	const ENTITY_CLASSNAME = Entreprise::class;
	const ENTITY_SERVICE = serviceEntreprise::class;
	const ENTITY_CREATE_TYPE = EntrepriseAnnotateType::class;
	const ENTITY_UPDATE_TYPE = EntrepriseAnnotateType::class;

	/**
	 * @Route(
	 *		"/tier/entreprise/list",
	 *		name="wsa_entreprise_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getEntreprisesAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/tier/entreprise/show/{id}",
	 *		name="wsa_entreprise_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getEntrepriseAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/tier/entreprise/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_entreprise_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createEntrepriseAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/entreprise/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_entreprise_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postEntrepriseAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/entreprise/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_entreprise_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateEntrepriseAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/tier/entreprise/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_entreprise_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putEntrepriseAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/tier/entreprise/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_entreprise_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchEntrepriseAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/tier/entreprise/prefered/{id}/{status}",
	 *		name="wsa_entreprise_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function preferedEntrepriseAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/tier/entreprise/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_entreprise_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableEntrepriseAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/tier/entreprise/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_entreprise_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteEntrepriseAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/entreprise/delete/{id}",
	 *		name="wsa_entreprise_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteEntrepriseAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
