<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EventController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// EventBundle
use ModelApi\EventBundle\Service\serviceAtelier;
use ModelApi\EventBundle\Entity\Atelier;
use ModelApi\EventBundle\Form\Type\AtelierAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AtelierController extends EventController {

	const ENTITY_CLASSNAME = Atelier::class;
	const ENTITY_SERVICE = serviceAtelier::class;
	const ENTITY_CREATE_TYPE = AtelierAnnotateType::class;
	const ENTITY_UPDATE_TYPE = AtelierAnnotateType::class;

	/**
	 * @Route(
	 *		"/event/atelier/list",
	 *		name="wsa_atelier_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getAteliersAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/event/atelier/show/{id}",
	 *		name="wsa_atelier_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getAtelierAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/event/atelier/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_atelier_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createAtelierAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/atelier/create/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_atelier_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postAtelierAction(Request $request, $context = null) {
		return $this->postEntityAction($request);
	}

	/**
	 * @Route(
	 *		"/event/atelier/update/{id}/{context}/{method}",
	 * 		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_atelier_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateAtelierAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/event/atelier/put/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_atelier_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putAtelierAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/event/atelier/patch/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_atelier_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchAtelierAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request);
	}




	/**
	 * @Route(
	 *		"/event/atelier/prefered/{id}/{status}",
	 *		name="wsa_atelier_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function preferedAtelierAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/atelier/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_atelier_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableAtelierAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/atelier/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_atelier_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteAtelierAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/atelier/delete/{id}",
	 *		name="wsa_atelier_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteAtelierAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
