<?php

namespace Website\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
// FileBundle
use ModelApi\FileBundle\Entity\Item;
use ModelApi\FileBundle\Entity\File;
use ModelApi\FileBundle\Entity\Image;
use ModelApi\FileBundle\Entity\Pdf;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Entity\Text;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Entity\Directory;
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Entity\DirectoryInterface;
// use ModelApi\FileBundle\Entity\Nestlink;
use ModelApi\FileBundle\Service\FileManager;
// Pageweb
use ModelApi\PagewebBundle\Entity\Pageweb;
// CrudsBundle
use ModelApi\CrudsBundle\Service\serviceCruds;
// UserBundle
use ModelApi\UserBundle\Service\serviceUser;
use ModelApi\UserBundle\Service\serviceEntreprise;

use \Traversable;
// use \ReflectionClass;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class ConfigurationController extends Controller {

	// /**
	//  * @Route("/configuration", name="wsa_configuration", options={ "method_prefix" = false }, methods={"GET"})
	//  */
	// public function indexAction(Request $request) {
	// 	return $this->render('WebsiteAdminBundle:Superadmin:index.html.twig', []);
	// }

	/**
	 * @Route(
	 * 		"/cruds/list",
	 * 		name="wsa_cruds_list",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"},
	 * 	)
	 */
	public function crudsListAction(Request $request) {
		$globals['entities'] = $this->get(serviceCruds::class)->getRepository()->findAll();
		$globals['model'] = $this->get(serviceCruds::class)->getModel();
		return $this->render('WebsiteAdminBundle:Superadmin:cruds_list.html.twig', $globals);
	}

	/**
	 * @Route(
	 * 		"/cruds/check",
	 * 		name="wsa_cruds_check",
	 * 		options={ "method_prefix" = false },
	 * 		methods={"GET"},
	 * 	)
	 */
	public function checkCrudsEntitiesAction(Request $request) {
		$this->get(serviceCruds::class)->checkCrudsEntities();
		return $this->redirectToRoute('wsa_cruds_list');
	}




}