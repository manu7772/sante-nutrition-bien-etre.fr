<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// EventBundle
use ModelApi\EventBundle\Service\serviceEvent;
use ModelApi\EventBundle\Service\serviceJourney;
use ModelApi\EventBundle\Entity\Event;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class EventController extends EntityController {

	const ENTITY_CLASSNAME = Event::class;
	const ENTITY_SERVICE = serviceEvent::class;

	/**
	 * @Route(
	 *		"/event/check-journeys/{id}",
	 *		defaults={"id" = null},
	 *		name="wsa_event_checkjourneys",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function checkEventToJourneysAction($id = null, Request $request) {
		if(empty($id)) {
			$events = $this->get(serviceEvent::class)->getRepository()->findAll();
		} else {
			$events = $this->get(serviceEvent::class)->getRepository()->findById($id);
		}
		if(empty($events)) {
			$this->get(serviceFlashbag::class)->addFlashToastr('error', 'Événement(s) introuvable(s).');
		} else {
			foreach ($events as $event) {
				$this->get(serviceJourney::class)->checkEventToJourneys($event);
			}
			$this->getDoctrine()->getEntityManager()->flush();
			$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Événement(s) checké(s).');
		}
		$this->get(serviceJourney::class)->checkJourneysParcs();
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/event/visible-agenda/{visibleagenda}/{id}",
	 *		defaults={"id" = null},
	 *		name="wsa_event_visibleagenda",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function setEventVisibleagendaAction($visibleagenda, $id = null, Request $request) {
		if(empty($id)) {
			$events = $this->get(serviceEvent::class)->getRepository()->findAll();
		} else {
			$events = $this->get(serviceEvent::class)->getRepository()->findById($id);
		}
		if(empty($events)) {
			$this->get(serviceFlashbag::class)->addFlashToastr('error', 'Événement(s) introuvable(s).');
		} else {
			foreach ($events as $event) {
				$value = (boolean)$visibleagenda;
				$event->setVisibleagenda($value);
			}
			$this->get(serviceEntities::class)->getEntityManager()->flush();
			$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Événement(s)'.($value ? '' : ' non').' visible(s) dans les agendas.');
		}
		$this->get(serviceEvent::class)->recreateMainsCalendarCaches();
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/event/delete/{id}",
	 *		name="wsa_event_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteEventAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}




}
