<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// EventBundle
use ModelApi\EventBundle\Service\serviceSeason;
use ModelApi\EventBundle\Service\serviceJourney;
use ModelApi\EventBundle\Service\serviceEvent;
use ModelApi\EventBundle\Entity\Season;
use ModelApi\EventBundle\Form\Type\SeasonAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use DateTime;

class SeasonController extends EntityController {

	const ENTITY_CLASSNAME = Season::class;
	const ENTITY_SERVICE = serviceSeason::class;
	const ENTITY_CREATE_TYPE = SeasonAnnotateType::class;
	const ENTITY_UPDATE_TYPE = SeasonAnnotateType::class;

	/**
	 * @Route(
	 *		"/event/season/list",
	 *		name="wsa_season_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getSeasonsAction(Request $request) {
		$globals['report'] = $this->get(static::ENTITY_SERVICE)->getSeasonsReport();
		$globals['years'] = $this->get(serviceJourney::class)->getRepository()->findYears();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/event/season/show/{id}",
	 *		name="wsa_season_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getSeasonAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/event/season/create/{context}",
	 *		defaults={"context" = "create_season"},
	 *		name="wsa_season_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createSeasonAction(Request $request, $context = "create_season") {
		$globals['entity'] = $this->getManager()->createNewSeason();
		return $this->createEntityAction($request, $context, $globals);
	}

	/**
	 * @Route(
	 *		"/event/season/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_season_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postSeasonAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/season/update/{id}/{context}/{method}",
	 *		defaults={"context" = "update_season", "method" = "put"},
	 *		name="wsa_season_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateSeasonAction($id, Request $request, $context = "update_season", $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/event/season/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_season_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putSeasonAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/event/season/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_season_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchSeasonAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/event/season/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_season_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableSeasonAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/season/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_season_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteSeasonAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/season/delete/{id}",
	 *		name="wsa_season_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteSeasonAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}


	/**
	 * @Route(
	 *		"/event/season/check/{id}",
	 *		defaults={"id" = null},
	 *		name="wsa_season_check",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function checkSeasonsAction($id = null, Request $request) {
		$globals['season_check'] = $this->getManager()->checkSeasons($id, [], true);
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
		// return $this->getEntitysAction($request, $globals);
	}







}
