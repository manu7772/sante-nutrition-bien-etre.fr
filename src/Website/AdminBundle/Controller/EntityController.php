<?php

namespace Website\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// Translatable
use Gedmo\Translatable\Entity\Translation;
use Gedmo\Translatable\Translatable;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceForms;
// use ModelApi\BaseBundle\Service\serviceKernel;
use ModelApi\BaseBundle\Service\serviceRoute;
use ModelApi\BaseBundle\Form\Type\BaseAnnotateType;
// PagewebBundle
use ModelApi\PagewebBundle\Service\servicePageweb;
use ModelApi\PagewebBundle\Service\TwigDatabaseLoader;
use ModelApi\PagewebBundle\Entity\Pageweb;
// UserBundle
use ModelApi\UserBundle\Service\serviceTier;
use ModelApi\UserBundle\Service\serviceRoles;
// FileBundle
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Entity\File;
// InternationalBundle
use ModelApi\InternationalBundle\Entity\TwgTranslation;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use \ReflectionClass;
use \Exception;

class EntityController extends Controller {

	const ENTITY_CLASSNAME = null;
	const ENTITY_SERVICE = null;
	const ENTITY_CREATE_TYPE = null;
	const ENTITY_UPDATE_TYPE = null;
	const DEFAULT_ROUTE = 'website_admin_homepage';
	// const FIND_ONE_METHOD = 'findOneWithOptimizedJoins';

	protected $manager = null;

	protected function getManager() {
		if(null !== $this->manager) return $this->manager;
		if($this->has(static::ENTITY_SERVICE)) {
			// defined in Controller
			$this->manager = $this->get(static::ENTITY_SERVICE);
			return $this->manager;
		}
		$this->manager = $this->get(serviceEntities::class)->getEntityService(static::ENTITY_CLASSNAME);
		return $this->manager;
	}

	protected function getShortname($classname = null, $lowercase = false) {
		if(null === $classname) $classname = static::ENTITY_CLASSNAME;
		$shortname = serviceClasses::getShortnameIfClassname($classname);
		return $lowercase ? strtolower($shortname) : $shortname;
	}

	protected function getRepository($classname = null) {
		if(null === $classname) $classname = static::ENTITY_CLASSNAME;
		return $this->get(serviceEntities::class)->getRepository($classname);
	}

	/**
	 * Find pageweb for entity
	 * @param string $action
	 * @return Pageweb | string
	 */
	protected function getPageweb($action) {
		$list_test = [];
		$tests = array($this->getShortname(null,true),'entity');
		// Search in database
		foreach ($tests as $test) {
			$name = 'WebsiteSiteBundle:Admin:Pageweb/'.$test.'_'.$action.'.html.twig';
			$list_test[] = $name;
			$pageweb = $this->get(servicePageweb::class)->getRepository()->findOneByFilename($name);
			if(!empty($pageweb)) break;
			$name = 'WebsiteAdminBundle:Pageweb:'.$test.'_'.$action.'.html.twig';
			$list_test[] = $name;
			$pageweb = $this->get(servicePageweb::class)->getRepository()->findOneByFilename($name);
			if(!empty($pageweb)) break;
		}
		if(empty($pageweb)) {
			// If not found then get the file in views...
			foreach ($tests as $test) {
				$name = 'WebsiteSiteBundle:Admin:Pageweb/'.$test.'_'.$action.'.html.twig';
				if($this->get('templating')->exists($name)) {
					$pageweb = $name;
					break;
				}
				$name = 'WebsiteAdminBundle:Pageweb:'.$test.'_'.$action.'.html.twig';
				if($this->get('templating')->exists($name)) {
					$pageweb = $name;
					break;
				}
			}
			if(empty($pageweb)) throw new NotFoundHttpException("Not found (".$this->getShortname()."/".json_encode($action).")! Searched in database and files: ".json_encode($list_test)." but not found.");
		}
		return $pageweb;
	}

	public function redirectLastOrDefaultUrl($request, $defaultRoute = null, $params = []) {
		if(!is_string($defaultRoute)) $defaultRoute = static::DEFAULT_ROUTE;
		$url = $this->get(serviceRoute::class)->getLastOrDefaultUrl($request, $defaultRoute, $params);
		return $this->redirect($url);
	}

	/**
	 * Find entity by id
	 * @param integer|string $id
	 * @return Object
	 */
	protected function getEntityById($id, $method = null, $entity = null) {
		$entity ??= static::ENTITY_CLASSNAME;
		$method ??= serviceEntities::FIND_ONE_METHOD;
		$entity = $this->get(serviceEntities::class)->getEntityById($id, $method, $entity, false);
		if(empty($entity)) throw new NotFoundHttpException("Erreur : id ".json_encode($id)." dans ".json_encode(static::ENTITY_CLASSNAME)." n'a pu être trouvé !");
		return $entity;
	}

	/**
	 * Get entities
	 * @param Request $request
	 * @param array $globals = []
	 * @return Response
	 */
	public function getEntitysAction(Request $request, $globals = []) {
		$globals['model'] ??= $this->getManager()->getModel(static::ENTITY_CLASSNAME);
		// $globals['entities'] ??= $this->getRepository(static::ENTITY_CLASSNAME)->findAll();
		// echo('<h3>Repository method: '.(method_exists($this->getManager(), 'findByEnvironment') ? 'by environment' : 'find all').'</h3>');
		$globals['entities'] ??= method_exists($this->getManager(), 'findByEnvironment') ?
			$this->getManager()->findByEnvironment():
			$this->getManager()->findAll();
		$globals['Pageweb'] ??= $this->getPageweb('list');
		$globals['actions'] ??= '@list';
		return $this->render($globals['Pageweb'] instanceOf Pageweb ? $globals['Pageweb']->getBundlepathname() : $globals['Pageweb'], $globals);
	}

	/**
	 * Get entity
	 * @param integer|string $id
	 * @param Request $request
	 * @param array $globals = []
	 * @return Response
	 */
	public function getEntityAction($id, Request $request, $globals = []) {
		$globals['model'] ??= $this->getManager()->getModel(static::ENTITY_CLASSNAME);
		$globals['entity'] ??= is_object($id) ? $id : $this->getEntityById($id);
		$globals['Pageweb'] ??= $this->getPageweb('show');
		$globals['actions'] ??= empty($globals['entity']) ? '@list' : '@view';
		return $this->render($globals['Pageweb'] instanceOf Pageweb ? $globals['Pageweb']->getBundlepathname() : $globals['Pageweb'], $globals);
	}




	/**
	 * Create new entity
	 * @param Request $request
	 * @param string $context = null // only form_contexts
	 * @return Response
	 */
	public function createEntityAction(Request $request, $context = null, $globals = []) {
		$classname = static::ENTITY_CLASSNAME;
		if(!isset($globals['entity']) || !($globals['entity'] instanceOf $classname)) {
			$globals['entity'] = $this->getManager()->getModelForCreate(static::ENTITY_CLASSNAME);
			// $globals['entity'] = $this->getManager() instanceOf serviceEntities ? $this->getManager()->createNew(static::ENTITY_CLASSNAME) : $this->getManager()->createNew();
			// BEWARE!!! Get model for create form (--> get new entity in post only)
			// $globals['entity'] = $this->getManager() instanceOf serviceEntities ? $this->getManager()->getModel(static::ENTITY_CLASSNAME, true) : $this->getManager()->getModel(null, true);
		}
		$form = $this->get(serviceForms::class)->getCreateForm($globals['entity'], $this->getUser(), $context);
		$globals['form'] ??= $form->createView();
		$globals['entity'] = $form->getData();
		$globals['Pageweb'] ??= $this->getPageweb('create');
		$globals['actions'] = '@create';
		return $this->render($globals['Pageweb'] instanceOf Pageweb ? $globals['Pageweb']->getBundlepathname() : $globals['Pageweb'], $globals);
	}

	/**
	 * Post new entity
	 * @param Request $request
	 * @param string $context = null // only form_contexts
	 * @return Response
	 */
	public function postEntityAction(Request $request, $context = null) {
		$globals['entity'] = $this->getManager()->getModelForCreate(static::ENTITY_CLASSNAME);
		// $globals['entity'] = $this->getManager() instanceOf serviceEntities ? $this->getManager()->createNew(static::ENTITY_CLASSNAME) : $this->getManager()->createNew();
		$form = $this->get(serviceForms::class)->getCreateForm($globals['entity'], $this->getUser(), $context);
		$form->handleRequest($request);
		// FORM IS VALID
		if($form->isSubmitted() && $form->isValid()) {
			$globals['entity'] = $form->getData();
			$em = $this->getManager()->getEntityManager();
			$em->persist($globals['entity']);
			// try {
				$em->flush();
			// } catch (Exception $e) {
				// throw new Exception("Error ".__METHOD__."(): ".$e->getMessage().".", 1);
			// }
			switch (static::getSubmitName($request)) {
				case 'submit_show': return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_show', ['id' => $globals['entity']->getId()]); break;
				case 'submit_new':
					$this->getManager()->setModelForCreate($globals['entity']);
					return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_create', ['context' => $context]);
					// switch ($globals['entity']->getClassname()) {
					// 	case TwgTranslation::class:
					// 		return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_create', ['context' => $context, 'domain' => $globals['entity']->getDomain()]); 
					// 		break;
					// 	default:
					// 		return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_create', ['context' => $context]);
					// 		break;
					// }
					break;
				default: return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_list'); break;
			}
		}
		// FORM HAS ERRORS
		$globals['form'] ??= $form->createView();
		$globals['entity'] ??= $form->getData();
		$globals['Pageweb'] ??= $this->getPageweb('create');
		$globals['actions'] ??= '@create';
		return $this->render($globals['Pageweb'] instanceOf Pageweb ? $globals['Pageweb']->getBundlepathname() : $globals['Pageweb'], $globals);
	}




	/**
	 * Update Entity
	 * @param integer|string $id
	 * @param Request $request
	 * @param string $context = null // only form_contexts
	 * @param string $method = 'put'
	 * @param string $globals = []
	 * @return Response
	 */
	public function updateEntityAction($id, Request $request, $context = null, $method = 'put', $globals = []) {
		$globals['entity'] ??= $this->getEntityById($id);
		if(empty($globals['entity'])) throw new NotFoundHttpException($this->getShortname()." not found");
		$form = $this->get(serviceForms::class)->getUpdateForm($globals['entity'], $this->getUser(), $context, $method);
		$globals['form'] ??= $form->createView();
		$globals['Pageweb'] ??= $this->getPageweb('update');
		$globals['routeChoices'] ??= $this->get(serviceRoute::class)->getTemplatableBundleRoutesForChoice();
		$globals['actions'] ??= '@update';
		return $this->render($globals['Pageweb'] instanceOf Pageweb ? $globals['Pageweb']->getBundlepathname() : $globals['Pageweb'], $globals);
	}

	/**
	 * Auto Update Entity
	 * @param integer|string $id
	 * @param Request $request
	 * @param string $globals = []
	 * @return Response
	 */
	public function autoUpdateEntityAction($id, Request $request, $globals = []) {
		$globals['entity'] ??= $this->getEntityById($id);
		if(empty($globals['entity'])) throw new NotFoundHttpException($this->getShortname()." not found");
		if(method_exists($globals['entity'], 'setUpdated')) $globals['entity']->setUpdated();
		$this->getManager()->getEntityManager()->flush();
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * Put Entity
	 * @param integer|string $id
	 * @param Request $request
	 * @param string $context = null // only form_contexts
	 * @param string $globals = []
	 * @return Response
	 */
	public function putEntityAction($id, Request $request, $context = null, $globals = []) {
		$globals['entity'] ??= $this->getEntityById($id);
		// if(empty($globals['entity'])) throw new NotFoundHttpException($this->getShortname()." not found");
		$globals['form'] ??= $this->get(serviceForms::class)->getUpdateForm($globals['entity'], $this->getUser(), $context, 'put');
		$globals['form']->handleRequest($request, true);
		// FORM IS VALID
		if($globals['form']->isSubmitted() && $globals['form']->isValid()) {
			$globals['entity'] = $globals['form']->getData();
			$this->getManager()->getEntityManager()->flush();
			switch (static::getSubmitName($request)) {
				case 'submit_show': return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_show', ['id' => $globals['entity']->getId()]); break;
				case 'submit_continue': return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_update', ['id' => $globals['entity']->getId()]); break;
				case 'submit_new': return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_create'); break;
				// single submit
				default: return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_list'); break;
			}
		}
		// FORM HAS ERRORS
		$globals['Pageweb'] ??= $this->getPageweb('update');
		$globals['form'] = $globals['form']->createView();
		$globals['actions'] ??= '@update';
		return $this->render($globals['Pageweb'] instanceOf Pageweb ? $globals['Pageweb']->getBundlepathname() : $globals['Pageweb'], $globals);
	}

	/**
	 * Patch Entity
	 * @param integer|string $id
	 * @param Request $request
	 * @param string $context = null // only form_contexts
	 * @param string $globals = []
	 * @return Response
	 */
	public function patchEntityAction($id, Request $request, $context = null, $globals = []) {
		$globals['entity'] ??= $this->getEntityById($id);
		// if(empty($globals['entity'])) throw new NotFoundHttpException($this->getShortname()." not found");
		$globals['form'] ??= $this->get(serviceForms::class)->getUpdateForm($globals['entity'], $this->getUser(), $context, 'patch');
		$globals['form']->handleRequest($request, false);
		// FORM IS VALID
		if($globals['form']->isSubmitted() && $globals['form']->isValid()) {
			$globals['entity'] = $globals['form']->getData();
			$this->getManager()->getEntityManager()->flush();
			switch (static::getSubmitName($request)) {
				case 'submit_show': return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_show', ['id' => $globals['entity']->getId()]); break;
				case 'submit_continue': return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_update', ['id' => $globals['entity']->getId()]); break;
				case 'submit_new': return $this->redirectToRoute('wsa_'.$globals['entity']->getShortname(true).'_create'); break;
				// single submit
				default: return $this->redirectToRoute('wsa_'.$this->getShortname(null,true).'_list'); break;
			}
		}
		// FORM HAS ERRORS
		$globals['Pageweb'] ??= $this->getPageweb('update');
		$globals['form'] = $globals['form']->createView();
		$globals['actions'] ??= '@update';
		return $this->render($globals['Pageweb'] instanceOf Pageweb ? $globals['Pageweb']->getBundlepathname() : $globals['Pageweb'], $globals);
	}


	/**
	 * Get type of submit, for redirect
	 * @param mixed $data
	 * @return string | null
	 */
	public static function getSubmitName($data) {
		if($data instanceOf Request) $data = $data->request->all();
		foreach ($data as $key => $values) {
			# $submit = preg_replace('/^\\w+\\[(submit_?\\w+)\\]$/i', '$1', $key);
			// if(is_string($submit) && in_array($submit, ['submit_show','submit_continue','submit_new'])) return $submit;
			if(in_array($key, ['submit_show','submit_continue','submit_new'])) return $key;
			if(is_array($values)) {
				$subvalues = static::getSubmitName($values);
				if(is_string($subvalues)) return $subvalues;
			}
		}
		return null;
	}





	/**
	 * Set Prefered Entity
	 * @param integer|string $id
	 * @param integer|boolean $status = 1
	 * @param Request $request
	 * @return Response
	 */
	public function preferedEntityAction($id, $status = 1, Request $request, $globals = []) {
		$globals['entity'] ??= $this->getEntityById($id);
		if(method_exists($globals['entity'], 'setPrefered')) {
			$globals['entity']->setPrefered((boolean)$status);
			$this->getManager()->getEntityManager()->flush();
		} else {
			// not possible!
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
		// return $this->redirect($request->headers->get('referer'));
	}

	/**
	 * Enable/Disable Entity
	 * @param integer|string $id
	 * @param integer|boolean $status = 1
	 * @param Request $request
	 * @return Response
	 */
	public function enableEntityAction($id, $status = 1, Request $request, $globals = []) {
		$globals['entity'] ??= $this->getEntityById($id);
		if($this->getUser() !== $globals['entity']->getOwner() && !$this->isGranted('ROLE_ADMIN')) throw AccessDeniedHttpException("Vous n'avez pas l'autorisation pour modifier cet élément !");
		if(method_exists($globals['entity'], 'setEnabled')) {
			if($this->isGranted('ROLE_ADMIN') && (boolean)$status && !$globals['entity']->isAdminvalidated()) {
				$globals['entity']->setAdminvalidated(true);
			} else {
				$globals['entity']->setEnabled((boolean)$status);
			}
			$this->getManager()->getEntityManager()->flush();
		} else {
			// not possible!
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
		// return $this->redirect($request->headers->get('referer'));
	}

	/**
	 * Softdelete/Unsoftdelete Entity
	 * @param integer|string $id
	 * @param integer|boolean $status = 1
	 * @param Request $request
	 * @return Response
	 */
	public function softdeleteEntityAction($id, $status = 1, Request $request, $globals = []) {
		$globals['entity'] ??= $this->getEntityById($id);
		if($this->getUser() !== $globals['entity']->getOwner() && !$this->isGranted('ROLE_ADMIN')) throw AccessDeniedHttpException("Vous n'avez pas l'autorisation pour modifier cet élément !");
		$em = $this->getManager()->getEntityManager();
		if(method_exists($globals['entity'], 'setSoftdeleted')) {
			(boolean)$status ? $globals['entity']->setUnsoftdeleted() : $globals['entity']->setSoftdeleted();
			$em->flush();
		} else {
			// not possible!
			$em->remove($globals['entity']);
			$em->flush();
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
		// return $this->redirect($request->headers->get('referer'));
	}

	/**
	 * Delete/Remove Entity
	 * @param integer|string $id
	 * @param Request $request
	 * @return Response
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function deleteEntityAction($id, Request $request, $globals = []) {
		$globals['entity'] ??= $this->getEntityById($id);
		if($this->getUser() !== $globals['entity']->getOwner() && !$this->isGranted('ROLE_ADMIN')) throw AccessDeniedHttpException("Vous n'avez pas l'autorisation pour modifier cet élément !");
		$em = $this->getManager()->getEntityManager();
		$em->remove($globals['entity']);
		$em->flush();
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
		// return $this->redirect($request->headers->get('referer'));
	}






}
