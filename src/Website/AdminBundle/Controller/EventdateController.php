<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// EventBundle
use ModelApi\EventBundle\Service\serviceEventdate;
use ModelApi\EventBundle\Service\serviceEvent;
use ModelApi\EventBundle\Entity\Eventdate;
use ModelApi\EventBundle\Form\Type\EventdateAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class EventdateController extends EntityController {

	const ENTITY_CLASSNAME = Eventdate::class;
	const ENTITY_SERVICE = serviceEventdate::class;
	const ENTITY_CREATE_TYPE = EventdateAnnotateType::class;
	const ENTITY_UPDATE_TYPE = EventdateAnnotateType::class;

	/**
	 * @Route(
	 *		"/event/calendar",
	 *		name="wsa_calendar",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getCalendarAction(Request $request) {
		$globals = [];
		return $this->render('WebsiteAdminBundle:Pageweb:calendar.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/list",
	 *		name="wsa_eventdate_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getEventdatesAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/show/{id}",
	 *		name="wsa_eventdate_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getEventdateAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/update-cache",
	 *		name="wsa_calendar_updateallcachedchilds",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function updateAllCachedCalendarAction(Request $request) {
		$this->get(serviceEvent::class)->recreateMainsCalendarCaches();
		// $this->get(serviceFlashbag::class)->addFlashToastr('success', 'Tous agendas mis à jour.');
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_eventdate_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createEventdateAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_eventdate_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postEventdateAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_eventdate_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function updateEventdateAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_eventdate_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function putEventdateAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_eventdate_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function patchEventdateAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/event/eventdate/prefered/{id}/{status}",
	 *		name="wsa_eventdate_prefered",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function preferedEventdateAction($id, $status, Request $request) {
		return $this->preferedEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_eventdate_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableEventdateAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_eventdate_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteEventdateAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/delete/{id}",
	 *		name="wsa_eventdate_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteEventdateAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/event/eventdate/check",
	 *		name="wsa_eventdate_check",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function checkEventdatesAction(Request $request) {
		$this->get(serviceEventdate::class)->checkEventdates();
		$this->get(serviceFlashbag::class)->addFlashToastr('success', 'Toutes dates mises à jour.');
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}




}
