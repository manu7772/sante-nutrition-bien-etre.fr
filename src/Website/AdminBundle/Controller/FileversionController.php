<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
// FileBundle
use ModelApi\FileBundle\Entity\Fileversion;
use ModelApi\FileBundle\Service\serviceFileversion;
use ModelApi\FileBundle\Form\Type\FileversionAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FileversionController extends EntityController {

	const ENTITY_CLASSNAME = Fileversion::class;
	const ENTITY_SERVICE = serviceFileversion::class;
	const ENTITY_CREATE_TYPE = FileversionAnnotateType::class;
	const ENTITY_UPDATE_TYPE = FileversionAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/fileversion/list",
	 *		name="wsa_fileversion_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function getFileversionsAction(Request $request) {
		$globals['entities'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findAllAsCurrent();
		return $this->getEntitysAction($request, $globals);
	}

	/**
	 * @Route(
	 *		"/file/fileversion/show/{id}",
	 *		name="wsa_fileversion_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function getFileversionAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/fileversion/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_fileversion_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function updateFileversionAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/fileversion/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_fileversion_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function putFileversionAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/fileversion/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_fileversion_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function patchFileversionAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/fileversion/check-slug-links/{id}",
	 *		name="wsa_fileversion_checksluglinks",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function checksluglinksAction($id, Request $request) {
		$entity = $this->getEntityById($id);
		// $test = htmlentities($entity->getBinaryfile());
		$check = $this->get(serviceFileversion::class)->checksluglinks($entity);
		// $code = $this->get(serviceFileversion::class)->checksluglinks($entity, false, true);
		// echo('<div style="width: 45%; display: inline-block; margin: 8px;"><h3>CODE</h3><br><div style="width: 100%; height: 400px;">');echo($test);echo('</div></div>');
		// echo('<div style="width: 45%; display: inline-block; margin: 8px;"><h3>RESULT</h3><br><div style="width: 100%; height: 400px;">');echo($code);die('</div></div>');
		if($check['result']) {
			if($check['found'] > 0) $this->get(serviceFlashbag::class)->addFlashToastr('success', 'Les recherches par slugs/Bddid dans le code ont été modifiés et remplacés par des recherches par ID.<br>Nombre de remplacements effectués : '.$check['found']);
				else $this->get(serviceFlashbag::class)->addFlashToastr('info', 'Aucun remplacement trouvé. Aucune opération réalisée.');
		} else {
			$this->get(serviceFlashbag::class)->addFlashToastr('error', 'Ce fichier n\'est pas un fichier texte. Il ne peut être traité.');
		}
		return $this->redirectLastOrDefaultUrl($request, static::DEFAULT_ROUTE);
	}

	/**
	 * @Route(
	 *		"/event/fileversion/delete/{id}",
	 *		name="wsa_fileversion_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteFileversionAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}









}
