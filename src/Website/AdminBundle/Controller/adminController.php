<?php

namespace Website\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceFlashbag;
use ModelApi\BaseBundle\Service\serviceClasses;
use ModelApi\BaseBundle\Service\serviceLogg;
// FileBundle
use ModelApi\FileBundle\Entity\NestedInterface;
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceBasedirectory;
// EventBundle
use ModelApi\EventBundle\Service\serviceAtelier;
use ModelApi\EventBundle\Entity\Atelier;
use ModelApi\EventBundle\Form\Type\AtelierAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class adminController extends Controller {

	/**
	 * @Route(
	 *		"/admin/info/entities",
	 *		name="wsa_info_entities",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 */
	public function getEntitiesInfoAction(Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		// $globals['icons'] = $serviceEntities->getIcons(true);
		// $globals['logg'] = $this->get(serviceLogg::class)->getCurrentLogg();
		$globals['entities'] = $serviceEntities->getEntitiesNames(true);
		sort($globals['entities']);
		return $this->render('WebsiteAdminBundle:Pageweb:admin_info_entities.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/admin/info/entities/{entity}",
	 *		name="wsa_info_entity",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 */
	public function getEntityInfoAction($entity, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$globals['icons'] = $serviceEntities->getIcons(true);
		$globals['entities'] = $serviceEntities->getEntitiesNames(true);
		sort($globals['entities']);
		$globals['entity'] = $serviceEntities->getShortnameByAnything($entity, true);
		if(empty($globals['entity'])) {
			$this->get(serviceFlashbag::class)->addFlashSweet('error', 'L\'entité '.json_encode($entity).' n\'a pu être trouvée !');
		} else {
			$globals['entity'] = $serviceEntities->getModel($globals['entity'], true);
			$globals['schema_own_directorys'] = $this->get(serviceBasedirectory::class)->getAllSchemaDirectorys($globals['entity'], true);
		}
		$globals['actions'] = empty($globals['entity']) ? null : '@info_model';
		return $this->render('WebsiteAdminBundle:Pageweb:admin_info_entities.html.twig', $globals);
	}

	// /**
	//  * @Route(
	//  *		"/admin/checl/stored_directorys",
	//  *		name="wsa_check_strored_directorys",
	//  *		options={ "method_prefix" = false },
	//  *		methods="GET"
	//  * )
	//  */
	// public function getCheckStoredDirectorysAction(Request $request) {
	// 	set_time_limit(300);
	// 	$serviceEntities = $this->get(serviceEntities::class);
	// 	$fileManager = $this->get(FileManager::class);
	// 	$globals['entities'] = [];
	// 	foreach ($fileManager->getStoreDirContentClasses() as $classname => $shortname) {
	// 		$entities = $serviceEntities->getRepository($classname)->findAll();
	// 		foreach ($entities as $entity) if($entity instanceOf NestedInterface) $fileManager->updateStoreDirContent($entity);
	// 		$globals['entities'][$classname] = [
	// 			'shortname' => $shortname,
	// 			'entities' => $entities,
	// 		];
	// 	}
	// 	try {
	// 		$serviceEntities->getEntityManager()->flush();
	// 		$this->get(serviceFlashbag::class)->addFlashSweet('success', 'Les dossiers ont été vérifiés.');
	// 	} catch (Exception $e) {
	// 		$this->get(serviceFlashbag::class)->addFlashSweet('error', 'Erreur lors du traitement : '.$e->getMessage());
	// 	}
	// 	return $this->render('WebsiteAdminBundle:Pageweb:admin_check_strored_directorys.html.twig', $globals);
	// }


	/**
	 * @Route(
	 *		"/admin/report-entity/{bddid}",
	 *		name="wsa_report_entity",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 */
	public function getEntityReportAction($bddid, Request $request) {
		$serviceEntities = $this->get(serviceEntities::class);
		$globals = [];
		$globals['bddid'] = $bddid;
		$globals['entity'] = $serviceEntities->findByBddid($bddid);
		$globals['model'] = null;
		$globals['report'] = null;
		if(empty($globals['entity'])) {
			$this->get(serviceFlashbag::class)->addFlashSweet('error', 'L\'entité '.json_encode($entity).' n\'a pu être trouvée !');
		} else {
			$globals['model'] = $serviceEntities->getModel($globals['entity'], true);
			$globals['report'] = $serviceEntities->getEntityReport($globals['entity']);
			if(!$globals['report']->isValid()) {
				$globals['entity'] = null;
				$globals['model'] = null;
				$globals['report'] = null;
			}
		}
		$globals['actions'] = empty($globals['entity']) ? null : '@info_entity';
		return $this->render('WebsiteAdminBundle:Pageweb:admin_report_entity.html.twig', $globals);
	}









}
