<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations

// BaseBundle
// use ModelApi\BaseBundle\Service\serviceEntities;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Binary;
use ModelApi\BinaryBundle\Service\serviceBinary;
// use ModelApi\BinaryBundle\Form\Type\BinaryAnnotateType;

// BaseBundle
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceSysfiles;
// BinaryBundle
use ModelApi\BinaryBundle\Entity\Tempbinary;
use ModelApi\BinaryBundle\Service\serviceTempbinary;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class BinaryController extends EntityController {

	const ENTITY_CLASSNAME = Binary::class;
	const ENTITY_SERVICE = serviceBinary::class;
	// const ENTITY_CREATE_TYPE = BinaryAnnotateType::class;
	// const ENTITY_UPDATE_TYPE = BinaryAnnotateType::class;
	const TMP_PATH = [__DIR__, '..', '..', '..', '..', 'web', 'tmp'];

	/**
	 * @Route(
	 *		"/binary/list",
	 *		name="wsa_binary_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function getBinarysAction(Request $request) {
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->getRepository()->findAll();
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/binary/show/{id}",
	 *		name="wsa_binary_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function getBinaryAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}



	// /**
	//  * @ApiDoc(
	//  *    resource=true,
	//  *    section="File",
	//  *    description="Upload binary file",
	//  *    output = { "class" = Tempbinary::class, "collection" = false, "groups" = {"File"} },
	//  *    views = { "default", "file" }
	//  * )
	//  * @Rest\View(serializerGroups={"base"})
	//  * @Rest\Post("/upload/binary", name="upload_binary")
	//  */
	// public function uploadBinaryAction(Request $request) {
	// 	$data = $request->request->all();
	// 	$tempbinary = $this->get(serviceTempbinary::class)->createNew();
	// 	if($tempbinary instanceOf Tempbinary) {
	// 		$filepath = serviceSysfiles::createFile(serviceSysfiles::concatWithSeparators(static::TMP_PATH), $data['name'], $data['file'], true);
	// 		// $uploadFile = serviceSysfiles::getUploadFileInstance($data['file'], $data['name']);
	// 		$file = new File($filepath);
	// 		$uploadFile = new UploadedFile($filepath, $file->getFilename());
	// 		// if($uploadFile instanceOf UploadedFile) {
	// 			$tempbinary->setOriginalFileName($data['name']);
	// 			// $tempbinary->setOriginalFileName(serviceTools::getSafeFilename($data['name']));
	// 			$tempbinary->setUploadFile($uploadFile);
	// 			// $em = $this->getDoctrine()->getEntityManager();
	// 			// $em->persist($tempbinary);
	// 			// $em->flush();
	// 		// } else {
	// 			// throw new Exception("Could not generate binary file!", 1);
	// 		// }
	// 	} else {
	// 		throw new Exception("Could not create binary file!", 1);
	// 	}
	// 	return $tempbinary;
	// }








}
