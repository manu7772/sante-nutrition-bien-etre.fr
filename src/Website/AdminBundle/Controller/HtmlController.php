<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Service\serviceHtml;
use ModelApi\FileBundle\Entity\Html;
use ModelApi\FileBundle\Form\Type\HtmlAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class HtmlController extends EntityController {

	const ENTITY_CLASSNAME = Html::class;
	const ENTITY_SERVICE = serviceHtml::class;
	const ENTITY_CREATE_TYPE = HtmlAnnotateType::class;
	const ENTITY_UPDATE_TYPE = HtmlAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/html/list",
	 *		name="wsa_html_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getHtmlsAction(Request $request) {
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->findHtmlsByTier($this->getUser(), false);
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/file/html/show/{id}",
	 *		name="wsa_html_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function getHtmlAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/html/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_html_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createHtmlAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/html/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_html_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postHtmlAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/html/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_html_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function updateHtmlAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/html/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_html_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function putHtmlAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/html/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_html_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function patchHtmlAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/html/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_html_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableHtmlAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/html/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_html_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteHtmlAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/html/delete/{id}",
	 *		name="wsa_html_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteHtmlAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
