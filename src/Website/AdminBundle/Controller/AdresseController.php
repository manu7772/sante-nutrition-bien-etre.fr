<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceAdresse;
use ModelApi\BaseBundle\Entity\Adresse;
use ModelApi\BaseBundle\Form\Type\AdresseAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AdresseController extends EntityController {

	const ENTITY_CLASSNAME = Adresse::class;
	const ENTITY_SERVICE = serviceAdresse::class;
	const ENTITY_CREATE_TYPE = AdresseAnnotateType::class;
	const ENTITY_UPDATE_TYPE = AdresseAnnotateType::class;

	/**
	 * @Route(
	 *		"/adresse/list",
	 *		name="wsa_adresse_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getAdressesAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/adresse/show/{id}",
	 *		name="wsa_adresse_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getAdresseAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/adresse/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_adresse_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function createAdresseAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/adresse/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_adresse_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function postAdresseAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/adresse/update/{id}/{context}/{method}",
	 * 		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_adresse_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function updateAdresseAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, 'default', $method);
	}

	/**
	 * @Route(
	 *		"/adresse/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_adresse_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function putAdresseAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/adresse/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_adresse_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function patchAdresseAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/adresse/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_adresse_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function enableAdresseAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/adresse/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_adresse_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function softdeleteAdresseAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/adresse/delete/{id}",
	 *		name="wsa_adresse_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_USER')")
	 */
	public function deleteAdresseAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
