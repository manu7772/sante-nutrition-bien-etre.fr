<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Service\FileManager;
use ModelApi\FileBundle\Service\serviceTwig;
use ModelApi\FileBundle\Entity\Twig;
use ModelApi\FileBundle\Form\Type\TwigAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * !!!@Cache(expires="tomorrow", public=true)
 */
class TwigController extends EntityController {

	const ENTITY_CLASSNAME = Twig::class;
	const ENTITY_SERVICE = serviceTwig::class;
	const ENTITY_CREATE_TYPE = TwigAnnotateType::class;
	const ENTITY_UPDATE_TYPE = TwigAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/twig/list",
	 *		name="wsa_twig_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getTwigsAction(Request $request) {
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->findTwigsByTier($this->getUser(), false);
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/file/twig/show/{id}",
	 *		name="wsa_twig_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 * !!!@ParamConverter("id", class=Twig::class)
	 * !!!@Cache(expires="tomorrow", public=true)
	 * !!!@Entity(Twig::class, expr="repository.findOneWithOptimizedJoins(id)")
	 * !!!@Method(value="findOneWithOptimizedJoins")
	 */
	public function getTwigAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/twig/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twig_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createTwigAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/twig/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twig_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postTwigAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/twig/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_twig_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateTwigAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/twig/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twig_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putTwigAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/twig/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_twig_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchTwigAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/twig/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_twig_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableTwigAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/twig/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_twig_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteTwigAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/twig/check/{id}",
	 *		defaults={"id" = null},
	 *		name="wsa_twig_check",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function checkTwigsAction($id = null, Request $request) {
		$globals['model'] ??= $this->getManager()->getModel(static::ENTITY_CLASSNAME);
		$globals['checkeds'] ??= $this->getManager()->getAllCheckeds($id);
		return $this->render('WebsiteAdminBundle:Superadmin:check_twigs.html.twig', $globals);
	}

	/**
	 * @Route(
	 *		"/event/twig/delete/{id}",
	 *		name="wsa_twig_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteTwigAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
