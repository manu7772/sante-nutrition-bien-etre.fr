<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
// FileBundle
use ModelApi\FileBundle\Service\serviceVideo;
use ModelApi\FileBundle\Entity\Video;
use ModelApi\FileBundle\Form\Type\VideoAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class VideoController extends EntityController {

	const ENTITY_CLASSNAME = Video::class;
	const ENTITY_SERVICE = serviceVideo::class;
	const ENTITY_CREATE_TYPE = VideoAnnotateType::class;
	const ENTITY_UPDATE_TYPE = VideoAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/video/list",
	 *		name="wsa_video_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getVideosAction(Request $request) {
		// $globals['entities'] = $this->get(static::ENTITY_SERVICE)->findVideosByTier($this->getUser(), false);
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/file/video/show/{id}",
	 *		name="wsa_video_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function getVideoAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/video/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_video_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function createVideoAction(Request $request, $context = null) {
		$globals['source'] = $this->getManager()->getServiceParameter('source');
		return $this->createEntityAction($request, $context, $globals);
	}

	/**
	 * @Route(
	 *		"/file/video/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_video_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function postVideoAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/video/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_video_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function updateVideoAction($id, Request $request, $method = 'put') {
		return $this->updateEntityAction($id, $request, $method);
	}

	/**
	 * @Route(
	 *		"/file/video/put/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_video_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function putVideoAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/video/patch/{id}/{context}",
	 * 		defaults={"context" = null},
	 *		name="wsa_video_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_TRANSLATOR')")
	 */
	public function patchVideoAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/video/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_video_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function enableVideoAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/video/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_video_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function softdeleteVideoAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/video/delete/{id}",
	 *		name="wsa_video_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteVideoAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
