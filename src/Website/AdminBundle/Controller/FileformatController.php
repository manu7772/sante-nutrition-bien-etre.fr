<?php

namespace Website\AdminBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Website\AdminBundle\Controller\EntityController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// FileBundle
use ModelApi\FileBundle\Entity\Fileformat;
use ModelApi\FileBundle\Service\serviceFileformat;
use ModelApi\FileBundle\Form\Type\FileformatAnnotateType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FileformatController extends EntityController {

	const ENTITY_CLASSNAME = Fileformat::class;
	const ENTITY_SERVICE = serviceFileformat::class;
	const ENTITY_CREATE_TYPE = FileformatAnnotateType::class;
	const ENTITY_UPDATE_TYPE = FileformatAnnotateType::class;

	/**
	 * @Route(
	 *		"/file/fileformat/list",
	 *		name="wsa_fileformat_list",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function getFileformatsAction(Request $request) {
		return $this->getEntitysAction($request);
	}

	/**
	 * @Route(
	 *		"/file/fileformat/show/{id}",
	 *		name="wsa_fileformat_show",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function getFileformatAction($id, Request $request) {
		return $this->getEntityAction($id, $request);
	}

	/**
	 * @Route(
	 *		"/file/fileformat/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_fileformat_create",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function createFileformatAction(Request $request, $context = null) {
		return $this->createEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/fileformat/create/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_fileformat_post",
	 *		options={ "method_prefix" = false },
	 *		methods="POST"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function postFileformatAction(Request $request, $context = null) {
		return $this->postEntityAction($request, $context);
	}

	/**
	 * @Route(
	 *		"/file/fileformat/update/{id}/{context}/{method}",
	 *		defaults={"context" = null, "method" = "put"},
	 *		name="wsa_fileformat_update",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function updateFileformatAction($id, Request $request, $context = null, $method = 'put') {
		return $this->updateEntityAction($id, $request, $context, $method);
	}

	/**
	 * @Route(
	 *		"/file/fileformat/put/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_fileformat_put",
	 *		options={ "method_prefix" = false },
	 *		methods="PUT"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function putFileformatAction($id, Request $request, $context = null) {
		return $this->putEntityAction($id, $request, $context);
	}

	/**
	 * @Route(
	 *		"/file/fileformat/patch/{id}/{context}",
	 *		defaults={"context" = null},
	 *		name="wsa_fileformat_patch",
	 *		options={ "method_prefix" = false },
	 *		methods="PATCH"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function patchFileformatAction($id, Request $request, $context = null) {
		return $this->patchEntityAction($id, $request, $context);
	}




	/**
	 * @Route(
	 *		"/file/fileformat/enable/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_fileformat_enable",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function enableFileformatAction($id, $status = 1, Request $request) {
		return $this->enableEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/file/fileformat/softdelete/{id}/{status}",
	 *		defaults={"status" = 1},
	 *		name="wsa_fileformat_softdelete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_SUPER_ADMIN')")
	 */
	public function softdeleteFileformatAction($id, $status = 1, Request $request) {
		return $this->softdeleteEntityAction($id, $status, $request);
	}

	/**
	 * @Route(
	 *		"/event/fileformat/delete/{id}",
	 *		name="wsa_fileformat_delete",
	 *		options={ "method_prefix" = false },
	 *		methods="GET"
	 * )
	 * @Security("has_role('ROLE_EDITOR')")
	 */
	public function deleteFileformatAction($id, Request $request) {
		return $this->deleteEntityAction($id, $request);
	}







}
