<?php

namespace Website\AdminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class DefaultControllerTest extends WebTestCase {

	// private $thisKernel;

	public function testIndex() {
		$client = static::createClient();
		$crawler = $client->request('GET', '/admin');
		$statusCode = $client->getResponse()->getStatusCode();
		$this->assertTrue(in_array($statusCode, [Response::HTTP_OK, Response::HTTP_FOUND]));
		// $this->assertContains('Hello World', $client->getResponse()->getContent());
		// static::tearDown();
	}

	// private function getKernel() {
	// 	if(!is_object($this->thisKernel)) {
	// 		$this->thisKernel = static::createKernel();
	// 		$this->thisKernel->boot();
	// 	}
	// 	return $this->thisKernel;
	// }

}
