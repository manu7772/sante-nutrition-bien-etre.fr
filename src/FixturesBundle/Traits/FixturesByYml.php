<?php
// namespace FixturesBundle\Traits;

// use Doctrine\Common\Persistence\ObjectManager;
// use Symfony\Component\DependencyInjection\ContainerInterface;
// use Doctrine\Common\Collections\ArrayCollection;
// use Symfony\Component\Yaml\Parser;
// use Symfony\Component\Yaml\Dumper;
// use Gedmo\Translatable\Entity\Translation;
// // BaseBundle
// use ModelApi\BaseBundle\Service\serviceEntities;
// use ModelApi\BaseBundle\Service\serviceTools;
// use ModelApi\BaseBundle\Service\serviceKernel;
// use ModelApi\BaseBundle\Service\servicePreference;
// use ModelApi\BaseBundle\Component\Preference;
// // InternationalBundle
// use ModelApi\InternationalBundle\Service\serviceLanguage;
// use ModelApi\InternationalBundle\Service\serviceTranslation;
// // DevprintBundle
// use ModelApi\DevprintsBundle\Service\DevTerminal;
// // https://symfony.com/doc/3.4/components/filesystem.html
// use Symfony\Component\Filesystem\Filesystem;
// use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
// // NestedBundle
// use ModelApi\NestedBundle\NestedInterface\SysDirsInterface;

// use \ReflectionClass;
// use \Exception;
// use \DateTime;

// trait FixturesByYml {
// 	// https://stackoverflow.com/questions/2124195/command-line-progress-bar-in-php

// 	// const TEST_MODE = true;

// 	private $container;
// 	private $_em;
// 	private $order;
// 	private $manager;
// 	private $serviceEntities;
// 	private $serviceTranslation;
// 	private $classname;
// 	private $shortname;
// 	private $fileSystem;
// 	private $ymlfolder;
// 	private $validator;
// 	private $progress_size = 50;
// 	protected $test_mode = false;
// 	protected $timezone = '+02:00';

// 	protected function defineEntityNames() {
// 		$this->doNotDeep = false;
// 		$reflex = new ReflectionClass($this);
// 		$this->shortname = preg_replace('#^Load(.+)Data$#', '$1', $reflex->getShortName());
// 		// DevTerminal::Info("     --> Entity name: ".json_encode($this->shortname));
// 		$this->classname = $this->serviceEntities->getUniqueInstantiableClassnameByShortname($this->shortname);
// 		// DevTerminal::Info("     --> Entity class: ".json_encode($this->classname));
// 		$this->manager = $this->serviceEntities->getEntityService($this->classname);
// 		if(!$this->serviceEntities->entityExists($this->shortname, true)) {
// 			DevTerminal::Warning("     --> Entity ".json_encode($this->shortname)." does not exist! Operation aborted.");
// 			$this->doNotDeep = true;
// 		} else if(null == $this->manager) {
// 			DevTerminal::Warning("     --> Entity ".json_encode($this->shortname)." has no manager! Operation aborted.");
// 			$this->doNotDeep = true;
// 		}
// 		return $this;
// 	}

// 	// protected function getManager() {
// 	// 	$this->manager = $this->serviceEntities->getEntityService($this->classname);
// 	// 	return $this->manager;
// 	// }

// 	/**
// 	 * {@inheritDoc}
// 	 * Load data fixtures with the passed EntityManager
// 	 * @param ObjectManager $_em
// 	 */
// 	public function load(ObjectManager $_em) {
// 		$this->_em = $_em;
// 		$this->validator = $this->container->get('validator');
// 		if(method_exists($this, 'preload')) $this->preload($this->_em);

// 		// Remove files and folders
// 		if(0 === $this->getOrder()) {
// 			$fileSystem = new Filesystem();
// 			$files = [
// 				'files' => 'web/files',
// 				'tmp' => 'web/tmp',
// 				'cron' => 'var/cron/cron.yml',
// 				'aew_logs' => 'var/logs/aew_logs.yml',
// 				'cache_entities' => 'var/cache/cache_entities',
// 			];
// 			// Remove files...
// 			$i = 1;
// 			foreach ($files as $name => $file) {
// 				$path = $this->container->get(serviceKernel::class)->getBaseDir($file);
// 				if(is_string($path)) {
// 					DevTerminal::progressBar($i++, count($files), '     Removing '.$name, $this->progress_size, false, DevTerminal::WARN_STYLE);
// 					$fileSystem->remove($path);
// 					if($fileSystem->exists($path)) throw new Exception("Error ".__METHOD__."(): ".(is_dir($path) ? "directory" : "file")." ".json_encode($path)." could not be deleted!", 1);
// 				} else {
// 					$i++;
// 					// throw new Exception("Error ".__METHOD__."(): file/folder ".$file." could not be found!", 1);
// 				}
// 			}
// 			if(count($files)) {
// 				DevTerminal::progressBar($i-1, count($files), '     Files removed!', $this->progress_size, false, DevTerminal::WARN_STYLE);
// 				DevTerminal::EOL();
// 			}
// 			// Translation Disabled
// 			if(!$this->serviceTranslation->isTranslatator()) {
// 				DevTerminal::Warning("     --> TRANSLATION DISABLED: all entities translations are disabled!");
// 			}
// 			// TEST MODE
// 			if(true === $this->test_mode) DevTerminal::error("     --> BEWARE: FIXTURES LAUNCHED IN TEST MODE!!!");
// 			// YML FILE
// 			DevTerminal::Warning("     --> YML FOLDER: ".$this->ymlfolder);
// 		}

// 		if($this->doNotDeep) return $this;
// 		// $MULTIentities = array();
// 		$ONEentity = null;

// 		// File data
// 		$file = __DIR__.'/../DataFixtures/'.$this->ymlfolder.DIRECTORY_SEPARATOR.$this->shortname.'.yml';
// 		if(!@file_exists($file)) {
// 			DevTerminal::Warning("     --> WARNING: Yaml file for ".$this->shortname." does not exist! (".$file.")");
// 			return $this;
// 		}

// 		$yaml = new Parser;
// 		$entitiesList = $yaml->parse(file_get_contents($file));
// 		if(!isset($entitiesList[$this->shortname])) {
// 			DevTerminal::Warning("     --> WARNING: Yaml file for ".$this->shortname." does not contains data! (".$file.")");
// 			return $this;
// 		}
// 		$entitiesList = $entitiesList[$this->shortname];

// 		if(!isset($entitiesList['data']) || !is_array($entitiesList['data']) || count($entitiesList['data']) < 1) {
// 			DevTerminal::Warning("     --> WARNING: no data found for ".$this->shortname."! (".$file.")");
// 			return $this;
// 		}

// 		$translationRepository = $this->_em->getRepository(Translation::class);
// 		// $parameter_locale = $this->container->getParameter('locale');
// 		$parameter_locale = $this->serviceLanguage->getDefaultLocale(true);
// 		$allLanguages = $this->serviceLanguage->getLanguages(false, true);

// 		// Options & Special features
// 		$entitiesList = $this->compileEntitiesList($entitiesList);

// 		// TEST MODE
// 		if(true === $this->test_mode) {
// 			if(isset($entitiesList['test_disabled']) && $entitiesList['test_disabled'] == true) {
// 				DevTerminal::Warning("     --> DISABLED IN TEST MODE: data for ".$this->shortname." is disabled! (".$file.")");
// 				return $this;
// 			}
// 			// Only some entities / reduce data (only if not empty finally)
// 			$reduced = [];
// 			foreach ($entitiesList['data'] as $ref => $item) {
// 				if(isset($item['__test_enabled']) && true === $item['__test_enabled']) $reduced[$ref] = $item;
// 			}
// 			// $reduced = array_filter($entitiesList['data'], function ($item) { return isset($item['__test_enabled'] && true === $item['__test_enabled']); });
// 			if(count($reduced) > 0) $entitiesList['data'] = $reduced;
// 			// max entities in test mode
// 			if(isset($entitiesList['test_max']) && (integer)$entitiesList['test_max'] < count($entitiesList['data'])) {
// 				$entitiesList['data'] = array_slice($entitiesList['data'], 0, (integer)$entitiesList['test_max']);
// 			}
// 		}

// 		// Disabled
// 		if(isset($entitiesList['disabled']) && $entitiesList['disabled'] == true) {
// 			DevTerminal::Warning("     --> DISABLED: data for ".$this->shortname." is disabled! (".$file.")");
// 			return $this;
// 		}

// 		$deepControl = true; // setDeepControl
// 		if(isset($entitiesList['options']['deepControl']) && false === $entitiesList['options']['deepControl']) $deepControl = false;
// 		// if(!$deepControl) DevTerminal::Warning('     --> Deep controls for "'.$this->shortname.'": '.($deepControl ? 'enabled' : 'disabled'));

// 		$this->count = 1;
// 		$reference = true;
// 		if(isset($entitiesList['fixt_reference']) && false === $entitiesList['fixt_reference']) $reference = false;

// 		$this->dataCount = count($entitiesList['data']);
// 		DevTerminal::progressBar(0, $this->dataCount, '     >>> Preparing data...', $this->progress_size, true);

// 		foreach ($entitiesList['data'] as $ref => $values) {
// 			// TEST MODE
// 			// if(true === $this->test_mode && (!isset($values['__test_enabled']) || false === $values['__test_enabled'])) continue;
// 			// manage closures before postNew event
// 			$closureData = [];
// 			foreach ($values as $field => $value) {
// 				if(preg_match('#@twgtranslate$#', $field)) {
// 					$normal_field = preg_replace('#@.+$#', '', $field);
// 					// DevTerminal::info('     ---> '.$normal_field.' set'.ucfirst($normal_field).' => '.json_encode($value));
// 					if($this->serviceTranslation->isTranslatator()) {
// 						foreach ($value as $locale => $val) {
// 							$value[$locale] = $this->computeValue($val);
// 						}
// 						$closureData['set'.ucfirst($normal_field)] = $value;
// 					} else if(isset($value[$parameter_locale])) {
// 						$closureData['set'.ucfirst($normal_field)] = $this->computeValue($value[$parameter_locale]);
// 						unset($values[$field]);
// 						// $values[$normal_field] = $value[$parameter_locale];
// 						// DevTerminal::Warning("     --> ".$normal_field." is ".json_encode($values[$normal_field]).".");
// 					} else {
// 						DevTerminal::error("     --> ERROR: Translation is disabled. Do not care of fields with lists of translations. But no default locale (".json_encode($parameter_locale).") found!");
// 					}
// 				}
// 			}
// 			if($this->manager instanceOf serviceEntities) {
// 				$ONEentity = $this->manager->createNew($this->classname, [], function($entity) use ($closureData) {
// 					foreach ($closureData as $method => $value) {
// 						if(method_exists($entity, $method)) $entity->$method($value);
// 					}
// 				});
// 			} else {
// 				$ONEentity = $this->manager->createNew([], function($entity) use ($closureData) {
// 					foreach ($closureData as $method => $value) {
// 						if(method_exists($entity, $method)) $entity->$method($value);
// 					}
// 				});
// 			}
// 			// memorize persisted
// 			$ONEentity->__persisted = false;
// 			// $this->serviceEntities->preUpdateForForm($ONEentity);
// 			$this->serviceEntities->preCreateForForm($ONEentity);
// 			// deepControl
// 			// if(method_exists($ONEentity, 'setDeepControl')) $ONEentity->setDeepControl($deepControl);
// 			// Ref
// 			$fixture_ref = $ONEentity->getShortname().'@'.$ref;
// 			// if(method_exists($ONEentity, 'getTranslatableLocale')) {
// 			// 	$parameter_locale = $ONEentity->getTranslatableLocale();
// 			// 	DevTerminal::Warning("     --> LOCALE: ".$parameter_locale.".");
// 			// }
// 			$this->hydrateEntity($ONEentity, $values, $fixture_ref);
// 			if($reference) $this->addReference($fixture_ref, $ONEentity);
// 			$tx = $this->count >= $this->dataCount ? '     Success!' : '     Create, persist and flush';
// 			DevTerminal::progressBar($this->count++, $this->dataCount, $tx, $this->progress_size, true);
// 		}
// 		DevTerminal::EOL();
// 		return $this;
// 	}

// 	protected function hydrateEntity($ONEentity, $values, $fixture_ref) {
// 		// $POSTFLUSHS = null;
// 		foreach ($values as $field => $value) {
// 			$pd = $this->serviceEntities->getPropertyDescription($field, $ONEentity);
// 			$typeCol = 'column';
// 			if(is_array($pd)) {
// 				if(isset($pd['columnName'])) {
// 					// is column
// 					$typeCol = 'column';
// 				} else if(isset($pd['targetEntity'])) {
// 					// is association
// 					$typeCol = 'association';
// 				}
// 			}
// 			if(in_array($field, ['__test_enabled'])) continue;
// 			if(preg_match('#@twgtranslate$#', $field)) continue;
// 				// do nothing --> allready managed
// 			if(preg_match('#@translate$#', $field)) {
// 				// Translation
// 				DevTerminal::Warning('     --> Gedmo Translatable is not used anymore. Please, use Annotation twgtranslate instead!');
// 			// } else if($field === '__POST__FLUSH__OPERATIONS') {
// 			// 	// POST FLUSH OPERATIONS
// 			// 	$POSTFLUSHS = $value;
// 			} else if($field === 'preferences') {
// 				// Tier preferences
// 				$ONEentity->preferencesArrayToObjects();
// 				$ONEentity->setPreferences($value);
// 				$this->container->get(servicePreference::class)->checkPreferences($ONEentity, false);
// 				// $ONEentity->setPreferences(new Preference(Preference::getBaseName(), $value), false);
// 			} else {
// 				if(is_array($value)) {
// 					$method = 'set'.ucfirst($field);
// 					if(method_exists($ONEentity, $method)) {
// 						foreach ($value as $key => $one_value) $value[$key] = $this->computeValue($one_value);
// 						// if association
// 						if($typeCol === 'association') $value = new ArrayCollection(array_filter($value, function ($item) { return !empty($item); }));
// 						$ONEentity->$method($value);
// 					} else {
// 						$method = 'add'.ucfirst($field);
// 						if(method_exists($ONEentity, $method)) {
// 							foreach ($value as $key => $one_value) $value[$key] = $this->computeValue($one_value);
// 							// if association
// 							if($typeCol === 'association') $value = new ArrayCollection(array_filter($value, function ($item) { return !empty($item); }));
// 							foreach ($value as $key => $one_value) $ONEentity->$method($one_value);
// 						}
// 					}
// 				} else {
// 					$method = 'set'.ucfirst($field);
// 					if(!method_exists($ONEentity, $method)) $method = 'add'.ucfirst($field);
// 					if(method_exists($ONEentity, $method)) {
// 						$ONEentity->$method($this->computeValue($value));
// 					}
// 				}
// 			}
// 		}
// 		$errors = $this->validator->validate($ONEentity);
// 		// $errors = new ArrayCollection();
// 		if($errors->count() > 0) {
// 			DevTerminal::Errors("     !--> VALIDATOR ERROR: Entity ".$fixture_ref." is invalid!", $errors);
// 			// die;
// 		} else {
// 			// if(!method_exists($ONEentity, 'isActive') || $ONEentity->isActive() && true === $reference) $this->addReference($fixture_ref, $ONEentity);
// 			if(!$ONEentity->__persisted) $this->_em->persist($ONEentity);
// 			$ONEentity->__persisted = true;
// 			$this->_em->flush();
// 			// if(is_array($POSTFLUSHS)) {
// 			// 	// POST FLUSH OPERATIONS
// 			// 	$this->hydrateEntity($ONEentity, $POSTFLUSHS, $fixture_ref);
// 			// }
// 		}
// 	}

// 	// protected function getCount($data) {
// 	// 	$banneds = ['__POST__FLUSH__OPERATIONS'];
// 	// 	$valids = array_filter($data, function($key) { return !in_array($key, ['__POST__FLUSH__OPERATIONS']); }, ARRAY_FILTER_USE_KEY);
// 	// 	$dataCount = count($valids);
// 	// 	if(isset($data['__POST__FLUSH__OPERATIONS'])) $dataCount = $dataCount + $this->getCount($data['__POST__FLUSH__OPERATIONS']);
// 	// 	return $dataCount;
// 	// }

// 	protected function computeValue($value) {
// 		if(is_object($value)) return $value;
// 		if(preg_match('#^@PHP_EVAL=#', (string)$value)) {
// 			eval('$value = '.preg_replace('#^@PHP_EVAL=#', '', $value).';');
// 			return $value;
// 		}
// 		if(preg_match('#^@FIXT_REF=#', (string)$value)) {
// 			return $this->hasReference(preg_replace('#^@FIXT_REF=#', '', $value))
// 				? $this->getReference(preg_replace('#^@FIXT_REF=#', '', $value))
// 				: null;
// 		}
// 		if(preg_match('#^@FILE_URL=#', (string)$value)) {
// 			return preg_replace('#^@FILE_URL=#', preg_match('#^@FILE_URL=https?#', $value) ? '' : __DIR__."/../DataFixtures/", $value);
// 			$value = file_exists($file) ? $file : null;
// 			if(null === $value) throw new Exception("File ".$file." does not exist!", 1);
// 			return $value;
// 		}
// 		if(preg_match('#^@FILE_CONTENT=#', (string)$value)) {
// 			$file = preg_replace('#^@FILE_CONTENT=#', __DIR__."/../DataFixtures/", $value);
// 			$value = file_exists($file) ? @file_get_contents($file) : null;
// 			if(null === $value) throw new Exception("File ".$file." does not exist, or is realy empty!", 1);
// 			return $value;
// 		}
// 		return $value;
// 	}

// 	protected function compileEntitiesList($entitiesList) {
// 		$compiled_entitiesList = [];
// 		// Options
// 		if(!isset($entitiesList['options'])) $entitiesList['options'] = array();
// 		if(!isset($entitiesList['options']['onlyEnabled'])) $entitiesList['options']['onlyEnabled'] = false;
// 		// Special features
// 		foreach ($entitiesList['data'] as $ref => $values) {
// 			if(is_integer($ref)) {
// 				$list = [$ref => $values];
// 			} else {
// 				switch ($ref) {
// 					case 'mapped@BYFILES': // By files
// 						$list = $this->addEntitiesByFiles($values);
// 						if(!count($list)) throw new Exception("Error: this entity has no data description in files!", 1);
// 						break;
// 					case 'mapped@EVENTDATES': // By dates - special Evendate feature
// 						$list = $this->addEntitiesByEventdates($values);
// 						break;
// 					default:
// 						$list = [$ref => $values];
// 						break;
// 				}
// 			}
// 			// add to list
// 			foreach ($list as $key => $value) {
// 				if(false === $entitiesList['options']['onlyEnabled'] || (isset($value['enabled']) && true === $value['enabled'])) {
// 					$compiled_entitiesList[$key] = $value;
// 				}
// 			}
// 		}
// 		$entitiesList['data'] = $compiled_entitiesList;
// 		return $entitiesList;
// 	}

// 	protected function getPath($path) {
// 		return preg_match("#^\\/#", $path) ? __DIR__.'/../../..'.$path : __DIR__.DIRECTORY_SEPARATOR.$path;
// 	}

// 	/**
// 	 * Add entities by Files
// 	 * @param array $values
// 	 * @return array
// 	 */
// 	protected function addEntitiesByEventdates($values) {
// 		$newValues = array();
// 		foreach ($values as $ref => $data) {
// 			if($this->hasReference($ref)) {
// 				$ref = $this->getReference($ref);
// 				foreach ($data as $key => $values) {
// 					switch ($key) {
// 						case 'dates':
// 							foreach ($values as $dates) {
// 								$newValues[] = [
// 									'start' => new DateTime($dates['start'].$this->timezone),
// 									'end' => new DateTime($dates['end'].$this->timezone),
// 									'event' => $ref,
// 								];
// 							}
// 							break;
// 					}
// 				}
// 			}
// 		}
// 		return $newValues;
// 	}

// 	/**
// 	 * Add entities by Files
// 	 * @param array $values
// 	 * @return array
// 	 */
// 	protected function addEntitiesByFiles($values) {
// 		$newValues = array();
// 		$hasOrder = false;
// 		$newOrder = array();
// 		foreach ($values as $files) {
// 			if(!isset($files['type'])) $files['type'] = 'twig';
// 			if(!isset($files['recursive'])) $files['recursive'] = false;
// 			if(!isset($files['preg'])) $files['preg'] = '(\\.html)?\\.twig$';
// 			$files['path'] = $this->getPath($files['path']);
// 			if($this->fileSystem->exists($files['path']) && is_dir($files['path'])) {
// 				// DevTerminal::Info("     --> INFO: searching path ".json_encode($files['path'])."...");
// 				$found = scandir($files['path']);
// 				foreach ($found as $file) if(preg_match("#".$files['preg']."#", $file)) {
// 					// DevTerminal::Info("       - Found file ".json_encode($file));
// 					$file_content = @file_get_contents($files['path'].DIRECTORY_SEPARATOR.$file);
// 					if(is_string($file_content)) {
// 						// found content!
// 						$data = array();
// 						$ref = null;
// 						$order = 1000000;
// 						$data['stringAsVersion'] = preg_replace_callback("/(?:\\{#+\\s*)([!#]*)FIXT@(\\w+)\\s*=([^#\\}]+)(?:#+\\})\\s*/", function($matches) use (&$data, &$ref) {
// 								# $matches[4] = preg_replace(['/[\\n]+/','/[\\s]+/'], ['',' '], $matches[4]);
// 								// DevTerminal::Info('         • Eval: '.'$data["'.$matches[3].'"] = '.json_encode($matches[4]).';');
// 								if(strlen($matches[1]) <= 0) switch ($matches[2]) {
// 									case 'fixt_reference':
// 										eval('$ref = '.$matches[3].';');
// 										break;
// 									case '_order':
// 										eval('$order = '.$matches[3].';');
// 										$hasOrder = true;
// 										break;
// 									default:
// 										eval('$data["'.$matches[2].'"] = '.trim(preg_replace(['/[\\n\\r]+/','/[\\s]+/'], ['',' '], $matches[3])).';');
// 										break;
// 								}
// 								return null;
// 							}, $file_content);
// 						if(!is_string($ref)) {
// 							// DevTerminal::Error("      --> ERROR: you have to define a ref value \"FIXT@fixt_reference\" as comment in ".json_encode($files['path'].DIRECTORY_SEPARATOR.$file)."!");
// 						} else {
// 							$newValues[$ref] = $data;
// 							if($hasOrder) $newOrder[$ref] = $order;
// 						}
// 						// $newValues[$ref]['stringAsVersion'] = $content;
// 						// DevTerminal::Info("         • Data: ref. ".json_encode($ref));
// 					}
// 				}
// 			} else {
// 				DevTerminal::Warning("      --> WARNING: path ".json_encode($files['path'])." does not exist!");
// 			}
// 		}
// 		// ordering files…
// 		if($hasOrder) {
// 			$orderedNewValues = $newValues;
// 			$newValues = array();
// 			asort($newOrder);
// 			foreach ($newOrder as $ref => $value) $newValues[$ref] = $orderedNewValues[$ref];
// 		}
// 		return $newValues;
// 	}

// 	/**
// 	 * Sets the container.
// 	 * @param ContainerInterface|null $container A ContainerInterface instance or null
// 	 */
// 	public function setContainer(ContainerInterface $container = null) {
// 		$this->container = $container;
// 		$this->fileSystem = new Filesystem();
// 		$this->ymlfolder = $this->container->getParameter('fixtures_yml_folder');
// 		if($this->container->hasParameter('fixtures_test_mode')) $this->test_mode = $this->container->getParameter('fixtures_test_mode');
// 		$this->serviceEntities = $this->container->get(serviceEntities::class);
// 		$this->serviceLanguage = $this->container->get(serviceLanguage::class);
// 		$this->serviceTranslation = $this->container->get(serviceTranslation::class);
// 		$this->defineEntityNames();
// 	}


// 	protected function compileOrders() {
// 		$mult = 100;
// 		$order = 100000;
// 		$entities = [];
// 		$entities_last = [];
// 		$reflex = new ReflectionClass($this);
// 		$this->shortname = preg_replace('#^Load(.+)Data$#', '$1', $reflex->getShortName());
// 		// Order file
// 		$file_order = __DIR__.'/../DataFixtures/'.$this->ymlfolder.'/_order.yml';
// 		$orderlist = [];
// 		if(file_exists($file_order)) {
// 			$parser = new Parser;
// 			$orderlistdata = $parser->parse(file_get_contents($file_order));
// 			if(isset($orderlistdata['reorder']) && isset($orderlistdata['order']) && !$orderlistdata['reorder']) return $orderlistdata['order'];
// 			if(isset($orderlistdata['orders'])) $orderlist = $orderlistdata['orders'];
// 		}
// 		foreach (serviceTools::getPathFiles(__DIR__.'/../DataFixtures/ORM/') as $phpfile) {
// 			$info = pathinfo($phpfile);
// 			if(preg_match('#^Load(.+)Data\.php$#', $info['basename'])) {
// 				$file_shortname = preg_replace('#^Load(.+)Data.php$#', '$1', $info['basename']);
// 				// 1 : Order yml file
// 				foreach ($orderlist as $key => $shortname) {
// 					if($shortname === $file_shortname) $entities[($key + 1) * $mult] = $file_shortname;
// 				}
// 				// 2 : Yml file data attribute "order"
// 				if(!in_array($file_shortname, $entities)) {
// 					$ymlfile = __DIR__.'/../DataFixtures/'.$this->ymlfolder.DIRECTORY_SEPARATOR.$this->shortname.'.yml';
// 					if(file_exists($ymlfile)) {
// 						$yaml = new Parser;
// 						$entitiesList = $yaml->parse(file_get_contents($ymlfile));
// 						if(isset($entitiesList[$this->shortname]) && isset($entitiesList[$this->shortname]['order'])) {
// 							$entities[floor($entitiesList[$this->shortname]['order'] * $mult)] = $file_shortname;
// 						}
// 						// DevTerminal::Warning("     --> WARNING: Yaml file for ".$this->shortname." does not contains data! (".$ymlfile.")");
// 					}
// 				}
// 				// 3 : is last...
// 				if(!in_array($file_shortname, $entities)) $entities_last[count($entities_last) + 1] = $file_shortname;
// 			}
// 		}

// 		ksort($entities);
// 		$entities = array_values($entities);
// 		ksort($entities_last);
// 		$entities_last = array_values($entities_last);
// 		foreach ($entities_last as $shortname) $entities[] = $shortname;
// 		foreach ($entities as $key => $shortname) {
// 			if($shortname === $this->shortname) $order = $key + 1;
// 		}
// 		// Register orders in "_order.yml"
// 		$reorder = isset($orderlistdata) && isset($orderlistdata['reorder']) ? $orderlistdata['reorder'] : true;
// 		if($reorder) {
// 			$file_neworder = __DIR__.'/../DataFixtures/'.$this->ymlfolder.'/_order.yml';
// 			$dumper = new Dumper();
// 			file_put_contents($file_neworder, $dumper->dump(['reorder' => false, 'order' => $entities], 2));
// 			DevTerminal::info("     --> Reordered entities: ".PHP_EOL.'         '.json_encode($entities));
// 		}
// 		return $entities;
// 	}


// 	/**
// 	 * {@inheritDoc}
// 	 * Get the order of this fixture
// 	 * @return integer
// 	 */
// 	public function getOrder() {
// 		if(isset($this->order) && is_integer($this->order)) return $this->order;
// 		$orders = $this->compileOrders();
// 		$keys = array_keys($orders, $this->shortname);
// 		$this->order = reset($keys);
// 		return $this->order + 1;
// 	}

// }