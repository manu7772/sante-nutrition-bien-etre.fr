<?php
namespace FixturesBundle\DataFixtures\ORM;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;

Interface AbstractPreFixturesInterface {

	// protected $container;

	/**
	 * Sets the container.
	 * @param ContainerInterface|null $container A ContainerInterface instance or null
	 */
	public function setContainer(ContainerInterface $container = null);

	public function load(ObjectManager $_em);

	/**
	 * {@inheritDoc}
	 * Get the order of this fixture
	 * @return integer
	 */
	public function getOrder();

	public function isEnabled();

}
