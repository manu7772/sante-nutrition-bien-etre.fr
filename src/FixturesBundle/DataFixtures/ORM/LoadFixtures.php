<?php
namespace FixturesBundle\DataFixtures\ORM;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
// use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Finder\Finder;
use Doctrine\Common\Collections\ArrayCollection;
// BaseBundle
use ModelApi\BaseBundle\Service\serviceEntities;
use ModelApi\BaseBundle\Service\serviceTools;
use ModelApi\BaseBundle\Service\serviceKernel;
// InternationalBundle
use ModelApi\InternationalBundle\Service\serviceLanguage;
use ModelApi\InternationalBundle\Service\serviceTranslation;
// FixturesBundle
use FixturesBundle\DataFixtures\ORM\AbstractPreFixturesInterface;
// DevprintBundle
use ModelApi\DevprintsBundle\Service\DevTerminal;

use \SplFileInfo;
use \Exception;
use \ReflectionClass;
use \DateTime;

class LoadFixtures extends AbstractFixture implements ORMFixtureInterface, ContainerAwareInterface {

	use \ModelApi\BaseBundle\Service\baseService;

	const ORDER_FILENAME = __DIR__.'/fixturesParameters.yml';
	const PROGRESS_SIZE = 50;

	protected $container;
	protected $_em;
	protected $manager;
	protected $fileSystem;
	protected $parser;
	protected $fixtures_data;
	protected $serviceKernel;
	protected $serviceEntities;
	protected $serviceLanguage;
	protected $serviceTranslation;
	protected $fixturesParameters;
	protected $entities;
	protected $current_entity;
	protected $used_medias;
	protected $count;
	protected $dataCount;
	protected $progress_size;
	protected $timezone = '+02:00';

	public function load(ObjectManager $_em) {
		$this->_em = $_em;
		$parameter_locale = $this->serviceLanguage->getDefaultLocale(true);
		// Clean forlders
		$this->cleanFolders();
		// Translation Disabled
		if(!$this->serviceTranslation->isTranslatator()) {
			DevTerminal::FixturesWarning("TRANSLATION DISABLED: all entities translations are disabled!");
		}
		// LOAD ENTITIES
		foreach ($this->fixturesParameters['order'] as $key => $shortname)  {
			if(isset($this->fixtures_data[$shortname])) {
				$this->current_entity = $this->fixtures_data[$shortname];
				$this->current_entity['ymldata'] = $this->compileEntitiesList($this->current_entity['ymldata']);
				if(is_array($this->current_entity['ymldata'])) {
					$this->manager = $this->serviceEntities->getEntityService($this->current_entity['classname']);
					DevTerminal::FixturesMain("[".$key."] \033[1;32m".$shortname."\033[1;30m / type: ".$this->current_entity['type']." / service: ".$this->manager);
					$this->count = 1;
					$reference = true;
					if(isset($this->current_entity['fixt_reference']) && false === $this->current_entity['fixt_reference']) $reference = false;

					if('yml' === $this->current_entity['type']) {
						$this->dataCount = count($this->current_entity['ymldata']['data']);
						DevTerminal::progressBar(0, $this->dataCount, '>>> Computing data...', static::PROGRESS_SIZE, true);
						foreach ($this->current_entity['ymldata']['data'] as $ref => $values) if(is_array($values)) {
							$closureData = [];
							foreach ($values as $field => $value) {
								if(preg_match('#@twgtranslate$#', $field)) {
									$normal_field = preg_replace('#@.+$#', '', $field);
									if($this->serviceTranslation->isTranslatator()) {
										foreach ($value as $locale => $val) {
											$value[$locale] = $this->computeValue($val);
										}
										$closureData['set'.ucfirst($normal_field)] = $value;
									} else if(isset($value[$parameter_locale])) {
										$closureData['set'.ucfirst($normal_field)] = $this->computeValue($value[$parameter_locale]);
										unset($values[$field]);
									} else {
										DevTerminal::FixturesError("ERROR: Translation is disabled. Do not care of fields with lists of translations. But no default locale (".json_encode($parameter_locale).") found!");
									}
								}
							}
							if($this->manager instanceOf serviceEntities) {
								$ONEentity = $this->manager->createNew($this->current_entity['classname'], [], function($entity) use ($closureData) {
									foreach ($closureData as $method => $value) {
										if(method_exists($entity, $method)) $entity->$method($value);
									}
								});
							} else {
								$ONEentity = $this->manager->createNew([], function($entity) use ($closureData) {
									foreach ($closureData as $method => $value) {
										if(method_exists($entity, $method)) $entity->$method($value);
									}
								});
							}
							$this->serviceEntities->preCreateForForm($ONEentity);
							$fixture_ref = $ONEentity->getShortname().'@'.$ref;
							$ONEentity->_fixture_ref = $fixture_ref;
							$ONEentity->__persisted = false;
							$this->hydrateEntity($ONEentity, $values);
							if($reference) $this->addReference($ONEentity->_fixture_ref, $ONEentity);
							$tx = $this->count >= $this->dataCount ? 'Success!' : 'Create, persist and flush';
							DevTerminal::progressBar($this->count++, $this->dataCount, $tx, static::PROGRESS_SIZE, true);
						}
					} else if('class' === $this->current_entity['type']) {
						$this->current_entity['loader']->setContainer($this->container);
						$this->current_entity['loader']->load($this->_em);
					}
				} else {
					DevTerminal::FixturesMain("[".$key."] \033[1;32m".$shortname."\033[1;30m / type: ".$this->current_entity['type']." has no data. Aborted.", false);
				}
				DevTerminal::EOL();
			} else {
				DevTerminal::FixturesWarning("No data for entity ".json_encode($shortname).". Aborted this entity. Continue...");
			}
		}
		DevTerminal::FixturesMain("Entities hydratation terminated!");
		DevTerminal::EOL();
		return $this;
	}

	protected function cleanFolders() {
		// Remove files and folders
		$files = [
			'files' => 'web/files',
			'files_save' => 'web/files_save',
			'tmp' => 'web/tmp',
			'mail-spooler' => 'mail-spooler',
			'cron' => 'var/cron/cron.yml',
			'aew_logs' => 'var/logs/aew_logs.yml',
			'cache_entities' => 'var/cache/cache_entities',
		];
		// filter
		$serviceKernel = $this->serviceKernel;
		$files = array_filter($files, function($file) use ($serviceKernel) { return is_string($serviceKernel->getBaseDir($file)); });
		// Remove files...
		if(count($files)) {
			$i = 1;
			foreach ($files as $name => $file) {
				$path = $this->serviceKernel->getBaseDir($file);
				DevTerminal::progressBar($i++, count($files), 'Removing '.$name, static::PROGRESS_SIZE, false, DevTerminal::WARN_STYLE);
				$this->fileSystem->remove($path);
				if($this->fileSystem->exists($path)) throw new Exception("Error ".__METHOD__."(): ".(is_dir($path) ? "directory" : "file")." ".json_encode($path)." could not be deleted!", 1);
			}
			DevTerminal::progressBar($i-1, count($files), 'Cleaned forlders!', static::PROGRESS_SIZE, false, DevTerminal::WARN_STYLE);
			DevTerminal::EOL();
		}
		return $this;
	}

	/**
	 * Sets the container.
	 * @param ContainerInterface|null $container A ContainerInterface instance or null
	 */
	public function setContainer(ContainerInterface $container = null) {
		DevTerminal::FixturesMain("Fixtures pre-initialization...");
		$this->container = $container;
		$this->fileSystem = new Filesystem();
		$this->parser = new Parser();
		$this->entities = [];
		$this->used_medias = [];
		// SERVICES
		$this->serviceKernel = $this->container->get(serviceKernel::class);
		$this->serviceEntities = $this->container->get(serviceEntities::class);
		$this->serviceLanguage = $this->container->get(serviceLanguage::class);
		$this->serviceTranslation = $this->container->get(serviceTranslation::class);
		$this->validator = $this->container->get('validator');
		// Compute entities
		if($this->fileSystem->exists(static::ORDER_FILENAME) && is_file(static::ORDER_FILENAME)) {
			$this->fixturesParameters = $this->parser->parse(file_get_contents(static::ORDER_FILENAME));
		}
		if(!isset($this->fixturesParameters['remove_unused_medias']) || !is_bool($this->fixturesParameters['remove_unused_medias'])) $this->fixturesParameters['remove_unused_medias'] = false;
		if(!isset($this->fixturesParameters['reorder']) || !is_bool($this->fixturesParameters['reorder'])) $this->fixturesParameters['reorder'] = false;
		if(!isset($this->fixturesParameters['order']) || !is_array($this->fixturesParameters['order'])) $this->fixturesParameters['order'] = [];
		// ENTITIES
		foreach ($this->serviceEntities->getEntitiesNames(true) as $classname => $shortname) {
			// PHP file
			$phpfile = 'Load'.ucfirst($shortname).'Data.php';
			$ymlfile = ucfirst($shortname).'yml';
			$this->entities[$shortname] = [
				'php' => $phpfile,
				'yml' => $ymlfile,
				'shortname' => $shortname,
				'classname' => $classname,
			];
		}
		if(!count($this->entities)) DevTerminal::FixturesError("No entites found: stopped!", true, true);
		// FILES
		$this->fixtures_parameters = $this->container->getParameter('fixtures');
		$this->fixtures_data = [];
		foreach ($this->fixtures_parameters['bundles'] as $key => $bundle) {
			$path = $this->serviceKernel->getBundlePath($bundle);
			if(!empty($path)) {
				$base = $this->serviceKernel->getBundlePath($bundle, 'Resources/Fixtures');
				$entities = $this->serviceKernel->getBundlePath($bundle, 'Resources/Fixtures/entities');
				$yml = $this->serviceKernel->getBundlePath($bundle, 'Resources/Fixtures/yml');
				$media = $this->serviceKernel->getBundlePath($bundle, 'Resources/Fixtures/media');
				$bundleobject = $this->serviceKernel->getBundleByName($bundle);
				$bundlereflectionclass = new ReflectionClass($bundleobject);
				foreach ($this->entities as $shortname => $entity) {
					$bundleinfo = [
						'bundle' => $bundleobject,
						'classname' => $bundlereflectionclass->getName(),
						'namespace' => $bundlereflectionclass->getNamespaceName(),
						'paths' => [
							'bundle' => $path,
							'base' => $base,
							'entities' => is_dir($entities) ? $entities : null,
							'yml' => is_dir($yml) ? $yml : null,
							'media' => is_dir($media) ? $media : null,
						],
						'loaders' => [],
					];
					$loaderclass = $bundlereflectionclass->getNamespaceName().'\\Resources\\Fixtures\\entities\\Load'.$shortname."Data";
					$loaderyml = is_string($bundleinfo['paths']['yml']) ? $bundleinfo['paths']['yml'].$shortname.'.yml' : null;
					if(!$this->fileSystem->exists($loaderyml)) $loaderyml = null;
					$loader = null;
					if(class_exists($loaderclass)) {
						$reflectionClass = new ReflectionClass($loaderclass);
						if($reflectionClass->isSubclassOf(AbstractPreFixturesInterface::class)) {
							$loader = new $reflectionClass->name;
							if(!$loader->isEnabled()) $loader = null;
						}
					}
					if(!empty($loader)) {
						$this->fixtures_data[$shortname] = [
							'shortname' => $shortname,
							'classname' => $entity['classname'],
							'resource' => $reflectionClass->name, // classname
							'loader' => $loader,
							'type' => 'class',
							'order' => $loader->getOrder(),
							'bundle' => $bundleinfo,
						];
					} else if(is_string($loaderyml)) {
						$data = $this->parser->parse(file_get_contents($loaderyml));
						if(isset($data[$shortname])) {
							if(!isset($data[$shortname]['disabled']) || !$data[$shortname]['disabled']) {
								$this->fixtures_data[$shortname] = [
									'shortname' => $shortname,
									'classname' => $entity['classname'],
									'resource' => $loaderyml, // filename
									'ymldata' => $data[$shortname],
									'type' => 'yml',
									'order' => isset($data[$shortname]['order']) ? $data[$shortname]['order'] : $this->getEntityOrder($shortname),
									'bundle' => $bundleinfo,
								];
							}
						}
					}
				}
			} else {
				DevTerminal::FixturesError("NO bundle/path found for ".json_encode($bundle));
			}
		}
		DevTerminal::FixturesMain("Found entities: ".count($this->entities)." / Confirmed entities: ".count($this->fixtures_data));
		if(!count($this->fixtures_data)) DevTerminal::FixturesError("NO bundle found: stopped!", true, true);
		$this->reorderEntities();
		return $this;
	}

	protected function getEntityOrder($shortname) {
		$keys = array_keys($this->fixturesParameters['order'], $shortname);
		return count($keys) ? reset($keys) : -1;
	}

	protected function reorderEntities() {
		if(empty($this->entities)) throw new Exception("Error ".__METHOD__."(): list of entities is not initialized!", 1);
		// 
		$memo = serialize($this->fixturesParameters['order']);
		if($this->fixturesParameters['reorder']) {
			$fixtures_data = $this->fixtures_data;
			usort($this->fixturesParameters['order'], function($a, $b) use ($fixtures_data) {
				return $fixtures_data[$a]['order'] > $fixtures_data[$b]['order'] ? 1 : -1;
			});
		}
		foreach ($this->fixtures_data as $shortname => $data) if(!in_array($shortname, $this->fixturesParameters['order'])) {
			$this->fixturesParameters['order'][] = $shortname;
		}
		if($this->fixturesParameters['reorder'] && $memo !== serialize($this->fixturesParameters['order'])) {
			// changes so register...
			$this->fixturesParameters['reorder'] = false;
			$dumper = new Dumper();
			file_put_contents(static::ORDER_FILENAME, $dumper->dump($this->fixturesParameters, 2));
			DevTerminal::FixturesMain("Registered new order of entities");
		}
		return $this;
	}

	protected function removeUnusedMedias($folder) {
		if($this->fixturesParameters['remove_unused_medias'] && !empty($this->used_medias)) {
			$finder = new Finder();
			// Remove files
			foreach($finder->in($folder)->files() as $file) {
				if(!in_array($file->getRealpath(), $this->used_medias)) unlink($file->getRealpath());
			}
		}
		return $this;
	}












	protected function hydrateEntity($ONEentity, $values) {
		foreach ($values as $field => $value) {
			$pd = $this->serviceEntities->getPropertyDescription($field, $ONEentity);
			$typeCol = 'column';
			if(is_array($pd)) {
				if(isset($pd['columnName'])) {
					// is column
					$typeCol = 'column';
				} else if(isset($pd['targetEntity'])) {
					// is association
					$typeCol = 'association';
				}
			}
			if(in_array($field, ['__test_enabled'])) continue;
			if(preg_match('#@twgtranslate$#', $field)) continue;
				// do nothing --> allready managed
			if(preg_match('#@translate$#', $field)) {
				// Translation
				DevTerminal::FixturesWarning('Gedmo Translatable is not used anymore. Please, use Annotation twgtranslate instead!');
			// } else if($field === '__POST__FLUSH__OPERATIONS') {
			} else if($field === 'preferences') {
				// Tier preferences
				$ONEentity->setPreferences($value, true);
			} else {
				if(is_array($value)) {
					$method = 'set'.ucfirst($field);
					if(method_exists($ONEentity, $method)) {
						foreach ($value as $key => $one_value) $value[$key] = $this->computeValue($one_value);
						// if association
						if($typeCol === 'association') $value = new ArrayCollection(array_filter($value, function ($item) { return !empty($item); }));
						$ONEentity->$method($value);
					} else {
						$method = 'add'.ucfirst($field);
						if(method_exists($ONEentity, $method)) {
							foreach ($value as $key => $one_value) $value[$key] = $this->computeValue($one_value);
							// if association
							if($typeCol === 'association') $value = new ArrayCollection(array_filter($value, function ($item) { return !empty($item); }));
							foreach ($value as $key => $one_value) $ONEentity->$method($one_value);
						}
					}
				} else {
					$method = 'set'.ucfirst($field);
					if(!method_exists($ONEentity, $method)) $method = 'add'.ucfirst($field);
					if(method_exists($ONEentity, $method)) {
						$ONEentity->$method($this->computeValue($value));
					}
				}
			}
		}
		$errors = $this->validator->validate($ONEentity);
		if($errors->count() > 0) {
			DevTerminal::Errors("VALIDATOR ERROR: Entity ".$ONEentity->_fixture_ref." is invalid!", $errors);
		} else {
			if(!$ONEentity->__persisted) $this->_em->persist($ONEentity);
			$ONEentity->__persisted = true;
			$this->_em->flush();
		}
	}

	protected function computeValue($value) {
		if(is_object($value)) return $value;
		if(preg_match('#^@PHP_EVAL=#', $value)) {
			eval('$value = '.preg_replace('#^@PHP_EVAL=#', '', $value).';');
			return $value;
		}
		if(preg_match('#^@FIXT_REF=#', $value)) {
			return $this->getReference(preg_replace('#^@FIXT_REF=#', '', $value));
			// return $this->hasReference(preg_replace('#^@FIXT_REF=#', '', $value))
			// 	? $this->getReference(preg_replace('#^@FIXT_REF=#', '', $value))
			// 	: null;
		}
		if(preg_match('#^@FILE_URL=#', $value)) {
			if(preg_match('#^@FILE_URL=https?#', $value)) throw new Exception("File ".json_encode($value)." can not be get by http(s) protocole!", 1);
			// DevTerminal::FixturesMain("File URL: ".preg_replace('#^@FILE_URL=#',  '', $value));
			return $this->getCurrentPath(preg_replace('#^@FILE_URL=#',  '', $value));
		}
		if(preg_match('#^@FILE_CONTENT=#', $value)) {
			$file = $this->getCurrentPath(preg_replace('#^@FILE_CONTENT=#',  '', $value));
			return @file_get_contents($file);
		}
		return $value;
	}

	protected function getCurrentPath($path = '') {
		if(empty($path) || !is_string($path)) $path = '';
		$path = $this->current_entity['bundle']['paths']['base'].preg_replace('/^\\'.DIRECTORY_SEPARATOR.'*/', '', $path);
		if(!$this->fileSystem->exists($path)) throw new Exception("Error ".__METHOD__."(): file/path ".json_encode($path)." does not exist!", 1);
		if(@is_file($path)) {
			$this->used_medias[] = $path;
		}
		return $path;
	}

	protected function compileEntitiesList($entitiesList) {
		$compiled_entitiesList = [];
		$entitiesList['onlyEnabled'] ??= false;
		if(!array_key_exists('data', $entitiesList) || !is_array($entitiesList['data'])) return false;
		// Special features
		foreach ($entitiesList['data'] as $ref => $values) {
			if(is_integer($ref)) {
				$list = [$ref => $values];
			} else {
				switch ($ref) {
					case 'mapped@BYFILES': // By files
						$list = $this->addEntitiesByFiles($values);
						if(!count($list)) throw new Exception("Error: this entity has no data description in files!", 1);
						break;
					case 'mapped@EVENTDATES': // By dates - special Evendate feature
						$list = $this->addEntitiesByEventdates($values);
						break;
					default:
						$list = [$ref => $values];
						break;
				}
			}
			// add to list
			foreach ($list as $key => $value) {
				if(false === $entitiesList['onlyEnabled'] || (isset($value['enabled']) && true === $value['enabled'])) {
					$compiled_entitiesList[$key] = $value;
				}
			}
		}
		$entitiesList['data'] = $compiled_entitiesList;
		return $entitiesList;
	}

	/**
	 * Add entities by Files
	 * @param array $values
	 * @return array
	 */
	protected function addEntitiesByEventdates($values) {
		$newValues = array();
		foreach ($values as $ref => $data) {
			if($this->hasReference($ref)) {
				$ref = $this->getReference($ref);
				foreach ($data as $key => $values) {
					switch ($key) {
						case 'dates':
							foreach ($values as $dates) {
								$newValues[] = [
									'start' => new DateTime($dates['start'].$this->timezone),
									'end' => new DateTime($dates['end'].$this->timezone),
									'event' => $ref,
								];
							}
							break;
					}
				}
			}
		}
		return $newValues;
	}

	/**
	 * Add entities by Files
	 * @param array $values
	 * @return array
	 */
	protected function addEntitiesByFiles($values) {
		$newValues = array();
		$hasOrder = false;
		$newOrder = array();
		foreach ($values as $files) {
			if(!isset($files['type'])) $files['type'] = 'twig';
			if(!isset($files['recursive'])) $files['recursive'] = false;
			if(!isset($files['preg'])) $files['preg'] = '(\\.html)?\\.twig$';
			$files['path'] = $this->getCurrentPath($files['path']);
			if($this->fileSystem->exists($files['path']) && is_dir($files['path'])) {
				$found = scandir($files['path']);
				foreach ($found as $file) if(preg_match("#".$files['preg']."#", $file)) {
					$filepath = $files['path'].DIRECTORY_SEPARATOR.$file;
					$this->used_medias[] = $filepath;
					$file_content = @file_get_contents($filepath);
					if(is_string($file_content)) {
						// found content!
						$data = array();
						$ref = null;
						$order = 1000000;
						$data['stringAsVersion'] = preg_replace_callback("/(?:\\{#+\\s*)([!#]*)FIXT@(\\w+)\\s*=([^#\\}]+)(?:#+\\})\\s*/", function($matches) use (&$data, &$ref) {
								# $matches[4] = preg_replace(['/[\\n]+/','/[\\s]+/'], ['',' '], $matches[4]);
								if(strlen($matches[1]) <= 0) switch ($matches[2]) {
									case 'fixt_reference':
										eval('$ref = '.$matches[3].';');
										break;
									case '_order':
										eval('$order = '.$matches[3].';');
										$hasOrder = true;
										break;
									default:
										eval('$data["'.$matches[2].'"] = '.trim(preg_replace(['/[\\n\\r]+/','/[\\s]+/'], ['',' '], $matches[3])).';');
										break;
								}
								return null;
							}, $file_content);
						if(!is_string($ref)) {
							// DevTerminal::Error("ERROR: you have to define a ref value \"FIXT@fixt_reference\" as comment in ".json_encode($filepath)."!");
						} else {
							$newValues[$ref] = $data;
							if($hasOrder) $newOrder[$ref] = $order;
						}
					}
				}
			} else {
				DevTerminal::FixturesWarning("WARNING: path ".json_encode($files['path'])." does not exist!");
			}
		}
		// ordering files…
		if($hasOrder) {
			$orderedNewValues = $newValues;
			$newValues = array();
			asort($newOrder);
			foreach ($newOrder as $ref => $value) $newValues[$ref] = $orderedNewValues[$ref];
		}
		return $newValues;
	}

}
