<?php

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;
// DevprintBundle
// use ModelApi\DevprintsBundle\Service\DevTerminal;

class AppKernel extends Kernel
{
	public function registerBundles()
	{
		$bundles = [
			new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
			new Symfony\Bundle\SecurityBundle\SecurityBundle(),
			new Symfony\Bundle\TwigBundle\TwigBundle(),
			new Symfony\Bundle\MonologBundle\MonologBundle(),
			new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
			new Symfony\Bundle\AsseticBundle\AsseticBundle(),
			new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
			new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
			new FOS\RestBundle\FOSRestBundle(),
			new JMS\SerializerBundle\JMSSerializerBundle(),
			new FOS\UserBundle\FOSUserBundle(),
			new Nelmio\CorsBundle\NelmioCorsBundle(),
			new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
			new WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle(),
			new Bazinga\Bundle\HateoasBundle\BazingaHateoasBundle(),
			new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
			// new Braincrafted\Bundle\BootstrapBundle\BraincraftedBootstrapBundle(),
			// src
			// API models
			// BASE FOR API
			new ModelApi\AnnotBundle\ModelApiAnnotBundle(),
			new ModelApi\BaseBundle\ModelApiBaseBundle(),
			new ModelApi\BinaryBundle\ModelApiBinaryBundle(),
			new ModelApi\CrudsBundle\ModelApiCrudsBundle(),
			new ModelApi\DevprintsBundle\ModelApiDevprintsBundle(),
			new ModelApi\FileBundle\ModelApiFileBundle(),
			new ModelApi\EventBundle\ModelApiEventBundle(),
			new ModelApi\InternationalBundle\ModelApiInternationalBundle(),
			new ModelApi\MarketBundle\ModelApiMarketBundle(),
			new ModelApi\PagewebBundle\ModelApiPagewebBundle(),
			new ModelApi\UserBundle\ModelApiUserBundle(),
			// Website
			new Website\AdminBundle\WebsiteAdminBundle(),
			new Website\SiteBundle\WebsiteSiteBundle(),
		];

		if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
			$bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
			$bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
			$bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
			$bundles[] = new Symfony\Bundle\WebServerBundle\WebServerBundle();
			// Fixtures
			$bundles[] = new FixturesBundle\FixturesBundle();
			$bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
		}
		// DevTerminal::Warning("     --> Fixtures environment: ".json_encode($this->getEnvironment()).".");
		return $bundles;
	}

	public function getRootDir()
	{
		return __DIR__;
	}

	public function getCacheDir()
	{
		return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
	}

	public function getLogDir()
	{
		return dirname(__DIR__).'/var/logs';
	}

	public function registerContainerConfiguration(LoaderInterface $loader)
	{
		$loader->load(function (ContainerBuilder $container) {
			$container->setParameter('container.autowiring.strict_mode', true);
			$container->setParameter('container.dumper.inline_class_loader', true);

			$container->addObjectResource($this);
		});
		$loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
	}
}
